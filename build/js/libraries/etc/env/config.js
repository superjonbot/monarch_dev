require.config({
    baseUrl:"./js",
    paths : {
        underscore:'./libraries/underscore/underscore',
        jquery : "./libraries/jquery1x/jquery-1.10.2",
        modernizr:"./libraries/modernizr/modernizr.custom.43687",
        x2js:"./libraries/parsing/x2js/xml2json",
        knockout:"./libraries/knockout/knockout-2.3.0",
        i18n:"./libraries/require/i18n",
        dust:"./libraries/dust/dist/dust-full-2.0.0.min",
        tweenmax:"./libraries/greensocks/TweenMax.min",
        tweenlite:"./libraries/greensocks/TweenLite.min",
        timelinelite:"./libraries/greensocks/TimelineLite.min",
        timelinemax:"./libraries/greensocks/TimelineMax.min",
        hammer:"./libraries/hammer/jquery.hammer",
        scrollto:"./libraries/greensocks/plugins/ScrollToPlugin.min"

    },
    shim:{
        'underscore': {
            exports: '_'
        },
        'modernizr': {
            exports: 'Modernizr'
        },
        'x2js': {
            exports: 'X2JS'
        },
        'dust': {
            exports: 'dust'
        },
        'tweenmax': {
            exports: 'TweenMax'
        },
        'tweenlite': {
            exports: 'TweenLite'
        },
        'timelinelite': {
            exports: 'TimelineLite'
        },
        'timelinemax': {
            exports: 'TimelineMax'
        },
        'hammer': {
            exports: 'Hammer'
        },
        'scrollto': {
            exports: 'Scrollto'
        }
    }
    //,config:{i18n:{locale:'fr-fr'}} //uncomment to test/force internationalization


});