/**
 * asyncloader Instance
 * sample by Jonathan Robles
 *
 * Date: 12/12/13 : 3:36 PM
 *
 */

// Start main asyncloader instance

require(['jquery', 'modules/data/asyncloader'],
    function ($, Asyncloader) {

        var myinstancetest = new Asyncloader({
            //variable:value
        });

        myinstancetest.init();


    });


// End main asyncloader instance




