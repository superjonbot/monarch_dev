/**
 * slideshow_promo Instance
 * sample by Jonathan Robles
 *
 * Date: 1/15/14 : 2:44 PM
 *
 */

// Start main slideshow_promo instance

require(['jquery', 'templates/slideshow_promo'],
    function ($, Slideshow) {

        var myinstancetest = new Slideshow({
            //variable:value
        });

        myinstancetest.init();

        $(document).keydown(function(e){
            if (e.keyCode == 37) {
                myinstancetest._var().viewmodel.slide_prev()
                return false;
            }
            if (e.keyCode == 39) {
                myinstancetest._var().viewmodel.slide_next()
                return false;

            }
        });



    });


// End main slideshow_promo instance




