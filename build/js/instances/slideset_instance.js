/**
 * slideset Instance
 * sample by Jonathan Robles
 *
 * Date: <inZertDATe>
 *
 */

// Start the main app logic.


require(['jquery', 'modules/controllers/slideset'],
    function ($, Slideset) {




        var mytemplatetest = new Slideset({

            //variables here over ride any default variables, for example, uncomment below to get your typical boring medscape slideshow

            /* child_focus:function(o){
             var argz=o;
             var tempObject= $(this.childObjects[argz.index]);
             tempObject.show();

             },
             child_unfocus:function(o){
             var argz=o
             var tempObject= $(this.childObjects[argz.unfocusedindex]);
             tempObject.hide();
             }*/


        });

        mytemplatetest.init();




        //* EVERYTHING BELOW IS FOR THE TEMPORARY NAVIGATION - since this is just a container object it does not have navigation on its own

        $(document).keypress(function(event){

            if(event.keyCode==97){mytemplatetest.prev();}
            if(event.keyCode==115){mytemplatetest.next();}

        });



        var navitems=$('#navigation').children();

        $(navitems[0]).bind('click',function(event){
            mytemplatetest.pagejump(0)
        })
        $(navitems[1]).bind('click',function(event){
            mytemplatetest.prev();
        })

        $(navitems[2]).bind('click',function(event){
            mytemplatetest.next();
        })

        $(navitems[3]).bind('click',function(event){
            mytemplatetest.pagejump(6)
        })

        $(navitems[4]).bind('click',function(event){
            mytemplatetest.pagejump(4)
        })

        $(navitems[5]).bind('click',function(event){
            mytemplatetest.jump(4)
        })

        $(navitems[6]).bind('click',function(event){
            mytemplatetest.quickjump(4)
        })

    });





