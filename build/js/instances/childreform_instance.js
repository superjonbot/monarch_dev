/**
 * childreform Instance
 * sample by Jonathan Robles
 *
 * Date: 1/24/14 : 4:44 PM
 *
 */

// Start main childreform instance

require(['jquery', 'modules/view/childreform'],
    function ($, Childreform) {

        var myinstancetest = new Childreform({
            //variable:value
        });

        myinstancetest.init();


    });


// End main childreform instance




