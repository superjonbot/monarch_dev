/**
 * flexibleregulatory Instance
 * sample by Jonathan Robles
 *
 * Date: 12/17/13 : 3:00 PM
 *
 */

// Start main flexibleregulatory instance

require(['jquery', 'templates/flexibleregulatory'],
    function ($, FRFramework) {
        var myinstancetest = new FRFramework().init();
    });

//your typical template javascript (crazy easy peasy right?!

// End main flexibleregulatory instance




