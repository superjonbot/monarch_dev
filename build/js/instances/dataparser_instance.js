// dataparser demo
// Start the main app logic.
require(['jquery', 'underscore', 'modules/parsers/dataparser'],
    function ($, _, Template) {

        //define object, lets get an xml file and alert part of it
        var myjsontest = new Template({
            file: 'data/sitedata.xml',
            format: 'text',
            callback: function (data) {
                alert('Successfully read data!: ' + data.status + ' ' + this.busy)
            }
        });

        //Initializes object, also does a refresh, default callback is an alert so data is alerted
        myjsontest.init();

        //use the _var() command to change initializing variables in the object, lets get a json object now.
        myjsontest._var({
            file: 'data/sitedata.json',
            format: 'json'
        });

        //use the refresh command to refresh the object and execute the callback with new variables
        myjsontest.refresh();

        //lets do it again, but now with a jsonp file
        myjsontest._var({
            file: 'data/sitedata.jsonp',
            format: 'jsonp'
        });

        //use the refresh command to refresh the object and execute the callback with new variables
        myjsontest.refresh();



    });

// End main app logic.