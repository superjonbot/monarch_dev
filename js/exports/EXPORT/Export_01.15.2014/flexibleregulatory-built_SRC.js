/*!Last Updated: 01.15.2014[16.41.01] by Jonathan Robles*/



/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! Brand Advance Template                       */
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*CL1*/


/*!Last Updated: 01.15.2014[16.41.01] by Jonathan Robles*/
var requirejs, require, define;

(function(global) {
    var req, s, head, baseElement, dataMain, src, interactiveScript, currentlyAddingScript, mainScript, subPath, version = "2.1.8", commentRegExp = /(\/\*([\s\S]*?)\*\/|([^:]|^)\/\/(.*)$)/gm, cjsRequireRegExp = /[^.]\s*require\s*\(\s*["']([^'"\s]+)["']\s*\)/g, jsSuffixRegExp = /\.js$/, currDirRegExp = /^\.\//, op = Object.prototype, ostring = op.toString, hasOwn = op.hasOwnProperty, ap = Array.prototype, apsp = ap.splice, isBrowser = !!(typeof window !== "undefined" && navigator && window.document), isWebWorker = !isBrowser && typeof importScripts !== "undefined", readyRegExp = isBrowser && navigator.platform === "PLAYSTATION 3" ? /^complete$/ : /^(complete|loaded)$/, defContextName = "_", isOpera = typeof opera !== "undefined" && opera.toString() === "[object Opera]", contexts = {}, cfg = {}, globalDefQueue = [], useInteractive = false;
    function isFunction(it) {
        return ostring.call(it) === "[object Function]";
    }
    function isArray(it) {
        return ostring.call(it) === "[object Array]";
    }
    function each(ary, func) {
        if (ary) {
            var i;
            for (i = 0; i < ary.length; i += 1) if (ary[i] && func(ary[i], i, ary)) break;
        }
    }
    function eachReverse(ary, func) {
        if (ary) {
            var i;
            for (i = ary.length - 1; i > -1; i -= 1) if (ary[i] && func(ary[i], i, ary)) break;
        }
    }
    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }
    function getOwn(obj, prop) {
        return hasProp(obj, prop) && obj[prop];
    }
    function eachProp(obj, func) {
        var prop;
        for (prop in obj) if (hasProp(obj, prop)) if (func(obj[prop], prop)) break;
    }
    function mixin(target, source, force, deepStringMixin) {
        if (source) eachProp(source, function(value, prop) {
            if (force || !hasProp(target, prop)) if (deepStringMixin && typeof value !== "string") {
                if (!target[prop]) target[prop] = {};
                mixin(target[prop], value, force, deepStringMixin);
            } else target[prop] = value;
        });
        return target;
    }
    function bind(obj, fn) {
        return function() {
            return fn.apply(obj, arguments);
        };
    }
    function scripts() {
        return document.getElementsByTagName("script");
    }
    function defaultOnError(err) {
        throw err;
    }
    function getGlobal(value) {
        if (!value) return value;
        var g = global;
        each(value.split("."), function(part) {
            g = g[part];
        });
        return g;
    }
    function makeError(id, msg, err, requireModules) {
        var e = new Error(msg + "\nhttp://requirejs.org/docs/errors.html#" + id);
        e.requireType = id;
        e.requireModules = requireModules;
        if (err) e.originalError = err;
        return e;
    }
    if (typeof define !== "undefined") return;
    if (typeof requirejs !== "undefined") {
        if (isFunction(requirejs)) return;
        cfg = requirejs;
        requirejs = undefined;
    }
    if (typeof require !== "undefined" && !isFunction(require)) {
        cfg = require;
        require = undefined;
    }
    function newContext(contextName) {
        var inCheckLoaded, Module, context, handlers, checkLoadedTimeoutId, config = {
            waitSeconds: 7,
            baseUrl: "./",
            paths: {},
            pkgs: {},
            shim: {},
            config: {}
        }, registry = {}, enabledRegistry = {}, undefEvents = {}, defQueue = [], defined = {}, urlFetched = {}, requireCounter = 1, unnormalizedCounter = 1;
        function trimDots(ary) {
            var i, part;
            for (i = 0; ary[i]; i += 1) {
                part = ary[i];
                if (part === ".") {
                    ary.splice(i, 1);
                    i -= 1;
                } else if (part === "..") if (i === 1 && (ary[2] === ".." || ary[0] === "..")) break; else if (i > 0) {
                    ary.splice(i - 1, 2);
                    i -= 2;
                }
            }
        }
        function normalize(name, baseName, applyMap) {
            var pkgName, pkgConfig, mapValue, nameParts, i, j, nameSegment, foundMap, foundI, foundStarMap, starI, baseParts = baseName && baseName.split("/"), normalizedBaseParts = baseParts, map = config.map, starMap = map && map["*"];
            if (name && name.charAt(0) === ".") if (baseName) {
                if (getOwn(config.pkgs, baseName)) normalizedBaseParts = baseParts = [ baseName ]; else normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                name = normalizedBaseParts.concat(name.split("/"));
                trimDots(name);
                pkgConfig = getOwn(config.pkgs, pkgName = name[0]);
                name = name.join("/");
                if (pkgConfig && name === pkgName + "/" + pkgConfig.main) name = pkgName;
            } else if (name.indexOf("./") === 0) name = name.substring(2);
            if (applyMap && map && (baseParts || starMap)) {
                nameParts = name.split("/");
                for (i = nameParts.length; i > 0; i -= 1) {
                    nameSegment = nameParts.slice(0, i).join("/");
                    if (baseParts) for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = getOwn(map, baseParts.slice(0, j).join("/"));
                        if (mapValue) {
                            mapValue = getOwn(mapValue, nameSegment);
                            if (mapValue) {
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                    if (foundMap) break;
                    if (!foundStarMap && starMap && getOwn(starMap, nameSegment)) {
                        foundStarMap = getOwn(starMap, nameSegment);
                        starI = i;
                    }
                }
                if (!foundMap && foundStarMap) {
                    foundMap = foundStarMap;
                    foundI = starI;
                }
                if (foundMap) {
                    nameParts.splice(0, foundI, foundMap);
                    name = nameParts.join("/");
                }
            }
            return name;
        }
        function removeScript(name) {
            if (isBrowser) each(scripts(), function(scriptNode) {
                if (scriptNode.getAttribute("data-requiremodule") === name && scriptNode.getAttribute("data-requirecontext") === context.contextName) {
                    scriptNode.parentNode.removeChild(scriptNode);
                    return true;
                }
            });
        }
        function hasPathFallback(id) {
            var pathConfig = getOwn(config.paths, id);
            if (pathConfig && isArray(pathConfig) && pathConfig.length > 1) {
                removeScript(id);
                pathConfig.shift();
                context.require.undef(id);
                context.require([ id ]);
                return true;
            }
        }
        function splitPrefix(name) {
            var prefix, index = name ? name.indexOf("!") : -1;
            if (index > -1) {
                prefix = name.substring(0, index);
                name = name.substring(index + 1, name.length);
            }
            return [ prefix, name ];
        }
        function makeModuleMap(name, parentModuleMap, isNormalized, applyMap) {
            var url, pluginModule, suffix, nameParts, prefix = null, parentName = parentModuleMap ? parentModuleMap.name : null, originalName = name, isDefine = true, normalizedName = "";
            if (!name) {
                isDefine = false;
                name = "_@r" + (requireCounter += 1);
            }
            nameParts = splitPrefix(name);
            prefix = nameParts[0];
            name = nameParts[1];
            if (prefix) {
                prefix = normalize(prefix, parentName, applyMap);
                pluginModule = getOwn(defined, prefix);
            }
            if (name) if (prefix) if (pluginModule && pluginModule.normalize) normalizedName = pluginModule.normalize(name, function(name) {
                return normalize(name, parentName, applyMap);
            }); else normalizedName = normalize(name, parentName, applyMap); else {
                normalizedName = normalize(name, parentName, applyMap);
                nameParts = splitPrefix(normalizedName);
                prefix = nameParts[0];
                normalizedName = nameParts[1];
                isNormalized = true;
                url = context.nameToUrl(normalizedName);
            }
            suffix = prefix && !pluginModule && !isNormalized ? "_unnormalized" + (unnormalizedCounter += 1) : "";
            return {
                prefix: prefix,
                name: normalizedName,
                parentMap: parentModuleMap,
                unnormalized: !!suffix,
                url: url,
                originalName: originalName,
                isDefine: isDefine,
                id: (prefix ? prefix + "!" + normalizedName : normalizedName) + suffix
            };
        }
        function getModule(depMap) {
            var id = depMap.id, mod = getOwn(registry, id);
            if (!mod) mod = registry[id] = new context.Module(depMap);
            return mod;
        }
        function on(depMap, name, fn) {
            var id = depMap.id, mod = getOwn(registry, id);
            if (hasProp(defined, id) && (!mod || mod.defineEmitComplete)) {
                if (name === "defined") fn(defined[id]);
            } else {
                mod = getModule(depMap);
                if (mod.error && name === "error") fn(mod.error); else mod.on(name, fn);
            }
        }
        function onError(err, errback) {
            var ids = err.requireModules, notified = false;
            if (errback) errback(err); else {
                each(ids, function(id) {
                    var mod = getOwn(registry, id);
                    if (mod) {
                        mod.error = err;
                        if (mod.events.error) {
                            notified = true;
                            mod.emit("error", err);
                        }
                    }
                });
                if (!notified) req.onError(err);
            }
        }
        function takeGlobalQueue() {
            if (globalDefQueue.length) {
                apsp.apply(defQueue, [ defQueue.length - 1, 0 ].concat(globalDefQueue));
                globalDefQueue = [];
            }
        }
        handlers = {
            require: function(mod) {
                if (mod.require) return mod.require; else return mod.require = context.makeRequire(mod.map);
            },
            exports: function(mod) {
                mod.usingExports = true;
                if (mod.map.isDefine) if (mod.exports) return mod.exports; else return mod.exports = defined[mod.map.id] = {};
            },
            module: function(mod) {
                if (mod.module) return mod.module; else return mod.module = {
                    id: mod.map.id,
                    uri: mod.map.url,
                    config: function() {
                        var c, pkg = getOwn(config.pkgs, mod.map.id);
                        c = pkg ? getOwn(config.config, mod.map.id + "/" + pkg.main) : getOwn(config.config, mod.map.id);
                        return c || {};
                    },
                    exports: defined[mod.map.id]
                };
            }
        };
        function cleanRegistry(id) {
            delete registry[id];
            delete enabledRegistry[id];
        }
        function breakCycle(mod, traced, processed) {
            var id = mod.map.id;
            if (mod.error) mod.emit("error", mod.error); else {
                traced[id] = true;
                each(mod.depMaps, function(depMap, i) {
                    var depId = depMap.id, dep = getOwn(registry, depId);
                    if (dep && !mod.depMatched[i] && !processed[depId]) if (getOwn(traced, depId)) {
                        mod.defineDep(i, defined[depId]);
                        mod.check();
                    } else breakCycle(dep, traced, processed);
                });
                processed[id] = true;
            }
        }
        function checkLoaded() {
            var map, modId, err, usingPathFallback, waitInterval = config.waitSeconds * 1e3, expired = waitInterval && context.startTime + waitInterval < new Date().getTime(), noLoads = [], reqCalls = [], stillLoading = false, needCycleCheck = true;
            if (inCheckLoaded) return;
            inCheckLoaded = true;
            eachProp(enabledRegistry, function(mod) {
                map = mod.map;
                modId = map.id;
                if (!mod.enabled) return;
                if (!map.isDefine) reqCalls.push(mod);
                if (!mod.error) if (!mod.inited && expired) if (hasPathFallback(modId)) {
                    usingPathFallback = true;
                    stillLoading = true;
                } else {
                    noLoads.push(modId);
                    removeScript(modId);
                } else if (!mod.inited && mod.fetched && map.isDefine) {
                    stillLoading = true;
                    if (!map.prefix) return needCycleCheck = false;
                }
            });
            if (expired && noLoads.length) {
                err = makeError("timeout", "Load timeout for modules: " + noLoads, null, noLoads);
                err.contextName = context.contextName;
                return onError(err);
            }
            if (needCycleCheck) each(reqCalls, function(mod) {
                breakCycle(mod, {}, {});
            });
            if ((!expired || usingPathFallback) && stillLoading) if ((isBrowser || isWebWorker) && !checkLoadedTimeoutId) checkLoadedTimeoutId = setTimeout(function() {
                checkLoadedTimeoutId = 0;
                checkLoaded();
            }, 50);
            inCheckLoaded = false;
        }
        Module = function(map) {
            this.events = getOwn(undefEvents, map.id) || {};
            this.map = map;
            this.shim = getOwn(config.shim, map.id);
            this.depExports = [];
            this.depMaps = [];
            this.depMatched = [];
            this.pluginMaps = {};
            this.depCount = 0;
        };
        Module.prototype = {
            init: function(depMaps, factory, errback, options) {
                options = options || {};
                if (this.inited) return;
                this.factory = factory;
                if (errback) this.on("error", errback); else if (this.events.error) errback = bind(this, function(err) {
                    this.emit("error", err);
                });
                this.depMaps = depMaps && depMaps.slice(0);
                this.errback = errback;
                this.inited = true;
                this.ignore = options.ignore;
                if (options.enabled || this.enabled) this.enable(); else this.check();
            },
            defineDep: function(i, depExports) {
                if (!this.depMatched[i]) {
                    this.depMatched[i] = true;
                    this.depCount -= 1;
                    this.depExports[i] = depExports;
                }
            },
            fetch: function() {
                if (this.fetched) return;
                this.fetched = true;
                context.startTime = new Date().getTime();
                var map = this.map;
                if (this.shim) context.makeRequire(this.map, {
                    enableBuildCallback: true
                })(this.shim.deps || [], bind(this, function() {
                    return map.prefix ? this.callPlugin() : this.load();
                })); else return map.prefix ? this.callPlugin() : this.load();
            },
            load: function() {
                var url = this.map.url;
                if (!urlFetched[url]) {
                    urlFetched[url] = true;
                    context.load(this.map.id, url);
                }
            },
            check: function() {
                if (!this.enabled || this.enabling) return;
                var err, cjsModule, id = this.map.id, depExports = this.depExports, exports = this.exports, factory = this.factory;
                if (!this.inited) this.fetch(); else if (this.error) this.emit("error", this.error); else if (!this.defining) {
                    this.defining = true;
                    if (this.depCount < 1 && !this.defined) {
                        if (isFunction(factory)) {
                            if (this.events.error && this.map.isDefine || req.onError !== defaultOnError) try {
                                exports = context.execCb(id, factory, depExports, exports);
                            } catch (e) {
                                err = e;
                            } else exports = context.execCb(id, factory, depExports, exports);
                            if (this.map.isDefine) {
                                cjsModule = this.module;
                                if (cjsModule && cjsModule.exports !== undefined && cjsModule.exports !== this.exports) exports = cjsModule.exports; else if (exports === undefined && this.usingExports) exports = this.exports;
                            }
                            if (err) {
                                err.requireMap = this.map;
                                err.requireModules = this.map.isDefine ? [ this.map.id ] : null;
                                err.requireType = this.map.isDefine ? "define" : "require";
                                return onError(this.error = err);
                            }
                        } else exports = factory;
                        this.exports = exports;
                        if (this.map.isDefine && !this.ignore) {
                            defined[id] = exports;
                            if (req.onResourceLoad) req.onResourceLoad(context, this.map, this.depMaps);
                        }
                        cleanRegistry(id);
                        this.defined = true;
                    }
                    this.defining = false;
                    if (this.defined && !this.defineEmitted) {
                        this.defineEmitted = true;
                        this.emit("defined", this.exports);
                        this.defineEmitComplete = true;
                    }
                }
            },
            callPlugin: function() {
                var map = this.map, id = map.id, pluginMap = makeModuleMap(map.prefix);
                this.depMaps.push(pluginMap);
                on(pluginMap, "defined", bind(this, function(plugin) {
                    var load, normalizedMap, normalizedMod, name = this.map.name, parentName = this.map.parentMap ? this.map.parentMap.name : null, localRequire = context.makeRequire(map.parentMap, {
                        enableBuildCallback: true
                    });
                    if (this.map.unnormalized) {
                        if (plugin.normalize) name = plugin.normalize(name, function(name) {
                            return normalize(name, parentName, true);
                        }) || "";
                        normalizedMap = makeModuleMap(map.prefix + "!" + name, this.map.parentMap);
                        on(normalizedMap, "defined", bind(this, function(value) {
                            this.init([], function() {
                                return value;
                            }, null, {
                                enabled: true,
                                ignore: true
                            });
                        }));
                        normalizedMod = getOwn(registry, normalizedMap.id);
                        if (normalizedMod) {
                            this.depMaps.push(normalizedMap);
                            if (this.events.error) normalizedMod.on("error", bind(this, function(err) {
                                this.emit("error", err);
                            }));
                            normalizedMod.enable();
                        }
                        return;
                    }
                    load = bind(this, function(value) {
                        this.init([], function() {
                            return value;
                        }, null, {
                            enabled: true
                        });
                    });
                    load.error = bind(this, function(err) {
                        this.inited = true;
                        this.error = err;
                        err.requireModules = [ id ];
                        eachProp(registry, function(mod) {
                            if (mod.map.id.indexOf(id + "_unnormalized") === 0) cleanRegistry(mod.map.id);
                        });
                        onError(err);
                    });
                    load.fromText = bind(this, function(text, textAlt) {
                        var moduleName = map.name, moduleMap = makeModuleMap(moduleName), hasInteractive = useInteractive;
                        if (textAlt) text = textAlt;
                        if (hasInteractive) useInteractive = false;
                        getModule(moduleMap);
                        if (hasProp(config.config, id)) config.config[moduleName] = config.config[id];
                        try {
                            req.exec(text);
                        } catch (e) {
                            return onError(makeError("fromtexteval", "fromText eval for " + id + " failed: " + e, e, [ id ]));
                        }
                        if (hasInteractive) useInteractive = true;
                        this.depMaps.push(moduleMap);
                        context.completeLoad(moduleName);
                        localRequire([ moduleName ], load);
                    });
                    plugin.load(map.name, localRequire, load, config);
                }));
                context.enable(pluginMap, this);
                this.pluginMaps[pluginMap.id] = pluginMap;
            },
            enable: function() {
                enabledRegistry[this.map.id] = this;
                this.enabled = true;
                this.enabling = true;
                each(this.depMaps, bind(this, function(depMap, i) {
                    var id, mod, handler;
                    if (typeof depMap === "string") {
                        depMap = makeModuleMap(depMap, this.map.isDefine ? this.map : this.map.parentMap, false, !this.skipMap);
                        this.depMaps[i] = depMap;
                        handler = getOwn(handlers, depMap.id);
                        if (handler) {
                            this.depExports[i] = handler(this);
                            return;
                        }
                        this.depCount += 1;
                        on(depMap, "defined", bind(this, function(depExports) {
                            this.defineDep(i, depExports);
                            this.check();
                        }));
                        if (this.errback) on(depMap, "error", bind(this, this.errback));
                    }
                    id = depMap.id;
                    mod = registry[id];
                    if (!hasProp(handlers, id) && mod && !mod.enabled) context.enable(depMap, this);
                }));
                eachProp(this.pluginMaps, bind(this, function(pluginMap) {
                    var mod = getOwn(registry, pluginMap.id);
                    if (mod && !mod.enabled) context.enable(pluginMap, this);
                }));
                this.enabling = false;
                this.check();
            },
            on: function(name, cb) {
                var cbs = this.events[name];
                if (!cbs) cbs = this.events[name] = [];
                cbs.push(cb);
            },
            emit: function(name, evt) {
                each(this.events[name], function(cb) {
                    cb(evt);
                });
                if (name === "error") delete this.events[name];
            }
        };
        function callGetModule(args) {
            if (!hasProp(defined, args[0])) getModule(makeModuleMap(args[0], null, true)).init(args[1], args[2]);
        }
        function removeListener(node, func, name, ieName) {
            if (node.detachEvent && !isOpera) {
                if (ieName) node.detachEvent(ieName, func);
            } else node.removeEventListener(name, func, false);
        }
        function getScriptData(evt) {
            var node = evt.currentTarget || evt.srcElement;
            removeListener(node, context.onScriptLoad, "load", "onreadystatechange");
            removeListener(node, context.onScriptError, "error");
            return {
                node: node,
                id: node && node.getAttribute("data-requiremodule")
            };
        }
        function intakeDefines() {
            var args;
            takeGlobalQueue();
            while (defQueue.length) {
                args = defQueue.shift();
                if (args[0] === null) return onError(makeError("mismatch", "Mismatched anonymous define() module: " + args[args.length - 1])); else callGetModule(args);
            }
        }
        context = {
            config: config,
            contextName: contextName,
            registry: registry,
            defined: defined,
            urlFetched: urlFetched,
            defQueue: defQueue,
            Module: Module,
            makeModuleMap: makeModuleMap,
            nextTick: req.nextTick,
            onError: onError,
            configure: function(cfg) {
                if (cfg.baseUrl) if (cfg.baseUrl.charAt(cfg.baseUrl.length - 1) !== "/") cfg.baseUrl += "/";
                var pkgs = config.pkgs, shim = config.shim, objs = {
                    paths: true,
                    config: true,
                    map: true
                };
                eachProp(cfg, function(value, prop) {
                    if (objs[prop]) if (prop === "map") {
                        if (!config.map) config.map = {};
                        mixin(config[prop], value, true, true);
                    } else mixin(config[prop], value, true); else config[prop] = value;
                });
                if (cfg.shim) {
                    eachProp(cfg.shim, function(value, id) {
                        if (isArray(value)) value = {
                            deps: value
                        };
                        if ((value.exports || value.init) && !value.exportsFn) value.exportsFn = context.makeShimExports(value);
                        shim[id] = value;
                    });
                    config.shim = shim;
                }
                if (cfg.packages) {
                    each(cfg.packages, function(pkgObj) {
                        var location;
                        pkgObj = typeof pkgObj === "string" ? {
                            name: pkgObj
                        } : pkgObj;
                        location = pkgObj.location;
                        pkgs[pkgObj.name] = {
                            name: pkgObj.name,
                            location: location || pkgObj.name,
                            main: (pkgObj.main || "main").replace(currDirRegExp, "").replace(jsSuffixRegExp, "")
                        };
                    });
                    config.pkgs = pkgs;
                }
                eachProp(registry, function(mod, id) {
                    if (!mod.inited && !mod.map.unnormalized) mod.map = makeModuleMap(id);
                });
                if (cfg.deps || cfg.callback) context.require(cfg.deps || [], cfg.callback);
            },
            makeShimExports: function(value) {
                function fn() {
                    var ret;
                    if (value.init) ret = value.init.apply(global, arguments);
                    return ret || value.exports && getGlobal(value.exports);
                }
                return fn;
            },
            makeRequire: function(relMap, options) {
                options = options || {};
                function localRequire(deps, callback, errback) {
                    var id, map, requireMod;
                    if (options.enableBuildCallback && callback && isFunction(callback)) callback.__requireJsBuild = true;
                    if (typeof deps === "string") {
                        if (isFunction(callback)) return onError(makeError("requireargs", "Invalid require call"), errback);
                        if (relMap && hasProp(handlers, deps)) return handlers[deps](registry[relMap.id]);
                        if (req.get) return req.get(context, deps, relMap, localRequire);
                        map = makeModuleMap(deps, relMap, false, true);
                        id = map.id;
                        if (!hasProp(defined, id)) return onError(makeError("notloaded", 'Module name "' + id + '" has not been loaded yet for context: ' + contextName + (relMap ? "" : ". Use require([])")));
                        return defined[id];
                    }
                    intakeDefines();
                    context.nextTick(function() {
                        intakeDefines();
                        requireMod = getModule(makeModuleMap(null, relMap));
                        requireMod.skipMap = options.skipMap;
                        requireMod.init(deps, callback, errback, {
                            enabled: true
                        });
                        checkLoaded();
                    });
                    return localRequire;
                }
                mixin(localRequire, {
                    isBrowser: isBrowser,
                    toUrl: function(moduleNamePlusExt) {
                        var ext, index = moduleNamePlusExt.lastIndexOf("."), segment = moduleNamePlusExt.split("/")[0], isRelative = segment === "." || segment === "..";
                        if (index !== -1 && (!isRelative || index > 1)) {
                            ext = moduleNamePlusExt.substring(index, moduleNamePlusExt.length);
                            moduleNamePlusExt = moduleNamePlusExt.substring(0, index);
                        }
                        return context.nameToUrl(normalize(moduleNamePlusExt, relMap && relMap.id, true), ext, true);
                    },
                    defined: function(id) {
                        return hasProp(defined, makeModuleMap(id, relMap, false, true).id);
                    },
                    specified: function(id) {
                        id = makeModuleMap(id, relMap, false, true).id;
                        return hasProp(defined, id) || hasProp(registry, id);
                    }
                });
                if (!relMap) localRequire.undef = function(id) {
                    takeGlobalQueue();
                    var map = makeModuleMap(id, relMap, true), mod = getOwn(registry, id);
                    delete defined[id];
                    delete urlFetched[map.url];
                    delete undefEvents[id];
                    if (mod) {
                        if (mod.events.defined) undefEvents[id] = mod.events;
                        cleanRegistry(id);
                    }
                };
                return localRequire;
            },
            enable: function(depMap) {
                var mod = getOwn(registry, depMap.id);
                if (mod) getModule(depMap).enable();
            },
            completeLoad: function(moduleName) {
                var found, args, mod, shim = getOwn(config.shim, moduleName) || {}, shExports = shim.exports;
                takeGlobalQueue();
                while (defQueue.length) {
                    args = defQueue.shift();
                    if (args[0] === null) {
                        args[0] = moduleName;
                        if (found) break;
                        found = true;
                    } else if (args[0] === moduleName) found = true;
                    callGetModule(args);
                }
                mod = getOwn(registry, moduleName);
                if (!found && !hasProp(defined, moduleName) && mod && !mod.inited) if (config.enforceDefine && (!shExports || !getGlobal(shExports))) if (hasPathFallback(moduleName)) return; else return onError(makeError("nodefine", "No define call for " + moduleName, null, [ moduleName ])); else callGetModule([ moduleName, shim.deps || [], shim.exportsFn ]);
                checkLoaded();
            },
            nameToUrl: function(moduleName, ext, skipExt) {
                var paths, pkgs, pkg, pkgPath, syms, i, parentModule, url, parentPath;
                if (req.jsExtRegExp.test(moduleName)) url = moduleName + (ext || ""); else {
                    paths = config.paths;
                    pkgs = config.pkgs;
                    syms = moduleName.split("/");
                    for (i = syms.length; i > 0; i -= 1) {
                        parentModule = syms.slice(0, i).join("/");
                        pkg = getOwn(pkgs, parentModule);
                        parentPath = getOwn(paths, parentModule);
                        if (parentPath) {
                            if (isArray(parentPath)) parentPath = parentPath[0];
                            syms.splice(0, i, parentPath);
                            break;
                        } else if (pkg) {
                            if (moduleName === pkg.name) pkgPath = pkg.location + "/" + pkg.main; else pkgPath = pkg.location;
                            syms.splice(0, i, pkgPath);
                            break;
                        }
                    }
                    url = syms.join("/");
                    url += ext || (/\?/.test(url) || skipExt ? "" : ".js");
                    url = (url.charAt(0) === "/" || url.match(/^[\w\+\.\-]+:/) ? "" : config.baseUrl) + url;
                }
                return config.urlArgs ? url + ((url.indexOf("?") === -1 ? "?" : "&") + config.urlArgs) : url;
            },
            load: function(id, url) {
                req.load(context, id, url);
            },
            execCb: function(name, callback, args, exports) {
                return callback.apply(exports, args);
            },
            onScriptLoad: function(evt) {
                if (evt.type === "load" || readyRegExp.test((evt.currentTarget || evt.srcElement).readyState)) {
                    interactiveScript = null;
                    var data = getScriptData(evt);
                    context.completeLoad(data.id);
                }
            },
            onScriptError: function(evt) {
                var data = getScriptData(evt);
                if (!hasPathFallback(data.id)) return onError(makeError("scripterror", "Script error for: " + data.id, evt, [ data.id ]));
            }
        };
        context.require = context.makeRequire();
        return context;
    }
    req = requirejs = function(deps, callback, errback, optional) {
        var context, config, contextName = defContextName;
        if (!isArray(deps) && typeof deps !== "string") {
            config = deps;
            if (isArray(callback)) {
                deps = callback;
                callback = errback;
                errback = optional;
            } else deps = [];
        }
        if (config && config.context) contextName = config.context;
        context = getOwn(contexts, contextName);
        if (!context) context = contexts[contextName] = req.s.newContext(contextName);
        if (config) context.configure(config);
        return context.require(deps, callback, errback);
    };
    req.config = function(config) {
        return req(config);
    };
    req.nextTick = typeof setTimeout !== "undefined" ? function(fn) {
        setTimeout(fn, 4);
    } : function(fn) {
        fn();
    };
    if (!require) require = req;
    req.version = version;
    req.jsExtRegExp = /^\/|:|\?|\.js$/;
    req.isBrowser = isBrowser;
    s = req.s = {
        contexts: contexts,
        newContext: newContext
    };
    req({});
    each([ "toUrl", "undef", "defined", "specified" ], function(prop) {
        req[prop] = function() {
            var ctx = contexts[defContextName];
            return ctx.require[prop].apply(ctx, arguments);
        };
    });
    if (isBrowser) {
        head = s.head = document.getElementsByTagName("head")[0];
        baseElement = document.getElementsByTagName("base")[0];
        if (baseElement) head = s.head = baseElement.parentNode;
    }
    req.onError = defaultOnError;
    req.createNode = function(config, moduleName, url) {
        var node = config.xhtml ? document.createElementNS("http://www.w3.org/1999/xhtml", "html:script") : document.createElement("script");
        node.type = config.scriptType || "text/javascript";
        node.charset = "utf-8";
        node.async = true;
        return node;
    };
    req.load = function(context, moduleName, url) {
        var config = context && context.config || {}, node;
        if (isBrowser) {
            node = req.createNode(config, moduleName, url);
            node.setAttribute("data-requirecontext", context.contextName);
            node.setAttribute("data-requiremodule", moduleName);
            if (node.attachEvent && !(node.attachEvent.toString && node.attachEvent.toString().indexOf("[native code") < 0) && !isOpera) {
                useInteractive = true;
                node.attachEvent("onreadystatechange", context.onScriptLoad);
            } else {
                node.addEventListener("load", context.onScriptLoad, false);
                node.addEventListener("error", context.onScriptError, false);
            }
            node.src = url;
            currentlyAddingScript = node;
            if (baseElement) head.insertBefore(node, baseElement); else head.appendChild(node);
            currentlyAddingScript = null;
            return node;
        } else if (isWebWorker) try {
            importScripts(url);
            context.completeLoad(moduleName);
        } catch (e) {
            context.onError(makeError("importscripts", "importScripts failed for " + moduleName + " at " + url, e, [ moduleName ]));
        }
    };
    function getInteractiveScript() {
        if (interactiveScript && interactiveScript.readyState === "interactive") return interactiveScript;
        eachReverse(scripts(), function(script) {
            if (script.readyState === "interactive") return interactiveScript = script;
        });
        return interactiveScript;
    }
    if (isBrowser) eachReverse(scripts(), function(script) {
        if (!head) head = script.parentNode;
        dataMain = script.getAttribute("data-main");
        if (dataMain) {
            mainScript = dataMain;
            if (!cfg.baseUrl) {
                src = mainScript.split("/");
                mainScript = src.pop();
                subPath = src.length ? src.join("/") + "/" : "./";
                cfg.baseUrl = subPath;
            }
            mainScript = mainScript.replace(jsSuffixRegExp, "");
            if (req.jsExtRegExp.test(mainScript)) mainScript = dataMain;
            cfg.deps = cfg.deps ? cfg.deps.concat(mainScript) : [ mainScript ];
            return true;
        }
    });
    define = function(name, deps, callback) {
        var node, context;
        if (typeof name !== "string") {
            callback = deps;
            deps = name;
            name = null;
        }
        if (!isArray(deps)) {
            callback = deps;
            deps = null;
        }
        if (!deps && isFunction(callback)) {
            deps = [];
            if (callback.length) {
                callback.toString().replace(commentRegExp, "").replace(cjsRequireRegExp, function(match, dep) {
                    deps.push(dep);
                });
                deps = (callback.length === 1 ? [ "require" ] : [ "require", "exports", "module" ]).concat(deps);
            }
        }
        if (useInteractive) {
            node = currentlyAddingScript || getInteractiveScript();
            if (node) {
                if (!name) name = node.getAttribute("data-requiremodule");
                context = contexts[node.getAttribute("data-requirecontext")];
            }
        }
        (context ? context.defQueue : globalDefQueue).push([ name, deps, callback ]);
    };
    define.amd = {
        jQuery: true
    };
    req.exec = function(text) {
        return eval(text);
    };
    req(cfg);
})(this);

define("requireLib", function() {});

define("jquery", [], function() {
    return jQuery;
});

define("modules/definitions/standardmodule", [ "jquery" ], function($) {
    var _instanceID = 0;
    var _nextInstanceID = function() {
        return ++_instanceID;
    };
    var defaults = [];
    function _thizOBJ_(o) {
        this._instanceID = _nextInstanceID();
        var _parent = this;
        defaults[this._instanceID] = {
            type: "Standard Module Definition",
            author: "Jonathan Robles",
            notifyscope: "global",
            target: undefined,
            file: undefined,
            usenocache: true,
            data: undefined,
            callback: undefined,
            interval: undefined,
            init: function() {
                _notify.broadcast("Initialize", [ {
                    senderID: _parent._instanceID,
                    sendertype: this.type,
                    notifyscope: this.notifyscope,
                    data: {
                        author: this.author
                    }
                } ]);
            },
            parent: this
        };
        defaults[this._instanceID] = $.extend(defaults[this._instanceID], o);
        defaults[this._instanceID].init();
        return this;
    }
    _thizOBJ_.prototype = {
        _init: function() {
            this._var().init();
        },
        _showdata: function() {
            return JSON.stringify(defaults[this._instanceID]);
        },
        _id: function() {
            return this._instanceID;
        },
        _var: function(o) {
            if (o != undefined) defaults[this._instanceID] = $.extend(defaults[this._instanceID], o);
            return defaults[this._instanceID];
        },
        _nocache: function(string) {
            if (typeof string === "string") if (this._var().usenocache) {
                var addOn = "?";
                if (string.indexOf("?") != -1) addOn = "&";
                return string + addOn + "nocache=" + Math.floor(Math.random() * 9999);
            } else return string; else {
                this.notify("Alert", "_nocache needs a string!");
                return;
            }
        },
        notify: function(type, data) {
            _notify.broadcast(type, [ {
                senderID: this._id(),
                sendertype: this._var().type,
                notifyscope: this._var().notifyscope,
                data: data
            } ]);
        },
        parent: this
    };
    return _thizOBJ_;
});

(function() {
    function F(q) {
        return function() {
            return q;
        };
    }
    (function(q) {
        var w = this || (0, eval)("this"), s = w.document, H = w.navigator, t = w.jQuery, y = w.JSON;
        (function(q) {
            "function" === typeof require && "object" === typeof exports && "object" === typeof module ? q(module.exports || exports) : "function" === typeof define && define.amd ? define("knockout", [ "exports" ], q) : q(w.ko = {});
        })(function(C) {
            function G(b, c, d, f) {
                a.d[b] = {
                    init: function(b) {
                        a.a.f.set(b, I, {});
                        return {
                            controlsDescendantBindings: !0
                        };
                    },
                    update: function(b, e, m, h, k) {
                        m = a.a.f.get(b, I);
                        e = a.a.c(e());
                        h = !d !== !e;
                        var l = !m.fb;
                        if (l || c || h !== m.vb) l && (m.fb = a.a.Oa(a.e.childNodes(b), !0)), h ? (l || a.e.P(b, a.a.Oa(m.fb)), 
                        a.Ja(f ? f(k, e) : k, b)) : a.e.ba(b), m.vb = h;
                    }
                };
                a.g.S[b] = !1;
                a.e.L[b] = !0;
            }
            function J(b, c, d) {
                d && c !== a.h.n(b) && a.h.W(b, c);
                c !== a.h.n(b) && a.q.I(a.a.Ga, null, [ b, "change" ]);
            }
            var a = "undefined" !== typeof C ? C : {};
            a.b = function(b, c) {
                for (var d = b.split("."), f = a, g = 0; g < d.length - 1; g++) f = f[d[g]];
                f[d[d.length - 1]] = c;
            };
            a.r = function(a, c, d) {
                a[c] = d;
            };
            a.version = "2.3.0";
            a.b("version", a.version);
            a.a = function() {
                function b(a, b) {
                    for (var e in a) a.hasOwnProperty(e) && b(e, a[e]);
                }
                function c(b, e) {
                    if ("input" !== a.a.u(b) || !b.type || "click" != e.toLowerCase()) return !1;
                    var k = b.type;
                    return "checkbox" == k || "radio" == k;
                }
                var d = {}, f = {};
                d[H && /Firefox\/2/i.test(H.userAgent) ? "KeyboardEvent" : "UIEvents"] = [ "keyup", "keydown", "keypress" ];
                d.MouseEvents = "click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave".split(" ");
                b(d, function(a, b) {
                    if (b.length) for (var e = 0, c = b.length; e < c; e++) f[b[e]] = a;
                });
                var g = {
                    propertychange: !0
                }, e = s && function() {
                    for (var a = 3, b = s.createElement("div"), e = b.getElementsByTagName("i"); b.innerHTML = "<!--[if gt IE " + ++a + "]><i></i><![endif]-->", 
                    e[0]; ) ;
                    return 4 < a ? a : q;
                }();
                return {
                    Ta: [ "authenticity_token", /^__RequestVerificationToken(_.*)?$/ ],
                    p: function(a, b) {
                        for (var e = 0, c = a.length; e < c; e++) b(a[e]);
                    },
                    k: function(a, b) {
                        if ("function" == typeof Array.prototype.indexOf) return Array.prototype.indexOf.call(a, b);
                        for (var e = 0, c = a.length; e < c; e++) if (a[e] === b) return e;
                        return -1;
                    },
                    La: function(a, b, e) {
                        for (var c = 0, d = a.length; c < d; c++) if (b.call(e, a[c])) return a[c];
                        return null;
                    },
                    ka: function(b, e) {
                        var c = a.a.k(b, e);
                        0 <= c && b.splice(c, 1);
                    },
                    Ma: function(b) {
                        b = b || [];
                        for (var e = [], c = 0, d = b.length; c < d; c++) 0 > a.a.k(e, b[c]) && e.push(b[c]);
                        return e;
                    },
                    Z: function(a, b) {
                        a = a || [];
                        for (var e = [], c = 0, d = a.length; c < d; c++) e.push(b(a[c]));
                        return e;
                    },
                    Y: function(a, b) {
                        a = a || [];
                        for (var e = [], c = 0, d = a.length; c < d; c++) b(a[c]) && e.push(a[c]);
                        return e;
                    },
                    R: function(a, b) {
                        if (b instanceof Array) a.push.apply(a, b); else for (var e = 0, c = b.length; e < c; e++) a.push(b[e]);
                        return a;
                    },
                    ja: function(b, e, c) {
                        var d = b.indexOf ? b.indexOf(e) : a.a.k(b, e);
                        0 > d ? c && b.push(e) : c || b.splice(d, 1);
                    },
                    extend: function(a, b) {
                        if (b) for (var e in b) b.hasOwnProperty(e) && (a[e] = b[e]);
                        return a;
                    },
                    w: b,
                    oa: function(b) {
                        for (;b.firstChild; ) a.removeNode(b.firstChild);
                    },
                    Mb: function(b) {
                        b = a.a.N(b);
                        for (var e = s.createElement("div"), c = 0, d = b.length; c < d; c++) e.appendChild(a.H(b[c]));
                        return e;
                    },
                    Oa: function(b, e) {
                        for (var c = 0, d = b.length, g = []; c < d; c++) {
                            var f = b[c].cloneNode(!0);
                            g.push(e ? a.H(f) : f);
                        }
                        return g;
                    },
                    P: function(b, e) {
                        a.a.oa(b);
                        if (e) for (var c = 0, d = e.length; c < d; c++) b.appendChild(e[c]);
                    },
                    eb: function(b, e) {
                        var c = b.nodeType ? [ b ] : b;
                        if (0 < c.length) {
                            for (var d = c[0], g = d.parentNode, f = 0, r = e.length; f < r; f++) g.insertBefore(e[f], d);
                            f = 0;
                            for (r = c.length; f < r; f++) a.removeNode(c[f]);
                        }
                    },
                    hb: function(a, b) {
                        7 > e ? a.setAttribute("selected", b) : a.selected = b;
                    },
                    F: function(a) {
                        return null === a || a === q ? "" : a.trim ? a.trim() : a.toString().replace(/^[\s\xa0]+|[\s\xa0]+$/g, "");
                    },
                    Wb: function(b, e) {
                        for (var c = [], d = (b || "").split(e), g = 0, f = d.length; g < f; g++) {
                            var r = a.a.F(d[g]);
                            "" !== r && c.push(r);
                        }
                        return c;
                    },
                    Tb: function(a, b) {
                        a = a || "";
                        return b.length > a.length ? !1 : a.substring(0, b.length) === b;
                    },
                    yb: function(a, b) {
                        if (b.compareDocumentPosition) return 16 == (b.compareDocumentPosition(a) & 16);
                        for (;null != a; ) {
                            if (a == b) return !0;
                            a = a.parentNode;
                        }
                        return !1;
                    },
                    aa: function(b) {
                        return a.a.yb(b, b.ownerDocument);
                    },
                    pb: function(b) {
                        return !!a.a.La(b, a.a.aa);
                    },
                    u: function(a) {
                        return a && a.tagName && a.tagName.toLowerCase();
                    },
                    o: function(b, d, k) {
                        var f = e && g[d];
                        if (f || "undefined" == typeof t) if (f || "function" != typeof b.addEventListener) if ("undefined" != typeof b.attachEvent) {
                            var n = function(a) {
                                k.call(b, a);
                            }, p = "on" + d;
                            b.attachEvent(p, n);
                            a.a.C.ia(b, function() {
                                b.detachEvent(p, n);
                            });
                        } else throw Error("Browser doesn't support addEventListener or attachEvent"); else b.addEventListener(d, k, !1); else {
                            if (c(b, d)) {
                                var r = k;
                                k = function(a, b) {
                                    var e = this.checked;
                                    b && (this.checked = !0 !== b.sb);
                                    r.call(this, a);
                                    this.checked = e;
                                };
                            }
                            t(b).bind(d, k);
                        }
                    },
                    Ga: function(a, b) {
                        if (!a || !a.nodeType) throw Error("element must be a DOM node when calling triggerEvent");
                        if ("undefined" != typeof t) {
                            var e = [];
                            c(a, b) && e.push({
                                sb: a.checked
                            });
                            t(a).trigger(b, e);
                        } else if ("function" == typeof s.createEvent) if ("function" == typeof a.dispatchEvent) e = s.createEvent(f[b] || "HTMLEvents"), 
                        e.initEvent(b, !0, !0, w, 0, 0, 0, 0, 0, !1, !1, !1, !1, 0, a), a.dispatchEvent(e); else throw Error("The supplied element doesn't support dispatchEvent"); else if ("undefined" != typeof a.fireEvent) c(a, b) && (a.checked = !0 !== a.checked), 
                        a.fireEvent("on" + b); else throw Error("Browser doesn't support triggering events");
                    },
                    c: function(b) {
                        return a.T(b) ? b() : b;
                    },
                    ya: function(b) {
                        return a.T(b) ? b.t() : b;
                    },
                    ga: function(b, e, c) {
                        if (e) {
                            var d = /\S+/g, g = b.className.match(d) || [];
                            a.a.p(e.match(d), function(b) {
                                a.a.ja(g, b, c);
                            });
                            b.className = g.join(" ");
                        }
                    },
                    ib: function(b, e) {
                        var c = a.a.c(e);
                        if (null === c || c === q) c = "";
                        var d = a.e.firstChild(b);
                        !d || 3 != d.nodeType || a.e.nextSibling(d) ? a.e.P(b, [ s.createTextNode(c) ]) : d.data = c;
                        a.a.Bb(b);
                    },
                    gb: function(a, b) {
                        a.name = b;
                        if (7 >= e) try {
                            a.mergeAttributes(s.createElement("<input name='" + a.name + "'/>"), !1);
                        } catch (c) {}
                    },
                    Bb: function(a) {
                        9 <= e && (a = 1 == a.nodeType ? a : a.parentNode, a.style && (a.style.zoom = a.style.zoom));
                    },
                    zb: function(a) {
                        if (e) {
                            var b = a.style.width;
                            a.style.width = 0;
                            a.style.width = b;
                        }
                    },
                    Qb: function(b, e) {
                        b = a.a.c(b);
                        e = a.a.c(e);
                        for (var c = [], d = b; d <= e; d++) c.push(d);
                        return c;
                    },
                    N: function(a) {
                        for (var b = [], e = 0, c = a.length; e < c; e++) b.push(a[e]);
                        return b;
                    },
                    Ub: 6 === e,
                    Vb: 7 === e,
                    ca: e,
                    Ua: function(b, e) {
                        for (var c = a.a.N(b.getElementsByTagName("input")).concat(a.a.N(b.getElementsByTagName("textarea"))), d = "string" == typeof e ? function(a) {
                            return a.name === e;
                        } : function(a) {
                            return e.test(a.name);
                        }, g = [], f = c.length - 1; 0 <= f; f--) d(c[f]) && g.push(c[f]);
                        return g;
                    },
                    Nb: function(b) {
                        return "string" == typeof b && (b = a.a.F(b)) ? y && y.parse ? y.parse(b) : new Function("return " + b)() : null;
                    },
                    Ca: function(b, e, c) {
                        if (!y || !y.stringify) throw Error("Cannot find JSON.stringify(). Some browsers (e.g., IE < 8) don't support it natively, but you can overcome this by adding a script reference to json2.js, downloadable from http://www.json.org/json2.js");
                        return y.stringify(a.a.c(b), e, c);
                    },
                    Ob: function(e, c, d) {
                        d = d || {};
                        var g = d.params || {}, f = d.includeFields || this.Ta, p = e;
                        if ("object" == typeof e && "form" === a.a.u(e)) for (var p = e.action, r = f.length - 1; 0 <= r; r--) for (var z = a.a.Ua(e, f[r]), D = z.length - 1; 0 <= D; D--) g[z[D].name] = z[D].value;
                        c = a.a.c(c);
                        var q = s.createElement("form");
                        q.style.display = "none";
                        q.action = p;
                        q.method = "post";
                        for (var v in c) e = s.createElement("input"), e.name = v, e.value = a.a.Ca(a.a.c(c[v])), 
                        q.appendChild(e);
                        b(g, function(a, b) {
                            var e = s.createElement("input");
                            e.name = a;
                            e.value = b;
                            q.appendChild(e);
                        });
                        s.body.appendChild(q);
                        d.submitter ? d.submitter(q) : q.submit();
                        setTimeout(function() {
                            q.parentNode.removeChild(q);
                        }, 0);
                    }
                };
            }();
            a.b("utils", a.a);
            a.b("utils.arrayForEach", a.a.p);
            a.b("utils.arrayFirst", a.a.La);
            a.b("utils.arrayFilter", a.a.Y);
            a.b("utils.arrayGetDistinctValues", a.a.Ma);
            a.b("utils.arrayIndexOf", a.a.k);
            a.b("utils.arrayMap", a.a.Z);
            a.b("utils.arrayPushAll", a.a.R);
            a.b("utils.arrayRemoveItem", a.a.ka);
            a.b("utils.extend", a.a.extend);
            a.b("utils.fieldsIncludedWithJsonPost", a.a.Ta);
            a.b("utils.getFormFields", a.a.Ua);
            a.b("utils.peekObservable", a.a.ya);
            a.b("utils.postJson", a.a.Ob);
            a.b("utils.parseJson", a.a.Nb);
            a.b("utils.registerEventHandler", a.a.o);
            a.b("utils.stringifyJson", a.a.Ca);
            a.b("utils.range", a.a.Qb);
            a.b("utils.toggleDomNodeCssClass", a.a.ga);
            a.b("utils.triggerEvent", a.a.Ga);
            a.b("utils.unwrapObservable", a.a.c);
            a.b("utils.objectForEach", a.a.w);
            a.b("utils.addOrRemoveItem", a.a.ja);
            a.b("unwrap", a.a.c);
            Function.prototype.bind || (Function.prototype.bind = function(a) {
                var c = this, d = Array.prototype.slice.call(arguments);
                a = d.shift();
                return function() {
                    return c.apply(a, d.concat(Array.prototype.slice.call(arguments)));
                };
            });
            a.a.f = new function() {
                var b = 0, c = "__ko__" + new Date().getTime(), d = {};
                return {
                    get: function(b, c) {
                        var e = a.a.f.pa(b, !1);
                        return e === q ? q : e[c];
                    },
                    set: function(b, c, e) {
                        if (e !== q || a.a.f.pa(b, !1) !== q) a.a.f.pa(b, !0)[c] = e;
                    },
                    pa: function(a, g) {
                        var e = a[c];
                        if (!e || "null" === e || !d[e]) {
                            if (!g) return q;
                            e = a[c] = "ko" + b++;
                            d[e] = {};
                        }
                        return d[e];
                    },
                    clear: function(a) {
                        var b = a[c];
                        return b ? (delete d[b], a[c] = null, !0) : !1;
                    }
                };
            }();
            a.b("utils.domData", a.a.f);
            a.b("utils.domData.clear", a.a.f.clear);
            a.a.C = new function() {
                function b(b, c) {
                    var g = a.a.f.get(b, d);
                    g === q && c && (g = [], a.a.f.set(b, d, g));
                    return g;
                }
                function c(e) {
                    var d = b(e, !1);
                    if (d) for (var d = d.slice(0), f = 0; f < d.length; f++) d[f](e);
                    a.a.f.clear(e);
                    "function" == typeof t && "function" == typeof t.cleanData && t.cleanData([ e ]);
                    if (g[e.nodeType]) for (d = e.firstChild; e = d; ) d = e.nextSibling, 8 === e.nodeType && c(e);
                }
                var d = "__ko_domNodeDisposal__" + new Date().getTime(), f = {
                    1: !0,
                    8: !0,
                    9: !0
                }, g = {
                    1: !0,
                    9: !0
                };
                return {
                    ia: function(a, c) {
                        if ("function" != typeof c) throw Error("Callback must be a function");
                        b(a, !0).push(c);
                    },
                    cb: function(e, c) {
                        var g = b(e, !1);
                        g && (a.a.ka(g, c), 0 == g.length && a.a.f.set(e, d, q));
                    },
                    H: function(b) {
                        if (f[b.nodeType] && (c(b), g[b.nodeType])) {
                            var d = [];
                            a.a.R(d, b.getElementsByTagName("*"));
                            for (var h = 0, k = d.length; h < k; h++) c(d[h]);
                        }
                        return b;
                    },
                    removeNode: function(b) {
                        a.H(b);
                        b.parentNode && b.parentNode.removeChild(b);
                    }
                };
            }();
            a.H = a.a.C.H;
            a.removeNode = a.a.C.removeNode;
            a.b("cleanNode", a.H);
            a.b("removeNode", a.removeNode);
            a.b("utils.domNodeDisposal", a.a.C);
            a.b("utils.domNodeDisposal.addDisposeCallback", a.a.C.ia);
            a.b("utils.domNodeDisposal.removeDisposeCallback", a.a.C.cb);
            (function() {
                a.a.xa = function(b) {
                    var c;
                    if ("undefined" != typeof t) if (t.parseHTML) c = t.parseHTML(b) || []; else {
                        if ((c = t.clean([ b ])) && c[0]) {
                            for (b = c[0]; b.parentNode && 11 !== b.parentNode.nodeType; ) b = b.parentNode;
                            b.parentNode && b.parentNode.removeChild(b);
                        }
                    } else {
                        var d = a.a.F(b).toLowerCase();
                        c = s.createElement("div");
                        d = d.match(/^<(thead|tbody|tfoot)/) && [ 1, "<table>", "</table>" ] || !d.indexOf("<tr") && [ 2, "<table><tbody>", "</tbody></table>" ] || (!d.indexOf("<td") || !d.indexOf("<th")) && [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ] || [ 0, "", "" ];
                        b = "ignored<div>" + d[1] + b + d[2] + "</div>";
                        for ("function" == typeof w.innerShiv ? c.appendChild(w.innerShiv(b)) : c.innerHTML = b; d[0]--; ) c = c.lastChild;
                        c = a.a.N(c.lastChild.childNodes);
                    }
                    return c;
                };
                a.a.fa = function(b, c) {
                    a.a.oa(b);
                    c = a.a.c(c);
                    if (null !== c && c !== q) if ("string" != typeof c && (c = c.toString()), "undefined" != typeof t) t(b).html(c); else for (var d = a.a.xa(c), f = 0; f < d.length; f++) b.appendChild(d[f]);
                };
            })();
            a.b("utils.parseHtmlFragment", a.a.xa);
            a.b("utils.setHtml", a.a.fa);
            a.s = function() {
                function b(c, f) {
                    if (c) if (8 == c.nodeType) {
                        var g = a.s.$a(c.nodeValue);
                        null != g && f.push({
                            xb: c,
                            Kb: g
                        });
                    } else if (1 == c.nodeType) for (var g = 0, e = c.childNodes, m = e.length; g < m; g++) b(e[g], f);
                }
                var c = {};
                return {
                    va: function(a) {
                        if ("function" != typeof a) throw Error("You can only pass a function to ko.memoization.memoize()");
                        var b = (4294967296 * (1 + Math.random()) | 0).toString(16).substring(1) + (4294967296 * (1 + Math.random()) | 0).toString(16).substring(1);
                        c[b] = a;
                        return "<!--[ko_memo:" + b + "]-->";
                    },
                    mb: function(a, b) {
                        var g = c[a];
                        if (g === q) throw Error("Couldn't find any memo with ID " + a + ". Perhaps it's already been unmemoized.");
                        try {
                            return g.apply(null, b || []), !0;
                        } finally {
                            delete c[a];
                        }
                    },
                    nb: function(c, f) {
                        var g = [];
                        b(c, g);
                        for (var e = 0, m = g.length; e < m; e++) {
                            var h = g[e].xb, k = [ h ];
                            f && a.a.R(k, f);
                            a.s.mb(g[e].Kb, k);
                            h.nodeValue = "";
                            h.parentNode && h.parentNode.removeChild(h);
                        }
                    },
                    $a: function(a) {
                        return (a = a.match(/^\[ko_memo\:(.*?)\]$/)) ? a[1] : null;
                    }
                };
            }();
            a.b("memoization", a.s);
            a.b("memoization.memoize", a.s.va);
            a.b("memoization.unmemoize", a.s.mb);
            a.b("memoization.parseMemoText", a.s.$a);
            a.b("memoization.unmemoizeDomNodeAndDescendants", a.s.nb);
            a.Sa = {
                throttle: function(b, c) {
                    b.throttleEvaluation = c;
                    var d = null;
                    return a.j({
                        read: b,
                        write: function(a) {
                            clearTimeout(d);
                            d = setTimeout(function() {
                                b(a);
                            }, c);
                        }
                    });
                },
                notify: function(b, c) {
                    b.equalityComparer = "always" == c ? F(!1) : a.m.fn.equalityComparer;
                    return b;
                }
            };
            a.b("extenders", a.Sa);
            a.kb = function(b, c, d) {
                this.target = b;
                this.la = c;
                this.wb = d;
                a.r(this, "dispose", this.B);
            };
            a.kb.prototype.B = function() {
                this.Hb = !0;
                this.wb();
            };
            a.V = function() {
                this.G = {};
                a.a.extend(this, a.V.fn);
                a.r(this, "subscribe", this.Da);
                a.r(this, "extend", this.extend);
                a.r(this, "getSubscriptionsCount", this.Db);
            };
            a.V.fn = {
                Da: function(b, c, d) {
                    d = d || "change";
                    var f = new a.kb(this, c ? b.bind(c) : b, function() {
                        a.a.ka(this.G[d], f);
                    }.bind(this));
                    this.G[d] || (this.G[d] = []);
                    this.G[d].push(f);
                    return f;
                },
                notifySubscribers: function(b, c) {
                    c = c || "change";
                    this.G[c] && a.q.I(function() {
                        a.a.p(this.G[c].slice(0), function(a) {
                            a && !0 !== a.Hb && a.la(b);
                        });
                    }, this);
                },
                Db: function() {
                    var b = 0;
                    a.a.w(this.G, function(a, d) {
                        b += d.length;
                    });
                    return b;
                },
                extend: function(b) {
                    var c = this;
                    b && a.a.w(b, function(b, f) {
                        var g = a.Sa[b];
                        "function" == typeof g && (c = g(c, f));
                    });
                    return c;
                }
            };
            a.Wa = function(a) {
                return null != a && "function" == typeof a.Da && "function" == typeof a.notifySubscribers;
            };
            a.b("subscribable", a.V);
            a.b("isSubscribable", a.Wa);
            a.q = function() {
                var b = [];
                return {
                    rb: function(a) {
                        b.push({
                            la: a,
                            Ra: []
                        });
                    },
                    end: function() {
                        b.pop();
                    },
                    bb: function(c) {
                        if (!a.Wa(c)) throw Error("Only subscribable things can act as dependencies");
                        if (0 < b.length) {
                            var d = b[b.length - 1];
                            !d || 0 <= a.a.k(d.Ra, c) || (d.Ra.push(c), d.la(c));
                        }
                    },
                    I: function(a, d, f) {
                        try {
                            return b.push(null), a.apply(d, f || []);
                        } finally {
                            b.pop();
                        }
                    }
                };
            }();
            var L = {
                undefined: !0,
                "boolean": !0,
                number: !0,
                string: !0
            };
            a.m = function(b) {
                function c() {
                    if (0 < arguments.length) return c.equalityComparer && c.equalityComparer(d, arguments[0]) || (c.K(), 
                    d = arguments[0], c.J()), this;
                    a.q.bb(c);
                    return d;
                }
                var d = b;
                a.V.call(c);
                c.t = function() {
                    return d;
                };
                c.J = function() {
                    c.notifySubscribers(d);
                };
                c.K = function() {
                    c.notifySubscribers(d, "beforeChange");
                };
                a.a.extend(c, a.m.fn);
                a.r(c, "peek", c.t);
                a.r(c, "valueHasMutated", c.J);
                a.r(c, "valueWillMutate", c.K);
                return c;
            };
            a.m.fn = {
                equalityComparer: function(a, c) {
                    return null === a || typeof a in L ? a === c : !1;
                }
            };
            var A = a.m.Pb = "__ko_proto__";
            a.m.fn[A] = a.m;
            a.qa = function(b, c) {
                return null === b || b === q || b[A] === q ? !1 : b[A] === c ? !0 : a.qa(b[A], c);
            };
            a.T = function(b) {
                return a.qa(b, a.m);
            };
            a.Xa = function(b) {
                return "function" == typeof b && b[A] === a.m || "function" == typeof b && b[A] === a.j && b.Eb ? !0 : !1;
            };
            a.b("observable", a.m);
            a.b("isObservable", a.T);
            a.b("isWriteableObservable", a.Xa);
            a.U = function(b) {
                b = b || [];
                if ("object" != typeof b || !("length" in b)) throw Error("The argument passed when initializing an observable array must be an array, or null, or undefined.");
                b = a.m(b);
                a.a.extend(b, a.U.fn);
                return b;
            };
            a.U.fn = {
                remove: function(a) {
                    for (var c = this.t(), d = [], f = "function" == typeof a ? a : function(e) {
                        return e === a;
                    }, g = 0; g < c.length; g++) {
                        var e = c[g];
                        f(e) && (0 === d.length && this.K(), d.push(e), c.splice(g, 1), g--);
                    }
                    d.length && this.J();
                    return d;
                },
                removeAll: function(b) {
                    if (b === q) {
                        var c = this.t(), d = c.slice(0);
                        this.K();
                        c.splice(0, c.length);
                        this.J();
                        return d;
                    }
                    return b ? this.remove(function(c) {
                        return 0 <= a.a.k(b, c);
                    }) : [];
                },
                destroy: function(a) {
                    var c = this.t(), d = "function" == typeof a ? a : function(c) {
                        return c === a;
                    };
                    this.K();
                    for (var f = c.length - 1; 0 <= f; f--) d(c[f]) && (c[f]._destroy = !0);
                    this.J();
                },
                destroyAll: function(b) {
                    return b === q ? this.destroy(F(!0)) : b ? this.destroy(function(c) {
                        return 0 <= a.a.k(b, c);
                    }) : [];
                },
                indexOf: function(b) {
                    var c = this();
                    return a.a.k(c, b);
                },
                replace: function(a, c) {
                    var d = this.indexOf(a);
                    0 <= d && (this.K(), this.t()[d] = c, this.J());
                }
            };
            a.a.p("pop push reverse shift sort splice unshift".split(" "), function(b) {
                a.U.fn[b] = function() {
                    var a = this.t();
                    this.K();
                    a = a[b].apply(a, arguments);
                    this.J();
                    return a;
                };
            });
            a.a.p([ "slice" ], function(b) {
                a.U.fn[b] = function() {
                    var a = this();
                    return a[b].apply(a, arguments);
                };
            });
            a.b("observableArray", a.U);
            a.j = function(b, c, d) {
                function f() {
                    a.a.p(v, function(a) {
                        a.B();
                    });
                    v = [];
                }
                function g() {
                    var a = m.throttleEvaluation;
                    a && 0 <= a ? (clearTimeout(t), t = setTimeout(e, a)) : e();
                }
                function e() {
                    if (!n) if (l && D()) x(); else {
                        n = !0;
                        try {
                            var b = a.a.Z(v, function(a) {
                                return a.target;
                            });
                            a.q.rb(function(e) {
                                var c;
                                0 <= (c = a.a.k(b, e)) ? b[c] = q : v.push(e.Da(g));
                            });
                            for (var e = p.call(c), d = b.length - 1; 0 <= d; d--) b[d] && v.splice(d, 1)[0].B();
                            l = !0;
                            m.notifySubscribers(k, "beforeChange");
                            k = e;
                            m.notifySubscribers(k);
                        } finally {
                            a.q.end(), n = !1;
                        }
                        v.length || x();
                    }
                }
                function m() {
                    if (0 < arguments.length) {
                        if ("function" === typeof r) r.apply(c, arguments); else throw Error("Cannot write a value to a ko.computed unless you specify a 'write' option. If you wish to read the current value, don't pass any parameters.");
                        return this;
                    }
                    l || e();
                    a.q.bb(m);
                    return k;
                }
                function h() {
                    return !l || 0 < v.length;
                }
                var k, l = !1, n = !1, p = b;
                p && "object" == typeof p ? (d = p, p = d.read) : (d = d || {}, p || (p = d.read));
                if ("function" != typeof p) throw Error("Pass a function that returns the value of the ko.computed");
                var r = d.write, z = d.disposeWhenNodeIsRemoved || d.$ || null, D = d.disposeWhen || d.Qa || F(!1), x = f, v = [], t = null;
                c || (c = d.owner);
                m.t = function() {
                    l || e();
                    return k;
                };
                m.Cb = function() {
                    return v.length;
                };
                m.Eb = "function" === typeof d.write;
                m.B = function() {
                    x();
                };
                m.ta = h;
                a.V.call(m);
                a.a.extend(m, a.j.fn);
                a.r(m, "peek", m.t);
                a.r(m, "dispose", m.B);
                a.r(m, "isActive", m.ta);
                a.r(m, "getDependenciesCount", m.Cb);
                !0 !== d.deferEvaluation && e();
                if (z && h()) {
                    x = function() {
                        a.a.C.cb(z, x);
                        f();
                    };
                    a.a.C.ia(z, x);
                    var s = D, D = function() {
                        return !a.a.aa(z) || s();
                    };
                }
                return m;
            };
            a.Gb = function(b) {
                return a.qa(b, a.j);
            };
            C = a.m.Pb;
            a.j[C] = a.m;
            a.j.fn = {};
            a.j.fn[C] = a.j;
            a.b("dependentObservable", a.j);
            a.b("computed", a.j);
            a.b("isComputed", a.Gb);
            (function() {
                function b(a, g, e) {
                    e = e || new d();
                    a = g(a);
                    if ("object" != typeof a || null === a || a === q || a instanceof Date || a instanceof String || a instanceof Number || a instanceof Boolean) return a;
                    var m = a instanceof Array ? [] : {};
                    e.save(a, m);
                    c(a, function(c) {
                        var d = g(a[c]);
                        switch (typeof d) {
                          case "boolean":
                          case "number":
                          case "string":
                          case "function":
                            m[c] = d;
                            break;

                          case "object":
                          case "undefined":
                            var l = e.get(d);
                            m[c] = l !== q ? l : b(d, g, e);
                        }
                    });
                    return m;
                }
                function c(a, b) {
                    if (a instanceof Array) {
                        for (var e = 0; e < a.length; e++) b(e);
                        "function" == typeof a.toJSON && b("toJSON");
                    } else for (e in a) b(e);
                }
                function d() {
                    this.keys = [];
                    this.Ha = [];
                }
                a.lb = function(c) {
                    if (0 == arguments.length) throw Error("When calling ko.toJS, pass the object you want to convert.");
                    return b(c, function(b) {
                        for (var e = 0; a.T(b) && 10 > e; e++) b = b();
                        return b;
                    });
                };
                a.toJSON = function(b, c, e) {
                    b = a.lb(b);
                    return a.a.Ca(b, c, e);
                };
                d.prototype = {
                    save: function(b, c) {
                        var e = a.a.k(this.keys, b);
                        0 <= e ? this.Ha[e] = c : (this.keys.push(b), this.Ha.push(c));
                    },
                    get: function(b) {
                        b = a.a.k(this.keys, b);
                        return 0 <= b ? this.Ha[b] : q;
                    }
                };
            })();
            a.b("toJS", a.lb);
            a.b("toJSON", a.toJSON);
            (function() {
                a.h = {
                    n: function(b) {
                        switch (a.a.u(b)) {
                          case "option":
                            return !0 === b.__ko__hasDomDataOptionValue__ ? a.a.f.get(b, a.d.options.wa) : 7 >= a.a.ca ? b.getAttributeNode("value") && b.getAttributeNode("value").specified ? b.value : b.text : b.value;

                          case "select":
                            return 0 <= b.selectedIndex ? a.h.n(b.options[b.selectedIndex]) : q;

                          default:
                            return b.value;
                        }
                    },
                    W: function(b, c) {
                        switch (a.a.u(b)) {
                          case "option":
                            switch (typeof c) {
                              case "string":
                                a.a.f.set(b, a.d.options.wa, q);
                                "__ko__hasDomDataOptionValue__" in b && delete b.__ko__hasDomDataOptionValue__;
                                b.value = c;
                                break;

                              default:
                                a.a.f.set(b, a.d.options.wa, c), b.__ko__hasDomDataOptionValue__ = !0, b.value = "number" === typeof c ? c : "";
                            }
                            break;

                          case "select":
                            "" === c && (c = q);
                            if (null === c || c === q) b.selectedIndex = -1;
                            for (var d = b.options.length - 1; 0 <= d; d--) if (a.h.n(b.options[d]) == c) {
                                b.selectedIndex = d;
                                break;
                            }
                            1 < b.size || -1 !== b.selectedIndex || (b.selectedIndex = 0);
                            break;

                          default:
                            if (null === c || c === q) c = "";
                            b.value = c;
                        }
                    }
                };
            })();
            a.b("selectExtensions", a.h);
            a.b("selectExtensions.readValue", a.h.n);
            a.b("selectExtensions.writeValue", a.h.W);
            a.g = function() {
                function b(a, b) {
                    for (var d = null; a != d; ) d = a, a = a.replace(c, function(a, c) {
                        return b[c];
                    });
                    return a;
                }
                var c = /\@ko_token_(\d+)\@/g, d = [ "true", "false", "null", "undefined" ], f = /^(?:[$_a-z][$\w]*|(.+)(\.\s*[$_a-z][$\w]*|\[.+\]))$/i;
                return {
                    S: [],
                    da: function(c) {
                        var e = a.a.F(c);
                        if (3 > e.length) return [];
                        "{" === e.charAt(0) && (e = e.substring(1, e.length - 1));
                        c = [];
                        for (var d = null, f, k = 0; k < e.length; k++) {
                            var l = e.charAt(k);
                            if (null === d) switch (l) {
                              case '"':
                              case "'":
                              case "/":
                                d = k, f = l;
                            } else if (l == f && "\\" !== e.charAt(k - 1)) {
                                l = e.substring(d, k + 1);
                                c.push(l);
                                var n = "@ko_token_" + (c.length - 1) + "@", e = e.substring(0, d) + n + e.substring(k + 1), k = k - (l.length - n.length), d = null;
                            }
                        }
                        f = d = null;
                        for (var p = 0, r = null, k = 0; k < e.length; k++) {
                            l = e.charAt(k);
                            if (null === d) switch (l) {
                              case "{":
                                d = k;
                                r = l;
                                f = "}";
                                break;

                              case "(":
                                d = k;
                                r = l;
                                f = ")";
                                break;

                              case "[":
                                d = k, r = l, f = "]";
                            }
                            l === r ? p++ : l === f && (p--, 0 === p && (l = e.substring(d, k + 1), c.push(l), 
                            n = "@ko_token_" + (c.length - 1) + "@", e = e.substring(0, d) + n + e.substring(k + 1), 
                            k -= l.length - n.length, d = null));
                        }
                        f = [];
                        e = e.split(",");
                        d = 0;
                        for (k = e.length; d < k; d++) p = e[d], r = p.indexOf(":"), 0 < r && r < p.length - 1 ? (l = p.substring(r + 1), 
                        f.push({
                            key: b(p.substring(0, r), c),
                            value: b(l, c)
                        })) : f.push({
                            unknown: b(p, c)
                        });
                        return f;
                    },
                    ea: function(b) {
                        var e = "string" === typeof b ? a.g.da(b) : b, c = [];
                        b = [];
                        for (var h, k = 0; h = e[k]; k++) if (0 < c.length && c.push(","), h.key) {
                            var l;
                            a: {
                                l = h.key;
                                var n = a.a.F(l);
                                switch (n.length && n.charAt(0)) {
                                  case "'":
                                  case '"':
                                    break a;

                                  default:
                                    l = "'" + n + "'";
                                }
                            }
                            h = h.value;
                            c.push(l);
                            c.push(":");
                            c.push(h);
                            h = a.a.F(h);
                            0 <= a.a.k(d, a.a.F(h).toLowerCase()) ? h = !1 : (n = h.match(f), h = null === n ? !1 : n[1] ? "Object(" + n[1] + ")" + n[2] : h);
                            h && (0 < b.length && b.push(", "), b.push(l + " : function(__ko_value) { " + h + " = __ko_value; }"));
                        } else h.unknown && c.push(h.unknown);
                        e = c.join("");
                        0 < b.length && (e = e + ", '_ko_property_writers' : { " + b.join("") + " } ");
                        return e;
                    },
                    Jb: function(b, c) {
                        for (var d = 0; d < b.length; d++) if (a.a.F(b[d].key) == c) return !0;
                        return !1;
                    },
                    ha: function(b, c, d, f, k) {
                        if (b && a.T(b)) !a.Xa(b) || k && b.t() === f || b(f); else if ((b = c()._ko_property_writers) && b[d]) b[d](f);
                    }
                };
            }();
            a.b("expressionRewriting", a.g);
            a.b("expressionRewriting.bindingRewriteValidators", a.g.S);
            a.b("expressionRewriting.parseObjectLiteral", a.g.da);
            a.b("expressionRewriting.preProcessBindings", a.g.ea);
            a.b("jsonExpressionRewriting", a.g);
            a.b("jsonExpressionRewriting.insertPropertyAccessorsIntoJson", a.g.ea);
            (function() {
                function b(a) {
                    return 8 == a.nodeType && (g ? a.text : a.nodeValue).match(e);
                }
                function c(a) {
                    return 8 == a.nodeType && (g ? a.text : a.nodeValue).match(m);
                }
                function d(a, e) {
                    for (var d = a, g = 1, f = []; d = d.nextSibling; ) {
                        if (c(d) && (g--, 0 === g)) return f;
                        f.push(d);
                        b(d) && g++;
                    }
                    if (!e) throw Error("Cannot find closing comment tag to match: " + a.nodeValue);
                    return null;
                }
                function f(a, b) {
                    var c = d(a, b);
                    return c ? 0 < c.length ? c[c.length - 1].nextSibling : a.nextSibling : null;
                }
                var g = s && "<!--test-->" === s.createComment("test").text, e = g ? /^\x3c!--\s*ko(?:\s+(.+\s*\:[\s\S]*))?\s*--\x3e$/ : /^\s*ko(?:\s+(.+\s*\:[\s\S]*))?\s*$/, m = g ? /^\x3c!--\s*\/ko\s*--\x3e$/ : /^\s*\/ko\s*$/, h = {
                    ul: !0,
                    ol: !0
                };
                a.e = {
                    L: {},
                    childNodes: function(a) {
                        return b(a) ? d(a) : a.childNodes;
                    },
                    ba: function(c) {
                        if (b(c)) {
                            c = a.e.childNodes(c);
                            for (var e = 0, d = c.length; e < d; e++) a.removeNode(c[e]);
                        } else a.a.oa(c);
                    },
                    P: function(c, e) {
                        if (b(c)) {
                            a.e.ba(c);
                            for (var d = c.nextSibling, g = 0, f = e.length; g < f; g++) d.parentNode.insertBefore(e[g], d);
                        } else a.a.P(c, e);
                    },
                    ab: function(a, c) {
                        b(a) ? a.parentNode.insertBefore(c, a.nextSibling) : a.firstChild ? a.insertBefore(c, a.firstChild) : a.appendChild(c);
                    },
                    Va: function(c, e, d) {
                        d ? b(c) ? c.parentNode.insertBefore(e, d.nextSibling) : d.nextSibling ? c.insertBefore(e, d.nextSibling) : c.appendChild(e) : a.e.ab(c, e);
                    },
                    firstChild: function(a) {
                        return b(a) ? !a.nextSibling || c(a.nextSibling) ? null : a.nextSibling : a.firstChild;
                    },
                    nextSibling: function(a) {
                        b(a) && (a = f(a));
                        return a.nextSibling && c(a.nextSibling) ? null : a.nextSibling;
                    },
                    ob: function(a) {
                        return (a = b(a)) ? a[1] : null;
                    },
                    Za: function(e) {
                        if (h[a.a.u(e)]) {
                            var d = e.firstChild;
                            if (d) {
                                do if (1 === d.nodeType) {
                                    var g;
                                    g = d.firstChild;
                                    var m = null;
                                    if (g) {
                                        do if (m) m.push(g); else if (b(g)) {
                                            var r = f(g, !0);
                                            r ? g = r : m = [ g ];
                                        } else c(g) && (m = [ g ]); while (g = g.nextSibling);
                                    }
                                    if (g = m) for (m = d.nextSibling, r = 0; r < g.length; r++) m ? e.insertBefore(g[r], m) : e.appendChild(g[r]);
                                } while (d = d.nextSibling);
                            }
                        }
                    }
                };
            })();
            a.b("virtualElements", a.e);
            a.b("virtualElements.allowedBindings", a.e.L);
            a.b("virtualElements.emptyNode", a.e.ba);
            a.b("virtualElements.insertAfter", a.e.Va);
            a.b("virtualElements.prepend", a.e.ab);
            a.b("virtualElements.setDomNodeChildren", a.e.P);
            (function() {
                a.M = function() {
                    this.Na = {};
                };
                a.a.extend(a.M.prototype, {
                    nodeHasBindings: function(b) {
                        switch (b.nodeType) {
                          case 1:
                            return null != b.getAttribute("data-bind");

                          case 8:
                            return null != a.e.ob(b);

                          default:
                            return !1;
                        }
                    },
                    getBindings: function(a, c) {
                        var d = this.getBindingsString(a, c);
                        return d ? this.parseBindingsString(d, c, a) : null;
                    },
                    getBindingsString: function(b) {
                        switch (b.nodeType) {
                          case 1:
                            return b.getAttribute("data-bind");

                          case 8:
                            return a.e.ob(b);

                          default:
                            return null;
                        }
                    },
                    parseBindingsString: function(b, c, d) {
                        try {
                            var f;
                            if (!(f = this.Na[b])) {
                                var g = this.Na, e, m = "with($context){with($data||{}){return{" + a.g.ea(b) + "}}}";
                                e = new Function("$context", "$element", m);
                                f = g[b] = e;
                            }
                            return f(c, d);
                        } catch (h) {
                            throw h.message = "Unable to parse bindings.\nBindings value: " + b + "\nMessage: " + h.message, 
                            h;
                        }
                    }
                });
                a.M.instance = new a.M();
            })();
            a.b("bindingProvider", a.M);
            (function() {
                function b(b, e, d) {
                    for (var f = a.e.firstChild(e); e = f; ) f = a.e.nextSibling(e), c(b, e, d);
                }
                function c(c, e, f) {
                    var h = !0, k = 1 === e.nodeType;
                    k && a.e.Za(e);
                    if (k && f || a.M.instance.nodeHasBindings(e)) h = d(e, null, c, f).Sb;
                    h && b(c, e, !k);
                }
                function d(b, c, d, h) {
                    function k(a) {
                        return function() {
                            return p[a];
                        };
                    }
                    function l() {
                        return p;
                    }
                    var n = 0, p, r, z = a.a.f.get(b, f);
                    if (!c) {
                        if (z) throw Error("You cannot apply bindings multiple times to the same element.");
                        a.a.f.set(b, f, !0);
                    }
                    a.j(function() {
                        var f = d && d instanceof a.A ? d : new a.A(a.a.c(d)), x = f.$data;
                        !z && h && a.jb(b, f);
                        if (p = ("function" == typeof c ? c(f, b) : c) || a.M.instance.getBindings(b, f)) 0 === n && (n = 1, 
                        a.a.w(p, function(c) {
                            var e = a.d[c];
                            if (e && 8 === b.nodeType && !a.e.L[c]) throw Error("The binding '" + c + "' cannot be used with virtual elements");
                            if (e && "function" == typeof e.init && (e = (0, e.init)(b, k(c), l, x, f)) && e.controlsDescendantBindings) {
                                if (r !== q) throw Error("Multiple bindings (" + r + " and " + c + ") are trying to control descendant bindings of the same element. You cannot use these bindings together on the same element.");
                                r = c;
                            }
                        }), n = 2), 2 === n && a.a.w(p, function(c) {
                            var e = a.d[c];
                            e && "function" == typeof e.update && (0, e.update)(b, k(c), l, x, f);
                        });
                    }, null, {
                        $: b
                    });
                    return {
                        Sb: r === q
                    };
                }
                a.d = {};
                a.A = function(b, c, d) {
                    c ? (a.a.extend(this, c), this.$parentContext = c, this.$parent = c.$data, this.$parents = (c.$parents || []).slice(0), 
                    this.$parents.unshift(this.$parent)) : (this.$parents = [], this.$root = b, this.ko = a);
                    this.$data = b;
                    d && (this[d] = b);
                };
                a.A.prototype.createChildContext = function(b, c) {
                    return new a.A(b, this, c);
                };
                a.A.prototype.extend = function(b) {
                    var c = a.a.extend(new a.A(), this);
                    return a.a.extend(c, b);
                };
                var f = "__ko_boundElement";
                a.jb = function(b, c) {
                    if (2 == arguments.length) a.a.f.set(b, "__ko_bindingContext__", c); else return a.a.f.get(b, "__ko_bindingContext__");
                };
                a.Ka = function(b, c, f) {
                    1 === b.nodeType && a.e.Za(b);
                    return d(b, c, f, !0);
                };
                a.Ja = function(a, c) {
                    1 !== c.nodeType && 8 !== c.nodeType || b(a, c, !0);
                };
                a.Ia = function(a, b) {
                    if (b && 1 !== b.nodeType && 8 !== b.nodeType) throw Error("ko.applyBindings: first parameter should be your view model; second parameter should be a DOM node");
                    b = b || w.document.body;
                    c(a, b, !0);
                };
                a.na = function(b) {
                    switch (b.nodeType) {
                      case 1:
                      case 8:
                        var c = a.jb(b);
                        if (c) return c;
                        if (b.parentNode) return a.na(b.parentNode);
                    }
                    return q;
                };
                a.ub = function(b) {
                    return (b = a.na(b)) ? b.$data : q;
                };
                a.b("bindingHandlers", a.d);
                a.b("applyBindings", a.Ia);
                a.b("applyBindingsToDescendants", a.Ja);
                a.b("applyBindingsToNode", a.Ka);
                a.b("contextFor", a.na);
                a.b("dataFor", a.ub);
            })();
            var K = {
                "class": "className",
                "for": "htmlFor"
            };
            a.d.attr = {
                update: function(b, c) {
                    var d = a.a.c(c()) || {};
                    a.a.w(d, function(c, d) {
                        d = a.a.c(d);
                        var e = !1 === d || null === d || d === q;
                        e && b.removeAttribute(c);
                        8 >= a.a.ca && c in K ? (c = K[c], e ? b.removeAttribute(c) : b[c] = d) : e || b.setAttribute(c, d.toString());
                        "name" === c && a.a.gb(b, e ? "" : d.toString());
                    });
                }
            };
            a.d.checked = {
                init: function(b, c, d) {
                    a.a.o(b, "click", function() {
                        var f;
                        if ("checkbox" == b.type) f = b.checked; else if ("radio" == b.type && b.checked) f = b.value; else return;
                        var g = c(), e = a.a.c(g);
                        "checkbox" == b.type && e instanceof Array ? a.a.ja(g, b.value, b.checked) : a.g.ha(g, d, "checked", f, !0);
                    });
                    "radio" != b.type || b.name || a.d.uniqueName.init(b, F(!0));
                },
                update: function(b, c) {
                    var d = a.a.c(c());
                    "checkbox" == b.type ? b.checked = d instanceof Array ? 0 <= a.a.k(d, b.value) : d : "radio" == b.type && (b.checked = b.value == d);
                }
            };
            a.d.css = {
                update: function(b, c) {
                    var d = a.a.c(c());
                    "object" == typeof d ? a.a.w(d, function(c, d) {
                        d = a.a.c(d);
                        a.a.ga(b, c, d);
                    }) : (d = String(d || ""), a.a.ga(b, b.__ko__cssValue, !1), b.__ko__cssValue = d, 
                    a.a.ga(b, d, !0));
                }
            };
            a.d.enable = {
                update: function(b, c) {
                    var d = a.a.c(c());
                    d && b.disabled ? b.removeAttribute("disabled") : d || b.disabled || (b.disabled = !0);
                }
            };
            a.d.disable = {
                update: function(b, c) {
                    a.d.enable.update(b, function() {
                        return !a.a.c(c());
                    });
                }
            };
            a.d.event = {
                init: function(b, c, d, f) {
                    var g = c() || {};
                    a.a.w(g, function(e) {
                        "string" == typeof e && a.a.o(b, e, function(b) {
                            var g, k = c()[e];
                            if (k) {
                                var l = d();
                                try {
                                    var n = a.a.N(arguments);
                                    n.unshift(f);
                                    g = k.apply(f, n);
                                } finally {
                                    !0 !== g && (b.preventDefault ? b.preventDefault() : b.returnValue = !1);
                                }
                                !1 === l[e + "Bubble"] && (b.cancelBubble = !0, b.stopPropagation && b.stopPropagation());
                            }
                        });
                    });
                }
            };
            a.d.foreach = {
                Ya: function(b) {
                    return function() {
                        var c = b(), d = a.a.ya(c);
                        if (!d || "number" == typeof d.length) return {
                            foreach: c,
                            templateEngine: a.D.sa
                        };
                        a.a.c(c);
                        return {
                            foreach: d.data,
                            as: d.as,
                            includeDestroyed: d.includeDestroyed,
                            afterAdd: d.afterAdd,
                            beforeRemove: d.beforeRemove,
                            afterRender: d.afterRender,
                            beforeMove: d.beforeMove,
                            afterMove: d.afterMove,
                            templateEngine: a.D.sa
                        };
                    };
                },
                init: function(b, c) {
                    return a.d.template.init(b, a.d.foreach.Ya(c));
                },
                update: function(b, c, d, f, g) {
                    return a.d.template.update(b, a.d.foreach.Ya(c), d, f, g);
                }
            };
            a.g.S.foreach = !1;
            a.e.L.foreach = !0;
            a.d.hasfocus = {
                init: function(b, c, d) {
                    function f(e) {
                        b.__ko_hasfocusUpdating = !0;
                        var f = b.ownerDocument;
                        if ("activeElement" in f) {
                            var g;
                            try {
                                g = f.activeElement;
                            } catch (l) {
                                g = f.body;
                            }
                            e = g === b;
                        }
                        f = c();
                        a.g.ha(f, d, "hasfocus", e, !0);
                        b.__ko_hasfocusLastValue = e;
                        b.__ko_hasfocusUpdating = !1;
                    }
                    var g = f.bind(null, !0), e = f.bind(null, !1);
                    a.a.o(b, "focus", g);
                    a.a.o(b, "focusin", g);
                    a.a.o(b, "blur", e);
                    a.a.o(b, "focusout", e);
                },
                update: function(b, c) {
                    var d = !!a.a.c(c());
                    b.__ko_hasfocusUpdating || b.__ko_hasfocusLastValue === d || (d ? b.focus() : b.blur(), 
                    a.q.I(a.a.Ga, null, [ b, d ? "focusin" : "focusout" ]));
                }
            };
            a.d.hasFocus = a.d.hasfocus;
            a.d.html = {
                init: function() {
                    return {
                        controlsDescendantBindings: !0
                    };
                },
                update: function(b, c) {
                    a.a.fa(b, c());
                }
            };
            var I = "__ko_withIfBindingData";
            G("if");
            G("ifnot", !1, !0);
            G("with", !0, !1, function(a, c) {
                return a.createChildContext(c);
            });
            a.d.options = {
                init: function(b) {
                    if ("select" !== a.a.u(b)) throw Error("options binding applies only to SELECT elements");
                    for (;0 < b.length; ) b.remove(0);
                    return {
                        controlsDescendantBindings: !0
                    };
                },
                update: function(b, c, d) {
                    function f(a, b, c) {
                        var d = typeof b;
                        return "function" == d ? b(a) : "string" == d ? a[b] : c;
                    }
                    function g(b, c) {
                        if (p) {
                            var d = 0 <= a.a.k(p, a.h.n(c[0]));
                            a.a.hb(c[0], d);
                        }
                    }
                    var e = 0 == b.length, m = !e && b.multiple ? b.scrollTop : null;
                    c = a.a.c(c());
                    var h = d(), k = h.optionsIncludeDestroyed, l = {}, n, p;
                    b.multiple ? p = a.a.Z(b.selectedOptions || a.a.Y(b.childNodes, function(b) {
                        return b.tagName && "option" === a.a.u(b) && b.selected;
                    }), function(b) {
                        return a.h.n(b);
                    }) : 0 <= b.selectedIndex && (p = [ a.h.n(b.options[b.selectedIndex]) ]);
                    if (c) {
                        "undefined" == typeof c.length && (c = [ c ]);
                        var r = a.a.Y(c, function(b) {
                            return k || b === q || null === b || !a.a.c(b._destroy);
                        });
                        "optionsCaption" in h && (n = a.a.c(h.optionsCaption), null !== n && n !== q && r.unshift(l));
                    } else c = [];
                    d = g;
                    h.optionsAfterRender && (d = function(b, c) {
                        g(0, c);
                        a.q.I(h.optionsAfterRender, null, [ c[0], b !== l ? b : q ]);
                    });
                    a.a.Aa(b, r, function(b, c, d) {
                        d.length && (p = d[0].selected && [ a.h.n(d[0]) ]);
                        c = s.createElement("option");
                        b === l ? (a.a.fa(c, n), a.h.W(c, q)) : (d = f(b, h.optionsValue, b), a.h.W(c, a.a.c(d)), 
                        b = f(b, h.optionsText, d), a.a.ib(c, b));
                        return [ c ];
                    }, null, d);
                    p = null;
                    e && "value" in h && J(b, a.a.ya(h.value), !0);
                    a.a.zb(b);
                    m && 20 < Math.abs(m - b.scrollTop) && (b.scrollTop = m);
                }
            };
            a.d.options.wa = "__ko.optionValueDomData__";
            a.d.selectedOptions = {
                init: function(b, c, d) {
                    a.a.o(b, "change", function() {
                        var f = c(), g = [];
                        a.a.p(b.getElementsByTagName("option"), function(b) {
                            b.selected && g.push(a.h.n(b));
                        });
                        a.g.ha(f, d, "selectedOptions", g);
                    });
                },
                update: function(b, c) {
                    if ("select" != a.a.u(b)) throw Error("values binding applies only to SELECT elements");
                    var d = a.a.c(c());
                    d && "number" == typeof d.length && a.a.p(b.getElementsByTagName("option"), function(b) {
                        var c = 0 <= a.a.k(d, a.h.n(b));
                        a.a.hb(b, c);
                    });
                }
            };
            a.d.style = {
                update: function(b, c) {
                    var d = a.a.c(c() || {});
                    a.a.w(d, function(c, d) {
                        d = a.a.c(d);
                        b.style[c] = d || "";
                    });
                }
            };
            a.d.submit = {
                init: function(b, c, d, f) {
                    if ("function" != typeof c()) throw Error("The value for a submit binding must be a function");
                    a.a.o(b, "submit", function(a) {
                        var d, m = c();
                        try {
                            d = m.call(f, b);
                        } finally {
                            !0 !== d && (a.preventDefault ? a.preventDefault() : a.returnValue = !1);
                        }
                    });
                }
            };
            a.d.text = {
                update: function(b, c) {
                    a.a.ib(b, c());
                }
            };
            a.e.L.text = !0;
            a.d.uniqueName = {
                init: function(b, c) {
                    if (c()) {
                        var d = "ko_unique_" + ++a.d.uniqueName.tb;
                        a.a.gb(b, d);
                    }
                }
            };
            a.d.uniqueName.tb = 0;
            a.d.value = {
                init: function(b, c, d) {
                    function f() {
                        m = !1;
                        var e = c(), f = a.h.n(b);
                        a.g.ha(e, d, "value", f);
                    }
                    var g = [ "change" ], e = d().valueUpdate, m = !1;
                    e && ("string" == typeof e && (e = [ e ]), a.a.R(g, e), g = a.a.Ma(g));
                    !a.a.ca || "input" != b.tagName.toLowerCase() || "text" != b.type || "off" == b.autocomplete || b.form && "off" == b.form.autocomplete || -1 != a.a.k(g, "propertychange") || (a.a.o(b, "propertychange", function() {
                        m = !0;
                    }), a.a.o(b, "blur", function() {
                        m && f();
                    }));
                    a.a.p(g, function(c) {
                        var d = f;
                        a.a.Tb(c, "after") && (d = function() {
                            setTimeout(f, 0);
                        }, c = c.substring(5));
                        a.a.o(b, c, d);
                    });
                },
                update: function(b, c) {
                    var d = "select" === a.a.u(b), f = a.a.c(c()), g = a.h.n(b);
                    f !== g && (g = function() {
                        a.h.W(b, f);
                    }, g(), d && setTimeout(g, 0));
                    d && 0 < b.length && J(b, f, !1);
                }
            };
            a.d.visible = {
                update: function(b, c) {
                    var d = a.a.c(c()), f = "none" != b.style.display;
                    d && !f ? b.style.display = "" : !d && f && (b.style.display = "none");
                }
            };
            (function(b) {
                a.d[b] = {
                    init: function(c, d, f, g) {
                        return a.d.event.init.call(this, c, function() {
                            var a = {};
                            a[b] = d();
                            return a;
                        }, f, g);
                    }
                };
            })("click");
            a.v = function() {};
            a.v.prototype.renderTemplateSource = function() {
                throw Error("Override renderTemplateSource");
            };
            a.v.prototype.createJavaScriptEvaluatorBlock = function() {
                throw Error("Override createJavaScriptEvaluatorBlock");
            };
            a.v.prototype.makeTemplateSource = function(b, c) {
                if ("string" == typeof b) {
                    c = c || s;
                    var d = c.getElementById(b);
                    if (!d) throw Error("Cannot find template with ID " + b);
                    return new a.l.i(d);
                }
                if (1 == b.nodeType || 8 == b.nodeType) return new a.l.Q(b);
                throw Error("Unknown template type: " + b);
            };
            a.v.prototype.renderTemplate = function(a, c, d, f) {
                a = this.makeTemplateSource(a, f);
                return this.renderTemplateSource(a, c, d);
            };
            a.v.prototype.isTemplateRewritten = function(a, c) {
                return !1 === this.allowTemplateRewriting ? !0 : this.makeTemplateSource(a, c).data("isRewritten");
            };
            a.v.prototype.rewriteTemplate = function(a, c, d) {
                a = this.makeTemplateSource(a, d);
                c = c(a.text());
                a.text(c);
                a.data("isRewritten", !0);
            };
            a.b("templateEngine", a.v);
            a.Ea = function() {
                function b(b, c, d, m) {
                    b = a.g.da(b);
                    for (var h = a.g.S, k = 0; k < b.length; k++) {
                        var l = b[k].key;
                        if (h.hasOwnProperty(l)) {
                            var n = h[l];
                            if ("function" === typeof n) {
                                if (l = n(b[k].value)) throw Error(l);
                            } else if (!n) throw Error("This template engine does not support the '" + l + "' binding within its templates");
                        }
                    }
                    d = "ko.__tr_ambtns(function($context,$element){return(function(){return{ " + a.g.ea(b) + " } })()},'" + d.toLowerCase() + "')";
                    return m.createJavaScriptEvaluatorBlock(d) + c;
                }
                var c = /(<([a-z]+\d*)(?:\s+(?!data-bind\s*=\s*)[a-z0-9\-]+(?:=(?:\"[^\"]*\"|\'[^\']*\'))?)*\s+)data-bind\s*=\s*(["'])([\s\S]*?)\3/gi, d = /\x3c!--\s*ko\b\s*([\s\S]*?)\s*--\x3e/g;
                return {
                    Ab: function(b, c, d) {
                        c.isTemplateRewritten(b, d) || c.rewriteTemplate(b, function(b) {
                            return a.Ea.Lb(b, c);
                        }, d);
                    },
                    Lb: function(a, g) {
                        return a.replace(c, function(a, c, d, f, l) {
                            return b(l, c, d, g);
                        }).replace(d, function(a, c) {
                            return b(c, "<!-- ko -->", "#comment", g);
                        });
                    },
                    qb: function(b, c) {
                        return a.s.va(function(d, m) {
                            var h = d.nextSibling;
                            h && h.nodeName.toLowerCase() === c && a.Ka(h, b, m);
                        });
                    }
                };
            }();
            a.b("__tr_ambtns", a.Ea.qb);
            (function() {
                a.l = {};
                a.l.i = function(a) {
                    this.i = a;
                };
                a.l.i.prototype.text = function() {
                    var b = a.a.u(this.i), b = "script" === b ? "text" : "textarea" === b ? "value" : "innerHTML";
                    if (0 == arguments.length) return this.i[b];
                    var c = arguments[0];
                    "innerHTML" === b ? a.a.fa(this.i, c) : this.i[b] = c;
                };
                a.l.i.prototype.data = function(b) {
                    if (1 === arguments.length) return a.a.f.get(this.i, "templateSourceData_" + b);
                    a.a.f.set(this.i, "templateSourceData_" + b, arguments[1]);
                };
                a.l.Q = function(a) {
                    this.i = a;
                };
                a.l.Q.prototype = new a.l.i();
                a.l.Q.prototype.text = function() {
                    if (0 == arguments.length) {
                        var b = a.a.f.get(this.i, "__ko_anon_template__") || {};
                        b.Fa === q && b.ma && (b.Fa = b.ma.innerHTML);
                        return b.Fa;
                    }
                    a.a.f.set(this.i, "__ko_anon_template__", {
                        Fa: arguments[0]
                    });
                };
                a.l.i.prototype.nodes = function() {
                    if (0 == arguments.length) return (a.a.f.get(this.i, "__ko_anon_template__") || {}).ma;
                    a.a.f.set(this.i, "__ko_anon_template__", {
                        ma: arguments[0]
                    });
                };
                a.b("templateSources", a.l);
                a.b("templateSources.domElement", a.l.i);
                a.b("templateSources.anonymousTemplate", a.l.Q);
            })();
            (function() {
                function b(b, c, d) {
                    var f;
                    for (c = a.e.nextSibling(c); b && (f = b) !== c; ) b = a.e.nextSibling(f), 1 !== f.nodeType && 8 !== f.nodeType || d(f);
                }
                function c(c, d) {
                    if (c.length) {
                        var f = c[0], g = c[c.length - 1];
                        b(f, g, function(b) {
                            a.Ia(d, b);
                        });
                        b(f, g, function(b) {
                            a.s.nb(b, [ d ]);
                        });
                    }
                }
                function d(a) {
                    return a.nodeType ? a : 0 < a.length ? a[0] : null;
                }
                function f(b, f, h, k, l) {
                    l = l || {};
                    var n = b && d(b), n = n && n.ownerDocument, p = l.templateEngine || g;
                    a.Ea.Ab(h, p, n);
                    h = p.renderTemplate(h, k, l, n);
                    if ("number" != typeof h.length || 0 < h.length && "number" != typeof h[0].nodeType) throw Error("Template engine must return an array of DOM nodes");
                    n = !1;
                    switch (f) {
                      case "replaceChildren":
                        a.e.P(b, h);
                        n = !0;
                        break;

                      case "replaceNode":
                        a.a.eb(b, h);
                        n = !0;
                        break;

                      case "ignoreTargetNode":
                        break;

                      default:
                        throw Error("Unknown renderMode: " + f);
                    }
                    n && (c(h, k), l.afterRender && a.q.I(l.afterRender, null, [ h, k.$data ]));
                    return h;
                }
                var g;
                a.Ba = function(b) {
                    if (b != q && !(b instanceof a.v)) throw Error("templateEngine must inherit from ko.templateEngine");
                    g = b;
                };
                a.za = function(b, c, h, k, l) {
                    h = h || {};
                    if ((h.templateEngine || g) == q) throw Error("Set a template engine before calling renderTemplate");
                    l = l || "replaceChildren";
                    if (k) {
                        var n = d(k);
                        return a.j(function() {
                            var g = c && c instanceof a.A ? c : new a.A(a.a.c(c)), r = "function" == typeof b ? b(g.$data, g) : b, g = f(k, l, r, g, h);
                            "replaceNode" == l && (k = g, n = d(k));
                        }, null, {
                            Qa: function() {
                                return !n || !a.a.aa(n);
                            },
                            $: n && "replaceNode" == l ? n.parentNode : n
                        });
                    }
                    return a.s.va(function(d) {
                        a.za(b, c, h, d, "replaceNode");
                    });
                };
                a.Rb = function(b, d, g, k, l) {
                    function n(a, b) {
                        c(b, r);
                        g.afterRender && g.afterRender(b, a);
                    }
                    function p(c, d) {
                        r = l.createChildContext(a.a.c(c), g.as);
                        r.$index = d;
                        var k = "function" == typeof b ? b(c, r) : b;
                        return f(null, "ignoreTargetNode", k, r, g);
                    }
                    var r;
                    return a.j(function() {
                        var b = a.a.c(d) || [];
                        "undefined" == typeof b.length && (b = [ b ]);
                        b = a.a.Y(b, function(b) {
                            return g.includeDestroyed || b === q || null === b || !a.a.c(b._destroy);
                        });
                        a.q.I(a.a.Aa, null, [ k, b, p, g, n ]);
                    }, null, {
                        $: k
                    });
                };
                a.d.template = {
                    init: function(b, c) {
                        var d = a.a.c(c());
                        "string" == typeof d || d.name || 1 != b.nodeType && 8 != b.nodeType || (d = 1 == b.nodeType ? b.childNodes : a.e.childNodes(b), 
                        d = a.a.Mb(d), new a.l.Q(b).nodes(d));
                        return {
                            controlsDescendantBindings: !0
                        };
                    },
                    update: function(b, c, d, f, g) {
                        c = a.a.c(c());
                        d = {};
                        f = !0;
                        var n, p = null;
                        "string" != typeof c && (d = c, c = a.a.c(d.name), "if" in d && (f = a.a.c(d["if"])), 
                        f && "ifnot" in d && (f = !a.a.c(d.ifnot)), n = a.a.c(d.data));
                        "foreach" in d ? p = a.Rb(c || b, f && d.foreach || [], d, b, g) : f ? (g = "data" in d ? g.createChildContext(n, d.as) : g, 
                        p = a.za(c || b, g, d, b)) : a.e.ba(b);
                        g = p;
                        (n = a.a.f.get(b, "__ko__templateComputedDomDataKey__")) && "function" == typeof n.B && n.B();
                        a.a.f.set(b, "__ko__templateComputedDomDataKey__", g && g.ta() ? g : q);
                    }
                };
                a.g.S.template = function(b) {
                    b = a.g.da(b);
                    return 1 == b.length && b[0].unknown || a.g.Jb(b, "name") ? null : "This template engine does not support anonymous templates nested within its templates";
                };
                a.e.L.template = !0;
            })();
            a.b("setTemplateEngine", a.Ba);
            a.b("renderTemplate", a.za);
            a.a.Pa = function() {
                function a(b, d, f, g, e) {
                    var m = Math.min, h = Math.max, k = [], l, n = b.length, p, r = d.length, q = r - n || 1, t = n + r + 1, s, v, w;
                    for (l = 0; l <= n; l++) for (v = s, k.push(s = []), w = m(r, l + q), p = h(0, l - 1); p <= w; p++) s[p] = p ? l ? b[l - 1] === d[p - 1] ? v[p - 1] : m(v[p] || t, s[p - 1] || t) + 1 : p + 1 : l + 1;
                    m = [];
                    h = [];
                    q = [];
                    l = n;
                    for (p = r; l || p; ) r = k[l][p] - 1, p && r === k[l][p - 1] ? h.push(m[m.length] = {
                        status: f,
                        value: d[--p],
                        index: p
                    }) : l && r === k[l - 1][p] ? q.push(m[m.length] = {
                        status: g,
                        value: b[--l],
                        index: l
                    }) : (m.push({
                        status: "retained",
                        value: d[--p]
                    }), --l);
                    if (h.length && q.length) {
                        b = 10 * n;
                        var E;
                        for (d = f = 0; (e || d < b) && (E = h[f]); f++) {
                            for (g = 0; k = q[g]; g++) if (E.value === k.value) {
                                E.moved = k.index;
                                k.moved = E.index;
                                q.splice(g, 1);
                                d = g = 0;
                                break;
                            }
                            d += g;
                        }
                    }
                    return m.reverse();
                }
                return function(c, d, f) {
                    c = c || [];
                    d = d || [];
                    return c.length <= d.length ? a(c, d, "added", "deleted", f) : a(d, c, "deleted", "added", f);
                };
            }();
            a.b("utils.compareArrays", a.a.Pa);
            (function() {
                function b(b) {
                    for (;b.length && !a.a.aa(b[0]); ) b.splice(0, 1);
                    if (1 < b.length) {
                        for (var c = b[0], g = b[b.length - 1], e = [ c ]; c !== g; ) {
                            c = c.nextSibling;
                            if (!c) return;
                            e.push(c);
                        }
                        Array.prototype.splice.apply(b, [ 0, b.length ].concat(e));
                    }
                    return b;
                }
                function c(c, f, g, e, m) {
                    var h = [];
                    c = a.j(function() {
                        var c = f(g, m, b(h)) || [];
                        0 < h.length && (a.a.eb(h, c), e && a.q.I(e, null, [ g, c, m ]));
                        h.splice(0, h.length);
                        a.a.R(h, c);
                    }, null, {
                        $: c,
                        Qa: function() {
                            return !a.a.pb(h);
                        }
                    });
                    return {
                        O: h,
                        j: c.ta() ? c : q
                    };
                }
                a.a.Aa = function(d, f, g, e, m) {
                    function h(a, c) {
                        u = n[c];
                        x !== c && (E[a] = u);
                        u.ra(x++);
                        b(u.O);
                        t.push(u);
                        w.push(u);
                    }
                    function k(b, c) {
                        if (b) for (var d = 0, e = c.length; d < e; d++) c[d] && a.a.p(c[d].O, function(a) {
                            b(a, d, c[d].X);
                        });
                    }
                    f = f || [];
                    e = e || {};
                    var l = a.a.f.get(d, "setDomNodeChildrenFromArrayMapping_lastMappingResult") === q, n = a.a.f.get(d, "setDomNodeChildrenFromArrayMapping_lastMappingResult") || [], p = a.a.Z(n, function(a) {
                        return a.X;
                    }), r = a.a.Pa(p, f, e.dontLimitMoves), t = [], s = 0, x = 0, v = [], w = [];
                    f = [];
                    for (var E = [], p = [], u, B = 0, y, A; y = r[B]; B++) switch (A = y.moved, y.status) {
                      case "deleted":
                        A === q && (u = n[s], u.j && u.j.B(), v.push.apply(v, b(u.O)), e.beforeRemove && (f[B] = u, 
                        w.push(u)));
                        s++;
                        break;

                      case "retained":
                        h(B, s++);
                        break;

                      case "added":
                        A !== q ? h(B, A) : (u = {
                            X: y.value,
                            ra: a.m(x++)
                        }, t.push(u), w.push(u), l || (p[B] = u));
                    }
                    k(e.beforeMove, E);
                    a.a.p(v, e.beforeRemove ? a.H : a.removeNode);
                    for (var B = 0, l = a.e.firstChild(d), C; u = w[B]; B++) {
                        u.O || a.a.extend(u, c(d, g, u.X, m, u.ra));
                        for (s = 0; r = u.O[s]; l = r.nextSibling, C = r, s++) r !== l && a.e.Va(d, r, C);
                        !u.Fb && m && (m(u.X, u.O, u.ra), u.Fb = !0);
                    }
                    k(e.beforeRemove, f);
                    k(e.afterMove, E);
                    k(e.afterAdd, p);
                    a.a.f.set(d, "setDomNodeChildrenFromArrayMapping_lastMappingResult", t);
                };
            })();
            a.b("utils.setDomNodeChildrenFromArrayMapping", a.a.Aa);
            a.D = function() {
                this.allowTemplateRewriting = !1;
            };
            a.D.prototype = new a.v();
            a.D.prototype.renderTemplateSource = function(b) {
                var c = (9 > a.a.ca ? 0 : b.nodes) ? b.nodes() : null;
                if (c) return a.a.N(c.cloneNode(!0).childNodes);
                b = b.text();
                return a.a.xa(b);
            };
            a.D.sa = new a.D();
            a.Ba(a.D.sa);
            a.b("nativeTemplateEngine", a.D);
            (function() {
                a.ua = function() {
                    var a = this.Ib = function() {
                        if ("undefined" == typeof t || !t.tmpl) return 0;
                        try {
                            if (0 <= t.tmpl.tag.tmpl.open.toString().indexOf("__")) return 2;
                        } catch (a) {}
                        return 1;
                    }();
                    this.renderTemplateSource = function(b, f, g) {
                        g = g || {};
                        if (2 > a) throw Error("Your version of jQuery.tmpl is too old. Please upgrade to jQuery.tmpl 1.0.0pre or later.");
                        var e = b.data("precompiled");
                        e || (e = b.text() || "", e = t.template(null, "{{ko_with $item.koBindingContext}}" + e + "{{/ko_with}}"), 
                        b.data("precompiled", e));
                        b = [ f.$data ];
                        f = t.extend({
                            koBindingContext: f
                        }, g.templateOptions);
                        f = t.tmpl(e, b, f);
                        f.appendTo(s.createElement("div"));
                        t.fragments = {};
                        return f;
                    };
                    this.createJavaScriptEvaluatorBlock = function(a) {
                        return "{{ko_code ((function() { return " + a + " })()) }}";
                    };
                    this.addTemplate = function(a, b) {
                        s.write("<script type='text/html' id='" + a + "'>" + b + "</script>");
                    };
                    0 < a && (t.tmpl.tag.ko_code = {
                        open: "__.push($1 || '');"
                    }, t.tmpl.tag.ko_with = {
                        open: "with($1) {",
                        close: "} "
                    });
                };
                a.ua.prototype = new a.v();
                var b = new a.ua();
                0 < b.Ib && a.Ba(b);
                a.b("jqueryTmplTemplateEngine", a.ua);
            })();
        });
    })();
})();

(window._gsQueue || (window._gsQueue = [])).push(function() {
    window._gsDefine("TweenMax", [ "core.Animation", "core.SimpleTimeline", "TweenLite" ], function(t, e, i) {
        var s = [].slice, r = function(t, e, s) {
            i.call(this, t, e, s), this._cycle = 0, this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, 
            this._repeatDelay = this.vars.repeatDelay || 0, this._dirty = !0, this.render = r.prototype.render;
        }, n = function(t) {
            return t.jquery || t.length && t !== window && t[0] && (t[0] === window || t[0].nodeType && t[0].style && !t.nodeType);
        }, a = r.prototype = i.to({}, .1, {}), o = [];
        r.version = "1.10.3", a.constructor = r, a.kill()._gc = !1, r.killTweensOf = r.killDelayedCallsTo = i.killTweensOf, 
        r.getTweensOf = i.getTweensOf, r.ticker = i.ticker, a.invalidate = function() {
            return this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, 
            this._repeatDelay = this.vars.repeatDelay || 0, this._uncache(!0), i.prototype.invalidate.call(this);
        }, a.updateTo = function(t, e) {
            var s, r = this.ratio;
            e && this.timeline && this._startTime < this._timeline._time && (this._startTime = this._timeline._time, 
            this._uncache(!1), this._gc ? this._enabled(!0, !1) : this._timeline.insert(this, this._startTime - this._delay));
            for (s in t) this.vars[s] = t[s];
            if (this._initted) if (e) this._initted = !1; else if (this._notifyPluginsOfEnabled && this._firstPT && i._onPluginEvent("_onDisable", this), 
            this._time / this._duration > .998) {
                var n = this._time;
                this.render(0, !0, !1), this._initted = !1, this.render(n, !0, !1);
            } else if (this._time > 0) {
                this._initted = !1, this._init();
                for (var a, o = 1 / (1 - r), h = this._firstPT; h; ) a = h.s + h.c, h.c *= o, h.s = a - h.c, 
                h = h._next;
            }
            return this;
        }, a.render = function(t, e, i) {
            var s, r, n, a, h, l, _, u = this._dirty ? this.totalDuration() : this._totalDuration, p = this._time, f = this._totalTime, c = this._cycle;
            if (t >= u ? (this._totalTime = u, this._cycle = this._repeat, this._yoyo && 0 !== (1 & this._cycle) ? (this._time = 0, 
            this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0) : (this._time = this._duration, 
            this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1), this._reversed || (s = !0, 
            r = "onComplete"), 0 === this._duration && ((0 === t || 0 > this._rawPrevTime) && this._rawPrevTime !== t && (i = !0, 
            this._rawPrevTime > 0 && (r = "onReverseComplete", e && (t = -1))), this._rawPrevTime = t)) : 1e-7 > t ? (this._totalTime = this._time = this._cycle = 0, 
            this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== f || 0 === this._duration && this._rawPrevTime > 0) && (r = "onReverseComplete", 
            s = this._reversed), 0 > t ? (this._active = !1, 0 === this._duration && (this._rawPrevTime >= 0 && (i = !0), 
            this._rawPrevTime = t)) : this._initted || (i = !0)) : (this._totalTime = this._time = t, 
            0 !== this._repeat && (a = this._duration + this._repeatDelay, this._cycle = this._totalTime / a >> 0, 
            0 !== this._cycle && this._cycle === this._totalTime / a && this._cycle--, this._time = this._totalTime - this._cycle * a, 
            this._yoyo && 0 !== (1 & this._cycle) && (this._time = this._duration - this._time), 
            this._time > this._duration ? this._time = this._duration : 0 > this._time && (this._time = 0)), 
            this._easeType ? (h = this._time / this._duration, l = this._easeType, _ = this._easePower, 
            (1 === l || 3 === l && h >= .5) && (h = 1 - h), 3 === l && (h *= 2), 1 === _ ? h *= h : 2 === _ ? h *= h * h : 3 === _ ? h *= h * h * h : 4 === _ && (h *= h * h * h * h), 
            this.ratio = 1 === l ? 1 - h : 2 === l ? h : .5 > this._time / this._duration ? h / 2 : 1 - h / 2) : this.ratio = this._ease.getRatio(this._time / this._duration)), 
            p === this._time && !i) return f !== this._totalTime && this._onUpdate && (e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || o)), 
            void 0;
            if (!this._initted) {
                if (this._init(), !this._initted) return;
                this._time && !s ? this.ratio = this._ease.getRatio(this._time / this._duration) : s && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1));
            }
            for (this._active || !this._paused && this._time !== p && t >= 0 && (this._active = !0), 
            0 === f && (this._startAt && (t >= 0 ? this._startAt.render(t, e, i) : r || (r = "_dummyGS")), 
            this.vars.onStart && (0 !== this._totalTime || 0 === this._duration) && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || o))), 
            n = this._firstPT; n; ) n.f ? n.t[n.p](n.c * this.ratio + n.s) : n.t[n.p] = n.c * this.ratio + n.s, 
            n = n._next;
            this._onUpdate && (0 > t && this._startAt && this._startAt.render(t, e, i), e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || o)), 
            this._cycle !== c && (e || this._gc || this.vars.onRepeat && this.vars.onRepeat.apply(this.vars.onRepeatScope || this, this.vars.onRepeatParams || o)), 
            r && (this._gc || (0 > t && this._startAt && !this._onUpdate && this._startAt.render(t, e, i), 
            s && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), 
            !e && this.vars[r] && this.vars[r].apply(this.vars[r + "Scope"] || this, this.vars[r + "Params"] || o)));
        }, r.to = function(t, e, i) {
            return new r(t, e, i);
        }, r.from = function(t, e, i) {
            return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, new r(t, e, i);
        }, r.fromTo = function(t, e, i, s) {
            return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, 
            new r(t, e, s);
        }, r.staggerTo = r.allTo = function(t, e, a, h, l, _, u) {
            h = h || 0;
            var p, f, c, m, d = a.delay || 0, g = [], v = function() {
                a.onComplete && a.onComplete.apply(a.onCompleteScope || this, arguments), l.apply(u || this, _ || o);
            };
            for (t instanceof Array || ("string" == typeof t && (t = i.selector(t) || t), n(t) && (t = s.call(t, 0))), 
            p = t.length, c = 0; p > c; c++) {
                f = {};
                for (m in a) f[m] = a[m];
                f.delay = d, c === p - 1 && l && (f.onComplete = v), g[c] = new r(t[c], e, f), d += h;
            }
            return g;
        }, r.staggerFrom = r.allFrom = function(t, e, i, s, n, a, o) {
            return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, r.staggerTo(t, e, i, s, n, a, o);
        }, r.staggerFromTo = r.allFromTo = function(t, e, i, s, n, a, o, h) {
            return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, 
            r.staggerTo(t, e, s, n, a, o, h);
        }, r.delayedCall = function(t, e, i, s, n) {
            return new r(e, 0, {
                delay: t,
                onComplete: e,
                onCompleteParams: i,
                onCompleteScope: s,
                onReverseComplete: e,
                onReverseCompleteParams: i,
                onReverseCompleteScope: s,
                immediateRender: !1,
                useFrames: n,
                overwrite: 0
            });
        }, r.set = function(t, e) {
            return new r(t, 0, e);
        }, r.isTweening = function(t) {
            for (var e, s = i.getTweensOf(t), r = s.length; --r > -1; ) if (e = s[r], e._active || e._startTime === e._timeline._time && e._timeline._active) return !0;
            return !1;
        };
        var h = function(t, e) {
            for (var s = [], r = 0, n = t._first; n; ) n instanceof i ? s[r++] = n : (e && (s[r++] = n), 
            s = s.concat(h(n, e)), r = s.length), n = n._next;
            return s;
        }, l = r.getAllTweens = function(e) {
            return h(t._rootTimeline, e).concat(h(t._rootFramesTimeline, e));
        };
        r.killAll = function(t, i, s, r) {
            null == i && (i = !0), null == s && (s = !0);
            var n, a, o, h = l(0 != r), _ = h.length, u = i && s && r;
            for (o = 0; _ > o; o++) a = h[o], (u || a instanceof e || (n = a.target === a.vars.onComplete) && s || i && !n) && (t ? a.totalTime(a.totalDuration()) : a._enabled(!1, !1));
        }, r.killChildTweensOf = function(t, e) {
            if (null != t) {
                var a, o, h, l, _, u = i._tweenLookup;
                if ("string" == typeof t && (t = i.selector(t) || t), n(t) && (t = s(t, 0)), t instanceof Array) for (l = t.length; --l > -1; ) r.killChildTweensOf(t[l], e); else {
                    a = [];
                    for (h in u) for (o = u[h].target.parentNode; o; ) o === t && (a = a.concat(u[h].tweens)), 
                    o = o.parentNode;
                    for (_ = a.length, l = 0; _ > l; l++) e && a[l].totalTime(a[l].totalDuration()), 
                    a[l]._enabled(!1, !1);
                }
            }
        };
        var _ = function(t, i, s, r) {
            i = i !== !1, s = s !== !1, r = r !== !1;
            for (var n, a, o = l(r), h = i && s && r, _ = o.length; --_ > -1; ) a = o[_], (h || a instanceof e || (n = a.target === a.vars.onComplete) && s || i && !n) && a.paused(t);
        };
        return r.pauseAll = function(t, e, i) {
            _(!0, t, e, i);
        }, r.resumeAll = function(t, e, i) {
            _(!1, t, e, i);
        }, r.globalTimeScale = function(e) {
            var s = t._rootTimeline, r = i.ticker.time;
            return arguments.length ? (e = e || 1e-6, s._startTime = r - (r - s._startTime) * s._timeScale / e, 
            s = t._rootFramesTimeline, r = i.ticker.frame, s._startTime = r - (r - s._startTime) * s._timeScale / e, 
            s._timeScale = t._rootTimeline._timeScale = e, e) : s._timeScale;
        }, a.progress = function(t) {
            return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - t : t) + this._cycle * (this._duration + this._repeatDelay), !1) : this._time / this.duration();
        }, a.totalProgress = function(t) {
            return arguments.length ? this.totalTime(this.totalDuration() * t, !1) : this._totalTime / this.totalDuration();
        }, a.time = function(t, e) {
            return arguments.length ? (this._dirty && this.totalDuration(), t > this._duration && (t = this._duration), 
            this._yoyo && 0 !== (1 & this._cycle) ? t = this._duration - t + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (t += this._cycle * (this._duration + this._repeatDelay)), 
            this.totalTime(t, e)) : this._time;
        }, a.duration = function(e) {
            return arguments.length ? t.prototype.duration.call(this, e) : this._duration;
        }, a.totalDuration = function(t) {
            return arguments.length ? -1 === this._repeat ? this : this.duration((t - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat, 
            this._dirty = !1), this._totalDuration);
        }, a.repeat = function(t) {
            return arguments.length ? (this._repeat = t, this._uncache(!0)) : this._repeat;
        }, a.repeatDelay = function(t) {
            return arguments.length ? (this._repeatDelay = t, this._uncache(!0)) : this._repeatDelay;
        }, a.yoyo = function(t) {
            return arguments.length ? (this._yoyo = t, this) : this._yoyo;
        }, r;
    }, !0), window._gsDefine("TimelineLite", [ "core.Animation", "core.SimpleTimeline", "TweenLite" ], function(t, e, i) {
        var s = function(t) {
            e.call(this, t), this._labels = {}, this.autoRemoveChildren = this.vars.autoRemoveChildren === !0, 
            this.smoothChildTiming = this.vars.smoothChildTiming === !0, this._sortChildren = !0, 
            this._onUpdate = this.vars.onUpdate;
            var i, s, r = this.vars;
            for (s in r) i = r[s], i instanceof Array && -1 !== i.join("").indexOf("{self}") && (r[s] = this._swapSelfInParams(i));
            r.tweens instanceof Array && this.add(r.tweens, 0, r.align, r.stagger);
        }, r = [], n = function(t) {
            var e, i = {};
            for (e in t) i[e] = t[e];
            return i;
        }, a = function(t, e, i, s) {
            t._timeline.pause(t._startTime), e && e.apply(s || t._timeline, i || r);
        }, o = r.slice, h = s.prototype = new e();
        return s.version = "1.10.3", h.constructor = s, h.kill()._gc = !1, h.to = function(t, e, s, r) {
            return e ? this.add(new i(t, e, s), r) : this.set(t, s, r);
        }, h.from = function(t, e, s, r) {
            return this.add(i.from(t, e, s), r);
        }, h.fromTo = function(t, e, s, r, n) {
            return e ? this.add(i.fromTo(t, e, s, r), n) : this.set(t, r, n);
        }, h.staggerTo = function(t, e, r, a, h, l, _, u) {
            var p, f = new s({
                onComplete: l,
                onCompleteParams: _,
                onCompleteScope: u
            });
            for ("string" == typeof t && (t = i.selector(t) || t), !(t instanceof Array) && t.length && t !== window && t[0] && (t[0] === window || t[0].nodeType && t[0].style && !t.nodeType) && (t = o.call(t, 0)), 
            a = a || 0, p = 0; t.length > p; p++) r.startAt && (r.startAt = n(r.startAt)), f.to(t[p], e, n(r), p * a);
            return this.add(f, h);
        }, h.staggerFrom = function(t, e, i, s, r, n, a, o) {
            return i.immediateRender = 0 != i.immediateRender, i.runBackwards = !0, this.staggerTo(t, e, i, s, r, n, a, o);
        }, h.staggerFromTo = function(t, e, i, s, r, n, a, o, h) {
            return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, 
            this.staggerTo(t, e, s, r, n, a, o, h);
        }, h.call = function(t, e, s, r) {
            return this.add(i.delayedCall(0, t, e, s), r);
        }, h.set = function(t, e, s) {
            return s = this._parseTimeOrLabel(s, 0, !0), null == e.immediateRender && (e.immediateRender = s === this._time && !this._paused), 
            this.add(new i(t, 0, e), s);
        }, s.exportRoot = function(t, e) {
            t = t || {}, null == t.smoothChildTiming && (t.smoothChildTiming = !0);
            var r, n, a = new s(t), o = a._timeline;
            for (null == e && (e = !0), o._remove(a, !0), a._startTime = 0, a._rawPrevTime = a._time = a._totalTime = o._time, 
            r = o._first; r; ) n = r._next, e && r instanceof i && r.target === r.vars.onComplete || a.add(r, r._startTime - r._delay), 
            r = n;
            return o.add(a, 0), a;
        }, h.add = function(r, n, a, o) {
            var h, l, _, u, p, f;
            if ("number" != typeof n && (n = this._parseTimeOrLabel(n, 0, !0, r)), !(r instanceof t)) {
                if (r instanceof Array) {
                    for (a = a || "normal", o = o || 0, h = n, l = r.length, _ = 0; l > _; _++) (u = r[_]) instanceof Array && (u = new s({
                        tweens: u
                    })), this.add(u, h), "string" != typeof u && "function" != typeof u && ("sequence" === a ? h = u._startTime + u.totalDuration() / u._timeScale : "start" === a && (u._startTime -= u.delay())), 
                    h += o;
                    return this._uncache(!0);
                }
                if ("string" == typeof r) return this.addLabel(r, n);
                if ("function" != typeof r) throw "Cannot add " + r + " into the timeline; it is not a tween, timeline, function, or string.";
                r = i.delayedCall(0, r);
            }
            if (e.prototype.add.call(this, r, n), this._gc && !this._paused && this._duration < this.duration()) for (p = this, 
            f = p.rawTime() > r._startTime; p._gc && p._timeline; ) p._timeline.smoothChildTiming && f ? p.totalTime(p._totalTime, !0) : p._enabled(!0, !1), 
            p = p._timeline;
            return this;
        }, h.remove = function(e) {
            if (e instanceof t) return this._remove(e, !1);
            if (e instanceof Array) {
                for (var i = e.length; --i > -1; ) this.remove(e[i]);
                return this;
            }
            return "string" == typeof e ? this.removeLabel(e) : this.kill(null, e);
        }, h._remove = function(t, i) {
            return e.prototype._remove.call(this, t, i), this._last ? this._time > this._last._startTime && (this._time = this.duration(), 
            this._totalTime = this._totalDuration) : this._time = this._totalTime = 0, this;
        }, h.append = function(t, e) {
            return this.add(t, this._parseTimeOrLabel(null, e, !0, t));
        }, h.insert = h.insertMultiple = function(t, e, i, s) {
            return this.add(t, e || 0, i, s);
        }, h.appendMultiple = function(t, e, i, s) {
            return this.add(t, this._parseTimeOrLabel(null, e, !0, t), i, s);
        }, h.addLabel = function(t, e) {
            return this._labels[t] = this._parseTimeOrLabel(e), this;
        }, h.addPause = function(t, e, i, s) {
            return this.call(a, [ "{self}", e, i, s ], this, t);
        }, h.removeLabel = function(t) {
            return delete this._labels[t], this;
        }, h.getLabelTime = function(t) {
            return null != this._labels[t] ? this._labels[t] : -1;
        }, h._parseTimeOrLabel = function(e, i, s, r) {
            var n;
            if (r instanceof t && r.timeline === this) this.remove(r); else if (r instanceof Array) for (n = r.length; --n > -1; ) r[n] instanceof t && r[n].timeline === this && this.remove(r[n]);
            if ("string" == typeof i) return this._parseTimeOrLabel(i, s && "number" == typeof e && null == this._labels[i] ? e - this.duration() : 0, s);
            if (i = i || 0, "string" != typeof e || !isNaN(e) && null == this._labels[e]) null == e && (e = this.duration()); else {
                if (n = e.indexOf("="), -1 === n) return null == this._labels[e] ? s ? this._labels[e] = this.duration() + i : i : this._labels[e] + i;
                i = parseInt(e.charAt(n - 1) + "1", 10) * Number(e.substr(n + 1)), e = n > 1 ? this._parseTimeOrLabel(e.substr(0, n - 1), 0, s) : this.duration();
            }
            return Number(e) + i;
        }, h.seek = function(t, e) {
            return this.totalTime("number" == typeof t ? t : this._parseTimeOrLabel(t), e !== !1);
        }, h.stop = function() {
            return this.paused(!0);
        }, h.gotoAndPlay = function(t, e) {
            return this.play(t, e);
        }, h.gotoAndStop = function(t, e) {
            return this.pause(t, e);
        }, h.render = function(t, e, i) {
            this._gc && this._enabled(!0, !1);
            var s, n, a, o, h, l = this._dirty ? this.totalDuration() : this._totalDuration, _ = this._time, u = this._startTime, p = this._timeScale, f = this._paused;
            if (t >= l ? (this._totalTime = this._time = l, this._reversed || this._hasPausedChild() || (n = !0, 
            o = "onComplete", 0 === this._duration && (0 === t || 0 > this._rawPrevTime) && this._rawPrevTime !== t && this._first && (h = !0, 
            this._rawPrevTime > 0 && (o = "onReverseComplete"))), this._rawPrevTime = t, t = l + 1e-6) : 1e-7 > t ? (this._totalTime = this._time = 0, 
            (0 !== _ || 0 === this._duration && this._rawPrevTime > 0) && (o = "onReverseComplete", 
            n = this._reversed), 0 > t ? (this._active = !1, 0 === this._duration && this._rawPrevTime >= 0 && this._first && (h = !0), 
            this._rawPrevTime = t) : (this._rawPrevTime = t, t = 0, this._initted || (h = !0))) : this._totalTime = this._time = this._rawPrevTime = t, 
            this._time !== _ && this._first || i || h) {
                if (this._initted || (this._initted = !0), this._active || !this._paused && this._time !== _ && t > 0 && (this._active = !0), 
                0 === _ && this.vars.onStart && 0 !== this._time && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || r)), 
                this._time >= _) for (s = this._first; s && (a = s._next, !this._paused || f); ) (s._active || s._startTime <= this._time && !s._paused && !s._gc) && (s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)), 
                s = a; else for (s = this._last; s && (a = s._prev, !this._paused || f); ) (s._active || _ >= s._startTime && !s._paused && !s._gc) && (s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)), 
                s = a;
                this._onUpdate && (e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || r)), 
                o && (this._gc || (u === this._startTime || p !== this._timeScale) && (0 === this._time || l >= this.totalDuration()) && (n && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), 
                this._active = !1), !e && this.vars[o] && this.vars[o].apply(this.vars[o + "Scope"] || this, this.vars[o + "Params"] || r)));
            }
        }, h._hasPausedChild = function() {
            for (var t = this._first; t; ) {
                if (t._paused || t instanceof s && t._hasPausedChild()) return !0;
                t = t._next;
            }
            return !1;
        }, h.getChildren = function(t, e, s, r) {
            r = r || -9999999999;
            for (var n = [], a = this._first, o = 0; a; ) r > a._startTime || (a instanceof i ? e !== !1 && (n[o++] = a) : (s !== !1 && (n[o++] = a), 
            t !== !1 && (n = n.concat(a.getChildren(!0, e, s)), o = n.length))), a = a._next;
            return n;
        }, h.getTweensOf = function(t, e) {
            for (var s = i.getTweensOf(t), r = s.length, n = [], a = 0; --r > -1; ) (s[r].timeline === this || e && this._contains(s[r])) && (n[a++] = s[r]);
            return n;
        }, h._contains = function(t) {
            for (var e = t.timeline; e; ) {
                if (e === this) return !0;
                e = e.timeline;
            }
            return !1;
        }, h.shiftChildren = function(t, e, i) {
            i = i || 0;
            for (var s, r = this._first, n = this._labels; r; ) r._startTime >= i && (r._startTime += t), 
            r = r._next;
            if (e) for (s in n) n[s] >= i && (n[s] += t);
            return this._uncache(!0);
        }, h._kill = function(t, e) {
            if (!t && !e) return this._enabled(!1, !1);
            for (var i = e ? this.getTweensOf(e) : this.getChildren(!0, !0, !1), s = i.length, r = !1; --s > -1; ) i[s]._kill(t, e) && (r = !0);
            return r;
        }, h.clear = function(t) {
            var e = this.getChildren(!1, !0, !0), i = e.length;
            for (this._time = this._totalTime = 0; --i > -1; ) e[i]._enabled(!1, !1);
            return t !== !1 && (this._labels = {}), this._uncache(!0);
        }, h.invalidate = function() {
            for (var t = this._first; t; ) t.invalidate(), t = t._next;
            return this;
        }, h._enabled = function(t, i) {
            if (t === this._gc) for (var s = this._first; s; ) s._enabled(t, !0), s = s._next;
            return e.prototype._enabled.call(this, t, i);
        }, h.progress = function(t) {
            return arguments.length ? this.totalTime(this.duration() * t, !1) : this._time / this.duration();
        }, h.duration = function(t) {
            return arguments.length ? (0 !== this.duration() && 0 !== t && this.timeScale(this._duration / t), 
            this) : (this._dirty && this.totalDuration(), this._duration);
        }, h.totalDuration = function(t) {
            if (!arguments.length) {
                if (this._dirty) {
                    for (var e, i, s = 0, r = this._last, n = 999999999999; r; ) e = r._prev, r._dirty && r.totalDuration(), 
                    r._startTime > n && this._sortChildren && !r._paused ? this.add(r, r._startTime - r._delay) : n = r._startTime, 
                    0 > r._startTime && !r._paused && (s -= r._startTime, this._timeline.smoothChildTiming && (this._startTime += r._startTime / this._timeScale), 
                    this.shiftChildren(-r._startTime, !1, -9999999999), n = 0), i = r._startTime + r._totalDuration / r._timeScale, 
                    i > s && (s = i), r = e;
                    this._duration = this._totalDuration = s, this._dirty = !1;
                }
                return this._totalDuration;
            }
            return 0 !== this.totalDuration() && 0 !== t && this.timeScale(this._totalDuration / t), 
            this;
        }, h.usesFrames = function() {
            for (var e = this._timeline; e._timeline; ) e = e._timeline;
            return e === t._rootFramesTimeline;
        }, h.rawTime = function() {
            return this._paused ? this._totalTime : (this._timeline.rawTime() - this._startTime) * this._timeScale;
        }, s;
    }, !0), window._gsDefine("TimelineMax", [ "TimelineLite", "TweenLite", "easing.Ease" ], function(t, e, i) {
        var s = function(e) {
            t.call(this, e), this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, 
            this._cycle = 0, this._yoyo = this.vars.yoyo === !0, this._dirty = !0;
        }, r = [], n = new i(null, null, 1, 0), a = function(t) {
            for (;t; ) {
                if (t._paused) return !0;
                t = t._timeline;
            }
            return !1;
        }, o = s.prototype = new t();
        return o.constructor = s, o.kill()._gc = !1, s.version = "1.10.3", o.invalidate = function() {
            return this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, 
            this._repeatDelay = this.vars.repeatDelay || 0, this._uncache(!0), t.prototype.invalidate.call(this);
        }, o.addCallback = function(t, i, s, r) {
            return this.add(e.delayedCall(0, t, s, r), i);
        }, o.removeCallback = function(t, e) {
            if (t) if (null == e) this._kill(null, t); else for (var i = this.getTweensOf(t, !1), s = i.length, r = this._parseTimeOrLabel(e); --s > -1; ) i[s]._startTime === r && i[s]._enabled(!1, !1);
            return this;
        }, o.tweenTo = function(t, i) {
            i = i || {};
            var s, a, o = {
                ease: n,
                overwrite: 2,
                useFrames: this.usesFrames(),
                immediateRender: !1
            };
            for (s in i) o[s] = i[s];
            return o.time = this._parseTimeOrLabel(t), a = new e(this, Math.abs(Number(o.time) - this._time) / this._timeScale || .001, o), 
            o.onStart = function() {
                a.target.paused(!0), a.vars.time !== a.target.time() && a.duration(Math.abs(a.vars.time - a.target.time()) / a.target._timeScale), 
                i.onStart && i.onStart.apply(i.onStartScope || a, i.onStartParams || r);
            }, a;
        }, o.tweenFromTo = function(t, e, i) {
            i = i || {}, t = this._parseTimeOrLabel(t), i.startAt = {
                onComplete: this.seek,
                onCompleteParams: [ t ],
                onCompleteScope: this
            }, i.immediateRender = i.immediateRender !== !1;
            var s = this.tweenTo(e, i);
            return s.duration(Math.abs(s.vars.time - t) / this._timeScale || .001);
        }, o.render = function(t, e, i) {
            this._gc && this._enabled(!0, !1);
            var s, n, a, o, h, l, _ = this._dirty ? this.totalDuration() : this._totalDuration, u = this._duration, p = this._time, f = this._totalTime, c = this._startTime, m = this._timeScale, d = this._rawPrevTime, g = this._paused, v = this._cycle;
            if (t >= _ ? (this._locked || (this._totalTime = _, this._cycle = this._repeat), 
            this._reversed || this._hasPausedChild() || (n = !0, o = "onComplete", 0 === u && (0 === t || 0 > this._rawPrevTime) && this._rawPrevTime !== t && this._first && (h = !0, 
            this._rawPrevTime > 0 && (o = "onReverseComplete"))), this._rawPrevTime = t, this._yoyo && 0 !== (1 & this._cycle) ? this._time = t = 0 : (this._time = u, 
            t = u + 1e-6)) : 1e-7 > t ? (this._locked || (this._totalTime = this._cycle = 0), 
            this._time = 0, (0 !== p || 0 === u && this._rawPrevTime > 0 && !this._locked) && (o = "onReverseComplete", 
            n = this._reversed), 0 > t ? (this._active = !1, 0 === u && this._rawPrevTime >= 0 && this._first && (h = !0), 
            this._rawPrevTime = t) : (this._rawPrevTime = t, t = 0, this._initted || (h = !0))) : (this._time = this._rawPrevTime = t, 
            this._locked || (this._totalTime = t, 0 !== this._repeat && (l = u + this._repeatDelay, 
            this._cycle = this._totalTime / l >> 0, 0 !== this._cycle && this._cycle === this._totalTime / l && this._cycle--, 
            this._time = this._totalTime - this._cycle * l, this._yoyo && 0 !== (1 & this._cycle) && (this._time = u - this._time), 
            this._time > u ? (this._time = u, t = u + 1e-6) : 0 > this._time ? this._time = t = 0 : t = this._time))), 
            this._cycle !== v && !this._locked) {
                var y = this._yoyo && 0 !== (1 & v), T = y === (this._yoyo && 0 !== (1 & this._cycle)), w = this._totalTime, x = this._cycle, b = this._rawPrevTime, P = this._time;
                if (this._totalTime = v * u, v > this._cycle ? y = !y : this._totalTime += u, this._time = p, 
                this._rawPrevTime = 0 === u ? d - 1e-5 : d, this._cycle = v, this._locked = !0, 
                p = y ? 0 : u, this.render(p, e, 0 === u), e || this._gc || this.vars.onRepeat && this.vars.onRepeat.apply(this.vars.onRepeatScope || this, this.vars.onRepeatParams || r), 
                T && (p = y ? u + 1e-6 : -1e-6, this.render(p, !0, !1)), this._locked = !1, this._paused && !g) return;
                this._time = P, this._totalTime = w, this._cycle = x, this._rawPrevTime = b;
            }
            if (!(this._time !== p && this._first || i || h)) return f !== this._totalTime && this._onUpdate && (e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || r)), 
            void 0;
            if (this._initted || (this._initted = !0), this._active || !this._paused && this._totalTime !== f && t > 0 && (this._active = !0), 
            0 === f && this.vars.onStart && 0 !== this._totalTime && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || r)), 
            this._time >= p) for (s = this._first; s && (a = s._next, !this._paused || g); ) (s._active || s._startTime <= this._time && !s._paused && !s._gc) && (s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)), 
            s = a; else for (s = this._last; s && (a = s._prev, !this._paused || g); ) (s._active || p >= s._startTime && !s._paused && !s._gc) && (s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)), 
            s = a;
            this._onUpdate && (e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || r)), 
            o && (this._locked || this._gc || (c === this._startTime || m !== this._timeScale) && (0 === this._time || _ >= this.totalDuration()) && (n && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), 
            this._active = !1), !e && this.vars[o] && this.vars[o].apply(this.vars[o + "Scope"] || this, this.vars[o + "Params"] || r)));
        }, o.getActive = function(t, e, i) {
            null == t && (t = !0), null == e && (e = !0), null == i && (i = !1);
            var s, r, n = [], o = this.getChildren(t, e, i), h = 0, l = o.length;
            for (s = 0; l > s; s++) r = o[s], r._paused || r._timeline._time >= r._startTime && r._timeline._time < r._startTime + r._totalDuration / r._timeScale && (a(r._timeline) || (n[h++] = r));
            return n;
        }, o.getLabelAfter = function(t) {
            t || 0 !== t && (t = this._time);
            var e, i = this.getLabelsArray(), s = i.length;
            for (e = 0; s > e; e++) if (i[e].time > t) return i[e].name;
            return null;
        }, o.getLabelBefore = function(t) {
            null == t && (t = this._time);
            for (var e = this.getLabelsArray(), i = e.length; --i > -1; ) if (t > e[i].time) return e[i].name;
            return null;
        }, o.getLabelsArray = function() {
            var t, e = [], i = 0;
            for (t in this._labels) e[i++] = {
                time: this._labels[t],
                name: t
            };
            return e.sort(function(t, e) {
                return t.time - e.time;
            }), e;
        }, o.progress = function(t) {
            return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - t : t) + this._cycle * (this._duration + this._repeatDelay), !1) : this._time / this.duration();
        }, o.totalProgress = function(t) {
            return arguments.length ? this.totalTime(this.totalDuration() * t, !1) : this._totalTime / this.totalDuration();
        }, o.totalDuration = function(e) {
            return arguments.length ? -1 === this._repeat ? this : this.duration((e - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (t.prototype.totalDuration.call(this), 
            this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat), 
            this._totalDuration);
        }, o.time = function(t, e) {
            return arguments.length ? (this._dirty && this.totalDuration(), t > this._duration && (t = this._duration), 
            this._yoyo && 0 !== (1 & this._cycle) ? t = this._duration - t + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (t += this._cycle * (this._duration + this._repeatDelay)), 
            this.totalTime(t, e)) : this._time;
        }, o.repeat = function(t) {
            return arguments.length ? (this._repeat = t, this._uncache(!0)) : this._repeat;
        }, o.repeatDelay = function(t) {
            return arguments.length ? (this._repeatDelay = t, this._uncache(!0)) : this._repeatDelay;
        }, o.yoyo = function(t) {
            return arguments.length ? (this._yoyo = t, this) : this._yoyo;
        }, o.currentLabel = function(t) {
            return arguments.length ? this.seek(t, !0) : this.getLabelBefore(this._time + 1e-8);
        }, s;
    }, !0), function() {
        var t = 180 / Math.PI, e = Math.PI / 180, i = [], s = [], r = [], n = {}, a = function(t, e, i, s) {
            this.a = t, this.b = e, this.c = i, this.d = s, this.da = s - t, this.ca = i - t, 
            this.ba = e - t;
        }, o = ",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,", h = function(t, e, i, s) {
            var r = {
                a: t
            }, n = {}, a = {}, o = {
                c: s
            }, h = (t + e) / 2, l = (e + i) / 2, _ = (i + s) / 2, u = (h + l) / 2, p = (l + _) / 2, f = (p - u) / 8;
            return r.b = h + (t - h) / 4, n.b = u + f, r.c = n.a = (r.b + n.b) / 2, n.c = a.a = (u + p) / 2, 
            a.b = p - f, o.b = _ + (s - _) / 4, a.c = o.a = (a.b + o.b) / 2, [ r, n, a, o ];
        }, l = function(t, e, n, a, o) {
            var l, _, u, p, f, c, m, d, g, v, y, T, w, x = t.length - 1, b = 0, P = t[0].a;
            for (l = 0; x > l; l++) f = t[b], _ = f.a, u = f.d, p = t[b + 1].d, o ? (y = i[l], 
            T = s[l], w = .25 * (T + y) * e / (a ? .5 : r[l] || .5), c = u - (u - _) * (a ? .5 * e : 0 !== y ? w / y : 0), 
            m = u + (p - u) * (a ? .5 * e : 0 !== T ? w / T : 0), d = u - (c + ((m - c) * (3 * y / (y + T) + .5) / 4 || 0))) : (c = u - .5 * (u - _) * e, 
            m = u + .5 * (p - u) * e, d = u - (c + m) / 2), c += d, m += d, f.c = g = c, f.b = 0 !== l ? P : P = f.a + .6 * (f.c - f.a), 
            f.da = u - _, f.ca = g - _, f.ba = P - _, n ? (v = h(_, P, g, u), t.splice(b, 1, v[0], v[1], v[2], v[3]), 
            b += 4) : b++, P = m;
            f = t[b], f.b = P, f.c = P + .4 * (f.d - P), f.da = f.d - f.a, f.ca = f.c - f.a, 
            f.ba = P - f.a, n && (v = h(f.a, P, f.c, f.d), t.splice(b, 1, v[0], v[1], v[2], v[3]));
        }, _ = function(t, e, r, n) {
            var o, h, l, _, u, p, f = [];
            if (n) for (t = [ n ].concat(t), h = t.length; --h > -1; ) "string" == typeof (p = t[h][e]) && "=" === p.charAt(1) && (t[h][e] = n[e] + Number(p.charAt(0) + p.substr(2)));
            if (o = t.length - 2, 0 > o) return f[0] = new a(t[0][e], 0, 0, t[-1 > o ? 0 : 1][e]), 
            f;
            for (h = 0; o > h; h++) l = t[h][e], _ = t[h + 1][e], f[h] = new a(l, 0, 0, _), 
            r && (u = t[h + 2][e], i[h] = (i[h] || 0) + (_ - l) * (_ - l), s[h] = (s[h] || 0) + (u - _) * (u - _));
            return f[h] = new a(t[h][e], 0, 0, t[h + 1][e]), f;
        }, u = function(t, e, a, h, u, p) {
            var f, c, m, d, g, v, y, T, w = {}, x = [], b = p || t[0];
            u = "string" == typeof u ? "," + u + "," : o, null == e && (e = 1);
            for (c in t[0]) x.push(c);
            if (t.length > 1) {
                for (T = t[t.length - 1], y = !0, f = x.length; --f > -1; ) if (c = x[f], Math.abs(b[c] - T[c]) > .05) {
                    y = !1;
                    break;
                }
                y && (t = t.concat(), p && t.unshift(p), t.push(t[1]), p = t[t.length - 3]);
            }
            for (i.length = s.length = r.length = 0, f = x.length; --f > -1; ) c = x[f], n[c] = -1 !== u.indexOf("," + c + ","), 
            w[c] = _(t, c, n[c], p);
            for (f = i.length; --f > -1; ) i[f] = Math.sqrt(i[f]), s[f] = Math.sqrt(s[f]);
            if (!h) {
                for (f = x.length; --f > -1; ) if (n[c]) for (m = w[x[f]], v = m.length - 1, d = 0; v > d; d++) g = m[d + 1].da / s[d] + m[d].da / i[d], 
                r[d] = (r[d] || 0) + g * g;
                for (f = r.length; --f > -1; ) r[f] = Math.sqrt(r[f]);
            }
            for (f = x.length, d = a ? 4 : 1; --f > -1; ) c = x[f], m = w[c], l(m, e, a, h, n[c]), 
            y && (m.splice(0, d), m.splice(m.length - d, d));
            return w;
        }, p = function(t, e, i) {
            e = e || "soft";
            var s, r, n, o, h, l, _, u, p, f, c, m = {}, d = "cubic" === e ? 3 : 2, g = "soft" === e, v = [];
            if (g && i && (t = [ i ].concat(t)), null == t || d + 1 > t.length) throw "invalid Bezier data";
            for (p in t[0]) v.push(p);
            for (l = v.length; --l > -1; ) {
                for (p = v[l], m[p] = h = [], f = 0, u = t.length, _ = 0; u > _; _++) s = null == i ? t[_][p] : "string" == typeof (c = t[_][p]) && "=" === c.charAt(1) ? i[p] + Number(c.charAt(0) + c.substr(2)) : Number(c), 
                g && _ > 1 && u - 1 > _ && (h[f++] = (s + h[f - 2]) / 2), h[f++] = s;
                for (u = f - d + 1, f = 0, _ = 0; u > _; _ += d) s = h[_], r = h[_ + 1], n = h[_ + 2], 
                o = 2 === d ? 0 : h[_ + 3], h[f++] = c = 3 === d ? new a(s, r, n, o) : new a(s, (2 * r + s) / 3, (2 * r + n) / 3, n);
                h.length = f;
            }
            return m;
        }, f = function(t, e, i) {
            for (var s, r, n, a, o, h, l, _, u, p, f, c = 1 / i, m = t.length; --m > -1; ) for (p = t[m], 
            n = p.a, a = p.d - n, o = p.c - n, h = p.b - n, s = r = 0, _ = 1; i >= _; _++) l = c * _, 
            u = 1 - l, s = r - (r = (l * l * a + 3 * u * (l * o + u * h)) * l), f = m * i + _ - 1, 
            e[f] = (e[f] || 0) + s * s;
        }, c = function(t, e) {
            e = e >> 0 || 6;
            var i, s, r, n, a = [], o = [], h = 0, l = 0, _ = e - 1, u = [], p = [];
            for (i in t) f(t[i], a, e);
            for (r = a.length, s = 0; r > s; s++) h += Math.sqrt(a[s]), n = s % e, p[n] = h, 
            n === _ && (l += h, n = s / e >> 0, u[n] = p, o[n] = l, h = 0, p = []);
            return {
                length: l,
                lengths: o,
                segments: u
            };
        }, m = window._gsDefine.plugin({
            propName: "bezier",
            priority: -1,
            API: 2,
            global: !0,
            init: function(t, e, i) {
                this._target = t, e instanceof Array && (e = {
                    values: e
                }), this._func = {}, this._round = {}, this._props = [], this._timeRes = null == e.timeResolution ? 6 : parseInt(e.timeResolution, 10);
                var s, r, n, a, o, h = e.values || [], l = {}, _ = h[0], f = e.autoRotate || i.vars.orientToBezier;
                this._autoRotate = f ? f instanceof Array ? f : [ [ "x", "y", "rotation", f === !0 ? 0 : Number(f) || 0 ] ] : null;
                for (s in _) this._props.push(s);
                for (n = this._props.length; --n > -1; ) s = this._props[n], this._overwriteProps.push(s), 
                r = this._func[s] = "function" == typeof t[s], l[s] = r ? t[s.indexOf("set") || "function" != typeof t["get" + s.substr(3)] ? s : "get" + s.substr(3)]() : parseFloat(t[s]), 
                o || l[s] !== h[0][s] && (o = l);
                if (this._beziers = "cubic" !== e.type && "quadratic" !== e.type && "soft" !== e.type ? u(h, isNaN(e.curviness) ? 1 : e.curviness, !1, "thruBasic" === e.type, e.correlate, o) : p(h, e.type, l), 
                this._segCount = this._beziers[s].length, this._timeRes) {
                    var m = c(this._beziers, this._timeRes);
                    this._length = m.length, this._lengths = m.lengths, this._segments = m.segments, 
                    this._l1 = this._li = this._s1 = this._si = 0, this._l2 = this._lengths[0], this._curSeg = this._segments[0], 
                    this._s2 = this._curSeg[0], this._prec = 1 / this._curSeg.length;
                }
                if (f = this._autoRotate) for (f[0] instanceof Array || (this._autoRotate = f = [ f ]), 
                n = f.length; --n > -1; ) for (a = 0; 3 > a; a++) s = f[n][a], this._func[s] = "function" == typeof t[s] ? t[s.indexOf("set") || "function" != typeof t["get" + s.substr(3)] ? s : "get" + s.substr(3)] : !1;
                return !0;
            },
            set: function(e) {
                var i, s, r, n, a, o, h, l, _, u, p = this._segCount, f = this._func, c = this._target;
                if (this._timeRes) {
                    if (_ = this._lengths, u = this._curSeg, e *= this._length, r = this._li, e > this._l2 && p - 1 > r) {
                        for (l = p - 1; l > r && e >= (this._l2 = _[++r]); ) ;
                        this._l1 = _[r - 1], this._li = r, this._curSeg = u = this._segments[r], this._s2 = u[this._s1 = this._si = 0];
                    } else if (this._l1 > e && r > 0) {
                        for (;r > 0 && (this._l1 = _[--r]) >= e; ) ;
                        0 === r && this._l1 > e ? this._l1 = 0 : r++, this._l2 = _[r], this._li = r, this._curSeg = u = this._segments[r], 
                        this._s1 = u[(this._si = u.length - 1) - 1] || 0, this._s2 = u[this._si];
                    }
                    if (i = r, e -= this._l1, r = this._si, e > this._s2 && u.length - 1 > r) {
                        for (l = u.length - 1; l > r && e >= (this._s2 = u[++r]); ) ;
                        this._s1 = u[r - 1], this._si = r;
                    } else if (this._s1 > e && r > 0) {
                        for (;r > 0 && (this._s1 = u[--r]) >= e; ) ;
                        0 === r && this._s1 > e ? this._s1 = 0 : r++, this._s2 = u[r], this._si = r;
                    }
                    o = (r + (e - this._s1) / (this._s2 - this._s1)) * this._prec;
                } else i = 0 > e ? 0 : e >= 1 ? p - 1 : p * e >> 0, o = (e - i * (1 / p)) * p;
                for (s = 1 - o, r = this._props.length; --r > -1; ) n = this._props[r], a = this._beziers[n][i], 
                h = (o * o * a.da + 3 * s * (o * a.ca + s * a.ba)) * o + a.a, this._round[n] && (h = h + (h > 0 ? .5 : -.5) >> 0), 
                f[n] ? c[n](h) : c[n] = h;
                if (this._autoRotate) {
                    var m, d, g, v, y, T, w, x = this._autoRotate;
                    for (r = x.length; --r > -1; ) n = x[r][2], T = x[r][3] || 0, w = x[r][4] === !0 ? 1 : t, 
                    a = this._beziers[x[r][0]], m = this._beziers[x[r][1]], a && m && (a = a[i], m = m[i], 
                    d = a.a + (a.b - a.a) * o, v = a.b + (a.c - a.b) * o, d += (v - d) * o, v += (a.c + (a.d - a.c) * o - v) * o, 
                    g = m.a + (m.b - m.a) * o, y = m.b + (m.c - m.b) * o, g += (y - g) * o, y += (m.c + (m.d - m.c) * o - y) * o, 
                    h = Math.atan2(y - g, v - d) * w + T, f[n] ? c[n](h) : c[n] = h);
                }
            }
        }), d = m.prototype;
        m.bezierThrough = u, m.cubicToQuadratic = h, m._autoCSS = !0, m.quadraticToCubic = function(t, e, i) {
            return new a(t, (2 * e + t) / 3, (2 * e + i) / 3, i);
        }, m._cssRegister = function() {
            var t = window._gsDefine.globals.CSSPlugin;
            if (t) {
                var i = t._internals, s = i._parseToProxy, r = i._setPluginRatio, n = i.CSSPropTween;
                i._registerComplexSpecialProp("bezier", {
                    parser: function(t, i, a, o, h, l) {
                        i instanceof Array && (i = {
                            values: i
                        }), l = new m();
                        var _, u, p, f = i.values, c = f.length - 1, d = [], g = {};
                        if (0 > c) return h;
                        for (_ = 0; c >= _; _++) p = s(t, f[_], o, h, l, c !== _), d[_] = p.end;
                        for (u in i) g[u] = i[u];
                        return g.values = d, h = new n(t, "bezier", 0, 0, p.pt, 2), h.data = p, h.plugin = l, 
                        h.setRatio = r, 0 === g.autoRotate && (g.autoRotate = !0), !g.autoRotate || g.autoRotate instanceof Array || (_ = g.autoRotate === !0 ? 0 : Number(g.autoRotate) * e, 
                        g.autoRotate = null != p.end.left ? [ [ "left", "top", "rotation", _, !0 ] ] : null != p.end.x ? [ [ "x", "y", "rotation", _, !0 ] ] : !1), 
                        g.autoRotate && (o._transform || o._enableTransforms(!1), p.autoRotate = o._target._gsTransform), 
                        l._onInitTween(p.proxy, g, o._tween), h;
                    }
                });
            }
        }, d._roundProps = function(t, e) {
            for (var i = this._overwriteProps, s = i.length; --s > -1; ) (t[i[s]] || t.bezier || t.bezierThrough) && (this._round[i[s]] = e);
        }, d._kill = function(t) {
            var e, i, s = this._props;
            for (e in this._beziers) if (e in t) for (delete this._beziers[e], delete this._func[e], 
            i = s.length; --i > -1; ) s[i] === e && s.splice(i, 1);
            return this._super._kill.call(this, t);
        };
    }(), window._gsDefine("plugins.CSSPlugin", [ "plugins.TweenPlugin", "TweenLite" ], function(t, e) {
        var i, s, r, n, a = function() {
            t.call(this, "css"), this._overwriteProps.length = 0, this.setRatio = a.prototype.setRatio;
        }, o = {}, h = a.prototype = new t("css");
        h.constructor = a, a.version = "1.10.3", a.API = 2, a.defaultTransformPerspective = 0, 
        h = "px", a.suffixMap = {
            top: h,
            right: h,
            bottom: h,
            left: h,
            width: h,
            height: h,
            fontSize: h,
            padding: h,
            margin: h,
            perspective: h
        };
        var l, _, u, p, f, c, m = /(?:\d|\-\d|\.\d|\-\.\d)+/g, d = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g, g = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi, v = /[^\d\-\.]/g, y = /(?:\d|\-|\+|=|#|\.)*/g, T = /opacity *= *([^)]*)/, w = /opacity:([^;]*)/, x = /alpha\(opacity *=.+?\)/i, b = /^(rgb|hsl)/, P = /([A-Z])/g, k = /-([a-z])/gi, S = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi, R = function(t, e) {
            return e.toUpperCase();
        }, A = /(?:Left|Right|Width)/i, C = /(M11|M12|M21|M22)=[\d\-\.e]+/gi, O = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i, D = /,(?=[^\)]*(?:\(|$))/gi, M = Math.PI / 180, I = 180 / Math.PI, F = {}, E = document, N = E.createElement("div"), L = E.createElement("img"), X = a._internals = {
            _specialProps: o
        }, z = navigator.userAgent, U = function() {
            var t, e = z.indexOf("Android"), i = E.createElement("div");
            return u = -1 !== z.indexOf("Safari") && -1 === z.indexOf("Chrome") && (-1 === e || Number(z.substr(e + 8, 1)) > 3), 
            f = u && 6 > Number(z.substr(z.indexOf("Version/") + 8, 1)), p = -1 !== z.indexOf("Firefox"), 
            /MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(z), c = parseFloat(RegExp.$1), i.innerHTML = "<a style='top:1px;opacity:.55;'>a</a>", 
            t = i.getElementsByTagName("a")[0], t ? /^0.55/.test(t.style.opacity) : !1;
        }(), Y = function(t) {
            return T.test("string" == typeof t ? t : (t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? parseFloat(RegExp.$1) / 100 : 1;
        }, j = function(t) {
            window.console && console.log(t);
        }, B = "", q = "", V = function(t, e) {
            e = e || N;
            var i, s, r = e.style;
            if (void 0 !== r[t]) return t;
            for (t = t.charAt(0).toUpperCase() + t.substr(1), i = [ "O", "Moz", "ms", "Ms", "Webkit" ], 
            s = 5; --s > -1 && void 0 === r[i[s] + t]; ) ;
            return s >= 0 ? (q = 3 === s ? "ms" : i[s], B = "-" + q.toLowerCase() + "-", q + t) : null;
        }, Z = E.defaultView ? E.defaultView.getComputedStyle : function() {}, G = a.getStyle = function(t, e, i, s, r) {
            var n;
            return U || "opacity" !== e ? (!s && t.style[e] ? n = t.style[e] : (i = i || Z(t, null)) ? (t = i.getPropertyValue(e.replace(P, "-$1").toLowerCase()), 
            n = t || i.length ? t : i[e]) : t.currentStyle && (n = t.currentStyle[e]), null == r || n && "none" !== n && "auto" !== n && "auto auto" !== n ? n : r) : Y(t);
        }, $ = function(t, e, i, s, r) {
            if ("px" === s || !s) return i;
            if ("auto" === s || !i) return 0;
            var n, a = A.test(e), o = t, h = N.style, l = 0 > i;
            return l && (i = -i), "%" === s && -1 !== e.indexOf("border") ? n = i / 100 * (a ? t.clientWidth : t.clientHeight) : (h.cssText = "border-style:solid;border-width:0;position:absolute;line-height:0;", 
            "%" !== s && o.appendChild ? h[a ? "borderLeftWidth" : "borderTopWidth"] = i + s : (o = t.parentNode || E.body, 
            h[a ? "width" : "height"] = i + s), o.appendChild(N), n = parseFloat(N[a ? "offsetWidth" : "offsetHeight"]), 
            o.removeChild(N), 0 !== n || r || (n = $(t, e, i, s, !0))), l ? -n : n;
        }, Q = function(t, e, i) {
            if ("absolute" !== G(t, "position", i)) return 0;
            var s = "left" === e ? "Left" : "Top", r = G(t, "margin" + s, i);
            return t["offset" + s] - ($(t, e, parseFloat(r), r.replace(y, "")) || 0);
        }, W = function(t, e) {
            var i, s, r = {};
            if (e = e || Z(t, null)) if (i = e.length) for (;--i > -1; ) r[e[i].replace(k, R)] = e.getPropertyValue(e[i]); else for (i in e) r[i] = e[i]; else if (e = t.currentStyle || t.style) for (i in e) r[i.replace(k, R)] = e[i];
            return U || (r.opacity = Y(t)), s = be(t, e, !1), r.rotation = s.rotation * I, r.skewX = s.skewX * I, 
            r.scaleX = s.scaleX, r.scaleY = s.scaleY, r.x = s.x, r.y = s.y, xe && (r.z = s.z, 
            r.rotationX = s.rotationX * I, r.rotationY = s.rotationY * I, r.scaleZ = s.scaleZ), 
            r.filters && delete r.filters, r;
        }, H = function(t, e, i, s, r) {
            var n, a, o, h = {}, l = t.style;
            for (a in i) "cssText" !== a && "length" !== a && isNaN(a) && (e[a] !== (n = i[a]) || r && r[a]) && -1 === a.indexOf("Origin") && ("number" == typeof n || "string" == typeof n) && (h[a] = "auto" !== n || "left" !== a && "top" !== a ? "" !== n && "auto" !== n && "none" !== n || "string" != typeof e[a] || "" === e[a].replace(v, "") ? n : 0 : Q(t, a), 
            void 0 !== l[a] && (o = new ue(l, a, l[a], o)));
            if (s) for (a in s) "className" !== a && (h[a] = s[a]);
            return {
                difs: h,
                firstMPT: o
            };
        }, K = {
            width: [ "Left", "Right" ],
            height: [ "Top", "Bottom" ]
        }, J = [ "marginLeft", "marginRight", "marginTop", "marginBottom" ], te = function(t, e, i) {
            var s = parseFloat("width" === e ? t.offsetWidth : t.offsetHeight), r = K[e], n = r.length;
            for (i = i || Z(t, null); --n > -1; ) s -= parseFloat(G(t, "padding" + r[n], i, !0)) || 0, 
            s -= parseFloat(G(t, "border" + r[n] + "Width", i, !0)) || 0;
            return s;
        }, ee = function(t, e) {
            (null == t || "" === t || "auto" === t || "auto auto" === t) && (t = "0 0");
            var i = t.split(" "), s = -1 !== t.indexOf("left") ? "0%" : -1 !== t.indexOf("right") ? "100%" : i[0], r = -1 !== t.indexOf("top") ? "0%" : -1 !== t.indexOf("bottom") ? "100%" : i[1];
            return null == r ? r = "0" : "center" === r && (r = "50%"), ("center" === s || isNaN(parseFloat(s)) && -1 === (s + "").indexOf("=")) && (s = "50%"), 
            e && (e.oxp = -1 !== s.indexOf("%"), e.oyp = -1 !== r.indexOf("%"), e.oxr = "=" === s.charAt(1), 
            e.oyr = "=" === r.charAt(1), e.ox = parseFloat(s.replace(v, "")), e.oy = parseFloat(r.replace(v, ""))), 
            s + " " + r + (i.length > 2 ? " " + i[2] : "");
        }, ie = function(t, e) {
            return "string" == typeof t && "=" === t.charAt(1) ? parseInt(t.charAt(0) + "1", 10) * parseFloat(t.substr(2)) : parseFloat(t) - parseFloat(e);
        }, se = function(t, e) {
            return null == t ? e : "string" == typeof t && "=" === t.charAt(1) ? parseInt(t.charAt(0) + "1", 10) * Number(t.substr(2)) + e : parseFloat(t);
        }, re = function(t, e, i, s) {
            var r, n, a, o, h = 1e-6;
            return null == t ? o = e : "number" == typeof t ? o = t * M : (r = 2 * Math.PI, 
            n = t.split("_"), a = Number(n[0].replace(v, "")) * (-1 === t.indexOf("rad") ? M : 1) - ("=" === t.charAt(1) ? 0 : e), 
            n.length && (s && (s[i] = e + a), -1 !== t.indexOf("short") && (a %= r, a !== a % (r / 2) && (a = 0 > a ? a + r : a - r)), 
            -1 !== t.indexOf("_cw") && 0 > a ? a = (a + 9999999999 * r) % r - (0 | a / r) * r : -1 !== t.indexOf("ccw") && a > 0 && (a = (a - 9999999999 * r) % r - (0 | a / r) * r)), 
            o = e + a), h > o && o > -h && (o = 0), o;
        }, ne = {
            aqua: [ 0, 255, 255 ],
            lime: [ 0, 255, 0 ],
            silver: [ 192, 192, 192 ],
            black: [ 0, 0, 0 ],
            maroon: [ 128, 0, 0 ],
            teal: [ 0, 128, 128 ],
            blue: [ 0, 0, 255 ],
            navy: [ 0, 0, 128 ],
            white: [ 255, 255, 255 ],
            fuchsia: [ 255, 0, 255 ],
            olive: [ 128, 128, 0 ],
            yellow: [ 255, 255, 0 ],
            orange: [ 255, 165, 0 ],
            gray: [ 128, 128, 128 ],
            purple: [ 128, 0, 128 ],
            green: [ 0, 128, 0 ],
            red: [ 255, 0, 0 ],
            pink: [ 255, 192, 203 ],
            cyan: [ 0, 255, 255 ],
            transparent: [ 255, 255, 255, 0 ]
        }, ae = function(t, e, i) {
            return t = 0 > t ? t + 1 : t > 1 ? t - 1 : t, 0 | 255 * (1 > 6 * t ? e + 6 * (i - e) * t : .5 > t ? i : 2 > 3 * t ? e + 6 * (i - e) * (2 / 3 - t) : e) + .5;
        }, oe = function(t) {
            var e, i, s, r, n, a;
            return t && "" !== t ? "number" == typeof t ? [ t >> 16, 255 & t >> 8, 255 & t ] : ("," === t.charAt(t.length - 1) && (t = t.substr(0, t.length - 1)), 
            ne[t] ? ne[t] : "#" === t.charAt(0) ? (4 === t.length && (e = t.charAt(1), i = t.charAt(2), 
            s = t.charAt(3), t = "#" + e + e + i + i + s + s), t = parseInt(t.substr(1), 16), 
            [ t >> 16, 255 & t >> 8, 255 & t ]) : "hsl" === t.substr(0, 3) ? (t = t.match(m), 
            r = Number(t[0]) % 360 / 360, n = Number(t[1]) / 100, a = Number(t[2]) / 100, i = .5 >= a ? a * (n + 1) : a + n - a * n, 
            e = 2 * a - i, t.length > 3 && (t[3] = Number(t[3])), t[0] = ae(r + 1 / 3, e, i), 
            t[1] = ae(r, e, i), t[2] = ae(r - 1 / 3, e, i), t) : (t = t.match(m) || ne.transparent, 
            t[0] = Number(t[0]), t[1] = Number(t[1]), t[2] = Number(t[2]), t.length > 3 && (t[3] = Number(t[3])), 
            t)) : ne.black;
        }, he = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#.+?\\b";
        for (h in ne) he += "|" + h + "\\b";
        he = RegExp(he + ")", "gi");
        var le = function(t, e, i, s) {
            if (null == t) return function(t) {
                return t;
            };
            var r, n = e ? (t.match(he) || [ "" ])[0] : "", a = t.split(n).join("").match(g) || [], o = t.substr(0, t.indexOf(a[0])), h = ")" === t.charAt(t.length - 1) ? ")" : "", l = -1 !== t.indexOf(" ") ? " " : ",", _ = a.length, u = _ > 0 ? a[0].replace(m, "") : "";
            return _ ? r = e ? function(t) {
                var e, p, f, c;
                if ("number" == typeof t) t += u; else if (s && D.test(t)) {
                    for (c = t.replace(D, "|").split("|"), f = 0; c.length > f; f++) c[f] = r(c[f]);
                    return c.join(",");
                }
                if (e = (t.match(he) || [ n ])[0], p = t.split(e).join("").match(g) || [], f = p.length, 
                _ > f--) for (;_ > ++f; ) p[f] = i ? p[0 | (f - 1) / 2] : a[f];
                return o + p.join(l) + l + e + h + (-1 !== t.indexOf("inset") ? " inset" : "");
            } : function(t) {
                var e, n, p;
                if ("number" == typeof t) t += u; else if (s && D.test(t)) {
                    for (n = t.replace(D, "|").split("|"), p = 0; n.length > p; p++) n[p] = r(n[p]);
                    return n.join(",");
                }
                if (e = t.match(g) || [], p = e.length, _ > p--) for (;_ > ++p; ) e[p] = i ? e[0 | (p - 1) / 2] : a[p];
                return o + e.join(l) + h;
            } : function(t) {
                return t;
            };
        }, _e = function(t) {
            return t = t.split(","), function(e, i, s, r, n, a, o) {
                var h, l = (i + "").split(" ");
                for (o = {}, h = 0; 4 > h; h++) o[t[h]] = l[h] = l[h] || l[(h - 1) / 2 >> 0];
                return r.parse(e, o, n, a);
            };
        }, ue = (X._setPluginRatio = function(t) {
            this.plugin.setRatio(t);
            for (var e, i, s, r, n = this.data, a = n.proxy, o = n.firstMPT, h = 1e-6; o; ) e = a[o.v], 
            o.r ? e = e > 0 ? 0 | e + .5 : 0 | e - .5 : h > e && e > -h && (e = 0), o.t[o.p] = e, 
            o = o._next;
            if (n.autoRotate && (n.autoRotate.rotation = a.rotation), 1 === t) for (o = n.firstMPT; o; ) {
                if (i = o.t, i.type) {
                    if (1 === i.type) {
                        for (r = i.xs0 + i.s + i.xs1, s = 1; i.l > s; s++) r += i["xn" + s] + i["xs" + (s + 1)];
                        i.e = r;
                    }
                } else i.e = i.s + i.xs0;
                o = o._next;
            }
        }, function(t, e, i, s, r) {
            this.t = t, this.p = e, this.v = i, this.r = r, s && (s._prev = this, this._next = s);
        }), pe = (X._parseToProxy = function(t, e, i, s, r, n) {
            var a, o, h, l, _, u = s, p = {}, f = {}, c = i._transform, m = F;
            for (i._transform = null, F = e, s = _ = i.parse(t, e, s, r), F = m, n && (i._transform = c, 
            u && (u._prev = null, u._prev && (u._prev._next = null))); s && s !== u; ) {
                if (1 >= s.type && (o = s.p, f[o] = s.s + s.c, p[o] = s.s, n || (l = new ue(s, "s", o, l, s.r), 
                s.c = 0), 1 === s.type)) for (a = s.l; --a > 0; ) h = "xn" + a, o = s.p + "_" + h, 
                f[o] = s.data[h], p[o] = s[h], n || (l = new ue(s, h, o, l, s.rxp[h]));
                s = s._next;
            }
            return {
                proxy: p,
                end: f,
                firstMPT: l,
                pt: _
            };
        }, X.CSSPropTween = function(t, e, s, r, a, o, h, l, _, u, p) {
            this.t = t, this.p = e, this.s = s, this.c = r, this.n = h || e, t instanceof pe || n.push(this.n), 
            this.r = l, this.type = o || 0, _ && (this.pr = _, i = !0), this.b = void 0 === u ? s : u, 
            this.e = void 0 === p ? s + r : p, a && (this._next = a, a._prev = this);
        }), fe = a.parseComplex = function(t, e, i, s, r, n, a, o, h, _) {
            i = i || n || "", a = new pe(t, e, 0, 0, a, _ ? 2 : 1, null, !1, o, i, s), s += "";
            var u, p, f, c, g, v, y, T, w, x, P, k, S = i.split(", ").join(",").split(" "), R = s.split(", ").join(",").split(" "), A = S.length, C = l !== !1;
            for ((-1 !== s.indexOf(",") || -1 !== i.indexOf(",")) && (S = S.join(" ").replace(D, ", ").split(" "), 
            R = R.join(" ").replace(D, ", ").split(" "), A = S.length), A !== R.length && (S = (n || "").split(" "), 
            A = S.length), a.plugin = h, a.setRatio = _, u = 0; A > u; u++) if (c = S[u], g = R[u], 
            T = parseFloat(c), T || 0 === T) a.appendXtra("", T, ie(g, T), g.replace(d, ""), C && -1 !== g.indexOf("px"), !0); else if (r && ("#" === c.charAt(0) || ne[c] || b.test(c))) k = "," === g.charAt(g.length - 1) ? ")," : ")", 
            c = oe(c), g = oe(g), w = c.length + g.length > 6, w && !U && 0 === g[3] ? (a["xs" + a.l] += a.l ? " transparent" : "transparent", 
            a.e = a.e.split(R[u]).join("transparent")) : (U || (w = !1), a.appendXtra(w ? "rgba(" : "rgb(", c[0], g[0] - c[0], ",", !0, !0).appendXtra("", c[1], g[1] - c[1], ",", !0).appendXtra("", c[2], g[2] - c[2], w ? "," : k, !0), 
            w && (c = 4 > c.length ? 1 : c[3], a.appendXtra("", c, (4 > g.length ? 1 : g[3]) - c, k, !1))); else if (v = c.match(m)) {
                if (y = g.match(d), !y || y.length !== v.length) return a;
                for (f = 0, p = 0; v.length > p; p++) P = v[p], x = c.indexOf(P, f), a.appendXtra(c.substr(f, x - f), Number(P), ie(y[p], P), "", C && "px" === c.substr(x + P.length, 2), 0 === p), 
                f = x + P.length;
                a["xs" + a.l] += c.substr(f);
            } else a["xs" + a.l] += a.l ? " " + c : c;
            if (-1 !== s.indexOf("=") && a.data) {
                for (k = a.xs0 + a.data.s, u = 1; a.l > u; u++) k += a["xs" + u] + a.data["xn" + u];
                a.e = k + a["xs" + u];
            }
            return a.l || (a.type = -1, a.xs0 = a.e), a.xfirst || a;
        }, ce = 9;
        for (h = pe.prototype, h.l = h.pr = 0; --ce > 0; ) h["xn" + ce] = 0, h["xs" + ce] = "";
        h.xs0 = "", h._next = h._prev = h.xfirst = h.data = h.plugin = h.setRatio = h.rxp = null, 
        h.appendXtra = function(t, e, i, s, r, n) {
            var a = this, o = a.l;
            return a["xs" + o] += n && o ? " " + t : t || "", i || 0 === o || a.plugin ? (a.l++, 
            a.type = a.setRatio ? 2 : 1, a["xs" + a.l] = s || "", o > 0 ? (a.data["xn" + o] = e + i, 
            a.rxp["xn" + o] = r, a["xn" + o] = e, a.plugin || (a.xfirst = new pe(a, "xn" + o, e, i, a.xfirst || a, 0, a.n, r, a.pr), 
            a.xfirst.xs0 = 0), a) : (a.data = {
                s: e + i
            }, a.rxp = {}, a.s = e, a.c = i, a.r = r, a)) : (a["xs" + o] += e + (s || ""), a);
        };
        var me = function(t, e) {
            e = e || {}, this.p = e.prefix ? V(t) || t : t, o[t] = o[this.p] = this, this.format = e.formatter || le(e.defaultValue, e.color, e.collapsible, e.multi), 
            e.parser && (this.parse = e.parser), this.clrs = e.color, this.multi = e.multi, 
            this.keyword = e.keyword, this.dflt = e.defaultValue, this.pr = e.priority || 0;
        }, de = X._registerComplexSpecialProp = function(t, e, i) {
            "object" != typeof e && (e = {
                parser: i
            });
            var s, r, n = t.split(","), a = e.defaultValue;
            for (i = i || [ a ], s = 0; n.length > s; s++) e.prefix = 0 === s && e.prefix, e.defaultValue = i[s] || a, 
            r = new me(n[s], e);
        }, ge = function(t) {
            if (!o[t]) {
                var e = t.charAt(0).toUpperCase() + t.substr(1) + "Plugin";
                de(t, {
                    parser: function(t, i, s, r, n, a, h) {
                        var l = (window.GreenSockGlobals || window).com.greensock.plugins[e];
                        return l ? (l._cssRegister(), o[s].parse(t, i, s, r, n, a, h)) : (j("Error: " + e + " js file not loaded."), 
                        n);
                    }
                });
            }
        };
        h = me.prototype, h.parseComplex = function(t, e, i, s, r, n) {
            var a, o, h, l, _, u, p = this.keyword;
            if (this.multi && (D.test(i) || D.test(e) ? (o = e.replace(D, "|").split("|"), h = i.replace(D, "|").split("|")) : p && (o = [ e ], 
            h = [ i ])), h) {
                for (l = h.length > o.length ? h.length : o.length, a = 0; l > a; a++) e = o[a] = o[a] || this.dflt, 
                i = h[a] = h[a] || this.dflt, p && (_ = e.indexOf(p), u = i.indexOf(p), _ !== u && (i = -1 === u ? h : o, 
                i[a] += " " + p));
                e = o.join(", "), i = h.join(", ");
            }
            return fe(t, this.p, e, i, this.clrs, this.dflt, s, this.pr, r, n);
        }, h.parse = function(t, e, i, s, n, a) {
            return this.parseComplex(t.style, this.format(G(t, this.p, r, !1, this.dflt)), this.format(e), n, a);
        }, a.registerSpecialProp = function(t, e, i) {
            de(t, {
                parser: function(t, s, r, n, a, o) {
                    var h = new pe(t, r, 0, 0, a, 2, r, !1, i);
                    return h.plugin = o, h.setRatio = e(t, s, n._tween, r), h;
                },
                priority: i
            });
        };
        var ve = "scaleX,scaleY,scaleZ,x,y,z,skewX,rotation,rotationX,rotationY,perspective".split(","), ye = V("transform"), Te = B + "transform", we = V("transformOrigin"), xe = null !== V("perspective"), be = function(t, e, i, s) {
            if (t._gsTransform && i && !s) return t._gsTransform;
            var r, n, o, h, l, _, u, p, f, c, m, d, g, v = i ? t._gsTransform || {
                skewY: 0
            } : {
                skewY: 0
            }, y = 0 > v.scaleX, T = 2e-5, w = 1e5, x = -Math.PI + 1e-4, b = Math.PI - 1e-4, P = xe ? parseFloat(G(t, we, e, !1, "0 0 0").split(" ")[2]) || v.zOrigin || 0 : 0;
            for (ye ? r = G(t, Te, e, !0) : t.currentStyle && (r = t.currentStyle.filter.match(C), 
            r = r && 4 === r.length ? [ r[0].substr(4), Number(r[2].substr(4)), Number(r[1].substr(4)), r[3].substr(4), v.x || 0, v.y || 0 ].join(",") : ""), 
            n = (r || "").match(/(?:\-|\b)[\d\-\.e]+\b/gi) || [], o = n.length; --o > -1; ) h = Number(n[o]), 
            n[o] = (l = h - (h |= 0)) ? (0 | l * w + (0 > l ? -.5 : .5)) / w + h : h;
            if (16 === n.length) {
                var k = n[8], S = n[9], R = n[10], A = n[12], O = n[13], D = n[14];
                if (v.zOrigin && (D = -v.zOrigin, A = k * D - n[12], O = S * D - n[13], D = R * D + v.zOrigin - n[14]), 
                !i || s || null == v.rotationX) {
                    var M, I, F, E, N, L, X, z = n[0], U = n[1], Y = n[2], j = n[3], B = n[4], q = n[5], V = n[6], Z = n[7], $ = n[11], Q = v.rotationX = Math.atan2(V, R), W = x > Q || Q > b;
                    Q && (E = Math.cos(-Q), N = Math.sin(-Q), M = B * E + k * N, I = q * E + S * N, 
                    F = V * E + R * N, k = B * -N + k * E, S = q * -N + S * E, R = V * -N + R * E, $ = Z * -N + $ * E, 
                    B = M, q = I, V = F), Q = v.rotationY = Math.atan2(k, z), Q && (L = x > Q || Q > b, 
                    E = Math.cos(-Q), N = Math.sin(-Q), M = z * E - k * N, I = U * E - S * N, F = Y * E - R * N, 
                    S = U * N + S * E, R = Y * N + R * E, $ = j * N + $ * E, z = M, U = I, Y = F), Q = v.rotation = Math.atan2(U, q), 
                    Q && (X = x > Q || Q > b, E = Math.cos(-Q), N = Math.sin(-Q), z = z * E + B * N, 
                    I = U * E + q * N, q = U * -N + q * E, V = Y * -N + V * E, U = I), X && W ? v.rotation = v.rotationX = 0 : X && L ? v.rotation = v.rotationY = 0 : L && W && (v.rotationY = v.rotationX = 0), 
                    v.scaleX = (0 | Math.sqrt(z * z + U * U) * w + .5) / w, v.scaleY = (0 | Math.sqrt(q * q + S * S) * w + .5) / w, 
                    v.scaleZ = (0 | Math.sqrt(V * V + R * R) * w + .5) / w, v.skewX = 0, v.perspective = $ ? 1 / (0 > $ ? -$ : $) : 0, 
                    v.x = A, v.y = O, v.z = D;
                }
            } else if (!(xe && !s && n.length && v.x === n[4] && v.y === n[5] && (v.rotationX || v.rotationY) || void 0 !== v.x && "none" === G(t, "display", e))) {
                var H = n.length >= 6, K = H ? n[0] : 1, J = n[1] || 0, te = n[2] || 0, ee = H ? n[3] : 1;
                v.x = n[4] || 0, v.y = n[5] || 0, _ = Math.sqrt(K * K + J * J), u = Math.sqrt(ee * ee + te * te), 
                p = K || J ? Math.atan2(J, K) : v.rotation || 0, f = te || ee ? Math.atan2(te, ee) + p : v.skewX || 0, 
                c = _ - Math.abs(v.scaleX || 0), m = u - Math.abs(v.scaleY || 0), Math.abs(f) > Math.PI / 2 && Math.abs(f) < 1.5 * Math.PI && (y ? (_ *= -1, 
                f += 0 >= p ? Math.PI : -Math.PI, p += 0 >= p ? Math.PI : -Math.PI) : (u *= -1, 
                f += 0 >= f ? Math.PI : -Math.PI)), d = (p - v.rotation) % Math.PI, g = (f - v.skewX) % Math.PI, 
                (void 0 === v.skewX || c > T || -T > c || m > T || -T > m || d > x && b > d && false | d * w || g > x && b > g && false | g * w) && (v.scaleX = _, 
                v.scaleY = u, v.rotation = p, v.skewX = f), xe && (v.rotationX = v.rotationY = v.z = 0, 
                v.perspective = parseFloat(a.defaultTransformPerspective) || 0, v.scaleZ = 1);
            }
            v.zOrigin = P;
            for (o in v) T > v[o] && v[o] > -T && (v[o] = 0);
            return i && (t._gsTransform = v), v;
        }, Pe = function(t) {
            var e, i, s = this.data, r = -s.rotation, n = r + s.skewX, a = 1e5, o = (0 | Math.cos(r) * s.scaleX * a) / a, h = (0 | Math.sin(r) * s.scaleX * a) / a, l = (0 | Math.sin(n) * -s.scaleY * a) / a, _ = (0 | Math.cos(n) * s.scaleY * a) / a, u = this.t.style, p = this.t.currentStyle;
            if (p) {
                i = h, h = -l, l = -i, e = p.filter, u.filter = "";
                var f, m, d = this.t.offsetWidth, g = this.t.offsetHeight, v = "absolute" !== p.position, w = "progid:DXImageTransform.Microsoft.Matrix(M11=" + o + ", M12=" + h + ", M21=" + l + ", M22=" + _, x = s.x, b = s.y;
                if (null != s.ox && (f = (s.oxp ? .01 * d * s.ox : s.ox) - d / 2, m = (s.oyp ? .01 * g * s.oy : s.oy) - g / 2, 
                x += f - (f * o + m * h), b += m - (f * l + m * _)), v ? (f = d / 2, m = g / 2, 
                w += ", Dx=" + (f - (f * o + m * h) + x) + ", Dy=" + (m - (f * l + m * _) + b) + ")") : w += ", sizingMethod='auto expand')", 
                u.filter = -1 !== e.indexOf("DXImageTransform.Microsoft.Matrix(") ? e.replace(O, w) : w + " " + e, 
                (0 === t || 1 === t) && 1 === o && 0 === h && 0 === l && 1 === _ && (v && -1 === w.indexOf("Dx=0, Dy=0") || T.test(e) && 100 !== parseFloat(RegExp.$1) || -1 === e.indexOf("gradient(" && e.indexOf("Alpha")) && u.removeAttribute("filter")), 
                !v) {
                    var P, k, S, R = 8 > c ? 1 : -1;
                    for (f = s.ieOffsetX || 0, m = s.ieOffsetY || 0, s.ieOffsetX = Math.round((d - ((0 > o ? -o : o) * d + (0 > h ? -h : h) * g)) / 2 + x), 
                    s.ieOffsetY = Math.round((g - ((0 > _ ? -_ : _) * g + (0 > l ? -l : l) * d)) / 2 + b), 
                    ce = 0; 4 > ce; ce++) k = J[ce], P = p[k], i = -1 !== P.indexOf("px") ? parseFloat(P) : $(this.t, k, parseFloat(P), P.replace(y, "")) || 0, 
                    S = i !== s[k] ? 2 > ce ? -s.ieOffsetX : -s.ieOffsetY : 2 > ce ? f - s.ieOffsetX : m - s.ieOffsetY, 
                    u[k] = (s[k] = Math.round(i - S * (0 === ce || 2 === ce ? 1 : R))) + "px";
                }
            }
        }, ke = function() {
            var t, e, i, s, r, n, a, o, h, l, _, u, f, c, m, d, g, v, y, T, w, x, b, P, k, S, R = this.data, A = this.t.style, C = R.rotation, O = R.scaleX, D = R.scaleY, M = R.scaleZ, I = R.perspective;
            if (p && (P = A.top ? "top" : A.bottom ? "bottom" : parseFloat(G(this.t, "top", null, !1)) ? "bottom" : "top", 
            T = G(this.t, P, null, !1), k = parseFloat(T) || 0, S = T.substr((k + "").length) || "px", 
            R._ffFix = !R._ffFix, A[P] = (R._ffFix ? k + .05 : k - .05) + S), C || R.skewX) v = Math.cos(C), 
            y = Math.sin(C), t = v, r = y, R.skewX && (C -= R.skewX, v = Math.cos(C), y = Math.sin(C)), 
            e = -y, n = v; else {
                if (!(R.rotationY || R.rotationX || 1 !== M || I)) return A[ye] = "translate3d(" + R.x + "px," + R.y + "px," + R.z + "px)" + (1 !== O || 1 !== D ? " scale(" + O + "," + D + ")" : ""), 
                void 0;
                t = n = 1, e = r = 0;
            }
            _ = 1, i = s = a = o = h = l = u = f = c = 0, m = I ? -1 / I : 0, d = R.zOrigin, 
            g = 1e5, C = R.rotationY, C && (v = Math.cos(C), y = Math.sin(C), h = _ * -y, f = m * -y, 
            i = t * y, a = r * y, _ *= v, m *= v, t *= v, r *= v), C = R.rotationX, C && (v = Math.cos(C), 
            y = Math.sin(C), T = e * v + i * y, w = n * v + a * y, x = l * v + _ * y, b = c * v + m * y, 
            i = e * -y + i * v, a = n * -y + a * v, _ = l * -y + _ * v, m = c * -y + m * v, 
            e = T, n = w, l = x, c = b), 1 !== M && (i *= M, a *= M, _ *= M, m *= M), 1 !== D && (e *= D, 
            n *= D, l *= D, c *= D), 1 !== O && (t *= O, r *= O, h *= O, f *= O), d && (u -= d, 
            s = i * u, o = a * u, u = _ * u + d), s = (T = (s += R.x) - (s |= 0)) ? (0 | T * g + (0 > T ? -.5 : .5)) / g + s : s, 
            o = (T = (o += R.y) - (o |= 0)) ? (0 | T * g + (0 > T ? -.5 : .5)) / g + o : o, 
            u = (T = (u += R.z) - (u |= 0)) ? (0 | T * g + (0 > T ? -.5 : .5)) / g + u : u, 
            A[ye] = "matrix3d(" + [ (0 | t * g) / g, (0 | r * g) / g, (0 | h * g) / g, (0 | f * g) / g, (0 | e * g) / g, (0 | n * g) / g, (0 | l * g) / g, (0 | c * g) / g, (0 | i * g) / g, (0 | a * g) / g, (0 | _ * g) / g, (0 | m * g) / g, s, o, u, I ? 1 + -u / I : 1 ].join(",") + ")";
        }, Se = function() {
            var t, e, i, s, r, n, a, o, h, l = this.data, _ = this.t, u = _.style;
            p && (t = u.top ? "top" : u.bottom ? "bottom" : parseFloat(G(_, "top", null, !1)) ? "bottom" : "top", 
            e = G(_, t, null, !1), i = parseFloat(e) || 0, s = e.substr((i + "").length) || "px", 
            l._ffFix = !l._ffFix, u[t] = (l._ffFix ? i + .05 : i - .05) + s), l.rotation || l.skewX ? (r = l.rotation, 
            n = r - l.skewX, a = 1e5, o = l.scaleX * a, h = l.scaleY * a, u[ye] = "matrix(" + (0 | Math.cos(r) * o) / a + "," + (0 | Math.sin(r) * o) / a + "," + (0 | Math.sin(n) * -h) / a + "," + (0 | Math.cos(n) * h) / a + "," + l.x + "," + l.y + ")") : u[ye] = "matrix(" + l.scaleX + ",0,0," + l.scaleY + "," + l.x + "," + l.y + ")";
        };
        de("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,transformPerspective,directionalRotation,parseTransform,force3D", {
            parser: function(t, e, i, s, n, a, o) {
                if (s._transform) return n;
                var h, l, _, u, p, f, c, m = s._transform = be(t, r, !0, o.parseTransform), d = t.style, g = 1e-6, v = ve.length, y = o, T = {};
                if ("string" == typeof y.transform && ye) _ = d.cssText, d[ye] = y.transform, d.display = "block", 
                h = be(t, null, !1), d.cssText = _; else if ("object" == typeof y) {
                    if (h = {
                        scaleX: se(null != y.scaleX ? y.scaleX : y.scale, m.scaleX),
                        scaleY: se(null != y.scaleY ? y.scaleY : y.scale, m.scaleY),
                        scaleZ: se(null != y.scaleZ ? y.scaleZ : y.scale, m.scaleZ),
                        x: se(y.x, m.x),
                        y: se(y.y, m.y),
                        z: se(y.z, m.z),
                        perspective: se(y.transformPerspective, m.perspective)
                    }, c = y.directionalRotation, null != c) if ("object" == typeof c) for (_ in c) y[_] = c[_]; else y.rotation = c;
                    h.rotation = re("rotation" in y ? y.rotation : "shortRotation" in y ? y.shortRotation + "_short" : "rotationZ" in y ? y.rotationZ : m.rotation * I, m.rotation, "rotation", T), 
                    xe && (h.rotationX = re("rotationX" in y ? y.rotationX : "shortRotationX" in y ? y.shortRotationX + "_short" : m.rotationX * I || 0, m.rotationX, "rotationX", T), 
                    h.rotationY = re("rotationY" in y ? y.rotationY : "shortRotationY" in y ? y.shortRotationY + "_short" : m.rotationY * I || 0, m.rotationY, "rotationY", T)), 
                    h.skewX = null == y.skewX ? m.skewX : re(y.skewX, m.skewX), h.skewY = null == y.skewY ? m.skewY : re(y.skewY, m.skewY), 
                    (l = h.skewY - m.skewY) && (h.skewX += l, h.rotation += l);
                }
                for (null != y.force3D && (m.force3D = y.force3D, f = !0), p = m.force3D || m.z || m.rotationX || m.rotationY || h.z || h.rotationX || h.rotationY || h.perspective, 
                p || null == y.scale || (h.scaleZ = 1); --v > -1; ) i = ve[v], u = h[i] - m[i], 
                (u > g || -g > u || null != F[i]) && (f = !0, n = new pe(m, i, m[i], u, n), i in T && (n.e = T[i]), 
                n.xs0 = 0, n.plugin = a, s._overwriteProps.push(n.n));
                return u = y.transformOrigin, (u || xe && p && m.zOrigin) && (ye ? (f = !0, i = we, 
                u = (u || G(t, i, r, !1, "50% 50%")) + "", n = new pe(d, i, 0, 0, n, -1, "transformOrigin"), 
                n.b = d[i], n.plugin = a, xe ? (_ = m.zOrigin, u = u.split(" "), m.zOrigin = (u.length > 2 && (0 === _ || "0px" !== u[2]) ? parseFloat(u[2]) : _) || 0, 
                n.xs0 = n.e = d[i] = u[0] + " " + (u[1] || "50%") + " 0px", n = new pe(m, "zOrigin", 0, 0, n, -1, n.n), 
                n.b = _, n.xs0 = n.e = m.zOrigin) : n.xs0 = n.e = d[i] = u) : ee(u + "", m)), f && (s._transformType = p || 3 === this._transformType ? 3 : 2), 
                n;
            },
            prefix: !0
        }), de("boxShadow", {
            defaultValue: "0px 0px 0px 0px #999",
            prefix: !0,
            color: !0,
            multi: !0,
            keyword: "inset"
        }), de("borderRadius", {
            defaultValue: "0px",
            parser: function(t, e, i, n, a) {
                e = this.format(e);
                var o, h, l, _, u, p, f, c, m, d, g, v, y, T, w, x, b = [ "borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius" ], P = t.style;
                for (m = parseFloat(t.offsetWidth), d = parseFloat(t.offsetHeight), o = e.split(" "), 
                h = 0; b.length > h; h++) this.p.indexOf("border") && (b[h] = V(b[h])), u = _ = G(t, b[h], r, !1, "0px"), 
                -1 !== u.indexOf(" ") && (_ = u.split(" "), u = _[0], _ = _[1]), p = l = o[h], f = parseFloat(u), 
                v = u.substr((f + "").length), y = "=" === p.charAt(1), y ? (c = parseInt(p.charAt(0) + "1", 10), 
                p = p.substr(2), c *= parseFloat(p), g = p.substr((c + "").length - (0 > c ? 1 : 0)) || "") : (c = parseFloat(p), 
                g = p.substr((c + "").length)), "" === g && (g = s[i] || v), g !== v && (T = $(t, "borderLeft", f, v), 
                w = $(t, "borderTop", f, v), "%" === g ? (u = 100 * (T / m) + "%", _ = 100 * (w / d) + "%") : "em" === g ? (x = $(t, "borderLeft", 1, "em"), 
                u = T / x + "em", _ = w / x + "em") : (u = T + "px", _ = w + "px"), y && (p = parseFloat(u) + c + g, 
                l = parseFloat(_) + c + g)), a = fe(P, b[h], u + " " + _, p + " " + l, !1, "0px", a);
                return a;
            },
            prefix: !0,
            formatter: le("0px 0px 0px 0px", !1, !0)
        }), de("backgroundPosition", {
            defaultValue: "0 0",
            parser: function(t, e, i, s, n, a) {
                var o, h, l, _, u, p, f = "background-position", m = r || Z(t, null), d = this.format((m ? c ? m.getPropertyValue(f + "-x") + " " + m.getPropertyValue(f + "-y") : m.getPropertyValue(f) : t.currentStyle.backgroundPositionX + " " + t.currentStyle.backgroundPositionY) || "0 0"), g = this.format(e);
                if (-1 !== d.indexOf("%") != (-1 !== g.indexOf("%")) && (p = G(t, "backgroundImage").replace(S, ""), 
                p && "none" !== p)) {
                    for (o = d.split(" "), h = g.split(" "), L.setAttribute("src", p), l = 2; --l > -1; ) d = o[l], 
                    _ = -1 !== d.indexOf("%"), _ !== (-1 !== h[l].indexOf("%")) && (u = 0 === l ? t.offsetWidth - L.width : t.offsetHeight - L.height, 
                    o[l] = _ ? parseFloat(d) / 100 * u + "px" : 100 * (parseFloat(d) / u) + "%");
                    d = o.join(" ");
                }
                return this.parseComplex(t.style, d, g, n, a);
            },
            formatter: ee
        }), de("backgroundSize", {
            defaultValue: "0 0",
            formatter: ee
        }), de("perspective", {
            defaultValue: "0px",
            prefix: !0
        }), de("perspectiveOrigin", {
            defaultValue: "50% 50%",
            prefix: !0
        }), de("transformStyle", {
            prefix: !0
        }), de("backfaceVisibility", {
            prefix: !0
        }), de("margin", {
            parser: _e("marginTop,marginRight,marginBottom,marginLeft")
        }), de("padding", {
            parser: _e("paddingTop,paddingRight,paddingBottom,paddingLeft")
        }), de("clip", {
            defaultValue: "rect(0px,0px,0px,0px)",
            parser: function(t, e, i, s, n, a) {
                var o, h, l;
                return 9 > c ? (h = t.currentStyle, l = 8 > c ? " " : ",", o = "rect(" + h.clipTop + l + h.clipRight + l + h.clipBottom + l + h.clipLeft + ")", 
                e = this.format(e).split(",").join(l)) : (o = this.format(G(t, this.p, r, !1, this.dflt)), 
                e = this.format(e)), this.parseComplex(t.style, o, e, n, a);
            }
        }), de("textShadow", {
            defaultValue: "0px 0px 0px #999",
            color: !0,
            multi: !0
        }), de("autoRound,strictUnits", {
            parser: function(t, e, i, s, r) {
                return r;
            }
        }), de("border", {
            defaultValue: "0px solid #000",
            parser: function(t, e, i, s, n, a) {
                return this.parseComplex(t.style, this.format(G(t, "borderTopWidth", r, !1, "0px") + " " + G(t, "borderTopStyle", r, !1, "solid") + " " + G(t, "borderTopColor", r, !1, "#000")), this.format(e), n, a);
            },
            color: !0,
            formatter: function(t) {
                var e = t.split(" ");
                return e[0] + " " + (e[1] || "solid") + " " + (t.match(he) || [ "#000" ])[0];
            }
        }), de("float,cssFloat,styleFloat", {
            parser: function(t, e, i, s, r) {
                var n = t.style, a = "cssFloat" in n ? "cssFloat" : "styleFloat";
                return new pe(n, a, 0, 0, r, -1, i, !1, 0, n[a], e);
            }
        });
        var Re = function(t) {
            var e, i = this.t, s = i.filter || G(this.data, "filter"), r = 0 | this.s + this.c * t;
            100 === r && (-1 === s.indexOf("atrix(") && -1 === s.indexOf("radient(") && -1 === s.indexOf("oader(") ? (i.removeAttribute("filter"), 
            e = !G(this.data, "filter")) : (i.filter = s.replace(x, ""), e = !0)), e || (this.xn1 && (i.filter = s = s || "alpha(opacity=" + r + ")"), 
            -1 === s.indexOf("opacity") ? 0 === r && this.xn1 || (i.filter = s + " alpha(opacity=" + r + ")") : i.filter = s.replace(T, "opacity=" + r));
        };
        de("opacity,alpha,autoAlpha", {
            defaultValue: "1",
            parser: function(t, e, i, s, n, a) {
                var o = parseFloat(G(t, "opacity", r, !1, "1")), h = t.style, l = "autoAlpha" === i;
                return e = parseFloat(e), l && 1 === o && "hidden" === G(t, "visibility", r) && 0 !== e && (o = 0), 
                U ? n = new pe(h, "opacity", o, e - o, n) : (n = new pe(h, "opacity", 100 * o, 100 * (e - o), n), 
                n.xn1 = l ? 1 : 0, h.zoom = 1, n.type = 2, n.b = "alpha(opacity=" + n.s + ")", n.e = "alpha(opacity=" + (n.s + n.c) + ")", 
                n.data = t, n.plugin = a, n.setRatio = Re), l && (n = new pe(h, "visibility", 0, 0, n, -1, null, !1, 0, 0 !== o ? "inherit" : "hidden", 0 === e ? "hidden" : "inherit"), 
                n.xs0 = "inherit", s._overwriteProps.push(n.n), s._overwriteProps.push(i)), n;
            }
        });
        var Ae = function(t, e) {
            e && (t.removeProperty ? t.removeProperty(e.replace(P, "-$1").toLowerCase()) : t.removeAttribute(e));
        }, Ce = function(t) {
            if (this.t._gsClassPT = this, 1 === t || 0 === t) {
                this.t.className = 0 === t ? this.b : this.e;
                for (var e = this.data, i = this.t.style; e; ) e.v ? i[e.p] = e.v : Ae(i, e.p), 
                e = e._next;
                1 === t && this.t._gsClassPT === this && (this.t._gsClassPT = null);
            } else this.t.className !== this.e && (this.t.className = this.e);
        };
        de("className", {
            parser: function(t, e, s, n, a, o, h) {
                var l, _, u, p, f, c = t.className, m = t.style.cssText;
                if (a = n._classNamePT = new pe(t, s, 0, 0, a, 2), a.setRatio = Ce, a.pr = -11, 
                i = !0, a.b = c, _ = W(t, r), u = t._gsClassPT) {
                    for (p = {}, f = u.data; f; ) p[f.p] = 1, f = f._next;
                    u.setRatio(1);
                }
                return t._gsClassPT = a, a.e = "=" !== e.charAt(1) ? e : c.replace(RegExp("\\s*\\b" + e.substr(2) + "\\b"), "") + ("+" === e.charAt(0) ? " " + e.substr(2) : ""), 
                n._tween._duration && (t.className = a.e, l = H(t, _, W(t), h, p), t.className = c, 
                a.data = l.firstMPT, t.style.cssText = m, a = a.xfirst = n.parse(t, l.difs, a, o)), 
                a;
            }
        });
        var Oe = function(t) {
            if ((1 === t || 0 === t) && this.data._totalTime === this.data._totalDuration) {
                var e, i, s, r, n = this.t.style, a = o.transform.parse;
                if ("all" === this.e) n.cssText = "", r = !0; else for (e = this.e.split(","), s = e.length; --s > -1; ) i = e[s], 
                o[i] && (o[i].parse === a ? r = !0 : i = "transformOrigin" === i ? we : o[i].p), 
                Ae(n, i);
                r && (Ae(n, ye), this.t._gsTransform && delete this.t._gsTransform);
            }
        };
        for (de("clearProps", {
            parser: function(t, e, s, r, n) {
                return n = new pe(t, s, 0, 0, n, 2), n.setRatio = Oe, n.e = e, n.pr = -10, n.data = r._tween, 
                i = !0, n;
            }
        }), h = "bezier,throwProps,physicsProps,physics2D".split(","), ce = h.length; ce--; ) ge(h[ce]);
        h = a.prototype, h._firstPT = null, h._onInitTween = function(t, e, o) {
            if (!t.nodeType) return !1;
            this._target = t, this._tween = o, this._vars = e, l = e.autoRound, i = !1, s = e.suffixMap || a.suffixMap, 
            r = Z(t, ""), n = this._overwriteProps;
            var h, p, c, m, d, g, v, y, T, x = t.style;
            if (_ && "" === x.zIndex && (h = G(t, "zIndex", r), ("auto" === h || "" === h) && (x.zIndex = 0)), 
            "string" == typeof e && (m = x.cssText, h = W(t, r), x.cssText = m + ";" + e, h = H(t, h, W(t)).difs, 
            !U && w.test(e) && (h.opacity = parseFloat(RegExp.$1)), e = h, x.cssText = m), this._firstPT = p = this.parse(t, e, null), 
            this._transformType) {
                for (T = 3 === this._transformType, ye ? u && (_ = !0, "" === x.zIndex && (v = G(t, "zIndex", r), 
                ("auto" === v || "" === v) && (x.zIndex = 0)), f && (x.WebkitBackfaceVisibility = this._vars.WebkitBackfaceVisibility || (T ? "visible" : "hidden"))) : x.zoom = 1, 
                c = p; c && c._next; ) c = c._next;
                y = new pe(t, "transform", 0, 0, null, 2), this._linkCSSP(y, null, c), y.setRatio = T && xe ? ke : ye ? Se : Pe, 
                y.data = this._transform || be(t, r, !0), n.pop();
            }
            if (i) {
                for (;p; ) {
                    for (g = p._next, c = m; c && c.pr > p.pr; ) c = c._next;
                    (p._prev = c ? c._prev : d) ? p._prev._next = p : m = p, (p._next = c) ? c._prev = p : d = p, 
                    p = g;
                }
                this._firstPT = m;
            }
            return !0;
        }, h.parse = function(t, e, i, n) {
            var a, h, _, u, p, f, c, m, d, g, v = t.style;
            for (a in e) f = e[a], h = o[a], h ? i = h.parse(t, f, a, this, i, n, e) : (p = G(t, a, r) + "", 
            d = "string" == typeof f, "color" === a || "fill" === a || "stroke" === a || -1 !== a.indexOf("Color") || d && b.test(f) ? (d || (f = oe(f), 
            f = (f.length > 3 ? "rgba(" : "rgb(") + f.join(",") + ")"), i = fe(v, a, p, f, !0, "transparent", i, 0, n)) : !d || -1 === f.indexOf(" ") && -1 === f.indexOf(",") ? (_ = parseFloat(p), 
            c = _ || 0 === _ ? p.substr((_ + "").length) : "", ("" === p || "auto" === p) && ("width" === a || "height" === a ? (_ = te(t, a, r), 
            c = "px") : "left" === a || "top" === a ? (_ = Q(t, a, r), c = "px") : (_ = "opacity" !== a ? 0 : 1, 
            c = "")), g = d && "=" === f.charAt(1), g ? (u = parseInt(f.charAt(0) + "1", 10), 
            f = f.substr(2), u *= parseFloat(f), m = f.replace(y, "")) : (u = parseFloat(f), 
            m = d ? f.substr((u + "").length) || "" : ""), "" === m && (m = s[a] || c), f = u || 0 === u ? (g ? u + _ : u) + m : e[a], 
            c !== m && "" !== m && (u || 0 === u) && (_ || 0 === _) && (_ = $(t, a, _, c), "%" === m ? (_ /= $(t, a, 100, "%") / 100, 
            _ > 100 && (_ = 100), e.strictUnits !== !0 && (p = _ + "%")) : "em" === m ? _ /= $(t, a, 1, "em") : (u = $(t, a, u, m), 
            m = "px"), g && (u || 0 === u) && (f = u + _ + m)), g && (u += _), !_ && 0 !== _ || !u && 0 !== u ? void 0 !== v[a] && (f || "NaN" != f + "" && null != f) ? (i = new pe(v, a, u || _ || 0, 0, i, -1, a, !1, 0, p, f), 
            i.xs0 = "none" !== f || "display" !== a && -1 === a.indexOf("Style") ? f : p) : j("invalid " + a + " tween value: " + e[a]) : (i = new pe(v, a, _, u - _, i, 0, a, l !== !1 && ("px" === m || "zIndex" === a), 0, p, f), 
            i.xs0 = m)) : i = fe(v, a, p, f, !0, null, i, 0, n)), n && i && !i.plugin && (i.plugin = n);
            return i;
        }, h.setRatio = function(t) {
            var e, i, s, r = this._firstPT, n = 1e-6;
            if (1 !== t || this._tween._time !== this._tween._duration && 0 !== this._tween._time) if (t || this._tween._time !== this._tween._duration && 0 !== this._tween._time || this._tween._rawPrevTime === -1e-6) for (;r; ) {
                if (e = r.c * t + r.s, r.r ? e = e > 0 ? 0 | e + .5 : 0 | e - .5 : n > e && e > -n && (e = 0), 
                r.type) if (1 === r.type) if (s = r.l, 2 === s) r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2; else if (3 === s) r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3; else if (4 === s) r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3 + r.xn3 + r.xs4; else if (5 === s) r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3 + r.xn3 + r.xs4 + r.xn4 + r.xs5; else {
                    for (i = r.xs0 + e + r.xs1, s = 1; r.l > s; s++) i += r["xn" + s] + r["xs" + (s + 1)];
                    r.t[r.p] = i;
                } else -1 === r.type ? r.t[r.p] = r.xs0 : r.setRatio && r.setRatio(t); else r.t[r.p] = e + r.xs0;
                r = r._next;
            } else for (;r; ) 2 !== r.type ? r.t[r.p] = r.b : r.setRatio(t), r = r._next; else for (;r; ) 2 !== r.type ? r.t[r.p] = r.e : r.setRatio(t), 
            r = r._next;
        }, h._enableTransforms = function(t) {
            this._transformType = t || 3 === this._transformType ? 3 : 2, this._transform = this._transform || be(this._target, r, !0);
        }, h._linkCSSP = function(t, e, i, s) {
            return t && (e && (e._prev = t), t._next && (t._next._prev = t._prev), t._prev ? t._prev._next = t._next : this._firstPT === t && (this._firstPT = t._next, 
            s = !0), i ? i._next = t : s || null !== this._firstPT || (this._firstPT = t), t._next = e, 
            t._prev = i), t;
        }, h._kill = function(e) {
            var i, s, r, n = e;
            if (e.autoAlpha || e.alpha) {
                n = {};
                for (s in e) n[s] = e[s];
                n.opacity = 1, n.autoAlpha && (n.visibility = 1);
            }
            return e.className && (i = this._classNamePT) && (r = i.xfirst, r && r._prev ? this._linkCSSP(r._prev, i._next, r._prev._prev) : r === this._firstPT && (this._firstPT = i._next), 
            i._next && this._linkCSSP(i._next, i._next._next, r._prev), this._classNamePT = null), 
            t.prototype._kill.call(this, n);
        };
        var De = function(t, e, i) {
            var s, r, n, a;
            if (t.slice) for (r = t.length; --r > -1; ) De(t[r], e, i); else for (s = t.childNodes, 
            r = s.length; --r > -1; ) n = s[r], a = n.type, n.style && (e.push(W(n)), i && i.push(n)), 
            1 !== a && 9 !== a && 11 !== a || !n.childNodes.length || De(n, e, i);
        };
        return a.cascadeTo = function(t, i, s) {
            var r, n, a, o = e.to(t, i, s), h = [ o ], l = [], _ = [], u = [], p = e._internals.reservedProps;
            for (t = o._targets || o.target, De(t, l, u), o.render(i, !0), De(t, _), o.render(0, !0), 
            o._enabled(!0), r = u.length; --r > -1; ) if (n = H(u[r], l[r], _[r]), n.firstMPT) {
                n = n.difs;
                for (a in s) p[a] && (n[a] = s[a]);
                h.push(e.to(u[r], i, n));
            }
            return h;
        }, t.activate([ a ]), a;
    }, !0), function() {
        var t = window._gsDefine.plugin({
            propName: "roundProps",
            priority: -1,
            API: 2,
            init: function(t, e, i) {
                return this._tween = i, !0;
            }
        }), e = t.prototype;
        e._onInitAllProps = function() {
            for (var t, e, i, s = this._tween, r = s.vars.roundProps instanceof Array ? s.vars.roundProps : s.vars.roundProps.split(","), n = r.length, a = {}, o = s._propLookup.roundProps; --n > -1; ) a[r[n]] = 1;
            for (n = r.length; --n > -1; ) for (t = r[n], e = s._firstPT; e; ) i = e._next, 
            e.pg ? e.t._roundProps(a, !0) : e.n === t && (this._add(e.t, t, e.s, e.c), i && (i._prev = e._prev), 
            e._prev ? e._prev._next = i : s._firstPT === e && (s._firstPT = i), e._next = e._prev = null, 
            s._propLookup[t] = o), e = i;
            return !1;
        }, e._add = function(t, e, i, s) {
            this._addTween(t, e, i, i + s, e, !0), this._overwriteProps.push(e);
        };
    }(), window._gsDefine.plugin({
        propName: "attr",
        API: 2,
        init: function(t, e) {
            var i;
            if ("function" != typeof t.setAttribute) return !1;
            this._target = t, this._proxy = {};
            for (i in e) this._addTween(this._proxy, i, parseFloat(t.getAttribute(i)), e[i], i) && this._overwriteProps.push(i);
            return !0;
        },
        set: function(t) {
            this._super.setRatio.call(this, t);
            for (var e, i = this._overwriteProps, s = i.length; --s > -1; ) e = i[s], this._target.setAttribute(e, this._proxy[e] + "");
        }
    }), window._gsDefine.plugin({
        propName: "directionalRotation",
        API: 2,
        init: function(t, e) {
            "object" != typeof e && (e = {
                rotation: e
            }), this.finals = {};
            var i, s, r, n, a, o, h = e.useRadians === !0 ? 2 * Math.PI : 360, l = 1e-6;
            for (i in e) "useRadians" !== i && (o = (e[i] + "").split("_"), s = o[0], r = parseFloat("function" != typeof t[i] ? t[i] : t[i.indexOf("set") || "function" != typeof t["get" + i.substr(3)] ? i : "get" + i.substr(3)]()), 
            n = this.finals[i] = "string" == typeof s && "=" === s.charAt(1) ? r + parseInt(s.charAt(0) + "1", 10) * Number(s.substr(2)) : Number(s) || 0, 
            a = n - r, o.length && (s = o.join("_"), -1 !== s.indexOf("short") && (a %= h, a !== a % (h / 2) && (a = 0 > a ? a + h : a - h)), 
            -1 !== s.indexOf("_cw") && 0 > a ? a = (a + 9999999999 * h) % h - (0 | a / h) * h : -1 !== s.indexOf("ccw") && a > 0 && (a = (a - 9999999999 * h) % h - (0 | a / h) * h)), 
            (a > l || -l > a) && (this._addTween(t, i, r, r + a, i), this._overwriteProps.push(i)));
            return !0;
        },
        set: function(t) {
            var e;
            if (1 !== t) this._super.setRatio.call(this, t); else for (e = this._firstPT; e; ) e.f ? e.t[e.p](this.finals[e.p]) : e.t[e.p] = this.finals[e.p], 
            e = e._next;
        }
    })._autoCSS = !0, window._gsDefine("easing.Back", [ "easing.Ease" ], function(t) {
        var e, i, s, r = window.GreenSockGlobals || window, n = r.com.greensock, a = 2 * Math.PI, o = Math.PI / 2, h = n._class, l = function(e, i) {
            var s = h("easing." + e, function() {}, !0), r = s.prototype = new t();
            return r.constructor = s, r.getRatio = i, s;
        }, _ = t.register || function() {}, u = function(t, e, i, s) {
            var r = h("easing." + t, {
                easeOut: new e(),
                easeIn: new i(),
                easeInOut: new s()
            }, !0);
            return _(r, t), r;
        }, p = function(t, e, i) {
            this.t = t, this.v = e, i && (this.next = i, i.prev = this, this.c = i.v - e, this.gap = i.t - t);
        }, f = function(e, i) {
            var s = h("easing." + e, function(t) {
                this._p1 = t || 0 === t ? t : 1.70158, this._p2 = 1.525 * this._p1;
            }, !0), r = s.prototype = new t();
            return r.constructor = s, r.getRatio = i, r.config = function(t) {
                return new s(t);
            }, s;
        }, c = u("Back", f("BackOut", function(t) {
            return (t -= 1) * t * ((this._p1 + 1) * t + this._p1) + 1;
        }), f("BackIn", function(t) {
            return t * t * ((this._p1 + 1) * t - this._p1);
        }), f("BackInOut", function(t) {
            return 1 > (t *= 2) ? .5 * t * t * ((this._p2 + 1) * t - this._p2) : .5 * ((t -= 2) * t * ((this._p2 + 1) * t + this._p2) + 2);
        })), m = h("easing.SlowMo", function(t, e, i) {
            e = e || 0 === e ? e : .7, null == t ? t = .7 : t > 1 && (t = 1), this._p = 1 !== t ? e : 0, 
            this._p1 = (1 - t) / 2, this._p2 = t, this._p3 = this._p1 + this._p2, this._calcEnd = i === !0;
        }, !0), d = m.prototype = new t();
        return d.constructor = m, d.getRatio = function(t) {
            var e = t + (.5 - t) * this._p;
            return this._p1 > t ? this._calcEnd ? 1 - (t = 1 - t / this._p1) * t : e - (t = 1 - t / this._p1) * t * t * t * e : t > this._p3 ? this._calcEnd ? 1 - (t = (t - this._p3) / this._p1) * t : e + (t - e) * (t = (t - this._p3) / this._p1) * t * t * t : this._calcEnd ? 1 : e;
        }, m.ease = new m(.7, .7), d.config = m.config = function(t, e, i) {
            return new m(t, e, i);
        }, e = h("easing.SteppedEase", function(t) {
            t = t || 1, this._p1 = 1 / t, this._p2 = t + 1;
        }, !0), d = e.prototype = new t(), d.constructor = e, d.getRatio = function(t) {
            return 0 > t ? t = 0 : t >= 1 && (t = .999999999), (this._p2 * t >> 0) * this._p1;
        }, d.config = e.config = function(t) {
            return new e(t);
        }, i = h("easing.RoughEase", function(e) {
            e = e || {};
            for (var i, s, r, n, a, o, h = e.taper || "none", l = [], _ = 0, u = 0 | (e.points || 20), f = u, c = e.randomize !== !1, m = e.clamp === !0, d = e.template instanceof t ? e.template : null, g = "number" == typeof e.strength ? .4 * e.strength : .4; --f > -1; ) i = c ? Math.random() : 1 / u * f, 
            s = d ? d.getRatio(i) : i, "none" === h ? r = g : "out" === h ? (n = 1 - i, r = n * n * g) : "in" === h ? r = i * i * g : .5 > i ? (n = 2 * i, 
            r = .5 * n * n * g) : (n = 2 * (1 - i), r = .5 * n * n * g), c ? s += Math.random() * r - .5 * r : f % 2 ? s += .5 * r : s -= .5 * r, 
            m && (s > 1 ? s = 1 : 0 > s && (s = 0)), l[_++] = {
                x: i,
                y: s
            };
            for (l.sort(function(t, e) {
                return t.x - e.x;
            }), o = new p(1, 1, null), f = u; --f > -1; ) a = l[f], o = new p(a.x, a.y, o);
            this._prev = new p(0, 0, 0 !== o.t ? o : o.next);
        }, !0), d = i.prototype = new t(), d.constructor = i, d.getRatio = function(t) {
            var e = this._prev;
            if (t > e.t) {
                for (;e.next && t >= e.t; ) e = e.next;
                e = e.prev;
            } else for (;e.prev && e.t >= t; ) e = e.prev;
            return this._prev = e, e.v + (t - e.t) / e.gap * e.c;
        }, d.config = function(t) {
            return new i(t);
        }, i.ease = new i(), u("Bounce", l("BounceOut", function(t) {
            return 1 / 2.75 > t ? 7.5625 * t * t : 2 / 2.75 > t ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : 2.5 / 2.75 > t ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375;
        }), l("BounceIn", function(t) {
            return 1 / 2.75 > (t = 1 - t) ? 1 - 7.5625 * t * t : 2 / 2.75 > t ? 1 - (7.5625 * (t -= 1.5 / 2.75) * t + .75) : 2.5 / 2.75 > t ? 1 - (7.5625 * (t -= 2.25 / 2.75) * t + .9375) : 1 - (7.5625 * (t -= 2.625 / 2.75) * t + .984375);
        }), l("BounceInOut", function(t) {
            var e = .5 > t;
            return t = e ? 1 - 2 * t : 2 * t - 1, t = 1 / 2.75 > t ? 7.5625 * t * t : 2 / 2.75 > t ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : 2.5 / 2.75 > t ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375, 
            e ? .5 * (1 - t) : .5 * t + .5;
        })), u("Circ", l("CircOut", function(t) {
            return Math.sqrt(1 - (t -= 1) * t);
        }), l("CircIn", function(t) {
            return -(Math.sqrt(1 - t * t) - 1);
        }), l("CircInOut", function(t) {
            return 1 > (t *= 2) ? -.5 * (Math.sqrt(1 - t * t) - 1) : .5 * (Math.sqrt(1 - (t -= 2) * t) + 1);
        })), s = function(e, i, s) {
            var r = h("easing." + e, function(t, e) {
                this._p1 = t || 1, this._p2 = e || s, this._p3 = this._p2 / a * (Math.asin(1 / this._p1) || 0);
            }, !0), n = r.prototype = new t();
            return n.constructor = r, n.getRatio = i, n.config = function(t, e) {
                return new r(t, e);
            }, r;
        }, u("Elastic", s("ElasticOut", function(t) {
            return this._p1 * Math.pow(2, -10 * t) * Math.sin((t - this._p3) * a / this._p2) + 1;
        }, .3), s("ElasticIn", function(t) {
            return -(this._p1 * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - this._p3) * a / this._p2));
        }, .3), s("ElasticInOut", function(t) {
            return 1 > (t *= 2) ? -.5 * this._p1 * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - this._p3) * a / this._p2) : .5 * this._p1 * Math.pow(2, -10 * (t -= 1)) * Math.sin((t - this._p3) * a / this._p2) + 1;
        }, .45)), u("Expo", l("ExpoOut", function(t) {
            return 1 - Math.pow(2, -10 * t);
        }), l("ExpoIn", function(t) {
            return Math.pow(2, 10 * (t - 1)) - .001;
        }), l("ExpoInOut", function(t) {
            return 1 > (t *= 2) ? .5 * Math.pow(2, 10 * (t - 1)) : .5 * (2 - Math.pow(2, -10 * (t - 1)));
        })), u("Sine", l("SineOut", function(t) {
            return Math.sin(t * o);
        }), l("SineIn", function(t) {
            return -Math.cos(t * o) + 1;
        }), l("SineInOut", function(t) {
            return -.5 * (Math.cos(Math.PI * t) - 1);
        })), h("easing.EaseLookup", {
            find: function(e) {
                return t.map[e];
            }
        }, !0), _(r.SlowMo, "SlowMo", "ease,"), _(i, "RoughEase", "ease,"), _(e, "SteppedEase", "ease,"), 
        c;
    }, !0);
}), function(t) {
    var e, i, s, r, n, a = t.GreenSockGlobals || t, o = function(t) {
        var e, i = t.split("."), s = a;
        for (e = 0; i.length > e; e++) s[i[e]] = s = s[i[e]] || {};
        return s;
    }, h = o("com.greensock"), l = [].slice, _ = function() {}, u = {}, p = function(e, i, s, r) {
        this.sc = u[e] ? u[e].sc : [], u[e] = this, this.gsClass = null, this.func = s;
        var n = [];
        this.check = function(h) {
            for (var l, _, f, c, m = i.length, d = m; --m > -1; ) (l = u[i[m]] || new p(i[m], [])).gsClass ? (n[m] = l.gsClass, 
            d--) : h && l.sc.push(this);
            if (0 === d && s) for (_ = ("com.greensock." + e).split("."), f = _.pop(), c = o(_.join("."))[f] = this.gsClass = s.apply(s, n), 
            r && (a[f] = c, "function" == typeof define && define.amd ? define((t.GreenSockAMDPath ? t.GreenSockAMDPath + "/" : "") + e.split(".").join("/"), [], function() {
                return c;
            }) : "undefined" != typeof module && module.exports && (module.exports = c)), m = 0; this.sc.length > m; m++) this.sc[m].check();
        }, this.check(!0);
    }, f = t._gsDefine = function(t, e, i, s) {
        return new p(t, e, i, s);
    }, c = h._class = function(t, e, i) {
        return e = e || function() {}, f(t, [], function() {
            return e;
        }, i), e;
    };
    f.globals = a;
    var m = [ 0, 0, 1, 1 ], d = [], g = c("easing.Ease", function(t, e, i, s) {
        this._func = t, this._type = i || 0, this._power = s || 0, this._params = e ? m.concat(e) : m;
    }, !0), v = g.map = {}, y = g.register = function(t, e, i, s) {
        for (var r, n, a, o, l = e.split(","), _ = l.length, u = (i || "easeIn,easeOut,easeInOut").split(","); --_ > -1; ) for (n = l[_], 
        r = s ? c("easing." + n, null, !0) : h.easing[n] || {}, a = u.length; --a > -1; ) o = u[a], 
        v[n + "." + o] = v[o + n] = r[o] = t.getRatio ? t : t[o] || new t();
    };
    for (s = g.prototype, s._calcEnd = !1, s.getRatio = function(t) {
        if (this._func) return this._params[0] = t, this._func.apply(null, this._params);
        var e = this._type, i = this._power, s = 1 === e ? 1 - t : 2 === e ? t : .5 > t ? 2 * t : 2 * (1 - t);
        return 1 === i ? s *= s : 2 === i ? s *= s * s : 3 === i ? s *= s * s * s : 4 === i && (s *= s * s * s * s), 
        1 === e ? 1 - s : 2 === e ? s : .5 > t ? s / 2 : 1 - s / 2;
    }, e = [ "Linear", "Quad", "Cubic", "Quart", "Quint,Strong" ], i = e.length; --i > -1; ) s = e[i] + ",Power" + i, 
    y(new g(null, null, 1, i), s, "easeOut", !0), y(new g(null, null, 2, i), s, "easeIn" + (0 === i ? ",easeNone" : "")), 
    y(new g(null, null, 3, i), s, "easeInOut");
    v.linear = h.easing.Linear.easeIn, v.swing = h.easing.Quad.easeInOut;
    var T = c("events.EventDispatcher", function(t) {
        this._listeners = {}, this._eventTarget = t || this;
    });
    s = T.prototype, s.addEventListener = function(t, e, i, s, a) {
        a = a || 0;
        var o, h, l = this._listeners[t], _ = 0;
        for (null == l && (this._listeners[t] = l = []), h = l.length; --h > -1; ) o = l[h], 
        o.c === e && o.s === i ? l.splice(h, 1) : 0 === _ && a > o.pr && (_ = h + 1);
        l.splice(_, 0, {
            c: e,
            s: i,
            up: s,
            pr: a
        }), this !== r || n || r.wake();
    }, s.removeEventListener = function(t, e) {
        var i, s = this._listeners[t];
        if (s) for (i = s.length; --i > -1; ) if (s[i].c === e) return s.splice(i, 1), void 0;
    }, s.dispatchEvent = function(t) {
        var e, i, s, r = this._listeners[t];
        if (r) for (e = r.length, i = this._eventTarget; --e > -1; ) s = r[e], s.up ? s.c.call(s.s || i, {
            type: t,
            target: i
        }) : s.c.call(s.s || i);
    };
    var w = t.requestAnimationFrame, x = t.cancelAnimationFrame, b = Date.now || function() {
        return new Date().getTime();
    }, P = b();
    for (e = [ "ms", "moz", "webkit", "o" ], i = e.length; --i > -1 && !w; ) w = t[e[i] + "RequestAnimationFrame"], 
    x = t[e[i] + "CancelAnimationFrame"] || t[e[i] + "CancelRequestAnimationFrame"];
    c("Ticker", function(t, e) {
        var i, s, a, o, h, l = this, u = b(), p = e !== !1 && w, f = function(t) {
            P = b(), l.time = (P - u) / 1e3;
            var e, r = l.time - h;
            (!i || r > 0 || t === !0) && (l.frame++, h += r + (r >= o ? .004 : o - r), e = !0), 
            t !== !0 && (a = s(f)), e && l.dispatchEvent("tick");
        };
        T.call(l), l.time = l.frame = 0, l.tick = function() {
            f(!0);
        }, l.sleep = function() {
            null != a && (p && x ? x(a) : clearTimeout(a), s = _, a = null, l === r && (n = !1));
        }, l.wake = function() {
            null !== a && l.sleep(), s = 0 === i ? _ : p && w ? w : function(t) {
                return setTimeout(t, 0 | 1e3 * (h - l.time) + 1);
            }, l === r && (n = !0), f(2);
        }, l.fps = function(t) {
            return arguments.length ? (i = t, o = 1 / (i || 60), h = this.time + o, l.wake(), 
            void 0) : i;
        }, l.useRAF = function(t) {
            return arguments.length ? (l.sleep(), p = t, l.fps(i), void 0) : p;
        }, l.fps(t), setTimeout(function() {
            p && (!a || 5 > l.frame) && l.useRAF(!1);
        }, 1500);
    }), s = h.Ticker.prototype = new h.events.EventDispatcher(), s.constructor = h.Ticker;
    var k = c("core.Animation", function(t, e) {
        if (this.vars = e = e || {}, this._duration = this._totalDuration = t || 0, this._delay = Number(e.delay) || 0, 
        this._timeScale = 1, this._active = e.immediateRender === !0, this.data = e.data, 
        this._reversed = e.reversed === !0, X) {
            n || r.wake();
            var i = this.vars.useFrames ? L : X;
            i.add(this, i._time), this.vars.paused && this.paused(!0);
        }
    });
    r = k.ticker = new h.Ticker(), s = k.prototype, s._dirty = s._gc = s._initted = s._paused = !1, 
    s._totalTime = s._time = 0, s._rawPrevTime = -1, s._next = s._last = s._onUpdate = s._timeline = s.timeline = null, 
    s._paused = !1;
    var S = function() {
        b() - P > 2e3 && r.wake(), setTimeout(S, 2e3);
    };
    S(), s.play = function(t, e) {
        return arguments.length && this.seek(t, e), this.reversed(!1).paused(!1);
    }, s.pause = function(t, e) {
        return arguments.length && this.seek(t, e), this.paused(!0);
    }, s.resume = function(t, e) {
        return arguments.length && this.seek(t, e), this.paused(!1);
    }, s.seek = function(t, e) {
        return this.totalTime(Number(t), e !== !1);
    }, s.restart = function(t, e) {
        return this.reversed(!1).paused(!1).totalTime(t ? -this._delay : 0, e !== !1, !0);
    }, s.reverse = function(t, e) {
        return arguments.length && this.seek(t || this.totalDuration(), e), this.reversed(!0).paused(!1);
    }, s.render = function() {}, s.invalidate = function() {
        return this;
    }, s._enabled = function(t, e) {
        return n || r.wake(), this._gc = !t, this._active = t && !this._paused && this._totalTime > 0 && this._totalTime < this._totalDuration, 
        e !== !0 && (t && !this.timeline ? this._timeline.add(this, this._startTime - this._delay) : !t && this.timeline && this._timeline._remove(this, !0)), 
        !1;
    }, s._kill = function() {
        return this._enabled(!1, !1);
    }, s.kill = function(t, e) {
        return this._kill(t, e), this;
    }, s._uncache = function(t) {
        for (var e = t ? this : this.timeline; e; ) e._dirty = !0, e = e.timeline;
        return this;
    }, s._swapSelfInParams = function(t) {
        for (var e = t.length, i = t.concat(); --e > -1; ) "{self}" === t[e] && (i[e] = this);
        return i;
    }, s.eventCallback = function(t, e, i, s) {
        if ("on" === (t || "").substr(0, 2)) {
            var r = this.vars;
            if (1 === arguments.length) return r[t];
            null == e ? delete r[t] : (r[t] = e, r[t + "Params"] = i instanceof Array && -1 !== i.join("").indexOf("{self}") ? this._swapSelfInParams(i) : i, 
            r[t + "Scope"] = s), "onUpdate" === t && (this._onUpdate = e);
        }
        return this;
    }, s.delay = function(t) {
        return arguments.length ? (this._timeline.smoothChildTiming && this.startTime(this._startTime + t - this._delay), 
        this._delay = t, this) : this._delay;
    }, s.duration = function(t) {
        return arguments.length ? (this._duration = this._totalDuration = t, this._uncache(!0), 
        this._timeline.smoothChildTiming && this._time > 0 && this._time < this._duration && 0 !== t && this.totalTime(this._totalTime * (t / this._duration), !0), 
        this) : (this._dirty = !1, this._duration);
    }, s.totalDuration = function(t) {
        return this._dirty = !1, arguments.length ? this.duration(t) : this._totalDuration;
    }, s.time = function(t, e) {
        return arguments.length ? (this._dirty && this.totalDuration(), this.totalTime(t > this._duration ? this._duration : t, e)) : this._time;
    }, s.totalTime = function(t, e, i) {
        if (n || r.wake(), !arguments.length) return this._totalTime;
        if (this._timeline) {
            if (0 > t && !i && (t += this.totalDuration()), this._timeline.smoothChildTiming) {
                this._dirty && this.totalDuration();
                var s = this._totalDuration, a = this._timeline;
                if (t > s && !i && (t = s), this._startTime = (this._paused ? this._pauseTime : a._time) - (this._reversed ? s - t : t) / this._timeScale, 
                a._dirty || this._uncache(!1), a._timeline) for (;a._timeline; ) a._timeline._time !== (a._startTime + a._totalTime) / a._timeScale && a.totalTime(a._totalTime, !0), 
                a = a._timeline;
            }
            this._gc && this._enabled(!0, !1), this._totalTime !== t && this.render(t, e, !1);
        }
        return this;
    }, s.startTime = function(t) {
        return arguments.length ? (t !== this._startTime && (this._startTime = t, this.timeline && this.timeline._sortChildren && this.timeline.add(this, t - this._delay)), 
        this) : this._startTime;
    }, s.timeScale = function(t) {
        if (!arguments.length) return this._timeScale;
        if (t = t || 1e-6, this._timeline && this._timeline.smoothChildTiming) {
            var e = this._pauseTime, i = e || 0 === e ? e : this._timeline.totalTime();
            this._startTime = i - (i - this._startTime) * this._timeScale / t;
        }
        return this._timeScale = t, this._uncache(!1);
    }, s.reversed = function(t) {
        return arguments.length ? (t != this._reversed && (this._reversed = t, this.totalTime(this._totalTime, !0)), 
        this) : this._reversed;
    }, s.paused = function(t) {
        if (!arguments.length) return this._paused;
        if (t != this._paused && this._timeline) {
            n || t || r.wake();
            var e = this._timeline, i = e.rawTime(), s = i - this._pauseTime;
            !t && e.smoothChildTiming && (this._startTime += s, this._uncache(!1)), this._pauseTime = t ? i : null, 
            this._paused = t, this._active = !t && this._totalTime > 0 && this._totalTime < this._totalDuration, 
            t || 0 === s || 0 === this._duration || this.render(e.smoothChildTiming ? this._totalTime : (i - this._startTime) / this._timeScale, !0, !0);
        }
        return this._gc && !t && this._enabled(!0, !1), this;
    };
    var R = c("core.SimpleTimeline", function(t) {
        k.call(this, 0, t), this.autoRemoveChildren = this.smoothChildTiming = !0;
    });
    s = R.prototype = new k(), s.constructor = R, s.kill()._gc = !1, s._first = s._last = null, 
    s._sortChildren = !1, s.add = s.insert = function(t, e) {
        var i, s;
        if (t._startTime = Number(e || 0) + t._delay, t._paused && this !== t._timeline && (t._pauseTime = t._startTime + (this.rawTime() - t._startTime) / t._timeScale), 
        t.timeline && t.timeline._remove(t, !0), t.timeline = t._timeline = this, t._gc && t._enabled(!0, !0), 
        i = this._last, this._sortChildren) for (s = t._startTime; i && i._startTime > s; ) i = i._prev;
        return i ? (t._next = i._next, i._next = t) : (t._next = this._first, this._first = t), 
        t._next ? t._next._prev = t : this._last = t, t._prev = i, this._timeline && this._uncache(!0), 
        this;
    }, s._remove = function(t, e) {
        return t.timeline === this && (e || t._enabled(!1, !0), t.timeline = null, t._prev ? t._prev._next = t._next : this._first === t && (this._first = t._next), 
        t._next ? t._next._prev = t._prev : this._last === t && (this._last = t._prev), 
        this._timeline && this._uncache(!0)), this;
    }, s.render = function(t, e, i) {
        var s, r = this._first;
        for (this._totalTime = this._time = this._rawPrevTime = t; r; ) s = r._next, (r._active || t >= r._startTime && !r._paused) && (r._reversed ? r.render((r._dirty ? r.totalDuration() : r._totalDuration) - (t - r._startTime) * r._timeScale, e, i) : r.render((t - r._startTime) * r._timeScale, e, i)), 
        r = s;
    }, s.rawTime = function() {
        return n || r.wake(), this._totalTime;
    };
    var A = c("TweenLite", function(e, i, s) {
        if (k.call(this, i, s), this.render = A.prototype.render, null == e) throw "Cannot tween a null target.";
        this.target = e = "string" != typeof e ? e : A.selector(e) || e;
        var r, n, a, o = e.jquery || e.length && e !== t && e[0] && (e[0] === t || e[0].nodeType && e[0].style && !e.nodeType), h = this.vars.overwrite;
        if (this._overwrite = h = null == h ? N[A.defaultOverwrite] : "number" == typeof h ? h >> 0 : N[h], 
        (o || e instanceof Array) && "number" != typeof e[0]) for (this._targets = a = l.call(e, 0), 
        this._propLookup = [], this._siblings = [], r = 0; a.length > r; r++) n = a[r], 
        n ? "string" != typeof n ? n.length && n !== t && n[0] && (n[0] === t || n[0].nodeType && n[0].style && !n.nodeType) ? (a.splice(r--, 1), 
        this._targets = a = a.concat(l.call(n, 0))) : (this._siblings[r] = z(n, this, !1), 
        1 === h && this._siblings[r].length > 1 && U(n, this, null, 1, this._siblings[r])) : (n = a[r--] = A.selector(n), 
        "string" == typeof n && a.splice(r + 1, 1)) : a.splice(r--, 1); else this._propLookup = {}, 
        this._siblings = z(e, this, !1), 1 === h && this._siblings.length > 1 && U(e, this, null, 1, this._siblings);
        (this.vars.immediateRender || 0 === i && 0 === this._delay && this.vars.immediateRender !== !1) && this.render(-this._delay, !1, !0);
    }, !0), C = function(e) {
        return e.length && e !== t && e[0] && (e[0] === t || e[0].nodeType && e[0].style && !e.nodeType);
    }, O = function(t, e) {
        var i, s = {};
        for (i in t) E[i] || i in e && "x" !== i && "y" !== i && "width" !== i && "height" !== i && "className" !== i && "border" !== i || !(!M[i] || M[i] && M[i]._autoCSS) || (s[i] = t[i], 
        delete t[i]);
        t.css = s;
    };
    s = A.prototype = new k(), s.constructor = A, s.kill()._gc = !1, s.ratio = 0, s._firstPT = s._targets = s._overwrittenProps = s._startAt = null, 
    s._notifyPluginsOfEnabled = !1, A.version = "1.10.3", A.defaultEase = s._ease = new g(null, null, 1, 1), 
    A.defaultOverwrite = "auto", A.ticker = r, A.autoSleep = !0, A.selector = t.$ || t.jQuery || function(e) {
        return t.$ ? (A.selector = t.$, t.$(e)) : t.document ? t.document.getElementById("#" === e.charAt(0) ? e.substr(1) : e) : e;
    };
    var D = A._internals = {}, M = A._plugins = {}, I = A._tweenLookup = {}, F = 0, E = D.reservedProps = {
        ease: 1,
        delay: 1,
        overwrite: 1,
        onComplete: 1,
        onCompleteParams: 1,
        onCompleteScope: 1,
        useFrames: 1,
        runBackwards: 1,
        startAt: 1,
        onUpdate: 1,
        onUpdateParams: 1,
        onUpdateScope: 1,
        onStart: 1,
        onStartParams: 1,
        onStartScope: 1,
        onReverseComplete: 1,
        onReverseCompleteParams: 1,
        onReverseCompleteScope: 1,
        onRepeat: 1,
        onRepeatParams: 1,
        onRepeatScope: 1,
        easeParams: 1,
        yoyo: 1,
        immediateRender: 1,
        repeat: 1,
        repeatDelay: 1,
        data: 1,
        paused: 1,
        reversed: 1,
        autoCSS: 1
    }, N = {
        none: 0,
        all: 1,
        auto: 2,
        concurrent: 3,
        allOnStart: 4,
        preexisting: 5,
        "true": 1,
        "false": 0
    }, L = k._rootFramesTimeline = new R(), X = k._rootTimeline = new R();
    X._startTime = r.time, L._startTime = r.frame, X._active = L._active = !0, k._updateRoot = function() {
        if (X.render((r.time - X._startTime) * X._timeScale, !1, !1), L.render((r.frame - L._startTime) * L._timeScale, !1, !1), 
        !(r.frame % 120)) {
            var t, e, i;
            for (i in I) {
                for (e = I[i].tweens, t = e.length; --t > -1; ) e[t]._gc && e.splice(t, 1);
                0 === e.length && delete I[i];
            }
            if (i = X._first, (!i || i._paused) && A.autoSleep && !L._first && 1 === r._listeners.tick.length) {
                for (;i && i._paused; ) i = i._next;
                i || r.sleep();
            }
        }
    }, r.addEventListener("tick", k._updateRoot);
    var z = function(t, e, i) {
        var s, r, n = t._gsTweenID;
        if (I[n || (t._gsTweenID = n = "t" + F++)] || (I[n] = {
            target: t,
            tweens: []
        }), e && (s = I[n].tweens, s[r = s.length] = e, i)) for (;--r > -1; ) s[r] === e && s.splice(r, 1);
        return I[n].tweens;
    }, U = function(t, e, i, s, r) {
        var n, a, o, h;
        if (1 === s || s >= 4) {
            for (h = r.length, n = 0; h > n; n++) if ((o = r[n]) !== e) o._gc || o._enabled(!1, !1) && (a = !0); else if (5 === s) break;
            return a;
        }
        var l, _ = e._startTime + 1e-10, u = [], p = 0, f = 0 === e._duration;
        for (n = r.length; --n > -1; ) (o = r[n]) === e || o._gc || o._paused || (o._timeline !== e._timeline ? (l = l || Y(e, 0, f), 
        0 === Y(o, l, f) && (u[p++] = o)) : _ >= o._startTime && o._startTime + o.totalDuration() / o._timeScale + 1e-10 > _ && ((f || !o._initted) && 2e-10 >= _ - o._startTime || (u[p++] = o)));
        for (n = p; --n > -1; ) o = u[n], 2 === s && o._kill(i, t) && (a = !0), (2 !== s || !o._firstPT && o._initted) && o._enabled(!1, !1) && (a = !0);
        return a;
    }, Y = function(t, e, i) {
        for (var s = t._timeline, r = s._timeScale, n = t._startTime, a = 1e-10; s._timeline; ) {
            if (n += s._startTime, r *= s._timeScale, s._paused) return -100;
            s = s._timeline;
        }
        return n /= r, n > e ? n - e : i && n === e || !t._initted && 2 * a > n - e ? a : (n += t.totalDuration() / t._timeScale / r) > e + a ? 0 : n - e - a;
    };
    s._init = function() {
        var t, e, i, s, r = this.vars, n = this._overwrittenProps, a = this._duration, o = r.immediateRender, h = r.ease;
        if (r.startAt) {
            if (this._startAt && this._startAt.render(-1, !0), r.startAt.overwrite = 0, r.startAt.immediateRender = !0, 
            this._startAt = A.to(this.target, 0, r.startAt), o) if (this._time > 0) this._startAt = null; else if (0 !== a) return;
        } else if (r.runBackwards && r.immediateRender && 0 !== a) if (this._startAt) this._startAt.render(-1, !0), 
        this._startAt = null; else if (0 === this._time) {
            i = {};
            for (s in r) E[s] && "autoCSS" !== s || (i[s] = r[s]);
            return i.overwrite = 0, this._startAt = A.to(this.target, 0, i), void 0;
        }
        if (this._ease = h ? h instanceof g ? r.easeParams instanceof Array ? h.config.apply(h, r.easeParams) : h : "function" == typeof h ? new g(h, r.easeParams) : v[h] || A.defaultEase : A.defaultEase, 
        this._easeType = this._ease._type, this._easePower = this._ease._power, this._firstPT = null, 
        this._targets) for (t = this._targets.length; --t > -1; ) this._initProps(this._targets[t], this._propLookup[t] = {}, this._siblings[t], n ? n[t] : null) && (e = !0); else e = this._initProps(this.target, this._propLookup, this._siblings, n);
        if (e && A._onPluginEvent("_onInitAllProps", this), n && (this._firstPT || "function" != typeof this.target && this._enabled(!1, !1)), 
        r.runBackwards) for (i = this._firstPT; i; ) i.s += i.c, i.c = -i.c, i = i._next;
        this._onUpdate = r.onUpdate, this._initted = !0;
    }, s._initProps = function(e, i, s, r) {
        var n, a, o, h, l, _;
        if (null == e) return !1;
        this.vars.css || e.style && e !== t && e.nodeType && M.css && this.vars.autoCSS !== !1 && O(this.vars, e);
        for (n in this.vars) {
            if (_ = this.vars[n], E[n]) _ instanceof Array && -1 !== _.join("").indexOf("{self}") && (this.vars[n] = _ = this._swapSelfInParams(_, this)); else if (M[n] && (h = new M[n]())._onInitTween(e, this.vars[n], this)) {
                for (this._firstPT = l = {
                    _next: this._firstPT,
                    t: h,
                    p: "setRatio",
                    s: 0,
                    c: 1,
                    f: !0,
                    n: n,
                    pg: !0,
                    pr: h._priority
                }, a = h._overwriteProps.length; --a > -1; ) i[h._overwriteProps[a]] = this._firstPT;
                (h._priority || h._onInitAllProps) && (o = !0), (h._onDisable || h._onEnable) && (this._notifyPluginsOfEnabled = !0);
            } else this._firstPT = i[n] = l = {
                _next: this._firstPT,
                t: e,
                p: n,
                f: "function" == typeof e[n],
                n: n,
                pg: !1,
                pr: 0
            }, l.s = l.f ? e[n.indexOf("set") || "function" != typeof e["get" + n.substr(3)] ? n : "get" + n.substr(3)]() : parseFloat(e[n]), 
            l.c = "string" == typeof _ && "=" === _.charAt(1) ? parseInt(_.charAt(0) + "1", 10) * Number(_.substr(2)) : Number(_) - l.s || 0;
            l && l._next && (l._next._prev = l);
        }
        return r && this._kill(r, e) ? this._initProps(e, i, s, r) : this._overwrite > 1 && this._firstPT && s.length > 1 && U(e, this, i, this._overwrite, s) ? (this._kill(i, e), 
        this._initProps(e, i, s, r)) : o;
    }, s.render = function(t, e, i) {
        var s, r, n, a = this._time;
        if (t >= this._duration) this._totalTime = this._time = this._duration, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1, 
        this._reversed || (s = !0, r = "onComplete"), 0 === this._duration && ((0 === t || 0 > this._rawPrevTime) && this._rawPrevTime !== t && (i = !0, 
        this._rawPrevTime > 0 && (r = "onReverseComplete", e && (t = -1))), this._rawPrevTime = t); else if (1e-7 > t) this._totalTime = this._time = 0, 
        this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== a || 0 === this._duration && this._rawPrevTime > 0) && (r = "onReverseComplete", 
        s = this._reversed), 0 > t ? (this._active = !1, 0 === this._duration && (this._rawPrevTime >= 0 && (i = !0), 
        this._rawPrevTime = t)) : this._initted || (i = !0); else if (this._totalTime = this._time = t, 
        this._easeType) {
            var o = t / this._duration, h = this._easeType, l = this._easePower;
            (1 === h || 3 === h && o >= .5) && (o = 1 - o), 3 === h && (o *= 2), 1 === l ? o *= o : 2 === l ? o *= o * o : 3 === l ? o *= o * o * o : 4 === l && (o *= o * o * o * o), 
            this.ratio = 1 === h ? 1 - o : 2 === h ? o : .5 > t / this._duration ? o / 2 : 1 - o / 2;
        } else this.ratio = this._ease.getRatio(t / this._duration);
        if (this._time !== a || i) {
            if (!this._initted) {
                if (this._init(), !this._initted) return;
                this._time && !s ? this.ratio = this._ease.getRatio(this._time / this._duration) : s && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1));
            }
            for (this._active || !this._paused && this._time !== a && t >= 0 && (this._active = !0), 
            0 === a && (this._startAt && (t >= 0 ? this._startAt.render(t, e, i) : r || (r = "_dummyGS")), 
            this.vars.onStart && (0 !== this._time || 0 === this._duration) && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || d))), 
            n = this._firstPT; n; ) n.f ? n.t[n.p](n.c * this.ratio + n.s) : n.t[n.p] = n.c * this.ratio + n.s, 
            n = n._next;
            this._onUpdate && (0 > t && this._startAt && this._startAt.render(t, e, i), e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || d)), 
            r && (this._gc || (0 > t && this._startAt && !this._onUpdate && this._startAt.render(t, e, i), 
            s && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), 
            !e && this.vars[r] && this.vars[r].apply(this.vars[r + "Scope"] || this, this.vars[r + "Params"] || d)));
        }
    }, s._kill = function(t, e) {
        if ("all" === t && (t = null), null == t && (null == e || e === this.target)) return this._enabled(!1, !1);
        e = "string" != typeof e ? e || this._targets || this.target : A.selector(e) || e;
        var i, s, r, n, a, o, h, l;
        if ((e instanceof Array || C(e)) && "number" != typeof e[0]) for (i = e.length; --i > -1; ) this._kill(t, e[i]) && (o = !0); else {
            if (this._targets) for (i = this._targets.length; --i > -1; ) {
                if (e === this._targets[i]) {
                    a = this._propLookup[i] || {}, this._overwrittenProps = this._overwrittenProps || [], 
                    s = this._overwrittenProps[i] = t ? this._overwrittenProps[i] || {} : "all";
                    break;
                }
            } else {
                if (e !== this.target) return !1;
                a = this._propLookup, s = this._overwrittenProps = t ? this._overwrittenProps || {} : "all";
            }
            if (a) {
                h = t || a, l = t !== s && "all" !== s && t !== a && (null == t || t._tempKill !== !0);
                for (r in h) (n = a[r]) && (n.pg && n.t._kill(h) && (o = !0), n.pg && 0 !== n.t._overwriteProps.length || (n._prev ? n._prev._next = n._next : n === this._firstPT && (this._firstPT = n._next), 
                n._next && (n._next._prev = n._prev), n._next = n._prev = null), delete a[r]), l && (s[r] = 1);
                !this._firstPT && this._initted && this._enabled(!1, !1);
            }
        }
        return o;
    }, s.invalidate = function() {
        return this._notifyPluginsOfEnabled && A._onPluginEvent("_onDisable", this), this._firstPT = null, 
        this._overwrittenProps = null, this._onUpdate = null, this._startAt = null, this._initted = this._active = this._notifyPluginsOfEnabled = !1, 
        this._propLookup = this._targets ? {} : [], this;
    }, s._enabled = function(t, e) {
        if (n || r.wake(), t && this._gc) {
            var i, s = this._targets;
            if (s) for (i = s.length; --i > -1; ) this._siblings[i] = z(s[i], this, !0); else this._siblings = z(this.target, this, !0);
        }
        return k.prototype._enabled.call(this, t, e), this._notifyPluginsOfEnabled && this._firstPT ? A._onPluginEvent(t ? "_onEnable" : "_onDisable", this) : !1;
    }, A.to = function(t, e, i) {
        return new A(t, e, i);
    }, A.from = function(t, e, i) {
        return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, new A(t, e, i);
    }, A.fromTo = function(t, e, i, s) {
        return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, 
        new A(t, e, s);
    }, A.delayedCall = function(t, e, i, s, r) {
        return new A(e, 0, {
            delay: t,
            onComplete: e,
            onCompleteParams: i,
            onCompleteScope: s,
            onReverseComplete: e,
            onReverseCompleteParams: i,
            onReverseCompleteScope: s,
            immediateRender: !1,
            useFrames: r,
            overwrite: 0
        });
    }, A.set = function(t, e) {
        return new A(t, 0, e);
    }, A.killTweensOf = A.killDelayedCallsTo = function(t, e) {
        for (var i = A.getTweensOf(t), s = i.length; --s > -1; ) i[s]._kill(e, t);
    }, A.getTweensOf = function(t) {
        if (null == t) return [];
        t = "string" != typeof t ? t : A.selector(t) || t;
        var e, i, s, r;
        if ((t instanceof Array || C(t)) && "number" != typeof t[0]) {
            for (e = t.length, i = []; --e > -1; ) i = i.concat(A.getTweensOf(t[e]));
            for (e = i.length; --e > -1; ) for (r = i[e], s = e; --s > -1; ) r === i[s] && i.splice(e, 1);
        } else for (i = z(t).concat(), e = i.length; --e > -1; ) i[e]._gc && i.splice(e, 1);
        return i;
    };
    var j = c("plugins.TweenPlugin", function(t, e) {
        this._overwriteProps = (t || "").split(","), this._propName = this._overwriteProps[0], 
        this._priority = e || 0, this._super = j.prototype;
    }, !0);
    if (s = j.prototype, j.version = "1.10.1", j.API = 2, s._firstPT = null, s._addTween = function(t, e, i, s, r, n) {
        var a, o;
        return null != s && (a = "number" == typeof s || "=" !== s.charAt(1) ? Number(s) - i : parseInt(s.charAt(0) + "1", 10) * Number(s.substr(2))) ? (this._firstPT = o = {
            _next: this._firstPT,
            t: t,
            p: e,
            s: i,
            c: a,
            f: "function" == typeof t[e],
            n: r || e,
            r: n
        }, o._next && (o._next._prev = o), o) : void 0;
    }, s.setRatio = function(t) {
        for (var e, i = this._firstPT, s = 1e-6; i; ) e = i.c * t + i.s, i.r ? e = 0 | e + (e > 0 ? .5 : -.5) : s > e && e > -s && (e = 0), 
        i.f ? i.t[i.p](e) : i.t[i.p] = e, i = i._next;
    }, s._kill = function(t) {
        var e, i = this._overwriteProps, s = this._firstPT;
        if (null != t[this._propName]) this._overwriteProps = []; else for (e = i.length; --e > -1; ) null != t[i[e]] && i.splice(e, 1);
        for (;s; ) null != t[s.n] && (s._next && (s._next._prev = s._prev), s._prev ? (s._prev._next = s._next, 
        s._prev = null) : this._firstPT === s && (this._firstPT = s._next)), s = s._next;
        return !1;
    }, s._roundProps = function(t, e) {
        for (var i = this._firstPT; i; ) (t[this._propName] || null != i.n && t[i.n.split(this._propName + "_").join("")]) && (i.r = e), 
        i = i._next;
    }, A._onPluginEvent = function(t, e) {
        var i, s, r, n, a, o = e._firstPT;
        if ("_onInitAllProps" === t) {
            for (;o; ) {
                for (a = o._next, s = r; s && s.pr > o.pr; ) s = s._next;
                (o._prev = s ? s._prev : n) ? o._prev._next = o : r = o, (o._next = s) ? s._prev = o : n = o, 
                o = a;
            }
            o = e._firstPT = r;
        }
        for (;o; ) o.pg && "function" == typeof o.t[t] && o.t[t]() && (i = !0), o = o._next;
        return i;
    }, j.activate = function(t) {
        for (var e = t.length; --e > -1; ) t[e].API === j.API && (M[new t[e]()._propName] = t[e]);
        return !0;
    }, f.plugin = function(t) {
        if (!(t && t.propName && t.init && t.API)) throw "illegal plugin definition.";
        var e, i = t.propName, s = t.priority || 0, r = t.overwriteProps, n = {
            init: "_onInitTween",
            set: "setRatio",
            kill: "_kill",
            round: "_roundProps",
            initAll: "_onInitAllProps"
        }, a = c("plugins." + i.charAt(0).toUpperCase() + i.substr(1) + "Plugin", function() {
            j.call(this, i, s), this._overwriteProps = r || [];
        }, t.global === !0), o = a.prototype = new j(i);
        o.constructor = a, a.API = t.API;
        for (e in n) "function" == typeof t[e] && (o[n[e]] = t[e]);
        return a.version = t.version, j.activate([ a ]), a;
    }, e = t._gsQueue) {
        for (i = 0; e.length > i; i++) e[i]();
        for (s in u) u[s].func || t.console.log("GSAP encountered missing dependency: com.greensock." + s);
    }
    n = !1;
}(window);

define("tweenmax", function(global) {
    return function() {
        var ret, fn;
        return ret || global.TweenMax;
    };
}(this));

(function() {
    var root = this;
    var previousUnderscore = root._;
    var breaker = {};
    var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;
    var push = ArrayProto.push, slice = ArrayProto.slice, concat = ArrayProto.concat, toString = ObjProto.toString, hasOwnProperty = ObjProto.hasOwnProperty;
    var nativeForEach = ArrayProto.forEach, nativeMap = ArrayProto.map, nativeReduce = ArrayProto.reduce, nativeReduceRight = ArrayProto.reduceRight, nativeFilter = ArrayProto.filter, nativeEvery = ArrayProto.every, nativeSome = ArrayProto.some, nativeIndexOf = ArrayProto.indexOf, nativeLastIndexOf = ArrayProto.lastIndexOf, nativeIsArray = Array.isArray, nativeKeys = Object.keys, nativeBind = FuncProto.bind;
    var _ = function(obj) {
        if (obj instanceof _) return obj;
        if (!(this instanceof _)) return new _(obj);
        this._wrapped = obj;
    };
    if (typeof exports !== "undefined") {
        if (typeof module !== "undefined" && module.exports) exports = module.exports = _;
        exports._ = _;
    } else root._ = _;
    _.VERSION = "1.5.2";
    var each = _.each = _.forEach = function(obj, iterator, context) {
        if (obj == null) return;
        if (nativeForEach && obj.forEach === nativeForEach) obj.forEach(iterator, context); else if (obj.length === +obj.length) for (var i = 0, length = obj.length; i < length; i++) {
            if (iterator.call(context, obj[i], i, obj) === breaker) return;
        } else {
            var keys = _.keys(obj);
            for (var i = 0, length = keys.length; i < length; i++) if (iterator.call(context, obj[keys[i]], keys[i], obj) === breaker) return;
        }
    };
    _.map = _.collect = function(obj, iterator, context) {
        var results = [];
        if (obj == null) return results;
        if (nativeMap && obj.map === nativeMap) return obj.map(iterator, context);
        each(obj, function(value, index, list) {
            results.push(iterator.call(context, value, index, list));
        });
        return results;
    };
    var reduceError = "Reduce of empty array with no initial value";
    _.reduce = _.foldl = _.inject = function(obj, iterator, memo, context) {
        var initial = arguments.length > 2;
        if (obj == null) obj = [];
        if (nativeReduce && obj.reduce === nativeReduce) {
            if (context) iterator = _.bind(iterator, context);
            return initial ? obj.reduce(iterator, memo) : obj.reduce(iterator);
        }
        each(obj, function(value, index, list) {
            if (!initial) {
                memo = value;
                initial = true;
            } else memo = iterator.call(context, memo, value, index, list);
        });
        if (!initial) throw new TypeError(reduceError);
        return memo;
    };
    _.reduceRight = _.foldr = function(obj, iterator, memo, context) {
        var initial = arguments.length > 2;
        if (obj == null) obj = [];
        if (nativeReduceRight && obj.reduceRight === nativeReduceRight) {
            if (context) iterator = _.bind(iterator, context);
            return initial ? obj.reduceRight(iterator, memo) : obj.reduceRight(iterator);
        }
        var length = obj.length;
        if (length !== +length) {
            var keys = _.keys(obj);
            length = keys.length;
        }
        each(obj, function(value, index, list) {
            index = keys ? keys[--length] : --length;
            if (!initial) {
                memo = obj[index];
                initial = true;
            } else memo = iterator.call(context, memo, obj[index], index, list);
        });
        if (!initial) throw new TypeError(reduceError);
        return memo;
    };
    _.find = _.detect = function(obj, iterator, context) {
        var result;
        any(obj, function(value, index, list) {
            if (iterator.call(context, value, index, list)) {
                result = value;
                return true;
            }
        });
        return result;
    };
    _.filter = _.select = function(obj, iterator, context) {
        var results = [];
        if (obj == null) return results;
        if (nativeFilter && obj.filter === nativeFilter) return obj.filter(iterator, context);
        each(obj, function(value, index, list) {
            if (iterator.call(context, value, index, list)) results.push(value);
        });
        return results;
    };
    _.reject = function(obj, iterator, context) {
        return _.filter(obj, function(value, index, list) {
            return !iterator.call(context, value, index, list);
        }, context);
    };
    _.every = _.all = function(obj, iterator, context) {
        iterator || (iterator = _.identity);
        var result = true;
        if (obj == null) return result;
        if (nativeEvery && obj.every === nativeEvery) return obj.every(iterator, context);
        each(obj, function(value, index, list) {
            if (!(result = result && iterator.call(context, value, index, list))) return breaker;
        });
        return !!result;
    };
    var any = _.some = _.any = function(obj, iterator, context) {
        iterator || (iterator = _.identity);
        var result = false;
        if (obj == null) return result;
        if (nativeSome && obj.some === nativeSome) return obj.some(iterator, context);
        each(obj, function(value, index, list) {
            if (result || (result = iterator.call(context, value, index, list))) return breaker;
        });
        return !!result;
    };
    _.contains = _.include = function(obj, target) {
        if (obj == null) return false;
        if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1;
        return any(obj, function(value) {
            return value === target;
        });
    };
    _.invoke = function(obj, method) {
        var args = slice.call(arguments, 2);
        var isFunc = _.isFunction(method);
        return _.map(obj, function(value) {
            return (isFunc ? method : value[method]).apply(value, args);
        });
    };
    _.pluck = function(obj, key) {
        return _.map(obj, function(value) {
            return value[key];
        });
    };
    _.where = function(obj, attrs, first) {
        if (_.isEmpty(attrs)) return first ? void 0 : [];
        return _[first ? "find" : "filter"](obj, function(value) {
            for (var key in attrs) if (attrs[key] !== value[key]) return false;
            return true;
        });
    };
    _.findWhere = function(obj, attrs) {
        return _.where(obj, attrs, true);
    };
    _.max = function(obj, iterator, context) {
        if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) return Math.max.apply(Math, obj);
        if (!iterator && _.isEmpty(obj)) return -Infinity;
        var result = {
            computed: -Infinity,
            value: -Infinity
        };
        each(obj, function(value, index, list) {
            var computed = iterator ? iterator.call(context, value, index, list) : value;
            computed > result.computed && (result = {
                value: value,
                computed: computed
            });
        });
        return result.value;
    };
    _.min = function(obj, iterator, context) {
        if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) return Math.min.apply(Math, obj);
        if (!iterator && _.isEmpty(obj)) return Infinity;
        var result = {
            computed: Infinity,
            value: Infinity
        };
        each(obj, function(value, index, list) {
            var computed = iterator ? iterator.call(context, value, index, list) : value;
            computed < result.computed && (result = {
                value: value,
                computed: computed
            });
        });
        return result.value;
    };
    _.shuffle = function(obj) {
        var rand;
        var index = 0;
        var shuffled = [];
        each(obj, function(value) {
            rand = _.random(index++);
            shuffled[index - 1] = shuffled[rand];
            shuffled[rand] = value;
        });
        return shuffled;
    };
    _.sample = function(obj, n, guard) {
        if (arguments.length < 2 || guard) return obj[_.random(obj.length - 1)];
        return _.shuffle(obj).slice(0, Math.max(0, n));
    };
    var lookupIterator = function(value) {
        return _.isFunction(value) ? value : function(obj) {
            return obj[value];
        };
    };
    _.sortBy = function(obj, value, context) {
        var iterator = lookupIterator(value);
        return _.pluck(_.map(obj, function(value, index, list) {
            return {
                value: value,
                index: index,
                criteria: iterator.call(context, value, index, list)
            };
        }).sort(function(left, right) {
            var a = left.criteria;
            var b = right.criteria;
            if (a !== b) {
                if (a > b || a === void 0) return 1;
                if (a < b || b === void 0) return -1;
            }
            return left.index - right.index;
        }), "value");
    };
    var group = function(behavior) {
        return function(obj, value, context) {
            var result = {};
            var iterator = value == null ? _.identity : lookupIterator(value);
            each(obj, function(value, index) {
                var key = iterator.call(context, value, index, obj);
                behavior(result, key, value);
            });
            return result;
        };
    };
    _.groupBy = group(function(result, key, value) {
        (_.has(result, key) ? result[key] : result[key] = []).push(value);
    });
    _.indexBy = group(function(result, key, value) {
        result[key] = value;
    });
    _.countBy = group(function(result, key) {
        _.has(result, key) ? result[key]++ : result[key] = 1;
    });
    _.sortedIndex = function(array, obj, iterator, context) {
        iterator = iterator == null ? _.identity : lookupIterator(iterator);
        var value = iterator.call(context, obj);
        var low = 0, high = array.length;
        while (low < high) {
            var mid = low + high >>> 1;
            iterator.call(context, array[mid]) < value ? low = mid + 1 : high = mid;
        }
        return low;
    };
    _.toArray = function(obj) {
        if (!obj) return [];
        if (_.isArray(obj)) return slice.call(obj);
        if (obj.length === +obj.length) return _.map(obj, _.identity);
        return _.values(obj);
    };
    _.size = function(obj) {
        if (obj == null) return 0;
        return obj.length === +obj.length ? obj.length : _.keys(obj).length;
    };
    _.first = _.head = _.take = function(array, n, guard) {
        if (array == null) return void 0;
        return n == null || guard ? array[0] : slice.call(array, 0, n);
    };
    _.initial = function(array, n, guard) {
        return slice.call(array, 0, array.length - (n == null || guard ? 1 : n));
    };
    _.last = function(array, n, guard) {
        if (array == null) return void 0;
        if (n == null || guard) return array[array.length - 1]; else return slice.call(array, Math.max(array.length - n, 0));
    };
    _.rest = _.tail = _.drop = function(array, n, guard) {
        return slice.call(array, n == null || guard ? 1 : n);
    };
    _.compact = function(array) {
        return _.filter(array, _.identity);
    };
    var flatten = function(input, shallow, output) {
        if (shallow && _.every(input, _.isArray)) return concat.apply(output, input);
        each(input, function(value) {
            if (_.isArray(value) || _.isArguments(value)) shallow ? push.apply(output, value) : flatten(value, shallow, output); else output.push(value);
        });
        return output;
    };
    _.flatten = function(array, shallow) {
        return flatten(array, shallow, []);
    };
    _.without = function(array) {
        return _.difference(array, slice.call(arguments, 1));
    };
    _.uniq = _.unique = function(array, isSorted, iterator, context) {
        if (_.isFunction(isSorted)) {
            context = iterator;
            iterator = isSorted;
            isSorted = false;
        }
        var initial = iterator ? _.map(array, iterator, context) : array;
        var results = [];
        var seen = [];
        each(initial, function(value, index) {
            if (isSorted ? !index || seen[seen.length - 1] !== value : !_.contains(seen, value)) {
                seen.push(value);
                results.push(array[index]);
            }
        });
        return results;
    };
    _.union = function() {
        return _.uniq(_.flatten(arguments, true));
    };
    _.intersection = function(array) {
        var rest = slice.call(arguments, 1);
        return _.filter(_.uniq(array), function(item) {
            return _.every(rest, function(other) {
                return _.indexOf(other, item) >= 0;
            });
        });
    };
    _.difference = function(array) {
        var rest = concat.apply(ArrayProto, slice.call(arguments, 1));
        return _.filter(array, function(value) {
            return !_.contains(rest, value);
        });
    };
    _.zip = function() {
        var length = _.max(_.pluck(arguments, "length").concat(0));
        var results = new Array(length);
        for (var i = 0; i < length; i++) results[i] = _.pluck(arguments, "" + i);
        return results;
    };
    _.object = function(list, values) {
        if (list == null) return {};
        var result = {};
        for (var i = 0, length = list.length; i < length; i++) if (values) result[list[i]] = values[i]; else result[list[i][0]] = list[i][1];
        return result;
    };
    _.indexOf = function(array, item, isSorted) {
        if (array == null) return -1;
        var i = 0, length = array.length;
        if (isSorted) if (typeof isSorted == "number") i = isSorted < 0 ? Math.max(0, length + isSorted) : isSorted; else {
            i = _.sortedIndex(array, item);
            return array[i] === item ? i : -1;
        }
        if (nativeIndexOf && array.indexOf === nativeIndexOf) return array.indexOf(item, isSorted);
        for (;i < length; i++) if (array[i] === item) return i;
        return -1;
    };
    _.lastIndexOf = function(array, item, from) {
        if (array == null) return -1;
        var hasIndex = from != null;
        if (nativeLastIndexOf && array.lastIndexOf === nativeLastIndexOf) return hasIndex ? array.lastIndexOf(item, from) : array.lastIndexOf(item);
        var i = hasIndex ? from : array.length;
        while (i--) if (array[i] === item) return i;
        return -1;
    };
    _.range = function(start, stop, step) {
        if (arguments.length <= 1) {
            stop = start || 0;
            start = 0;
        }
        step = arguments[2] || 1;
        var length = Math.max(Math.ceil((stop - start) / step), 0);
        var idx = 0;
        var range = new Array(length);
        while (idx < length) {
            range[idx++] = start;
            start += step;
        }
        return range;
    };
    var ctor = function() {};
    _.bind = function(func, context) {
        var args, bound;
        if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
        if (!_.isFunction(func)) throw new TypeError();
        args = slice.call(arguments, 2);
        return bound = function() {
            if (!(this instanceof bound)) return func.apply(context, args.concat(slice.call(arguments)));
            ctor.prototype = func.prototype;
            var self = new ctor();
            ctor.prototype = null;
            var result = func.apply(self, args.concat(slice.call(arguments)));
            if (Object(result) === result) return result;
            return self;
        };
    };
    _.partial = function(func) {
        var args = slice.call(arguments, 1);
        return function() {
            return func.apply(this, args.concat(slice.call(arguments)));
        };
    };
    _.bindAll = function(obj) {
        var funcs = slice.call(arguments, 1);
        if (funcs.length === 0) throw new Error("bindAll must be passed function names");
        each(funcs, function(f) {
            obj[f] = _.bind(obj[f], obj);
        });
        return obj;
    };
    _.memoize = function(func, hasher) {
        var memo = {};
        hasher || (hasher = _.identity);
        return function() {
            var key = hasher.apply(this, arguments);
            return _.has(memo, key) ? memo[key] : memo[key] = func.apply(this, arguments);
        };
    };
    _.delay = function(func, wait) {
        var args = slice.call(arguments, 2);
        return setTimeout(function() {
            return func.apply(null, args);
        }, wait);
    };
    _.defer = function(func) {
        return _.delay.apply(_, [ func, 1 ].concat(slice.call(arguments, 1)));
    };
    _.throttle = function(func, wait, options) {
        var context, args, result;
        var timeout = null;
        var previous = 0;
        options || (options = {});
        var later = function() {
            previous = options.leading === false ? 0 : new Date();
            timeout = null;
            result = func.apply(context, args);
        };
        return function() {
            var now = new Date();
            if (!previous && options.leading === false) previous = now;
            var remaining = wait - (now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0) {
                clearTimeout(timeout);
                timeout = null;
                previous = now;
                result = func.apply(context, args);
            } else if (!timeout && options.trailing !== false) timeout = setTimeout(later, remaining);
            return result;
        };
    };
    _.debounce = function(func, wait, immediate) {
        var timeout, args, context, timestamp, result;
        return function() {
            context = this;
            args = arguments;
            timestamp = new Date();
            var later = function() {
                var last = new Date() - timestamp;
                if (last < wait) timeout = setTimeout(later, wait - last); else {
                    timeout = null;
                    if (!immediate) result = func.apply(context, args);
                }
            };
            var callNow = immediate && !timeout;
            if (!timeout) timeout = setTimeout(later, wait);
            if (callNow) result = func.apply(context, args);
            return result;
        };
    };
    _.once = function(func) {
        var ran = false, memo;
        return function() {
            if (ran) return memo;
            ran = true;
            memo = func.apply(this, arguments);
            func = null;
            return memo;
        };
    };
    _.wrap = function(func, wrapper) {
        return function() {
            var args = [ func ];
            push.apply(args, arguments);
            return wrapper.apply(this, args);
        };
    };
    _.compose = function() {
        var funcs = arguments;
        return function() {
            var args = arguments;
            for (var i = funcs.length - 1; i >= 0; i--) args = [ funcs[i].apply(this, args) ];
            return args[0];
        };
    };
    _.after = function(times, func) {
        return function() {
            if (--times < 1) return func.apply(this, arguments);
        };
    };
    _.keys = nativeKeys || function(obj) {
        if (obj !== Object(obj)) throw new TypeError("Invalid object");
        var keys = [];
        for (var key in obj) if (_.has(obj, key)) keys.push(key);
        return keys;
    };
    _.values = function(obj) {
        var keys = _.keys(obj);
        var length = keys.length;
        var values = new Array(length);
        for (var i = 0; i < length; i++) values[i] = obj[keys[i]];
        return values;
    };
    _.pairs = function(obj) {
        var keys = _.keys(obj);
        var length = keys.length;
        var pairs = new Array(length);
        for (var i = 0; i < length; i++) pairs[i] = [ keys[i], obj[keys[i]] ];
        return pairs;
    };
    _.invert = function(obj) {
        var result = {};
        var keys = _.keys(obj);
        for (var i = 0, length = keys.length; i < length; i++) result[obj[keys[i]]] = keys[i];
        return result;
    };
    _.functions = _.methods = function(obj) {
        var names = [];
        for (var key in obj) if (_.isFunction(obj[key])) names.push(key);
        return names.sort();
    };
    _.extend = function(obj) {
        each(slice.call(arguments, 1), function(source) {
            if (source) for (var prop in source) obj[prop] = source[prop];
        });
        return obj;
    };
    _.pick = function(obj) {
        var copy = {};
        var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
        each(keys, function(key) {
            if (key in obj) copy[key] = obj[key];
        });
        return copy;
    };
    _.omit = function(obj) {
        var copy = {};
        var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
        for (var key in obj) if (!_.contains(keys, key)) copy[key] = obj[key];
        return copy;
    };
    _.defaults = function(obj) {
        each(slice.call(arguments, 1), function(source) {
            if (source) for (var prop in source) if (obj[prop] === void 0) obj[prop] = source[prop];
        });
        return obj;
    };
    _.clone = function(obj) {
        if (!_.isObject(obj)) return obj;
        return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
    };
    _.tap = function(obj, interceptor) {
        interceptor(obj);
        return obj;
    };
    var eq = function(a, b, aStack, bStack) {
        if (a === b) return a !== 0 || 1 / a == 1 / b;
        if (a == null || b == null) return a === b;
        if (a instanceof _) a = a._wrapped;
        if (b instanceof _) b = b._wrapped;
        var className = toString.call(a);
        if (className != toString.call(b)) return false;
        switch (className) {
          case "[object String]":
            return a == String(b);

          case "[object Number]":
            return a != +a ? b != +b : a == 0 ? 1 / a == 1 / b : a == +b;

          case "[object Date]":
          case "[object Boolean]":
            return +a == +b;

          case "[object RegExp]":
            return a.source == b.source && a.global == b.global && a.multiline == b.multiline && a.ignoreCase == b.ignoreCase;
        }
        if (typeof a != "object" || typeof b != "object") return false;
        var length = aStack.length;
        while (length--) if (aStack[length] == a) return bStack[length] == b;
        var aCtor = a.constructor, bCtor = b.constructor;
        if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor && _.isFunction(bCtor) && bCtor instanceof bCtor)) return false;
        aStack.push(a);
        bStack.push(b);
        var size = 0, result = true;
        if (className == "[object Array]") {
            size = a.length;
            result = size == b.length;
            if (result) while (size--) if (!(result = eq(a[size], b[size], aStack, bStack))) break;
        } else {
            for (var key in a) if (_.has(a, key)) {
                size++;
                if (!(result = _.has(b, key) && eq(a[key], b[key], aStack, bStack))) break;
            }
            if (result) {
                for (key in b) if (_.has(b, key) && !size--) break;
                result = !size;
            }
        }
        aStack.pop();
        bStack.pop();
        return result;
    };
    _.isEqual = function(a, b) {
        return eq(a, b, [], []);
    };
    _.isEmpty = function(obj) {
        if (obj == null) return true;
        if (_.isArray(obj) || _.isString(obj)) return obj.length === 0;
        for (var key in obj) if (_.has(obj, key)) return false;
        return true;
    };
    _.isElement = function(obj) {
        return !!(obj && obj.nodeType === 1);
    };
    _.isArray = nativeIsArray || function(obj) {
        return toString.call(obj) == "[object Array]";
    };
    _.isObject = function(obj) {
        return obj === Object(obj);
    };
    each([ "Arguments", "Function", "String", "Number", "Date", "RegExp" ], function(name) {
        _["is" + name] = function(obj) {
            return toString.call(obj) == "[object " + name + "]";
        };
    });
    if (!_.isArguments(arguments)) _.isArguments = function(obj) {
        return !!(obj && _.has(obj, "callee"));
    };
    if (typeof /./ !== "function") _.isFunction = function(obj) {
        return typeof obj === "function";
    };
    _.isFinite = function(obj) {
        return isFinite(obj) && !isNaN(parseFloat(obj));
    };
    _.isNaN = function(obj) {
        return _.isNumber(obj) && obj != +obj;
    };
    _.isBoolean = function(obj) {
        return obj === true || obj === false || toString.call(obj) == "[object Boolean]";
    };
    _.isNull = function(obj) {
        return obj === null;
    };
    _.isUndefined = function(obj) {
        return obj === void 0;
    };
    _.has = function(obj, key) {
        return hasOwnProperty.call(obj, key);
    };
    _.noConflict = function() {
        root._ = previousUnderscore;
        return this;
    };
    _.identity = function(value) {
        return value;
    };
    _.times = function(n, iterator, context) {
        var accum = Array(Math.max(0, n));
        for (var i = 0; i < n; i++) accum[i] = iterator.call(context, i);
        return accum;
    };
    _.random = function(min, max) {
        if (max == null) {
            max = min;
            min = 0;
        }
        return min + Math.floor(Math.random() * (max - min + 1));
    };
    var entityMap = {
        escape: {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;"
        }
    };
    entityMap.unescape = _.invert(entityMap.escape);
    var entityRegexes = {
        escape: new RegExp("[" + _.keys(entityMap.escape).join("") + "]", "g"),
        unescape: new RegExp("(" + _.keys(entityMap.unescape).join("|") + ")", "g")
    };
    _.each([ "escape", "unescape" ], function(method) {
        _[method] = function(string) {
            if (string == null) return "";
            return ("" + string).replace(entityRegexes[method], function(match) {
                return entityMap[method][match];
            });
        };
    });
    _.result = function(object, property) {
        if (object == null) return void 0;
        var value = object[property];
        return _.isFunction(value) ? value.call(object) : value;
    };
    _.mixin = function(obj) {
        each(_.functions(obj), function(name) {
            var func = _[name] = obj[name];
            _.prototype[name] = function() {
                var args = [ this._wrapped ];
                push.apply(args, arguments);
                return result.call(this, func.apply(_, args));
            };
        });
    };
    var idCounter = 0;
    _.uniqueId = function(prefix) {
        var id = ++idCounter + "";
        return prefix ? prefix + id : id;
    };
    _.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var noMatch = /(.)^/;
    var escapes = {
        "'": "'",
        "\\": "\\",
        "\r": "r",
        "\n": "n",
        "	": "t",
        "\u2028": "u2028",
        "\u2029": "u2029"
    };
    var escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    _.template = function(text, data, settings) {
        var render;
        settings = _.defaults({}, settings, _.templateSettings);
        var matcher = new RegExp([ (settings.escape || noMatch).source, (settings.interpolate || noMatch).source, (settings.evaluate || noMatch).source ].join("|") + "|$", "g");
        var index = 0;
        var source = "__p+='";
        text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
            source += text.slice(index, offset).replace(escaper, function(match) {
                return "\\" + escapes[match];
            });
            if (escape) source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
            if (interpolate) source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
            if (evaluate) source += "';\n" + evaluate + "\n__p+='";
            index = offset + match.length;
            return match;
        });
        source += "';\n";
        if (!settings.variable) source = "with(obj||{}){\n" + source + "}\n";
        source = "var __t,__p='',__j=Array.prototype.join," + "print=function(){__p+=__j.call(arguments,'');};\n" + source + "return __p;\n";
        try {
            render = new Function(settings.variable || "obj", "_", source);
        } catch (e) {
            e.source = source;
            throw e;
        }
        if (data) return render(data, _);
        var template = function(data) {
            return render.call(this, data, _);
        };
        template.source = "function(" + (settings.variable || "obj") + "){\n" + source + "}";
        return template;
    };
    _.chain = function(obj) {
        return _(obj).chain();
    };
    var result = function(obj) {
        return this._chain ? _(obj).chain() : obj;
    };
    _.mixin(_);
    each([ "pop", "push", "reverse", "shift", "sort", "splice", "unshift" ], function(name) {
        var method = ArrayProto[name];
        _.prototype[name] = function() {
            var obj = this._wrapped;
            method.apply(obj, arguments);
            if ((name == "shift" || name == "splice") && obj.length === 0) delete obj[0];
            return result.call(this, obj);
        };
    });
    each([ "concat", "join", "slice" ], function(name) {
        var method = ArrayProto[name];
        _.prototype[name] = function() {
            return result.call(this, method.apply(this._wrapped, arguments));
        };
    });
    _.extend(_.prototype, {
        chain: function() {
            this._chain = true;
            return this;
        },
        value: function() {
            return this._wrapped;
        }
    });
}).call(this);

define("underscore", function(global) {
    return function() {
        var ret, fn;
        return ret || global._;
    };
}(this));

window.Modernizr = function(a, b, c) {
    function B(a) {
        j.cssText = a;
    }
    function C(a, b) {
        return B(m.join(a + ";") + (b || ""));
    }
    function D(a, b) {
        return typeof a === b;
    }
    function E(a, b) {
        return !!~("" + a).indexOf(b);
    }
    function F(a, b) {
        for (var d in a) {
            var e = a[d];
            if (!E(e, "-") && j[e] !== c) return b == "pfx" ? e : !0;
        }
        return !1;
    }
    function G(a, b, d) {
        for (var e in a) {
            var f = b[a[e]];
            if (f !== c) return d === !1 ? a[e] : D(f, "function") ? f.bind(d || b) : f;
        }
        return !1;
    }
    function H(a, b, c) {
        var d = a.charAt(0).toUpperCase() + a.slice(1), e = (a + " " + o.join(d + " ") + d).split(" ");
        return D(b, "string") || D(b, "undefined") ? F(e, b) : (e = (a + " " + p.join(d + " ") + d).split(" "), 
        G(e, b, c));
    }
    var d = "2.7.1", e = {}, f = !0, g = b.documentElement, h = "modernizr", i = b.createElement(h), j = i.style, k, l = {}.toString, m = " -webkit- -moz- -o- -ms- ".split(" "), n = "Webkit Moz O ms", o = n.split(" "), p = n.toLowerCase().split(" "), q = {
        svg: "http://www.w3.org/2000/svg"
    }, r = {}, s = {}, t = {}, u = [], v = u.slice, w, x = function(a, c, d, e) {
        var f, i, j, k, l = b.createElement("div"), m = b.body, n = m || b.createElement("body");
        if (parseInt(d, 10)) while (d--) j = b.createElement("div"), j.id = e ? e[d] : h + (d + 1), 
        l.appendChild(j);
        return f = [ "&#173;", '<style id="s', h, '">', a, "</style>" ].join(""), l.id = h, 
        (m ? l : n).innerHTML += f, n.appendChild(l), m || (n.style.background = "", n.style.overflow = "hidden", 
        k = g.style.overflow, g.style.overflow = "hidden", g.appendChild(n)), i = c(l, a), 
        m ? l.parentNode.removeChild(l) : (n.parentNode.removeChild(n), g.style.overflow = k), 
        !!i;
    }, y = function(b) {
        var c = a.matchMedia || a.msMatchMedia;
        if (c) return c(b).matches;
        var d;
        return x("@media " + b + " { #" + h + " { position: absolute; } }", function(b) {
            d = (a.getComputedStyle ? getComputedStyle(b, null) : b.currentStyle)["position"] == "absolute";
        }), d;
    }, z = {}.hasOwnProperty, A;
    !D(z, "undefined") && !D(z.call, "undefined") ? A = function(a, b) {
        return z.call(a, b);
    } : A = function(a, b) {
        return b in a && D(a.constructor.prototype[b], "undefined");
    }, Function.prototype.bind || (Function.prototype.bind = function(b) {
        var c = this;
        if (typeof c != "function") throw new TypeError();
        var d = v.call(arguments, 1), e = function() {
            if (this instanceof e) {
                var a = function() {};
                a.prototype = c.prototype;
                var f = new a(), g = c.apply(f, d.concat(v.call(arguments)));
                return Object(g) === g ? g : f;
            }
            return c.apply(b, d.concat(v.call(arguments)));
        };
        return e;
    }), r.canvas = function() {
        var a = b.createElement("canvas");
        return !!a.getContext && !!a.getContext("2d");
    }, r.touch = function() {
        var c;
        return "ontouchstart" in a || a.DocumentTouch && b instanceof DocumentTouch ? c = !0 : x([ "@media (", m.join("touch-enabled),("), h, ")", "{#modernizr{top:9px;position:absolute}}" ].join(""), function(a) {
            c = a.offsetTop === 9;
        }), c;
    }, r.geolocation = function() {
        return "geolocation" in navigator;
    }, r.cssanimations = function() {
        return H("animationName");
    }, r.csstransforms = function() {
        return !!H("transform");
    }, r.csstransforms3d = function() {
        var a = !!H("perspective");
        return a && "webkitPerspective" in g.style && x("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(b, c) {
            a = b.offsetLeft === 9 && b.offsetHeight === 3;
        }), a;
    }, r.csstransitions = function() {
        return H("transition");
    }, r.fontface = function() {
        var a;
        return x('@font-face {font-family:"font";src:url("https://")}', function(c, d) {
            var e = b.getElementById("smodernizr"), f = e.sheet || e.styleSheet, g = f ? f.cssRules && f.cssRules[0] ? f.cssRules[0].cssText : f.cssText || "" : "";
            a = /src/i.test(g) && g.indexOf(d.split(" ")[0]) === 0;
        }), a;
    }, r.video = function() {
        var a = b.createElement("video"), c = !1;
        try {
            if (c = !!a.canPlayType) c = new Boolean(c), c.ogg = a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), 
            c.h264 = a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), c.webm = a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, "");
        } catch (d) {}
        return c;
    }, r.audio = function() {
        var a = b.createElement("audio"), c = !1;
        try {
            if (c = !!a.canPlayType) c = new Boolean(c), c.ogg = a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""), 
            c.mp3 = a.canPlayType("audio/mpeg;").replace(/^no$/, ""), c.wav = a.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""), 
            c.m4a = (a.canPlayType("audio/x-m4a;") || a.canPlayType("audio/aac;")).replace(/^no$/, "");
        } catch (d) {}
        return c;
    }, r.webworkers = function() {
        return !!a.Worker;
    }, r.svg = function() {
        return !!b.createElementNS && !!b.createElementNS(q.svg, "svg").createSVGRect;
    }, r.inlinesvg = function() {
        var a = b.createElement("div");
        return a.innerHTML = "<svg/>", (a.firstChild && a.firstChild.namespaceURI) == q.svg;
    }, r.svgclippaths = function() {
        return !!b.createElementNS && /SVGClipPath/.test(l.call(b.createElementNS(q.svg, "clipPath")));
    };
    for (var I in r) A(r, I) && (w = I.toLowerCase(), e[w] = r[I](), u.push((e[w] ? "" : "no-") + w));
    return e.addTest = function(a, b) {
        if (typeof a == "object") for (var d in a) A(a, d) && e.addTest(d, a[d]); else {
            a = a.toLowerCase();
            if (e[a] !== c) return e;
            b = typeof b == "function" ? b() : b, typeof f != "undefined" && f && (g.className += " " + (b ? "" : "no-") + a), 
            e[a] = b;
        }
        return e;
    }, B(""), i = k = null, function(a, b) {
        function l(a, b) {
            var c = a.createElement("p"), d = a.getElementsByTagName("head")[0] || a.documentElement;
            return c.innerHTML = "x<style>" + b + "</style>", d.insertBefore(c.lastChild, d.firstChild);
        }
        function m() {
            var a = s.elements;
            return typeof a == "string" ? a.split(" ") : a;
        }
        function n(a) {
            var b = j[a[h]];
            return b || (b = {}, i++, a[h] = i, j[i] = b), b;
        }
        function o(a, c, d) {
            c || (c = b);
            if (k) return c.createElement(a);
            d || (d = n(c));
            var g;
            return d.cache[a] ? g = d.cache[a].cloneNode() : f.test(a) ? g = (d.cache[a] = d.createElem(a)).cloneNode() : g = d.createElem(a), 
            g.canHaveChildren && !e.test(a) && !g.tagUrn ? d.frag.appendChild(g) : g;
        }
        function p(a, c) {
            a || (a = b);
            if (k) return a.createDocumentFragment();
            c = c || n(a);
            var d = c.frag.cloneNode(), e = 0, f = m(), g = f.length;
            for (;e < g; e++) d.createElement(f[e]);
            return d;
        }
        function q(a, b) {
            b.cache || (b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment, 
            b.frag = b.createFrag()), a.createElement = function(c) {
                return s.shivMethods ? o(c, a, b) : b.createElem(c);
            }, a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + m().join().replace(/[\w\-]+/g, function(a) {
                return b.createElem(a), b.frag.createElement(a), 'c("' + a + '")';
            }) + ");return n}")(s, b.frag);
        }
        function r(a) {
            a || (a = b);
            var c = n(a);
            return s.shivCSS && !g && !c.hasCSS && (c.hasCSS = !!l(a, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), 
            k || q(a, c), a;
        }
        var c = "3.7.0", d = a.html5 || {}, e = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i, f = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i, g, h = "_html5shiv", i = 0, j = {}, k;
        (function() {
            try {
                var a = b.createElement("a");
                a.innerHTML = "<xyz></xyz>", g = "hidden" in a, k = a.childNodes.length == 1 || function() {
                    b.createElement("a");
                    var a = b.createDocumentFragment();
                    return typeof a.cloneNode == "undefined" || typeof a.createDocumentFragment == "undefined" || typeof a.createElement == "undefined";
                }();
            } catch (c) {
                g = !0, k = !0;
            }
        })();
        var s = {
            elements: d.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
            version: c,
            shivCSS: d.shivCSS !== !1,
            supportsUnknownElements: k,
            shivMethods: d.shivMethods !== !1,
            type: "default",
            shivDocument: r,
            createElement: o,
            createDocumentFragment: p
        };
        a.html5 = s, r(b);
    }(this, b), e._version = d, e._prefixes = m, e._domPrefixes = p, e._cssomPrefixes = o, 
    e.mq = y, e.testProp = function(a) {
        return F([ a ]);
    }, e.testAllProps = H, e.testStyles = x, g.className = g.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (f ? " js " + u.join(" ") : ""), 
    e;
}(this, this.document), function(a, b, c) {
    function d(a) {
        return "[object Function]" == o.call(a);
    }
    function e(a) {
        return "string" == typeof a;
    }
    function f() {}
    function g(a) {
        return !a || "loaded" == a || "complete" == a || "uninitialized" == a;
    }
    function h() {
        var a = p.shift();
        q = 1, a ? a.t ? m(function() {
            ("c" == a.t ? B.injectCss : B.injectJs)(a.s, 0, a.a, a.x, a.e, 1);
        }, 0) : (a(), h()) : q = 0;
    }
    function i(a, c, d, e, f, i, j) {
        function k(b) {
            if (!o && g(l.readyState) && (u.r = o = 1, !q && h(), l.onload = l.onreadystatechange = null, 
            b)) {
                "img" != a && m(function() {
                    t.removeChild(l);
                }, 50);
                for (var d in y[c]) y[c].hasOwnProperty(d) && y[c][d].onload();
            }
        }
        var j = j || B.errorTimeout, l = b.createElement(a), o = 0, r = 0, u = {
            t: d,
            s: c,
            e: f,
            a: i,
            x: j
        };
        1 === y[c] && (r = 1, y[c] = []), "object" == a ? l.data = c : (l.src = c, l.type = a), 
        l.width = l.height = "0", l.onerror = l.onload = l.onreadystatechange = function() {
            k.call(this, r);
        }, p.splice(e, 0, u), "img" != a && (r || 2 === y[c] ? (t.insertBefore(l, s ? null : n), 
        m(k, j)) : y[c].push(l));
    }
    function j(a, b, c, d, f) {
        return q = 0, b = b || "j", e(a) ? i("c" == b ? v : u, a, b, this.i++, c, d, f) : (p.splice(this.i++, 0, a), 
        1 == p.length && h()), this;
    }
    function k() {
        var a = B;
        return a.loader = {
            load: j,
            i: 0
        }, a;
    }
    var l = b.documentElement, m = a.setTimeout, n = b.getElementsByTagName("script")[0], o = {}.toString, p = [], q = 0, r = "MozAppearance" in l.style, s = r && !!b.createRange().compareNode, t = s ? l : n.parentNode, l = a.opera && "[object Opera]" == o.call(a.opera), l = !!b.attachEvent && !l, u = r ? "object" : l ? "script" : "img", v = l ? "script" : u, w = Array.isArray || function(a) {
        return "[object Array]" == o.call(a);
    }, x = [], y = {}, z = {
        timeout: function(a, b) {
            return b.length && (a.timeout = b[0]), a;
        }
    }, A, B;
    B = function(a) {
        function b(a) {
            var a = a.split("!"), b = x.length, c = a.pop(), d = a.length, c = {
                url: c,
                origUrl: c,
                prefixes: a
            }, e, f, g;
            for (f = 0; f < d; f++) g = a[f].split("="), (e = z[g.shift()]) && (c = e(c, g));
            for (f = 0; f < b; f++) c = x[f](c);
            return c;
        }
        function g(a, e, f, g, h) {
            var i = b(a), j = i.autoCallback;
            i.url.split(".").pop().split("?").shift(), i.bypass || (e && (e = d(e) ? e : e[a] || e[g] || e[a.split("/").pop().split("?")[0]]), 
            i.instead ? i.instead(a, e, f, g, h) : (y[i.url] ? i.noexec = !0 : y[i.url] = 1, 
            f.load(i.url, i.forceCSS || !i.forceJS && "css" == i.url.split(".").pop().split("?").shift() ? "c" : c, i.noexec, i.attrs, i.timeout), 
            (d(e) || d(j)) && f.load(function() {
                k(), e && e(i.origUrl, h, g), j && j(i.origUrl, h, g), y[i.url] = 2;
            })));
        }
        function h(a, b) {
            function c(a, c) {
                if (a) if (e(a)) c || (j = function() {
                    var a = [].slice.call(arguments);
                    k.apply(this, a), l();
                }), g(a, j, b, 0, h); else {
                    if (Object(a) === a) for (n in m = function() {
                        var b = 0, c;
                        for (c in a) a.hasOwnProperty(c) && b++;
                        return b;
                    }(), a) a.hasOwnProperty(n) && (!c && !--m && (d(j) ? j = function() {
                        var a = [].slice.call(arguments);
                        k.apply(this, a), l();
                    } : j[n] = function(a) {
                        return function() {
                            var b = [].slice.call(arguments);
                            a && a.apply(this, b), l();
                        };
                    }(k[n])), g(a[n], j, b, n, h));
                } else !c && l();
            }
            var h = !!a.test, i = a.load || a.both, j = a.callback || f, k = j, l = a.complete || f, m, n;
            c(h ? a.yep : a.nope, !!i), i && c(i);
        }
        var i, j, l = this.yepnope.loader;
        if (e(a)) g(a, 0, l, 0); else if (w(a)) for (i = 0; i < a.length; i++) j = a[i], 
        e(j) ? g(j, 0, l, 0) : w(j) ? B(j) : Object(j) === j && h(j, l); else Object(a) === a && h(a, l);
    }, B.addPrefix = function(a, b) {
        z[a] = b;
    }, B.addFilter = function(a) {
        x.push(a);
    }, B.errorTimeout = 1e4, null == b.readyState && b.addEventListener && (b.readyState = "loading", 
    b.addEventListener("DOMContentLoaded", A = function() {
        b.removeEventListener("DOMContentLoaded", A, 0), b.readyState = "complete";
    }, 0)), a.yepnope = k(), a.yepnope.executeStack = h, a.yepnope.injectJs = function(a, c, d, e, i, j) {
        var k = b.createElement("script"), l, o, e = e || B.errorTimeout;
        k.src = a;
        for (o in d) k.setAttribute(o, d[o]);
        c = j ? h : c || f, k.onreadystatechange = k.onload = function() {
            !l && g(k.readyState) && (l = 1, c(), k.onload = k.onreadystatechange = null);
        }, m(function() {
            l || (l = 1, c(1));
        }, e), i ? k.onload() : n.parentNode.insertBefore(k, n);
    }, a.yepnope.injectCss = function(a, c, d, e, g, i) {
        var e = b.createElement("link"), j, c = i ? h : c || f;
        e.href = a, e.rel = "stylesheet", e.type = "text/css";
        for (j in d) e.setAttribute(j, d[j]);
        g || (n.parentNode.insertBefore(e, n), m(c, 0));
    };
}(this, document), Modernizr.load = function() {
    yepnope.apply(window, [].slice.call(arguments, 0));
};

define("modernizr", function(global) {
    return function() {
        var ret, fn;
        return ret || global.Modernizr;
    };
}(this));

define("modules/controllers/slideset", [ "jquery", "modules/definitions/standardmodule", "tweenmax", "underscore", "modernizr" ], function($, parentModel, TweenMax, _, Modernizr) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "slideset",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv"),
            childObjects: undefined,
            currentslide: 0,
            totalslides: undefined,
            loopslides: false,
            toplevelZ: 500,
            autoZ: true,
            child_stack: [],
            child_Zstack: [],
            child_Mstack: [],
            child_offstack: [],
            child_centerstack: [],
            child_stackpile_left: [],
            child_stackpile_right: [],
            errorfunctions: {
                childchange: function(data) {
                    console.log("error: same index");
                }
            },
            interval: undefined,
            busy: false,
            unfocusALL: true,
            childtrack: function(index) {
                var tracking = function(index) {
                    console.log("some tracking function you defined for scene# " + index);
                };
                if (index != this.lasttrack) {
                    tracking(index);
                    this.lasttrack = index;
                }
            },
            childinit: function(index, currentslide) {},
            childchange: function(index, o) {
                var self = this;
                self.parent._setstacks(index);
                self.parent._getZorder(index);
                if (self.parent._var().autoZ) self.parent._putZorder(index);
                if (this.currentslide === index) this.errorfunctions["childchange"](); else {
                    var passVars = {
                        index: index,
                        Zindex: self.child_Zstack,
                        Multiplier: self.child_Mstack
                    };
                    focusvars = $.extend(passVars, o);
                    this.child_focus(focusvars);
                    if (this.unfocusALL) {
                        $.each(self.parent._var().child_stackpile_left, function(offset, value) {
                            var leftvars = $.extend({
                                unfocusedindex: value,
                                offset: offset
                            }, passVars);
                            leftvars = $.extend(leftvars, o);
                            self.parent._var().child_unfocus(leftvars);
                        });
                        $.each(self.parent._var().child_stackpile_right, function(offset, value) {
                            var rightvars = $.extend({
                                unfocusedindex: value,
                                offset: offset
                            }, passVars);
                            rightvars = $.extend(rightvars, o);
                            self.parent._var().child_unfocus(rightvars);
                        });
                    } else {
                        var unfocusVars = $.extend({
                            unfocusedindex: this.currentslide
                        }, passVars);
                        this.child_unfocus(unfocusVars);
                    }
                    this.currentslide = index;
                }
            },
            child_focus: function(o) {
                var argz = {
                    Zindex: undefined,
                    Multiplier: undefined,
                    index: undefined,
                    speed: .5,
                    tweenvars: {
                        x: 0,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    }
                };
                argz = $.extend(argz, o);
                var tempObject = $(this.childObjects[argz.index]);
                argz = $.extend(argz, {
                    object: tempObject
                });
                if (Modernizr.csstransitions) TweenMax.to(argz.object, argz.speed, argz.tweenvars); else argz.object.show();
            },
            child_unfocus: function(o) {
                var argz = {
                    Zindex: undefined,
                    Multiplier: undefined,
                    index: undefined,
                    unfocusedindex: undefined,
                    speed: 1,
                    offset: 0,
                    tweenvarsIN: {
                        x: -400,
                        y: 0,
                        rotation: 10,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: .5,
                        opacity: 0
                    },
                    tweenvarsOUT: {
                        x: 400,
                        y: 0,
                        rotation: -10,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: .5,
                        opacity: 0
                    }
                };
                argz = $.extend(argz, o);
                var tempObject = $(this.childObjects[argz.unfocusedindex]);
                argz = $.extend(argz, {
                    object: tempObject
                });
                argz.tweenvarsIN.y -= 30 * argz.offset;
                argz.tweenvarsOUT.y -= 30 * argz.offset;
                argz.tweenvarsIN.x += 10 * argz.offset * 2 * argz.offset;
                argz.tweenvarsOUT.x -= 10 * argz.offset * 2 * argz.offset;
                argz.tweenvarsIN.rotation += 15 * argz.offset;
                argz.tweenvarsOUT.rotation += 15 * argz.offset;
                argz.tweenvarsIN.opacity += .9 - .15 * argz.offset;
                argz.tweenvarsOUT.opacity += .9 - .15 * argz.offset;
                if (Modernizr.csstransitions) if (argz.unfocusedindex < argz.index) TweenMax.to(argz.object, argz.speed, argz.tweenvarsIN); else TweenMax.to(argz.object, argz.speed, argz.tweenvarsOUT); else argz.object.hide();
            },
            _multijump: function(index) {
                var parent = this;
                var counter = {
                    tempvar: parent.currentslide
                };
                var lastindex = parent.currentslide;
                var difference = Math.abs(index - parent.currentslide);
                var animtime = difference * .2;
                TweenMax.to(counter, animtime, {
                    tempvar: index,
                    onUpdate: function() {
                        var targetvalue = Math.ceil(counter.tempvar);
                        if (targetvalue != lastindex) {
                            parent.childchange(targetvalue);
                            lastindex = targetvalue;
                        }
                    }
                });
            }
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onQuickjump: function(o) {
                    if (o.senderID == myID) parent.quickjump(o.data.index);
                },
                onJump: function(o) {
                    if (o.senderID == myID) parent.jump(o.data.index);
                },
                onPagejump: function(o) {
                    if (o.senderID == myID) parent.pagejump(o.data.index);
                },
                onNext: function(o) {
                    if (o.senderID == myID) parent.next();
                },
                onPrev: function(o) {
                    if (o.senderID == myID) parent.prev();
                }
            };
        }());
    };
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this._childrentochildObjects();
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype._childrentochildObjects = function() {
        var parent = this;
        parent._var({
            childObjects: parent._var().target.children()
        });
        var childObjects = parent._var().childObjects;
        parent._var({
            totalslides: childObjects.length,
            child_stack: [],
            child_offstack: [],
            child_centerstack: [],
            child_stackpile_left: [],
            child_stackpile_right: []
        });
        $.each(childObjects, function(index, value) {
            parent._var().child_stack.push(index);
            parent._var().childinit.call($(value), index, parent._var().currentslide);
        });
        parent._setstacks(parent._var().currentslide);
        parent._getZorder(parent._var().currentslide);
        parent._var().child_focus({
            index: parent._var().currentslide,
            speed: 0,
            Zindex: parent._var().child_Zstack,
            Multiplier: parent._var().child_Mstack
        });
        if (parent._var().autoZ) parent._putZorder(parent._var().currentslide);
        $.each(parent._var().child_stackpile_left, function(offset, value) {
            parent._var().child_unfocus({
                index: parent._var().currentslide,
                unfocusedindex: value,
                offset: offset,
                speed: 0,
                Zindex: parent._var().child_Zstack,
                Multiplier: parent._var().child_Mstack
            });
        });
        $.each(parent._var().child_stackpile_right, function(offset, value) {
            parent._var().child_unfocus({
                index: parent._var().currentslide,
                unfocusedindex: value,
                offset: offset,
                speed: 0,
                Zindex: parent._var().child_Zstack,
                Multiplier: parent._var().child_Mstack
            });
        });
        parent._var().childtrack(parent._var().currentslide);
    };
    _thizOBJ_.prototype._putZorder = function(index) {
        var parent = this;
        var tempZindex = parent._var().toplevelZ;
        var currentObject = $(parent._var().childObjects[index]);
        currentObject.css("position", "absolute").css("z-index", tempZindex);
        $.each(parent._var().child_stackpile_left, function(index, value) {
            var currentObject = $(parent._var().childObjects[value]);
            currentObject.css("position", "absolute").css("z-index", tempZindex - (index + 1));
        });
        $.each(parent._var().child_stackpile_right, function(index, value) {
            var currentObject = $(parent._var().childObjects[value]);
            currentObject.css("position", "absolute").css("z-index", tempZindex - (index + 1));
        });
    };
    _thizOBJ_.prototype._getZorder = function(index) {
        var parent = this;
        var tempZindex = parent._var().toplevelZ;
        parent._var().child_Zstack = [];
        parent._var().child_Mstack = [];
        parent._var().child_Zstack[index] = tempZindex;
        parent._var().child_Mstack[index] = 0;
        var currentObject = $(parent._var().childObjects[index]);
        $.each(parent._var().child_stackpile_left, function(index, value) {
            parent._var().child_Zstack[value] = tempZindex - (index + 1);
            parent._var().child_Mstack[value] = index + 1;
        });
        $.each(parent._var().child_stackpile_right, function(index, value) {
            parent._var().child_Zstack[value] = tempZindex - (index + 1);
            parent._var().child_Mstack[value] = index + 1;
        });
    };
    _thizOBJ_.prototype._setstacks = function(index) {
        var parent = this;
        var splitpoint = _.indexOf(parent._var().child_stack, index);
        var splitB = _.rest(parent._var().child_stack, [ splitpoint ]);
        var splitA = _.first(parent._var().child_stack, [ splitpoint ]);
        var offsetarray = splitB.concat(splitA);
        parent._var({
            child_offstack: offsetarray
        });
        var centerindex = Math.floor(parent._var().child_stack.length / 2);
        var splitB = _.rest(parent._var().child_offstack, [ centerindex + 1 ]);
        var splitA = _.first(parent._var().child_offstack, [ centerindex + 1 ]);
        var offsetarray = splitB.concat(splitA);
        parent._var({
            child_centerstack: offsetarray
        });
        var child_stackpile_left = _.first(parent._var().child_stack, [ splitpoint ]).reverse();
        var child_stackpile_right = _.rest(parent._var().child_stack, [ splitpoint + 1 ]);
        parent._var({
            child_stackpile_left: child_stackpile_left,
            child_stackpile_right: child_stackpile_right
        });
    };
    _thizOBJ_.prototype.pagejump = function(index) {
        var difference = Math.abs(index - this._var().currentslide);
        if (difference > 1) this._multijump(index); else this.jump(index);
    };
    _thizOBJ_.prototype.jump = function(index) {
        this._var().childtrack(index);
        this._var().childchange(index);
    };
    _thizOBJ_.prototype.quickjump = function(index) {
        this._var().childtrack(index);
        this._var().childchange(index, {
            speed: 0
        });
    };
    _thizOBJ_.prototype._multijump = function(index) {
        this._var().childtrack(index);
        this._var()._multijump(index);
    };
    _thizOBJ_.prototype.next = function() {
        var parent = this;
        var childObjects = parent._var().childObjects;
        var objectcount = childObjects.length;
        var indexCandidate = parent._var().currentslide;
        indexCandidate += 1;
        if (indexCandidate == objectcount) this._var().loopslides ? indexCandidate = 0 : indexCandidate = objectcount - 1;
        this._var().childtrack(indexCandidate);
        this._var().childchange(indexCandidate);
    };
    _thizOBJ_.prototype.prev = function() {
        var parent = this;
        var childObjects = parent._var().childObjects;
        var objectcount = childObjects.length;
        var indexCandidate = parent._var().currentslide;
        indexCandidate -= 1;
        if (indexCandidate < 0) this._var().loopslides ? indexCandidate = objectcount - 1 : indexCandidate = 0;
        this._var().childtrack(indexCandidate);
        this._var().childchange(indexCandidate);
    };
    return _thizOBJ_;
});

define("templates/flexibleregulatory.js", [ "jquery", "modules/definitions/standardmodule", "knockout", "modules/controllers/slideset", "tweenmax" ], function($, parentModel, ko, Slideset, TweenMax) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "flexibleregulatory",
            author: "Jonathan Robles",
            lasteditby: "",
            info_Slideset: undefined,
            slide_Slideset: undefined,
            viewmodel: undefined,
            app_target: $("#app-main"),
            infobox_target: $("#infobox"),
            slidebox_target: $("#slidebox"),
            menuanim: {
                anim_target: $("#app-main>.bot-right"),
                anim_time: .5
            },
            shareanim: {
                target: $("#app-main>.share"),
                anim_target: $("#app-main>.share>div"),
                anim_time: .5
            },
            callback: function() {
                this.app_target.css("opacity", 1);
            },
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var self = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onWindowWidth: function(o) {
                    if (self._var().viewmodel.menuopen()) self._var().viewmodel.menu_toggle();
                }
            };
        }());
    };
    _thizOBJ_.prototype.viewmodel = function(Slideset, Infoset, Menuanim, Shareanim) {
        var self = this;
        self.menuopen = ko.observable(false);
        self.shareopen = ko.observable(false);
        self.slide_currentIndex = ko.observable(Slideset._var().currentslide);
        self.slide_totalCount = ko.observable(Slideset._var().totalslides);
        self.info_currentIndex = ko.observable(Infoset._var().currentslide);
        self.info_totalCount = ko.observable(Infoset._var().totalslides);
        self.controlmenu = ko.computed(function() {
            if (self.menuopen()) {
                var destinationX = Menuanim.anim_target.innerWidth() - 70;
                TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                    x: destinationX
                });
            } else TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                x: 0
            });
        });
        self.controlshare = ko.computed(function() {
            if (self.shareopen()) {
                Shareanim.target.show();
                TweenMax.to(Shareanim.anim_target, .25, {
                    opacity: 1,
                    y: 0
                });
            } else {
                TweenMax.to(Shareanim.anim_target, 0, {
                    opacity: 0,
                    y: 25
                });
                Shareanim.target.hide();
            }
        });
        self.disable_next = ko.computed(function() {
            return self.slide_currentIndex() == self.slide_totalCount() - 1 ? true : false;
        });
        self.disable_prev = ko.computed(function() {
            return self.slide_currentIndex() == 0 ? true : false;
        });
        self.databindtoobject = function(getEvent) {
            try {
                var string = $(getEvent.target).attr("data-bind");
                var properties = string.split(",");
            } catch (err) {
                var string = $(getEvent.srcElement).attr("data-bind");
                var properties = string.split(",");
            }
            var obj = {};
            $.each(properties, function(i, v) {
                var tup = v.split(":");
                obj[tup[0]] = tup[1];
            });
            return obj;
        };
        self.info_button = function(data, event) {
            var switchindex = Number(self.databindtoobject(event).target);
            Infoset.jump(switchindex);
        };
        self.slide_button = function(data, event) {
            var switchindex = Number(self.databindtoobject(event).target);
            Slideset.pagejump(switchindex);
            if (self.menuopen()) self.menu_toggle();
        };
        self.slide_next = function() {
            Slideset.next();
        };
        self.slide_prev = function() {
            Slideset.prev();
        };
        self.menu_toggle = function() {
            self.menuopen(!self.menuopen());
        };
        self.share_toggle = function() {
            self.shareopen(!self.shareopen());
        };
        Slideset._var().childtrack = function(index) {
            self.slide_currentIndex(index);
        };
        Infoset._var().childtrack = function(index) {
            self.info_currentIndex(index);
        };
    };
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        var self = this;
        self.getdependancies();
        self.buildViewmodel();
        self._var().callback();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        var self = this;
        alert("this module has no refresh function");
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.getdependancies = function() {
        var self = this;
        self._var().info_Slideset = new Slideset({
            target: self._var().infobox_target,
            child_focus: function(o) {
                var argz = o;
                var tempObject = $(this.childObjects[argz.index]);
                tempObject.show();
            },
            child_unfocus: function(o) {
                var argz = o;
                var tempObject = $(this.childObjects[argz.unfocusedindex]);
                tempObject.hide();
            }
        });
        self._var().slide_Slideset = new Slideset({
            target: self._var().slidebox_target,
            autoZ: true,
            child_focus: function(o) {
                var argz = {
                    index: undefined,
                    speed: .5,
                    tweenvars: {
                        x: 0,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    }
                };
                argz = $.extend(argz, o);
                var tempObject = $(this.childObjects[argz.index]);
                if (Modernizr.csstransitions) TweenMax.to(tempObject, argz.speed, argz.tweenvars); else tempObject.show();
            },
            child_unfocus: function(o) {
                var argz = {
                    index: undefined,
                    unfocusedindex: undefined,
                    speed: 1,
                    offset: 0,
                    tweenvarsIN: {
                        x: -400,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    },
                    tweenvarsOUT: {
                        x: 400,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    }
                };
                argz = $.extend(argz, o);
                var tempObject = $(this.childObjects[argz.unfocusedindex]);
                if (Modernizr.csstransitions) if (argz.unfocusedindex < argz.index) TweenMax.to(tempObject, argz.speed, argz.tweenvarsIN); else TweenMax.to(tempObject, argz.speed, argz.tweenvarsOUT); else tempObject.hide();
            }
        });
        self._var().info_Slideset.init();
        self._var().slide_Slideset.init();
    };
    _thizOBJ_.prototype.buildViewmodel = function() {
        var self = this;
        self._var().viewmodel = new this.viewmodel(self._var().slide_Slideset, self._var().info_Slideset, self._var().menuanim, self._var().shareanim);
        ko.bindingHandlers.isolatedOptions = {
            init: function(element, valueAccessor) {
                var args = arguments;
                ko.computed({
                    read: function() {
                        ko.utils.unwrapObservable(valueAccessor());
                        ko.bindingHandlers.options.update.apply(this, args);
                    },
                    owner: this,
                    disposeWhenNodeIsRemoved: element
                });
            }
        };
        ko.bindingHandlers.CSSonMatch = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var objEkt = $(element);
                var databinds = allBindings();
                var comparevalue = ko.unwrap(valueAccessor());
                if (databinds.CSSclass != undefined) if (databinds.target != undefined) if (comparevalue == databinds.target) objEkt.addClass(databinds.CSSclass); else objEkt.removeClass(databinds.CSSclass); else alert("no target is defined with Jonathan's awesome CSSonMatch BINDING!"); else alert("no CSSclass is defined with Jonathan's awesome CSSonMatch BINDING!");
            }
        };
        ko.applyBindings(self._var().viewmodel);
    };
    return _thizOBJ_;
});

_notify = function() {
    var debug = function() {};
    var components = {};
    var broadcast = function(event, args, source) {
        if (!event) return;
        args = args || [];
        for (var c in components) if (typeof components[c]["on" + event] == "function") try {
            source = source || components[c];
            components[c]["on" + event].apply(source, args);
        } catch (err) {
            debug([ "Mediator error.", event, args, source, err ].join(" "));
        }
    };
    var addComponent = function(name, component, replaceDuplicate) {
        if (name in components) if (replaceDuplicate) removeComponent(name); else throw new Error("The object: " + name + " has already applied listeners");
        components[name] = component;
    };
    var removeComponent = function(name) {
        if (name in components) delete components[name];
    };
    var getComponent = function(name) {
        return components[name];
    };
    var contains = function(name) {
        return name in components;
    };
    return {
        name: "Mediator",
        broadcast: broadcast,
        add: addComponent,
        rem: removeComponent,
        get: getComponent,
        has: contains
    };
}();

_notify.add("global", function() {
    var tracecount = 0;
    var alertcount = 0;
    return {
        onTrace: function(o) {
            tracecount++;
            var datastring = o.data;
            if (typeof datastring == "object") datastring = JSON.stringify(datastring);
            var buildstring = "[" + String(tracecount) + "] (" + o.senderID + ":" + o.sendertype + ":" + o.notifyscope + ") > " + datastring;
            console.log(buildstring);
        },
        onAlert: function(o) {
            alertcount++;
            var datastring = o.data;
            if (typeof datastring == "object") datastring = JSON.stringify(datastring);
            var buildstring = "[" + String(tracecount) + "] (" + o.senderID + ":" + o.sendertype + ":" + o.notifyscope + ") > " + datastring;
            alert(buildstring);
        },
        onInitialize: function(o) {
            console.log("Created Instance #" + o.senderID + " type:" + o.sendertype + " by " + o.data.author + " [notifyscope:" + o.notifyscope + "]");
        },
        onOrientation: function(o) {
            window._global$.addorientationtohtml(o);
        }
    };
}());

if (typeof Object.create !== "function") Object.create = function(o) {
    function F() {}
    F.prototype = o;
    return new F();
};

(function() {
    var lastTime = 0;
    var vendors = [ "ms", "moz", "webkit", "o" ];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + "RequestAnimationFrame"];
        window.cancelAnimationFrame = window[vendors[x] + "CancelAnimationFrame"] || window[vendors[x] + "CancelRequestAnimationFrame"];
    }
    if (!window.requestAnimationFrame) window.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() {
            callback(currTime + timeToCall);
        }, timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };
    if (!window.cancelAnimationFrame) window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    };
})();

require([ "modernizr", "jquery", "underscore" ], function(Modernizr, $, _) {
    function CorePlayerBase() {
        if (arguments.callee._singletonInstance) return arguments.callee._singletonInstance;
        arguments.callee._singletonInstance = this;
        var parent = this;
        var debouncetimeout = 250;
        this.orientation = "notset";
        this.windowHeight = undefined;
        this.windowWidth = undefined;
        this.documentHeight = undefined;
        this.documentWidth = undefined;
        this.screenHeight = undefined;
        this.screenWidth = undefined;
        this.isMobile = Modernizr.mq("only screen and (min-device-width : 320px) and (max-device-width : 480px)");
        this.isTouch = Modernizr.touch;
        this.host = location.host;
        this.addorientationtohtml = function(o) {
            if (o.data == "portrait") {
                $("html").addClass("portrait");
                $("html").removeClass("landscape");
            } else {
                $("html").addClass("landscape");
                $("html").removeClass("portrait");
            }
        };
        this.setOrientation = function() {
            var candidate = "portrait";
            this.setWindowHeight();
            this.setWindowWidth();
            if (this.windowWidth > this.windowHeight) candidate = "landscape";
            this._isupdated("orientation", candidate, "Orientation");
        };
        this.setWindowHeight = function() {
            this._isupdated("windowHeight", $(window).height(), "WindowHeight");
        };
        this.setWindowWidth = function() {
            this._isupdated("windowWidth", $(window).width(), "WindowWidth");
        };
        this.setDocumentHeight = function() {
            this._isupdated("documentHeight", $(document).height(), "DocumentHeight");
        };
        this.setDocumentWidth = function() {
            this._isupdated("documentWidth", $(document).width(), "DocumentWidth");
        };
        this.setScreenHeight = function() {
            this._isupdated("screenHeight", screen.height, "ScreenHeight");
        };
        this.setScreenWidth = function() {
            this._isupdated("screenWidth", screen.width, "ScreenWidth");
        };
        this.jsonpReturn = function(id, type, notifyscope) {
            return function(data) {
                _notify.broadcast(type, [ {
                    senderID: id,
                    sendertype: type,
                    notifyscope: notifyscope,
                    data: data
                } ]);
            };
        };
        this._isupdated = function(value, currentVal, notify) {
            if (parent[value] != currentVal) {
                var Oldvalue = parent[value];
                parent[value] = currentVal;
                if (Oldvalue != undefined) this.notify(notify, parent[value]);
            }
        };
        this.notify = function(type, data) {
            _notify.broadcast(type, [ {
                senderID: "global",
                sendertype: "global",
                notifyscope: "global",
                data: data
            } ]);
        };
        this.getQuery = function(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    var returncandidate = pair[1];
                    if (returncandidate == "true" || returncandidate == "yes" || returncandidate == "y" || returncandidate == "t") returncandidate = true;
                    if (returncandidate == "false" || returncandidate == "no" || returncandidate == "n" || returncandidate == "f") returncandidate = false;
                    return returncandidate;
                }
            }
            return false;
        };
        $(window).on("resize", _.debounce(function(event) {
            parent.setOrientation();
        }, debouncetimeout));
    }
    window._global$ = new CorePlayerBase();
    window._global$.setOrientation();
    window._global$.setDocumentHeight();
    window._global$.setDocumentWidth();
    window._global$.setScreenHeight();
    window._global$.setScreenWidth();
});

define("requiredLib", function() {});