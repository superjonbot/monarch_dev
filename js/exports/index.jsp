<%@ include file="/fr_fr/all/common/declaration/global-top.jsp" %>
<title>DPC | Medscape France</title>
<meta name="description" content="" />
<link rel="canonical" href="http://www.medscape.fr/dpc" />

<%@ taglib uri="/WEB-INF/tld/prof-cobrand.tld" prefix="framework" %>
<%@ taglib uri="/WEB-INF/tld/prof-ads.tld" prefix="ads" %>
<%@ taglib uri="/WEB-INF/tld/prof-customtags.tld" prefix="medscape" %>
<%@ include file="/files/fr_fr/all/common/useragent/list.jsp" %>
<%@ include file="/files/fr_fr/all/common/version/css-js-version.jsp" %>
<% request.setAttribute("pclass", "dpc-index"); %>

<!-- HEADER -->
<framework:include fragment="header" styleheader="header-global" pagename="dpc-index" adsuppress="true" globalstylesuppress="true" noticesuppress="false" nocache="true" />
<!-- /HEADER -->

<!-- MAIN CONTENT -->
<div id="msfr" class="fullCol"> 

<!-- FULL -->    
<div id="full" class="boxed">

<!-- DPC index list -->
<div class="bucket" id="dpcIndex">
<div class="bucketHeader"><h2>DPC</h2></div>
<div class="bucketContent">
<ul id="dpclistitem">
</ul>
</div>
<div class="bucketFooter"></div>
</div>
<!-- /DPC index list -->

</div>
<!-- /FULL -->

</div>
<!-- /MAIN CONTENT --> 

<!-- FOOTER -->
<framework:include fragment="footer" adsuppress="true" />
<!--  /FOOTER  -->

</body>
</html>