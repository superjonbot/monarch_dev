yourFilePath="../../../SJB_Caster"
DATE=$(date +"%m-%d-%y")
TIME=$(date +"%r")
cachebuster=$(date +"%s")

#rm -rf ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/AE_Cast/js/receiver*
#rm -rf ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/AE_Cast/css/*
#rm -rf ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/monarch_dev/exports/chromecast_sender_JS_temp/*

#SENDER
#grunt --brand=mlt --environment=dev --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js
grunt --brand=ae --environment=dev --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js
#grunt --brand=his --environment=dev --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js
#grunt --brand=fyi --environment=dev --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js

#grunt --brand=mlt --environment=qa --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js
#grunt --brand=ae --environment=qa --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js
#grunt --brand=his --environment=qa --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js
#grunt --brand=fyi --environment=qa --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js

#grunt --brand=mlt --environment=prod --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js
#grunt --brand=ae --environment=prod --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js
#grunt --brand=his --environment=prod --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js
#grunt --brand=fyi --environment=prod --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_sender_JS.js

#RECEIVER
#grunt --brand=mlt --environment=dev --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js  #LIFETIME DEV
grunt --brand=ae --environment=dev --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js   #AE DEV
#grunt --brand=his --environment=dev --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js  #HISTORY DEV
#grunt --brand=fyi --environment=dev --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js  #FYI DEV

#grunt --brand=mlt --environment=qa --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js  #LIFETIME QA
#grunt --brand=ae --environment=qa --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js   #AE QA
#grunt --brand=his --environment=qa --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js  #HISTORY QA
#grunt --brand=fyi --environment=qa --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js  #FYI QA

#grunt --brand=mlt --environment=prod --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js  #LIFETIME PROD
#grunt --brand=ae --environment=prod --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js   #AE PROD
#grunt --brand=his --environment=prod --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js  #HISTORY PROD
#grunt --brand=fyi --environment=prod --cachebuster=$cachebuster --gruntfile Gruntfile_chromecast_receiver_JS.js  #FYI PROD

####rm -rf ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/export/public
####cp -rf ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/public ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/export/public
####cp -f ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/proxy.php ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/export/
####cp -f ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/htaccess.txt ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/export/
####>>> cp -rf ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/monarch_dev/exports/chromecast_sender_JS_temp/sender.js ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/AE_Cast/testSender/js/
####>>> cp -rf ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/monarch_dev/exports/chromecast_receiver_JS_temp/receiver.js ~/Sites/myDEVBOX/localhost/ChromeCast/Chrome_Samples/SJB_Caster/AE_Cast/js/


#move to AE git repo
#erase everything
rm -rf $yourFilePath/export_ae/public/*
rm -rf $yourFilePath/export_ae/source/*
mkdir $yourFilePath/export_ae/source/instances
mkdir $yourFilePath/export_ae/source/modules
mkdir $yourFilePath/export_ae/source/modules/controllers
mkdir $yourFilePath/export_ae/source/modules/definitions

#copy AE_Cast
cp -rf $yourFilePath/AE_Cast/* $yourFilePath/export_ae/public

#SOURCE FILES
cp -rf $yourFilePath/monarch_dev/js/Gruntfile_chromecast* $yourFilePath/export_ae/source
cp -rf $yourFilePath/monarch_dev/js/instances/chromecast* $yourFilePath/export_ae/source/instances
cp -rf $yourFilePath/monarch_dev/js/libraries $yourFilePath/export_ae/source
cp -rf $yourFilePath/monarch_dev/js/modules/controllers/chrome* $yourFilePath/export_ae/source/modules/controllers
cp -rf $yourFilePath/monarch_dev/js/modules/definitions/*module* $yourFilePath/export_ae/source/modules/definitions
cp -rf $yourFilePath/monarch_dev/exports/chrome* $yourFilePath/export_ae/builds

#source cleanup
rm -rf $yourFilePath/export_ae/source/libraries/angular
rm -rf $yourFilePath/export_ae/source/libraries/dust
rm -rf $yourFilePath/export_ae/source/libraries/green*
rm -rf $yourFilePath/export_ae/source/libraries/hammer
rm -rf $yourFilePath/export_ae/source/libraries/jquery*
rm -rf $yourFilePath/export_ae/source/libraries/knockout
rm -rf $yourFilePath/export_ae/source/libraries/mediator
rm -rf $yourFilePath/export_ae/source/libraries/pars
rm -rf $yourFilePath/export_ae/source/libraries/etc/env/config_standin.js
rm -rf $yourFilePath/export_ae/source/libraries/etc/env/config_wp.js
rm -rf $yourFilePath/export_ae/source/libraries/etc/env/config.js
rm -rf $yourFilePath/export_ae/source/libraries/etc/EXPORT*
rm -rf $yourFilePath/export_ae/source/libraries/etc/SRC

# export_ae/js clean up
rm -rf $yourFilePath/export_ae/public/js/*_f_*
rm -rf $yourFilePath/export_ae/public/js/*_h_*
rm -rf $yourFilePath/export_ae/public/testSender/js/*_f_*
rm -rf $yourFilePath/export_ae/public/testSender/js/*_h_*

# css clean up
mv $yourFilePath/export_ae/public/css_src $yourFilePath/export_ae/source/

echo " "

cd $yourFilePath/export_ae && git add . && git commit -m "auto commit date:$DATE time:$TIME version:$cachebuster" && git push origin develop && cd ../../SJB_Caster/monarch_dev/js

#read -p "Generic Commit? " -n 1 -r
#echo    # (optional) move to a new line
#if [[ $REPLY =~ ^[Yy]$ ]]
#then
#    cd $yourFilePath/export_ae && git add . && git commit -m "auto commit date:$DATE time:$TIME version:$cachebuster" && git push origin develop && cd ../../SJB_Caster/monarch_dev/js
#else
#    echo "cd $yourFilePath/export_ae && git add . && git commit -m \"auto commit date:$DATE time:$TIME version:$cachebuster\" && git push origin develop && cd ../../SJB_Caster/monarch_dev/js"
#fi


####git status

echo "created $cachebuster"