
module.exports = function (grunt) {
    var versionnumber = '<%= pkg.version %>.' + (Math.floor(new Date().getTime() / 100000000000) / 10);

    // USER DEFINED *start*
    var projectName = 'chromecast_receiver_JS';
    var thisFile = 'Gruntfile_chromecast_receiver_JS.js';
    var projectFolder = '../../public/js/';  //DESTINATION FOR FILES
    var masterFiles = ['libraries/etc/required_chromecast.js', 'instances/chromecast_receiver_JS.js'];
    var finalNames = ['receiver_h.js','receiver_f.js','receiver.js'];   //application.JS is the header and footer combined
    var thirdpartyInc = '/* almond, modernizr, underscore, oboe */';
    var requirePATHS = {
                        almondLib: 'libraries/almond/almond',
                        requireLib: 'libraries/require/require',
                        requirejsConfig: 'libraries/etc/env/config_grunt',
                        underscore: 'libraries/underscore/underscore'
                    };
    // USER DEFINED *end*

    var myTaskList = ['jshint','shell', 'requirejs','concat',  'uglify', 'jsbeautifier', 'usebanner','copy', 'notify:complete'];
    var destinationFolder = '../exports/'+projectName+'_v' + versionnumber + '/';


    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('../package.json'),

       returnBanner:function(src, filepath) {
            var complete_src = grunt.file.expand(src); 
            var final_name = '';
            // some string manipulations to get your the format you want 
        for (var i = 0; i < complete_src.length; i++) {
            complete_src[i] = complete_src[i].substring(complete_src[i].lastIndexOf('/') + 1, complete_src[i].length );
        }
        final_name = complete_src.join('-');

        var final_banner='';
        final_banner+='/*! '+projectName+' : '+final_name+' */';
        return final_banner;
              },    //This function places the filename at the top of the file

        banner: '/*! codebase: <%= pkg.name %> v' + versionnumber + ' by Jonathan Robles */\n' +
                '/*! built:<%= grunt.template.today("mm-dd-yyyy [h:MM:ssTT]") %> */\n'+
                '/*! -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */\n\n'+
                '/*! Prerequisites: //www.gstatic.com/cast/sdk/libs/receiver/2.0.0/cast_receiver.js */\n\n'+
                '/*! Third Party Includes [start] */\n' + thirdpartyInc +
              '\n/*! Third Party Includes [end] */\n',
        jshint: {
            // define the files to lint
            files: [thisFile],
            // configure JSHint (documented at http://www.jshint.com/docs/)
            options: {
                // more options here if you want to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true
                }
            }
        },

shell: {
  erase_directories: {
    command: 'rm -rf '+projectFolder+'receiver*'
  },
erase_temp_directory: {
    command: 'rm -rf ../exports/'+projectName+'_temp/*.js'
  },
erase_proj_directory: {
    command: 'rm -rf ../exports/'+projectName+'_v' + versionnumber + '/*'
  }


    

},


        requirejs: {
            required_file: {
                options: {
                    baseUrl: ".",
                    mainConfigFile: 'libraries/etc/env/config_grunt.js',
                    optimize: 'none',
                    preserveLicenseComments: false,
                    name: masterFiles[0],
                    out: "../exports/"+projectName+"_temp/"+finalNames[0],
                    paths: requirePATHS,
                    include: ['almondLib','requirejsConfig']
                }
            },
            application_file: {
                options: {
                    baseUrl: ".",
                    mainConfigFile: 'libraries/etc/env/config_grunt.js',
                    optimize: 'none',
                    preserveLicenseComments: false,
                    name: masterFiles[1],
                    out: "../exports/"+projectName+"_temp/"+finalNames[1],
                    paths: requirePATHS
                }
            }
        },



        concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: ["../exports/"+projectName+"_temp/"+finalNames[0], "../exports/"+projectName+"_temp/"+finalNames[1]],
                dest: "../exports/"+projectName+"_temp/"+finalNames[2]
            },
        },



        //Minify!!!
        uglify: {
            beautify: {
                options: {
                    banner: '<%= banner %>',
                    beautify: true,
                    mangle: false,
                    compress: false
                },

                src: '../exports/'+projectName+'_temp/*.js',
                dest: destinationFolder,
                expand: true, // allow dynamic building
                flatten: true // remove all unnecessary nesting
            },
            minifier: {
                options: {
                    banner: '<%= banner %>',
                    beautify: false,
                    mangle: true,
                    compress: true
                },

                src: '../exports/'+projectName+'_temp/*.js',
                dest: destinationFolder,
                expand: true, // allow dynamic building
                flatten: true, // remove all unnecessary nesting
                ext: '_min.js' // replace .js to .min.js
            }
        },


        jsbeautifier: {
            files: [(destinationFolder+'*.js'), ('!'+destinationFolder+'*_min.js')]
        },



usebanner: {
    taskName: {
      options: {
        position: 'top',
        linebreak: true ,
        process: '<%= returnBanner %>'


      },
      files: {
        src: [ (destinationFolder+'*') ]
      }
    }
  },


        copy: {
            main: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        flatten: true,
                        src: ['../exports/'+projectName+'_v' + versionnumber + '/*'],
                        dest: projectFolder,
                        filter: 'isFile'
                    }
                ],
            },
        },



        watch: {
            scripts: {
                files: masterFiles,  //watch these files and GRUNT IT!
                tasks: myTaskList,
                options: {
                    spawn: true,
                    livereload: true
                }
            }


        },
        notify_hooks: {
           options: {
              enabled: true,
              max_jshint_notifications: 5, // maximum number of notifications from jshint output
              title: projectName, // defaults to the name in package.json, or will use project directory's name
              success: false, // whether successful grunt executions should be notified automatically
              duration: 3 // the duration of notification in seconds, for `notify-send only
           }
        },
        notify: {
            complete: {
                options: {
                    title: '<%= notify_hooks.options.title %>',  // optional
                    message: 'DONE! [<%= notify_hooks.options.title %>]'
                }
            }
        }

    });

    // PLUGINS
    grunt.loadNpmTasks('grunt-banner'); //https://github.com/gruntjs/grunt-contrib-concat
    grunt.loadNpmTasks('grunt-contrib-concat'); //https://github.com/gruntjs/grunt-contrib-concat
    grunt.loadNpmTasks('grunt-contrib-copy'); //https://github.com/gruntjs/grunt-contrib-copy
    //grunt.loadNpmTasks('grunt-contrib-cssmin'); //https://github.com/gruntjs/grunt-contrib-cssmin
    //grunt.loadNpmTasks('grunt-contrib-imagemin'); //https://github.com/gruntjs/grunt-contrib-imagemin
    //grunt.loadNpmTasks('grunt-contrib-jasmine');   //https://github.com/gruntjs/grunt-contrib-jasmine
    grunt.loadNpmTasks('grunt-contrib-jshint'); //https://github.com/gruntjs/grunt-contrib-jshint
    //grunt.loadNpmTasks('grunt-contrib-qunit'); //https://github.com/gruntjs/grunt-contrib-qunit
    grunt.loadNpmTasks('grunt-contrib-requirejs'); //https://github.com/gruntjs/grunt-contrib-requirejs
    grunt.loadNpmTasks('grunt-contrib-uglify');    //https://github.com/gruntjs/grunt-contrib-uglify
    grunt.loadNpmTasks('grunt-contrib-watch');    //https://github.com/gruntjs/grunt-contrib-watch
    grunt.loadNpmTasks('grunt-jsbeautifier');  //https://github.com/vkadam/grunt-jsbeautifier
    //grunt.loadNpmTasks('grunt-mocha');    //https://github.com/kmiyashiro/grunt-mocha
    //grunt.loadNpmTasks('grunt-mocha-phantomjs');  //https://github.com/jdcataldo/grunt-mocha-phantomjs
    grunt.loadNpmTasks('grunt-notify');   //https://github.com/dylang/grunt-notify
    grunt.loadNpmTasks('grunt-shell');    //https://github.com/sindresorhus/grunt-shell
    //grunt.loadNpmTasks('grunt-ssh');    //https://github.com/chuckmo/grunt-ssh

    // run right away
    grunt.task.run('notify_hooks');  
    // Default task(s).
    grunt.registerTask('default', myTaskList);

};