/**
 * ChromeCast Receiver Instance
 * by Jonathan Robles
 *
 * Date: 12/16/15
 *
 */

// Start Receiver instance

require(['modules/controllers/chromecast_receiver'],
    function (ccReceiver) {

        window.myReceiver = new ccReceiver({
            //variable:value
        });

        window.myReceiver.init();


    });



