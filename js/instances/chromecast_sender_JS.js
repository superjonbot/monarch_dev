/**
 * ChromeCast Sender Instance
 * by Jonathan Robles
 *
 * Date: 12/16/15
 *
 */

// Start Receiver instance

require(['modules/controllers/chromecast_sender'],
    function (ccSender) {

        var mySender = new ccSender({
            //variable:value
        });

        //alert('mySender Init!');
        mySender.init();

    });



