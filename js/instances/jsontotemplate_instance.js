// jsontotemplate demo
// Start the main app logic.
require(['jquery', 'modules/data/jsontotemplate'],
    function ($, J2T) {

        //define object, override default dataIN and htmlIN values below
        var mytemplatetest = new J2T({

            dataIN: {
                'window': {
                    'title': 'Sample Widget',
                    'name': 'main_window',
                    'width': 500,
                    'height': 500
                },
                'image': {
                    'src': 'Images/Sun.png',
                    'name': 'sun1',
                    'width': 250,
                    'height': 250,
                    'alignment': 'center'
                }
            },
            htmlIN: '<div>{{locale.close}}</div><div id="{{window.name}}"><p>{{window.title}}</p><img src="{{image.src}}" alt="{{image.name}}" height="{{image.height}}" width="{{image.width}}"></div>'

        });

        //Initializes object, also does a refresh, default callback is an alert so data is alerted
        mytemplatetest.init();

        //use the _var() command to change initializing variables in the object, here only the json data is changed.
        mytemplatetest._var({
            dataIN: {
                'window': {
                    'title': 'Another Widget',
                    'name': 'main_window',
                    'width': 500,
                    'height': 500
                },
                'image': {
                    'src': 'Images/Moon.png',
                    'name': 'sun1',
                    'width': 250,
                    'height': 250,
                    'alignment': 'center'
                }
            }
        });

        //use the refresh command to refresh the object and execute the callback with new data
        mytemplatetest.refresh();

    });

// End main app logic.