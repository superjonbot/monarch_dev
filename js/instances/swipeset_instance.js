/**
 * swipeset Instance
 * sample by Jonathan Robles
 *
 * Date: 12/21/13 : 1:21 PM
 *
 */

// Start main swipeset instance

require(['jquery', 'modules/controllers/swipeset'],
    function ($, Swipeset) {

        var scrollXtest = new Swipeset({
            target:$('#yourdiv_X'),
            yscroll:false
        });

        scrollXtest.init();

        var scrollYtest = new Swipeset({
            target:$('#yourdiv_Y'),
            yscroll:true
        });

        scrollYtest.init();


        var scrollZtest = new Swipeset({
            target:$('#yourdiv_YB'),
            yscroll:true,
            virtualchildren:true,
            snaponrelease:false
        });

        scrollZtest.init();




       /* $('#xprev').on('click',function(){
            scrollXtest.prev();
        })
        $('#xnext').on('click',function(){
            scrollXtest.next();
        })
        $('#yprev').on('click',function(){
            scrollYtest.prev();
        })
        $('#ynext').on('click',function(){
            scrollYtest.next();
        })*/


        var navitems=$('#navigationA').children();

        $(navitems[0]).bind('click',function(event){
            scrollXtest.pagejump(0)
        })
        $(navitems[1]).bind('click',function(event){
            scrollXtest.prev();
        })

        $(navitems[2]).bind('click',function(event){
            scrollXtest.next();
        })

        $(navitems[3]).bind('click',function(event){
            scrollXtest.pagejump(scrollXtest._var().totalslides-1)
        })

        $(navitems[4]).bind('click',function(event){
            scrollXtest.pagejump(3)
        })

        $(navitems[5]).bind('click',function(event){
            scrollXtest.jump(3)
        })

        $(navitems[6]).bind('click',function(event){
            scrollXtest.quickjump(3)
        })

        $(navitems[7]).bind('click',function(event){
            scrollXtest._var({enable_touchbinds:!scrollXtest._var().enable_touchbinds})
        })

        $(navitems[8]).bind('click',function(event){
            scrollXtest._gotopercent(50)
        })

        $(navitems[9]).bind('click',function(event){
            scrollXtest.refresh()
        })

        var navitems=$('#navigationB').children();

        $(navitems[0]).bind('click',function(event){
            scrollYtest.pagejump(0)
        })
        $(navitems[1]).bind('click',function(event){
            scrollYtest.prev();
        })

        $(navitems[2]).bind('click',function(event){
            scrollYtest.next();
        })

        $(navitems[3]).bind('click',function(event){
            scrollYtest.pagejump(scrollYtest._var().totalslides-1)
        })

        $(navitems[4]).bind('click',function(event){
            scrollYtest.pagejump(3)
        })

        $(navitems[5]).bind('click',function(event){
            scrollYtest.jump(3)
        })

        $(navitems[6]).bind('click',function(event){
            scrollYtest.quickjump(3)
        })

        $(navitems[7]).bind('click',function(event){
            scrollYtest.refresh()
        })


    });


// End main swipeset instance




