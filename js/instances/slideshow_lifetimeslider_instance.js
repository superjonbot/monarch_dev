/**
 * myLifetime Slider Instance
 * sample by Jonathan Robles
 *
 * Date: 10/14
 *
 */

 require(['jquery', 'modules/controllers/carouselset','modules/parsers/dataparser','modules/data/jsontotemplate','tweenlite','scrollto'],
    function ($, Carousel, jsonParser,jsontoTemplate,TweenMax,Scrollto) {
        var mainOBJ=this;
        var self=this;
        var fullShows_list=[{"title":"Abby\u0027s Ultimate Dance Competition",
            "url":"shows\/abbys-ultimate-dance-competition",
            "nid":"22790","total_videos":"11"},
            {"title":"Dance Moms: Abby\u0027s Studio Rescue",
                "url":"shows\/abbys-studio-rescue",
                "nid":"22790","total_videos":"4"},
            {"title":"Raising Asia","url":"shows\/raising-asia",
                "nid":"22790","total_videos":"13"},
            {"title":"Bristol Palin: Life\u0027s a Tripp",
                "url":"shows\/bristol-palin-lifes-a-tripp",
                "nid":"22790","total_videos":"1"}];

        //This modifies the JSON data before it gets to the template
        var modifyData = function (getData){


            if(getData.duration!=undefined){


                var tempnum=Math.round(Number(getData.duration));
                var tempremainder=(tempnum%60);
                if (tempremainder<=9){tempremainder='0'+tempremainder}
                getData.duration=Math.floor(tempnum/60)+':'+tempremainder;



            }

            if(getData.season==''||getData.season==undefined){getData.season='0X'};
            if(getData.episode==''||getData.episode==undefined){getData.episode='0X'};
            if(getData.duration==''||getData.duration==undefined){getData.duration='X0X'};

            var mpxdataattrib='';
            if(getData.behind_wall=='1'){mpxdataattrib='data-auth-required="true" data-mpx-id="'+getData.mpxid+'" '}
            getData.mpxdataattrib=mpxdataattrib;

            return getData;
        }

        //This modifies the output before it gets displayed
        var modifyHTML = function (getHTML) {


            var getHTML = getHTML.replace("S0X | E0X |", "");
            var getHTML = getHTML.replace("S0X | E0X", "");
            var getHTML = getHTML.replace("E0X | X0X", "");
            var getHTML = getHTML.replace("S0X |", "");
            var getHTML = getHTML.replace("| X0X", "");
            var getHTML = getHTML.replace("E0X", "");
            var getHTML = getHTML.replace(/X0X/gi, "");

            var getHTML = getHTML.replace("Season 0X | Episode 0X |", "");
            var getHTML = getHTML.replace("Season 0X | Episode 0X", "");
            var getHTML = getHTML.replace("Episode 0X | X0X", "");
            var getHTML = getHTML.replace("Season 0X |", "");
            var getHTML = getHTML.replace("Episode 0X", "");

            return getHTML;
        }

        var initializeScroller = function(scrollerOBJ,getDATA){
            //initialize the scroller!

            if(getDATA!=undefined){
                scrollerOBJ.instance._var({
                    custom_contentData:getDATA.items
                })
            }; //plug in partial data to objects itemlist


            //initialize stuff
            scrollerOBJ.instance.init();
            scrollerOBJ.instance._var().custom_jsontotemplate._startlisteners();
            scrollerOBJ.instance._var().custom_Dataparser._startlisteners();




            //make navigation
            $(scrollerOBJ.obj).find(scrollerOBJ.data.next).on((_global$.isTouch)?'touchstart':'click',function(){
                scrollerOBJ.instance.next();
            });
            $(scrollerOBJ.obj).find(scrollerOBJ.data.previous).on((_global$.isTouch)?'touchstart':'click',function(){
                scrollerOBJ.instance.prev();
            });


        }


        var makeAd =function() {


            var adhtml='<div class="row-fluid standalone_ad_container container_outer">'
            adhtml+='<div class="container_inner">'
            adhtml+='<div class="standalone_ad_container_ad_area">'
            adhtml+='</div>'
            adhtml+='</div>'
            adhtml+='</div>'

            $('.fullepisodes_container').find('.container_inner').find('div.episode_set:eq(2)').after(adhtml);



            //start onWidth Listener




        };

        var placeAd =function() {
            var placeItnow= function(){var jumptoY=$('.standalone_ad_container_ad_area').offset().top;
                var diffY=$('.dart-name-sidebar_300x250').offset().top;
                var addYtarget=$('.dart-name-sidebar_300x250').position().top+(jumptoY-diffY);
                $('.dart-name-sidebar_300x250').css('top',(addYtarget+'px'));};

            placeItnow();
            if(waitforAnimation!=undefined){clearInterval(waitforAnimation)};
            var waitforAnimation=setTimeout(
                function(){
                    placeItnow();
                    $('.dart-name-sidebar_300x250').addClass('visible');
                }


                ,1000); //compensate for any height animations
        }


        var applyCarousels = function(){
            //build Carousels list
            var pageCarousels=new Array();
            $.each($('[data-uiType="carousel"]'), function (index, value) {


               $(value).attr('data-uiType','carousel_rendered') ;


            var uiData = $(value).attr('data-uiData')
            uiData = eval("(" + uiData + ")");//get JSON

            var targetListContainer = $(value).find(uiData.container)
            pageCarousels.push({
                obj: value,
                instance: new Carousel({
                    custom_updateClassesAndImagePreload: function (o) {
                        //alert('debounce callback!');
                        var visibleslides = this.visibleslides;
                        var visibleslides_with_bleed = this.visibleslides_with_bleed;
                        var childObjects = this.childObjects;

                        //console.log(visibleslides + ' : ' + visibleslides_with_bleed + ' : ' + childObjects)

                        $.each(childObjects, function (index, value) {
                            var isthere = _.indexOf(visibleslides, index);
                            if (isthere != -1) {
                                $(value).addClass('inview');
                                $(value).addClass('viewed');
                            } else {
                                $(value).removeClass('inview');
                            }
                        })

                        $.each(childObjects, function (index, value) {
                            var isthere = _.indexOf(visibleslides_with_bleed, index);
                            if (isthere != -1) {
                                $(value).addClass('inbleed');
                            } else {
                                $(value).removeClass('inbleed');
                            }
                        })


                        $.each(visibleslides_with_bleed, function (index, value) {

                            $(childObjects[value]).find('[data-imageUrl]').each(function () {
                                var tempimageCAN = $(this).attr('data-imageUrl');
                                var tempimageSRC = $(this).attr('src');
                                //console.log(tempimageCAN+' & '+tempimageSRC);
                                if (tempimageCAN != tempimageSRC) {
                                    $(this).attr('src', tempimageCAN)
                                }

                            });

                        })
                    },
                    custom_MissingbleedIndex:function(){
                        var self=this;
                        var visibleslides_with_bleed = this.visibleslides_with_bleed.reverse();
                        var returnCandidate=undefined; //true
                        $.each(visibleslides_with_bleed, function (index, value) {

                            var isMissing=(self.custom_contentData[value]==undefined);

                           // _global$.notify('Trace','xox checking item:'+value+' missing?'+isMissing);


                            if (isMissing){
                                returnCandidate=value;


                            }
                        });
                        //RETURN THE INDEX OF MISSING STUFF
                        return returnCandidate;
                    },
                    custom_jsontotemplate:new jsontoTemplate({
                        dataIN:undefined,
                        htmlIN:undefined,
                        callback:function(data){
                            alert(data);
                        }
                    }),
                    custom_Dataparser: new jsonParser({
                                usenocache:(_global$.getQuery('debug')),
                                file: undefined,
                                format: 'json',
                                callback: function (data) {
                                    alert(JSON.stringify(data.items));
                                }
                            }),
                    custom_uiData:uiData,
                    custom_contentData:[],   //Array that WILL HOLD ITEM DATA
                    custom_datatoIndex:function(getData){
                        var self=this;
                        _global$.notify('Trace','custom_datatoIndex('+getData.index+') >>> '+getData.data);


                        var getHTML=$(self.childObjects[getData.index]).html()


                        self.custom_jsontotemplate._var({
                            dataIN:modifyData(getData.data),
                            htmlIN:getHTML,
                            callback:function(data){

                                $(self.childObjects[getData.index]).empty().append(modifyHTML(data));
                               // $(self.childObjects[getData.index]).removeClass('loading');
                            }
                        })
                        self.custom_jsontotemplate.refresh();
                        self.custom_updateClassesAndImagePreload();



                    },


                    target: targetListContainer,
                    attachdisableclassonid:$(value),
                    attachhidearrowclassonid:$(value),

                    usetouchbinds:_global$.isTouch, //adds touchbinds to the target

                    callback: function (o) {

                        this.custom_updateClassesAndImagePreload(o);
                    },  //runs when swipepanel is made





                    onscroll_pause: function (o) {
                        //console.log('onscroll_pause');
                        var self=this;

                        if(this.custom_uiData.dataservice!=undefined){

                        var missingIndexData=this.custom_MissingbleedIndex();


                        if(missingIndexData!=undefined){


                        var numberofItemstoget=Number(this.custom_uiData.datacount);
                        var myDataURL= this.custom_uiData.dataservice;

                        var convertIndextoPage = function (getIndex) {
                            var pagecandidate = Math.floor(getIndex/numberofItemstoget);
                            return pagecandidate;
                        };
                        var getPage=convertIndextoPage(missingIndexData);

                        var convertIndextoHardIndex =function (getIndex) {
                            var pagecandidate = convertIndextoPage(getIndex)*numberofItemstoget
                            return pagecandidate;
                        };



                            _global$.notify('Trace',' received index:'+missingIndexData+' calculate page:'+convertIndextoPage(missingIndexData)+' start replacement index:'+convertIndextoHardIndex(missingIndexData));

                            // $(self.childObjects[getData.index]).removeClass('loading');

                        var myDataURLwithCount= myDataURL + '/'+getPage+((numberofItemstoget!=undefined)? ('/'+numberofItemstoget):'');
                        if (!_global$.islocalhost){myDataURL=myDataURLwithCount}; //patch for local development
                        _global$.notify('Trace','attempting to retrieve:'+ myDataURLwithCount+'  to index:'+missingIndexData+'  page:'+convertIndextoPage(missingIndexData));

                            //SHOWLOADER!
                            for(tempvar=0;tempvar<numberofItemstoget;tempvar++){

                                $(self.childObjects[convertIndextoHardIndex(missingIndexData)+tempvar]).addClass('loading');
                                $(self.childObjects[convertIndextoHardIndex(missingIndexData)+tempvar]).removeClass('notloaded');
                            }


                            //LOADEM!
                            self.custom_Dataparser._var({
                                file: myDataURL,
                                callback: function (data) {
                                    var listData=data.items;

                                $.each(listData,function(index,value){





                                    var useIndex = (          (convertIndextoPage(missingIndexData)*numberofItemstoget)          +index);





                                    var useData =     value;

                                    self.custom_contentData[useIndex]=useData;  //put data in Array
                                    self.custom_datatoIndex({index:useIndex,data:useData});  //put data in Object
                                    $(self.childObjects[useIndex]).removeClass('loading');
                                })
                            }
                            });
                            self.custom_Dataparser.refresh();


                        }




                        }else{

                            this.custom_updateClassesAndImagePreload(o);
                        }


                        //console.log('moving :'+ o.currentposition+' [ '+ o.currentpercent+'% ]')

                    }, //on scrolling callback

                    onscroll_debounce_callback: function (o) {
                        //alert(this.custom_contentData)
                        //console.log('onscroll_debounce_callback');
                        this.custom_updateClassesAndImagePreload(o);

                    }
                }),
                data: uiData
            });

        })
            //start Carousels in list
            $.each(pageCarousels,function(index,value){


            var _InitialBuild= function(getDATA){
//                alert('Successfully read data!: ' + JSON.stringify(getDATA) + ' !' +JSON.stringify(value.data))

                var listContainer=$(value.obj).find(value.data.container);
                var htmltemplateHolder=$(value.obj).find(value.data.template);
                var htmlTemplate=htmltemplateHolder.html();
                var htmlTemplateStrip = htmlTemplate.replace(/\s/g, '');
                var itemCount = Number(getDATA.total)

                if(htmltemplateHolder.length!=0&&htmlTemplateStrip.length!=0){

                    //clear the listcontainer
                    listContainer.empty();


for(temp_count=0;temp_count<itemCount;temp_count++){


    var injectData=getDATA.items[temp_count];


    //initially build scroller with as much data as possible
    if(injectData!=undefined){
        var pluggedInHTML = new jsontoTemplate({
            dataIN:modifyData(injectData),
            htmlIN:htmlTemplate,
            callback:function(data){
                //alert(data);
                listContainer.append(modifyHTML(data))

            }


        }).init();


    } else {

        listContainer.append(htmlTemplate);
        listContainer.children().last().addClass('notloaded')
    }

    //$(listContainer.children()[listContainer.children().length]).addClass('notloaded');


}

                    initializeScroller(value,getDATA); //BUILD THE SCROLLER!!!

//                    alert(
//
//                        itemCount+' <<< '+
//
//                        htmltemplateHolder.length+' '+
//                            htmlTemplateStrip
//
//                    );



                }else{



                    _global$.notify('Trace',('There is no template called "'+value.data.template+'" or the content is blank!') );
                }



            };

            //if there is json data lets do some special stuff
           if(value.data.dataservice!=undefined){

               var myDataURL = value.data.dataservice;
               var myDataURLwithCount= myDataURL + '/0'+((value.data.datacount!=undefined)? ('/'+value.data.datacount):'');

               if (!_global$.islocalhost){myDataURL=myDataURLwithCount};

               _global$.notify('Trace','attempting to retrieve:'+ myDataURLwithCount);

               var myjsontest = new jsonParser({
                   usenocache:(_global$.getQuery('debug')),
                   file: myDataURL,
                   format: 'json',
                   callback: function (data) {
                       _InitialBuild(data);
                   }
               });
               myjsontest.init();



           }  else {
               initializeScroller(value); //BUILD THE SCROLLER!!!
           }


        });

           // alert('xoxi')
if(_global$.path.indexOf('watch-full-episodes-online')!=-1){
    makeAd();
    showhideOnAdobePass();
    placeAd();

$('body').prepend('<div id="back-top" class="hidethis"><img src="profiles/mylifetime_com/themes/custom/ltv/images/show-landing-button-up.png"></div>');


            _notify.add('slider_instance', function() {
                return {
                    onWindowWidth:function(o){
                        placeAd();
                    },
                    onWindowScrollStart:function(o){
                        //console.log('scrollstart');
                    },
                    onWindowScrollEnd:function(o){
                        //console.log('scrollend');
                        if (_global$.lastscrollpositionY>_global$.windowHeight ){
                            $('#back-top').removeClass('hidethis');
                        } else {
                            $('#back-top').addClass('hidethis');

                        };
                        placeAd();
                    }
                }
            }());



} else {
    showhideOnAdobePass();

    $('body').prepend('<div id="back-top" class="hidethis"><img src="profiles/mylifetime_com/themes/custom/ltv/images/show-landing-button-up.png"></div>');

    _notify.add('slider_instance', function() {
        return {
            onWindowScrollEnd:function(o){


                if (_global$.lastscrollpositionY>_global$.windowHeight ){
                    $('#back-top').removeClass('hidethis');
                } else {
                    $('#back-top').addClass('hidethis');

                };

            }
        }
    }());



}



        };
        var makeButtons = function() {
               $(document).on('click','[data-uiType^="button_jumpto"]',function(value){
                    window.location.href = $(this).attr('data-uiType').substring(14,999);
                });

                $(document).on('click','[data-uiType="adobePassLogin"]',function(value){



                    try {
                        aetn.video.adobePass.login()
                    }
                    catch (e) {
                        console.log('ALERT!: aetn.video.adobePass.login() failed, prerequisite js loaded? '+e)
                    }


                });

            $(document).on((_global$.isTouch)?'touchstart':'click','#back-top',function(value){
               TweenMax.to(window, 2, {scrollTo:{y:0}, ease:Power2.easeOut});
            });
            //Second POPULATE fullepisodes template
            applyFEtemplate();
        };





        var applyFEtemplate = function(){




            if($('[data-uiType="full_episode_template"]').length==0){applyCarousels();}  else {



                var templateHolder = new Array(); //build Array to hold html templates

                var FEjsonplug = new jsontoTemplate({
                    dataIN:undefined,
                    htmlIN:undefined,
                    regEX:/{{{(.*?)}}}/g

                });
                var FEjsonloader = new jsonParser({
                    usenocache:(_global$.getQuery('debug'))
                });

                FEjsonplug._startlisteners();
                FEjsonloader._startlisteners();


                //find full episode template
                $.each($('[data-uiType="full_episode_template"]'),function(index,value){
                    //HIDE THIS OBJECT TILL ITS DONE BEING CREATED (you really should hardcode the class so its invisible right away)
                    $(value).addClass('hidden'); //
                    var uiData = $(value).attr('data-uiData');




                    uiData = eval("(" + uiData + ")");//get JSON

                    //hide button
                    $('[data-uiType="'+uiData.loadmore_button_uiType+'"]').hide();

                    if(uiData.loadlist!=undefined){

                        //Build the load list URL
                        var buildloadurl=uiData.loadlist;

                        if (!_global$.islocalhost){
                            buildloadurl+='/0/'+uiData.loadcount;
                        }
                        //PLACE the data in an abject incase we need it later
                        templateHolder.push({
                            target_obj:value,
                            html_template:$(value).html(),
                            data:uiData,
                            loadPageCount:uiData.loadcount,
                            currentPage:0
                        });
                        //LOAD JSON and plug it in
                        FEjsonloader._var({
                            file: buildloadurl,
                            callback: function (newloaddata) {
                                //BUILD
                                var originalHTML=$(value).html();
                                $(value).empty();
                                //BUILD each ROW
                                $.each(newloaddata.items,function(e_index,e_value){
                                    e_value= $.extend(e_value,uiData);
                                    FEjsonplug._var({
                                        dataIN:modifyData(e_value),
                                        htmlIN:originalHTML,
                                        callback:function(renderedHTML){
                                            $(value).append(modifyHTML(renderedHTML));
                                        }
                                    })
                                    FEjsonplug.refresh();
                                });
                                //SHOW ROWS
                                $(value).removeClass('hidden');
                                //POPULATE ROWS

                                if(Number(newloaddata.total)>$(value).children().length){
                                        $('[data-uiType="'+uiData.loadmore_button_uiType+'"]').show();
                                    }

                                //alert(Number(newloaddata.total)+' '+$(value).children().length)
//                                if(newloaddata.length==Number(uiData.loadcount)){
//                                    $('[data-uiType="'+uiData.loadmore_button_uiType+'"]').show();
//                                }


                                applyCarousels();
                            }
                        });
                        FEjsonloader.refresh();

                    } else {
                        //NO LOADLIST
                        alert('please define loadlist (json data for the scrollers)!');
                    }
                });



                $.each(templateHolder,function(index,value){
                    var self=templateHolder[index];
                   // alert(value.html_template)
                   // alert(JSON.stringify(value.data));
                   // alert();

//                    $.each($('[data-uiType="'+value.data.loadmore_button_uiType+'"]'),function(button_index,button_value){
//
//                    });


                    $(document).on('click','[data-uiType="'+value.data.loadmore_button_uiType+'"]',function(){
                      // alert('hi'+value.loadPageCount);
                     //   value.loadPageCount++
                       // window.location.href = $(this).attr('data-uiType').substring(14,999);

                        var thisButton=this;
                        var uiData=value.data;

                        //Build the load list URL
                        var buildloadurl=uiData.loadlist;

                        value.currentPage++
                       // alert(value.currentPage)
                        if (!_global$.islocalhost){
                            buildloadurl+='/'+value.currentPage+'/'+value.loadPageCount;
                        }


                        //LOAD JSON and plug it in
                        FEjsonloader._var({
                            file: buildloadurl,
                            callback: function (newloaddata) {
                                //BUILD
                                var originalHTML=value.html_template
                               // $(value).empty();
                                //BUILD each ROW
                                $.each(newloaddata.items,function(e_index,e_value){
                                    e_value= $.extend(e_value,uiData);
                                    FEjsonplug._var({
                                        dataIN:modifyData(e_value),
                                        htmlIN:originalHTML,
                                        callback:function(renderedHTML){
                                            //alert(renderedHTML)
                                            $(value.target_obj).append(modifyHTML(renderedHTML));
                                        }
                                    })
                                    FEjsonplug.refresh();
                                });
                                //SHOW ROWS
                                //$(value).removeClass('hidden');
                                //POPULATE ROWS

                                // if(Number(newloaddata.total)>$(value).children().length){
                                //alert( ($(value.target_obj).children().length>=Number(newloaddata.total))  );
                                var Showingall= ($(value.target_obj).children().length>=Number(newloaddata.total));

                                if(newloaddata.length<Number(value.loadPageCount)  || Showingall){

                                    $(thisButton).hide();

                                }

                                applyCarousels();
                            }
                        });
                        FEjsonloader.refresh();


















                    });




                })


            }
            //Third MAKE THEM CAROUSELS!

        }
        var startAdobePassListeners = function (){
            try {
                aetn.video.adobePass.addEventListener( 'displayUserAsUnAuthenticated', function(){
                    console.log('removeClass(hideOnVLoginStart)');
                    $('[data-uiType="hideOnVLoginStart"]').removeClass('hideOnVLoginStart'); //show login
                });

                aetn.video.adobePass.addEventListener( 'displayUserAsAuthenticated', function(){
                    console.log('addClass(hideOnVLoginStart)');
                    $('[data-uiType="hideOnVLoginStart"]').addClass('hideOnVLoginStart'); //hide login

                });

            }
            catch (e) {
                console.log('keep hidden if On Adobe.pass FAIL');
            }

        };
        var showhideOnAdobePass = function(){
           // alert('p3');

            var isAndroid=$('body').hasClass('android');
            var isIOS=$('body').hasClass('iphone')||$('body').hasClass('ipad');
//            if(!(isAndroid||isIOS)){
                //startAdobePassListeners();
                $('[data-uiType="hideOnVLoginStart"]').removeClass('hideOnVLoginStart');


           // };

            //placeAd();
            //var waitforAnimation=setTimeout(placeAd,1000);
        };



//        var checkAdobePass = function(OBJ){
//
//            if(!aetn.video.adobePass.isAuthenticated()){
//                console.log('unhiding login block');
//                $(OBJ).removeClass('hideOnVLoginStart'); //show login
//            } else {
//                console.log('hiding login block');
//                $(OBJ).addClass('hideOnVLoginStart'); //hide login
//
//            }
//        }


        makeButtons();


        //unhide stuff on *not* mobile
        $('.hideOnMobile').each(function(idx,value){
            var isAndroid=$('body').hasClass('android');
            var isIOS=$('body').hasClass('iphone')||$('body').hasClass('ipad');
            if(!(isAndroid||isIOS)){$(value).removeClass('hideOnMobile')};
        })



//        _notify.add('slider_instance', function() {
//            return {
//                onWindowScroll:function(o){
//                    alert('!'+ o.data.y +' : '+_global$.windowHeight)
//                }
//            }
//        }());




//        <iframe id="aetn_dart_2" iframe="" frameborder="0" scrolling="no" width="300" height="250" src="http://local.movies.mylifetime.com/profiles/mylifetime_com/modules/aenetworks/aetn_dart/iframe/multisize-iframe.html?dfp=N5936&amp;path=ltv.myltv.movies%2Flanding%3Bs1%3Dlanding%3Bs2%3D%3Bpid%3Dwatch-full-episodes-online%3Bmovie%3D%3Bptype%3D%3Baetn%3Dad%3Bmovies%3Dltv%3Btest%3D%3Bkuid%3Dokob4snnl%3Bpos%3Dtop%3Bsz%3D300x250%3Bshow%3D%3Btile%3D2%3Bord%3D1730669341%3F"></iframe>


        //alert($('div.standalone_ad_container').html())//$('div.standalone_ad_container').appendTo('#testOBJ');
        //alert('?')

        //$('#testOBJ').append($('div.standalone_ad_container').html())













    });


// End main slideshow_promo instance




