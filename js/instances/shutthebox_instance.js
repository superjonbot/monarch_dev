/**
 * shutthebox Instance
 * sample by Jonathan Robles
 *
 * Date: 2/19/14 : 1:57 PM
 *
 */

// Start main shutthebox instance

require(['jquery', 'templates/shutthebox'],
    function ($, Shutthebox) {

        var myinstancetest = new Shutthebox({
            //variable:value
        });

        myinstancetest.init();


    });


// End main shutthebox instance




