/**
 * bubble Instance
 * sample by Jonathan Robles
 *
 * Date: 6/23/14 : 3:30 PM
 *
 */

// Start main bubble instance

require(['jquery', 'templates/bubble'],
    function ($, Bubble) {

        var myinstancetest = new Bubble({
            //variable:value
        });

        myinstancetest.init();


    });


// End main bubble instance




