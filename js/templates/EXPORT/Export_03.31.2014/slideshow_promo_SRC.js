/*!Last Updated: 03.31.2014[17.03.59] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! slideshow_promo MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLS*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.31.2014[17.03.59] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule", "knockout", "modules/controllers/swipeset", "tweenmax", "modules/view/pinchzoom" ], function($, parentObject, ko, Swipeset, TweenMax, Pinch) {
    function _thisObject_(o) {
        var settings = {
            type: "slideshow_promo",
            author: "Jonathan Robles",
            lasteditby: "",
            slide_Swipeset: undefined,
            slidebox_target: $("#slidebox"),
            pinchZoomset: undefined,
            disableNavigation: true,
            knockout_viewmodel: undefined,
            mail_knockout_viewmodel: undefined,
            scrolluptheseobjs: [],
            autoscrolltheseobjs: [],
            lastscenehtmlclass: "",
            toastedclasses: [],
            app_target: $("#app-main"),
            menuanim: {
                anim_target: $("#app-main>.presentation_layer"),
                anim_time: .5
            },
            shareanim: {
                target: $("#app-main>.share"),
                anim_target: $("#app-main>.share>div"),
                anim_time: .5
            },
            emailanim: {
                target: $("#emailblock"),
                anim_time: .5
            },
            toggle_hiddenclass: "hidethis",
            toggleposition_open: function(target) {},
            toggleposition_close: function(target) {},
            callback: function() {
                $("#app-main").removeClass("hideapp");
            },
            busy: false
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var self = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onWindowWidth: function(o) {
                    self.refresh();
                    window.scrollTo(0, 0);
                }
            };
        }());
    };
    _thisObject_.prototype.knockout_viewmodel = function(Swipeset, Menuanim, Shareanim, Zoomset, _var) {
        var self = this;
        self.navdisabled = ko.observable(_var.disableNavigation);
        self.showthumbs = ko.observable(false);
        self.Zoomset = Zoomset;
        self.menuopen = ko.observable(false);
        self.shareopen = ko.observable(false);
        self.slide_currentIndex = ko.observable(Swipeset._var().currentslide);
        self.slide_currentIndex_display = ko.computed(function() {
            return self.slide_currentIndex() + 1;
        });
        self.slide_totalCount = ko.observable(Swipeset._var().totalslides);
        self.controlmenu = ko.computed(function() {
            if (self.menuopen()) {
                var destinationX = Menuanim.anim_target.innerWidth() - 70;
                TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                    x: destinationX
                });
            } else {
                TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                    x: 0
                });
            }
        });
        self.controlshare = ko.computed(function() {
            if (self.shareopen()) {
                Shareanim.target.removeClass(_var.toggle_hiddenclass);
            } else {
                Shareanim.target.addClass(_var.toggle_hiddenclass);
            }
        });
        self.controlthumbs = ko.computed(function() {
            if (self.showthumbs()) {
                $("#thumbOverlay").removeClass(_var.toggle_hiddenclass);
            } else {
                $("#thumbOverlay").addClass(_var.toggle_hiddenclass);
            }
        });
        self.disable_next = ko.computed(function() {
            return self.slide_currentIndex() == self.slide_totalCount() - 1 || self.navdisabled() ? true : false;
        });
        self.disable_prev = ko.computed(function() {
            return self.slide_currentIndex() == 0 || self.navdisabled() ? true : false;
        });
        self.disable_navigation = ko.computed(function() {
            if (self.navdisabled()) {
                window._global$.setCookie("roadblock", "true");
                Swipeset._var().enable_touchbinds = false;
            } else {
                window._global$.setCookie("roadblock", "false");
                Swipeset._var().enable_touchbinds = true;
            }
        });
        self.databindtoobject = function(getEvent) {
            try {
                var string = $(getEvent.target).attr("data-bind");
                var properties = string.split(",");
            } catch (err) {
                var string = $(getEvent.srcElement).attr("data-bind");
                var properties = string.split(",");
            }
            var obj = {};
            $.each(properties, function(i, v) {
                var tup = v.split(":");
                obj[tup[0]] = tup[1];
            });
            return obj;
        };
        self.slide_button = function(data, event) {
            var switchindex = Number(self.databindtoobject(event).target);
            Swipeset.pagejump(switchindex);
            if (self.menuopen()) {
                self.menu_toggle();
            }
            if (self.showthumbs()) {
                self.thumbs_toggle();
            }
        };
        self.slide_next = function() {
            if (!self.navdisabled()) {
                Swipeset.next();
            }
        };
        self.slide_prev = function() {
            if (!self.navdisabled()) {
                Swipeset.prev();
            }
        };
        self.menu_toggle = function() {
            if (!self.navdisabled()) {
                self.menuopen(!self.menuopen());
            }
        };
        self.share_toggle = function() {
            self.shareopen(!self.shareopen());
        };
        self.thumbs_toggle = function() {
            self.showthumbs(!self.showthumbs());
        };
        self.nav_toggle = function() {
            self.navdisabled(!self.navdisabled());
            if (self.navdisabled() != _var.disableNavigation) {
                _var.disableNavigation = self.navdisabled();
            }
        };
        self.email_isopen = ko.observable(false);
        self.email_toggle = function() {
            console.log("EMAIL TOGGLE!");
            self.email_isopen(!self.email_isopen());
        };
        self.email_validator = function(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };
        self.email_closepopup = function() {
            console.log("email close");
            _var.emailanim.target.addClass(_var.toggle_hiddenclass);
        };
        self.email_openpopup = function() {
            console.log("email open");
            _var.emailanim.target.removeClass(_var.toggle_hiddenclass);
        };
        self.controlemail = ko.computed(function() {
            if (self.email_isopen()) {
                self.email_openpopup();
            } else {
                self.email_closepopup();
            }
        });
        self.email_yourname = ko.observable("");
        self.email_youremail = ko.observable("");
        self.email_fromemailnotvalid = ko.observable(false);
        self.email_toemailnotvalid = ko.observable(false);
        self.email_toemail = ko.observable("");
        self.email_subject = ko.observable("");
        self.email_text = ko.observable("");
        self.email_textbuild = function() {
            return "Your friend " + self.email_yourname() + ", sent you this: " + self.email_text();
        };
        self.email_submit = function() {
            self.email_fromemailnotvalid(!self.email_validator(self.email_youremail()));
            self.email_toemailnotvalid(!self.email_validator(self.email_toemail()));
            if (self.email_fromemailnotvalid() && self.email_toemailnotvalid()) {
                self.email_send();
            } else {
                var message = "";
                message += "self.email_fromemailnotvalid()" + self.email_fromemailnotvalid() + "/n";
                message += "self.email_toemailnotvalid()" + self.email_toemailnotvalid();
            }
        };
        self.email_send = function() {
            var emailinject = {
                fromPage: ".",
                template: "",
                encryptEmail: false,
                confirmationPage: "/features/feedback/noscan/cf/fb-load",
                subject: self.email_subject(),
                message: self.email_textbuild(),
                toEmail: self.email_toemail(),
                fromEmail: self.email_youremail()
            };
            var buildpostURL = "/postemail?";
            for (var name in emailinject) {
                var value = encodeURIComponent(emailinject[name]);
                buildpostURL += name + "=" + value + "&";
            }
            $.post(buildpostURL, function() {
                self.email_closepopup();
            });
        };
    };
    _thisObject_.prototype.mail_knockout_viewmodel = function(_var) {
        var self = this;
    };
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        var self = this;
        self.getdependancies();
        self.buildViewmodel();
        self._var().callback();
    };
    _thisObject_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        var self = this;
        if (self._var().knockout_viewmodel.menuopen()) {
            self._var().knockout_viewmodel.menu_toggle();
        }
        self._var().slide_Swipeset.refresh();
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype.scrollupOBJs = function() {
        var self = this;
        $.each(self._var().scrolluptheseobjs, function(idx, val) {
            val.scrollTop(0);
        });
    };
    _thisObject_.prototype.setclasses = function(currentslide) {
        var self = this;
        $("html").removeClass(self._var().lastscenehtmlclass);
        var buildpageclass = "slidepage_" + (currentslide + 1);
        $("html").addClass(buildpageclass);
        self._var().lastscenehtmlclass = buildpageclass;
        var buildpageclass = "toastpage_" + (currentslide + 1);
        $("html").removeClass(self._var().toastedclasses.slice(-1)[0]);
        if (!$.inArray(buildpageclass, self._var().toastedclasses)) {
            $("html").addClass(buildpageclass);
            self._var().toastedclasses.push(buildpageclass);
        }
    };
    _thisObject_.prototype.getdependancies = function() {
        var self = this;
        Swipeset.prototype.tackmatrix = function() {
            if (Modernizr.csstransforms && Modernizr.csstransforms3d) {
                $.each(this._var().childVars, function(index, value) {
                    if (value.target.css("transform") == "none") {
                        value.target.css("webkitTransform", "skew(0)");
                    }
                    value.matrix = value.target.css("transform");
                    value.matrix = value.matrix.match(/\(([^)]+)\)/)[1].split(",");
                    value.z = Number(value.matrix[0]);
                    value.x = Number(value.matrix[4]);
                    value.y = Number(value.matrix[5]);
                });
            }
        };
        self._var().slide_Swipeset = new Swipeset({
            usetouchbinds: window._global$.isTouch,
            target: self._var().slidebox_target,
            callback: function() {
                this.parent.tackmatrix();
                self.setclasses(this.currentslide);
            },
            onscroll_debounce_callback: function(o) {
                self._var().knockout_viewmodel.slide_currentIndex(this.currentslide);
                window.location.hash = "page=" + (this.currentslide + 1);
                window._global$.setCookie("page", this.currentslide + 1);
                self.scrollupOBJs();
                self.setclasses(Number(this.currentslide));
            },
            onscroll_debounce_rate: 100
        });
        var cookie_getpage = window._global$.getCookie("page");
        var cookie_isroadblocked = window._global$.getCookie("roadblock");
        if (cookie_getpage != null) {
            var jumppage = Number(cookie_getpage);
            jumppage -= 1;
            self._var().slide_Swipeset._var({
                currentslide: jumppage
            });
        }
        var checkhash = window._global$.getHash("page");
        var jumppage = Number(checkhash.substring(6, checkhash.length));
        if (cookie_isroadblocked == "true") {
            jumppage = 1;
            self._var().disableNavigation = true;
        } else {
            self._var().disableNavigation = false;
        }
        if (jumppage > 0) {
            jumppage -= 1;
            self._var().slide_Swipeset._var({
                currentslide: jumppage
            });
        }
        self._var().slide_Swipeset.init();
        self._var().pinchZoomset = new Pinch({
            target: $("#popFrame"),
            bindsonchild: true,
            childindex: 1
        });
    };
    _thisObject_.prototype.buildViewmodel = function() {
        var self = this;
        var parent = this;
        self._var().knockout_viewmodel = new this.knockout_viewmodel(self._var().slide_Swipeset, self._var().menuanim, self._var().shareanim, self._var().pinchZoomset, self._var());
        ko.bindingHandlers.isolatedOptions = {
            init: function(element, valueAccessor) {
                var args = arguments;
                ko.computed({
                    read: function() {
                        ko.utils.unwrapObservable(valueAccessor());
                        ko.bindingHandlers.options.update.apply(this, args);
                    },
                    owner: this,
                    disposeWhenNodeIsRemoved: element
                });
            }
        };
        ko.bindingHandlers.inverseChecked = {
            init: function(element, valueAccessor, allBindingsAccessor) {
                var value = valueAccessor();
                var interceptor = ko.computed({
                    read: function() {
                        return !value();
                    },
                    write: function(newValue) {
                        value(!newValue);
                    },
                    disposeWhenNodeIsRemoved: element
                });
                var newValueAccessor = function() {
                    return interceptor;
                };
                ko.utils.domData.set(element, "newValueAccessor", newValueAccessor);
                ko.bindingHandlers.checked.init(element, newValueAccessor, allBindingsAccessor);
            },
            update: function(element, valueAccessor) {
                ko.bindingHandlers.checked.update(element, ko.utils.domData.get(element, "newValueAccessor"));
            }
        };
        ko.bindingHandlers.gethtml = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var comparevalue = ko.unwrap(valueAccessor());
                objEkt.html($(comparevalue).html());
            }
        };
        ko.bindingHandlers.contenttopop = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var targetcontent = ko.unwrap(valueAccessor());
                $(element).click(function() {
                    var Content = objEkt.html();
                    $(targetcontent).removeClass(parent._var().toggle_hiddenclass);
                    var PopupContentArea = $(targetcontent).find(".popper_Content");
                    var CloseButton = $(targetcontent).find(".popper_Close");
                    PopupContentArea.empty();
                    PopupContentArea.html(Content);
                    viewModel.Zoomset.init();
                    viewModel.Zoomset.resetposition();
                    CloseButton.unbind("click").bind("click", function() {
                        $(targetcontent).addClass("hidethis");
                    });
                });
            }
        };
        ko.bindingHandlers.toggletarget = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
                $(element).click(function() {
                    if ($.inArray(databinds.className, $(target).attr("class"))) {
                        target.removeClass(databinds.className);
                    } else {
                        target.addClass(databinds.className);
                    }
                    self.refresh();
                });
            }
        };
        ko.bindingHandlers.toggletargethover = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
                $(element).hover(function() {
                    if ($.inArray(databinds.className, $(target).attr("class"))) {
                        target.removeClass(databinds.className);
                    } else {
                        target.addClass(databinds.className);
                    }
                });
            }
        };
        ko.bindingHandlers.sharebind = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var xsvalue = ko.unwrap(valueAccessor());
                var target = $(xsvalue);
                $.data(target, "dothisthat", {});
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var xsvalue = ko.unwrap(valueAccessor());
                var target = $(xsvalue);
                var trypostCp = function(data) {
                    try {
                        postCp(data);
                    } catch (exception) {
                        console.log('ERROR, no prerequisite function: "postCp"');
                    }
                };
                var trywmdPageLink = function(data) {
                    try {
                        wmdPageLink(data);
                    } catch (exception) {
                        console.log('ERROR, no prerequisite function: "wmdPageLink"');
                    }
                };
                function segVarPop(obj) {
                    if ($("meta[name=metasegvar]").length > 0) {
                        if (segVarParam("artid") != "0") {
                            obj.activityId = segVarParam("artid");
                        }
                        if (segVarParam("ssp") != "0") {
                            obj.leadSpec = segVarParam("ssp");
                        }
                        if (segVarParam("scg") != "0") {
                            obj.leadConcept = segVarParam("scg");
                        }
                        if (segVarParam("cg") != "0") {
                            obj.contentGroup = segVarParam("cg");
                        }
                        if (segVarParam("bc") != "__") {
                            var bcTemp = segVarParam("bc");
                            bcTemp = bcTemp.substring(1, bcTemp.length - 1);
                            bcTemp = bcTemp.split("_");
                            obj.blockCode = bcTemp;
                        }
                    }
                }
                var socialCp = function(platform) {
                    var cpSocData = new Object();
                    cpSocData.appname = "social-functions";
                    cpSocData.activityName = encodeURIComponent(platform);
                    segVarPop(cpSocData);
                    trypostCp(cpSocData);
                };
                var sharobjs = {
                    facebook: {
                        runtime: function() {
                            trywmdPageLink("ar-share_face");
                            var fbtitle = document.title;
                            var fblink = window.location;
                            fblink = fblink + "?src=stfb";
                            var fbsharelink = "http://www.facebook.com/share.php?u=" + fblink + "&t=" + fbtitle;
                            socialCp("Facebook");
                            window.open(fbsharelink);
                        }
                    },
                    twitter: {
                        runtime: function() {
                            trywmdPageLink("ar-share_twit");
                            var twtitle = document.title;
                            var twiturl = window.location;
                            twiturl = twiturl + "?src=sttwit";
                            var ftwiturl = "http://twitter.com/share?url=" + encodeURIComponent(twiturl) + "&text=" + encodeURIComponent(twtitle);
                            socialCp("Twitter");
                            window.open(ftwiturl);
                        }
                    },
                    google: {
                        runtime: function() {
                            trywmdPageLink("ar-share_google");
                            var ggtitle = document.title;
                            var googleurl = window.location;
                            googleurl = googleurl + "?src=stgoogle";
                            var ftgoogleurl = "https://plusone.google.com/_/+1/confirm?url=" + encodeURIComponent(googleurl) + "&title=" + encodeURIComponent(ggtitle);
                            socialCp("Google+");
                            window.open(ftgoogleurl);
                        }
                    },
                    linkedin: {
                        runtime: function() {
                            var title = document.title;
                            var url = window.location;
                            url += "?src=stlinkedin";
                            var fturl = "http://www.linkedin.com/cws/share?url=" + url + "&title=" + title;
                            socialCp("LinkedIn");
                            window.open(fturl);
                        }
                    },
                    jonathanisawesome: {
                        runtime: function() {
                            alert("yes he is!");
                        }
                    }
                };
                $(element).click(function() {
                    if (typeof sharobjs[xsvalue] != "object") {
                        alert('Que Pasa? where did you get the sharebind called "' + xsvalue + '"??? Please check your databindings for this button');
                    } else {
                        sharobjs[xsvalue].runtime();
                    }
                });
            }
        };
        ko.bindingHandlers.framestop = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
                parent._var().scrolluptheseobjs.push(objEkt);
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
            }
        };
        ko.bindingHandlers.offsettocontentheight = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
                $(target).data("isopen", false);
                $(element).unbind("click");
                $(element).click(function() {
                    var offsetnumber = 0 - $(databinds.ofObject).innerHeight();
                    if (!$(target).data("isopen")) {
                        TweenMax.to($(target), .5, {
                            y: offsetnumber
                        });
                        $(target).data("isopen", true);
                    } else {
                        TweenMax.to($(target), .5, {
                            y: 0
                        });
                        $(target).data("isopen", false);
                    }
                    self.refresh();
                });
            }
        };
        ko.bindingHandlers.offsettocontentheighthover = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
                $(target).data("isopen", false);
                $(element).unbind("click");
                $(element).hover(function() {
                    var offsetnumber = 0 - $(databinds.ofObject).innerHeight();
                    TweenMax.to($(target), .5, {
                        y: offsetnumber
                    });
                    $(target).data("isopen", true);
                    self.refresh();
                }, function() {
                    TweenMax.to($(target), .5, {
                        y: 0
                    });
                    $(target).data("isopen", false);
                    self.refresh();
                });
            }
        };
        ko.applyBindings(self._var().knockout_viewmodel);
    };
    return _thisObject_;
});
