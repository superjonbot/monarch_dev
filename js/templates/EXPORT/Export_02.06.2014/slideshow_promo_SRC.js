/*!Last Updated: 02.06.2014[17.14.21] by Jonathan Robles*/



/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
//  slideshow_promo MODULE                                  
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/

 // NOTE: slideshow for promo
 //
 // USAGE: see instance
 //
/*CL1*/


/*!Last Updated: 02.06.2014[17.14.21] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule", "knockout", "modules/controllers/swipeset", "tweenmax", "modules/view/pinchzoom" ], function($, parentModel, ko, Swipeset, TweenMax, Pinch) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "slideshow_promo",
            author: "Jonathan Robles",
            lasteditby: "",
            slide_Swipeset: undefined,
            slidebox_target: $("#slidebox"),
            pinchZoomset: undefined,
            disableNavigation: true,
            viewmodel: undefined,
            app_target: $("#app-main"),
            menuanim: {
                anim_target: $("#app-main>.presentation_layer"),
                anim_time: .5
            },
            shareanim: {
                target: $("#app-main>.share"),
                anim_target: $("#app-main>.share>div"),
                anim_time: .5
            },
            toggleposition_open: function(target) {},
            toggleposition_close: function(target) {},
            callback: function() {},
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var self = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onWindowWidth: function(o) {
                    if (self._var().viewmodel.menuopen()) self._var().viewmodel.menu_toggle();
                    self._var().slide_Swipeset.refresh();
                }
            };
        }());
    };
    _thizOBJ_.prototype.viewmodel = function(Swipeset, Menuanim, Shareanim, Zoomset, _var) {
        var self = this;
        self.navdisabled = ko.observable(_var.disableNavigation);
        self.showthumbs = ko.observable(false);
        self.Zoomset = Zoomset;
        self.menuopen = ko.observable(false);
        self.shareopen = ko.observable(false);
        self.slide_currentIndex = ko.observable(Swipeset._var().currentslide);
        self.slide_currentIndex_display = ko.computed(function() {
            return self.slide_currentIndex() + 1;
        });
        self.slide_totalCount = ko.observable(Swipeset._var().totalslides);
        self.controlmenu = ko.computed(function() {
            if (self.menuopen()) {
                var destinationX = Menuanim.anim_target.innerWidth() - 70;
                TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                    x: destinationX
                });
            } else TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                x: 0
            });
        });
        self.controlshare = ko.computed(function() {
            if (self.shareopen()) {
                Shareanim.target.removeClass("hidethis");
                TweenMax.to(Shareanim.anim_target, .25, {
                    opacity: 1,
                    y: 0
                });
            } else TweenMax.to(Shareanim.anim_target, .25, {
                opacity: 0,
                y: 25,
                onComplete: function() {
                    Shareanim.target.addClass("hidethis");
                }
            });
        });
        self.controlthumbs = ko.computed(function() {
            if (self.showthumbs()) {
                $("#thumbOverlay").removeClass("hidethis");
                TweenMax.to($("#thumbOverlay"), .25, {
                    opacity: 1,
                    y: 0
                });
            } else TweenMax.to($("#thumbOverlay"), .25, {
                opacity: 0,
                y: 25,
                onComplete: function() {
                    $("#thumbOverlay").addClass("hidethis");
                }
            });
        });
        self.disable_next = ko.computed(function() {
            return self.slide_currentIndex() == self.slide_totalCount() - 1 || self.navdisabled() ? true : false;
        });
        self.disable_prev = ko.computed(function() {
            return self.slide_currentIndex() == 0 || self.navdisabled() ? true : false;
        });
        self.disable_navigation = ko.computed(function() {
            if (self.navdisabled()) Swipeset._var().enable_touchbinds = false; else Swipeset._var().enable_touchbinds = true;
        });
        self.databindtoobject = function(getEvent) {
            try {
                var string = $(getEvent.target).attr("data-bind");
                var properties = string.split(",");
            } catch (err) {
                var string = $(getEvent.srcElement).attr("data-bind");
                var properties = string.split(",");
            }
            var obj = {};
            $.each(properties, function(i, v) {
                var tup = v.split(":");
                obj[tup[0]] = tup[1];
            });
            return obj;
        };
        self.slide_button = function(data, event) {
            var switchindex = Number(self.databindtoobject(event).target);
            Swipeset.pagejump(switchindex);
            if (self.menuopen()) self.menu_toggle();
            if (self.showthumbs()) self.thumbs_toggle();
        };
        self.slide_next = function() {
            if (!self.navdisabled()) Swipeset.next();
        };
        self.slide_prev = function() {
            if (!self.navdisabled()) Swipeset.prev();
        };
        self.menu_toggle = function() {
            if (!self.navdisabled()) self.menuopen(!self.menuopen());
        };
        self.share_toggle = function() {
            self.shareopen(!self.shareopen());
        };
        self.thumbs_toggle = function() {
            self.showthumbs(!self.showthumbs());
        };
        self.nav_toggle = function() {
            self.navdisabled(!self.navdisabled());
            if (self.navdisabled() != _var.disableNavigation) _var.disableNavigation = self.navdisabled();
        };
    };
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        var self = this;
        self.getdependancies();
        self.buildViewmodel();
        self._var().callback();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        var self = this;
        alert("this module has no refresh function");
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.getdependancies = function() {
        var self = this;
        Swipeset.prototype.tackmatrix = function() {
            $.each(this._var().childVars, function(index, value) {
                if (value.target.css("transform") == "none") value.target.css("webkitTransform", "skew(0)");
                value.matrix = value.target.css("transform");
                value.matrix = value.matrix.match(/\(([^)]+)\)/)[1].split(",");
                value.z = Number(value.matrix[0]);
                value.x = Number(value.matrix[4]);
                value.y = Number(value.matrix[5]);
            });
        };
        self._var().slide_Swipeset = new Swipeset({
            target: self._var().slidebox_target,
            callback: function() {
                this.parent.tackmatrix();
            },
            onscroll_debounce_callback: function(o) {
                self._var().viewmodel.slide_currentIndex(this.currentslide);
                window.location.hash = "page=" + (this.currentslide + 1);
                window._global$.setCookie("page", this.currentslide + 1);
            },
            onscroll_debounce_rate: 200
        });
        var cookiecheck = window._global$.getCookie("page");
        if (cookiecheck != null) {
            var jumppage = Number(cookiecheck);
            jumppage -= 1;
            self._var().slide_Swipeset._var({
                currentslide: jumppage
            });
            self._var().disableNavigation = false;
        }
        var jumppage = Number(window._global$.getQuery("page"));
        if (!isNaN(jumppage) && jumppage > 0) {
            jumppage -= 1;
            self._var().slide_Swipeset._var({
                currentslide: jumppage
            });
            self._var().disableNavigation = false;
        }
        var checkhash = window._global$.getHash("page");
        var jumppage = Number(checkhash.substring(6, checkhash.length));
        if (jumppage > 0) {
            jumppage -= 1;
            self._var().slide_Swipeset._var({
                currentslide: jumppage
            });
            self._var().disableNavigation = false;
        }
        self._var().slide_Swipeset.init();
        self._var().pinchZoomset = new Pinch({
            target: $("#popFrame"),
            bindsonchild: true,
            childindex: 1
        });
    };
    _thizOBJ_.prototype.buildViewmodel = function() {
        var self = this;
        self._var().viewmodel = new this.viewmodel(self._var().slide_Swipeset, self._var().menuanim, self._var().shareanim, self._var().pinchZoomset, self._var());
        ko.bindingHandlers.isolatedOptions = {
            init: function(element, valueAccessor) {
                var args = arguments;
                ko.computed({
                    read: function() {
                        ko.utils.unwrapObservable(valueAccessor());
                        ko.bindingHandlers.options.update.apply(this, args);
                    },
                    owner: this,
                    disposeWhenNodeIsRemoved: element
                });
            }
        };
        ko.bindingHandlers.inverseChecked = {
            init: function(element, valueAccessor, allBindingsAccessor) {
                var value = valueAccessor();
                var interceptor = ko.computed({
                    read: function() {
                        return !value();
                    },
                    write: function(newValue) {
                        value(!newValue);
                    },
                    disposeWhenNodeIsRemoved: element
                });
                var newValueAccessor = function() {
                    return interceptor;
                };
                ko.utils.domData.set(element, "newValueAccessor", newValueAccessor);
                ko.bindingHandlers.checked.init(element, newValueAccessor, allBindingsAccessor);
            },
            update: function(element, valueAccessor) {
                ko.bindingHandlers.checked.update(element, ko.utils.domData.get(element, "newValueAccessor"));
            }
        };
        ko.bindingHandlers.gethtml = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var comparevalue = ko.unwrap(valueAccessor());
                objEkt.html($(comparevalue).html());
            }
        };
        ko.bindingHandlers.contenttopop = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var targetcontent = ko.unwrap(valueAccessor());
                $(element).click(function() {
                    var Content = objEkt.html();
                    $(targetcontent).removeClass("hidethis");
                    var PopupContentArea = $(targetcontent).find(".popper_Content");
                    var CloseButton = $(targetcontent).find(".popper_Close");
                    PopupContentArea.empty();
                    PopupContentArea.html(Content);
                    viewModel.Zoomset.init();
                    viewModel.Zoomset.resetposition();
                    CloseButton.unbind("click").bind("click", function() {
                        $(targetcontent).addClass("hidethis");
                    });
                });
            }
        };
        ko.bindingHandlers.toggleposition = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
                $.data(target, "isopened", false);
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
                $(element).click(function() {
                    if ($.data(target, "isopened")) {
                        self._var().toggleposition_close(target);
                        target.removeClass(databinds.className);
                        $.data(target, "isopened", false);
                    } else {
                        self._var().toggleposition_open(target);
                        target.addClass(databinds.className);
                        $.data(target, "isopened", true);
                    }
                });
            }
        };
        ko.applyBindings(self._var().viewmodel);
    };
    return _thizOBJ_;
});