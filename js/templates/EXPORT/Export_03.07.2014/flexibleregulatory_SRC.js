/*!Last Updated: 03.07.2014[21.54.13] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! flexibleregulatory MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.07.2014[21.54.13] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule", "knockout", "modules/controllers/slideset", "tweenmax" ], function($, parentObject, ko, Slideset, TweenMax) {
    function _thisObject_(o) {
        var settings = {
            type: "flexibleregulatory",
            author: "Jonathan Robles",
            lasteditby: "",
            info_Slideset: undefined,
            slide_Slideset: undefined,
            knockout_viewmodel: undefined,
            app_target: $("#app-main"),
            infobox_target: $("#infobox"),
            slidebox_target: $("#slidebox"),
            menuanim: {
                anim_target: $("#app-main>.bot-right"),
                anim_time: .5
            },
            shareanim: {
                target: $("#app-main>.share"),
                anim_target: $("#app-main>.share>div"),
                anim_time: .5
            },
            callback: function() {
                this.app_target.css("opacity", 1);
            },
            busy: false
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var self = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onWindowWidth: function(o) {
                    if (this._var().knockout_viewmodel.menuopen()) {
                        this._var().knockout_viewmodel.menu_toggle();
                    }
                }
            };
        }());
    };
    _thisObject_.prototype.knockout_viewmodel = function(Slideset, Infoset, Menuanim, Shareanim) {
        var self = this;
        this.menuopen = ko.observable(false);
        this.shareopen = ko.observable(false);
        this.slide_currentIndex = ko.observable(Slideset._var().currentslide);
        this.slide_totalCount = ko.observable(Slideset._var().totalslides);
        this.info_currentIndex = ko.observable(Infoset._var().currentslide);
        this.info_totalCount = ko.observable(Infoset._var().totalslides);
        this.controlmenu = ko.computed(function() {
            if (this.menuopen()) {
                var destinationX = Menuanim.anim_target.innerWidth() - 70;
                TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                    x: destinationX
                });
            } else {
                TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                    x: 0
                });
            }
        });
        this.controlshare = ko.computed(function() {
            if (this.shareopen()) {
                Shareanim.target.show();
                TweenMax.to(Shareanim.anim_target, .25, {
                    opacity: 1,
                    y: 0
                });
            } else {
                TweenMax.to(Shareanim.anim_target, 0, {
                    opacity: 0,
                    y: 25
                });
                Shareanim.target.hide();
            }
        });
        this.disable_next = ko.computed(function() {
            return this.slide_currentIndex() == this.slide_totalCount() - 1 ? true : false;
        });
        this.disable_prev = ko.computed(function() {
            return this.slide_currentIndex() == 0 ? true : false;
        });
        this.databindtoobject = function(getEvent) {
            try {
                var string = $(getEvent.target).attr("data-bind");
                var properties = string.split(",");
            } catch (err) {
                var string = $(getEvent.srcElement).attr("data-bind");
                var properties = string.split(",");
            }
            var obj = {};
            $.each(properties, function(i, v) {
                var tup = v.split(":");
                obj[tup[0]] = tup[1];
            });
            return obj;
        };
        this.info_button = function(data, event) {
            var switchcount = Number(this.databindtoobject(event).target);
            Infoset.jump(switchcount);
        };
        this.slide_button = function(data, event) {
            var switchcount = Number(this.databindtoobject(event).target);
            Slideset.pagejump(switchcount);
            if (this.menuopen()) {
                this.menu_toggle();
            }
        };
        this.slide_next = function() {
            Slideset.next();
        };
        this.slide_prev = function() {
            Slideset.prev();
        };
        this.menu_toggle = function() {
            this.menuopen(!this.menuopen());
        };
        this.share_toggle = function() {
            this.shareopen(!this.shareopen());
        };
        Slideset._var().childtrack = function(count) {
            this.slide_currentIndex(count);
        };
        Infoset._var().childtrack = function(count) {
            this.info_currentIndex(count);
        };
    };
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        var self = this;
        this.getdependancies();
        this.buildViewmodel();
        this._var().callback();
    };
    _thisObject_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        var self = this;
        alert("this module has no refresh function");
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype.getdependancies = function() {
        var self = this;
        this._var().info_Slideset = new Slideset({
            target: this._var().infobox_target,
            child_focus: function(o) {
                var argz = o;
                var tempObject = $(this.childObjects[argz.count]);
                tempObject.show();
            },
            child_unfocus: function(o) {
                var argz = o;
                var tempObject = $(this.childObjects[argz.unfocusedcount]);
                tempObject.hide();
            }
        });
        this._var().slide_Slideset = new Slideset({
            target: this._var().slidebox_target,
            autoZ: true,
            child_focus: function(o) {
                var argz = {
                    count: undefined,
                    speed: .5,
                    tweenvars: {
                        x: 0,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    }
                };
                argz = $.extend(argz, o);
                var tempObject = $(this.childObjects[argz.count]);
                if (Modernizr.csstransitions) {
                    TweenMax.to(tempObject, argz.speed, argz.tweenvars);
                } else {
                    tempObject.show();
                }
            },
            child_unfocus: function(o) {
                var argz = {
                    count: undefined,
                    unfocusedcount: undefined,
                    speed: 1,
                    offset: 0,
                    tweenvarsIN: {
                        x: -400,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    },
                    tweenvarsOUT: {
                        x: 400,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    }
                };
                argz = $.extend(argz, o);
                var tempObject = $(this.childObjects[argz.unfocusedcount]);
                if (Modernizr.csstransitions) {
                    if (argz.unfocusedcount < argz.count) {
                        TweenMax.to(tempObject, argz.speed, argz.tweenvarsIN);
                    } else {
                        TweenMax.to(tempObject, argz.speed, argz.tweenvarsOUT);
                    }
                } else {
                    tempObject.hide();
                }
            }
        });
        this._var().info_Slideset.init();
        this._var().slide_Slideset.init();
    };
    _thisObject_.prototype.buildViewmodel = function() {
        var self = this;
        this._var().knockout_viewmodel = new this.knockout_viewmodel(this._var().slide_Slideset, this._var().info_Slideset, this._var().menuanim, this._var().shareanim);
        ko.bindingHandlers.isolatedOptions = {
            init: function(element, valueAccessor) {
                var args = arguments;
                ko.computed({
                    read: function() {
                        ko.utils.unwrapObservable(valueAccessor());
                        ko.bindingHandlers.options.update.apply(this, args);
                    },
                    owner: this,
                    disposeWhenNodeIsRemoved: element
                });
            }
        };
        ko.bindingHandlers.CSSonMatch = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var objEkt = $(element);
                var databinds = allBindings();
                var comparevalue = ko.unwrap(valueAccessor());
                if (databinds.CSSclass != undefined) {
                    if (databinds.target != undefined) {
                        if (comparevalue == databinds.target) {
                            objEkt.addClass(databinds.CSSclass);
                        } else {
                            objEkt.removeClass(databinds.CSSclass);
                        }
                    } else {
                        alert("no target is defined with Jonathan's awesome CSSonMatch BINDING!");
                    }
                } else {
                    alert("no CSSclass is defined with Jonathan's awesome CSSonMatch BINDING!");
                }
            }
        };
        ko.applyBindings(this._var().knockout_viewmodel);
    };
    return _thisObject_;
});
