/*!Last Updated: 03.07.2014[21.54.15] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! shutthebox MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.07.2014[21.54.15] by Jonathan Robles*/
define([ "jquery", "underscore", "modules/definitions/standardmodule", "tweenmax" ], function($, _, parentObject, TweenMax) {
    function _thisObject_(o) {
        var settings = {
            type: "shutthebox",
            author: "Jonathan Robles",
            lasteditby: "",
            cardtarget: $("#cards"),
            dicetarget: $("#dice"),
            currentturn: 0,
            tryrandomcards: false,
            numberofdice: 2,
            lasttotal: undefined,
            Cards: [],
            makenewCard: function(count, value) {
                var this = {
                    count: count,
                    cardnumber: value,
                    selected: false,
                    used: false
                };
                return this;
            },
            rolldice: function(options) {
                var this = {
                    dicevalues: [],
                    total: 0
                };
                for (tempvar = 0; tempvar < options.dicecount; tempvar++) {
                    var randomNum = Math.floor(Math.random() * options.maxnumber + options.minnumber);
                    this.dicevalues.push(randomNum);
                    this.total += randomNum;
                }
                return this;
            },
            isvalueinpool: function(value, pool) {
                var isthere = false;
                pool.sort(function(a, b) {
                    return a - b;
                }).reverse();
                console.log(pool);
                console.log(pool.countOf(value));
                if (pool.countOf(value) != -1) {
                    isthere = true;
                } else {
                    var thesums = [];
                    for (a = 0; a < pool.length; a++) {
                        console.log("checking: " + pool[0] + " > " + value);
                        if (pool[0] > value) {
                            pool.shift();
                        }
                    }
                    pool.sort(function(a, b) {
                        return a - b;
                    });
                    $.each(pool, function(count, value) {
                        console.log(value);
                        thesums.push(value);
                        var temppool = pool.slice(0);
                        temppool.splice(count, 1);
                        $.each(temppool, function(t_count, t_value) {
                            console.log(value + " " + t_value);
                            thesums.push(value + t_value);
                            var temppoolb = temppool.slice(0);
                            temppoolb.splice(t_count, 1);
                            $.each(temppoolb, function(u_count, u_value) {
                                console.log(value + " " + t_value + " " + u_value);
                                thesums.push(value + t_value + u_value);
                            });
                        });
                    });
                    if (thesums.countOf(value) != -1) {
                        isthere = true;
                    }
                }
                return isthere;
            },
            shufflecards: function(count) {
                var this = this.parent;
                var makenewCard_local = this._var().makenewCard;
                this._var().Cards = [];
                for (tempcnt = 0; tempcnt < count; tempcnt++) {
                    if (this._var().tryrandomcards) {
                        var randomNum = Math.floor(Math.random() * 13 + 1);
                        this._var().Cards.push(new makenewCard_local(tempcnt, randomNum));
                    } else {
                        this._var().Cards.push(new makenewCard_local(tempcnt, tempcnt + 1));
                    }
                }
            },
            displaydice: function() {
                var this = this.parent;
                var dice = this._var().rolldice({
                    dicecount: this._var().numberofdice,
                    minnumber: 1,
                    maxnumber: 6
                });
                this._var().dicetarget.empty();
                $.each(dice.dicevalues, function(count, value) {
                    this._var().dicetarget.append('<div id="dice_' + count + '" class="dice dice_' + value + '">' + value + "</div>");
                });
                var sum = _.reduce(dice.dicevalues, function(memo, num) {
                    return memo + num;
                }, 0);
                this._var().lasttotal = sum;
                this._var().dicetarget.append('<div id="dicetotal" class="dicetotal"> = ' + sum + "</div>");
                TweenMax.to($("#dicetotal"), 0, {
                    opacity: 0
                });
                $.each(dice.dicevalues, function(count, value) {
                    TweenMax.from($("#dice_" + count), 1, {
                        ease: Bounce.easeOut,
                        y: 200,
                        rotation: 1e3,
                        scale: 5,
                        opacity: 0,
                        delay: count * .2,
                        onUpdate: function() {
                            var randomNum = Math.floor(Math.random() * 6 + 1);
                            var newclass = "dice_" + randomNum;
                            $("#dice_" + count).removeClass("dice_6").removeClass("dice_5").removeClass("dice_4").removeClass("dice_3").removeClass("dice_2").removeClass("dice_1").addClass(newclass);
                        },
                        onComplete: function() {
                            $("#dice_" + count).removeClass("dice_6").removeClass("dice_5").removeClass("dice_4").removeClass("dice_3").removeClass("dice_2").removeClass("dice_1").addClass("dice_" + value);
                            TweenMax.to($("#dicetotal"), .3, {
                                opacity: 1,
                                onComplete: function() {
                                    var cardsleft = _.filter(this._var().Cards, function(val) {
                                        return val.used == false;
                                    });
                                    if (this._var().isvalueinpool(sum, _.pluck(cardsleft, "cardnumber")) == false) {
                                        this._var().displayLOSE();
                                    } else {
                                        console.log("value should be possible");
                                    }
                                }
                            });
                        }
                    });
                });
            },
            displayLOSE: function() {
                var this = this.parent;
                TweenMax.to($("#grumpycat"), .5, {
                    top: 0,
                    onComplete: function() {
                        var namecalling = [ "Guy", "Pal", "Chief", "my Man", "Buddy", "Honey", "Honeypie", "Snookims", "Babycakes", "Boo", "Papi", "my Chiquita Banana", "Powder Puff", "Muffin", "Stud", "Princess", "Playa Hatah", "Tough Guy", "Mon Cheri", "Mamasita", "Captain Koolaid", "Sugarpuss" ];
                        var randomNum = Math.floor(Math.random() * namecalling.length);
                        $("#message").html("Sorry " + namecalling[randomNum] + ", you can't make " + this._var().lasttotal + " with the remaining cards!");
                        TweenMax.to($("#grumpymessage"), .6, {
                            scale: 1,
                            opacity: 1,
                            ease: Elastic.easeOut
                        });
                    }
                });
            },
            displayWIN: function() {
                var this = this.parent;
                TweenMax.to($("#happycat"), .5, {
                    "margin-left": 0,
                    ease: Bounce.easeOut,
                    onComplete: function() {
                        TweenMax.to($("#happymessage"), .6, {
                            scale: 1,
                            opacity: 1,
                            ease: Elastic.easeOut
                        });
                    }
                });
            },
            catreset: function() {
                TweenMax.to($("#happycat"), 2, {
                    "margin-left": 800
                });
                TweenMax.to($("#happymessage"), .3, {
                    scale: .01,
                    opacity: 0
                });
                TweenMax.to($("#grumpycat"), 2, {
                    top: 390
                });
                TweenMax.to($("#grumpymessage"), .3, {
                    scale: .01,
                    opacity: 0
                });
            },
            displaycards: function() {
                var this = this.parent;
                $(".card").empty();
                $("div>.card").remove();
                $.each(this._var().Cards, function(count, value) {
                    this._var().cardtarget.append('<div id="card_' + count + '" class="card"><div class="face card_' + (value.cardnumber - 1) + '"></div><div class="back"></div></div>');
                    var randomNum = Math.floor(Math.random() * 360 + 1);
                    var randomNumb = Math.floor(Math.random() * 400 + 1) - 200;
                    TweenMax.from($("#card_" + count), .5, {
                        rotation: randomNum,
                        x: randomNumb,
                        y: 700,
                        delay: count * .1
                    });
                    $("#card_" + count).bind("click", function() {
                        var thisCard = this._var().Cards[count];
                        if (!thisCard.used) {
                            thisCard.selected = !thisCard.selected;
                            if (thisCard.selected) {
                                $(this).addClass("selected");
                            } else {
                                $(this).removeClass("selected");
                            }
                            this._var().checkselected();
                        } else {
                            console.log("Meh, you used this already");
                        }
                    });
                });
            },
            wincheck: function() {
                var this = this.parent;
                var youWON = false;
                var usedCards = _.filter(this._var().Cards, function(val) {
                    return val.used == true;
                });
                if (usedCards.length == this._var().Cards.length) {
                    youWON = true;
                }
                return youWON;
            },
            updatescore: function() {
                var this = this.parent;
                this._var().currentturn++;
                $("#score").empty().html(this._var().currentturn);
            },
            checkselected: function() {
                var this = this.parent;
                var chosenCards = _.filter(this._var().Cards, function(val) {
                    return val.selected == true;
                });
                var sum = _.reduce(chosenCards, function(memo, val) {
                    return memo + val.cardnumber;
                }, 0);
                console.log(sum + " & " + this._var().lasttotal);
                if (sum == this._var().lasttotal) {
                    console.log("YAY@");
                    this._var().updatescore();
                    this._var().markselectedasused();
                    if (this._var().wincheck()) {
                        this._var().displayWIN();
                    } else {
                        this._var().displaydice();
                    }
                } else {
                    if (sum > this._var().lasttotal) {
                        console.log("you went over!");
                        this._var().resetunused();
                    }
                }
            },
            markselectedasused: function() {
                var this = this.parent;
                $.each(this._var().Cards, function(count, value) {
                    if (value.selected) {
                        value.used = true;
                        value.selected = false;
                        $("#card_" + count).removeClass("selected").addClass("used");
                    }
                });
            },
            resetunused: function() {
                var this = this.parent;
                $.each(this._var().Cards, function(count, value) {
                    if (!value.used && value.selected) {
                        value.selected = false;
                        $("#card_" + count).removeClass("selected");
                    }
                });
            },
            busy: true
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {};
    _thisObject_.prototype.init = function() {
        var self = this;
        this.notify("Trace", "init");
        this._startlisteners();
        $("#tryagain").bind("click", function() {
            this.refresh();
            this._var().displaydice();
        });
        $("#expertmode").bind("click", function() {
            this._var().tryrandomcards = !this._var().tryrandomcards;
            if (this._var().tryrandomcards) {
                this._var().numberofdice = 3;
                $("#expertmode").addClass("expertON");
            } else {
                this._var().numberofdice = 2;
                $("#expertmode").removeClass("expertON");
            }
            this.refresh();
            this._var().displaydice();
        });
        $("#sitehotspot").bind("click", function() {
            window.open("http://www.jonathanrobles.net");
        });
        $("#Begin").bind("click", function() {
            TweenMax.to($("#expertmode"), .3, {
                "margin-left": 0
            });
            TweenMax.to($("#welcome"), .5, {
                opacity: 0,
                top: 20,
                onComplete: function() {
                    $("#welcome").remove();
                    $("#welcomeblock").remove();
                    this._var().displaydice();
                    this._var().updatescore();
                }
            });
        });
        this.refresh();
    };
    _thisObject_.prototype.refresh = function() {
        var self = this;
        this.notify("Trace", "refresh");
        this._var().catreset();
        this._var().shufflecards(10);
        this._var().displaycards();
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype.sayhi = function() {
        this.notify("Trace", "hi there, from object#" + this._id() + " [ " + this._var().type + " by " + this._var().author + " ] ");
    };
    return _thisObject_;
});
