/*!Last Updated: 03.07.2014[21.54.13] by Jonathan Robles*/
/*!*/
/*!********************************************************************************************************/
/*!                                                                                                       */
/*! flexibleregulatory module (c) 2014 Jonathan Robles */
/*! http://jonathanrobles.net                                                                             */
/*!                                                                                                       */
/*! This software may be freely distributed under the                                                     */
/*! Creative Commons Attribution-ShareAlike (CC BY-SA) License                                            */
/*! This license lets others remix, tweak, and build upon your work even for commercial purposes,         */
/*! as long as they credit you and license their new creations under the identical terms. All new         */
/*! works based on yours will carry the same license, so any derivatives will also allow commercial use.  */
/*! link: https://creativecommons.org/examples#by-sa                                                      */
/*!                                                                                                       */
/*!********************************************************************************************************/
/*!CLX*/
/*!*/
/*!*/
/*!Last Updated: 03.07.2014[21.54.13] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule", "knockout", "modules/controllers/slideset", "tweenmax" ], function($, parentModel, ko, Slideset, TweenMax) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "flexibleregulatory",
            author: "Jonathan Robles",
            lasteditby: "",
            info_Slideset: undefined,
            slide_Slideset: undefined,
            viewmodel: undefined,
            app_target: $("#app-main"),
            infobox_target: $("#infobox"),
            slidebox_target: $("#slidebox"),
            menuanim: {
                anim_target: $("#app-main>.bot-right"),
                anim_time: .5
            },
            shareanim: {
                target: $("#app-main>.share"),
                anim_target: $("#app-main>.share>div"),
                anim_time: .5
            },
            callback: function() {
                this.app_target.css("opacity", 1);
            },
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var self = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onWindowWidth: function(o) {
                    if (self._var().viewmodel.menuopen()) {
                        self._var().viewmodel.menu_toggle();
                    }
                }
            };
        }());
    };
    _thizOBJ_.prototype.viewmodel = function(Slideset, Infoset, Menuanim, Shareanim) {
        var self = this;
        self.menuopen = ko.observable(false);
        self.shareopen = ko.observable(false);
        self.slide_currentIndex = ko.observable(Slideset._var().currentslide);
        self.slide_totalCount = ko.observable(Slideset._var().totalslides);
        self.info_currentIndex = ko.observable(Infoset._var().currentslide);
        self.info_totalCount = ko.observable(Infoset._var().totalslides);
        self.controlmenu = ko.computed(function() {
            if (self.menuopen()) {
                var destinationX = Menuanim.anim_target.innerWidth() - 70;
                TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                    x: destinationX
                });
            } else {
                TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                    x: 0
                });
            }
        });
        self.controlshare = ko.computed(function() {
            if (self.shareopen()) {
                Shareanim.target.show();
                TweenMax.to(Shareanim.anim_target, .25, {
                    opacity: 1,
                    y: 0
                });
            } else {
                TweenMax.to(Shareanim.anim_target, 0, {
                    opacity: 0,
                    y: 25
                });
                Shareanim.target.hide();
            }
        });
        self.disable_next = ko.computed(function() {
            return self.slide_currentIndex() == self.slide_totalCount() - 1 ? true : false;
        });
        self.disable_prev = ko.computed(function() {
            return self.slide_currentIndex() == 0 ? true : false;
        });
        self.databindtoobject = function(getEvent) {
            try {
                var string = $(getEvent.target).attr("data-bind");
                var properties = string.split(",");
            } catch (err) {
                var string = $(getEvent.srcElement).attr("data-bind");
                var properties = string.split(",");
            }
            var obj = {};
            $.each(properties, function(i, v) {
                var tup = v.split(":");
                obj[tup[0]] = tup[1];
            });
            return obj;
        };
        self.info_button = function(data, event) {
            var switchindex = Number(self.databindtoobject(event).target);
            Infoset.jump(switchindex);
        };
        self.slide_button = function(data, event) {
            var switchindex = Number(self.databindtoobject(event).target);
            Slideset.pagejump(switchindex);
            if (self.menuopen()) {
                self.menu_toggle();
            }
        };
        self.slide_next = function() {
            Slideset.next();
        };
        self.slide_prev = function() {
            Slideset.prev();
        };
        self.menu_toggle = function() {
            self.menuopen(!self.menuopen());
        };
        self.share_toggle = function() {
            self.shareopen(!self.shareopen());
        };
        Slideset._var().childtrack = function(index) {
            self.slide_currentIndex(index);
        };
        Infoset._var().childtrack = function(index) {
            self.info_currentIndex(index);
        };
    };
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        var self = this;
        self.getdependancies();
        self.buildViewmodel();
        self._var().callback();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        var self = this;
        alert("this module has no refresh function");
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.getdependancies = function() {
        var self = this;
        self._var().info_Slideset = new Slideset({
            target: self._var().infobox_target,
            child_focus: function(o) {
                var argz = o;
                var tempObject = $(this.childObjects[argz.index]);
                tempObject.show();
            },
            child_unfocus: function(o) {
                var argz = o;
                var tempObject = $(this.childObjects[argz.unfocusedindex]);
                tempObject.hide();
            }
        });
        self._var().slide_Slideset = new Slideset({
            target: self._var().slidebox_target,
            autoZ: true,
            child_focus: function(o) {
                var argz = {
                    index: undefined,
                    speed: .5,
                    tweenvars: {
                        x: 0,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    }
                };
                argz = $.extend(argz, o);
                var tempObject = $(this.childObjects[argz.index]);
                if (Modernizr.csstransitions) {
                    TweenMax.to(tempObject, argz.speed, argz.tweenvars);
                } else {
                    tempObject.show();
                }
            },
            child_unfocus: function(o) {
                var argz = {
                    index: undefined,
                    unfocusedindex: undefined,
                    speed: 1,
                    offset: 0,
                    tweenvarsIN: {
                        x: -400,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    },
                    tweenvarsOUT: {
                        x: 400,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    }
                };
                argz = $.extend(argz, o);
                var tempObject = $(this.childObjects[argz.unfocusedindex]);
                if (Modernizr.csstransitions) {
                    if (argz.unfocusedindex < argz.index) {
                        TweenMax.to(tempObject, argz.speed, argz.tweenvarsIN);
                    } else {
                        TweenMax.to(tempObject, argz.speed, argz.tweenvarsOUT);
                    }
                } else {
                    tempObject.hide();
                }
            }
        });
        self._var().info_Slideset.init();
        self._var().slide_Slideset.init();
    };
    _thizOBJ_.prototype.buildViewmodel = function() {
        var self = this;
        self._var().viewmodel = new this.viewmodel(self._var().slide_Slideset, self._var().info_Slideset, self._var().menuanim, self._var().shareanim);
        ko.bindingHandlers.isolatedOptions = {
            init: function(element, valueAccessor) {
                var args = arguments;
                ko.computed({
                    read: function() {
                        ko.utils.unwrapObservable(valueAccessor());
                        ko.bindingHandlers.options.update.apply(this, args);
                    },
                    owner: this,
                    disposeWhenNodeIsRemoved: element
                });
            }
        };
        ko.bindingHandlers.CSSonMatch = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var objEkt = $(element);
                var databinds = allBindings();
                var comparevalue = ko.unwrap(valueAccessor());
                if (databinds.CSSclass != undefined) {
                    if (databinds.target != undefined) {
                        if (comparevalue == databinds.target) {
                            objEkt.addClass(databinds.CSSclass);
                        } else {
                            objEkt.removeClass(databinds.CSSclass);
                        }
                    } else {
                        alert("no target is defined with Jonathan's awesome CSSonMatch BINDING!");
                    }
                } else {
                    alert("no CSSclass is defined with Jonathan's awesome CSSonMatch BINDING!");
                }
            }
        };
        ko.applyBindings(self._var().viewmodel);
    };
    return _thizOBJ_;
});