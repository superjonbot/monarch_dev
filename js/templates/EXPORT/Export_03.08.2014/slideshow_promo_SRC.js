/*!Last Updated: 03.08.2014[02.02.13] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! slideshow_promo MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.08.2014[02.02.13] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule", "knockout", "modules/controllers/swipeset", "tweenmax", "modules/view/pinchzoom" ], function($, parentObject, ko, Swipeset, TweenMax, Pinch) {
    function _thisObject_(o) {
        var settings = {
            type: "slideshow_promo",
            author: "Jonathan Robles",
            lasteditby: "",
            slide_Swipeset: undefined,
            slidebox_target: $("#slidebox"),
            pinchZoomset: undefined,
            disableNavigation: true,
            knockout_viewmodel: undefined,
            mail_knockout_viewmodel: undefined,
            app_target: $("#app-main"),
            menuanim: {
                anim_target: $("#app-main>.presentation_layer"),
                anim_time: .5
            },
            shareanim: {
                target: $("#app-main>.share"),
                anim_target: $("#app-main>.share>div"),
                anim_time: .5
            },
            emailanim: {
                target: $("#emailblock"),
                anim_time: .5
            },
            toggle_hiddenclass: "hidethis",
            toggleposition_open: function(target) {},
            toggleposition_close: function(target) {},
            callback: function() {
                $("#app-main").removeClass("hideapp");
            },
            busy: false
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var self = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onWindowWidth: function(o) {
                    this.refresh();
                }
            };
        }());
    };
    _thisObject_.prototype.knockout_viewmodel = function(Swipeset, Menuanim, Shareanim, Zoomset, _var) {
        var self = this;
        this.navdisabled = ko.observable(_var.disableNavigation);
        this.showthumbs = ko.observable(false);
        this.Zoomset = Zoomset;
        this.menuopen = ko.observable(false);
        this.shareopen = ko.observable(false);
        this.slide_currentIndex = ko.observable(Swipeset._var().currentslide);
        this.slide_currentIndex_display = ko.computed(function() {
            return this.slide_currentIndex() + 1;
        });
        this.slide_totalCount = ko.observable(Swipeset._var().totalslides);
        this.controlmenu = ko.computed(function() {
            if (this.menuopen()) {
                var destinationX = Menuanim.anim_target.innerWidth() - 70;
                TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                    x: destinationX
                });
            } else {
                TweenMax.to(Menuanim.anim_target, Menuanim.anim_time, {
                    x: 0
                });
            }
        });
        this.controlshare = ko.computed(function() {
            if (this.shareopen()) {
                Shareanim.target.removeClass(_var.toggle_hiddenclass);
            } else {
                Shareanim.target.addClass(_var.toggle_hiddenclass);
            }
        });
        this.controlthumbs = ko.computed(function() {
            if (this.showthumbs()) {
                $("#thumbOverlay").removeClass(_var.toggle_hiddenclass);
            } else {
                $("#thumbOverlay").addClass(_var.toggle_hiddenclass);
            }
        });
        this.disable_next = ko.computed(function() {
            return this.slide_currentIndex() == this.slide_totalCount() - 1 || this.navdisabled() ? true : false;
        });
        this.disable_prev = ko.computed(function() {
            return this.slide_currentIndex() == 0 || this.navdisabled() ? true : false;
        });
        this.disable_navigation = ko.computed(function() {
            if (this.navdisabled()) {
                window._global$.setCookie("roadblock", "true");
                Swipeset._var().enable_touchbinds = false;
            } else {
                window._global$.setCookie("roadblock", "false");
                Swipeset._var().enable_touchbinds = true;
            }
        });
        this.databindtoobject = function(getEvent) {
            try {
                var string = $(getEvent.target).attr("data-bind");
                var properties = string.split(",");
            } catch (err) {
                var string = $(getEvent.srcElement).attr("data-bind");
                var properties = string.split(",");
            }
            var obj = {};
            $.each(properties, function(i, v) {
                var tup = v.split(":");
                obj[tup[0]] = tup[1];
            });
            return obj;
        };
        this.slide_button = function(data, event) {
            var switchcount = Number(this.databindtoobject(event).target);
            Swipeset.pagejump(switchcount);
            if (this.menuopen()) {
                this.menu_toggle();
            }
            if (this.showthumbs()) {
                this.thumbs_toggle();
            }
        };
        this.slide_next = function() {
            if (!this.navdisabled()) {
                Swipeset.next();
            }
        };
        this.slide_prev = function() {
            if (!this.navdisabled()) {
                Swipeset.prev();
            }
        };
        this.menu_toggle = function() {
            if (!this.navdisabled()) {
                this.menuopen(!this.menuopen());
            }
        };
        this.share_toggle = function() {
            this.shareopen(!this.shareopen());
        };
        this.thumbs_toggle = function() {
            this.showthumbs(!this.showthumbs());
        };
        this.nav_toggle = function() {
            this.navdisabled(!this.navdisabled());
            if (this.navdisabled() != _var.disableNavigation) {
                _var.disableNavigation = this.navdisabled();
            }
        };
        this.email_isopen = ko.observable(false);
        this.email_toggle = function() {
            console.log("EMAIL TOGGLE!");
            this.email_isopen(!this.email_isopen());
        };
        this.email_validator = function(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };
        this.email_closepopup = function() {
            console.log("email close");
            _var.emailanim.target.addClass(_var.toggle_hiddenclass);
        };
        this.email_openpopup = function() {
            console.log("email open");
            _var.emailanim.target.removeClass(_var.toggle_hiddenclass);
        };
        this.controlemail = ko.computed(function() {
            if (this.email_isopen()) {
                this.email_openpopup();
            } else {
                this.email_closepopup();
            }
        });
        this.email_yourname = ko.observable("");
        this.email_youremail = ko.observable("");
        this.email_fromemailnotvalid = ko.observable(false);
        this.email_toemailnotvalid = ko.observable(false);
        this.email_toemail = ko.observable("");
        this.email_subject = ko.observable("");
        this.email_text = ko.observable("");
        this.email_textbuild = function() {
            return "Your friend " + this.email_yourname() + ", sent you this: " + this.email_text();
        };
        this.email_submit = function() {
            this.email_fromemailnotvalid(!this.email_validator(this.email_youremail()));
            this.email_toemailnotvalid(!this.email_validator(this.email_toemail()));
            if (this.email_fromemailnotvalid() && this.email_toemailnotvalid()) {
                this.email_send();
            } else {
                var message = "";
                message += "this.email_fromemailnotvalid()" + this.email_fromemailnotvalid() + "/n";
                message += "this.email_toemailnotvalid()" + this.email_toemailnotvalid();
                alert(message);
            }
        };
        this.email_send = function() {
            var emailinject = {
                fromPage: ".",
                template: "",
                encryptEmail: false,
                confirmationPage: "/features/feedback/noscan/cf/fb-load",
                subject: this.email_subject(),
                message: this.email_textbuild(),
                toEmail: this.email_toemail(),
                fromEmail: this.email_youremail()
            };
            var buildpostURL = "/postemail?";
            for (var name in emailinject) {
                var value = encodeURIComponent(emailinject[name]);
                buildpostURL += name + "=" + value + "&";
            }
            $.post(buildpostURL, function() {
                this.email_closepopup();
            });
        };
    };
    _thisObject_.prototype.mail_knockout_viewmodel = function(_var) {
        var self = this;
    };
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        var self = this;
        this.getdependancies();
        this.buildViewmodel();
        this._var().callback();
    };
    _thisObject_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        var self = this;
        if (this._var().knockout_viewmodel.menuopen()) {
            this._var().knockout_viewmodel.menu_toggle();
        }
        this._var().slide_Swipeset.refresh();
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype.getdependancies = function() {
        var self = this;
        Swipeset.prototype.tackmatrix = function() {
            $.each(this._var().childVars, function(count, value) {
                if (value.target.css("transform") == "none") {
                    value.target.css("webkitTransform", "skew(0)");
                }
                value.matrix = value.target.css("transform");
                value.matrix = value.matrix.match(/\(([^)]+)\)/)[1].split(",");
                value.z = Number(value.matrix[0]);
                value.x = Number(value.matrix[4]);
                value.y = Number(value.matrix[5]);
            });
        };
        this._var().slide_Swipeset = new Swipeset({
            usetouchbinds: window._global$.isTouch,
            target: this._var().slidebox_target,
            callback: function() {
                this.parent.tackmatrix();
            },
            onscroll_debounce_callback: function(o) {
                this._var().knockout_viewmodel.slide_currentIndex(this.currentslide);
                window.location.hash = "page=" + (this.currentslide + 1);
                window._global$.setCookie("page", this.currentslide + 1);
            },
            onscroll_debounce_rate: 200
        });
        var cookie_getpage = window._global$.getCookie("page");
        var cookie_isroadblocked = window._global$.getCookie("roadblock");
        if (cookie_getpage != null) {
            var jumppage = Number(cookie_getpage);
            jumppage -= 1;
            this._var().slide_Swipeset._var({
                currentslide: jumppage
            });
        }
        var checkhash = window._global$.getHash("page");
        var jumppage = Number(checkhash.substring(6, checkhash.length));
        if (cookie_isroadblocked == "true") {
            jumppage = 1;
            this._var().disableNavigation = true;
        } else {
            this._var().disableNavigation = false;
        }
        if (jumppage > 0) {
            jumppage -= 1;
            this._var().slide_Swipeset._var({
                currentslide: jumppage
            });
        }
        this._var().slide_Swipeset.init();
        this._var().pinchZoomset = new Pinch({
            target: $("#popFrame"),
            bindsonchild: true,
            childcount: 1
        });
    };
    _thisObject_.prototype.buildViewmodel = function() {
        var self = this;
        var parent = this;
        this._var().knockout_viewmodel = new this.knockout_viewmodel(this._var().slide_Swipeset, this._var().menuanim, this._var().shareanim, this._var().pinchZoomset, this._var());
        ko.bindingHandlers.isolatedOptions = {
            init: function(element, valueAccessor) {
                var args = arguments;
                ko.computed({
                    read: function() {
                        ko.utils.unwrapObservable(valueAccessor());
                        ko.bindingHandlers.options.update.apply(this, args);
                    },
                    owner: this,
                    disposeWhenNodeIsRemoved: element
                });
            }
        };
        ko.bindingHandlers.inverseChecked = {
            init: function(element, valueAccessor, allBindingsAccessor) {
                var value = valueAccessor();
                var interceptor = ko.computed({
                    read: function() {
                        return !value();
                    },
                    write: function(newValue) {
                        value(!newValue);
                    },
                    disposeWhenNodeIsRemoved: element
                });
                var newValueAccessor = function() {
                    return interceptor;
                };
                ko.utils.domData.set(element, "newValueAccessor", newValueAccessor);
                ko.bindingHandlers.checked.init(element, newValueAccessor, allBindingsAccessor);
            },
            update: function(element, valueAccessor) {
                ko.bindingHandlers.checked.update(element, ko.utils.domData.get(element, "newValueAccessor"));
            }
        };
        ko.bindingHandlers.gethtml = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var comparevalue = ko.unwrap(valueAccessor());
                objEkt.html($(comparevalue).html());
            }
        };
        ko.bindingHandlers.contenttopop = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var targetcontent = ko.unwrap(valueAccessor());
                $(element).click(function() {
                    var Content = objEkt.html();
                    $(targetcontent).removeClass(parent._var().toggle_hiddenclass);
                    var PopupContentArea = $(targetcontent).find(".popper_Content");
                    var CloseButton = $(targetcontent).find(".popper_Close");
                    PopupContentArea.empty();
                    PopupContentArea.html(Content);
                    viewModel.Zoomset.init();
                    viewModel.Zoomset.resetposition();
                    CloseButton.unbind("click").bind("click", function() {
                        $(targetcontent).addClass("hidethis");
                    });
                });
            }
        };
        ko.bindingHandlers.toggletarget = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var objEkt = $(element);
                var databinds = allBindings();
                var target = $(ko.unwrap(valueAccessor()));
                $(element).click(function() {
                    if ($(target).attr("class").countOf(databinds.className) != -1) {
                        target.removeClass(databinds.className);
                    } else {
                        target.addClass(databinds.className);
                    }
                    this.refresh();
                });
            }
        };
        ko.bindingHandlers.sharebind = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var xsvalue = ko.unwrap(valueAccessor());
                var target = $(xsvalue);
                $.data(target, "dothisthat", {});
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self = this;
                var objEkt = $(element);
                var databinds = allBindings();
                var xsvalue = ko.unwrap(valueAccessor());
                var target = $(xsvalue);
                var trypostCp = function(data) {
                    try {
                        postCp(data);
                    } catch (exception) {
                        console.log('ERROR, no prerequisite function: "postCp"');
                    }
                };
                var trywmdPageLink = function(data) {
                    try {
                        wmdPageLink(data);
                    } catch (exception) {
                        console.log('ERROR, no prerequisite function: "wmdPageLink"');
                    }
                };
                function segVarPop(obj) {
                    if ($("meta[name=metasegvar]").length > 0) {
                        if (segVarParam("artid") != "0") {
                            obj.activityId = segVarParam("artid");
                        }
                        if (segVarParam("ssp") != "0") {
                            obj.leadSpec = segVarParam("ssp");
                        }
                        if (segVarParam("scg") != "0") {
                            obj.leadConcept = segVarParam("scg");
                        }
                        if (segVarParam("cg") != "0") {
                            obj.contentGroup = segVarParam("cg");
                        }
                        if (segVarParam("bc") != "__") {
                            var bcTemp = segVarParam("bc");
                            bcTemp = bcTemp.substring(1, bcTemp.length - 1);
                            bcTemp = bcTemp.split("_");
                            obj.blockCode = bcTemp;
                        }
                    }
                }
                var socialCp = function(platform) {
                    var cpSocData = new Object();
                    cpSocData.appname = "social-functions";
                    cpSocData.activityName = encodeURIComponent(platform);
                    segVarPop(cpSocData);
                    trypostCp(cpSocData);
                };
                var sharobjs = {
                    facebook: {
                        runtime: function() {
                            trywmdPageLink("ar-share_face");
                            var fbtitle = document.title;
                            var fblink = window.location;
                            fblink = fblink + "?src=stfb";
                            var fbsharelink = "http://www.facebook.com/share.php?u=" + fblink + "&t=" + fbtitle;
                            socialCp("Facebook");
                            window.open(fbsharelink);
                        }
                    },
                    twitter: {
                        runtime: function() {
                            trywmdPageLink("ar-share_twit");
                            var twtitle = document.title;
                            var twiturl = window.location;
                            twiturl = twiturl + "?src=sttwit";
                            var ftwiturl = "http://twitter.com/share?url=" + encodeURIComponent(twiturl) + "&text=" + encodeURIComponent(twtitle);
                            socialCp("Twitter");
                            window.open(ftwiturl);
                        }
                    },
                    google: {
                        runtime: function() {
                            trywmdPageLink("ar-share_google");
                            var ggtitle = document.title;
                            var googleurl = window.location;
                            googleurl = googleurl + "?src=stgoogle";
                            var ftgoogleurl = "https://plusone.google.com/_/+1/confirm?url=" + encodeURIComponent(googleurl) + "&title=" + encodeURIComponent(ggtitle);
                            socialCp("Google+");
                            window.open(ftgoogleurl);
                        }
                    },
                    linkedin: {
                        runtime: function() {
                            var title = document.title;
                            var url = window.location;
                            url += "?src=stlinkedin";
                            var fturl = "http://www.linkedin.com/cws/share?url=" + url + "&title=" + title;
                            socialCp("LinkedIn");
                            window.open(fturl);
                        }
                    },
                    jonathanisawesome: {
                        runtime: function() {
                            alert("yes he is!");
                        }
                    }
                };
                $(element).click(function() {
                    if (typeof sharobjs[xsvalue] != "object") {
                        alert('Que Pasa? where did you get the sharebind called "' + xsvalue + '"??? Please check your databindings for this button');
                    } else {
                        sharobjs[xsvalue].runtime();
                    }
                });
            }
        };
        ko.applyBindings(this._var().knockout_viewmodel);
    };
    return _thisObject_;
});
