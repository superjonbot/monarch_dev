/*!Last Updated: 03.06.2014[22.10.37] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! shutthebox MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CL1b*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.06.2014[22.10.37] by Jonathan Robles*/
define([ "jquery", "underscore", "modules/definitions/standardmodule", "tweenmax" ], function($, _, parentModel, TweenMax) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "shutthebox",
            author: "Jonathan Robles",
            lasteditby: "",
            cardtarget: $("#cards"),
            dicetarget: $("#dice"),
            currentturn: 0,
            tryrandomcards: false,
            numberofdice: 2,
            lasttotal: undefined,
            Cards: [],
            makenewCard: function(index, value) {
                var self = {
                    index: index,
                    cardnumber: value,
                    selected: false,
                    used: false
                };
                return self;
            },
            rolldice: function(options) {
                var self = {
                    dicevalues: [],
                    total: 0
                };
                for (tempvar = 0; tempvar < options.dicecount; tempvar++) {
                    var randomNum = Math.floor(Math.random() * options.maxnumber + options.minnumber);
                    self.dicevalues.push(randomNum);
                    self.total += randomNum;
                }
                return self;
            },
            isvalueinpool: function(value, pool) {
                var isthere = false;
                pool.sort(function(a, b) {
                    return a - b;
                }).reverse();
                console.log(pool);
                console.log(pool.indexOf(value));
                if (pool.indexOf(value) != -1) {
                    isthere = true;
                } else {
                    var thesums = [];
                    for (a = 0; a < pool.length; a++) {
                        console.log("checking: " + pool[0] + " > " + value);
                        if (pool[0] > value) {
                            pool.shift();
                        }
                    }
                    pool.sort(function(a, b) {
                        return a - b;
                    });
                    $.each(pool, function(index, value) {
                        console.log(value);
                        thesums.push(value);
                        var temppool = pool.slice(0);
                        temppool.splice(index, 1);
                        $.each(temppool, function(t_index, t_value) {
                            console.log(value + " " + t_value);
                            thesums.push(value + t_value);
                            var temppoolb = temppool.slice(0);
                            temppoolb.splice(t_index, 1);
                            $.each(temppoolb, function(u_index, u_value) {
                                console.log(value + " " + t_value + " " + u_value);
                                thesums.push(value + t_value + u_value);
                            });
                        });
                    });
                    if (thesums.indexOf(value) != -1) {
                        isthere = true;
                    }
                }
                return isthere;
            },
            shufflecards: function(count) {
                var self = this.parent;
                var makenewCard_local = self._var().makenewCard;
                self._var().Cards = [];
                for (tempcnt = 0; tempcnt < count; tempcnt++) {
                    if (self._var().tryrandomcards) {
                        var randomNum = Math.floor(Math.random() * 13 + 1);
                        self._var().Cards.push(new makenewCard_local(tempcnt, randomNum));
                    } else {
                        self._var().Cards.push(new makenewCard_local(tempcnt, tempcnt + 1));
                    }
                }
            },
            displaydice: function() {
                var self = this.parent;
                var dice = self._var().rolldice({
                    dicecount: self._var().numberofdice,
                    minnumber: 1,
                    maxnumber: 6
                });
                self._var().dicetarget.empty();
                $.each(dice.dicevalues, function(index, value) {
                    self._var().dicetarget.append('<div id="dice_' + index + '" class="dice dice_' + value + '">' + value + "</div>");
                });
                var sum = _.reduce(dice.dicevalues, function(memo, num) {
                    return memo + num;
                }, 0);
                self._var().lasttotal = sum;
                self._var().dicetarget.append('<div id="dicetotal" class="dicetotal"> = ' + sum + "</div>");
                TweenMax.to($("#dicetotal"), 0, {
                    opacity: 0
                });
                $.each(dice.dicevalues, function(index, value) {
                    TweenMax.from($("#dice_" + index), 1, {
                        ease: Bounce.easeOut,
                        y: 200,
                        rotation: 1e3,
                        scale: 5,
                        opacity: 0,
                        delay: index * .2,
                        onUpdate: function() {
                            var randomNum = Math.floor(Math.random() * 6 + 1);
                            var newclass = "dice_" + randomNum;
                            $("#dice_" + index).removeClass("dice_6").removeClass("dice_5").removeClass("dice_4").removeClass("dice_3").removeClass("dice_2").removeClass("dice_1").addClass(newclass);
                        },
                        onComplete: function() {
                            $("#dice_" + index).removeClass("dice_6").removeClass("dice_5").removeClass("dice_4").removeClass("dice_3").removeClass("dice_2").removeClass("dice_1").addClass("dice_" + value);
                            TweenMax.to($("#dicetotal"), .3, {
                                opacity: 1,
                                onComplete: function() {
                                    var cardsleft = _.filter(self._var().Cards, function(val) {
                                        return val.used == false;
                                    });
                                    if (self._var().isvalueinpool(sum, _.pluck(cardsleft, "cardnumber")) == false) {
                                        self._var().displayLOSE();
                                    } else {
                                        console.log("value should be possible");
                                    }
                                }
                            });
                        }
                    });
                });
            },
            displayLOSE: function() {
                var self = this.parent;
                TweenMax.to($("#grumpycat"), .5, {
                    top: 0,
                    onComplete: function() {
                        var namecalling = [ "Guy", "Pal", "Chief", "my Man", "Buddy", "Honey", "Honeypie", "Snookims", "Babycakes", "Boo", "Papi", "my Chiquita Banana", "Powder Puff", "Muffin", "Stud", "Princess", "Playa Hatah", "Tough Guy", "Mon Cheri", "Mamasita", "Captain Koolaid", "Sugarpuss" ];
                        var randomNum = Math.floor(Math.random() * namecalling.length);
                        $("#message").html("Sorry " + namecalling[randomNum] + ", you can't make " + self._var().lasttotal + " with the remaining cards!");
                        TweenMax.to($("#grumpymessage"), .6, {
                            scale: 1,
                            opacity: 1,
                            ease: Elastic.easeOut
                        });
                    }
                });
            },
            displayWIN: function() {
                var self = this.parent;
                TweenMax.to($("#happycat"), .5, {
                    "margin-left": 0,
                    ease: Bounce.easeOut,
                    onComplete: function() {
                        TweenMax.to($("#happymessage"), .6, {
                            scale: 1,
                            opacity: 1,
                            ease: Elastic.easeOut
                        });
                    }
                });
            },
            catreset: function() {
                TweenMax.to($("#happycat"), 2, {
                    "margin-left": 800
                });
                TweenMax.to($("#happymessage"), .3, {
                    scale: .01,
                    opacity: 0
                });
                TweenMax.to($("#grumpycat"), 2, {
                    top: 390
                });
                TweenMax.to($("#grumpymessage"), .3, {
                    scale: .01,
                    opacity: 0
                });
            },
            displaycards: function() {
                var self = this.parent;
                $(".card").empty();
                $("div>.card").remove();
                $.each(self._var().Cards, function(index, value) {
                    self._var().cardtarget.append('<div id="card_' + index + '" class="card"><div class="face card_' + (value.cardnumber - 1) + '"></div><div class="back"></div></div>');
                    var randomNum = Math.floor(Math.random() * 360 + 1);
                    var randomNumb = Math.floor(Math.random() * 400 + 1) - 200;
                    TweenMax.from($("#card_" + index), .5, {
                        rotation: randomNum,
                        x: randomNumb,
                        y: 700,
                        delay: index * .1
                    });
                    $("#card_" + index).bind("click", function() {
                        var thisCard = self._var().Cards[index];
                        if (!thisCard.used) {
                            thisCard.selected = !thisCard.selected;
                            if (thisCard.selected) {
                                $(this).addClass("selected");
                            } else {
                                $(this).removeClass("selected");
                            }
                            self._var().checkselected();
                        } else {
                            console.log("Meh, you used this already");
                        }
                    });
                });
            },
            wincheck: function() {
                var self = this.parent;
                var youWON = false;
                var usedCards = _.filter(self._var().Cards, function(val) {
                    return val.used == true;
                });
                if (usedCards.length == self._var().Cards.length) {
                    youWON = true;
                }
                return youWON;
            },
            updatescore: function() {
                var self = this.parent;
                self._var().currentturn++;
                $("#score").empty().html(self._var().currentturn);
            },
            checkselected: function() {
                var self = this.parent;
                var chosenCards = _.filter(self._var().Cards, function(val) {
                    return val.selected == true;
                });
                var sum = _.reduce(chosenCards, function(memo, val) {
                    return memo + val.cardnumber;
                }, 0);
                console.log(sum + " & " + self._var().lasttotal);
                if (sum == self._var().lasttotal) {
                    console.log("YAY@");
                    self._var().updatescore();
                    self._var().markselectedasused();
                    if (self._var().wincheck()) {
                        self._var().displayWIN();
                    } else {
                        self._var().displaydice();
                    }
                } else {
                    if (sum > self._var().lasttotal) {
                        console.log("you went over!");
                        self._var().resetunused();
                    }
                }
            },
            markselectedasused: function() {
                var self = this.parent;
                $.each(self._var().Cards, function(index, value) {
                    if (value.selected) {
                        value.used = true;
                        value.selected = false;
                        $("#card_" + index).removeClass("selected").addClass("used");
                    }
                });
            },
            resetunused: function() {
                var self = this.parent;
                $.each(self._var().Cards, function(index, value) {
                    if (!value.used && value.selected) {
                        value.selected = false;
                        $("#card_" + index).removeClass("selected");
                    }
                });
            },
            busy: true
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {};
    _thizOBJ_.prototype.init = function() {
        var self = this;
        this.notify("Trace", "init");
        this._startlisteners();
        $("#tryagain").bind("click", function() {
            self.refresh();
            self._var().displaydice();
        });
        $("#expertmode").bind("click", function() {
            self._var().tryrandomcards = !self._var().tryrandomcards;
            if (self._var().tryrandomcards) {
                self._var().numberofdice = 3;
                $("#expertmode").addClass("expertON");
            } else {
                self._var().numberofdice = 2;
                $("#expertmode").removeClass("expertON");
            }
            self.refresh();
            self._var().displaydice();
        });
        $("#sitehotspot").bind("click", function() {
            window.open("http://www.jonathanrobles.net");
        });
        $("#Begin").bind("click", function() {
            TweenMax.to($("#expertmode"), .3, {
                "margin-left": 0
            });
            TweenMax.to($("#welcome"), .5, {
                opacity: 0,
                top: 20,
                onComplete: function() {
                    $("#welcome").remove();
                    $("#welcomeblock").remove();
                    self._var().displaydice();
                    self._var().updatescore();
                }
            });
        });
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        var self = this;
        self.notify("Trace", "refresh");
        self._var().catreset();
        self._var().shufflecards(10);
        self._var().displaycards();
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.sayhi = function() {
        this.notify("Trace", "hi there, from object#" + this._id() + " [ " + this._var().type + " by " + this._var().author + " ] ");
    };
    return _thizOBJ_;
});