/**
 * bubble Module
 *
 * Date: 6/23/14 : 3:31 PM
 *
 * NOTE: Bouncing Bubble Experiment
 *
 * USAGE:
 *
 */
define(['jquery', 'modules/definitions/standardmodule','tweenmax'], function ($, parentModel,TweenMax) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'bubble', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: 'JR',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)
            target:$('#app-main>ul>li'),
            objects:[],
            /*, TRY TO STICK TO THESE COMMON STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             file:'somedir/somefile.ext',
             usenocache:true,
             dataIN:{},
             data:undefined,
             callback:function(){},
             interval:undefined,

             */
            busy:false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {
        /*

         this.notify('Trace','_startlisteners');
         var parent=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {

         onJSONdata:function(o){
         if(o.senderID==myID){
         parent._var({data: o.data,busy:false});
         if(parent._var().callback!=undefined){
         parent._var().callback(o.data)
         }
         }
         }
         }
         }());

         */
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init = function () {
        var self=this;

        /*

         if(<object>!=exist){
         <object>.create();
         }else{
         <object>.show(); //just show it and start listeners
         };
         * */
        this.notify('Trace', 'init');
        this._startlisteners();//start this module's listeners
        this.refresh();

    };

    _thizOBJ_.prototype.refresh = function () {
        this.notify('Trace', 'refresh');
        this.giveOBJextraProps();

    };

    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    _thizOBJ_.prototype.giveOBJextraProps = function () {
        var self=this;
        var OBJArray=self._var().target;
        $.each(OBJArray,function(index,value){
            //alert($(value).html())
            var addProps={



                    obj_name:'pokeyking',
                    obj_selector: $(value),
                    obj_borderOBJ: $('body'),
                    obj_bounds: function(){
                        var bounds=[];
                        bounds[0]=this.obj_borderOBJ.width()-this.obj_size()[0];
                        bounds[1]=this.obj_borderOBJ.height()-this.obj_size()[1];
                        return bounds;
                    },   //returns X and Y boundries
                    obj_size:function(){
                        var size=[];
                        size[0]=$(value).width()+parseInt($(value).css('border-left'))+parseInt($(value).css('border-right'))+parseInt($(value).css('padding-left'))+parseInt($(value).css('padding-right'));
                        size[1]=$(value).height()+parseInt($(value).css('border-top'))+parseInt($(value).css('border-bottom'))+parseInt($(value).css('padding-top'))+parseInt($(value).css('padding-bottom'));
                        return size;
                    },//returns object size

                    obj_centerpointXYoffset: function(){
                        return [
                            this.obj_size()[0]/2,
                            this.obj_size()[1]/2]
                    }, //centerpoint offset


                    obj_current_XYposition:undefined, //holder for current XY position to determine velocity and angle  (used by Tweento Update)
                    obj_last_XYposition:[], //holder for last XY position to determine velocity and angle (used by Tweento Update)

                    obj_last_getSpeed:undefined,
                    obj_last_getAngle:undefined,

                    obj_getSpeed:function(){

                        var endX=this.obj_last_XYposition[4]||0;
                        var endY=this.obj_last_XYposition[5]||0;
                        var startX=this.obj_current_XYposition[4];
                        var startY=this.obj_current_XYposition[5];

                        var xs = 0;
                        var ys = 0;

                        xs = endX - startX;
                        xs = xs * xs;

                        ys = endY - startY;
                        ys = ys * ys;

                        this.obj_last_getSpeed=Math.sqrt( xs + ys );
                        return this.obj_last_getSpeed;

                    },

                    obj_getAngle:function(){

                        var endX=this.obj_last_XYposition[4]||0;
                        var endY=this.obj_last_XYposition[5]||0;
                        var startX=this.obj_current_XYposition[4];
                        var startY=this.obj_current_XYposition[5];

                        dy = endY - startY;
                        dx = endX - startX;
                        theta = Math.atan2(dy, dx);
                        theta *= 180/Math.PI // rads to degs

                        this.obj_last_getAngle=theta;
                        return this.obj_last_getAngle;
                    },

                    obj_setNewTimeLeft:function(){
                        var progress = this.obj_TweenContainer.progress();
                        var timeLeft = this.obj_time-(this.obj_time*progress);
                        this.obj_time=timeLeft;
                        return this.obj_TweenContainer.progress();
                    },

                    obj_XYposition: function (){
                        // actual TL position [0,1]
                        var position=[
                            parseInt(this.obj_selector.offset().left,10),
                            parseInt(this.obj_selector.offset().top,10)];
                        // center based actual TL position [2,3]
                        position[2]=position[0]+this.obj_centerpointXYoffset()[0];
                        position[3]=position[1]+this.obj_centerpointXYoffset()[1];
                        // borderOBJ TL position [4,5]
                        position[4]=position[0]-this.obj_borderOBJ.offset().left-parseInt(this.obj_borderOBJ.css('padding-left'), 10)-parseInt(this.obj_borderOBJ.css('border-left'), 10);
                        position[5]=position[1]-this.obj_borderOBJ.offset().top-parseInt(this.obj_borderOBJ.css('padding-top'), 10)-parseInt(this.obj_borderOBJ.css('border-top'), 10);
                        // center based borderOBJ TL position [4,5]
                        position[6]=position[4]+this.obj_centerpointXYoffset()[0];
                        position[7]=position[5]+this.obj_centerpointXYoffset()[1];
                        return position;
                    },
                    obj_placewithinbounds:function(){
                        var self=this;
                        var getOBJpos=this.obj_current_XYposition;
                        var bounds=this.obj_bounds();
                        var obj_X=getOBJpos[4];
                        var obj_Y=getOBJpos[5];

                        if(obj_X<0){obj_X=0}
                        if(obj_X>bounds[0]){obj_X=bounds[0]}
                        if(obj_Y<0){obj_Y=0}
                        if(obj_Y>bounds[1]){obj_Y=bounds[1]}
                        this.obj_XYdestination=[obj_X,obj_Y]
                        this.obj_TweenContainer.kill();
                        this.obj_TweenContainer=TweenMax.to(OBJArray[0].obj_selector,0,{x:self.obj_XYdestination[0],y:self.obj_XYdestination[1]});
                    }

                    ,

                    obj_bordercollision_check: function() {
                        var getOBJpos=this.obj_current_XYposition;
                        var bounds=this.obj_bounds();
                        var obj_X=getOBJpos[4];
                        var obj_Y=getOBJpos[5];
                       // console.log(this.obj_name+ ' is at '+obj_X+','+obj_Y);
                        if(obj_X>=0&&obj_X<=bounds[0]&&obj_Y>=0&&obj_Y<=bounds[1]){
                            console.log('no collision @'+obj_X+','+obj_Y)
                        }else{
                            this.obj_getAngle();
                            this.obj_getSpeed();
                            console.log('last angle:'+this.obj_last_getAngle+', last speed:'+this.obj_last_getSpeed);

                            this.obj_setNewTimeLeft();


                            this.obj_placewithinbounds();
                            //alert('tween dead')
                            this.obj_XYdestination=[0,0];




                            //this.obj_Tweento();
                        }


                    },







                    //destination:
                    obj_XYdestination: [0,800],

                    obj_time: 2,
                    obj_angle: undefined,

                    obj_TweenContainer:undefined,
                    obj_Tweento_update:function(){
                        this.obj_current_XYposition=this.obj_XYposition();

                      //  console.log(this.obj_name)
                        this.obj_bordercollision_check();


                        this.obj_last_XYposition=this.obj_current_XYposition;
                    },
                    obj_Tweento:function(){
                        var self=this;
                        self.obj_TweenContainer=TweenMax.to(self.obj_selector,self.obj_time,{x:self.obj_XYdestination[0],y:self.obj_XYdestination[1],onUpdate:function(){self.obj_Tweento_update.apply(self)}});
                    }


                }

           // addProps.obj_centerpointXYoffset=[addProps.obj_width/2,addProps.obj_height/2]

            OBJArray[index] = $.extend(value, addProps);


        })

        var borderOBJ=OBJArray[0].obj_borderOBJ;

   //     alert(  parseInt($('body').css('padding-top'), 10)  );


    //    alert(borderOBJ.offset().left+','+borderOBJ.offset().top+' ::: '+borderOBJ.width()+','+borderOBJ.height())


    //    alert('DONE! '+$(OBJArray[0]).html()+' '+String(OBJArray[0].obj_XYposition()))
        //OBJArray[0].obj_selector.hide();
    //    alert('Tweening to: '+OBJArray[0].obj_bounds()[0]+' x '+OBJArray[0].obj_bounds()[1])

        OBJArray[0].obj_Tweento();

       // TweenMax.to(OBJArray[1].obj_selector,1,{x:0,y:0});
       // TweenMax.to(OBJArray[2].obj_selector,1,{x:0,y:OBJArray[0].obj_bounds()[1]});
       // TweenMax.to(OBJArray[3].obj_selector,1,{x:OBJArray[0].obj_bounds()[0],y:0});
        //alert(typeof TweenMax)

    }




    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.sayhi = function () {
        this.notify('Trace', ('hi there, from object#' + this._id() + ' [ ' + this._var().type + ' by ' + this._var().author + ' ] '));
    }


    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


//knockout blurb
 /*
 define(['knockout','modules/view/viewmodels/application4vm'],
 function (ko,appViewModel) {
    ko.applyBindings(new appViewModel());
 });
 */
