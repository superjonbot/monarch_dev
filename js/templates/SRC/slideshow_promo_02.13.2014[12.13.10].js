/**
 * slideshow_promo Module
 *
 * Date: 1/15/14 : 12:58 PM
 *
 * NOTE: slideshow for promo
 *
 * USAGE:
 *
 */
define(['jquery', 'modules/definitions/standardmodule','knockout','modules/controllers/swipeset', 'tweenmax','modules/view/pinchzoom'], function ($, parentModel,ko,Swipeset,TweenMax,Pinch) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'slideshow_promo', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)

            /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
            //HOLDS MODULE DEPENDANCIES
            slide_Swipeset:undefined,
            slidebox_target:$('#slidebox'),

            pinchZoomset:undefined,
            disableNavigation:true,

           /* childVars:undefined,
            childVar_setter:function(index,value){
                var self = this;
                self.matrix= undefined;
                self.x= 0;
                self.y= 0;
                self.z= 0;
            },*/

            viewmodel: undefined,   //holds the viewmodel
            app_target:$('#app-main'),
            /*, TRY TO STICK TO THESE COMMON STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             file:'somedir/somefile.ext',
             usenocache:true,
             dataIN:{},
             data:undefined,

             interval:undefined,

             */

            menuanim:{
                anim_target:$('#app-main>.presentation_layer'),
                anim_time:.5
            },
            shareanim:{
                target:$('#app-main>.share'),
                anim_target:$('#app-main>.share>div'),
                anim_time:.5
            },


            toggleposition_open:function(target){
                //alert(window._global$.windowHeight)
                //TweenMax.to(target,.25,{opacity:1,y:180-window._global$.windowHeight});
            },
            toggleposition_close:function(target){
                //TweenMax.to(target,.25,{opacity:1,y:0});
            },


            callback:function(){},
            busy:false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {


        this.notify('Trace','_startlisteners');
        var self=this;
        var myID=  this._id();
        _notify.add(this._id(), function() {

            return {

                onWindowWidth: function (o) {
                    //if (o.senderID == myID) {
                    //alert('onit!')
                    self.refresh();
                    //if(self._var().viewmodel.menuopen()){self._var().viewmodel.menu_toggle()}

                    //self._var().slide_Swipeset.refresh();
                    // alert(o.data)
                    /* parent._var({data: o.data, busy: false});
                     if (parent._var().callback != undefined) {
                     parent._var().callback(o.data)
                     }*/
                    //}
                }
            }
        }());

        /*  */
    };

    _thizOBJ_.prototype.viewmodel = function (Swipeset,Menuanim,Shareanim,Zoomset,_var){
        var self=this;

        self.navdisabled=ko.observable(_var.disableNavigation);
        self.showthumbs=ko.observable(false);


        self.Zoomset=Zoomset;
        self.menuopen=ko.observable(false);
        self.shareopen=ko.observable(false);

        self.slide_currentIndex=ko.observable(Swipeset._var().currentslide);
        self.slide_currentIndex_display=ko.computed(function(){

            return self.slide_currentIndex()+1;

        });
        self.slide_totalCount=ko.observable(Swipeset._var().totalslides);


        self.controlmenu=ko.computed(function(){
            if(self.menuopen()) {
                var destinationX=Menuanim.anim_target.innerWidth()-70;

                TweenMax.to(Menuanim.anim_target,Menuanim.anim_time,{x:destinationX});
            }
            else {
                TweenMax.to(Menuanim.anim_target,Menuanim.anim_time,{x:0});

            }

        });
        self.controlshare=ko.computed(function(){
            if(self.shareopen()) {
                //alert('open')
                //Shareanim.target.show();
                Shareanim.target.removeClass('hidethis')
                TweenMax.to(Shareanim.anim_target,.25,{opacity:1,y:0});

                // Shareanim.anim_target.show();
                // Shareanim.anim_target.addClass('showthis')
                // Shareanim.anim_target.removeClass('hidethis')
            }
            else {
                //alert('close');

                TweenMax.to(Shareanim.anim_target,.25,{opacity:0,y:25,onComplete:function(){
                    Shareanim.target.addClass('hidethis')
                }});

                //Shareanim.target.hide();
                // Shareanim.anim_target.addClass('hidethis')
                // Shareanim.anim_target.removeClass('showthis')

            }

        });

        self.controlthumbs=ko.computed(function(){
            if(self.showthumbs()){

               $('#thumbOverlay').removeClass('hidethis')
                TweenMax.to($('#thumbOverlay'),.25,{opacity:1,y:0});

            } else {

                TweenMax.to($('#thumbOverlay'),.25,{opacity:0,y:25,onComplete:function(){
                    $('#thumbOverlay').addClass('hidethis')
                }});

            }

        });

        self.disable_next=ko.computed(function(){
            return ((self.slide_currentIndex()==(self.slide_totalCount()-1))||(self.navdisabled()))?true:false;
        });
        self.disable_prev=ko.computed(function(){
            return ((self.slide_currentIndex()==0)||(self.navdisabled()))?true:false;
        });
        self.disable_navigation=ko.computed(function(){
            if(self.navdisabled()){
                //alert('hmm')
                Swipeset._var().enable_touchbinds=false;
            } else {
                Swipeset._var().enable_touchbinds=true;
            }
        });


        self.databindtoobject=function(getEvent){


            try
            {
                var string= $(getEvent.target).attr('data-bind');
                var properties = string.split(',');



            }
            catch(err)
            {
                var string= $(getEvent.srcElement).attr('data-bind');
                var properties = string.split(',');


            }
            var obj = {};
            $.each(properties,function(i,v){
                var tup = v.split(':');
                obj[tup[0]] = tup[1];
            })




            return obj;
        };


        self.slide_button=function(data, event){
            var switchindex=Number(self.databindtoobject(event).target);
            Swipeset.pagejump(switchindex);
            if(self.menuopen()){self.menu_toggle()}
            if(self.showthumbs()){self.thumbs_toggle()}

        };

        self.slide_next=function(){
            if(!self.navdisabled()){Swipeset.next();};
            //console.log(Swipeset._var().currentslide+' : '+self.slide_currentIndex())
        }
        self.slide_prev=function(){
            if(!self.navdisabled()){Swipeset.prev();};
        }

        self.menu_toggle=function(){
            if(!self.navdisabled()){
            self.menuopen(!self.menuopen())
            }
        }
        self.share_toggle=function(){
            self.shareopen(!self.shareopen())
        }
        self.thumbs_toggle=function(){
            self.showthumbs(!self.showthumbs())
        }


        self.nav_toggle=function(){
            self.navdisabled(!self.navdisabled())
            if(self.navdisabled()!=_var.disableNavigation){_var.disableNavigation=self.navdisabled()}
        }

    };
    _thizOBJ_.prototype.init = function () {
        this.notify('Trace', 'init');
        this._startlisteners();//start this module's listeners
        var self=this;




        self.getdependancies();
        self.buildViewmodel();


        self._var().callback();

    };

    _thizOBJ_.prototype.refresh = function () {
        this.notify('Trace', 'refresh');
        var self=this;

        if(self._var().viewmodel.menuopen()){self._var().viewmodel.menu_toggle()}
        self._var().slide_Swipeset.refresh();
    };

    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.getdependancies = function () {
        var self=this;

        //add functions to the swipeset object
        Swipeset.prototype.tackmatrix = function() {
            //self=this;


            $.each(this._var().childVars,function(index, value ){

                if(value.target.css("transform")=='none'){value.target.css('webkitTransform', 'skew(0)')}

                value.matrix = value.target.css("transform");
                value.matrix = value.matrix.match(/\(([^)]+)\)/)[1].split(',')
                value.z = Number(value.matrix[0]);
                value.x = Number(value.matrix[4]);
                value.y = Number(value.matrix[5]);



            });
            //
            //
            //
            // TweenMax.to(this._var().childVars[0].target, 1.5, {scaleX:.5, scaleY:.5,x:0,y:0});
            //alert(this._var().childVars[3].matrix)

        }

/*        Swipeset.prototype.slidestothumbs = function(){
            var self=this;
            //alert('teehee')

            // var Content=$('#slidebox').html();
            var ThumbContentArea=$('#thumbOverlay').find('.thumb_Content')

            // ThumbContentArea.empty().html(Content);
            // $(ThumbContentArea.children()[7]).css('position','relative').css('transform', 'scale(.5)')


            $.each(ThumbContentArea.children(),function(index,value){

                // $(value).css('position','relative').css('overflow','hidden').css('float','left').css('height','300px').css('width','300px');

                //ko.cleanNode($(value)[0]);
                $(value).unbind('click').bind('click',function(event){
                    //   event.stopPropagation();
                    //   event.preventDefault();
                    //alert('hey! '+index)

                    self.pagejump(index);

                })

            })


            // alert(ThumbContentArea.children().length)


        }*/


        self._var().slide_Swipeset = new Swipeset({

            usetouchbinds:window._global$.isTouch,
            target:self._var().slidebox_target,

            callback:function(){
                //this.parent.slidestothumbs();
                this.parent.tackmatrix();
            },

            onscroll_debounce_callback:function(o){
                 self._var().viewmodel.slide_currentIndex(this.currentslide);
                //  //drop default matrix vars on objects
                window.location.hash = 'page='+(this.currentslide+1);
                window._global$.setCookie('page',(this.currentslide+1))
            },
            onscroll_debounce_rate:200
        });



        //cookie overrides
        var cookiecheck=window._global$.getCookie('page');
        if(cookiecheck!=null){
            var jumppage=Number(cookiecheck)
            jumppage-=1;
            //if(jumppage<0){jumppage=0}
            self._var().slide_Swipeset._var({currentslide:jumppage})
            self._var().disableNavigation=false;
            //alert(jumppage)
        }

        //query override
        var jumppage=Number(window._global$.getQuery('page'));
        if(!isNaN(jumppage)&&jumppage>0){
     //       self._var().slide_Swipeset._var({currentslide: (Number(window._global$.getQuery('page'))-1)})
            jumppage-=1;
            //if(jumppage<0){jumppage=0}
            self._var().slide_Swipeset._var({currentslide:jumppage})
            self._var().disableNavigation=false;
            //alert(jumppage)
        };

        //hash override
        var checkhash=window._global$.getHash('page');
        var jumppage=Number(checkhash.substring(6,checkhash.length))
        if(jumppage>0){
            jumppage-=1;
            self._var().slide_Swipeset._var({currentslide:jumppage})
            self._var().disableNavigation=false;
        }




        self._var().slide_Swipeset.init();
        //self.tackmatrix()
        self._var().pinchZoomset = new Pinch({
            target: $('#popFrame'),//>div.popper_Content
            bindsonchild:true,
            childindex:1
        })
        //self._var().pinchZoomset.init();

        //Pinch
    }

   /* _thizOBJ_.prototype.tackmatrix = function (){
        var self=this;
        alert(self._var().slide_Swipeset.childVars)
    }*/


    _thizOBJ_.prototype.buildViewmodel = function () {
        var self=this;
        self._var().viewmodel=new this.viewmodel(self._var().slide_Swipeset,self._var().menuanim,self._var().shareanim,self._var().pinchZoomset,self._var());

        ko.bindingHandlers.isolatedOptions = {
            init: function (element, valueAccessor) {
                var args = arguments;
                ko.computed({
                    read: function () {
                        ko.utils.unwrapObservable(valueAccessor());
                        ko.bindingHandlers.options.update.apply(this, args);
                    },
                    owner: this,
                    disposeWhenNodeIsRemoved: element
                });
            }
        };    //knockout patch

        ko.bindingHandlers.inverseChecked = {
            init: function(element, valueAccessor, allBindingsAccessor) {
                var value = valueAccessor();
                var interceptor = ko.computed({
                    read: function() {
                        return !value();
                    },
                    write: function(newValue) {
                        value(!newValue);
                    },
                    disposeWhenNodeIsRemoved: element
                });

                var newValueAccessor = function() { return interceptor; };


                //keep a reference, so we can use in update function
                ko.utils.domData.set(element, "newValueAccessor", newValueAccessor);
                //call the real checked binding's init with the interceptor instead of our real observable
                ko.bindingHandlers.checked.init(element, newValueAccessor, allBindingsAccessor);
            },
            update: function(element, valueAccessor) {
                //call the real checked binding's update with our interceptor instead of our real observable
                ko.bindingHandlers.checked.update(element, ko.utils.domData.get(element, "newValueAccessor"));
            }
        };

        ko.bindingHandlers.gethtml={
            init:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;
            },
            update:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;

                var objEkt=$(element);
                var databinds=allBindings();
                var comparevalue=ko.unwrap(valueAccessor());

                //alert()
                objEkt.html($(comparevalue).html())

            }
        }
        ko.bindingHandlers.contenttopop={
            init:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;
            },
            update:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;

                var objEkt=$(element);
                var databinds=allBindings();
                var targetcontent=ko.unwrap(valueAccessor());

                $(element).click(function(){
                    var Content=objEkt.html();
                    $(targetcontent).removeClass('hidethis')

                    var PopupContentArea=$(targetcontent).find('.popper_Content')
                    var CloseButton=$(targetcontent).find('.popper_Close')

                    PopupContentArea.empty();
                    PopupContentArea.html(Content)
                    //$( element ).clone().appendTo( '.popper_Content' );
                    viewModel.Zoomset.init();
                    viewModel.Zoomset.resetposition();
                    //viewModel.Zoomset.reset();

                    //self.self._var().pinchZoomset.init();

                    CloseButton.unbind('click').bind('click',function(){$(targetcontent).addClass('hidethis')})
                })

                //objEkt.html($(targetcontent).html())

            }
        }

        ko.bindingHandlers.toggletarget={
            init:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;
                var objEkt=$(element);
                var databinds=allBindings();
                var target=$(ko.unwrap(valueAccessor()));
                $.data(target,'isopened',false)
            },
            update:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                //var self=this;

                var objEkt=$(element);
                var databinds=allBindings();
                var target=$(ko.unwrap(valueAccessor()));

                $(element).click(function(){

                  //  alert(databinds.className);

                    if($.data(target,'isopened')){



                       /* if(databinds.className=='fullscreen'){
                            if (document.exitFullscreen) {
                                document.exitFullscreen();
                            } else if (document.msExitFullscreen) {
                                document.msExitFullscreen();
                            } else if (document.mozCancelFullScreen) {
                                document.mozCancelFullScreen();
                            } else if (document.webkitExitFullscreen) {
                                document.webkitExitFullscreen();
                            }
                        }*/

                        //close
                        self._var().toggleposition_close(target)
                        //$(element).removeClass('buttonon');
                        target.removeClass(databinds.className);
                        $.data(target,'isopened',false);
                    } else {
                        //open

                       /* if(databinds.className=='fullscreen'){
                            //$('#app-main')[0].msRequestFullscreen();
                            //$('#app-main')[0].mozRequestFullscreen()
                            //$('#app-main')[0].requestFullscreen()
                            var elem = self._var().app_target[0];
                            if (elem.requestFullscreen) {
                                elem.requestFullscreen();
                            } else if (elem.msRequestFullscreen) {
                                elem.msRequestFullscreen();
                            } else if (elem.mozRequestFullScreen) {
                                elem.mozRequestFullScreen();
                            } else if (elem.webkitRequestFullscreen) {
                                elem.webkitRequestFullscreen();
                            }


                        }*/

                        self._var().toggleposition_open(target)
                        //$(element).addClass('buttonon');
                        target.addClass(databinds.className);
                        $.data(target,'isopened',true);
                    }

                    //alert(target)
                        self.refresh();

/*                    var Content=objEkt.html();
                    $(targetcontent).removeClass('hidethis')

                    var PopupContentArea=$(targetcontent).find('.popper_Content')
                    var CloseButton=$(targetcontent).find('.popper_Close')

                    PopupContentArea.empty();
                    PopupContentArea.html(Content)

                    viewModel.Zoomset.init();
                    viewModel.Zoomset.resetposition();


                    CloseButton.unbind('click').bind('click',function(){$(targetcontent).addClass('hidethis')})*/

                    //alert('bam!'+$.data(target,'isopened'))

                })

                //objEkt.html($(targetcontent).html())

            }
        }

        ko.bindingHandlers.sharebind={
            init:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;
                var objEkt=$(element);
                var databinds=allBindings();
                var xsvalue=ko.unwrap(valueAccessor());
                var target=$(xsvalue);
                $.data(target,'dothisthat',{





                })
            },
            update:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;
                var objEkt=$(element);
                var databinds=allBindings();
                var xsvalue=ko.unwrap(valueAccessor());
                var target=$(xsvalue);



                var trypostCp = function (data){
                    try {
                        postCp(data);
                    }
                    catch(exception){
                        console.log('ERROR, no prerequisite function: "postCp"')
                    }
                }

                var trywmdPageLink = function (data){
                    try {
                        wmdPageLink(data);
                    }
                    catch(exception){
                        console.log('ERROR, no prerequisite function: "wmdPageLink"')
                    }
                }



                function segVarPop(obj) {
                    if ($('meta[name=metasegvar]').length > 0)
                    {
                        if (segVarParam("artid") != "0") {
                            obj.activityId = segVarParam("artid");
                        }
                        if (segVarParam("ssp") != "0") {
                            obj.leadSpec = segVarParam("ssp");
                        }
                        if (segVarParam("scg") != "0") {
                            obj.leadConcept = segVarParam("scg");
                        }
                        if (segVarParam("cg") != "0") {
                            obj.contentGroup = segVarParam("cg");
                        }
                        if (segVarParam("bc") != "__") {
                            var bcTemp = segVarParam("bc");
                            bcTemp = bcTemp.substring(1,bcTemp.length-1);
                            bcTemp = bcTemp.split("_");
                            obj.blockCode = bcTemp;
                        }
                    }
                }

                var socialCp=function(platform) {
                    var cpSocData = new Object();
                    cpSocData.appname = "social-functions";
                    cpSocData.activityName = encodeURIComponent(platform);
                    segVarPop(cpSocData);
                    trypostCp(cpSocData);
                }

                var sharobjs={
                    facebook:{
                        runtime:function(){


                            trywmdPageLink('ar-share_face');
                            var fbtitle = document.title;
                            var fblink = window.location;
                            fblink = fblink + "?src=stfb";
                            var fbsharelink = "http://www.facebook.com/share.php?u=" + fblink + "&t=" + fbtitle;
                            socialCp('Facebook');
                            window.open(fbsharelink);


                        }
                    },
                    twitter:{
                        runtime:function(){

                            trywmdPageLink('ar-share_twit');
                            var twtitle = document.title;
                            var twiturl = window.location;
                            twiturl = twiturl + "?src=sttwit";
                            var ftwiturl = "http://twitter.com/share?url="+encodeURIComponent(twiturl)+"&text="+encodeURIComponent(twtitle);
                            socialCp('Twitter');
                            window.open(ftwiturl);


                        }
                    },
                    google:{
                        runtime:function(){

                            trywmdPageLink("ar-share_google");
                            var ggtitle=document.title;
                            var googleurl=window.location;
                            googleurl=googleurl+"?src=stgoogle";
                            var ftgoogleurl="https://plusone.google.com/_/+1/confirm?url="+encodeURIComponent(googleurl)+"&title="+encodeURIComponent(ggtitle);
                            socialCp('Google+');
                            window.open(ftgoogleurl);

                        }
                    },
                    linkedin:{
                        runtime:function(){

                            var title = document.title;
                            var url = window.location;
                            url += "?src=stlinkedin";
                            var fturl = "http://www.linkedin.com/cws/share?url="+url+"&title="+title;
                            socialCp('LinkedIn');
                            window.open(fturl);

                        }
                    },

                    jonathanisawesome:{
                        runtime:function(){alert('yes he is!')}
                    }


                }




                $(element).click(function(){


                    if(typeof sharobjs[xsvalue]!='object'){
                        alert('Que Pasa? where did you get the sharebind called "'+xsvalue+'"??? Please check your databindings for this button')
                    } else {
                        sharobjs[xsvalue].runtime();

                    }
                  //  sharobjs.facebook.runtime();

                    //  alert(databinds.className);

               /*     if($.data(target,'isopened')){
                        //close
                        self._var().toggleposition_close(target)
                        //$(element).removeClass('buttonon');
                        target.removeClass(databinds.className);
                        $.data(target,'isopened',false);
                    } else {
                        //open
                        self._var().toggleposition_open(target)
                        //$(element).addClass('buttonon');
                        target.addClass(databinds.className);
                        $.data(target,'isopened',true);
                    }*/


                })



            }
        }

        ko.applyBindings(self._var().viewmodel);
    }

    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


//knockout blurb
 /*
 define(['knockout','modules/view/viewmodels/application4vm'],
 function (ko,appViewModel) {
    ko.applyBindings(new appViewModel());
 });
 */
