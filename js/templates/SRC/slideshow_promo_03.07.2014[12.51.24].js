/**
 * slideshow_promo Module
 *
 * Date: 1/15/14 : 12:58 PM
 *
 * NOTE: slideshow for promo
 *
 * USAGE:
 *
 */
define(['jquery', 'modules/definitions/standardmodule','knockout','modules/controllers/swipeset', 'tweenmax','modules/view/pinchzoom'], function ($, parentModel,ko,Swipeset,TweenMax,Pinch) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'slideshow_promo', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)

            /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
            //HOLDS MODULE DEPENDANCIES
            slide_Swipeset:undefined,
            slidebox_target:$('#slidebox'),

            pinchZoomset:undefined,
            disableNavigation:true,

           /* childVars:undefined,
            childVar_setter:function(index,value){
                var self = this;
                self.matrix= undefined;
                self.x= 0;
                self.y= 0;
                self.z= 0;
            },*/

            viewmodel: undefined,   //holds the viewmodel
            mail_viewmodel: undefined,
            app_target:$('#app-main'),
            /*, TRY TO STICK TO THESE COMMON STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             file:'somedir/somefile.ext',
             usenocache:true,
             dataIN:{},
             data:undefined,

             interval:undefined,

             */

            menuanim:{
                anim_target:$('#app-main>.presentation_layer'),
                anim_time:.5
            },
            shareanim:{
                target:$('#app-main>.share'),
                anim_target:$('#app-main>.share>div'),
                anim_time:.5
            },
            emailanim:{
                target:$('#emailblock'),
                anim_time:.5
            },

            toggle_hiddenclass:'hidethis',

            toggleposition_open:function(target){
                //alert(window._global$.windowHeight)
                //TweenMax.to(target,.25,{opacity:1,y:180-window._global$.windowHeight});
            },
            toggleposition_close:function(target){
                //TweenMax.to(target,.25,{opacity:1,y:0});
            },


            callback:function(){
                $('#app-main').removeClass('hideapp')
            },
            busy:false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {


        this.notify('Trace','_startlisteners');
        var self=this;
        var myID=  this._id();
        _notify.add(this._id(), function() {

            return {

                onWindowWidth: function (o) {
                    //if (o.senderID == myID) {
                    //alert('onit!')
                    self.refresh();
                    //if(self._var().viewmodel.menuopen()){self._var().viewmodel.menu_toggle()}

                    //self._var().slide_Swipeset.refresh();
                    // alert(o.data)
                    /* parent._var({data: o.data, busy: false});
                     if (parent._var().callback != undefined) {
                     parent._var().callback(o.data)
                     }*/
                    //}
                }
            }
        }());

        /*  */
    };

    _thizOBJ_.prototype.viewmodel = function (Swipeset,Menuanim,Shareanim,Zoomset,_var){
        var self=this;

        self.navdisabled=ko.observable(_var.disableNavigation);
        self.showthumbs=ko.observable(false);


        self.Zoomset=Zoomset;
        self.menuopen=ko.observable(false);
        self.shareopen=ko.observable(false);

        self.slide_currentIndex=ko.observable(Swipeset._var().currentslide);
        self.slide_currentIndex_display=ko.computed(function(){

            return self.slide_currentIndex()+1;

        });
        self.slide_totalCount=ko.observable(Swipeset._var().totalslides);


        self.controlmenu=ko.computed(function(){
            if(self.menuopen()) {
                var destinationX=Menuanim.anim_target.innerWidth()-70;

                TweenMax.to(Menuanim.anim_target,Menuanim.anim_time,{x:destinationX});
            }
            else {
                TweenMax.to(Menuanim.anim_target,Menuanim.anim_time,{x:0});

            }

        });
        self.controlshare=ko.computed(function(){
            if(self.shareopen()) {
                //alert('open')
                //Shareanim.target.show();
                Shareanim.target.removeClass(_var.toggle_hiddenclass)
                //TweenMax.to(Shareanim.anim_target,.25,{opacity:1,y:0});

                // Shareanim.anim_target.show();
                // Shareanim.anim_target.addClass('showthis')
                // Shareanim.anim_target.removeClass('hidethis')
            }
            else {
                //alert('close');

              /*  TweenMax.to(Shareanim.anim_target,.25,{opacity:0,y:25,onComplete:function(){

                }});*/
                Shareanim.target.addClass(_var.toggle_hiddenclass)
                //Shareanim.target.hide();
                // Shareanim.anim_target.addClass('hidethis')
                // Shareanim.anim_target.removeClass('showthis')

            }

        });

        self.controlthumbs=ko.computed(function(){
            if(self.showthumbs()){

               $('#thumbOverlay').removeClass(_var.toggle_hiddenclass)
              //  TweenMax.to($('#thumbOverlay'),.25,{opacity:1,y:0});

            } else {

               // TweenMax.to($('#thumbOverlay'),.25,{opacity:0,y:25,onComplete:function(){
                    $('#thumbOverlay').addClass(_var.toggle_hiddenclass)
              //  }});

            }

        });

        self.disable_next=ko.computed(function(){
            return ((self.slide_currentIndex()==(self.slide_totalCount()-1))||(self.navdisabled()))?true:false;
        });
        self.disable_prev=ko.computed(function(){
            return ((self.slide_currentIndex()==0)||(self.navdisabled()))?true:false;
        });
        self.disable_navigation=ko.computed(function(){
            if(self.navdisabled()){
                //alert('hmm')
                window._global$.setCookie('roadblock','true')
                Swipeset._var().enable_touchbinds=false;
            } else {
                window._global$.setCookie('roadblock','false')
                Swipeset._var().enable_touchbinds=true;
            }
        });


   /*     alert(':(')
        if(self.navdisabled()){
            window._global$.setCookie('roadblock','true')
        }else{
            window._global$.setCookie('roadblock','false')
        }
        */



        self.databindtoobject=function(getEvent){


            try
            {
                var string= $(getEvent.target).attr('data-bind');
                var properties = string.split(',');



            }
            catch(err)
            {
                var string= $(getEvent.srcElement).attr('data-bind');
                var properties = string.split(',');


            }
            var obj = {};
            $.each(properties,function(i,v){
                var tup = v.split(':');
                obj[tup[0]] = tup[1];
            })




            return obj;
        };


        self.slide_button=function(data, event){
            var switchindex=Number(self.databindtoobject(event).target);
            Swipeset.pagejump(switchindex);
            if(self.menuopen()){self.menu_toggle()}
            if(self.showthumbs()){self.thumbs_toggle()}

        };

        self.slide_next=function(){
            if(!self.navdisabled()){Swipeset.next();};
            //console.log(Swipeset._var().currentslide+' : '+self.slide_currentIndex())
        }
        self.slide_prev=function(){
            if(!self.navdisabled()){Swipeset.prev();};
        }

        self.menu_toggle=function(){
            if(!self.navdisabled()){
            self.menuopen(!self.menuopen())
            }
        }
        self.share_toggle=function(){
            self.shareopen(!self.shareopen())
        }
        self.thumbs_toggle=function(){
            self.showthumbs(!self.showthumbs())
        }


        self.nav_toggle=function(){
            self.navdisabled(!self.navdisabled())
            if(self.navdisabled()!=_var.disableNavigation){_var.disableNavigation=self.navdisabled()}

        }







        self.email_isopen=ko.observable(false);

        self.email_toggle=function(){
            console.log('EMAIL TOGGLE!')
            self.email_isopen(!self.email_isopen())
        }




        self.email_validator=function(email){
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);

        };

        self.email_closepopup=function(){
            console.log('email close')
            _var.emailanim.target.addClass(_var.toggle_hiddenclass)
        };
        self.email_openpopup=function(){
            console.log('email open')
            _var.emailanim.target.removeClass(_var.toggle_hiddenclass)
        };


        self.controlemail=ko.computed(function(){

            if(self.email_isopen()) {
                self.email_openpopup();
            }
            else {
                self.email_closepopup();
            }

        });


        self.email_yourname=ko.observable('');
        self.email_youremail=ko.observable('');
        self.email_fromemailnotvalid=ko.observable(false);
        self.email_toemailnotvalid=ko.observable(false);
        self.email_toemail=ko.observable('');
        self.email_subject=ko.observable('');
        self.email_text=ko.observable('');

        self.email_textbuild=function(){
            return 'Your friend '+self.email_yourname()+', sent you this: '+self.email_text();
        };

        self.email_submit=function(){

            self.email_fromemailnotvalid(!self.email_validator(self.email_youremail()));
            self.email_toemailnotvalid(!self.email_validator(self.email_toemail()))

            if(self.email_fromemailnotvalid()&&self.email_toemailnotvalid()){
                self.email_send();
            } else {
                var message='';
                message+='self.email_fromemailnotvalid()'+self.email_fromemailnotvalid()+'/n'
                message+='self.email_toemailnotvalid()'+self.email_toemailnotvalid()
                alert(message)
            };

        };

        self.email_send=function(){
            //alert('submitting')
            var emailinject={
                fromPage:'.',
                template:'',
                encryptEmail:false,
                confirmationPage:'/features/feedback/noscan/cf/fb-load',
                subject: self.email_subject(),
                message:self.email_textbuild(),
                toEmail:self.email_toemail(),
                fromEmail:self.email_youremail()
            };
            var buildpostURL='/postemail?';

            for(var name in emailinject) {
                var value = encodeURIComponent(emailinject[name]);
                buildpostURL+=name+'='+value+'&';
            };

            $.post(buildpostURL,function(){
                self.email_closepopup();
            });


        };




    };

    _thizOBJ_.prototype.mail_viewmodel = function (_var){
        var self = this;

    };

    _thizOBJ_.prototype.init = function () {
        this.notify('Trace', 'init');
        this._startlisteners();//start this module's listeners
        var self=this;




        self.getdependancies();
        self.buildViewmodel();


        self._var().callback();

    };

    _thizOBJ_.prototype.refresh = function () {
        this.notify('Trace', 'refresh');
        var self=this;

        if(self._var().viewmodel.menuopen()){self._var().viewmodel.menu_toggle()}
        self._var().slide_Swipeset.refresh();
    };

    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.getdependancies = function () {
        var self=this;

        //add functions to the swipeset object
        Swipeset.prototype.tackmatrix = function() {
            //self=this;


            $.each(this._var().childVars,function(index, value ){

                if(value.target.css("transform")=='none'){value.target.css('webkitTransform', 'skew(0)')}

                value.matrix = value.target.css("transform");
                value.matrix = value.matrix.match(/\(([^)]+)\)/)[1].split(',')
                value.z = Number(value.matrix[0]);
                value.x = Number(value.matrix[4]);
                value.y = Number(value.matrix[5]);



            });
            //
            //
            //
            // TweenMax.to(this._var().childVars[0].target, 1.5, {scaleX:.5, scaleY:.5,x:0,y:0});
            //alert(this._var().childVars[3].matrix)

        }

/*        Swipeset.prototype.slidestothumbs = function(){
            var self=this;
            //alert('teehee')

            // var Content=$('#slidebox').html();
            var ThumbContentArea=$('#thumbOverlay').find('.thumb_Content')

            // ThumbContentArea.empty().html(Content);
            // $(ThumbContentArea.children()[7]).css('position','relative').css('transform', 'scale(.5)')


            $.each(ThumbContentArea.children(),function(index,value){

                // $(value).css('position','relative').css('overflow','hidden').css('float','left').css('height','300px').css('width','300px');

                //ko.cleanNode($(value)[0]);
                $(value).unbind('click').bind('click',function(event){
                    //   event.stopPropagation();
                    //   event.preventDefault();
                    //alert('hey! '+index)

                    self.pagejump(index);

                })

            })


            // alert(ThumbContentArea.children().length)


        }*/


        self._var().slide_Swipeset = new Swipeset({

            usetouchbinds:window._global$.isTouch,
            target:self._var().slidebox_target,

            callback:function(){
                //this.parent.slidestothumbs();
                this.parent.tackmatrix();
            },

            onscroll_debounce_callback:function(o){
                 self._var().viewmodel.slide_currentIndex(this.currentslide);
                //  //drop default matrix vars on objects
                window.location.hash = 'page='+(this.currentslide+1);
                window._global$.setCookie('page',(this.currentslide+1))
            },
            onscroll_debounce_rate:200
        });



        //cookie overrides
        var cookie_getpage=window._global$.getCookie('page');
        var cookie_isroadblocked=window._global$.getCookie('roadblock');

        //if there's a roadblock, shoot this page back to #1
      /*  if(cookie_isroadblocked){
            cookie_getpage=1;
            window._global$.setCookie('page',1);

        }*/

        //alert(cookie_isroadblocked+' '+cookie_getpage)

        if(cookie_getpage!=null){
            var jumppage=Number(cookie_getpage)
            jumppage-=1;
            //if(jumppage<0){jumppage=0}
            self._var().slide_Swipeset._var({currentslide:jumppage})
            //self._var().disableNavigation=false;
            //alert(jumppage)
        }

/*
        //query override
        var jumppage=Number(window._global$.getQuery('page'));
        if(!isNaN(jumppage)&&jumppage>0){
     //       self._var().slide_Swipeset._var({currentslide: (Number(window._global$.getQuery('page'))-1)})
            jumppage-=1;
            //if(jumppage<0){jumppage=0}
            self._var().slide_Swipeset._var({currentslide:jumppage})
            //self._var().disableNavigation=false;
            //alert(jumppage)
        };
*/

        //hash override
        var checkhash=window._global$.getHash('page');
        //if(cookie_isroadblocked){checkhash=1}
        var jumppage=Number(checkhash.substring(6,checkhash.length))

        if(cookie_isroadblocked=='true'){
            jumppage=1;
            self._var().disableNavigation=true;
        } else {
            self._var().disableNavigation=false;
        }

        if(jumppage>0){
            jumppage-=1;
            self._var().slide_Swipeset._var({currentslide:jumppage})
            //
        }




        self._var().slide_Swipeset.init();
        //self.tackmatrix()
        self._var().pinchZoomset = new Pinch({
            target: $('#popFrame'),//>div.popper_Content
            bindsonchild:true,
            childindex:1
        })
        //self._var().pinchZoomset.init();

        //Pinch
    }

   /* _thizOBJ_.prototype.tackmatrix = function (){
        var self=this;
        alert(self._var().slide_Swipeset.childVars)
    }*/


    _thizOBJ_.prototype.buildViewmodel = function () {
        var self=this;
        var parent=this;
        self._var().viewmodel=new this.viewmodel(self._var().slide_Swipeset,self._var().menuanim,self._var().shareanim,self._var().pinchZoomset,self._var());

        //self._var().mail_viewmodel=new this.mail_viewmodel(self._var());

        ko.bindingHandlers.isolatedOptions = {
            init: function (element, valueAccessor) {
                var args = arguments;
                ko.computed({
                    read: function () {
                        ko.utils.unwrapObservable(valueAccessor());
                        ko.bindingHandlers.options.update.apply(this, args);
                    },
                    owner: this,
                    disposeWhenNodeIsRemoved: element
                });
            }
        };    //knockout patch

        ko.bindingHandlers.inverseChecked = {
            init: function(element, valueAccessor, allBindingsAccessor) {
                var value = valueAccessor();
                var interceptor = ko.computed({
                    read: function() {
                        return !value();
                    },
                    write: function(newValue) {
                        value(!newValue);
                    },
                    disposeWhenNodeIsRemoved: element
                });

                var newValueAccessor = function() { return interceptor; };


                //keep a reference, so we can use in update function
                ko.utils.domData.set(element, "newValueAccessor", newValueAccessor);
                //call the real checked binding's init with the interceptor instead of our real observable
                ko.bindingHandlers.checked.init(element, newValueAccessor, allBindingsAccessor);
            },
            update: function(element, valueAccessor) {
                //call the real checked binding's update with our interceptor instead of our real observable
                ko.bindingHandlers.checked.update(element, ko.utils.domData.get(element, "newValueAccessor"));
            }
        };

        ko.bindingHandlers.gethtml={
            init:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;
            },
            update:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;

                var objEkt=$(element);
                var databinds=allBindings();
                var comparevalue=ko.unwrap(valueAccessor());

                //alert()
                objEkt.html($(comparevalue).html())

            }
        }
        ko.bindingHandlers.contenttopop={
            init:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;
            },
            update:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;

                var objEkt=$(element);
                var databinds=allBindings();
                var targetcontent=ko.unwrap(valueAccessor());

                $(element).click(function(){
                    var Content=objEkt.html();
                    $(targetcontent).removeClass(parent._var().toggle_hiddenclass)

                    var PopupContentArea=$(targetcontent).find('.popper_Content')
                    var CloseButton=$(targetcontent).find('.popper_Close')

                    PopupContentArea.empty();
                    PopupContentArea.html(Content)
                    //$( element ).clone().appendTo( '.popper_Content' );
                    viewModel.Zoomset.init();
                    viewModel.Zoomset.resetposition();
                    //viewModel.Zoomset.reset();

                    //self.self._var().pinchZoomset.init();

                    CloseButton.unbind('click').bind('click',function(){$(targetcontent).addClass('hidethis')})
                })

                //objEkt.html($(targetcontent).html())

            }
        }

        ko.bindingHandlers.toggletarget={
            init:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;
                var objEkt=$(element);
                var databinds=allBindings();
                var target=$(ko.unwrap(valueAccessor()));
                //alert()
                /*if(typeof $.data(target,'isopened')!='boolean'){
                    $.data(target,'isopened',false);
                }*/

            },
            update:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                //var self=this;

                var objEkt=$(element);
                var databinds=allBindings();
                var target=$(ko.unwrap(valueAccessor()));

                $(element).click(function(){

                    //alert( $(target).attr('class')  );

                    if($(target).attr('class').indexOf(databinds.className)!=-1){



                       /* if(databinds.className=='fullscreen'){
                            if (document.exitFullscreen) {
                                document.exitFullscreen();
                            } else if (document.msExitFullscreen) {
                                document.msExitFullscreen();
                            } else if (document.mozCancelFullScreen) {
                                document.mozCancelFullScreen();
                            } else if (document.webkitExitFullscreen) {
                                document.webkitExitFullscreen();
                            }
                        }*/

                        //close
                        //self._var().toggleposition_close(target)
                        //$(element).removeClass('buttonon');
                        target.removeClass(databinds.className);
                        //$.data(target,'isopened',false);
                    } else {
                        //open

                       /* if(databinds.className=='fullscreen'){
                            //$('#app-main')[0].msRequestFullscreen();
                            //$('#app-main')[0].mozRequestFullscreen()
                            //$('#app-main')[0].requestFullscreen()
                            var elem = self._var().app_target[0];
                            if (elem.requestFullscreen) {
                                elem.requestFullscreen();
                            } else if (elem.msRequestFullscreen) {
                                elem.msRequestFullscreen();
                            } else if (elem.mozRequestFullScreen) {
                                elem.mozRequestFullScreen();
                            } else if (elem.webkitRequestFullscreen) {
                                elem.webkitRequestFullscreen();
                            }


                        }*/

                        //self._var().toggleposition_open(target)
                        //$(element).addClass('buttonon');
                        target.addClass(databinds.className);
                        //$.data(target,'isopened',true);
                    }

                    //alert(target)
                        self.refresh();

/*                    var Content=objEkt.html();
                    $(targetcontent).removeClass('hidethis')

                    var PopupContentArea=$(targetcontent).find('.popper_Content')
                    var CloseButton=$(targetcontent).find('.popper_Close')

                    PopupContentArea.empty();
                    PopupContentArea.html(Content)

                    viewModel.Zoomset.init();
                    viewModel.Zoomset.resetposition();


                    CloseButton.unbind('click').bind('click',function(){$(targetcontent).addClass('hidethis')})*/

                    //alert('bam!'+$.data(target,'isopened'))

                })

                //objEkt.html($(targetcontent).html())

            }
        }

        ko.bindingHandlers.sharebind={
            init:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;
                var objEkt=$(element);
                var databinds=allBindings();
                var xsvalue=ko.unwrap(valueAccessor());
                var target=$(xsvalue);
                $.data(target,'dothisthat',{





                })
            },
            update:function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var self=this;
                var objEkt=$(element);
                var databinds=allBindings();
                var xsvalue=ko.unwrap(valueAccessor());
                var target=$(xsvalue);



                var trypostCp = function (data){
                    try {
                        postCp(data);
                    }
                    catch(exception){
                        console.log('ERROR, no prerequisite function: "postCp"')
                    }
                }

                var trywmdPageLink = function (data){
                    try {
                        wmdPageLink(data);
                    }
                    catch(exception){
                        console.log('ERROR, no prerequisite function: "wmdPageLink"')
                    }
                }



                function segVarPop(obj) {
                    if ($('meta[name=metasegvar]').length > 0)
                    {
                        if (segVarParam("artid") != "0") {
                            obj.activityId = segVarParam("artid");
                        }
                        if (segVarParam("ssp") != "0") {
                            obj.leadSpec = segVarParam("ssp");
                        }
                        if (segVarParam("scg") != "0") {
                            obj.leadConcept = segVarParam("scg");
                        }
                        if (segVarParam("cg") != "0") {
                            obj.contentGroup = segVarParam("cg");
                        }
                        if (segVarParam("bc") != "__") {
                            var bcTemp = segVarParam("bc");
                            bcTemp = bcTemp.substring(1,bcTemp.length-1);
                            bcTemp = bcTemp.split("_");
                            obj.blockCode = bcTemp;
                        }
                    }
                }

                var socialCp=function(platform) {
                    var cpSocData = new Object();
                    cpSocData.appname = "social-functions";
                    cpSocData.activityName = encodeURIComponent(platform);
                    segVarPop(cpSocData);
                    trypostCp(cpSocData);
                }

                var sharobjs={
                    facebook:{
                        runtime:function(){


                            trywmdPageLink('ar-share_face');
                            var fbtitle = document.title;
                            var fblink = window.location;
                            fblink = fblink + "?src=stfb";
                            var fbsharelink = "http://www.facebook.com/share.php?u=" + fblink + "&t=" + fbtitle;
                            socialCp('Facebook');
                            window.open(fbsharelink);


                        }
                    },
                    twitter:{
                        runtime:function(){

                            trywmdPageLink('ar-share_twit');
                            var twtitle = document.title;
                            var twiturl = window.location;
                            twiturl = twiturl + "?src=sttwit";
                            var ftwiturl = "http://twitter.com/share?url="+encodeURIComponent(twiturl)+"&text="+encodeURIComponent(twtitle);
                            socialCp('Twitter');
                            window.open(ftwiturl);


                        }
                    },
                    google:{
                        runtime:function(){

                            trywmdPageLink("ar-share_google");
                            var ggtitle=document.title;
                            var googleurl=window.location;
                            googleurl=googleurl+"?src=stgoogle";
                            var ftgoogleurl="https://plusone.google.com/_/+1/confirm?url="+encodeURIComponent(googleurl)+"&title="+encodeURIComponent(ggtitle);
                            socialCp('Google+');
                            window.open(ftgoogleurl);

                        }
                    },
                    linkedin:{
                        runtime:function(){

                            var title = document.title;
                            var url = window.location;
                            url += "?src=stlinkedin";
                            var fturl = "http://www.linkedin.com/cws/share?url="+url+"&title="+title;
                            socialCp('LinkedIn');
                            window.open(fturl);

                        }
                    },

                    jonathanisawesome:{
                        runtime:function(){alert('yes he is!')}
                    }


                }




                $(element).click(function(){


                    if(typeof sharobjs[xsvalue]!='object'){
                        alert('Que Pasa? where did you get the sharebind called "'+xsvalue+'"??? Please check your databindings for this button')
                    } else {
                        sharobjs[xsvalue].runtime();

                    }
                  //  sharobjs.facebook.runtime();

                    //  alert(databinds.className);

               /*     if($.data(target,'isopened')){
                        //close
                        self._var().toggleposition_close(target)
                        //$(element).removeClass('buttonon');
                        target.removeClass(databinds.className);
                        $.data(target,'isopened',false);
                    } else {
                        //open
                        self._var().toggleposition_open(target)
                        //$(element).addClass('buttonon');
                        target.addClass(databinds.className);
                        $.data(target,'isopened',true);
                    }*/


                })



            }
        }

        ko.applyBindings(self._var().viewmodel);
        //ko.applyBindings(self._var().mail_viewmodel);



    }

    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


/* Email This Functions */
/*
function emailThis() {

    $('.addError').remove();
    $('#email_body h1:eq(0)').html(document.title);
    $('#email_layer #toEmail').val('');
    $('#email_layer #senderName').val('');
    $('#email_layer #fromEmail').val('');
    $('#email_layer #message').val('');
    $('#subject').val('Medscape: ' + document.title);

    $('#sentConfirm').hide();
    $('#emailform').show();

    if (navigator.userAgent.indexOf("MSIE") != "-1")
    {
        $('#modal').show();
    }
    else
    {
        $('#modal').fadeIn();
    }

    $('#emailLayerD').fadeIn();
    $('#emailLayerM').fadeIn();

    if (typeof mobileEmail !== 'undefined') {

        if (mobileEmail == true) {

            $('#emailLayerM').css('min-height',window.innerHeight + 'px');
            window.scrollTo(0,1);
            shown = $('#emailLayerM').siblings(':visible');
            $(shown).hide();

        }
    }

    // Convert Feedback Form to EmailThis content
    $('input[type="hidden"][name="template"]').val('jsp.emailthis.template.ftl');
    $('#copySender').prop('checked', true);
    $('input[type="submit"][id="btn_email_sub"]').val('SEND');
    $('#btn_email_sub').css("width","50px");
    $('#emailLayerD #yourEmail').css("min-height","40px");

    $('form[name="emailform"] h3').html("Email This");
    $('#feedbackText').remove();
    $('#email_body #sentConfirm h3').html('Your Email has been sent');
    $('#email_layer #toEmail').val('');
    $('#copy_send').show();
    $('#recipient').parent().show();
    $('#subject').val('Medscape: ' + document.title);
    $('#messageWrapLeft').text("Optional Message:");
    $('#feedlegal').remove();

    $("input:hidden[name='subject']").replaceWith('<input type="text" name="subject" id="subject" value="">');
    $("#feedbackSubject").remove();
    $('#subject').val('Medscape: ' + document.title);


    $('input[type="hidden"][name="fromEmail"]').replaceWith('<input type="text" id="fromEmail" name="fromEmail" value="">');
    $('input[type="text"][name="attrfromEmailFeedback"]').remove();


}




function feedbackThis() {

    $('.addError').remove();
    $('#email_body h1:eq(0)').html(document.title);
    $('#email_layer #toEmail').val('medscapefeedback@webmd.net');
    $('#email_layer #senderName').val('');
    $('#email_layer #fromEmail').val('');
    $('#email_layer #message').val('');
    $('#subject').val('Medscape: ' + document.title);

    $('#sentConfirm').hide();
    $('#emailform').show();

    if (navigator.userAgent.indexOf("MSIE") != "-1") {
        $('#modal').show();
    } else {
        $('#modal').fadeIn();
    }

    $('#emailLayerD').fadeIn();
    $('#emailLayerM').fadeIn();

    if (typeof mobileEmail !== 'undefined') {

        if (mobileEmail == true) {

            $('#emailLayerM').css('min-height', window.innerHeight + 'px');
            window.scrollTo(0, 1);
            shown = $('#emailLayerM').siblings(':visible');
            $(shown).hide();

        }
    }

    // Update and add Attr variables
    var windowlocation = window.location.href;
    $('input[type="hidden"][name="attrarticleId"]').val("N/A");
    $('input[type="hidden"][name="attrfromPage"]').val(windowlocation);
    $('input[type="hidden"][name="attruserProf"]').val(s_user_group);
    $('input[type="hidden"][name="template"]').val('ref.slidefeedback.template.ftl');
    $('input[type="submit"][id="btn_email_sub"]').val('SEND FEEDBACK');
    $('#btn_email_sub').css("width","140px");
    $('#emailLayerD #yourEmail').css("min-height","0");

    $('#copySender').prop('checked', false);

    // Convert email text to Feedback
    $('form[name="emailform"] h3').html("Feedback");
    if ($("#feedbackText")[0]) {} else {
        $('form[name="emailform"] h3').after('<div id="feedbackText" style="margin-bottom:10px;">Help us make reference on Medscape the best clinical resource possible. Please use this form to submit your questions or comments on how to make this slideshow more useful to clinicians.</div>');
        $('#email_body #sentConfirm h3').html('Your feedback has been sent');
    }
    $('#copy_send').hide();
    $('#recipient').parent().hide();
    $('#messageWrapLeft').text("Comments or Suggestions:");
    if ($("#feedlegal")[0]) {} else {
        $('#btn_email_sub').before('<div id="feedlegal" class="legaltext" style="text-align:left;">Please <strong><u>do not</u></strong> use this form to submit personal or patient medical information or to report adverse drug events. You are encouraged to report adverse drug event information to the FDA.</div>');
    }

    if ($("#feedbackSubject")[0]) {} else {
        $("input:text[name='subject']").replaceWith('<div id="feedbackSubject" >' + document.title + "</div>");
        $("#feedbackSubject").after('<input type="hidden" name="subject" id="subject" value="">');
        $("input:hidden[name='subject']").attr({
            value: document.title
        });
    }

    $('input[type="text"][name="fromEmail"]').replaceWith('<input type="hidden" id="fromEmail" name="fromEmail" value="medscapefeedback@webmd.net"><input type="text" id="attrfromEmailFeedback" name="attrfromEmailFeedback" value="">');

}


function emailInit() {

    if (typeof mobileEmail !== 'undefined') {
        if (mobileEmail == true) {

            $('.email_close').bind('click', function () {

                $(shown).show();
                $('#emailLayerM').fadeOut();
            });
        }
    }

    else {
        $('.emailLayerD_close').bind('click', function () {

            if (navigator.userAgent.indexOf("MSIE") != "-1")
            {
                $('#modal').hide();
            }
            else
            {
                $('#modal').fadeOut();
            }

            $('#emailLayerD').fadeOut();
        });
    }

    $('form[name=emailform]').submit(function(e){

        e.preventDefault();
        $('.addError').remove();

        var errFlag = false;
        var mailTest=/^\S+@\S+\.\S+$/i;

        $('#toEmail').val($.trim($('#toEmail').val()));

        if ($('#toEmail').val().match(/^[<>{}()\"'\[\]]\S+[<>{}()\"'\[\]]$/g) !== null)
        {
            $('#toEmail').val($('#toEmail').val().substring(1,$('#toEmail').val().length - 1));
            $('#toEmail').val($.trim($('#toEmail').val()));
        }

        $('#fromEmail').val($.trim($('#fromEmail').val()));

        if ($('#fromEmail').val().match(/^[<>{}()\"'\[\]]\S+[<>{}()\"'\[\]]$/g) !== null)
        {
            $('#fromEmail').val($('#fromEmail').val().substring(1,$('#fromEmail').val().length - 1));
            $('#fromEmail').val($.trim($('#fromEmail').val()));
        }

        if ($('#email_layer textarea[name=message]').val().length > 250)
        {
            $('form[name=emailform] h3:eq(0)').after('<div class="addError">Message can not be greater than 250 characters.</div>');
            errFlag = true;
        }

        if ($('#toEmail').val() == "")
        {
            $('form[name=emailform] h3:eq(0)').after('<div class="addError">Please enter Recipient Email Address.</div>');
            errFlag = true;
        }

        if ($('#toEmail').val() !== "" && (mailTest.test($('#toEmail').val()) == false || $('#toEmail').val().match(/@/g).length > 1 || $('#toEmail').val().match(/[ "(),:;<>\[\\\]]/g) !== null))
        {
            $('form[name=emailform] h3:eq(0)').after('<div class="addError">' + $('#toEmail').val() + ' is an Invalid Email Address.</div>');
            errFlag = true;
        }

        if ($('#fromEmail').val() == "")
        {
            $('form[name=emailform] h3:eq(0)').after('<div class="addError">Please enter Your Email Address.</div>');
            errFlag = true;
        }

        if ($('#fromEmail').val() !== "" && (mailTest.test($('#fromEmail').val()) == false || $('#fromEmail').val().match(/@/g).length > 1 || $('#fromEmail').val().match(/[ "(),:;<>\[\\\]]/g) !== null))
        {
            $('form[name=emailform] h3:eq(0)').after('<div class="addError">' + $('#fromEmail').val() + ' is an Invalid Email Address.</div>');
            errFlag = true;
        }

        if ($('#senderName').val() == "" || $('#senderName').val().match(/^\s+$/g) !== null)
        {
            $('form[name=emailform] h3:eq(0)').after('<div class="addError">Please enter Your Name.</div>');
            errFlag = true;
        }

        if (errFlag == false)
        {

            $('#attrtoEmail').val($('#toEmail').val());
            $('#attrsenderName').val($('#senderName').val());
            $('#attrfromEmail').val($('#fromEmail').val());
            $('#attrlinkURL').val(window.location.href.split('?')[0].split('#')[0]);
            $('#attrcontentTitle').val(document.title);

            var postMail = $('form[name=emailform]').serialize().replace(/\+/g,"%20");

            if ($('#email_layer textarea[name=message]').val().match(/\S/) == null)
            {
                $('#message').val($.trim($('#message').val()));
                postMail = $('form[name=emailform]').serialize().replace(/\+/g,"%20");
                postMail = postMail.replace(/message=/,"message=%26nbsp%3B");
            }

            $.post('/postemail?' + postMail, function() {
                $('#emailform').hide();
                $('#sentConfirm').show();
                socialCp("emailthis");
            });

            if ($('#email_layer input#copySender').is(':checked'))
            {
                $.post('/postemail?' + postMail.replace(/&toEmail=.+&attrtoEmail=.+&subject=/,'&toEmail=' + encodeURIComponent($('#email_layer input[name=fromEmail]').attr('value')) + '&attrtoEmail=' + encodeURIComponent($('#email_layer input[name=fromEmail]').attr('value')) + '&subject='));
            }

            if (navigator.userAgent.indexOf("Android") != "-1")
            {
                var droidVer = String(navigator.userAgent.match(/Android \d\.\d/));
                droidVer = Number(droidVer.substring(8, 11));
                if (droidVer < 2.3)
                {
                    $('#emailform').hide();
                    $('#sentConfirm').show();
                }
            }

        }

    });

}
*/


//knockout blurb
 /*
 define(['knockout','modules/view/viewmodels/application4vm'],
 function (ko,appViewModel) {
    ko.applyBindings(new appViewModel());
 });
 */
