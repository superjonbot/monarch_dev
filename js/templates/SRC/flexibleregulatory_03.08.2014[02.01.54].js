/*!*/
/*!NOTE: This is dual window template using the slide object */
/*!*/
/*!USAGE: */
/*!*/

define(['jquery', 'modules/definitions/standardmodule', 'knockout', 'modules/controllers/slideset', 'tweenmax'], function ($, parentModel,ko,Slideset,TweenMax) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'flexibleregulatory', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)
            /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
            //HOLDS MODULE DEPENDANCIES
            info_Slideset: undefined,  //holds the info slide object
            slide_Slideset: undefined,   //holds the content slide object
            viewmodel: undefined,   //holds the viewmodel

            app_target:$('#app-main'),
            infobox_target:$('#infobox'),
            slidebox_target:$('#slidebox'),
            menuanim:{
                anim_target:$('#app-main>.bot-right'),
                anim_time:.5
            },
            shareanim:{
                target:$('#app-main>.share'),
                anim_target:$('#app-main>.share>div'),
                anim_time:.5
            },
            callback:function(){
                //alert('I haz been built');
                this.app_target.css('opacity',1)
            },
            busy:false

            /*, TRY TO STICK TO THESE COMMON STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             file:'somedir/somefile.ext',
             usenocache:true,
             dataIN:{},
             data:undefined,
             callback:function(){},
             interval:undefined,
             busy:false
             */
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    }


    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {


         this.notify('Trace','_startlisteners');
         var self=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {

             onWindowWidth: function (o) {
                 //if (o.senderID == myID) {
                 //alert('onit!')
                  if(self._var().viewmodel.menuopen()){self._var().viewmodel.menu_toggle()}
                    // alert(o.data)
                     /* parent._var({data: o.data, busy: false});
                    if (parent._var().callback != undefined) {
                         parent._var().callback(o.data)
                     }*/
                 //}
             }
         }
         }());

        /*  */
    };
    // MODULE METHODS - you should always have init/refresh/kill defined!
    //VIEWMODEL DEFINITION
    _thizOBJ_.prototype.viewmodel = function (Slideset,Infoset,Menuanim,Shareanim){
        var self=this;

        self.menuopen=ko.observable(false);
        self.shareopen=ko.observable(false);

        self.slide_currentIndex=ko.observable(Slideset._var().currentslide);
        self.slide_totalCount=ko.observable(Slideset._var().totalslides);

        self.info_currentIndex=ko.observable(Infoset._var().currentslide);
        self.info_totalCount=ko.observable(Infoset._var().totalslides);

        self.controlmenu=ko.computed(function(){
            if(self.menuopen()) {
                var destinationX=Menuanim.anim_target.innerWidth()-70;

                TweenMax.to(Menuanim.anim_target,Menuanim.anim_time,{x:destinationX});
            }
            else {
                TweenMax.to(Menuanim.anim_target,Menuanim.anim_time,{x:0});

            }

        });
        self.controlshare=ko.computed(function(){
            if(self.shareopen()) {
                //alert('open')
                Shareanim.target.show();

                TweenMax.to(Shareanim.anim_target,.25,{opacity:1,y:0});

               // Shareanim.anim_target.show();
               // Shareanim.anim_target.addClass('showthis')
               // Shareanim.anim_target.removeClass('hidethis')
            }
            else {
                //alert('close');

                TweenMax.to(Shareanim.anim_target,0,{opacity:0,y:25});

                Shareanim.target.hide();
               // Shareanim.anim_target.addClass('hidethis')
               // Shareanim.anim_target.removeClass('showthis')

            }

        });

        self.disable_next=ko.computed(function(){
            return (self.slide_currentIndex()==(self.slide_totalCount()-1))?true:false;
        });
        self.disable_prev=ko.computed(function(){
            return (self.slide_currentIndex()==0)?true:false;
        });

        self.databindtoobject=function(getEvent){


            try
            {
                var string= $(getEvent.target).attr('data-bind');
                var properties = string.split(',');



            }
            catch(err)
            {
                var string= $(getEvent.srcElement).attr('data-bind');
                var properties = string.split(',');


            }
            var obj = {};
            $.each(properties,function(i,v){
                var tup = v.split(':');
                obj[tup[0]] = tup[1];
            })




            return obj;
        };
        self.info_button=function(data, event){

           //alert( $(event.target).attr('data-bind'));
            var switchindex=Number(self.databindtoobject(event).target);
/*
            try
            {

            }
            catch(err)
            {
                var switchindex=Number(self.databindtoobject(event).srcElement);
            }*/


            Infoset.jump(switchindex)
        };
        self.slide_button=function(data, event){
            var switchindex=Number(self.databindtoobject(event).target);
            Slideset.pagejump(switchindex);
            if(self.menuopen()){self.menu_toggle()}
        };
        self.slide_next=function(){
            Slideset.next();
        };
        self.slide_prev=function(){
            Slideset.prev();
        };
        self.menu_toggle=function(){
            self.menuopen(!self.menuopen())
        };
        self.share_toggle=function(){
            self.shareopen(!self.shareopen())
            //alert(self.shareopen())
        };

        //pass object callbacks to viewmodel
        Slideset._var().childtrack=function(index){self.slide_currentIndex(index)}


        Infoset._var().childtrack=function(index){
            self.info_currentIndex(index);
        }

    }

    _thizOBJ_.prototype.init = function () {
        this.notify('Trace', 'init');
        this._startlisteners();//start this module's listeners
        var self=this;

        self.getdependancies();
        self.buildViewmodel();


        self._var().callback();

    };

    _thizOBJ_.prototype.refresh = function () {
        this.notify('Trace', 'refresh');
        var self=this;

        alert('this module has no refresh function')
    };

    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.getdependancies = function () {

        var self=this;
        self._var().info_Slideset = new Slideset({
            target:self._var().infobox_target,
            child_focus:function(o){
                var argz=o;
                var tempObject= $(this.childObjects[argz.index]);
                tempObject.show();

            },
            child_unfocus:function(o){
                var argz=o
                var tempObject= $(this.childObjects[argz.unfocusedindex]);
                tempObject.hide();
            }
        });
        self._var().slide_Slideset = new Slideset({
            target: self._var().slidebox_target,
            autoZ: true,
            child_focus: function (o) {
                var argz = {
                    index: undefined,
                    speed: .5,
                    tweenvars: {
                        x: 0,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    }
                };
                argz = $.extend(argz, o);

                var tempObject = $(this.childObjects[argz.index]);
                if (Modernizr.csstransitions) {
                    TweenMax.to(tempObject, argz.speed, argz.tweenvars);
                }
                else {
                    tempObject.show()
                }

            },  //do this to focused slide
            child_unfocus: function (o) {
                var argz = {
                    index: undefined, //of current or candidate
                    unfocusedindex: undefined,
                    speed: 1,  // 1 for animation
                    offset: 0,
                    tweenvarsIN: {
                        x: -400,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    },
                    tweenvarsOUT: {
                        x: 400,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    }
                };
                argz = $.extend(argz, o);

                /* argz.tweenvarsIN.y -= (30 * argz.offset);
                 argz.tweenvarsOUT.y -= (30 * argz.offset);
                 argz.tweenvarsIN.x += ((10 * argz.offset) * (2 * argz.offset));
                 argz.tweenvarsOUT.x -= ((10 * argz.offset) * (2 * argz.offset));
                 argz.tweenvarsIN.rotation += (15 * argz.offset);
                 argz.tweenvarsOUT.rotation += (15 * argz.offset);
                 argz.tweenvarsIN.opacity += .9 - (.15 * argz.offset);
                 argz.tweenvarsOUT.opacity += .9 - (.15 * argz.offset);*/

                var tempObject = $(this.childObjects[argz.unfocusedindex]);

                if (Modernizr.csstransitions) {
                    if (argz.unfocusedindex < argz.index) {
                        TweenMax.to(tempObject, argz.speed, argz.tweenvarsIN);
                    } else {
                        TweenMax.to(tempObject, argz.speed, argz.tweenvarsOUT);
                    }
                    ;
                }
                else {
                    tempObject.hide();
                }


            }
        });
        self._var().info_Slideset.init();
        self._var().slide_Slideset.init();

    }

    _thizOBJ_.prototype.buildViewmodel = function () {
        var self=this;
        self._var().viewmodel=new this.viewmodel(self._var().slide_Slideset,self._var().info_Slideset,self._var().menuanim,self._var().shareanim);

        ko.bindingHandlers.isolatedOptions = {
            init: function (element, valueAccessor) {
                var args = arguments;
                ko.computed({
                    read: function () {
                        ko.utils.unwrapObservable(valueAccessor());
                        ko.bindingHandlers.options.update.apply(this, args);
                    },
                    owner: this,
                    disposeWhenNodeIsRemoved: element
                });
            }
        };    //knockout patch
        ko.bindingHandlers.CSSonMatch = {


            init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var self=this;


                //alert(object.attr('data-bind')); // value of target binding
                //alert(databinds.target); // value of target binding

                //alert(allBindings.has('click'))
                // This will be called when the binding is first applied to an element
                // Set up any initial state, event handlers, etc. here
            },
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
                // This will be called once when the binding is first applied to an element,
                // and again whenever the associated observable changes value.
                // Update the DOM element based on the supplied values here.

                var objEkt=$(element);
                var databinds=allBindings();
                var comparevalue=ko.unwrap(valueAccessor());


                if(databinds.CSSclass!=undefined){

                    if(databinds.target!=undefined){

                        if(comparevalue==databinds.target){
                            //  alert('place'+databinds.CSSclass)
                            objEkt.addClass(databinds.CSSclass)
                        }else{
                            // alert('remove'+databinds.CSSclass)
                            objEkt.removeClass(databinds.CSSclass)
                        }


                    }else{
                        alert("no target is defined with Jonathan's awesome CSSonMatch BINDING!")
                    }



                } else {alert("no CSSclass is defined with Jonathan's awesome CSSonMatch BINDING!")}

            }
        };

        ko.applyBindings(self._var().viewmodel);
    }


    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


//knockout blurb
 /*
 define(['knockout','modules/view/viewmodels/application4vm'],
 function (ko,appViewModel) {
    ko.applyBindings(new appViewModel());
 });
 */
