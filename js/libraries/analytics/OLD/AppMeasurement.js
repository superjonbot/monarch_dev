function OmnitureMeasurement(rsid) {
  
  var s = {
    account: rsid //required
  , linkURL: ''
  , linkName: ''
  , linkType: ''
  , dc: ''
  , trackingServer: ''
  , trackingServerSecure: ''
  , userAgent: ''
  , dynamicVariablePrefix: ''
  , visitorID: aetnUtils.getVisitorID() //required. no longer defaults to Device Hardware ID
  , vmk: ''
  , visitorMigrationKey: ''
  , visitorMigrationServer: ''
  , visitorMigrationServerSecure: ''
  , charSet:'UTF-8'
  , visitorNamespace: ''
  , pageName: '' //required
  , pageURL: '' //required
  , referrer: ''
  , currencyCode: ''
  , purchaseID: ''
  , variableProvider: ''
  , channel: ''
  , server: ''
  , pageType: ''
  , transactionID: ''
  , campaign: ''
  , state: ''
  , zip: ''
  , events: ''
  , products: ''
  , hier1: ''
  , hier2: ''
  , hier3: ''
  , hier4: ''
  , hier5: ''
  , prop1: ''
  , prop2: ''
  , prop3: ''
  , prop4: ''
  , prop5: ''
  , prop6: ''
  , prop7: ''
  , prop8: ''
  , prop9: ''
  , prop10: ''
  , prop11: ''
  , prop12: ''
  , prop13: ''
  , prop14: ''
  , prop15: ''
  , prop16: ''
  , prop17: ''
  , prop18: ''
  , prop19: ''
  , prop20: ''
  , prop21: ''
  , prop22: ''
  , prop23: ''
  , prop24: ''
  , prop25: ''
  , prop26: ''
  , prop27: ''
  , prop28: ''
  , prop29: ''
  , prop30: ''
  , prop31: ''
  , prop32: ''
  , prop33: ''
  , prop34: ''
  , prop35: ''
  , prop36: ''
  , prop37: ''
  , prop38: ''
  , prop39: ''
  , prop40: ''
  , prop41: ''
  , prop42: ''
  , prop43: ''
  , prop44: ''
  , prop45: ''
  , prop46: ''
  , prop47: ''
  , prop48: ''
  , prop49: ''
  , prop50: ''
  , prop51: ''
  , prop52: ''
  , prop53: ''
  , prop54: ''
  , prop55: ''
  , prop56: ''
  , prop57: ''
  , prop58: ''
  , prop59: ''
  , prop60: ''
  , prop61: ''
  , prop62: ''
  , prop63: ''
  , prop64: ''
  , prop65: ''
  , prop66: ''
  , prop67: ''
  , prop68: ''
  , prop69: ''
  , prop70: ''
  , prop71: ''
  , prop72: ''
  , prop73: ''
  , prop74: ''
  , prop75: ''
  , eVar1: ''
  , eVar2: ''
  , eVar3: ''
  , eVar4: ''
  , eVar5: ''
  , eVar6: ''
  , eVar7: ''
  , eVar8: ''
  , eVar9: ''
  , eVar10: ''
  , eVar11: ''
  , eVar12: ''
  , eVar13: ''
  , eVar14: ''
  , eVar15: ''
  , eVar16: ''
  , eVar17: ''
  , eVar18: ''
  , eVar19: ''
  , eVar20: ''
  , eVar21: ''
  , eVar22: ''
  , eVar23: ''
  , eVar24: ''
  , eVar25: ''
  , eVar26: ''
  , eVar27: ''
  , eVar28: ''
  , eVar29: ''
  , eVar30: ''
  , eVar31: ''
  , eVar32: ''
  , eVar33: ''
  , eVar34: ''
  , eVar35: ''
  , eVar36: ''
  , eVar37: ''
  , eVar38: ''
  , eVar39: ''
  , eVar40: ''
  , eVar41: ''
  , eVar42: ''
  , eVar43: ''
  , eVar44: ''
  , eVar45: ''
  , eVar46: ''
  , eVar47: ''
  , eVar48: ''
  , eVar49: ''
  , eVar50: ''
  , eVar51: ''
  , eVar52: ''
  , eVar53: ''
  , eVar54: ''
  , eVar55: ''
  , eVar56: ''
  , eVar57: ''
  , eVar58: ''
  , eVar59: ''
  , eVar60: ''
  , eVar61: ''
  , eVar62: ''
  , eVar63: ''
  , eVar64: ''
  , eVar65: ''
  , eVar66: ''
  , eVar67: ''
  , eVar68: ''
  , eVar69: ''
  , eVar70: ''
  , eVar71: ''
  , eVar72: ''
  , eVar73: ''
  , eVar74: ''
  , eVar75: ''
  , list1: ''
  , list2: ''
  , list3: ''
  , timestamp: ''
  , profileID: ''
  , ssl: false
  , debugTracking: false
  , autoTimestamp: false
  };
  
  return s;
}

//Version As String
function getResolution() {
  return atv.device.preferredVideoFormat;
}

//Version As String
function getFirmwareVersion() {
  return atv.device.softwareVersion;
}

function sendRequest(s, contextData) {

  var url = 'http';
  var prefix = '';
  //check for ssl && set inital prefix value if available
  if (s.ssl) {
    url = url + 's';
    prefix = s.trackingServerSecure;
  } else {
    prefix = s.trackingServer;
  }
  
  url = url + '://';
  
  //prepare the prefix if tracking server is not specified
  if (prefix === '') {
    //fix up the dc variable, default to SJO
    s.dc = s.dc.toLowerCase();
    if (s.dc === 'dc2' || s.dc === '122') {
      s.dc = '122';
    } else {
      s.dc = '112';
    }
    
    if (s.visitorNamespace !== '') {
      prefix = s.visitorNamespace + '.' + s.dc + '.207.net';
    } else {
      prefix = s.account + '.' + s.dc + '.207.net';
    }
  }
  
  url = url + prefix + '/b/ss/' + s.account + '/0/BRS-0.7/?AQB=1&ndh=1&';
  
  //build the query string
  var qs = '';
  var value = '';
  for (var o in s) {
    
    if (s.debugTracking) {
      value = '';
      if (typeof(s[o]) === 'string' && s[o] !== '') {
        value = s[o];
      }
    }
    
    if (o === 'visitorid' && s[o] !== '') {
      qs = qs + 'vid=' + encodeURI(s[o]) + '&';
    } else if (o === 'pageurl' && s[o].length > 0) {
      qs = qs + 'g=' + encodeURI(s[o].substring(0, 255)) + '&';
    } else if (o === 'referrer' && s[o] !== '') {
      qs = qs + 'r=' + encodeURI(s[o].substring(0, 255)) + '&';
    } else if (o === 'events' && s[o] !== '') {
      qs = qs + 'ev=' + encodeURI(s[o]) + '&';
    } else if (o === 'visitormigrationkey' && s[o] !== '') {
      qs = qs + 'vmt=' + s[o] + '&';
    } else if (o === 'vmk' && s[o] !== '') {
      qs = qs + 'vmt=' + s[o] + '&';
    } else if (o === 'visitormigrationserver' && s[o] !== '' && s.ssl === false) {
      qs = qs + 'vmf=' + s[o] + '&';
    } else if (o === 'visitormigrationserversecure' && s[o] !== '' && s.ssl === true) {
      qs = qs + 'vmf=' + s[o] + '&';
    } else if (o === 'timestamp') {
      if (s.autoTimestamp) {
        var dt = new Date();
        dt.mark();
        //autoTimestamp needs encodeURI?
        qs = qs + 'ts=' + encodeURI(dt.asSeconds().toStr()) + '&';
      } else if (s[o] !== '') {
        qs = qs + 'ts=' + encodeURI(s[o]) + '&';
      }
    } else if (o === 'pagename' && s[o] !== '') {
      qs = qs + 'pageName=' + encodeURI(s[o]) + '&';
    } else if (o === 'pagetype' && s[o] !== '') {
      qs = qs + 'gt=' + encodeURI(s[o]) + '&';
    } else if (o === 'products' && s[o] !== '') {
      qs = qs + 'pl=' + encodeURI(s[o]) + '&';
    } else if (o === 'purchaseid' && s[o] !== '') {
      qs = qs + 'pi=' + encodeURI(s[o]) + '&';
    } else if (o === 'server' && s[o] !== '') {
      qs = qs + 'sv=' + encodeURI(s[o]) + '&';
    } else if (o === 'charset' && s[o] !== '') {
      qs = qs + 'ce=' + s[o] + '&';
    } else if (o === 'visitornamespace' && s[o] !== '') {
      qs = qs + 'ns=' + s[o] + '&';
    } else if (o === 'currencycode' && s[o] !== '') {
      qs = qs + 'cc=' + s[o] + '&';
    } else if (o === 'channel' && s[o] !== '') {
      qs = qs + 'ch=' + encodeURI(s[o]) + '&';
    } else if (o === 'transactionid' && s[o] !== '') {
      qs = qs + 'xact=' + encodeURI(s[o]) + '&';
    } else if (o === 'campaign' && s[o] !== '') {
      qs = qs + 'v0=' + encodeURI(s[o]) + '&';
    } else if (o.substring(0,4) === 'prop' && s[o] !== '') {
      qs = qs + 'c' + o.substring(5) + '=' + encodeURI(s[o]) + '&';
    } else if (o.substring(0,4) === 'evar' && s[o] !== '') {
      qs = qs + 'v' + o.substring(5) + '=' + encodeURI(s[o]) + '&';
    } else if (o.substring(0,4) === 'list' && s[o] !== '') {
      qs = qs + 'l' + o.substring(5) + '=' + encodeURI(s[o]) + '&';
    } else if (o.substring(0,4) === 'hier' && s[o] !== '') {
      qs = qs + 'h' + o.substring(5) + '=' + encodeURI(s[o]) + '&';
    } else if (o === 'linktype' && s[o] !== '') {
      qs = qs + 'pe=lnk_' + s[o] + '&';
    } else if (o === 'linkurl' && s[o] !== '') {
      qs = qs + 'pev1=' + encodeURI(s[o]) + '&';
    } else if (o === 'linkname' && s[o] !== '') {
      qs = qs + 'pev2=' + encodeURI(s[o]) + '&';
    } else if (o === 'state' && s[o] !== '') {
      qs = qs + 'state=' + encodeURI(s[o]) + '&';
    } else if (o === 'zip' && s[o] !== '') {
      qs = qs + 'zip=' + encodeURI(s[o]) + '&';
    } else if (o === 'profileID' && s[o] !== '') {
      qs = qs + 'mtp=' + encodeURI(s[o]) + '&';
    }
  }

  var contextvars = 0;
  
  var cqs = 'c.' + '&';
  for (var key in contextData) {
    cqs = cqs + encodeURIComponent(key) + '=' + encodeURIComponent(contextData[key]) + '&';
    contextvars = contextvars + 1;
    if (s.debugTracking) {
      console.log('debug', 'siteCatalytic', 'context data', contextData[key], key);
    }
  }
  cqs = cqs + '.c' + '&';
  
  if (contextvars > 0) {
    qs = qs + cqs;
  }
  //cap the query string
  url = url + qs + 'AQE=1';
    
  //send the request
  if (s.debugTracking) {
    console.log('debug', 'siteCatalytic', 'send catalytic request: ' + url);
  }

  //write code to send requrest using Ajax
  var ajax = new ATVUtils.Ajax({
    url: url,
    headers: {},
    success: function(xhr) {
      console.log('successfully send omniture tracking request');
    },
    failure: function(status, xhr) {
      console.log('Failure sending request: ', status, xhr);
    }
  });

  
  return 1;
}