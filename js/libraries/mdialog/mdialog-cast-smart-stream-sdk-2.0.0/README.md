================================
mDialog Cast Smart Stream SDK
================================

This SDK provides ability to create mDialog-managed video streams for playing back on Cast devices such as Chromecast. It should be integrated and used in coordination with the Cast receiver and Cast sender applications.

---------------------
Usage :
---------------------

	To use the SDK, add the following script tag to your page and point to the correct SDK source link:
    <script type="text/javascript" src="http://example.com/mdialog-cast-sdk-version.js"></script>

---------------------
Required Dependencies :
---------------------

	Google Cast SDK (Sender and Receiver)
	=> https://developers.google.com/cast/

---------------------
Supported Devices/Versions :
---------------------

	Chromecast, Android TV
	
---------------------
Contact Information :
---------------------

	For any bugs or issues, contact mdialog-support@google.com.

---------------------
Contributors :
---------------------

	Chirayu Patel
