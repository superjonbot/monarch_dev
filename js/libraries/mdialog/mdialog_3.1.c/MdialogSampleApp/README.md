================================
mDialog AppleTV Stream SDK
================================

This SDK provides ability to create mDialog-managed video streams for playing back on AppleTV device. It should be integrated and tested with development enabled AppleTV device.

---------------------
Usage :
---------------------

	To test the SDK, put the whole package on your web server. Change all the "http://files.mdialog.com/appletv/sample-sdk-3.1" to your own domain and path. Follow the AppleTV development document to set up testing site.
	You may use the script changeHostname.sh inside the sample app to update the domain name.

---------------------
Required Dependencies :
---------------------


---------------------
Supported Devices/Versions :
---------------------

	AppleTV 7.x
	
---------------------
Contact Information :
---------------------

	For any bugs or issues, contact mdialog-support@google.com.

---------------------
Contributors :
---------------------

	Chirayu Patel
	Yan Zhao
