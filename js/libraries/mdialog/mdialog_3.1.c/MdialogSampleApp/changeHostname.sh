#!/bin/bash
# changeHostname.sh <originalhostname> <hostname> <output_folder>
# Change the default name used in the sample site (i.e. sample-web-server) to hostname. The new sample store
# is created in output_folder.
DefaultHostname=$1
Hostname=$2
OutputFolder=$3


function usage {
	echo "Usage: changeHostname.sh <originalhostname> <hostname> <output_folder_optional>"
	echo "For example:"
	echo "  ./changeHostname.sh files.mdialog.com/appletv/sample-sdk-3.1 192.168.0.100/mdialog ~/Desktop/demo"
}

if [ ! -f ./bag.plist ]; then
	echo "You must run this script from the folder from the root of the sample site"
	usage;
	exit 1
fi

if [ -z "$DefaultHostname" ]; then
	echo "You must specify an original hostname"
	usage;
	exit 1
fi

if [ -z "$Hostname" ]; then
	echo "You must specify a hostname"
	usage;
	exit 1
fi

#http://tldp.org/LDP/abs/html/string-manipulation.html
DefaultHostname="${DefaultHostname//\//\\/}"
Hostname="${Hostname//\//\\/}"


if [ ! -z "$OutputFolder" ]; then

	if [ -e "$OutputFolder" ]; then
		echo "The output folder '$OutputFolder' already exists. Remove it before running this script."
		exit 1
	fi

	echo Copying sample site to $OutputFolder
	ditto . "$OutputFolder" || exit 1

	cd "$OutputFolder" || exit 1
fi

echo Renaming all occurrances of $DefaultHostname in plist and JavaScript files to $Hostname
find . -type f -print0 \( -name '*.js' -or -name '*.plist' -or -name '*.xml' \) | xargs -0 perl -i -pe s/$DefaultHostname/$Hostname/g || exit 1

echo Success
