console.log('application.js 3.1 loaded');
// ***************************************************
// mDialog SDK - a JavaScript library for Apple TV
// ***************************************************

var mDialog=mDialog||{ATVPlayer:function(){function m(){var a=atv.sessionStorage["mDialog-sessionid"];if(!a){for(var b="",d=0;25>d;d++)a=Math.floor(35*Math.random()),b+="0123456789abcdefghiklmnopqrstuvwxyz".substring(a,a+1);a=b;atv.sessionStorage["mDialog-sessionid"]=a}return a}function v(a){return"https://"+atv.sessionStorage.getItem("mDialog-subdomain")+".mdialog.com/video_assets/"+a+"/streams"}function h(){return JSON.parse(atv.sessionStorage.getItem("mDialog-debug"))}function r(a){h()&&console.log("-- Ping URL: "+a);var b=new XMLHttpRequest;try{b.open("GET",a),b.send()}catch(f){try{var d=new XMLHttpRequest;a=encodeURI(a);a=a.replace(/#/g,"%23");h()&&console.log("-- Ping URL retry-------: "+a);d.open("GET",a);d.send()}catch(e){h()&&console.log("-- Ping URL retry failed-------: "+e)}}}function l(a){return{urlTemplates:a,fire:function(){if(this.urlTemplates)for(var a=0;a<this.urlTemplates.length;a++)if(void 0!==this.urlTemplates[a].href){var d=this.urlTemplates[a].href,f=JSON.parse(atv.sessionStorage.getItem("mDialog-trackingData"));if(f){var e=void 0;for(e in f)d=d.replace("#{"+e+"}",encodeURIComponent(f[e]))}d=d.replace("${APPLICATION_KEY}",atv.sessionStorage.getItem("mDialog-appKey")).replace("${CACHE_BUST}",Math.random().toFixed(8).substr(2)).replace("${SDK}","javascript").replace("${SDK_VERSION}","3.1").replace("${DEVICE}","AppleTV").replace("${NETWORK}","wifi").replace("${OS_NAME}","AppleTV").replace("${OS_VERSION}",atv.device.softwareVersion).replace("${SESSION_ID}",m());r(d)}}}}function t(a){var b={api_key:atv.sessionStorage.getItem("mDialog-apiKey"),application_key:atv.sessionStorage.getItem("mDialog-appKey"),application_version:"3.1",os_version:atv.device.softwareVersion,os_name:"AppleTV",model:"AppleTV",application_session_unique_identifier:m(),sdk_version:"3.1",wifi:!0};0<atv.sessionStorage.getItem("mDialog-activityMonitorKey").length&&(b.stream_activity_key=atv.sessionStorage.getItem("mDialog-activityMonitorKey"));for(var d in a)b[d]=a[d];a="";for(var f in b)""!=a&&(a+="&"),a+=f+"="+b[f];return a}function n(a){function b(a){return{consumed:a.consumed,startTime:a.startTime,endTime:a.endTime,duration:a.duration,containsTime:function(a){return this.startTime<=a&&this.endTime>a},timeRemaining:function(a){return this.containsTime(a)?this.endTime-a:0}}}function d(a){var c=new XMLHttpRequest;c.onreadystatechange=function(){if(4==c.readyState&&200==c.status){h()&&console.log("fetchEvents eventsData="+c.responseText);var a=JSON.parse(c.responseText);atv.sessionStorage.setItem("mDialog-eventsData",JSON.stringify(a))}};c.open("GET",a);h()&&console.log("fetchEvents :"+a);c.send()}function f(){var e=[],c;for(c in a.eventsData){var g=a.eventsData[c].ad_break,d=parseInt(c,10);g&&(g=new b({consumed:-1!=a.consumedAdBreaks.indexOf(d),startTime:d,endTime:d+g.duration,duration:g.duration}),e.push(g))}return e}return{key:a.key,prerollPlayed:a.prerollPlayed,prerollEventsData:a.prerollEventsData,manifestURL:a.manifestURL,interstitialsURL:a.interstitialsURL,shouldTrackFeature:a.shouldTrackFeature,eventsData:a.eventsData,type:a.type,metadataURL:a.metadataURL,completeURLs:a.completeURLs,heartBeatURLs:a.heartBeatURLs,watchMetadataEvents:function(){var a=this.metadataURL;d(a);var c=atv.setInterval(function(g){"Stopped"==atv.sessionStorage.getItem("mDialog-playerState")?(h()&&console.log("didStopPlaying "+c),atv.clearInterval(c),c=null):d(a)},1E4)},watchHeartbeatURL:function(a,c){r(a);var g=atv.setInterval(function(c){if("Stopped"==atv.sessionStorage.getItem("mDialog-playerState"))atv.clearInterval(g),p=g=null;else{h()&&console.log("-- Heartbeat URL: "+a);var b=new XMLHttpRequest;b.onreadystatechange=function(){4!=b.readyState||404!=b.status&&410!=b.status||(h()&&console.log("=====timout failed call back here"),p&&(p(b.responseText),p=null),g&&(atv.clearInterval(g),g=null))};b.open("GET",a);b.send()}},c)},watchHeartBeat:function(){if(this.heartBeatURLs)for(i in this.heartBeatURLs)this.watchHeartbeatURL(this.heartBeatURLs[i].href,1E3*this.heartBeatURLs[i].interval)},getAdBreaks:f(),save:function(){atv.sessionStorage.setItem("mDialog-stream-"+a.key,JSON.stringify(a));h()&&console.log("saved "+JSON.stringify(a.key))},update:function(e,c){a[e]=c;this.save()},markPrerollPlayed:function(){this.update("prerollPlayed",!0)},streamTimeWithoutAds:function(a){a=Math.floor(a);for(var c=f(),b=0,d=0;d<c.length;d++)c[d].startTime<a&&c[d].endTime<=a?b+=c[d].duration:c[d].startTime<a&&c[d].endTime>a&&(b+=a-c[d].startTime);a-=b;return 0<=a?a:0},streamTimeWithAds:function(a){a=Math.floor(a);for(var c=f(),b=0;b<c.length;b++)c[b].startTime<=a&&(a+=c[b].duration);return a},adBreakForTime:function(a){for(var c=f(),b=0;b<c.length;b++){var d=c[b];if(d.containsTime(a))return d}},nearestPreviousAdBreak:function(a){a=Math.floor(a);var c=f();c.sort(function(a,b){return parseFloat(a.startTime)-parseFloat(b.startTime)});for(var b,d=0;d<c.length;d++)a>=c[d].startTime&&(b=c[d]);return b},startFeature:function(){h()&&console.log("-- Start Feature Content Tracking");(new l(a.startURLs)).fire();this.update("shouldTrackFeature",!0)},updatePlayerTime:function(a,b){if("live"==this.type&&null!=k){-1==k.startTime&&(k.startTime=a);var d=u-(a-k.startTime);0>d&&(k=null);k&&b(k,d)}},consumeAdBreak:function(b){a.consumedAdBreaks.push(b.startTime);this.save()}}}function q(a){return new n(JSON.parse(atv.sessionStorage.getItem("mDialog-stream-"+a)))}function w(a,b,d,f){var e={key:d.stream_key,type:d.type,assetKey:a,duration:d.duration,manifestURL:b.hd_manifest.href,metadataURL:b.timed_metadata_events.href,eventsURL:b.stream_time_events.href,interstitialsURL:b.interstitials?b.interstitials.href:null,shouldTrackFeature:!1,watchFrequency:10,consumedAdBreaks:[],startURLs:d.events?d.events.start.tracking:null,completeURLs:d.events?d.events.complete.tracking:null,heartBeatURLs:d.events?d.events.heartbeat?d.events.heartbeat.tracking:null:null};"live"==e.type&&d.pre_roll&&(e.prerollManifestURL=b.pre_roll_hd_manifest?b.pre_roll_hd_manifest.href:null,e.prerollEventsData=d.pre_roll.stream_time_events,e.prerollPlayed=!1);if("live"==e.type)a=new n(e),a.save(),f(a),a.watchMetadataEvents(),a.watchHeartBeat();else{var c=new XMLHttpRequest;c.onreadystatechange=function(){if(4==c.readyState&&200==c.status){e.eventsData=JSON.parse(c.responseText);var a=new n(e);h()&&console.log("Saving stream "+e.key);a.save();f(a);a.watchHeartBeat()}};c.open("GET",e.eventsURL);c.send()}}atv.sessionStorage.getItem("mDialog-activityMonitorKey");var u=-1,k=null,p=null;return{version:"3.1",debug:atv.sessionStorage.getItem("mDialog-debug"),init:function(a){atv.sessionStorage.setItem("mDialog-subdomain",a.subdomain);atv.sessionStorage.setItem("mDialog-apiKey",a.apiKey);atv.sessionStorage.setItem("mDialog-appKey",a.appKey);atv.sessionStorage.setItem("mDialog-debug",a.debug);atv.sessionStorage.setItem("mDialog-activityMonitorKey",a.activityMonitorKey||"");atv.sessionStorage["mDialog-debug"]&&(console.log("--"),console.log("-- mDialog Player Initialized v"+this.version),console.log("-- subdomain: "+atv.sessionStorage["mDialog-subdomain"]),console.log("-- apiKey: "+atv.sessionStorage["mDialog-apiKey"]),console.log("-- appKey: "+atv.sessionStorage["mDialog-appKey"]),console.log("-- sessionId: "+m()),console.log("--"))},loadCachedStream:q,loadStreamForKey:function(a,b,d,f,e){var c=new XMLHttpRequest,g=new XMLHttpRequest,k,l,m=t(),n=atv.sessionStorage.getItem("mDialog-apiKey");p=e;b&&(b.decisioningData&&(m=t(b.decisioningData)),b.trackingData&&atv.sessionStorage.setItem("mDialog-trackingData",JSON.stringify(b.trackingData)));c.open("POST",v(a));c.setRequestHeader("Authorization","mDialogAPI "+n);g.onreadystatechange=function(){4==g.readyState&&(200==g.status?(l=JSON.parse(g.responseText),w(a,k,l,d)):f(g.status,g.responseText))};c.onreadystatechange=function(){if(4==c.readyState)if(201==c.status){var a=c.getResponseHeader("Location");k=JSON.parse(c.responseText);h()&&(console.log("-- Created stream with location: "+a),k.stream_activity&&console.log("-- Stream Activity can be viewed at: "+k.stream_activity.href));g.open("GET",a);g.send()}else f(c.status,c.responseText)};c.send(m)},timeUpdate:function(a,b,d,f){a=q(a);if("live"==a.type)a.updatePlayerTime(b,function(a,b){d(a,b)});else{var e=a.eventsData,c=a.adBreakForTime(b),g=!0;typeof f===typeof Function&&(g=f(c));g&&(c&&(f=c.timeRemaining(b),d(c,f),1==f&&0==c.consumed&&a.consumeAdBreak(c)),e[b]&&(new l(e[b].tracking)).fire())}},trackEvent:function(a){(new l(a)).fire()},metadataUpdateEvent:function(a){if("TXXX"==a.key){var b=JSON.parse(atv.sessionStorage.getItem("mDialog-eventsData"));h()&&console.log("=onTimedMetadataChanged=="+a.stringValue);h()&&console.log("=onTimedMetadataChanged=="+JSON.stringify(b[a.stringValue]));b[a.stringValue]&&(b[a.stringValue].ad_break&&(k=b[a.stringValue].ad_break,k.startTime=-1,h()&&console.log("=onTimedMetadataChanged=="+JSON.stringify(b[a.stringValue].ad_break)),u=b[a.stringValue].ad_break.duration),mDialog.ATVPlayer.trackEvent(b[a.stringValue].tracking))}},playerStatusUpdate:function(a){atv.sessionStorage.setItem("mDialog-playerState",a)},didStopPlaying:function(a){a=q(a);h()&&console.log(a.completeURLs);(new l(a.completeURLs)).fire()}}}()};
// End mDialog SDK
// ***************************************************
// ***************************************************



/*
  When set to true, then atv.onAppEntry must load the 
  root URL; otherwise, root-url from bag.plist is used.
*/
var atvInitialized = atv.sessionStorage["atvInitialized"];
if (!atvInitialized) {
	console.log("atv events are not initialized, initialize now...");
	atv.sessionStorage["atvInitialized"] = "YES";
atv.config = {
	doesJavaScriptLoadRoot: true
};

var lastTime = -1;
var seeking = false;
var currentBreak = null;
var playerState;
var transportControlsVisible;
var isDebug = false;
atv.sessionStorage.setItem('mDialog-debug', isDebug);
/*
  Get or set mDialog-steamExitTimes
*/

function getStreamExitTimes() {
	var streamExitTimes = atv.sessionStorage["streamExitTimes"];
	if (!streamExitTimes) {
		streamExitTimes = JSON.stringify([]);
		atv.sessionStorage["streamExitTimes"] = streamExitTimes;
	}
	return JSON.parse(streamExitTimes);
};

/*
  Helper to find asset key within array
*/

function findAssetKey(array, assetKey) {
	for (var i = 0; i < array.length; i++) {
		if (array[i]['assetKey'] == assetKey) {
			return i
		}
	};
	return false
}

// ***************************************************
// Apple TV Player Callbacks with mDialog.ATVPlayer 

if (atv.player) {

	/*
    Initialize mDialog.ATVPlayer and 
    load main.xml on onAppEntry
  */

	atv.onAppEntry = function() {
		atv.loadURL("http://files.mdialog.com/appletv/sample-sdk-3.1/main.xml");
	}

	/*
    Call mDialog.ATVPlayer.didStopPlaying
    reset ad countdown, add, or update assets
    streamTimeWithoutAds 
  */

	atv.player.didStopPlaying = function() {

		var currentStreamKey = atv.sessionStorage.getItem("currentStream")

		if (currentStreamKey) {
			var currentStream = new mDialog.ATVPlayer.loadCachedStream(currentStreamKey);

			/*
        If this is a VOD stream, store the exit time without ads
        along with the asset id into sessionStorage.

        This is a basic, working example.
      */

			if (currentStream['type'] == 'vod') {

				var exitTime = lastTime;
				exitTime = currentStream.streamTimeWithoutAds(exitTime);
				var assetInfo = {
					assetKey: currentStream['assetKey'],
					streamExitTime: exitTime
				};

				var streamExitTimes = getStreamExitTimes();

				var assetIndex = findAssetKey(streamExitTimes, currentStream['assetKey']);

				if (assetIndex != undefined && !(assetIndex === false)) {
					streamExitTimes[assetIndex]['streamExitTime'] = exitTime;
				} else {
					streamExitTimes.push(assetInfo);
				}

				atv.sessionStorage.setItem("streamExitTimes", JSON.stringify(streamExitTimes));
			}
			// End of exit-time storage

			mDialog.ATVPlayer.didStopPlaying(currentStreamKey);

			if (TextViewController.getView("counter")) {
				TextViewController.removeView("counter");
			}
		}
	}

	/*
    Example of resuming playback at the point 
    it was previously exited
  */

	atv.player.willStartPlaying = function() {

		var currentStreamKey = atv.sessionStorage.getItem("currentStream")

		if (currentStreamKey) {
			var currentStream = new mDialog.ATVPlayer.loadCachedStream(currentStreamKey);
			if(isDebug) console.log("debug stream = " + JSON.stringify(currentStream));
			TextViewController.initiateView("counter");
			TextViewController.hideView("counter", 0);
			lastTime = -1;

			// currentStream.startFeature();

			/*
        Check our steamExitTimes, define logic to seek
        player to time based on asset id, utilize 
        streamTimeWithAds to determine seek point
      */

			var streamExitTimes = getStreamExitTimes();
			var assetIndex = findAssetKey(streamExitTimes, currentStream['assetKey']);

			if (assetIndex != undefined && !(assetIndex === false)) {
				var timeWithAds = streamExitTimes[assetIndex]['streamExitTime'];
				timeWithAds = currentStream.streamTimeWithAds(timeWithAds);
				atv.player.playerSeekToTime(timeWithAds); // example to resume playback
			}
			if (currentStream.type == "live") {
				if (isDebug) console.log("live starts");

		        currentStream.startFeature();
				//Notifies the player that it should process ID3 timed metadata. 
				//TODO, if follow the document pass in the array ["TXXX"], the metadata change event callback stops working. need find reason
				atv.player.observeTimedMetadataKeys();
				
			}
		}
	}
	/*
		This function is called when the player selects an audio track. 
		The language identifier of the audio track is passed as a parameter to the callback.
	*/ 
	atv.player.onTimedMetadataChanged = function(timedMetadata) {
		mDialog.ATVPlayer.metadataUpdateEvent(timedMetadata);
	}
	/*
    Manage player states to disable playerTimeDidChange
    during FastForwarding and Rewinding.  This callback is
    also triggered when switching from pre-roll to 
    feature content during linear broadcast so we markPrerollPlayed
  */

	atv.player.playerStateChanged = function(newState, timeIntervalSec) {

		playerState = newState;
		//save the player state, linear or vod will pick up the data from different context
		mDialog.ATVPlayer.playerStatusUpdate(playerState);
		switch (newState) {
			case atv.player.states.FastForwarding:
				seeking = true;
				break;
			case atv.player.states.Rewinding:
				seeking = true;
				break;
			case atv.player.states.Loading:

				/*
        Check nearestPreviousAdBreak on atv.player.states.Loading, 
        if the break has not been consumed, snapback to the 
        startTime of the Adbreak
      */
				var currentStreamKey = atv.sessionStorage.getItem("currentStream");
				if (currentStreamKey) {
					var currentStream = new mDialog.ATVPlayer.loadCachedStream(currentStreamKey);

					if (seeking == true) {
						var nearestPreviousAdBreak = currentStream.nearestPreviousAdBreak(timeIntervalSec);
						if (nearestPreviousAdBreak && !nearestPreviousAdBreak["consumed"]) {
							atv.player.playerSeekToTime(nearestPreviousAdBreak["startTime"]);
						}
					}
				}

			case atv.player.states.Playing:
				seeking = false;
				break;
		}

	};


	/*
    Call mDialog.ATVPlayer.timeUpdate once every second, if we're 
    returned and Ad Breack set duringBreak True, and  updated our
    Ad Countdown, if not in an AdBreak hide the Ad Countdown
  */

	atv.player.playerTimeDidChange = function(timeIntervalSec) {
		var currentStreamKey = atv.sessionStorage.getItem("currentStream")
		if (currentStreamKey) {
			var currentTime = Math.floor(timeIntervalSec);
			if (!seeking && currentTime !== lastTime && playerState == 'Playing') {
				lastTime = currentTime;
				currentBreak = null;
				var breakCallback = function(breakInfo, timeRemaining) {
						currentBreak = breakInfo;
						TextViewController.updateMessage("ADVERTISEMENT " + timeRemaining + "s REMAINING");
						if (currentBreak.startTime == currentTime && TextViewController.visibility() == "hidden") {
							TextViewController.showView("counter", 0);
							if (transportControlsVisible === false) {
								atv.setTimeout(function() {
									TextViewController.hideView("counter", 0)
								}, 4000);
							}
						}

					};
				var streamShouldProcessAdBreak = function(breakInfo) {
						// if (breakInfo && breakInfo['consumed']) {
						// 	atv.player.playerSeekToTime(breakInfo["endTime"] + 1);
						// 	return false;
						// }
						return true;
					};
				mDialog.ATVPlayer.timeUpdate(
					currentStreamKey,
					currentTime,
					breakCallback,
					streamShouldProcessAdBreak
				);

				if (currentBreak == null) {
					if (TextViewController.visibility() == "visible") {
						TextViewController.hideView("counter", 0)
					}
				}
			}
		}
	};


	/*
    Called to check if the given event should be 
    allowed considering current player time and state.
  */

	atv.player.playerShouldHandleEvent = function(event, timeIntervalSec) {
		var allowEvent = true;
		switch (event) {
			case atv.player.events.FFwd:
				if (currentBreak && !currentBreak['consumed']) {
					allowEvent = false
				};
				break;
			case atv.player.events.Rew:
				if (currentBreak && !currentBreak['consumed']) {
					allowEvent = false
				};
				break;
			case atv.player.events.SkipFwd:
				if (currentBreak && !currentBreak['consumed']) {
					allowEvent = false
				};
				break;
			case atv.player.events.SkipBack:
				if (currentBreak && !currentBreak['consumed']) {
					allowEvent = false
				};
				break;
		}
		return allowEvent;
	};

	/*
    atv.player.onTransportControlsDisplayed
    called when the transport control is going to be displayed
  */

	atv.player.onTransportControlsDisplayed = function(animationDuration) {
		transportControlsVisible = true;
		if (TextViewController.getView("counter") && currentBreak && playerState == 'Playing') {
			TextViewController.showView("counter", animationDuration);
		}
	}

	/*
    atv.player.onTransportControlsDisplayed
    called when the transport control is going to be hidden
  */

	atv.player.onTransportControlsHidden = function(animationDuration) {
		transportControlsVisible = false;
		if (TextViewController.getView("counter") && currentBreak && playerState == 'Playing') {
			TextViewController.hideView("counter", animationDuration);
		}
	}

};

// End Apple TV Player Callbacks
// ***************************************************


/*
  atv.Element extensions used within 
  atv.player.loadMoreAssets to select
  generated XML node
*/

if (atv.Element) {

	atv.Element.prototype.getElementsByTagName = function(tagName) {
		return this.ownerDocument.evaluateXPath("descendant::" + tagName, this);
	}

	atv.Element.prototype.getElementByTagName = function(tagName) {
		var elements = this.getElementsByTagName(tagName);
		if (elements && elements.length > 0) {
			return elements[0];
		}
		return undefined;
	}

}

var TextViewController = (function() {
	var __config = {},
		__views = {};

	function SetConfig(property, value) {
		if (property) {
			__config[property] = value;
		}
	}

	function GetConfig(property) {
		if (property) {
			return __config[property];
		} else {
			return false;
		}
	}

	function SaveView(name, value) {
		if (name) {
			__views[name] = value;
		}
	}

	function GetView(name) {
		if (name) {
			return __views[name];
		} else {
			return false;
		}
	}

	function RemoveView(name) {
		if (GetView(name)) {
			delete __views[name];
		}
	}

	function HideView(name, timeIntervalSec) {

		var animation = {
			"type": "BasicAnimation",
			"keyPath": "opacity",
			"fromValue": 1,
			"toValue": 0,
			"duration": timeIntervalSec,
			"removedOnCompletion": false,
			"fillMode": "forwards",
			"animationDidStop": function(finished) {
				if (isDebug) console.log("Animation did finish? " + finished);
			}
		},
			viewContainer = GetView(name);

		SetConfig("visibility", "hidden");

		if (isDebug) console.log("Hiding view " + name + " : " + typeof(viewContainer) + " <--- ");
		if (viewContainer) {
			viewContainer.addAnimation(animation, name);
		}
	}

	function ShowView(name, timeIntervalSec) {

		var animation = {
			"type": "BasicAnimation",
			"keyPath": "opacity",
			"fromValue": 0,
			"toValue": 1,
			"duration": timeIntervalSec,
			"removedOnCompletion": false,
			"fillMode": "forwards",
			"animationDidStop": function(finished) {
				if (isDebug) console.log("Animation did finish? " + finished);
			}
		},
			viewContainer = GetView(name);

		SetConfig("visibility", "visible");

		if (isDebug) console.log("Showing view " + name + " : " + typeof(viewContainer) + " <--- ");
		if (viewContainer) {
			viewContainer.addAnimation(animation, name);
		}
	}

	function UpdateMessage(message) {

		var messageView = GetConfig("messageView"),
			seconds = GetConfig("numberOfSeconds");

		if (messageView && message) {
			messageView.attributedString = {
				"string": message,
				"attributes": {
					"pointSize": 22.0,
					"color": {
						"red": 1,
						"blue": 1,
						"green": 1
					}
				}
			}
		}
	}

	function InitiateView(name) {
		var viewContainer = new atv.View(),
			message = new atv.TextView(),
			screenFrame = atv.device.screenFrame
			width = screenFrame.width,
			height = screenFrame.height * 0.07;

		if (isDebug) console.log("\nwidth: " + width + "\nheight: " + height + "\nscreenFrame: " + JSON.stringify(screenFrame));

		// Setup the View container.
		viewContainer.frame = {
			"x": screenFrame.x,
			"y": screenFrame.y + screenFrame.height - height,
			"width": width,
			"height": height
		}

		viewContainer.backgroundColor = {
			"red": 0.188,
			"blue": 0.188,
			"green": 0.188,
			"alpha": 0.7
		}

		viewContainer.alpha = 1;

		var topPadding = viewContainer.frame.height * 0.35,
			horizontalPadding = viewContainer.frame.width * 0.05;

		// Setup the message frame
		message.frame = {
			"x": horizontalPadding,
			"y": 0,
			"width": viewContainer.frame.width - (2 * horizontalPadding),
			"height": viewContainer.frame.height - topPadding
		};

		// Save the initial number of seconds as 0
		SetConfig("numberOfSeconds", 0);

		// Update the overlay message
		//var messageTimer = atv.setInterval( __updateMessage, 1000 );
		//SetConfig( "messageTimer", messageTimer )

		// Save the message to config
		SetConfig("messageView", message);
		SetConfig("animation", "complete");

		UpdateMessage();

		// Add the sub view
		viewContainer.subviews = [message];

		// Paint the view on Screen.
		if (isDebug) console.log("pushing the image view to screen: ");
		atv.player.overlay = viewContainer;

		if (isDebug) console.log("Saving view to " + name + " : " + typeof(viewContainer) + " <--- ");
		SaveView(name, viewContainer);
	}

	function Visibility() {
		return GetConfig("visibility")
	}

	return {
		"initiateView": InitiateView,
		"hideView": HideView,
		"showView": ShowView,
		"saveView": SaveView,
		"getView": GetView,
		"removeView": RemoveView,
		"setConfig": SetConfig,
		"getConfig": GetConfig,
		"updateMessage": UpdateMessage,
		"visibility": Visibility
	}
})();
} else {
	console.log("atv events are already initialized.");
}

