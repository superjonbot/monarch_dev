console.log('main.js loaded');


/*
  XML as string for httpLiveStreamingVideoAsset
*/

function drawPlayer(mediaURL, interstitialsURL) {
  
  var mediaURL = mediaURL;
  var interstitialsURL = interstitialsURL || ''
  
  var player = '<?xml version="1.0" encoding="UTF-8"?>\
    <atv>\
      <body>\
        <videoPlayer id="com.sample.video-player">\
          <httpLiveStreamingVideoAsset id="ABCD1234" indefiniteDuration="false">\
            <mediaURL><![CDATA[' + mediaURL + ']]></mediaURL>\
            <title>Video Content</title>\
            <description>Lorem ipsum dolor sit amet, consectetur adipisicing elit</description>\
            <eventGroup>'+interstitialsURL+'</eventGroup>\
          </httpLiveStreamingVideoAsset>\
        </videoPlayer>\
      </body>\
    </atv>';

  return atv.parseXML(player);
  
};

/*
  Wraper for mDialog.ATVPlayer.loadStreamForKey,
  passes streamContext, draws player and loads XML
*/

function loadStreamForKey(assetKey, streamContext) {
	var proxy = new atv.ProxyDocument();
	proxy.show();

	mDialog.ATVPlayer.loadStreamForKey(assetKey,
		streamContext,
		function(stream) {

			/*
        If the loadedStream has a pre-roll manifest url and the
        pre-roll has NOT yet played, set the manifestURL to the 
        pre-roll, else set manifestURL to the feature content
      */

			var manifestURL = (stream.prerollManifestURL && !(stream.prerollPlayed)) ? stream.prerollManifestURL : stream.manifestURL;
			var interstitialsURL = stream['interstitialsURL'];

			console.log("Caching stream " + stream.key);
			atv.sessionStorage.setItem('currentStream', stream.key);
			console.log("Cached");

			atv.loadAndSwapXML(drawPlayer(manifestURL, interstitialsURL));
		},
		function(code, message) {
			console.log("Failed to load stream! " + code + " " + message);
		},
		//Stream Expired callback
		function(message) {
			console.log("stream expired: " + message);
			//load home screen here
			atv.loadURL('http://files.mdialog.com/appletv/sample-sdk-3.1/main.xml');
		}
	)
};

