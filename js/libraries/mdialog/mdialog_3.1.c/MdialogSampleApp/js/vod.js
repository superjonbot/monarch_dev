console.log('vod.js loaded');

/*
  This typically would be done within
  atv.onAppEntry, since this application
  crosses two subdomains we have to 
  re-initialize the ATVPlayer object
*/

atv.onPageLoad = function(id) { 
  mDialog.ATVPlayer.init({
    debug: false,
	subdomain: 'vod-pv',
    apiKey: 'examplepoc',
    appKey: 'demo-app-atv',
  });
};
