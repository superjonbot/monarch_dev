/*
 Copyright 2011 Abdulla Abdurakhmanov
 Original sources are available at https://code.google.com/p/x2js/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

function X2JS() {
    var VERSION = "1.0.11";
    var escapeMode = false;

    var DOMNodeTypes = {
        ELEMENT_NODE 	   : 1,
        TEXT_NODE    	   : 3,
        CDATA_SECTION_NODE : 4,
        DOCUMENT_NODE 	   : 9
    };

    function getNodeLocalName( node ) {
        var nodeLocalName = node.localName;
        if(nodeLocalName == null) // Yeah, this is IE!!
            nodeLocalName = node.baseName;
        if(nodeLocalName == null || nodeLocalName=="") // =="" is IE too
            nodeLocalName = node.nodeName;
        return nodeLocalName;
    }

    function getNodePrefix(node) {
        return node.prefix;
    }

    function escapeXmlChars(str) {
        if(typeof(str) == "string")
            return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#x27;').replace(/\//g, '&#x2F;');
        else
            return str;
    }

    function unescapeXmlChars(str) {
        return str.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&#x27;/g, "'").replace(/&#x2F;/g, '\/')
    }

    function parseDOMChildren( node ) {
        if(node.nodeType == DOMNodeTypes.DOCUMENT_NODE) {
            var result = new Object;
            var child = node.firstChild;
            var childName = getNodeLocalName(child);
            result[childName] = parseDOMChildren(child);
            return result;
        }
        else
        if(node.nodeType == DOMNodeTypes.ELEMENT_NODE) {
            var result = new Object;
            result.__cnt=0;

            var nodeChildren = node.childNodes;

            // Children nodes
            for(var cidx=0; cidx <nodeChildren.length; cidx++) {
                var child = nodeChildren.item(cidx); // nodeChildren[cidx];
                var childName = getNodeLocalName(child);

                result.__cnt++;
                if(result[childName] == null) {
                    result[childName] = parseDOMChildren(child);
                    result[childName+"_asArray"] = new Array(1);
                    result[childName+"_asArray"][0] = result[childName];
                }
                else {
                    if(result[childName] != null) {
                        if( !(result[childName] instanceof Array)) {
                            var tmpObj = result[childName];
                            result[childName] = new Array();
                            result[childName][0] = tmpObj;

                            result[childName+"_asArray"] = result[childName];
                        }
                    }
                    var aridx = 0;
                    while(result[childName][aridx]!=null) aridx++;
                    (result[childName])[aridx] = parseDOMChildren(child);
                }
            }

            // Attributes
            for(var aidx=0; aidx <node.attributes.length; aidx++) {
                var attr = node.attributes.item(aidx); // [aidx];
                result.__cnt++;
                result["_"+attr.name]=attr.value;
            }

            // Node namespace prefix
            var nodePrefix = getNodePrefix(node);
            if(nodePrefix!=null && nodePrefix!="") {
                result.__cnt++;
                result.__prefix=nodePrefix;
            }

            if( result.__cnt == 1 && result["#text"]!=null  ) {
                result = result["#text"];
            }

            if(result["#text"]!=null) {
                result.__text = result["#text"];
                if(escapeMode)
                    result.__text = unescapeXmlChars(result.__text)
                delete result["#text"];
                delete result["#text_asArray"];
            }
            if(result["#cdata-section"]!=null) {
                result.__cdata = result["#cdata-section"];
                delete result["#cdata-section"];
                delete result["#cdata-section_asArray"];
            }

            if(result.__text!=null || result.__cdata!=null) {
                result.toString = function() {
                    return (this.__text!=null? this.__text:'')+( this.__cdata!=null ? this.__cdata:'');
                }
            }
            return result;
        }
        else
        if(node.nodeType == DOMNodeTypes.TEXT_NODE || node.nodeType == DOMNodeTypes.CDATA_SECTION_NODE) {
            return node.nodeValue;
        }
    }

    function startTag(jsonObj, element, attrList, closed) {
        var resultStr = "<"+ ( (jsonObj!=null && jsonObj.__prefix!=null)? (jsonObj.__prefix+":"):"") + element;
        if(attrList!=null) {
            for(var aidx = 0; aidx < attrList.length; aidx++) {
                var attrName = attrList[aidx];
                var attrVal = jsonObj[attrName];
                resultStr+=" "+attrName.substr(1)+"='"+attrVal+"'";
            }
        }
        if(!closed)
            resultStr+=">";
        else
            resultStr+="/>";
        return resultStr;
    }

    function endTag(jsonObj,elementName) {
        return "</"+ (jsonObj.__prefix!=null? (jsonObj.__prefix+":"):"")+elementName+">";
    }

    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    function jsonXmlSpecialElem ( jsonObj, jsonObjField ) {
        if(endsWith(jsonObjField.toString(),("_asArray"))
            || jsonObjField.toString().indexOf("_")==0
            || (jsonObj[jsonObjField] instanceof Function) )
            return true;
        else
            return false;
    }

    function jsonXmlElemCount ( jsonObj ) {
        var elementsCnt = 0;
        if(jsonObj instanceof Object ) {
            for( var it in jsonObj  ) {
                if(jsonXmlSpecialElem ( jsonObj, it) )
                    continue;
                elementsCnt++;
            }
        }
        return elementsCnt;
    }

    function parseJSONAttributes ( jsonObj ) {
        var attrList = [];
        if(jsonObj instanceof Object ) {
            for( var ait in jsonObj  ) {
                if(ait.toString().indexOf("__")== -1 && ait.toString().indexOf("_")==0) {
                    attrList.push(ait);
                }
            }
        }
        return attrList;
    }

    function parseJSONTextAttrs ( jsonTxtObj ) {
        var result ="";

        if(jsonTxtObj.__cdata!=null) {
            result+="<![CDATA["+jsonTxtObj.__cdata+"]]>";
        }

        if(jsonTxtObj.__text!=null) {
            if(escapeMode)
                result+=escapeXmlChars(jsonTxtObj.__text);
            else
                result+=jsonTxtObj.__text;
        }
        return result
    }

    function parseJSONTextObject ( jsonTxtObj ) {
        var result ="";

        if( jsonTxtObj instanceof Object ) {
            result+=parseJSONTextAttrs ( jsonTxtObj )
        }
        else
        if(jsonTxtObj!=null) {
            if(escapeMode)
                result+=escapeXmlChars(jsonTxtObj);
            else
                result+=jsonTxtObj;
        }

        return result;
    }

    function parseJSONArray ( jsonArrRoot, jsonArrObj, attrList ) {
        var result = "";
        if(jsonArrRoot.length == 0) {
            result+=startTag(jsonArrRoot, jsonArrObj, attrList, true);
        }
        else {
            for(var arIdx = 0; arIdx < jsonArrRoot.length; arIdx++) {
                result+=startTag(jsonArrRoot[arIdx], jsonArrObj, parseJSONAttributes(jsonArrRoot[arIdx]), false);
                result+=parseJSONObject(jsonArrRoot[arIdx]);
                result+=endTag(jsonArrRoot[arIdx],jsonArrObj);
            }
        }
        return result;
    }

    function parseJSONObject ( jsonObj ) {
        var result = "";

        var elementsCnt = jsonXmlElemCount ( jsonObj );

        if(elementsCnt > 0) {
            for( var it in jsonObj ) {

                if(jsonXmlSpecialElem ( jsonObj, it) )
                    continue;

                var subObj = jsonObj[it];

                var attrList = parseJSONAttributes( subObj )

                if(subObj == null || subObj == undefined) {
                    result+=startTag(subObj, it, attrList, true)
                }
                else
                if(subObj instanceof Object) {

                    if(subObj instanceof Array) {
                        result+=parseJSONArray( subObj, it, attrList )
                    }
                    else {
                        var subObjElementsCnt = jsonXmlElemCount ( subObj );
                        if(subObjElementsCnt > 0 || subObj.__text!=null || subObj.__cdata!=null) {
                            result+=startTag(subObj, it, attrList, false);
                            result+=parseJSONObject(subObj);
                            result+=endTag(subObj,it);
                        }
                        else {
                            result+=startTag(subObj, it, attrList, true);
                        }
                    }
                }
                else {
                    result+=startTag(subObj, it, attrList, false);
                    result+=parseJSONTextObject(subObj);
                    result+=endTag(subObj,it);
                }
            }
        }
        result+=parseJSONTextObject(jsonObj);

        return result;
    }

    this.parseXmlString = function(xmlDocStr) {
        var xmlDoc;
        if (window.DOMParser) {
            var parser=new window.DOMParser();
            xmlDoc = parser.parseFromString( xmlDocStr, "text/xml" );
        }
        else {
            // IE :(
            if(xmlDocStr.indexOf("<?")==0) {
                xmlDocStr = xmlDocStr.substr( xmlDocStr.indexOf("?>") + 2 );
            }
            xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async="false";
            xmlDoc.loadXML(xmlDocStr);
        }
        return xmlDoc;
    }

    this.xml2json = function (xmlDoc) {
        return parseDOMChildren ( xmlDoc );
    }

    this.xml_str2json = function (xmlDocStr) {
        var xmlDoc = this.parseXmlString(xmlDocStr);
        return this.xml2json(xmlDoc);
    }

    this.json2xml_str = function (jsonObj) {
        return parseJSONObject ( jsonObj );
    }

    this.json2xml = function (jsonObj) {
        var xmlDocStr = this.json2xml_str (jsonObj);
        return this.parseXmlString(xmlDocStr);
    }

    this.getVersion = function () {
        return VERSION;
    }

    this.escapeMode = function(enabled) {
        escapeMode = enabled;
    }
}

var x2js = new X2JS();



/**
 * HELPER LIBRARY : A library of functions to make your coding life awesome
 * Copyright (c) 2013 Jonathan Robles  http://superjonbot.com
 * Released under MIT license
 * library prerequisits : jquery, xml2json
 */

/**
 * Clean easy to disable/enable logging
 */
function trace(gettrace) {
    if (window.console) {
        console.log('trace: '+gettrace);
    }
};



//String.prototype.capitalize = function() {
//    return this.charAt(0).toUpperCase() + this.slice(1);
//}

/**
 * Places Json or JsonP into an object
 * @param options
 *  file:'data/defaultdata.json', //name of your son file
 *  format:'json',  // either json or jsonp
 *  DATA:undefined,  //container for retrieved data
 *  usenocache:true,  // adds nocache  for  no browser memory goodness
 *  uselegacyinject:false,
 *  callback:undefined,//function(options){alert(this.DATA.data.playerrevisioncheck)},  (***this is a function for json OR a string for a jsonP***)
 *
 * @constructor
 * variable = new JsonOBJ({file:'data/defaultdata.json',format:'jsonp',callback:'jsonpReturn'})
 */
var JsonOBJ=function(options){
    trace('JsonOBJ_START-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
    var defaults={
        file:'data/defaultdata.json',
        format:'json',
        DATA:undefined,
        usenocache:true,
        uselegacyinject:false,

        busy:true,
        callback:undefined,//function(options){alert(this.DATA.data.playerrevisioncheck)},  (***this is a function for json OR a string for a jsonP***)
        parent:this
    }

    if(options==undefined){var options=new Object()}
    defaults = $.extend({}, defaults, options);


    if(defaults.usenocache){
        var ADDnocacheCon='?';
        if(defaults.file.indexOf('?')!=-1){ADDnocacheCon='&'}
        var JSONtoUSE=defaults.file+ADDnocacheCon+"nocache=" + (Math.floor(Math.random() * 9999));
    } else {

        var JSONtoUSE=defaults.file;
    }



    var onjsondata = function(jsondata){
        trace("JSON READ SUCCESS!");
        defaults.busy=false; //busy to false
        defaults.DATA=jsondata; // place data
        if(defaults.callback!=undefined){
            trace('found callback function, executing!')
            defaults.callback(jsondata);
        } //run optional callback
        trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
    }


    var getjson = function() {


        if(defaults.format=='jsonp'){


            if(!defaults.uselegacyinject){

                if(defaults.callback!=undefined){

                    JSONtoUSE += "&jsonp="+defaults.callback

                }

                JSONtoUSE += "&=?";
                trace('trying to get '+defaults.format+' file:'+JSONtoUSE);


                $.ajaxSetup({
                    type: "POST",
                    data: {},
                    //dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
                });



                $.getJSON(JSONtoUSE, {format: 'jsonp'}).error(function(){trace("JSONP READ COMPLETE! (note, object data/busy are not used for jsonp files!)");trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=');})//,function(jsondata) {onjsondata(jsondata);}).error(function() { trace("ERROR!");trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=') });
                //alert(JSONtoUSE)

            }  else {


                //	alert('JSONtoUSE:'+JSONtoUSE)


                var ref_script = document.createElement("script");
                ref_script.setAttribute("src",JSONtoUSE);
                ref_script.setAttribute("type","text/javascript");
                document.body.appendChild(ref_script);




            }




        }
        else {

            trace('trying to get '+defaults.format+' file:'+JSONtoUSE);
            $.getJSON(JSONtoUSE, {format: 'json'},function(jsondata) {onjsondata(jsondata);}).error(function() { trace("ERROR!");trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=') });
        }



    }

    //init object


    if(defaults.file==undefined){trace('a json file must be provided')} else {	getjson();}
};



/*
 * Places XML into an object
 * @param options
 */
var XmlOBJ=function(options){
    trace('XmlOBJ_START-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
    var defaults={
        file:'data/defaultdata.xml',
        format:'text',
        DATAXML:undefined,
        DATA:undefined, //json format
        usenocache:true,
        uselegacyinject:false,
        busy:true,
        callback:function(){alert('hi')},
        parent:this
    }

    if(options==undefined){var options=new Object()}
    defaults = $.extend({}, defaults, options);
    trace('XmlOBJ : attempting to read '+defaults.file+' as '+defaults.format) ;
    $.ajax({

        xhrFields: {
            withCredentials: false
        },
        crossDomain: false,

        type: "GET",
        url: defaults.file,
        dataType: defaults.format,
        statusCode: {404: function() {alert('please check xml!');}},



        success: function(xml) {
            //alert(':)1')







            var gparse = new X2JS(); //XML object parsing
            defaults.DATAXML = gparse.parseXmlString(xml);
            defaults.DATA = gparse.xml2json( defaults.DATAXML );
            defaults.busy = false;

            if(typeof defaults.callback==='function'){defaults.callback(defaults.DATA)}
            trace('XmlOBJ : Success') ;
            trace('XmlOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')



        }
    });


}





