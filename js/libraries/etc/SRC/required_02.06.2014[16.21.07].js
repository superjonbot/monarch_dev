//Create Object.create shim
if(typeof Object.create !=='function'){
    Object.create = function (o){
        function F(){}
        F.prototype = o;
        return new F();
    };
}


/**
 * requestAnimationFrame and console polyfill
 */
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
            window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

(function() {
    if (!window.console) {
        window.console = {};
    }
    // union of Chrome, FF, IE, and Safari console methods
    var m = [
        "log", "info", "warn", "error", "debug", "trace", "dir", "group",
        "groupCollapsed", "groupEnd", "time", "timeEnd", "profile", "profileEnd",
        "dirxml", "assert", "count", "markTimeline", "timeStamp", "clear"
    ];
    // define undefined methods as noops to prevent errors
    for (var i = 0; i < m.length; i++) {
        if (!window.console[m[i]]) {
            window.console[m[i]] = function() {};
        }
    }
})();

//Mediator Object
_notify = function() {

    var debug = function() {
        // console.log or air.trace as desired
    };

    var components = {};

    var broadcast = function(event, args, source) {
        if (!event) {
            return;
        }
        args = args || [];
        //debug(["Mediator received", event, args].join(' '));
        for (var c in components) {
            if (typeof components[c]["on" + event] == "function") {
                try {
                    //debug("Mediator calling " + event + " on " + c);
                    source = source || components[c];
                    components[c]["on" + event].apply(source, args);
                } catch (err) {
                    debug(["Mediator error.", event, args, source, err].join(' '));
                }
            }
        }
    };

    var addComponent = function(name, component, replaceDuplicate) {
        if (name in components) {
            if (replaceDuplicate) {
                removeComponent(name);
            } else {
                throw new Error('The object: ' + name +' has already applied listeners');
            }
        }
        components[name] = component;
    };

    var removeComponent = function(name) {
        if (name in components) {
            delete components[name];
        }
    };

    var getComponent = function(name) {
        return components[name]; // undefined if component has not been added
    };

    var contains = function(name) {
        return (name in components);
    };

    return {
        name      : "Mediator",
        broadcast : broadcast,
        add       : addComponent,
        rem       : removeComponent,
        get       : getComponent,
        has       : contains
    };
}();

//Set default Listeners
_notify.add('global', function() {

        var tracecount = 0; 
        var alertcount = 0;

        return {
            onTrace: function(o) {

                tracecount++;
                var datastring=o.data;
                if(typeof datastring=='object'){datastring=JSON.stringify(datastring)}
                var buildstring='['+String(tracecount)+'] ('+ o.senderID+':'+ o.sendertype+':'+ o.notifyscope+') > '+ datastring;
                console.log(buildstring);
            },
            onAlert: function(o) {
                alertcount++;
                var datastring=o.data;
                if(typeof datastring=='object'){datastring=JSON.stringify(datastring)}
                var buildstring='['+String(tracecount)+'] ('+ o.senderID+':'+ o.sendertype+':'+ o.notifyscope+') > '+ datastring;
                alert(buildstring);
            },
            onInitialize:function(o){
                console.log('Created Instance #'+ o.senderID+' type:'+o.sendertype+' by '+ o.data.author+' [notifyscope:'+ o.notifyscope+']');
            },
            onOrientation:function(o){
                window._global$.addorientationtohtml(o);
               // alert('Created Instance #'+ o.senderID+' type:'+o.data+']');
            }

        }
    }());




//Begin Global Singleton
require(['modernizr','jquery','underscore'],function(Modernizr,$,_){


    function CorePlayerBase() {

        if ( arguments.callee._singletonInstance )
            return arguments.callee._singletonInstance;
        arguments.callee._singletonInstance = this;

        //_global$
        var parent=this;
        var debouncetimeout=250;


        this.orientation='notset';
        this.windowHeight=undefined;
        this.windowWidth=undefined;
        this.documentHeight=undefined;
        this.documentWidth=undefined;
        this.screenHeight=undefined;
        this.screenWidth=undefined;

        this.isMobile=Modernizr.mq('only screen and (min-device-width : 320px) and (max-device-width : 480px)');
        this.isTouch=Modernizr.touch;
        /**/

        this.host=location.host;
        //this.path=document.domain;
        //his.domain=

        this.addorientationtohtml=function(o){
           //alert('Created Instance #'+ o.senderID+' type:'+o.data+']');

            if(o.data=='portrait'){
                $('html').addClass('portrait');
                $('html').removeClass('landscape');
            }
            else{
                $('html').addClass('landscape');
                $('html').removeClass('portrait');
            }
        };


        this.setOrientation=function(){
            var candidate='portrait';
            this.setWindowHeight();
            this.setWindowWidth();
            if(this.windowWidth>this.windowHeight){candidate='landscape'};
            //alert('ran')
            this._isupdated('orientation',candidate,'Orientation');
            //alert(candidate)
            //if(candidate=='portrait'){$('#app-main').addclass('portrait')}
        };


        this.setWindowHeight=function(){this._isupdated('windowHeight',$(window).height(),'WindowHeight')};
        this.setWindowWidth=function(){this._isupdated('windowWidth',$(window).width(),'WindowWidth')};
        this.setDocumentHeight=function(){this._isupdated('documentHeight',$(document).height(),'DocumentHeight')};
        this.setDocumentWidth=function(){this._isupdated('documentWidth',$(document).width(),'DocumentWidth')};
        this.setScreenHeight=function(){this._isupdated('screenHeight',screen.height,'ScreenHeight')};
        this.setScreenWidth=function(){this._isupdated('screenWidth',screen.width,'ScreenWidth')};


        //global jsonp return redirector
        this.jsonpReturn=function(id,type,notifyscope){
            return function(data){
                _notify.broadcast(type, [{
                    senderID:id,
                    sendertype:type,
                    notifyscope:notifyscope,
                    data:data
                }]);






            }

        }


        //Utility Functions
        this._isupdated=function(value,currentVal,notify){
           if(parent[value]!=currentVal){
               var Oldvalue=parent[value];
               parent[value]=currentVal;

               //alert(currentVal)

               if(Oldvalue!=undefined){
                   this.notify(notify,parent[value]);
                    //alert(notify+' : '+parent[value])
               }


           }
       }; //for orientation/screen/document/window
        this.notify=function(type,data){
            _notify.broadcast(type, [{
                senderID:'global',
                sendertype:'global',
                notifyscope:'global',
                data:data
            }]);
        }; //notifier
        this.getQuery=function(variable){

            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    var returncandidate=pair[1];
                    if(returncandidate=='true'||returncandidate=='yes'||returncandidate=='y'||returncandidate=='t'){returncandidate=true;}
                    if(returncandidate=='false'||returncandidate=='no'||returncandidate=='n'||returncandidate=='f'){returncandidate=false;}
                    return  returncandidate;
                }
            }
            return false;

        };

        this.getHash=function(variable){

            return window.location.hash
        };


        this.setCookie=function(key, value) {
            var expires = new Date();
            expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
            document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        }

        this.getCookie=function(key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null;
        }








        $(window).on("resize",_.debounce(
            function(event){
                parent.setOrientation();
            }
            ,debouncetimeout));


    };

    window._global$=new CorePlayerBase();
    window._global$.setOrientation();
    window._global$.setDocumentHeight();
    window._global$.setDocumentWidth();
    window._global$.setScreenHeight();
    window._global$.setScreenWidth();
    //window._global$.addorientationtohtml('',window._global$.orientation);

})




//var a = new CorePlayerBase();
//var b = CorePlayerBase()
//alert( a === b ); // prints: true
//alert(CorePlayerBase.orientation)

//alert('pork chop :)')









//trythis('a','b','c')('d');