//BASE REQUIRED FILE FOR APPLE TV
var globaldebug=false;
var root=this;
//Create Object.create shim
if(typeof Object.create !=='function'){
    Object.create = function (o){
        function F(){}
        F.prototype = o;
        return new F();
    };
}
//Create window object when not available
if (typeof window === 'undefined') {
    this.window={};
}



/**
 * requestAnimationFrame and console polyfill
 */
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
            window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

/**
 * Global Helper Functions  :   trace/alert/deepExtend
 */
(function() {
    if (!window.console) {
        window.console = {};
    }
    // union of Chrome, FF, IE, and Safari console methods
    var m = [
        "log", "info", "warn", "error", "debug", "trace", "dir", "group",
        "groupCollapsed", "groupEnd", "time", "timeEnd", "profile", "profileEnd",
        "dirxml", "assert", "count", "markTimeline", "timeStamp", "clear"
    ];
    // define undefined methods as noops to prevent errors
    for (var i = 0; i < m.length; i++) {
        if (!window.console[m[i]]) {
            window.console[m[i]] = function() {};
        }
    }

    root.trace = function (o){
        console.log(o);
    };
    root.alert = function (o){
        console.log('[alert] '+o);
    };

    root.deepExtend = function(destination, source){
        for (var property in source) {
            if (source[property] && source[property].constructor &&
                source[property].constructor === Object) {
                destination[property] = destination[property] || {};
                arguments.callee(destination[property], source[property]);
            } else {
                destination[property] = source[property];
            }
        }
        return destination;
    };





})();
//Create atv object when not available for setInterval
if (typeof atv === 'undefined') {
    trace('NOT AppleTV');
    atv=this;
} else {trace('AppleTV')}

/**
 * Mediator Object
 */
_notify = function() {

    var debug = function() {
        // console.log or air.trace as desired
    };

    var components = {};

    var broadcast = function(event, args, source) {
        if (!event) {
            return;
        }
        args = args || [];
        //debug(["Mediator received", event, args].join(' '));
        for (var c in components) {
            if (typeof components[c]["on" + event] == "function") {
                try {
                    //debug("Mediator calling " + event + " on " + c);
                    source = source || components[c];
                    components[c]["on" + event].apply(source, args);
                } catch (err) {
                    debug(["Mediator error.", event, args, source, err].join(' '));
                }
            }
        }
    };

    var addComponent = function(name, component, replaceDuplicate) {
        if (name in components) {
            if (replaceDuplicate) {
                removeComponent(name);
            } else {
                throw new Error('The object: ' + name +' has already applied listeners');
            }
        }
        components[name] = component;
    };

    var removeComponent = function(name) {
        if (name in components) {
            delete components[name];
        }
    };

    var getComponent = function(name) {
        return components[name]; // undefined if component has not been added
    };

    var contains = function(name) {
        return (name in components);
    };

    return {
        name      : "Mediator",
        broadcast : broadcast,
        add       : addComponent,
        rem       : removeComponent,
        get       : getComponent,
        has       : contains
    };
}();

//Set default Listeners
_notify.add('global', function() {
    var tracecount = 0;
    var alertcount = 0;

    return {
        onTrace: function(o) {

            tracecount++;
            var datastring=o.data;
            if(typeof datastring=='object'){datastring=JSON.stringify(datastring)}
            var buildstring='['+String(tracecount)+'] ('+ o.senderID+':'+ o.sendertype+':'+ o.notifyscope+') > '+ datastring;
            //if(_global$.islocalhost)console.log(buildstring);

            if(globaldebug)trace('[frm] '+buildstring);
        },
        onAlert: function(o) {
            alertcount++;
            var datastring=o.data;
            if(typeof datastring=='object'){datastring=JSON.stringify(datastring)}
            var buildstring='['+String(tracecount)+'] ('+ o.senderID+':'+ o.sendertype+':'+ o.notifyscope+') > '+ datastring;
            if(globaldebug){'[frm] '+alert(buildstring)};
        },
        onInitialize:function(o){
            if(globaldebug)trace('[frm] Created Instance #'+ o.senderID+' type:'+o.sendertype+' by '+ o.data.author+' [notifyscope:'+ o.notifyscope+']');

        }

    }
}());




//Begin Global Singleton
require(['underscore'],function(_){


    function Monarch_Base() {

        if ( arguments.callee._singletonInstance )
            return arguments.callee._singletonInstance;
        arguments.callee._singletonInstance = this;

        //_global$
        var parent=this;
        var debouncetimeout=250;
        var scrolltimeout=undefined;

        this.lastscrollpositionX=0;
        this.lastscrollpositionY=0;
        this.orientation='notset';
        this.windowHeight=undefined;
        this.windowWidth=undefined;
        this.documentHeight=undefined;
        this.documentWidth=undefined;
        this.screenHeight=undefined;
        this.screenWidth=undefined;

        //this.isMobile=Modernizr.mq('only screen and (min-device-width : 320px) and (max-device-width : 480px)');
        //this.isTouch=Modernizr.touch;   //(equivalent to .touch)
        /**/

        this.host='';//location.host;
        this.path='';//location.pathname;
        this.islocalhost=false;
        if(   (this.host.indexOf('localhost')!=-1) || (this.host.indexOf('127.0.0.1')!=-1)   ){
            this.islocalhost=true;
           // $('html').addClass('localhost');
        }




        this.setOrientation=function(){
            var candidate='portrait';
            this.setWindowHeight();
            this.setWindowWidth();
            if(this.windowWidth>this.windowHeight){candidate='landscape'};
            this._isupdated('orientation',candidate,'Orientation');
        };


        this.setWindowHeight=function(){this._isupdated('windowHeight',10/*$(window).height()*/,'WindowHeight')};
        this.setWindowWidth=function(){this._isupdated('windowWidth',10/*$(window).width()*/,'WindowWidth')};
        this.setDocumentHeight=function(){this._isupdated('documentHeight',10/*$(document).height()*/,'DocumentHeight')};
        this.setDocumentWidth=function(){this._isupdated('documentWidth',10/*$(document).width()*/,'DocumentWidth')};
        this.setScreenHeight=function(){this._isupdated('screenHeight',10/*screen.height*/,'ScreenHeight')};
        this.setScreenWidth=function(){this._isupdated('screenWidth',10/*screen.width*/,'ScreenWidth')};



        //global jsonp return redirector
        this.jsonpReturn=function(id,type,notifyscope){
            return function(data){
                _notify.broadcast(type, [{
                    senderID:id,
                    sendertype:type,
                    notifyscope:notifyscope,
                    data:data
                }]);
            }
        }


        //Utility Functions
        this._isupdated=function(value,currentVal,notify){
            if(parent[value]!=currentVal){
                var Oldvalue=parent[value];
                parent[value]=currentVal;
                if(Oldvalue!=undefined){
                    this.notify(notify,parent[value]);
                }
            }
        }; //for orientation/screen/document/window
        this.notify=function(type,data){
            _notify.broadcast(type, [{
                senderID:'global',
                sendertype:'global',
                notifyscope:'global',
                data:data
            }]);
        }; //notifier

        this.setCookie=function(key, value) {
            var expires = new Date();
            expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
            document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        }

        this.getCookie=function(key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null;
        }





    };

    window._global$=new Monarch_Base();
    window._global$.setOrientation();
    window._global$.setDocumentHeight();
    window._global$.setDocumentWidth();
    window._global$.setScreenHeight();
    window._global$.setScreenWidth();


})


//console.log('ummmm duh!')

//var a = new Monarch_Base();
//var b = Monarch_Base()
//alert( a === b ); // prints: true
//alert(Monarch_Base.orientation)

//alert('pork chop :)')









//trythis('a','b','c')('d');