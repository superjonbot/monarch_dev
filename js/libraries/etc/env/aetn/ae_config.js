/* Brand Config */
AETN.brandID='ae';
AETN.brandID_omnitureName='A&E'; // for omniture, do not change!
AETN.streamActivityKey='64524e36b96b6f547b3e9dce101bf8bd';////mDialog/Freewheel
AETN.applicationKey='31b27a1ee073b27a5bfbf0787ebf44c0';//AETN.brandID+'_'+AETN.environment+AETN.streamActivityKey;//mDialog/Freewheel
//AETN.titleFeed='http://dev.mediaservice.aetndigital.com/SDK_v2/wa/show_titles';
AETN.titleFeedNew='http://dev-feeds.video.aetnd.com/api/aetv/videos?filter%5Bid%5D=';
//titlefeed old:

AETN.krux_brand='AETV_Chromecast'; //kcp_d/kcp_s values

//dev.mediaservice.aetndigital.com/SDK_v2/wa/show_titles/episode/ae?title_id=560042563529&deviceId=appletv
//titlefeed new:
//http://dev-feeds.video.aetnd.com/api/aetv/videos?filter%5Bid%5D=560042563529

//http://dev-feeds.video.aetnd.com/api/aetv/videos
//http://dev-feeds.video.aetnd.com/api/lifetime/videos
//http://dev-feeds.video.aetnd.com/api/history/videos
//http://dev-feeds.video.aetnd.com/api/fyi/videos

//Environmental Cases for this brand
switch (AETN.environment) {
    case 'dev':
        AETN.applicationID = "60EECA51";
        AETN.adobeReportSuiteID='aetnchromecastaedev';
        break;
    case 'qa':
        AETN.applicationID = "4E8A6DD8";
        AETN.adobeReportSuiteID='aetnchromecastaedev';
        break;
    default: //production
        AETN.applicationID = "1DECAEC8";
        AETN.streamActivityKey=undefined;
        AETN.adobeReportSuiteID='aetnchromecastae';
}

AETN.namespace='urn:x-cast:com.'+AETN.brandID+'_'+AETN.environment+'.chromecast';
//Adobe omniture






//AETN.appMeasurement= new AppMeasurement();
//AETN.appMeasurement.account=AETN.adobeReportSuiteID