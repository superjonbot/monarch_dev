/**
 * Copyright 2014 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Receiver / Player sample
 * <p>
 * This sample demonstrates how to build your own Receiver for use with Google
 * Cast. One of the goals of this sample is to be fully UX compliant.
 * </p>
 * <p>
 * A receiver is typically an HTML5 application with a html, css, and JavaScript
 * components. It demonstrates the following Cast Receiver API's:
 * </p>
 * <ul>
 * <li>CastReceiverManager</li>
 * <li>MediaManager</li>
 * <li>Media Player Library</li>
 * </ul>
 * <p>
 * It also demonstrates the following player functions:
 * </p>
 * <ul>
 * <li>Branding Screen</li>
 * <li>Playback Complete image</li>
 * <li>Limited Animation</li>
 * <li>Buffering Indicator</li>
 * <li>Seeking</li>
 * <li>Pause indicator</li>
 * <li>Loading Indicator</li>
 * </ul>
 *
 */

'use strict';


/**
 * Creates the namespace
 */
var AECustomPlayer = AECustomPlayer || {};

AECustomPlayer.sessionVars={};
AECustomPlayer.sessionVars.advertisementTemplate=$('.admedia_message').text();// store timer text
$('.admedia_message').text('');

/**
 * <p>
 * Cast player constructor - This does the following:
 * </p>
 * <ol>
 * <li>Bind a listener to visibilitychange</li>
 * <li>Set the default state</li>
 * <li>Bind event listeners for img & video tags<br />
 *  error, stalled, waiting, playing, pause, ended, timeupdate, seeking, &
 *  seeked</li>
 * <li>Find and remember the various elements</li>
 * <li>Create the MediaManager and bind to onLoad & onStop</li>
 * </ol>
 *
 * @param {!Element} element the element to attach the player
 * @struct
 * @constructor
 * @export
 */
AECustomPlayer.CastPlayer = function(element) {

  /**
   * The debug setting to control receiver, MPL and player logging.
   * @private {boolean}
   */
  this.debug_ = AECustomPlayer.DISABLE_DEBUG_;
  if (this.debug_) {
    cast.player.api.setLoggerLevel(cast.player.api.LoggerLevel.DEBUG);
    cast.receiver.logger.setLevelValue(cast.receiver.LoggerLevel.DEBUG);
  }

  /**
   * The DOM element the player is attached.
   * @private {!Element}
   */
  this.element_ = element;

  /**
   * The current type of the player.
   * @private {AECustomPlayer.Type}
   */
  this.type_;

  this.setType_(AECustomPlayer.Type.UNKNOWN, false);

  /**
   * The current state of the player.
   * @private {AECustomPlayer.State}
   */
  this.state_;

  /**
   * Timestamp when state transition happened last time.
   * @private {number}
   */
  this.lastStateTransitionTime_ = 0;

  this.setState_(AECustomPlayer.State.LAUNCHING, false);

  /**
   * The id returned by setInterval for the screen burn timer
   * @private {number|undefined}
   */
  this.burnInPreventionIntervalId_;

  /**
   * The id returned by setTimeout for the idle timer
   * @private {number|undefined}
   */
  this.idleTimerId_;

  /**
   * The id of timer to handle seeking UI.
   * @private {number|undefined}
   */
  this.seekingTimerId_;

  /**
   * The id of timer to defer setting state.
   * @private {number|undefined}
   */
  this.setStateDelayTimerId_;

  /**
   * Current application state.
   * @private {string|undefined}
   */
  this.currentApplicationState_;

  /**
   * The DOM element for the inner portion of the progress bar.
   * @private {!Element}
   */
  this.progressBarInnerElement_ = this.getElementByClass_(
      '.controls-progress-inner');

  /**
   * The DOM element for the thumb portion of the progress bar.
   * @private {!Element}
   */
  this.progressBarThumbElement_ = this.getElementByClass_(
      '.controls-progress-thumb');

  /**
   * The DOM element for the current time label.
   * @private {!Element}
   */
  this.curTimeElement_ = this.getElementByClass_('.controls-cur-time');

  /**
   * The DOM element for the total time label.
   * @private {!Element}
   */
  //this.totalTimeElement_ = this.getElementByClass_('.controls-total-time');

  /**
   * The DOM element for the preview time label.
   * @private {!Element}
   */
  this.previewModeTimerElement_ = undefined;//this.getElementByClass_('.preview-mode-timer-countdown');

  /**
   * Handler for buffering-related events for MediaElement.
   * @private {function()}
   */
  this.bufferingHandler_ = this.onBuffering_.bind(this);

  /**
   * Media player to play given manifest.
   * @private {cast.player.api.Player}
   */
  this.player_ = null;

  /**
   * Media player used to preload content.
   * @private {cast.player.api.Player}
   */
  this.preloadPlayer_ = null;

  /**
   * Text Tracks currently supported.
   * @private {?AECustomPlayer.TextTrackType}
   */
  this.textTrackType_ = null;

  /**
   * Whether player app should handle autoplay behavior.
   * @private {boolean}
   */
  this.playerAutoPlay_ = false;

  /**
   * Whether player app should display the preview mode UI.
   * @private {boolean}
   */
  this.displayPreviewMode_ = false;

  /**
   * Id of deferred play callback
   * @private {?number}
   */
  this.deferredPlayCallbackId_ = null;

  /**
   * Whether the player is ready to receive messages after a LOAD request.
   * @private {boolean}
   */
  this.playerReady_ = false;

  /**
   * Whether the player has received the metadata loaded event after a LOAD
   * request.
   * @private {boolean}
   */
  this.metadataLoaded_ = false;

  /**
   * The media element.
   * @private {HTMLMediaElement}
   */
  window.mediaElement = this.mediaElement_ = /** @type {HTMLMediaElement} */
      (this.element_.querySelector('.mainvideo'));

  window.adElement =  (this.element_.querySelector('.ad_element'));

  this.mediaElement_.addEventListener('error', this.onError_.bind(this), false);
  this.mediaElement_.addEventListener('playing', this.onPlaying_.bind(this),
      false);
  this.mediaElement_.addEventListener('pause', this.onPause_.bind(this), false);
  this.mediaElement_.addEventListener('ended', this.onEnded_.bind(this), false);
  this.mediaElement_.addEventListener('abort', this.onAbort_.bind(this), false);
  this.mediaElement_.addEventListener('timeupdate', this.onProgress_.bind(this),
      false);
  this.mediaElement_.addEventListener('seeking', this.onSeekStart_.bind(this),
      false);
  this.mediaElement_.addEventListener('seeked', this.onSeekEnd_.bind(this),
      false);


  /**
   * The cast receiver manager.
   * @private {!cast.receiver.CastReceiverManager}
   */
  this.receiverManager_ = cast.receiver.CastReceiverManager.getInstance();


  this.receiverManager_.onReady = this.onReady_.bind(this);
  this.receiverManager_.onSenderDisconnected =
      this.onSenderDisconnected_.bind(this);
  this.receiverManager_.onVisibilityChanged =
      this.onVisibilityChanged_.bind(this);
  this.receiverManager_.setApplicationState(
      AECustomPlayer.getApplicationState_());


  /**
   * The remote media object.
   * @private {cast.receiver.MediaManager}
   */
  window.mediaManager = this.mediaManager_ = new cast.receiver.MediaManager(this.mediaElement_);

  /**
   * The original load callback.
   * @private {?function(cast.receiver.MediaManager.Event)}
   */
  this.onLoadOrig_ =
      this.mediaManager_.onLoad.bind(this.mediaManager_);
  this.mediaManager_.onLoad = this.onLoad_.bind(this);

  /**
   * The original editTracksInfo callback
   * @private {?function(!cast.receiver.MediaManager.Event)}
   */
  this.onEditTracksInfoOrig_ =
      this.mediaManager_.onEditTracksInfo.bind(this.mediaManager_);
  this.mediaManager_.onEditTracksInfo = this.onEditTracksInfo_.bind(this);

  /**
   * The original metadataLoaded callback
   * @private {?function(!cast.receiver.MediaManager.LoadInfo)}
   */
  this.onMetadataLoadedOrig_ =
      this.mediaManager_.onMetadataLoaded.bind(this.mediaManager_);
  this.mediaManager_.onMetadataLoaded = this.onMetadataLoaded_.bind(this);

  /**
   * The original stop callback.
   * @private {?function(cast.receiver.MediaManager.Event)}
   */
  this.onStopOrig_ =
      this.mediaManager_.onStop.bind(this.mediaManager_);
  this.mediaManager_.onStop = this.onStop_.bind(this);

  /**
   * The original metadata error callback.
   * @private {?function(!cast.receiver.MediaManager.LoadInfo)}
   */
  this.onLoadMetadataErrorOrig_ =
      this.mediaManager_.onLoadMetadataError.bind(this.mediaManager_);
  this.mediaManager_.onLoadMetadataError = this.onLoadMetadataError_.bind(this);

  /**
   * The original error callback
   * @private {?function(!Object)}
   */
  this.onErrorOrig_ =
      this.mediaManager_.onError.bind(this.mediaManager_);
  this.mediaManager_.onError = this.onError_.bind(this);

  this.mediaManager_.customizedStatusCallback =
      this.customizedStatusCallback_.bind(this);

  this.mediaManager_.onPreload = this.onPreload_.bind(this);
  this.mediaManager_.onCancelPreload = this.onCancelPreload_.bind(this);
};


/**
 * The amount of time in a given state before the player goes idle.
 */
AECustomPlayer.IDLE_TIMEOUT = {
  LAUNCHING: 1000 * 60 * 2, //  minutes
  LOADING: 1000 * 60 * 5,  //  minutes
  PAUSED: 1000 * 60 * 60,  //  minutes
  DONE: 1000 * 60 * 60,     //  minutes
  IDLE: 1000 * 60 * 60      //  minutes
};


/**
 * Describes the type of media being played.
 *
 * @enum {string}
 */
AECustomPlayer.Type = {
  AUDIO: 'audio',
  VIDEO: 'video',
  UNKNOWN: 'unknown'
};


/**
 * Describes the type of captions being used.
 *
 * @enum {string}
 */
AECustomPlayer.TextTrackType = {
  SIDE_LOADED_TTML: 'ttml',
  SIDE_LOADED_VTT: 'vtt',
  SIDE_LOADED_UNSUPPORTED: 'unsupported',
  EMBEDDED: 'embedded'
};


/**
 * Describes the type of captions being used.
 *
 * @enum {string}
 */
AECustomPlayer.CaptionsMimeType = {
  TTML: 'application/ttml+xml',
  VTT: 'text/vtt'
};


/**
 * Describes the type of track.
 *
 * @enum {string}
 */
AECustomPlayer.TrackType = {
  AUDIO: 'audio',
  VIDEO: 'video',
  TEXT: 'text'
};


/**
 * Describes the state of the player.
 *
 * @enum {string}
 */
AECustomPlayer.State = {
  LAUNCHING: 'launching',
  LOADING: 'loading',
  BUFFERING: 'buffering',
  PLAYING: 'playing',
  PAUSED: 'paused',
  DONE: 'done',
  IDLE: 'idle'
};

/**
 * The amount of time (in ms) a screen should stay idle before burn in
 * prevention kicks in
 *
 * @type {number}
 */
AECustomPlayer.BURN_IN_TIMEOUT = 30 * 1000;

/**
 * The minimum duration (in ms) that media info is displayed.
 *
 * @const @private {number}
 */
AECustomPlayer.MEDIA_INFO_DURATION_ = 3 * 1000;


/**
 * Transition animation duration (in sec).
 *
 * @const @private {number}
 */
AECustomPlayer.TRANSITION_DURATION_ = 1.5;


/**
 * Const to enable debugging.
 *
 * @const @private {boolean}
 */
AECustomPlayer.ENABLE_DEBUG_ = true;


/**
 * Const to disable debugging.
 *
 * #@const @private {boolean}
 */
AECustomPlayer.DISABLE_DEBUG_ = false;


AECustomPlayer.roundTime=-1;
AECustomPlayer.sessionVars.media=undefined;
AECustomPlayer.AdDataCurrent = {
  'pausedForAd':false,
  'currentIndex':0/*,
  'totalVideoDuration': 1326*/
};
AECustomPlayer.AdDataSlots = [];



AECustomPlayer.CastPlayer.prototype.placeProgPoint=function(progIndex){
  this.killProgPoint(progIndex);



  var thisCuepoint=AECustomPlayer.sessionVars.media.metadata.adBreaks[progIndex].startTime;
  var totalDuration=this.mediaElement_.duration;//AECustomPlayer.AdDataCurrent.totalVideoDuration;

  //if(AECustomPlayer.sessionVars.media.streamSource=='mDialog'){
    var curDurationNoAd=Math.floor(AECustomPlayer.streamData.getStreamTimeWithoutAds(totalDuration));
    totalDuration=curDurationNoAd;
    var curCuePointNoAd=Math.floor(AECustomPlayer.streamData.getStreamTimeWithoutAds(thisCuepoint));
    thisCuepoint=curCuePointNoAd;
  //}


  var cuepointPlacement= Math.floor((thisCuepoint/totalDuration)*100);

  //don't let cuepoints go off the progressbar
  if(cuepointPlacement<1){cuepointPlacement=.5}; //don't go past the left
  if(cuepointPlacement>99.5){cuepointPlacement=99.5}; //don't go past the right

  $('.controls-progress').prepend('<div id="progPoint_'+progIndex+'" class="progPoint"></div>');
  $('#progPoint_'+progIndex).css('left',cuepointPlacement+'%');
}

AECustomPlayer.CastPlayer.prototype.killProgPoint=function(progIndex){
  $('#progPoint_'+progIndex).remove();
}

AECustomPlayer.CastPlayer.prototype.placeAllProgPoints=function(progIndex){




  var rawAdbreaks=AECustomPlayer.streamData.getAdBreaks();
  //trace('AECustomPlayer.streamData.getAdBreaks()='+JSON.stringify(rawAdbreaks),"#00f","#fff");
  var metadata = AECustomPlayer.sessionVars.media.metadata;
  metadata.adBreaks=[];
  _.each(rawAdbreaks,function(value, key, list){
    //console.log('hi'+value.startTime)
    metadata.adBreaks.push({
      //"url": "-",
      //"duration":value.duration,
      "startTime":value.startTime,
      //"isUnwatched":value.isUnwatched
      //"position":value.position,
      //"endTime":value.endTime
    });

  });

  //trace('AECustomPlayer.sessionVars.media.metadata.adBreaks= '+JSON.stringify(AECustomPlayer.sessionVars.media.metadata.adBreaks))






  var parent=this;
  trace('placeAllProgPoints!'+metadata.adBreaks.length,'#fff','#000');
  var adSlots=metadata.adBreaks;
  _.each(adSlots, function(value, key, list){
    //trace('placeAllProgPoints['+key+']!'+value['cuepoint'],'#fff','#000');
    parent.placeProgPoint(key);
  });
}

AECustomPlayer.CastPlayer.prototype.killAllProgPoints=function(progIndex){
/*  var parent=this;
  var adSlots=AECustomPlayer.sessionVars.media.metadata.adBreaks;//AECustomPlayer.AdDataSlots;
  _.each(adSlots, function(value, key, list){
    trace('killProgPoint['+key+']!'+value['cuepoint'],'#fff','#000');
    parent.killProgPoint(key);
  });*/

  trace('removing ALL cuepoints','#000','#ffff80');
  $('.progPoint').remove();


}










/**
 * Returns the element with the given class name
 *
 * @param {string} className The class name of the element to return.
 * @return {!Element}
 * @throws {Error} If given class cannot be found.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.getElementByClass_ = function(className) {
  var element = this.element_.querySelector(className);
  if (element) {
    return element;
  } else {
    throw Error('Cannot find element with class: ' + className);
  }
};


/**
 * Returns this player's media element.
 *
 * @return {HTMLMediaElement} The media element.
 * @export
 */
AECustomPlayer.CastPlayer.prototype.getMediaElement = function() {

  return this.mediaElement_;
};


/**
 * Returns this player's media manager.
 *
 * @return {cast.receiver.MediaManager} The media manager.
 * @export
 */
AECustomPlayer.CastPlayer.prototype.getMediaManager = function() {
  return this.mediaManager_;
};


/**
 * Returns this player's MPL player.
 *
 * @return {cast.player.api.Player} The current MPL player.
 * @export
 */
AECustomPlayer.CastPlayer.prototype.getPlayer = function() {
  return this.player_;
};


/**
 * Starts the player.
 *
 * @export
 */
AECustomPlayer.CastPlayer.prototype.start = function() {
  this.receiverManager_.start();
  window.playerObject=this;
};


/**
 * Preloads the given data.
 *
 * @param {!cast.receiver.media.MediaInformation} mediaInformation The
 *     asset media information.
 * @return {boolean} Whether the media can be preloaded.
 * @export
 */
AECustomPlayer.CastPlayer.prototype.preload = function(mediaInformation) {
  trace('preload');
  // For video formats that cannot be preloaded (mp4...), display preview UI.
  if (AECustomPlayer.canDisplayPreview_(mediaInformation || {})) {
    this.showPreviewMode_(mediaInformation);
    return true;
  }
  if (!AECustomPlayer.supportsPreload_(mediaInformation || {})) {
    this.log_('preload: no supportsPreload_');
    return false;
  }
  if (this.preloadPlayer_) {
    this.preloadPlayer_.unload();
    this.preloadPlayer_ = null;
  }
  // Only videos are supported for now
  var couldPreload = this.preloadVideo_(mediaInformation);
  if (couldPreload) {
    this.showPreviewMode_(mediaInformation);
  }
  this.log_('preload: couldPreload=' + couldPreload);
  return couldPreload;
};


/**
 * Display preview mode metadata.
 *
 * @param {boolean} show whether player is showing preview mode metadata
 * @export
 */
AECustomPlayer.CastPlayer.prototype.showPreviewModeMetadata = function(show) {
  //this.element_.setAttribute('preview-mode', show.toString());
};

/**
 * Show the preview mode UI.
 *
 * @param {!cast.receiver.media.MediaInformation} mediaInformation The
 *     asset media information.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.showPreviewMode_ = function(mediaInformation) {
  this.displayPreviewMode_ = true;
  this.loadPreviewModeMetadata_(mediaInformation);
  this.showPreviewModeMetadata(true);
};


/**
 * Hide the preview mode UI.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.hidePreviewMode_ = function() {
  this.showPreviewModeMetadata(false);
  this.displayPreviewMode_ = false;
};


/**
 * Preloads some video content.
 *
 * @param {!cast.receiver.media.MediaInformation} mediaInformation The
 *     asset media information.
 * @return {boolean} Whether the video can be preloaded.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.preloadVideo_ = function(mediaInformation) {
  this.log_('preloadVideo_');
  var self = this;
  var url = mediaInformation.contentId;
  var protocolFunc = AECustomPlayer.getProtocolFunction_(mediaInformation);
  if (!protocolFunc) {
    this.log_('No protocol found for preload');
    return false;
  }
  var host = new cast.player.api.Host({
    'url': url,
    'mediaElement': self.mediaElement_
  });
  host.onError = function() {
    self.preloadPlayer_.unload();
    self.preloadPlayer_ = null;
    self.showPreviewModeMetadata(false);
    self.displayPreviewMode_ = false;
    self.log_('Error during preload');
  };
  self.preloadPlayer_ = new cast.player.api.Player(host);
  self.preloadPlayer_.preload(protocolFunc(host));
  return true;
};

/**
 * Loads the given data.
 *
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load request info.
 * @export
 */
AECustomPlayer.CastPlayer.prototype.load = function(info) {

  var allSenders=window.playerObject.receiverManager_.getSenders()
  trace('.new load from '+info.senderId+' all senders:'+JSON.stringify(allSenders),'000','#afa');
  AECustomPlayer.sessionVars.lastSenderId=info.senderId;


  //trace('.load '+JSON.stringify(info));
  clearTimeout(this.idleTimerId_);
  var self = this;
  var media = info.message.media || {};
  var contentType = media.contentType;
  var playerType = AECustomPlayer.getType_(media);
  var isLiveStream = media.streamType === cast.receiver.media.StreamType.LIVE;
 /* if (!media.contentId) {
    trace('Load failed: no content');
    self.onLoadMetadataError_(info);
  } else*/
  if (playerType === AECustomPlayer.Type.UNKNOWN) {
    trace('Load failed: unknown content type: ' + contentType);
    self.onLoadMetadataError_(info);
  } else {
    trace('Loading: ' + playerType);
    self.resetMediaElement_();
    self.setType_(playerType, isLiveStream);
    var preloaded = false;
    switch (playerType) {
      case AECustomPlayer.Type.AUDIO:
        self.loadAudio_(info);
        break;
      case AECustomPlayer.Type.VIDEO:
        preloaded = self.loadVideo_(info);
        break;
    }
    self.playerReady_ = false;
    self.metadataLoaded_ = false;
    self.loadMetadata_(media);
    //trace('load_p1')
    self.showPreviewModeMetadata(false);
    self.displayPreviewMode_ = false;
    AECustomPlayer.preload_(media, function() {
      self.log_('preloaded=' + preloaded);
      if (preloaded) {
        // Data is ready to play so transiton directly to playing.
        self.setState_(AECustomPlayer.State.PLAYING, false);
        self.playerReady_ = true;
        self.maybeSendLoadCompleted_(info);
        // Don't display metadata again, since autoplay already did that.
        self.deferPlay_(0);
        self.playerAutoPlay_ = false;
        //trace('load_p2')
      } else {
        AECustomPlayer.transition_(self.element_, AECustomPlayer.TRANSITION_DURATION_, function() {
          self.setState_(AECustomPlayer.State.LOADING, false);
          // Only send load completed after we reach this point so the media
          // manager state is still loading and the sender can't send any PLAY
          // messages
          self.playerReady_ = true;
          self.maybeSendLoadCompleted_(info);
          if (self.playerAutoPlay_) {
            // Make sure media info is displayed long enough before playback
            // starts.
            self.deferPlay_(AECustomPlayer.MEDIA_INFO_DURATION_);
            self.playerAutoPlay_ = false;
          }
        });
      }
    });
  }

  //trace('load_p3')





};

/**
 * Sends the load complete message to the sender if the two necessary conditions
 * are met, the player is ready for messages and the loaded metadata event has
 * been received.
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load request info.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.maybeSendLoadCompleted_ = function(info) {
  if (!this.playerReady_) {
    this.log_('Deferring load response, player not ready');
  } else if (!this.metadataLoaded_) {
    this.log_('Deferring load response, loadedmetadata event not received');
  } else {
    this.onMetadataLoadedOrig_(info);
    this.log_('Sent load response, player is ready and metadata loaded');
  }
};

/**
 * Resets the media element.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.resetMediaElement_ = function() {
  this.log_('resetMediaElement_');
  if (this.player_) {
    this.player_.unload();
    this.player_ = null;
  }
  this.textTrackType_ = null;
};


/**
 * Loads the metadata for the given media.
 *
 * @param {!cast.receiver.media.MediaInformation} media The media.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.loadMetadata_ = function(media) {
  //AECustomPlayer.sessionVars['media']=media;
  trace('loadMetadata_');
  if (!AECustomPlayer.isCastForAudioDevice_()) {
    var metadata = AECustomPlayer.sessionVars.media.metadata;//?AECustomPlayer.sessionVars.media.metadata:{};//media.metadata || {};
    var titleElement = this.element_.querySelector('.media-title');

    var displayTitle=AECustomPlayer.sessionVars.media.rawFeedData.seriesName; //metadata.aetnTitle;

    if(AECustomPlayer.sessionVars.media.customData['isMovie']){displayTitle=AECustomPlayer.sessionVars.media.rawFeedData.title}
    AECustomPlayer.setInnerText_(titleElement, displayTitle);

    var subtitleElement = this.element_.querySelector('.media-subtitle');
    AECustomPlayer.setInnerText_(subtitleElement, metadata.subtitle);

    var subtitleElement2 = this.element_.querySelector('.media-subtitle2');
    AECustomPlayer.setInnerText_(subtitleElement2, metadata.subtitle2);

    trace('using metadata='+JSON.stringify(metadata));



/*

    this.killAllProgPoints();
    if(metadata.adBreaks!=undefined){
      trace('metadata.adBreaks:::'+metadata.adBreaks.length);
      AECustomPlayer.AdDataSlots =metadata.adBreaks;

    } else {
      AECustomPlayer.AdDataSlots =[];
      trace('no adbreaks');
    }


*/


    //var artwork = AECustomPlayer.getMediaImageUrl_(media);
    //if (artwork) {
    //  var artworkElement = this.element_.querySelector('.media-artwork');
    //  //AECustomPlayer.setBackgroundImage_(artworkElement, artwork);
    //}
  }
};


/**
 * Loads the metadata for the given preview mode media.
 *
 * @param {!cast.receiver.media.MediaInformation} media The media.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.loadPreviewModeMetadata_ = function(media) {
  //this.log_('loadPreviewModeMetadata_');
  //if (!AECustomPlayer.isCastForAudioDevice_()) {
  //  var metadata = media.metadata || {};
  //  var titleElement = this.element_.querySelector('.preview-mode-title');
  //  AECustomPlayer.setInnerText_(titleElement, metadata.title);
  //
  //  var subtitleElement = this.element_.querySelector('.preview-mode-subtitle');
  //  AECustomPlayer.setInnerText_(subtitleElement, metadata.subtitle);
  //
  //
  //
  //  var artwork = AECustomPlayer.getMediaImageUrl_(media);
  //  if (artwork) {
  //    var artworkElement = this.element_.querySelector('.preview-mode-artwork');
  //    AECustomPlayer.setBackgroundImage_(artworkElement, artwork);
  //  }
  //}
};


/**
 * Lets player handle autoplay, instead of depending on underlying
 * MediaElement to handle it. By this way, we can make sure that media playback
 * starts after loading screen is displayed.
 *
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load request info.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.letPlayerHandleAutoPlay_ = function(info) {
  this.log_('letPlayerHandleAutoPlay_: ' + info.message.autoplay);
  var autoplay = info.message.autoplay;
  info.message.autoplay = false;
  this.mediaElement_.autoplay = false;
  this.playerAutoPlay_ = autoplay == undefined ? true : autoplay;
};


/**
 * Loads some audio content.
 *
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load request info.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.loadAudio_ = function(info) {
  this.log_('loadAudio_');
  this.letPlayerHandleAutoPlay_(info);
  this.loadDefault_(info);
};


/**
 * Loads some video content.
 *
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load request info.
 * @return {boolean} Whether the media was preloaded
 * @private
 */
AECustomPlayer.CastPlayer.prototype.loadVideo_ = function(info) {
  trace('loadVideo_ info.message.media.contentId='+info.message.media.contentId);

  //trace('this.mediaElement_ :'+JSON.stringify(this.mediaElement_));
  //trace('this.mediaManager_ :'+JSON.stringify(this.mediaManager_));
//
//  var senderData = {
//    "debug": true,
//    "sessionContext":{
//        "subDomain": "aetn-vod",
//        "apiKey": "599b88193e3f5a6c96f7a362a75b34ad",
//        "applicationKey": "599b88193e3f5a6c96f7a362a75b34ad",
//        "streamActivityKey": "599b88193e3f5a6c96f7a362a75b34ad"
//    },
//    "streamContext":{
//      "assetKey":"630362691506",
//      "adDecisioningData": {
//        "adconfig": "staging"
//      },
//      "trackingData": {
//        "VISITOR_ID": "7ef53b8c2d219245002afea2fe0aa8e68b6b"
//      }
//
//    }/*,
//    "subDomain": "aetn-vod",
//    "assetKey":"630362691506",
//    "subdomain": "aetn-vod",
//    "apiKey": "599b88193e3f5a6c96f7a362a75b34ad",
//    "debug": true,
//    "activityMonitorKey": "64524e36b96b6f547b3e9dce101bf8bd",
//    "appKey": "47d6d65de637a1f71b71e4cbb74b7545",
//    "omniture_version": "event_tracking",
//    "client_side_omniture_support": false,
//    "REPORT_SUITE_ID": "aetnappletvfyidev,aetnwatchglobaldev",
//    "OMNITURE_CODE_VERSION": "BRS 0.7",
//    "PLATFORM": "AppleTV",
//    "OMNITURE_APP_ID": "AEWatchApp 2.0.4",
//    "OS_VERSION": "None",
//    "DEVICE_NAME": "Apple TV",
//    "RESOLUTION": "1920x1080",
//    "CARRIER_NAME": "",
//    "VISITOR_ID_NAME": "7ef53b8c2d219245002afea2fe0aa8e68b6b",
//    "VISITOR_ID": "7ef53b8c2d219245002afea2fe0aa8e68b6b",
//    "brand": "Lifetime"*/
//  };
//  var receiverData = {
//    'mediaElement': this.mediaElement_,
//    'mediaManager': this.mediaManager_ };
//
//
//
//  var streamManager = new mdialog.cast.api.StreamManager(senderData, receiverData);
//window.streamManager=streamManager;
//  trace('making mdialog streamManager');
//
//  var chromecastStream = streamManager.getStream("630362691506");
//  streamManager.onStreamLoaded=function(data){
//    //alert(':P');
//    //trace(':P'+data);
//    trace(':P'+JSON.stringify(data));
//
//window.theStream=data;


    //!!!
    //myReceiver._getSignedURL(window.theStream.manifestURL,function(ret){console.log(':)'+ret)},function(ret){':('+console.log(ret)})








    //getAdBreaks()
    //getMidRollStartTimes()
    //getAdBreakForTime(curre ntTime)​As Object
    //getStreamTimeWithAds(cu rrentTime)​As Integer
    //getStreamTimeWithoutAds (currentTime)​As Integer
    //getNearestPreviousAdBre akForTime(currentTime) As Object



/*    {
      "manifestURL": "http://link.theplatform.com/s/xc6n8B/V_RJy81rXqiX?switch=hls_mdialog&amp;mb…gh_video_s3&amp;md_sk=69038f95-eac1-43b9-9b80-66753c9b5f4a&amp;md_profile=",
        "hdManifestURL": "http://link.theplatform.com/s/xc6n8B/V_RJy81rXqiX?switch=hls_mdialog&amp;mb…video_s3&amp;md_sk=69038f95-eac1-43b9-9b80-66753c9b5f4a&amp;md_profile=hd_",
        "lowManifestURL": "http://link.theplatform.com/s/xc6n8B/V_RJy81rXqiX?switch=hls_mdialog&amp;mb…ideo_s3&amp;md_sk=69038f95-eac1-43b9-9b80-66753c9b5f4a&amp;md_profile=low_",
        "type": "vod",
        "duration": 3148
    }
    */

//
//expecting link like:
//    http://link.theplatform.com/s/xc6n8B/gFbGQ4E8C62g?switch=hls&assetTypes=medium_video_ak&mbr=true&metafile=false&sig=0056d879f398b86abab9100f241c1992b8852030725217b32b733363723374
//
//  };
//
//  window.chromecastStream=chromecastStream;
  //trace(':P adbreaks:'+chromecastStream.getAdBreaks());



  var self = this;
  var protocolFunc = null;

  var url = info.message.media.contentId;
  var protocolFunc = AECustomPlayer.getProtocolFunction_(info.message.media);
  var wasPreloaded = false;

  this.letPlayerHandleAutoPlay_(info);
  if (!protocolFunc) {
    this.log_('loadVideo_: using MediaElement');
    this.mediaElement_.addEventListener('stalled', this.bufferingHandler_,
        false);
    this.mediaElement_.addEventListener('waiting', this.bufferingHandler_,
        false);
  } else {
    this.log_('loadVideo_: using Media Player Library');
    // When MPL is used, buffering status should be detected by
    // getState()['underflow]'
    this.mediaElement_.removeEventListener('stalled', this.bufferingHandler_);
    this.mediaElement_.removeEventListener('waiting', this.bufferingHandler_);

    // If we have not preloaded or the content preloaded does not match the
    // content that needs to be loaded, perform a full load
    var loadErrorCallback = function() {
      // unload player and trigger error event on media element
      if (self.player_) {
        self.resetMediaElement_();
        self.mediaElement_.dispatchEvent(new Event('error'));
      }
    };
    if (!this.preloadPlayer_ || (this.preloadPlayer_.getHost &&
        this.preloadPlayer_.getHost().url != url)) {
      if (this.preloadPlayer_) {
        this.preloadPlayer_.unload();
        this.preloadPlayer_ = null;
      }
      trace('Regular video load');
      var host = new cast.player.api.Host({
        'url': url,
        'mediaElement': this.mediaElement_
      });
      host.onError = loadErrorCallback;
      this.player_ = new cast.player.api.Player(host);
      this.player_.load(protocolFunc(host));
    } else {
      trace('Preloaded video load');
      this.player_ = this.preloadPlayer_;
      this.preloadPlayer_ = null;
      // Replace the "preload" error callback with the "load" error callback
      this.player_.getHost().onError = loadErrorCallback;
      this.player_.load();
      wasPreloaded = true;
    }
  }
  this.loadMediaManagerInfo_(info, !!protocolFunc);
  return wasPreloaded;
};


/**
 * Loads media and tracks info into media manager.
 *
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load request info.
 * @param {boolean} loadOnlyTracksMetadata Only load the tracks metadata (if
 *     it is in the info provided).
 * @private
 */
AECustomPlayer.CastPlayer.prototype.loadMediaManagerInfo_ =
    function(info, loadOnlyTracksMetadata) {

  if (loadOnlyTracksMetadata) {
    // In the case of media that uses MPL we do not
    // use the media manager default onLoad API but we still need to load
    // the tracks metadata information into media manager (so tracks can be
    // managed and properly reported in the status messages) if they are
    // provided in the info object (side loaded).
    this.maybeLoadSideLoadedTracksMetadata_(info);
  } else {
    // Media supported by mediamanager, use the media manager default onLoad API
    // to load the media, tracks metadata and, if the tracks are vtt the media
    // manager will process the cues too.
    this.loadDefault_(info);
  }
};


/**
 * Sets the captions type based on the text tracks.
 *
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load request info.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.readSideLoadedTextTrackType_ =
    function(info) {
  if (!info.message || !info.message.media || !info.message.media.tracks) {
    return;
  }
  for (var i = 0; i < info.message.media.tracks.length; i++) {
    var oldTextTrackType = this.textTrackType_;
    if (info.message.media.tracks[i].type !=
        cast.receiver.media.TrackType.TEXT) {
      continue;
    }
    if (this.isTtmlTrack_(info.message.media.tracks[i])) {
      this.textTrackType_ =
          AECustomPlayer.TextTrackType.SIDE_LOADED_TTML;
    } else if (this.isVttTrack_(info.message.media.tracks[i])) {
      this.textTrackType_ =
          AECustomPlayer.TextTrackType.SIDE_LOADED_VTT;
    } else {
      this.log_('Unsupported side loaded text track types');
      this.textTrackType_ =
          AECustomPlayer.TextTrackType.SIDE_LOADED_UNSUPPORTED;
      break;
    }
    // We do not support text tracks with different caption types for a single
    // piece of content
    if (oldTextTrackType && oldTextTrackType != this.textTrackType_) {
      this.log_('Load has inconsistent text track types');
      this.textTrackType_ =
          AECustomPlayer.TextTrackType.SIDE_LOADED_UNSUPPORTED;
      break;
    }
  }
};


/**
 * If there is tracks information in the LoadInfo, it loads the side loaded
 * tracks information in the media manager without loading media.
 *
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load request info.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.maybeLoadSideLoadedTracksMetadata_ =
    function(info) {
  // If there are no tracks we will not load the tracks information here as
  // we are likely in a embedded captions scenario and the information will
  // be loaded in the onMetadataLoaded_ callback
  if (!info.message || !info.message.media || !info.message.media.tracks ||
      info.message.media.tracks.length == 0) {
    return;
  }
  var tracksInfo = /** @type {cast.receiver.media.TracksInfo} **/ ({
    tracks: info.message.media.tracks,
    activeTrackIds: info.message.activeTrackIds,
    textTrackStyle: info.message.media.textTrackStyle
  });
  this.mediaManager_.loadTracksInfo(tracksInfo);
};


/**
 * Loads embedded tracks information without loading media.
 * If there is embedded tracks information, it loads the tracks information
 * in the media manager without loading media.
 *
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load request info.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.maybeLoadEmbeddedTracksMetadata_ =
    function(info) {
  if (!info.message || !info.message.media) {
    return;
  }
  var tracksInfo = this.readInBandTracksInfo_();
  if (tracksInfo) {
    this.textTrackType_ = AECustomPlayer.TextTrackType.EMBEDDED;
    tracksInfo.textTrackStyle = info.message.media.textTrackStyle;
    this.mediaManager_.loadTracksInfo(tracksInfo);
  }
};


/**
 * Processes ttml tracks and enables the active ones.
 *
 * @param {!Array.<number>} activeTrackIds The active tracks.
 * @param {!Array.<cast.receiver.media.Track>} tracks The track definitions.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.processTtmlCues_ =
    function(activeTrackIds, tracks) {
  if (activeTrackIds.length == 0) {
    return;
  }
  // If there is an active text track, that is using ttml, apply it
  for (var i = 0; i < tracks.length; i++) {
    var contains = false;
    for (var j = 0; j < activeTrackIds.length; j++) {
      if (activeTrackIds[j] == tracks[i].trackId) {
        contains = true;
        break;
      }
    }
    if (!contains ||
        !this.isTtmlTrack_(tracks[i])) {
      continue;
    }
    if (!this.player_) {
      // We do not have a player, it means we need to create it to support
      // loading ttml captions
      var host = new cast.player.api.Host({
        'url': '',
        'mediaElement': this.mediaElement_
      });
      this.protocol_ = null;
      this.player_ = new cast.player.api.Player(host);
    }
    AECustomPlayer.sessionVars.media.customData.captions=true;
    this.player_.enableCaptions(
        true, cast.player.api.CaptionsType.TTML, tracks[i].trackContentId);
  }
};


/**
 * Checks if a track is TTML.
 *
 * @param {cast.receiver.media.Track} track The track.
 * @return {boolean} Whether the track is in TTML format.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.isTtmlTrack_ = function(track) {
  return this.isKnownTextTrack_(track,
      AECustomPlayer.TextTrackType.SIDE_LOADED_TTML,
      AECustomPlayer.CaptionsMimeType.TTML);
};


/**
 * Checks if a track is VTT.
 *
 * @param {cast.receiver.media.Track} track The track.
 * @return {boolean} Whether the track is in VTT format.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.isVttTrack_ = function(track) {
  return this.isKnownTextTrack_(track,
      AECustomPlayer.TextTrackType.SIDE_LOADED_VTT,
      AECustomPlayer.CaptionsMimeType.VTT);
};


/**
 * Checks if a track is of a known type by verifying the extension or mimeType.
 *
 * @param {cast.receiver.media.Track} track The track.
 * @param {!AECustomPlayer.TextTrackType} textTrackType The text track
 *     type expected.
 * @param {!string} mimeType The mimeType expected.
 * @return {boolean} Whether the track has the specified format.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.isKnownTextTrack_ =
    function(track, textTrackType, mimeType) {
  if (!track) {
    return false;
  }
  // The AECustomPlayer.TextTrackType values match the
  // file extensions required
  var fileExtension = textTrackType;
  var trackContentId = track.trackContentId;
  var trackContentType = track.trackContentType;
  if ((trackContentId &&
          AECustomPlayer.getExtension_(trackContentId) === fileExtension) ||
      (trackContentType && trackContentType.indexOf(mimeType) === 0)) {
    return true;
  }
  return false;
};


/**
 * Processes embedded tracks, if they exist.
 *
 * @param {!Array.<number>} activeTrackIds The active tracks.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.processInBandTracks_ =
    function(activeTrackIds) {
  var protocol = this.player_.getStreamingProtocol();
  var streamCount = protocol.getStreamCount();
  for (var i = 0; i < streamCount; i++) {
    var trackId = i + 1;
    var isActive = false;
    for (var j = 0; j < activeTrackIds.length; j++) {
      if (activeTrackIds[j] == trackId) {
        isActive = true;
        break;
      }
    }
    var wasActive = protocol.isStreamEnabled(i);
    if (isActive && !wasActive) {
      protocol.enableStream(i, true);
    } else if (!isActive && wasActive) {
      protocol.enableStream(i, false);
    }
  }
};


/**
 * Reads in-band tracks info, if they exist.
 *
 * @return {cast.receiver.media.TracksInfo} The tracks info.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.readInBandTracksInfo_ = function() {
  var protocol = this.player_ ? this.player_.getStreamingProtocol() : null;
  if (!protocol) {
    return null;
  }
  var streamCount = protocol.getStreamCount();
  var activeTrackIds = [];
  var tracks = [];
  for (var i = 0; i < streamCount; i++) {
    var trackId = i + 1;
    if (protocol.isStreamEnabled(i)) {
      activeTrackIds.push(trackId);
    }
    var streamInfo = protocol.getStreamInfo(i);
    var mimeType = streamInfo.mimeType;
    var track;
    if (mimeType.indexOf(AECustomPlayer.TrackType.TEXT) === 0 ||
        mimeType === AECustomPlayer.CaptionsMimeType.TTML) {
      track = new cast.receiver.media.Track(
          trackId, cast.receiver.media.TrackType.TEXT);
    } else if (mimeType.indexOf(AECustomPlayer.TrackType.VIDEO) === 0) {
      track = new cast.receiver.media.Track(
          trackId, cast.receiver.media.TrackType.VIDEO);
    } else if (mimeType.indexOf(AECustomPlayer.TrackType.AUDIO) === 0) {
      track = new cast.receiver.media.Track(
          trackId, cast.receiver.media.TrackType.AUDIO);
    }
    if (track) {
      track.name = streamInfo.name;
      track.language = streamInfo.language;
      track.trackContentType = streamInfo.mimeType;
      tracks.push(track);
    }
  }
  if (tracks.length === 0) {
    return null;
  }
  var tracksInfo = /** @type {cast.receiver.media.TracksInfo} **/ ({
    tracks: tracks,
    activeTrackIds: activeTrackIds
  });
  return tracksInfo;
};


/**
 * Loads some media by delegating to default media manager.
 *
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load request info.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.loadDefault_ = function(info) {
  this.onLoadOrig_(new cast.receiver.MediaManager.Event(
      cast.receiver.MediaManager.EventType.LOAD,
      /** @type {!cast.receiver.MediaManager.RequestData} */ (info.message),
      info.senderId));
};


/**
 * Sets the amount of time before the player is considered idle.
 *
 * @param {number} t the time in milliseconds before the player goes idle
 * @private
 */
AECustomPlayer.CastPlayer.prototype.setIdleTimeout_ = function(t) {
  this.log_('setIdleTimeout_: ' + t);
  var self = this;
  clearTimeout(this.idleTimerId_);
  if (t) {
    this.idleTimerId_ = setTimeout(function() {
      self.receiverManager_.stop();
    }, t);
  }
};


/**
 * Sets the type of player.
 *
 * @param {AECustomPlayer.Type} type The type of player.
 * @param {boolean} isLiveStream whether player is showing live content
 * @private
 */
AECustomPlayer.CastPlayer.prototype.setType_ = function(type, isLiveStream) {
  trace('setType_: ' + type);
  this.type_ = type;
  this.element_.setAttribute('type', type);
  this.element_.setAttribute('live', isLiveStream.toString());
  var overlay = this.getElementByClass_('.overlay');
  //var watermark = this.getElementByClass_('.watermark');
  clearInterval(this.burnInPreventionIntervalId_);
  if (type != AECustomPlayer.Type.AUDIO) {
    overlay.removeAttribute('style');
  } else {
    // if we are in 'audio' mode float metadata around the screen to
    // prevent screen burn
    this.burnInPreventionIntervalId_ = setInterval(function() {
      overlay.style.marginBottom = Math.round(Math.random() * 100) + 'px';
      overlay.style.marginLeft = Math.round(Math.random() * 600) + 'px';
    }, AECustomPlayer.BURN_IN_TIMEOUT);
  }
};


/**
 * Sets the state of the player.
 *
 * @param {AECustomPlayer.State} state the new state of the player
 * @param {boolean=} opt_crossfade true if should cross fade between states
 * @param {number=} opt_delay the amount of time (in ms) to wait
 * @private
 */
AECustomPlayer.CastPlayer.prototype.setState_ = function(
    state, opt_crossfade, opt_delay) {
  this.log_('setState_: state=' + state + ', crossfade=' + opt_crossfade +
      ', delay=' + opt_delay);
  var self = this;
  self.lastStateTransitionTime_ = Date.now();
  clearTimeout(self.delay_);
  if (opt_delay) {
    var func = function() { self.setState_(state, opt_crossfade); };
    self.delay_ = setTimeout(func, opt_delay);
  } else {
    if (!opt_crossfade) {
      self.state_ = state;
      self.element_.setAttribute('state', state);
      self.updateApplicationState_();
      self.setIdleTimeout_(AECustomPlayer.IDLE_TIMEOUT[state.toUpperCase()]);
    } else {
      var stateTransitionTime = self.lastStateTransitionTime_;
      AECustomPlayer.transition_(self.element_, AECustomPlayer.TRANSITION_DURATION_,
          function() {
            // In the case of a crossfade transition, the transition will be completed
            // even if setState is called during the transition.  We need to be sure
            // that the requested state is ignored as the latest setState call should
            // take precedence.
            if (stateTransitionTime < self.lastStateTransitionTime_) {
              self.log_('discarded obsolete deferred state(' + state + ').');
              return;
            }
            self.setState_(state, false);
          });
    }
  }
};


/**
 * Updates the application state if it has changed.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.updateApplicationState_ = function() {
  this.log_('updateApplicationState_');
  if (this.mediaManager_) {
    var idle = this.state_ === AECustomPlayer.State.IDLE;
    var media = idle ? null : this.mediaManager_.getMediaInformation();
    var applicationState = AECustomPlayer.getApplicationState_(media);
    if (this.currentApplicationState_ != applicationState) {
      this.currentApplicationState_ = applicationState;
      this.receiverManager_.setApplicationState(applicationState);
    }
  }
};


/**
 * Called when the player is ready. We initialize the UI for the launching
 * and idle screens.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onReady_ = function() {
  trace('onReady');
  this.setState_(AECustomPlayer.State.IDLE, false);
};


/**
 * Called when a sender disconnects from the app.
 *
 * @param {cast.receiver.CastReceiverManager.SenderDisconnectedEvent} event
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onSenderDisconnected_ = function(event) {
  trace('onSenderDisconnected');
  // When the last or only sender is connected to a receiver,
  // tapping Disconnect stops the app running on the receiver.
  if (this.receiverManager_.getSenders().length === 0 &&
      event.reason ===
          cast.receiver.system.DisconnectReason.REQUESTED_BY_SENDER) {
    this.receiverManager_.stop();
  }
};


/**
 * Called when media has an error. Transitions to IDLE state and
 * calls to the original media manager implementation.
 *
 * @see cast.receiver.MediaManager#onError
 * @param {!Object} error
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onError_ = function(error) {
  trace('onError');
  var self = this;
  AECustomPlayer.transition_(self.element_, AECustomPlayer.TRANSITION_DURATION_,
      function() {

        $('html').addClass('display-error');
        trace('ERROR please check manifest ','#fff','#f00','.error-message');
        //event.data.media.contentId


        self.setState_(AECustomPlayer.State.IDLE, true);
        self.onErrorOrig_(error);
      });
};


/**
 * Called when media is buffering. If we were previously playing,
 * transition to the BUFFERING state.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onBuffering_ = function() {
  trace('onBuffering[readyState=' + this.mediaElement_.readyState + ']');
  if (this.state_ === AECustomPlayer.State.PLAYING &&
      this.mediaElement_.readyState < HTMLMediaElement.HAVE_ENOUGH_DATA) {
    this.setState_(AECustomPlayer.State.BUFFERING, false);
  }
};


/**
 * Called when media has started playing. We transition to the
 * PLAYING state.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onPlaying_ = function() {
  trace('onPlaying');
  this.cancelDeferredPlay_('media is already playing');
  var isAudio = this.type_ == AECustomPlayer.Type.AUDIO;
  var isLoading = this.state_ == AECustomPlayer.State.LOADING;
  var crossfade = isLoading && !isAudio;
  this.setState_(AECustomPlayer.State.PLAYING, crossfade);
};


/**
 * Called when media has been paused. If this is an auto-pause as a result of
 * buffer underflow, we transition to BUFFERING state; otherwise, if the media
 * isn't done, we transition to the PAUSED state.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onPause_ = function() {
  trace('onPause');
  this.cancelDeferredPlay_('media is paused');
  var isIdle = this.state_ === AECustomPlayer.State.IDLE;
  var isDone = this.mediaElement_.currentTime === this.mediaElement_.duration;
  var isUnderflow = this.player_ && this.player_.getState()['underflow'];
  if (isUnderflow) {
    this.log_('isUnderflow');
    this.setState_(AECustomPlayer.State.BUFFERING, false);
    this.mediaManager_.broadcastStatus(/* includeMedia */ false);
  } else if (!isIdle && !isDone) {
    this.setState_(AECustomPlayer.State.PAUSED, false);
  }
  this.updateProgress_();
};


/**
 * Changes player state reported to sender, if necessary.
 * @param {!cast.receiver.media.MediaStatus} mediaStatus Media status that is
 *     supposed to go to sender.
 * @return {cast.receiver.media.MediaStatus} MediaStatus that will be sent to
 *     sender.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.customizedStatusCallback_ = function(
    mediaStatus) {
  this.log_('customizedStatusCallback_: playerState=' +
      mediaStatus.playerState + ', this.state_=' + this.state_);
  // TODO: remove this workaround once MediaManager detects buffering
  // immediately.
  if (mediaStatus.playerState === cast.receiver.media.PlayerState.PAUSED &&
      this.state_ === AECustomPlayer.State.BUFFERING) {
    mediaStatus.playerState = cast.receiver.media.PlayerState.BUFFERING;
  }
  return mediaStatus;
};


/**
 * Called when we receive a STOP message. We stop the media and transition
 * to the IDLE state.
 *
 * @param {cast.receiver.MediaManager.Event} event The stop event.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onStop_ = function(event) {
  trace('onStop');
  this.cancelDeferredPlay_('media is stopped');
  var self = this;
  AECustomPlayer.transition_(self.element_, AECustomPlayer.TRANSITION_DURATION_,
      function() {
        self.setState_(AECustomPlayer.State.IDLE, false);
        self.onStopOrig_(event);
      });
};


/**
 * Called when media has ended. We transition to the IDLE state.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onEnded_ = function() {
  trace('onEnded');
  this.setState_(AECustomPlayer.State.IDLE, true);
  this.hidePreviewMode_();
};


/**
 * Called when media has been aborted. We transition to the IDLE state.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onAbort_ = function() {
  trace('onAbort! please check media','#f00','#000');
  this.setState_(AECustomPlayer.State.IDLE, true);
  this.hidePreviewMode_();
};

AECustomPlayer.CastPlayer.prototype.chapterReport = function(compareTime) {

  var chapIndex=_.findIndex(AECustomPlayer.sessionVars.media.customData['chapters'], {startTime:compareTime})
  if(chapIndex>-1){  //add some already fire once logic

    //trace('chapterReport '+compareTime+' ('+chapIndex+')','#fff','#b3b300');
  }



}

AECustomPlayer.CastPlayer.prototype.timelineReport = function(compareTime) {
  var overlap=5;
  var getTrackIndex=_.findIndex(AECustomPlayer.sessionVars.media.tracking, function(trk_value){return (trk_value.time>=(compareTime-overlap)&&trk_value.time<=(compareTime+overlap))});
  if(getTrackIndex!=-1){


    if(!AECustomPlayer.sessionVars.media.tracking[getTrackIndex].tracked){

      var retrievedData=AECustomPlayer.sessionVars.media.tracking[getTrackIndex];
      trace('omniture reporting ['+getTrackIndex+']> data:'+JSON.stringify(retrievedData),'#fff','#00d');
      myReceiver._omnitureTrack(retrievedData.events,retrievedData.watch);
      AECustomPlayer.sessionVars.media.tracking[getTrackIndex].tracked=true;
    }


  };
  //trace('timelineReport='+compareTime+' ('+JSON.stringify(getTrackIndex)+')','#000','#ffa');
}

//AECustomPlayer.sessionVars.media.tracking



/**
 * Called periodically during playback, to notify changes in playback position.
 * We transition to PLAYING state, if we were in BUFFERING or LOADING state.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onProgress_ = function(data) {
  // if we were previously buffering, update state to playing
  var curTime = this.mediaElement_.currentTime;
  var curTimeRound = Math.round(this.mediaElement_.currentTime);

 //TIMELINE REPORTING!!!
  if(curTimeRound!=this.roundTime){
    this.timelineReport(curTimeRound);
    this.roundTime=curTimeRound;
  }

  AECustomPlayer.sessionVars.media.currentTime=curTimeRound;

  if(AECustomPlayer.sessionVars.media.streamSource=='mDialog'){
    var curDurationNoAd=Math.floor(AECustomPlayer.streamData.getStreamTimeWithoutAds(AECustomPlayer.sessionVars.media.duration));
    var streamTimeWithAds = AECustomPlayer.streamData.getStreamTimeWithAds(curTimeRound);
    var streamTimeWithoutAds = AECustomPlayer.streamData.getStreamTimeWithoutAds(curTimeRound);
    var adBreakForTime = AECustomPlayer.streamData.getAdBreakForTime(curTimeRound);
    var adTimeRemaining = undefined;
    var timeforChapterCompare=streamTimeWithoutAds;

    //trace('adBreakForTime='+adBreakForTime)

    if(adBreakForTime){   //if time is on an Ad


      AECustomPlayer.sessionVars.lastAdbreak=adBreakForTime;
      //adTextTemplate
      //trace()

      /*  {
       "position": "pre",
       "duration": 30,
       "startTime": 0,
       "endTime": 30,
       "isUnwatched": false
       }*/
      adTimeRemaining=(adBreakForTime.endTime)-curTimeRound;
      //adTimeRemaining=(adBreakForTime.endTime+streamTimeWithoutAds)-curTimeRound;

    }


    if(adTimeRemaining){
      $('.admedia').addClass('adDisplayed');
      var metaData=AECustomPlayer.sessionVars.media.metadata;//.title
      var adTextTemplate=AECustomPlayer.sessionVars['advertisementTemplate'];

      var displayTitle=AECustomPlayer.sessionVars.media.rawFeedData.seriesName; //metadata.aetnTitle;

      if(AECustomPlayer.sessionVars.media.customData['isMovie']){displayTitle=AECustomPlayer.sessionVars.media.rawFeedData.title}


      adTextTemplate = adTextTemplate.replace(/{{title}}/g, displayTitle); //add title
      var secondstoMinutesRemainder=Math.floor(adTimeRemaining%60);
      if (secondstoMinutesRemainder<10){secondstoMinutesRemainder='0'+secondstoMinutesRemainder}
      var secondstoMinutes = Math.floor(adTimeRemaining/60)+':'+secondstoMinutesRemainder;
      var adTextDisplay = adTextTemplate.replace(/{{adSecsLeft}}/g, secondstoMinutes);



      $('.admedia_message').text(adTextDisplay);
    }  else {
      $('.admedia').removeClass('adDisplayed');
    }


    trace((Math.round(streamTimeWithAds*100)/100),'#fff','#000','#streamTimeWithAds');
    trace((Math.round(streamTimeWithoutAds*100)/100),'#fff','#000','#streamTimeWithoutAds');
    trace(adTimeRemaining,'#fff','#000','#adTimeRemaining');
    //trace(adTimeRemaining,'#00f','#ff9');


    AECustomPlayer.sessionVars.media.streamTimeWithAds=streamTimeWithAds;
    AECustomPlayer.sessionVars.media.streamTimeWithoutAds=streamTimeWithoutAds;
    AECustomPlayer.sessionVars.media.adTimeRemaining=adTimeRemaining;
    //AECustomPlayer.sessionVars.media.durationNoAds=curDurationNoAd;

    //jump to the last Seek drop if exists then reset seekdrop value
    if(adTimeRemaining==undefined&&AECustomPlayer.sessionVars.media.lastSeekDrop!=undefined){
      trace('JUMP BACK to '+AECustomPlayer.sessionVars.media.lastSeekDrop)
      playerObject.mediaElement_.currentTime=AECustomPlayer.sessionVars.media.lastSeekDrop;
      trace('clearing lastDrop','#F5FF3A','#020');
      AECustomPlayer.sessionVars.media.lastSeekDrop=undefined;
    }

  } else {
    var timeforChapterCompare=Math.round(curTime);

    $('.admedia').removeClass('adDisplayed');
    trace('n/a','#fff','#000','#streamTimeWithAds');
    trace('n/a','#fff','#000','#streamTimeWithoutAds');
    trace('n/a','#fff','#000','#adTimeRemaining');
    AECustomPlayer.sessionVars.media.streamTimeWithAds=undefined;
    AECustomPlayer.sessionVars.media.streamTimeWithoutAds=undefined;
    AECustomPlayer.sessionVars.media.adTimeRemaining=undefined;
    //AECustomPlayer.sessionVars.media.durationNoAds=Math.round(AECustomPlayer.sessionVars.media.duration); // no ads, time is the same
  }




 this.chapterReport(timeforChapterCompare);
  //chapter reporting()
//mdialog compare time to AECustomPlayer.sessionVars.media.streamTimeWithoutAds
//not mdialog compare time Math.round(curTime)





if(isNaN(AECustomPlayer.sessionVars.media.duration)){
  trace('n/a','#fff','#000','#counter');
} else {
  trace(Math.round(curTime)+' / '+Math.round(AECustomPlayer.sessionVars.media.duration)+'('+AECustomPlayer.sessionVars.media.customData['durationNoAds']+')','#fff','#000','#counter');
}



  if (this.state_ === AECustomPlayer.State.BUFFERING ||
      this.state_ === AECustomPlayer.State.LOADING) {
    this.setState_(AECustomPlayer.State.PLAYING, false);
  }
  this.updateProgress_();

  //if no add is playing and the player isn't paused'
  //!AECustomPlayer.AdDataCurrent['pausedForAd']&&
  // if(!this.mediaElement_.paused){
  //   this.checkAgainstAds_();
  // };


};

/*

AECustomPlayer.CastPlayer.prototype.checkAgainstAds_ = function(){
  trace('checkAgainstAds_','#fff','#f00')
  var parent=this;
  var curTime = this.mediaElement_.currentTime;
  var pauseForAd=undefined;
  _.each(AECustomPlayer.AdDataSlots,function(value, key, list){
    if(!value['played']&&Number(value['cuepoint'])<= curTime){
         if(pauseForAd==undefined){pauseForAd=key};
      }
  });
  if(pauseForAd!=undefined){
    trace('play interstitial#'+pauseForAd+' now!','#f00','#fff');
    parent.startPauseForAd_(pauseForAd);
  }



}




AECustomPlayer.CastPlayer.prototype.startPauseForAd_ = function(getAdIndex){
  AECustomPlayer.AdDataCurrent['pausedForAd']=true;
  var pauseCuepoint=Number(AECustomPlayer.AdDataSlots[getAdIndex]['cuepoint']);
  trace('starting ad index#'+getAdIndex+' @'+pauseCuepoint,'#FFCC11','#292421');

  AECustomPlayer.AdDataCurrent['currentIndex']=getAdIndex;
  var parent=this;
  //parent.mediaElement_.pause();
  parent.mediaElement_.currentTime=pauseCuepoint;
  $('.admedia').addClass('adDisplayed');
  this.waitForAd_();
}

AECustomPlayer.CastPlayer.prototype.waitForAd_ = function(){
  var parent = this;
  var adIndex=AECustomPlayer.AdDataCurrent['currentIndex'];
  var adTextTemplate=AECustomPlayer.sessionVars['advertisementTemplate'];
  var metaData=AECustomPlayer.sessionVars.media.metadata;//.title
  var timerStart=AECustomPlayer.AdDataSlots[adIndex]['duration'];
  adTextTemplate = adTextTemplate.replace(/{{title}}/g, metaData.title.substring(0,80));

  //trace('adTextTemplate='+adTextTemplate)
  $('.media').hide();
  //$('.ad_element').attr('src','http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4');
  //window.adElement.play();

  //parent.mediaElement_.attr('src','http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4');

  var adInterval=setInterval(function(){

    if(timerStart==-1){
      $('.admedia_message').text('');
      clearInterval(adInterval);  //timer is done, Escape!
      parent.endPauseForAd_();
    } else {
      //trace('waiting for ad index#'+adIndex+' in '+timerStart,'#FFCC11','#292421');
      var adTextDisplay = adTextTemplate.replace(/{{adSecsLeft}}/g, timerStart);
      timerStart--;
      $('.admedia_message').text(adTextDisplay);

    }
  },1000);



};


AECustomPlayer.CastPlayer.prototype.endPauseForAd_ = function(){

  //window.adElement.pause();

  $('.media').show();

  var parent=this;
  var adIndex=AECustomPlayer.AdDataCurrent['currentIndex'];
  AECustomPlayer.AdDataSlots[adIndex]['played']=true;  //mark ad as played!!!
  $('#progPoint_'+adIndex).addClass('progPointSeen');  //show ad as seen!!!

  $('.admedia').removeClass('adDisplayed');
  AECustomPlayer.AdDataCurrent['pausedForAd']=false;
  var pauseCuepoint=Number(AECustomPlayer.AdDataSlots[adIndex]['cuepoint']);
  //parent.mediaElement_.play();
  parent.mediaElement_.currentTime=pauseCuepoint;



  trace('ending ad index#'+adIndex,'#FFCC11','#292421');


}

*/

/**
 * Updates the current time and progress bar elements.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.updateProgress_ = function() {
  // Update the time and the progress bar
  if (!AECustomPlayer.isCastForAudioDevice_()) {
    var curTime = this.mediaElement_.currentTime;
    var totalTime = this.mediaElement_.duration;


    if(AECustomPlayer.sessionVars.media.streamSource=='mDialog'){
      totalTime=Math.round(AECustomPlayer.streamData.getStreamTimeWithoutAds(totalTime));
      curTime=Math.round(AECustomPlayer.streamData.getStreamTimeWithoutAds(curTime));
    }



    if (!isNaN(curTime) && !isNaN(totalTime)) {
      var pct = 100 * (curTime / totalTime);
      this.curTimeElement_.innerText = AECustomPlayer.formatDuration_(curTime)+' / '+AECustomPlayer.formatDuration_(totalTime);


      //hack for adjust progress bar size
      var startAdjWidth= 115;
      var defaultTimelineProgBar = 275;
      var getCurtimeSize =  $('.controls-cur-time').width();
      if(getCurtimeSize>startAdjWidth){
        var newProgSize=defaultTimelineProgBar+(getCurtimeSize-startAdjWidth);
        $('.controls-progress').css('margin-right',newProgSize+'px');
      }
      //end hack


      //this.totalTimeElement_.innerText = AECustomPlayer.formatDuration_(totalTime);
      this.progressBarInnerElement_.style.width = pct + '%';
      this.progressBarThumbElement_.style.left = pct + '%';
      // Handle preview mode
      //if (this.displayPreviewMode_) {
      //  this.previewModeTimerElement_.innerText = "" + Math.round(totalTime-curTime);
      //}
    }
  }
};


/**
 * Callback called when user starts seeking
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onSeekStart_ = function(data) {
  trace('onSeekStart:'+JSON.stringify(data)+':::'+playerObject.mediaElement_.currentTime);

  if(AECustomPlayer.sessionVars.media.streamSource=='mDialog'){


  var previousAdBreak=AECustomPlayer.streamData.getNearestPreviousAdBreakForTime(playerObject.mediaElement_.currentTime);
  var isInTheAd=(playerObject.mediaElement_.currentTime>=previousAdBreak.startTime&&playerObject.mediaElement_.currentTime<=previousAdBreak.endTime)
  trace('previousAdBreak:'+JSON.stringify(previousAdBreak));




  if (previousAdBreak.isUnwatched&&!isInTheAd){

    trace('seeked to AD @'+previousAdBreak.startTime,'#F5FF3A','#6B0008');
    trace('setting lastDrop'+playerObject.mediaElement_.currentTime,'#F5FF3A','#020');
    AECustomPlayer.sessionVars.media.lastSeekDrop=playerObject.mediaElement_.currentTime;
    playerObject.mediaElement_.currentTime=previousAdBreak.startTime;

  }  else if (!isInTheAd) {
   // var shouldSetContinueTime=(playerObject.mediaElement_.currentTime==previousAdBreak.startTime);
    //trace('seeked to target @'+playerObject.mediaElement_.currentTime+' shouldSetContinueTime:'+shouldSetContinueTime,'#F5FF3A','#6B0008')
    //if (!shouldSetContinueTime){
    trace('clearing lastDrop','#F5FF3A','#020');
    AECustomPlayer.sessionVars.media.lastSeekDrop=undefined;//}
  }


}



  clearTimeout(this.seekingTimeoutId_);
  this.element_.classList.add('seeking');
};


/**
 * Callback called when user stops seeking.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onSeekEnd_ = function(data) {
  trace('onSeekEnd:'+JSON.stringify(data)+':::'+playerObject.mediaElement_.currentTime);
  clearTimeout(this.seekingTimeoutId_);
  this.seekingTimeoutId_ = AECustomPlayer.addClassWithTimeout_(this.element_,
      'seeking', 3000);
};


/**
 * Called when the player is added/removed from the screen because HDMI
 * input has changed. If we were playing but no longer visible, pause
 * the currently playing media.
 *
 * @see cast.receiver.CastReceiverManager#onVisibilityChanged
 * @param {!cast.receiver.CastReceiverManager.VisibilityChangedEvent} event
 *    Event fired when visibility of application is changed.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onVisibilityChanged_ = function(event) {
  this.log_('onVisibilityChanged');
  if (!event.isVisible) {
    this.mediaElement_.pause();
    this.mediaManager_.broadcastStatus(false);
  }
};


/**
 * Called when we receive a PRELOAD message.
 *
 * @see castplayer.CastPlayer#load
 * @param {cast.receiver.MediaManager.Event} event The load event.
 * @return {boolean} Whether the item can be preloaded.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onPreload_ = function(event) {
  trace('onPreload_');
  var loadRequestData =
      /** @type {!cast.receiver.MediaManager.LoadRequestData} */ (event.data);
  return this.preload(loadRequestData.media);
};


/**
 * Called when we receive a CANCEL_PRELOAD message.
 *
 * @see castplayer.CastPlayer#load
 * @param {cast.receiver.MediaManager.Event} event The load event.
 * @return {boolean} Whether the item can be preloaded.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onCancelPreload_ = function(event) {
  trace('onCancelPreload_');
  this.hidePreviewMode_();
  return true;
};


/**
 * Called when we receive a LOAD message. Calls load().
 *
 * @see AECustomPlayer#load
 * @param {cast.receiver.MediaManager.Event} event The load event.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.OLDonLoad_ = function(event) {
  trace('onLoad_'+JSON.stringify(event));
  this.cancelDeferredPlay_('new media is loaded');
  this.load(new cast.receiver.MediaManager.LoadInfo(
      /** @type {!cast.receiver.MediaManager.LoadRequestData} */ (event.data),
      event.senderId));
};




/*

,
"adBreaks": [{
  "url": "http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8",
  "duration":5,
  "cuepoint":0,
  "played":false
},{
  "url": "http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8",
  "duration":6,
  "cuepoint":300,
  "played":false
},{
  "url": "http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8",
  "duration":7,
  "cuepoint":420,
  "played":false
},{
  "url": "http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8",
  "duration":8,
  "cuepoint":572,
  "played":false
}]


*/





AECustomPlayer.CastPlayer.prototype.onLoad_ = function(event) {
  var parent=this;
  var mediaData=event;//{};
  var media=mediaData.data.media;
  this.killAllProgPoints();
  $('html').removeClass('display-error');

  AECustomPlayer.sessionVars.media=event.data.media;


  AECustomPlayer.sessionVars.media.contentId=AECustomPlayer.sessionVars.media.customData.platformID;
  AECustomPlayer.sessionVars.media.streamType='BUFFERED';
  AECustomPlayer.sessionVars.media.contentType= 'video/mp4';



  AECustomPlayer.sessionVars.media.metadata=AECustomPlayer.sessionVars.metadata||{"type": 0,"metadataType": 0};
  //AECustomPlayer.sessionVars.media.metadata

  //AECustomPlayer.sessionVars.media.customData={}
  //AECustomPlayer.sessionVars.media=event.data.media;
  trace('AECustomPlayer.sessionVars.media='+JSON.stringify(AECustomPlayer.sessionVars.media),'#fff','#00f');

  //console.log('!!!RECEIVED: '+JSON.stringify(AECustomPlayer.sessionVars.media));


  //console.log(' :+a: AECustomPlayer='+JSON.stringify(AECustomPlayer.sessionVars));

 // var appendData={'media':media};

  //AECustomPlayer.sessionVars= window.myReceiver.deepExtend(AECustomPlayer.sessionVars,appendData);

  //console.log(' :+b: AECustomPlayer='+JSON.stringify(AECustomPlayer.sessionVars));

  //console.log(AECustomPlayer.sessionVars.media.meta)
 // description: "Test Description"
 // images: Array[1]
 // subtitle: "S1 E1"
 // subtitle2: "Some show on dev"
 // title: "Series on his"








  this.cancelDeferredPlay_('new media is loaded');

  //make success callback
  var onURLcallback=function(event){



    var thisMetadata=AECustomPlayer.sessionVars.media.metadata?AECustomPlayer.sessionVars.media.metadata:{};


    if(AECustomPlayer.sessionVars.media.rawFeedData!=undefined){

      var feedData=AECustomPlayer.sessionVars.media.rawFeedData;

      feedData.title=$('<div/>').html(feedData.title).text();
      feedData.seriesName=$('<div/>').html(feedData.seriesName).text();

      //place values from feed
      thisMetadata.title=feedData.title;
      //thisMetadata.aetnTitle=feedData.seriesName;
     // thisMetadata.aetnImage=feedData.images.sizes['video-16x9'];
      thisMetadata.images=[{'url':feedData.images.sizes['video-16x9']}];

      //thisMetadata.images[0].url=;
      //thisMetadata.description=thisMetadata.aetnDescription=feedData.description;
      thisMetadata.description=feedData.description;
      if(!AECustomPlayer.sessionVars.media.customData){AECustomPlayer.sessionVars.media.customData={}}

      //AECustomPlayer.sessionVars.media.customData['userID']=undefined;//'unknown';
      //AECustomPlayer.sessionVars.media.customData['watchlistKey']=undefined;//'unknown';
      AECustomPlayer.sessionVars.media.customData.captions=false;
      //Info from the feed
      AECustomPlayer.sessionVars.media.customData['platformID']=feedData.id;

      //AECustomPlayer.sessionVars.media.customData['imageURL']=feedData.images.sizes['video-16x9'];
      AECustomPlayer.sessionVars.media.customData['isMovie']=(feedData.tvNtvMix=='Movie');
      AECustomPlayer.sessionVars.media.customData['category']=feedData.tvNtvMix;
      AECustomPlayer.sessionVars.media.customData['assetTitle']=feedData.title;
      AECustomPlayer.sessionVars.media.customData['showName']=feedData.seriesName;
      AECustomPlayer.sessionVars.media.customData['season']=feedData.tvSeasonNumber;
      AECustomPlayer.sessionVars.media.customData['episode']=feedData.tvSeasonEpisodeNumber;
      AECustomPlayer.sessionVars.media.customData['captionsAvailable']=feedData.isClosedCaption;
      AECustomPlayer.sessionVars.media.customData['durationNoAds']=feedData.duration;
      AECustomPlayer.sessionVars.media.customData['chapters']=feedData.chapters;

      var adobeToken=AECustomPlayer.sessionVars.media.customData.auth;
      AECustomPlayer.sessionVars.media.customData['isAuth']=(adobeToken==undefined||adobeToken=='')?0:1;

      thisMetadata.title=feedData.title;
      thisMetadata.seriesTitle=feedData.seriesName;
      thisMetadata.seasonNumber=feedData.tvSeasonNumber;
      thisMetadata.season=feedData.tvSeasonNumber;
      thisMetadata.episodeNumber=feedData.tvSeasonEpisodeNumber;
      thisMetadata.episode=feedData.tvSeasonEpisodeNumber;
      thisMetadata.metadataType=(AECustomPlayer.sessionVars.media.customData['isMovie'])?1:2;
      //thisMetadata.type=(AECustomPlayer.sessionVars.media.customData['isMovie'])?1:2;
      //AECustomPlayer.sessionVars.media.MetadataType=(AECustomPlayer.sessionVars.media.customData['isMovie'])?1:2;



      //console.log('!!!'+adobeToken+':::'+feedData.isBehindWall)
      //if{



      switch(feedData.tvNtvMix) {
        case "Show":

          thisMetadata.subtitle='S'+feedData.tvSeasonNumber+' E'+feedData.tvSeasonEpisodeNumber;
          thisMetadata.subtitle2=feedData.title;

          break;
        default:
        //do nothing
      }


      if(feedData.chapters!=undefined){

        //add chapterPointHit value
        _.each(AECustomPlayer.sessionVars.media.customData['chapters'], function(value,key){AECustomPlayer.sessionVars.media.customData['chapters'][key].chapterPointHit=false});

        trace('> '+AECustomPlayer.sessionVars.media.customData['chapters'].length+' CHAPTERS   DATA:'+JSON.stringify(AECustomPlayer.sessionVars.media.customData['chapters']),'#88f','#400');

      }else{
        trace('NO CHAPTERS IN FEED!','#00f','#c22');

      }


      trace('id:'+feedData.id,'#fff','#000','#contentID');
      trace(feedData.tvNtvMix+'('+thisMetadata.metadataType+')','#fff','#000','#contentType');

      var rawAdbreaks=AECustomPlayer.streamData.getAdBreaks();
      trace('chap:'+(feedData.chapters?feedData.chapters.length:0)+'/ads:'+(rawAdbreaks?rawAdbreaks.length:0),'#fff','#000','#contentChapters');


      //make tracking array
      AECustomPlayer.sessionVars.media.tracking=[];
      //duration: 30
      //endTime: 30
      //isUnwatched: true
      //position: "pre"
      //startTime: 0
      var trackTimeBuffer=6;
      var lastChapter=undefined;
      var useLongForm=AECustomPlayer.sessionVars.media.rawFeedData.isLongForm;

      _.each(rawAdbreaks,function(value, key, list){
        //console.log('hi'+value.startTime)

        if(value.position!='pre'){
          AECustomPlayer.sessionVars.media.tracking.push({
            'type':'omniture',
            'details': 'chapter#'+key+' END',//***CHAPTER END***//
            'time': value.startTime-trackTimeBuffer,
            'events':'26',
            'tracked':false,
            'watch':{
              videocalltype:'ContentComplete',
              videochapter:'Chapter '+key
            }
          });
        }
        AECustomPlayer.sessionVars.media.tracking.push({
          'type':'omniture',
          'details': 'ad#'+key+' start ('+value.position+')',//*** AD START ***//
          'time': value.startTime,
          'events':'30',
          'tracked':false,
          'watch':{videocalltype:'Ad'}
        });
        AECustomPlayer.sessionVars.media.tracking.push({
          'type':'omniture',
          'details': 'ad#'+key+' end ('+value.position+')',//*** AD END ***//
          'time': value.endTime,
          'events':'31',
          'tracked':false,
          'watch':{videocalltype:'Ad'}
        });
        if(value.position!='post'){
          var useEvents='21,22';
          if(key==0&&useLongForm){useEvents='21,22,27'};
          lastChapter=(key+1);
          AECustomPlayer.sessionVars.media.tracking.push({
            'type':'omniture',
            'details': 'chapter#'+lastChapter+' START',//***CHAPTER START***//
            'time': value.endTime+trackTimeBuffer,
            'events':useEvents,
            'tracked':false,
            'watch':{
              videocalltype:'ContentView',
              videochapter:'Chapter '+(key+1)
            }
          });
        }


      });


      if(useLongForm){
        var ninetyEghtPercentTime=Math.round(AECustomPlayer.streamData.duration/100*98);
        AECustomPlayer.sessionVars.media.tracking.push({
          'type': 'Episode Complete',//***CHAPTER START***//
          'time': ninetyEghtPercentTime,
          'events':'28',
          'tracked':false,
          'watch':{
            videocalltype:'EpisodeComplete',
            videochapter:'Chapter '+lastChapter
          }
        });
      }




      AECustomPlayer.sessionVars.media.tracking = _.sortBy( AECustomPlayer.sessionVars.media.tracking, 'time');



    } /* else {

      //mirror stuff
      if(thisMetadata.aetnTitle){thisMetadata.title=thisMetadata.aetnTitle}else{if(thisMetadata.title!=undefined){thisMetadata.aetnTitle=thisMetadata.title}};
      if(thisMetadata.aetnDescription){thisMetadata.description=thisMetadata.aetnDescription}else{if(thisMetadata.description!=undefined){thisMetadata.aetnDescription=thisMetadata.description}};
      if(thisMetadata.aetnSubtitle){thisMetadata.subtitle=thisMetadata.aetnSubtitle}else{if(thisMetadata.subtitle!=undefined){thisMetadata.aetnSubtitle=thisMetadata.subtitle}};
      if(thisMetadata.aetnSubtitle2){thisMetadata.subtitle2=thisMetadata.aetnSubtitle2}else{if(thisMetadata.subtitle2!=undefined){thisMetadata.aetnSubtitle2=thisMetadata.subtitle2}};

      console.log('***** '+thisMetadata.aetnImages);
      if(thisMetadata.aetnImages&&(thisMetadata.aetnImages!='undefined')){
        thisMetadata.images=thisMetadata.aetnImages
      }else{
        if(thisMetadata.images!=undefined&&(thisMetadata.aetnImages!='undefined')){
          thisMetadata.aetnImages=thisMetadata.images
        }
      };
      if(thisMetadata.aetnThumb){thisMetadata.thumb=thisMetadata.aetnThumb}else{if(thisMetadata.thumb!=undefined){thisMetadata.aetnThumb=thisMetadata.thumb}};



    } */











    event.data.media=AECustomPlayer.sessionVars.media;






    if(AECustomPlayer.sessionVars.media.customData['hideConsole']=='true'){$('.debugConsole').css('display','none');}
   if(AECustomPlayer.sessionVars.media.customData['stickyTimeline']=='true'){$('html').addClass('forcetimeline');}

    //case 'forceTimeline':
    //trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#020','#000');
    //$('html').addClass('forcetimeline');
    //break;
    //case 'hideConsole'://"command":"someSwitch","data":"true"
    //trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#020','#000');
    //if(String(returnData.data)=='true'){
    //
    //}  else {
    //  $('.debugConsole').css('display','block');
    //}
    //break;


    //isBehindWall
    var adobeToken=AECustomPlayer.sessionVars.media.customData.auth;
    //console.log('!!!'+adobeToken+':::'+feedData.isBehindWall)
    if(feedData.isBehindWall&&(adobeToken==undefined||adobeToken=='')){
      $('html').addClass('display-error');
      trace('ERROR no AUTH token provided for isBehindWall video','#fff','#f00','.error-message');
    } else {
      trace('ATTEMPTING MANIFEST AT: '+event.data.media.contentId,'#f00','#fff');
      parent.load(new cast.receiver.MediaManager.LoadInfo(
          /** @type {!cast.receiver.MediaManager.LoadRequestData} */ (event.data),
          event.senderId));
    }



  }


  //console.log('A. mediaData.data.media.metadata.trackingData='+mediaData.data.media.metadata.trackingData);
  //console.log('B. mediaData.data.media.metadata.trackingData='+JSON.stringify(mediaData.data.media.metadata.trackingData));



  //console.log('C. AECustomPlayer.sessionVars='+AECustomPlayer.sessionVars);

  //console.log('D. AECustomPlayer.sessionVars='+JSON.stringify(AECustomPlayer.sessionVars));



  //AECustomPlayer.sessionVars['media']=media;

var doOnNoPlatformID=function(errorMessage){
  //URL (provided URL)
  trace('platformID NOT DEFINED / tokenized URL Failed','#fff','#f00');
  $('html').addClass('display-error');
  trace('ERROR '+errorMessage,'#fff','#f00','.error-message');
  trace('URL','#fff','#000','#streamSource');
  AECustomPlayer.sessionVars.media.streamSource='URL';
  //onURLcallback(event);
}

var doForMPX=function(){
  //MPX

  myReceiver._getMPXURL({"assetKey": mediaData.data.media.customData.platformID}, function(getHLSURL) {
        myReceiver._getSignedURL(getHLSURL, function(getTokenizedURL) {
          console.log('getTokenizedURL:' + getTokenizedURL);
          mediaData.data.media.contentId=getTokenizedURL;
          onURLcallback(mediaData);
        }, doOnNoPlatformID)
      },doOnNoPlatformID
  );
}





var doForMDIALOG=function(){
  //mDialog
  myReceiver._getMDialogURL({"assetKey": mediaData.data.media.customData.platformID}, function(getHLSURL) {
        myReceiver._getSignedURL(getHLSURL, function(getTokenizedURL) {
          console.log('getTokenizedURL:' + getTokenizedURL);
          mediaData.data.media.contentId=getTokenizedURL;
          onURLcallback(mediaData);
        }, doOnNoPlatformID)
      },doOnNoPlatformID
  );
}



  try {
    trace(':P is there a platformID? '+mediaData.data.media.customData.platformID);
    if(mediaData.data.media.customData.platformID!=undefined){
      myReceiver._getFeedData({"assetKey": mediaData.data.media.customData.platformID},function(getFeedData){


        AECustomPlayer.sessionVars.media.rawFeedData=getFeedData;
        trace('getFeedData: '+JSON.stringify(getFeedData),'#0f0','#777');



        if(mediaData.data.media.customData.forceMPX=='true'){
          doForMPX();
        }else{
          doForMDIALOG();
        }
      },doOnNoPlatformID);




    }else{
      doOnNoPlatformID();
    }








  }
  catch(err) {
    trace('onLoad_ received an error trying to check for platformID','#f00');
    onURLcallback(event);
  }


};






/**
 * Called when we receive a EDIT_TRACKS_INFO message.
 *
 * @param {!cast.receiver.MediaManager.Event} event The editTracksInfo event.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onEditTracksInfo_ = function(event) {
  this.log_('onEditTracksInfo');
  this.onEditTracksInfoOrig_(event);

  // If the captions are embedded or ttml we need to enable/disable tracks
  // as needed (vtt is processed by the media manager)
  if (!event.data || !event.data.activeTrackIds || !this.textTrackType_) {
    return;
  }
  var mediaInformation = this.mediaManager_.getMediaInformation() || {};
  var type = this.textTrackType_;
  if (type == AECustomPlayer.TextTrackType.SIDE_LOADED_TTML) {
    // The player_ may not have been created yet if the type of media did
    // not require MPL. It will be lazily created in processTtmlCues_
    if (this.player_) {
      AECustomPlayer.sessionVars.media.customData.captions=false;
      this.player_.enableCaptions(false, cast.player.api.CaptionsType.TTML);
    }
    this.processTtmlCues_(event.data.activeTrackIds,
        mediaInformation.tracks || []);
  } else if (type == AECustomPlayer.TextTrackType.EMBEDDED) {
    AECustomPlayer.sessionVars.media.customData.captions=false;
    this.player_.enableCaptions(false);
    this.processInBandTracks_(event.data.activeTrackIds);
    AECustomPlayer.sessionVars.media.customData.captions=true;
    this.player_.enableCaptions(true);
  }
};


//AECustomPlayer.CastPlayer.prototype.enableCaptions = function(info) {
//  this.player_.enableCaptions(true);
//}


/**
 * Called when metadata is loaded, at this point we have the tracks information
 * if we need to provision embedded captions.
 *
 * @param {!cast.receiver.MediaManager.LoadInfo} info The load information.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onMetadataLoaded_ = function(info) {
  trace('onMetadataLoaded');

/*  AECustomPlayer.AdDataSlots = [{
    'url': 'http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8',
    'duration':5,
    'cuepoint':0,
    'played':false
  },{
    'url': 'http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8',
    'duration':6,
    'cuepoint':300,
    'played':false
  },{
    'url': 'http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8',
    'duration':7,
    'cuepoint':420,
    'played':false
  },{
    'url': 'http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8',
    'duration':8,
    'cuepoint':572,
    'played':false
  }];*/

  if(AECustomPlayer.sessionVars.media.streamSource=='mDialog'){
    this.placeAllProgPoints();   // put the ad spots where belong

    //trace('feed:'+(feedData.chapters?feedData.chapters.length:0)+'/mdialog:'+AECustomPlayer.sessionVars.media.metadata.adBreaks.length,'#fff','#000','#contentChapters');

  }







  this.onLoadSuccess_();
  // In the case of ttml and embedded captions we need to load the cues using
  // MPL.
  this.readSideLoadedTextTrackType_(info);

  if (this.textTrackType_ ==
      AECustomPlayer.TextTrackType.SIDE_LOADED_TTML &&
      info.message && info.message.activeTrackIds && info.message.media &&
      info.message.media.tracks) {
    this.processTtmlCues_(
        info.message.activeTrackIds, info.message.media.tracks);
  } else if (!this.textTrackType_) {
    // If we do not have a textTrackType, check if the tracks are embedded
    this.maybeLoadEmbeddedTracksMetadata_(info);
  }
  // Only send load completed when we have completed the player LOADING state
  this.metadataLoaded_ = true;
  this.maybeSendLoadCompleted_(info);
};


/**
 * Called when the media could not be successfully loaded. Transitions to
 * IDLE state and calls the original media manager implementation.
 *
 * @see cast.receiver.MediaManager#onLoadMetadataError
 * @param {!cast.receiver.MediaManager.LoadInfo} event The data
 *     associated with a LOAD event.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onLoadMetadataError_ = function(event) {
  this.log_('onLoadMetadataError_');
  var self = this;
  AECustomPlayer.transition_(self.element_, AECustomPlayer.TRANSITION_DURATION_,
      function() {
        self.setState_(AECustomPlayer.State.IDLE, true);
        self.onLoadMetadataErrorOrig_(event);
      });
};


/**
 * Cancels deferred playback.
 *
 * @param {string} cancelReason
 * @private
 */
AECustomPlayer.CastPlayer.prototype.cancelDeferredPlay_ = function(cancelReason) {
  if (this.deferredPlayCallbackId_) {
    trace('Cancelled deferred playback: ' + cancelReason);
    clearTimeout(this.deferredPlayCallbackId_);
    this.deferredPlayCallbackId_ = null;
  }
};


/**
 * Defers playback start by given timeout.
 *
 * @param {number} timeout In msec.
 * @private
 */
AECustomPlayer.CastPlayer.prototype.deferPlay_ = function(timeout) {
  this.log_('Defering playback for ' + timeout + ' ms');
  var self = this;
  this.deferredPlayCallbackId_ = setTimeout(function() {
    self.deferredPlayCallbackId_ = null;
    if (self.player_) {
      self.log_('Playing when enough data');
      self.player_.playWhenHaveEnoughData();
    } else {
      self.log_('Playing');
      self.mediaElement_.play();
    }
  }, timeout);
};


/**
 * Called when the media is successfully loaded. Updates the progress bar.
 *
 * @private
 */
AECustomPlayer.CastPlayer.prototype.onLoadSuccess_ = function() {
  trace('onLoadSuccess');
  // we should have total time at this point, so update the label
  // and progress bar
  var totalTime = this.mediaElement_.duration;
  if (!isNaN(totalTime)) {
    //this.totalTimeElement_.textContent =
      //  AECustomPlayer.formatDuration_(totalTime);
  } else {
    //this.totalTimeElement_.textContent = '';
    this.progressBarInnerElement_.style.width = '100%';
    this.progressBarThumbElement_.style.left = '100%';
  }

  //sessionStatus broadcast
  //trace('Broadcasting Message({"command":"sessionStatus","data":'+JSON.stringify(AECustomPlayer.sessionVars)+'});','#0f0','#000');
  //window.customMessageBus.broadcast({'command':'sessionStatus','data':AECustomPlayer.sessionVars});


  var getFeedData=AECustomPlayer.sessionVars.media.rawFeedData;
  //myReceiver._omnitureTrack({
  //  videocalltype:"ContentView"
  //
  //});

  mediaManager.setMediaInformation(AECustomPlayer.sessionVars.media); //update mediaInfo




};


/**
 * Returns the image url for the given media object.
 *
 * @param {!cast.receiver.media.MediaInformation} media The media.
 * @return {string|undefined} The image url.
 * @private
 */
AECustomPlayer.getMediaImageUrl_ = function(media) {
  var metadata = media.metadata || {};
  var images = metadata['images'] || [];
  return images && images[0] && images[0]['url'];
};


/**
 * Gets the adaptive streaming protocol creation function based on the media
 * information.
 *
 * @param {!cast.receiver.media.MediaInformation} mediaInformation The
 *     asset media information.
 * @return {?function(cast.player.api.Host):player.StreamingProtocol}
 *     The protocol function that corresponds to this media type.
 * @private
 */
AECustomPlayer.getProtocolFunction_ = function(mediaInformation) {
  trace('getProtocolFunction_v : mediaInformation.contentId='+mediaInformation.contentId)
  var url = mediaInformation.contentId;
  var type = mediaInformation.contentType || '';
  var path = AECustomPlayer.getPath_(url) || '';
  if (AECustomPlayer.getExtension_(path) === 'm3u8' ||
          type === 'application/x-mpegurl' ||
          type === 'application/vnd.apple.mpegurl') {
    return cast.player.api.CreateHlsStreamingProtocol;
  } else if (AECustomPlayer.getExtension_(path) === 'mpd' ||
          type === 'application/dash+xml') {
    return cast.player.api.CreateDashStreamingProtocol;
  } else if (path.indexOf('.ism') > -1 ||
          type === 'application/vnd.ms-sstr+xml') {
    return cast.player.api.CreateSmoothStreamingProtocol;
  }
  return null;
};


/**
 * Returns true if the media can be preloaded.
 *
 * @param {!cast.receiver.media.MediaInformation} media The media information.
 * @return {boolean} whether the media can be preloaded.
 * @private
 */
AECustomPlayer.supportsPreload_ = function(media) {
  return AECustomPlayer.getProtocolFunction_(media) != null;
};


/**
 * Returns true if the preview UI should be shown for the type of media
 * although the media can not be preloaded.
 *
 * @param {!cast.receiver.media.MediaInformation} media The media information.
 * @return {boolean} whether the media can be previewed.
 * @private
 */
AECustomPlayer.canDisplayPreview_ = function(media) {
  var contentId = media.contentId || '';
  var contentUrlPath = AECustomPlayer.getPath_(contentId);
  if (AECustomPlayer.getExtension_(contentUrlPath) === 'mp4') {
    return true;
  } else if (AECustomPlayer.getExtension_(contentUrlPath) === 'ogv') {
    return true;
  } else if (AECustomPlayer.getExtension_(contentUrlPath) === 'webm') {
    return true;
  }
  return false;
};


/**
 * Returns the type of player to use for the given media.
 * By default this looks at the media's content type, but falls back
 * to file extension if not set.
 *
 * @param {!cast.receiver.media.MediaInformation} media The media.
 * @return {AECustomPlayer.Type} The player type.
 * @private
 */
AECustomPlayer.getType_ = function(media) {
  var contentId = media.contentId || '';
  var contentType = media.contentType || '';
  var contentUrlPath = AECustomPlayer.getPath_(contentId);
  if (contentType.indexOf('audio/') === 0) {
    return AECustomPlayer.Type.AUDIO;
  } else if (contentType.indexOf('video/') === 0) {
    return AECustomPlayer.Type.VIDEO;
  } else if (contentType.indexOf('application/x-mpegurl') === 0) {
    return AECustomPlayer.Type.VIDEO;
  } else if (contentType.indexOf('application/vnd.apple.mpegurl') === 0) {
    return AECustomPlayer.Type.VIDEO;
  } else if (contentType.indexOf('application/dash+xml') === 0) {
    return AECustomPlayer.Type.VIDEO;
  } else if (contentType.indexOf('application/vnd.ms-sstr+xml') === 0) {
    return AECustomPlayer.Type.VIDEO;
  } else if (AECustomPlayer.getExtension_(contentUrlPath) === 'mp3') {
    return AECustomPlayer.Type.AUDIO;
  } else if (AECustomPlayer.getExtension_(contentUrlPath) === 'oga') {
    return AECustomPlayer.Type.AUDIO;
  } else if (AECustomPlayer.getExtension_(contentUrlPath) === 'wav') {
    return AECustomPlayer.Type.AUDIO;
  } else if (AECustomPlayer.getExtension_(contentUrlPath) === 'mp4') {
    return AECustomPlayer.Type.VIDEO;
  } else if (AECustomPlayer.getExtension_(contentUrlPath) === 'ogv') {
    return AECustomPlayer.Type.VIDEO;
  } else if (AECustomPlayer.getExtension_(contentUrlPath) === 'webm') {
    return AECustomPlayer.Type.VIDEO;
  } else if (AECustomPlayer.getExtension_(contentUrlPath) === 'm3u8') {
    return AECustomPlayer.Type.VIDEO;
  } else if (AECustomPlayer.getExtension_(contentUrlPath) === 'mpd') {
    return AECustomPlayer.Type.VIDEO;
  } else if (contentType.indexOf('.ism') != 0) {
    return AECustomPlayer.Type.VIDEO;
  }
  return AECustomPlayer.Type.UNKNOWN;
};


/**
 * Formats the given duration.
 *
 * @param {number} dur the duration (in seconds)
 * @return {string} the time (in HH:MM:SS)
 * @private
 */
AECustomPlayer.formatDuration_ = function(dur) {
  dur = Math.floor(dur);
  function digit(n) { return ('00' + Math.round(n)).slice(-2); }
  var hr = Math.floor(dur / 3600);
  var min = Math.floor(dur / 60) % 60;
  var sec = dur % 60;
  if (!hr) {
    return digit(min) + ':' + digit(sec);
  } else {
    return digit(hr) + ':' + digit(min) + ':' + digit(sec);
  }
};


/**
 * Adds the given className to the given element for the specified amount of
 * time.
 *
 * @param {!Element} element The element to add the given class.
 * @param {string} className The class name to add to the given element.
 * @param {number} timeout The amount of time (in ms) the class should be
 *     added to the given element.
 * @return {number} A numerical id, which can be used later with
 *     window.clearTimeout().
 * @private
 */
AECustomPlayer.addClassWithTimeout_ = function(element, className, timeout) {
  element.classList.add(className);
  return setTimeout(function() {
    element.classList.remove(className);
  }, timeout);
};


/**
 * Causes the given element to fade out, does something, and then fades
 * it back in.
 *
 * @param {!Element} element The element to fade in/out.
 * @param {number} time The total amount of time (in seconds) to transition.
 * @param {function()} something The function that does something.
 * @private
 */
AECustomPlayer.transition_ = function(element, time, something) {
  if (time <= 0 || AECustomPlayer.isCastForAudioDevice_()) {
    // No transitions supported for Cast for Audio devices
    something();
  } else {
    AECustomPlayer.fadeOut_(element, time / 2.0, function() {
      something();
      AECustomPlayer.fadeIn_(element, time / 2.0);
    });
  }
};


/**
 * Preloads media data that can be preloaded.
 *
 * @param {!cast.receiver.media.MediaInformation} media The media to load.
 * @param {function()} doneFunc The function to call when done.
 * @private
 */
AECustomPlayer.preload_ = function(media, doneFunc) {
  if (AECustomPlayer.isCastForAudioDevice_()) {
    // No preloading for Cast for Audio devices
    doneFunc();
    return;
  }

  var imagesToPreload = [];
  var counter = 0;
  var images = [];
  function imageLoaded() {
      if (++counter === imagesToPreload.length) {
        doneFunc();
      }
  }

  // try to preload image metadata
  var thumbnailUrl = AECustomPlayer.getMediaImageUrl_(media);
  if (thumbnailUrl) {
    imagesToPreload.push(thumbnailUrl);
  }
  if (imagesToPreload.length === 0) {
    doneFunc();
  } else {
    for (var i = 0; i < imagesToPreload.length; i++) {
      images[i] = new Image();
      images[i].src = imagesToPreload[i];
      images[i].onload = function() {
        imageLoaded();
      };
      images[i].onerror = function() {
        imageLoaded();
      };
    }
  }
};


/**
 * Causes the given element to fade in.
 *
 * @param {!Element} element The element to fade in.
 * @param {number} time The amount of time (in seconds) to transition.
 * @param {function()=} opt_doneFunc The function to call when complete.
 * @private
 */
AECustomPlayer.fadeIn_ = function(element, time, opt_doneFunc) {
  AECustomPlayer.fadeTo_(element, '', time, opt_doneFunc);
};


/**
 * Causes the given element to fade out.
 *
 * @param {!Element} element The element to fade out.
 * @param {number} time The amount of time (in seconds) to transition.
 * @param {function()=} opt_doneFunc The function to call when complete.
 * @private
 */
AECustomPlayer.fadeOut_ = function(element, time, opt_doneFunc) {
  AECustomPlayer.fadeTo_(element, 0, time, opt_doneFunc);
};


/**
 * Causes the given element to fade to the given opacity in the given
 * amount of time.
 *
 * @param {!Element} element The element to fade in/out.
 * @param {string|number} opacity The opacity to transition to.
 * @param {number} time The amount of time (in seconds) to transition.
 * @param {function()=} opt_doneFunc The function to call when complete.
 * @private
 */
AECustomPlayer.fadeTo_ = function(element, opacity, time, opt_doneFunc) {
  var self = this;
  var id = Date.now();
  var listener = function() {
    element.style.webkitTransition = '';
    element.removeEventListener('webkitTransitionEnd', listener, false);
    if (opt_doneFunc) {
      opt_doneFunc();
    }
  };
  element.addEventListener('webkitTransitionEnd', listener, false);
  element.style.webkitTransition = 'opacity ' + time + 's';
  element.style.opacity = opacity;
};


/**
 * Utility function to get the extension of a URL file path.
 *
 * @param {string} url the URL
 * @return {string} the extension or "" if none
 * @private
 */
AECustomPlayer.getExtension_ = function(url) {
  var parts = url.split('.');
  // Handle files with no extensions and hidden files with no extension
  if (parts.length === 1 || (parts[0] === '' && parts.length === 2)) {
    return '';
  }
  return parts.pop().toLowerCase();
};


/**
 * Returns the application state.
 *
 * @param {cast.receiver.media.MediaInformation=} opt_media The current media
 *     metadata
 * @return {string} The application state.
 * @private
 */
AECustomPlayer.getApplicationState_ = function(opt_media) {
  if (opt_media && opt_media.metadata && opt_media.metadata.title) {
    return 'Now Casting: ' + opt_media.metadata.title;
  } else if (opt_media) {
    return 'Now Casting';
  } else {
    return 'Ready To Cast';
  }
};


/**
 * Returns the URL path.
 *
 * @param {string} url The URL
 * @return {string} The URL path.
 * @private
 */
AECustomPlayer.getPath_ = function(url) {
  var href = document.createElement('a');
  href.href = url;
  return href.pathname || '';
};


/**
 * Logging utility.
 *
 * @param {string} message to log
 * @private
 */
AECustomPlayer.CastPlayer.prototype.log_ = function(message) {
  if (this.debug_ && message) {
    console.log(message);
  }
};


/**
 * Sets the inner text for the given element.
 *
 * @param {Element} element The element.
 * @param {string=} opt_text The text.
 * @private
 */
AECustomPlayer.setInnerText_ = function(element, opt_text) {
  if (!element) {
    return;
  }
  element.innerText = opt_text || '';
};


/**
 * Sets the background image for the given element.
 *
 * @param {Element} element The element.
 * @param {string=} opt_url The image url.
 * @private
 */
AECustomPlayer.setBackgroundImage_ = function(element, opt_url) {
  if (!element) {
    return;
  }
  element.style.backgroundImage =
      (opt_url ? 'url("' + opt_url.replace(/"/g, '\\"') + '")' : 'none');
  element.style.display = (opt_url ? '' : 'none');
};


/**
 * Called to determine if the receiver device is an audio device.
 *
 * @return {boolean} Whether the device is a Cast for Audio device.
 * @private
 */
AECustomPlayer.isCastForAudioDevice_ = function() {
  var receiverManager = window.cast.receiver.CastReceiverManager.getInstance();
  if (receiverManager) {
    var deviceCapabilities = receiverManager.getDeviceCapabilities();
    if (deviceCapabilities) {
      return deviceCapabilities['display_supported'] === false;
    }
  }
  return false;
};


/*For Freewheel*/
//var player = new tv.freewheel.DemoPlayer();
//player.requestAds();
//$('#start').bind('click', function(evt){
//  player.play();
//});