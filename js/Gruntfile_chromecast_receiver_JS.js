module.exports = function (grunt) {

    var brandID = grunt.option('brand');
    var environment =  grunt.option('environment');
    var cachebuster =  grunt.option('cachebuster');

    var isLocal= grunt.option('isLocal');
    var overRideEnv='override_blank_config';
    if(isLocal===true||isLocal=='true'){overRideEnv='override_local_config';}

    console.log('begin Receiver export for ' + brandID+' on '+environment+' isLocal:'+isLocal+ ' use: '+overRideEnv);

    var versionnumber = '<%= pkg.version %>.' + (Math.floor(new Date().getTime() / 100000000000) / 10);

    // USER DEFINED *start*
    var projectName = 'chromecast_receiver_'+brandID+'_'+environment;
    var thisFile = 'Gruntfile_chromecast_receiver_JS.js';
    var projectFolder = '../../AE_Cast/js/';  //DESTINATION FOR FILES
    var masterFiles = ['libraries/etc/required_chromecast.js', 'instances/chromecast_receiver_JS.js'];
    var finalNames = ['receiver_h_'+brandID+'_'+environment+'.js', 'receiver_f_'+brandID+'_'+environment+'.js', 'receiver_'+brandID+'_'+environment+'.js'];   //application.JS is the header and footer combined
    if(isLocal===true||isLocal=='true'){
      //  finalNames = ['receiver_h_'+brandID+'_'+environment+'.js', 'receiver_f_'+brandID+'_'+environment+'.js', 'receiver_'+brandID+'_'+environment+'.js'];
    }

    var thirdpartyInc = '/* almond, modernizr, underscore, oboe */';
    var requirePATHS = {
        almondLib: 'libraries/almond/almond',
        requireLib: 'libraries/require/require',
        requirejsConfig: 'libraries/etc/env/config_grunt',
        underscore: 'libraries/underscore/underscore183',

        chromeSender: "libraries/chromecastIncludes/sender",
        chromeReceiver: "libraries/chromecastIncludes/receiver",
        globalConfig: "libraries/etc/env/aetn/global_config",
        brandConfig: "libraries/etc/env/aetn/"+ brandID +"_config",
        environmentConfig: "libraries/etc/env/aetn/"+ environment +"_config",
        overRideConfig: "libraries/etc/env/aetn/"+overRideEnv,
        mDialog: "libraries/mdialog/mdialog-cast-smart-stream-sdk-2.0.0/mdialog-cast-sdk-2.0.0",
        qwery: "libraries/qwery/qwery",
        freewheel:"libraries/chromecastIncludes/freewheel",
        appmeasurement:"libraries/analytics/AppMeasurement",
        visitorAPI:"libraries/analytics/VisitorAPI"
    };
    // USER DEFINED *end*

    var myTaskList = [];


    myTaskList.push('jshint', 'shell', 'requirejs', 'concat', 'uglify', 'jsbeautifier', 'usebanner', 'copy', 'notify:complete');


    var destinationFolder = '../exports/' + projectName + '_v' + versionnumber + '/';

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('../package.json'),

        returnBanner: function (src, filepath) {
            var complete_src = grunt.file.expand(src);
            var final_name = '';
            // some string manipulations to get your the format you want
            for (var i = 0; i < complete_src.length; i++) {
                complete_src[i] = complete_src[i].substring(complete_src[i].lastIndexOf('/') + 1, complete_src[i].length);
            }
            final_name = complete_src.join('-');

            var final_banner = '';
            final_banner += '/*! ' + projectName + ' : ' + final_name + ' */';
            return final_banner;
        },    //This function places the filename at the top of the file

        banner: '/*! codebase: <%= pkg.name %> v' + versionnumber + ' by Jonathan Robles */\n' +
        '/*! built:<%= grunt.template.today("mm-dd-yyyy [h:MM:ssTT]") %> */\n' +
        '/*! -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */\n\n' +
        '/*! Prerequisites: //www.gstatic.com/cast/sdk/libs/receiver/2.0.0/cast_receiver.js */\n\n' +
        '/*! Third Party Includes [start] */\n' + thirdpartyInc +
        '\n/*! Third Party Includes [end] */\n',
        jshint: {
            // define the files to lint
            files: [thisFile],
            // configure JSHint (documented at http://www.jshint.com/docs/)
            options: {
                // more options here if you want to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true
                }
            }
        },

        shell: {
            erase_directories: {
                command: 'rm -rf ' + projectFolder + 'receiver*' + brandID + '_'+environment+ '*'
            },
            erase_temp_directory: {
                command: 'rm -rf ../exports/' + projectName + '_temp/*' + brandID + '_'+environment+ '*.js'
            },
            erase_proj_directory: {
                command: 'rm -rf ../exports/' + projectName + '_v' + versionnumber + '/*' + brandID + '_'+environment+ '*'
            },
            combineCSS: {
                command: 'cat ../../AE_Cast/css_src/receiver_global.css ../../AE_Cast/css_src/receiver_'+brandID+'.css > ../../AE_Cast/css/receiver_'+brandID+'.css'
            },
            makeIndex: {
                command: 'cp -f ../../AE_Cast/receiver_template.html ../../AE_Cast/receiver_'+ brandID +'_'+environment+'.html'
            },
            fixIndexPathsA:{
                command: "sed 's/{{brandID}}/" + brandID + "/g' ../../AE_Cast/receiver_"+ brandID +"_"+environment+".html > ../../AE_Cast/receiver_"+ brandID +"_"+environment+"_temp.html"
            },
            fixIndexPathsB:{
                command: "sed 's/{{environment}}/" + environment + "/g' ../../AE_Cast/receiver_"+ brandID +"_"+environment+"_temp.html > ../../AE_Cast/receiver_"+ brandID +"_"+environment+".html"
            },
            fixIndexPathsC:{
                command: "sed 's/{{cachebuster}}/" + cachebuster + "/g' ../../AE_Cast/receiver_"+ brandID +"_"+environment+".html > ../../AE_Cast/receiver_"+ brandID +"_"+environment+"_temp.html"
            },
            fixIndexPathsD:{
                command: "sed 's/{{cachebusterX}}/" + cachebuster + "/g' ../../AE_Cast/receiver_"+ brandID +"_"+environment+"_temp.html > ../../AE_Cast/receiver_"+ brandID +"_"+environment+".html"
            },
            killTempFile:{
                command: "rm -rf ../../AE_Cast/receiver_"+ brandID +"_"+environment+"_temp.html"
            }



        },
        requirejs: {
            required_file: {
                options: {
                    baseUrl: ".",
                    mainConfigFile: 'libraries/etc/env/config_grunt.js',
                    optimize: 'none',
                    preserveLicenseComments: false,
                    name: masterFiles[0],
                    out: "../exports/" + projectName + "_temp/" + finalNames[0],
                    paths: requirePATHS,
                    include: ['jquery','mDialog','globalConfig', 'environmentConfig','brandConfig','overRideConfig','visitorAPI','appmeasurement', 'almondLib', 'requirejsConfig', 'chromeReceiver' ]
                }
            },
            application_file: {
                options: {
                    baseUrl: ".",
                    mainConfigFile: 'libraries/etc/env/config_grunt.js',
                    optimize: 'none',
                    preserveLicenseComments: false,
                    name: masterFiles[1],
                    out: "../exports/" + projectName + "_temp/" + finalNames[1],
                    paths: requirePATHS
                }
            }
        },

        concat: {
                options: {
                    separator: ';',
                },
                dist: {
                    src: ["../exports/" + projectName + "_temp/" + finalNames[0], "../exports/" + projectName + "_temp/" + finalNames[1]],
                    dest: "../exports/" + projectName + "_temp/" + finalNames[2]
                }

        },
        //Minify!!!
        uglify: {
            beautify: {
                options: {
                    banner: '<%= banner %>',
                    beautify: true,
                    mangle: false,
                    compress: false
                },

                src: '../exports/' + projectName + '_temp/*' + brandID + '_'+environment+ '*.js',
                dest: destinationFolder,
                expand: true, // allow dynamic building
                flatten: true // remove all unnecessary nesting
            },
            minifier: {
                options: {
                    banner: '<%= banner %>',
                    beautify: false,
                    mangle: true,
                    compress: true
                },

                src: '../exports/' + projectName + '_temp/*' + brandID + '_'+environment+ '*.js',
                dest: destinationFolder,
                expand: true, // allow dynamic building
                flatten: true, // remove all unnecessary nesting
                ext: '_min.js' // replace .js to .min.js
            }
        },
        jsbeautifier: {
            files: [(destinationFolder + '*' + brandID + '_'+environment+ '*.js'), ('!' + destinationFolder + '*' + brandID + '_'+environment+ '*_min.js')]
        },
        usebanner: {
            taskName: {
                options: {
                    position: 'top',
                    linebreak: true,
                    process: '<%= returnBanner %>'


                },
                files: {
                    src: [(destinationFolder + '*')]
                }
            }
        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        flatten: true,
                        src: ['../exports/' + projectName + '_v' + versionnumber + '/*'],
                        dest: projectFolder,
                        filter: 'isFile'
                    }
                ],
            },
        },
        watch: {
            scripts: {
                files: masterFiles,  //watch these files and GRUNT IT!
                tasks: myTaskList,
                options: {
                    spawn: true,
                    livereload: true
                }
            }


        },
        notify_hooks: {
            options: {
                enabled: true,
                max_jshint_notifications: 5, // maximum number of notifications from jshint output
                title: projectName, // defaults to the name in package.json, or will use project directory's name
                success: false, // whether successful grunt executions should be notified automatically
                duration: 3 // the duration of notification in seconds, for `notify-send only
            }
        },
        notify: {
            complete: {
                options: {
                    title: '<%= notify_hooks.options.title %>',  // optional
                    message: 'DONE! [<%= notify_hooks.options.title %>]'
                }
            }
        }
    });

    // PLUGINS
    grunt.loadNpmTasks('grunt-banner'); //https://github.com/gruntjs/grunt-contrib-concat
    grunt.loadNpmTasks('grunt-contrib-concat'); //https://github.com/gruntjs/grunt-contrib-concat
    grunt.loadNpmTasks('grunt-contrib-copy'); //https://github.com/gruntjs/grunt-contrib-copy
    //grunt.loadNpmTasks('grunt-contrib-cssmin'); //https://github.com/gruntjs/grunt-contrib-cssmin
    //grunt.loadNpmTasks('grunt-contrib-imagemin'); //https://github.com/gruntjs/grunt-contrib-imagemin
    //grunt.loadNpmTasks('grunt-contrib-jasmine');   //https://github.com/gruntjs/grunt-contrib-jasmine
    grunt.loadNpmTasks('grunt-contrib-jshint'); //https://github.com/gruntjs/grunt-contrib-jshint
    //grunt.loadNpmTasks('grunt-contrib-qunit'); //https://github.com/gruntjs/grunt-contrib-qunit
    grunt.loadNpmTasks('grunt-contrib-requirejs'); //https://github.com/gruntjs/grunt-contrib-requirejs
    grunt.loadNpmTasks('grunt-contrib-uglify');    //https://github.com/gruntjs/grunt-contrib-uglify
    grunt.loadNpmTasks('grunt-contrib-watch');    //https://github.com/gruntjs/grunt-contrib-watch
    grunt.loadNpmTasks('grunt-jsbeautifier');  //https://github.com/vkadam/grunt-jsbeautifier
    //grunt.loadNpmTasks('grunt-mocha');    //https://github.com/kmiyashiro/grunt-mocha
    //grunt.loadNpmTasks('grunt-mocha-phantomjs');  //https://github.com/jdcataldo/grunt-mocha-phantomjs
    grunt.loadNpmTasks('grunt-notify');   //https://github.com/dylang/grunt-notify
    grunt.loadNpmTasks('grunt-shell');    //https://github.com/sindresorhus/grunt-shell
    //grunt.loadNpmTasks('grunt-ssh');    //https://github.com/chuckmo/grunt-ssh

    // run right away
    grunt.task.run('notify_hooks');
    // Default task(s).


    //for (varIdx = 0; varIdx < brandID.length; varIdx++) {
    //    var thisBrand = brandID[varIdx];
    //    console.log(thisBrand)
    //}
    console.log('MYTASKS1! ' + myTaskList);
    grunt.registerTask('default', myTaskList);


    //grunt.registerTask('default', function(myTaskList){
    //    returnTasks =[];
    //    for (varIdx = 0; varIdx < brandID.length; varIdx++) {
    //        var thisBrand = brandID[varIdx];
    //        console.log(thisBrand)
    //        returnTasks.push.myTaskList
    //    }
    //    console.log('MYTASKS! '+returnTasks);
    //    return returnTasks;
    //});


};