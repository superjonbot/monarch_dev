/*!
 * Chromecast Sender Module
 * by Jonathan Robles
 * 12-17-15
 *
 *
 */







define(['modules/definitions/standardmodule','underscore','oboe'],function (parentModel,_,oboe) {

    // external global function(s) //comment out on init!!!
    //window.globalFunction=undefined;
    //window.testbutton=undefined;

    function _thizOBJ_( o ){

        var defaults={
            scope:true,
            type:'chromecast_receiver',
            author:'Jonathan Robles',
            lasteditby:'',
            playlist:[],
            playItemFormat:{}
        };
        defaults= this.deepExtend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };


    _thizOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners =function(){

        /*

         this.notify('Trace','_startlisteners');
         var parent=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {
         onQuickjump:function(o){
         if(o.senderID==myID){
         parent.quickjump(o.data.index)
         }
         },
         onJump:function(o){
         if(o.senderID==myID){
         parent.jump(o.data.index)
         }
         },
         onPagejump:function(o){
         if(o.senderID==myID){
         parent.pagejump(o.data.index)
         }
         },
         onNext:function(o){
         if(o.senderID==myID){
         parent.next()
         }
         },
         onPrev:function(o){
         if(o.senderID==myID){
         parent.prev()
         }
         }




         }
         }());

         */


    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init =function(){
        //alert(this._var().scope)
        this.notify('Trace','init');
        //append defaults
        this._var({
            //currentMediaURL : this._var().mediaURLs[0],
            //currentMediaTitle : this._var().mediaTitles[0],
            //currentMediaThumb : this._var().mediaThumbs[0]
        });
        this._startlisteners();//start this module's listeners
        this.refresh();
    };
    _thizOBJ_.prototype.refresh =function(){
        var parent=this;
        parent.notify('Trace','refresh');

        //try to get IP info
        oboe('whatip.php').node(function(){alert('ERROR')}).done(function(data){
            //alert("IPs:"+JSON.stringify(data));
            trace('IP:'+JSON.stringify(data));

        }).fail(function(){
            console.log(':( cant get IP!');
        });


        /*



                oboe('https://dev-chromecast.ott.aetnd.com/proxy.php?mode=JSON&full_status=1&full_headers=1&url=devmobileservices.aetndigital.com%2Fjservice%2Fwombattpservice%2Fshow_titles%2Fepisode%2Fhistory%3Ftitle_id%3D55477827650%26deviceId%3Dappletv%26range%3D1-100000').node(function(){alert('ERROR')}).done(function(data){
                    //alert(data.length);
                    //var titles = _.map(data, function(entry){
                    //    return {
                    //        title:entry.title,
                    //        id:entry.title,
                    //        tmsId:entry.tmsId
                    //    }; });

                    //ALL DATA
                    //console.log(':) '+JSON.stringify(data));   //get ALL DATA
                    //console.log(':) '+JSON.stringify(data.contents.Items[0].playURL_HLS));  //get HLS
                    parent._playURL_HLS(data.contents.Items[0].playURL_HLS)

                }).fail(function(){
                    console.log(':( cant get data... SAAAAAAAD!');
                });
        */

        var playerDiv = document.getElementById('player');

        try {
            window.castReceiverManager = window.cast.receiver.CastReceiverManager.getInstance();
            window.customMessageBus = castReceiverManager.getCastMessageBus(AETN.namespace,cast.receiver.CastMessageBus.MessageType.JSON);
            window.customMessageBus.onMessage = function(getData) {
                var senderId=getData.senderId;
                var returnData=getData.data;



                switch(returnData.command) {
                    case 'kitchenSink':   //displays all information in the receiver
                        trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#0f0','#000');
                        window.customMessageBus.send(senderId,{'command':returnData.command,'data':AECustomPlayer.sessionVars});
                        //trace('RETURNING to sender('+senderId+'): '+JSON.stringify(AECustomPlayer.sessionVars));
                        break;
                    case 'sessionStatus':   //displays just the customData
                        trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#0f0','#000');
                        window.customMessageBus.send(senderId,{'command':returnData.command,'data':AECustomPlayer.sessionVars.media.customData});
                        //trace('RETURNING to sender('+senderId+'): '+JSON.stringify(AECustomPlayer.sessionVars.media.customData));
                        break;
                    case 'mediaTime':   //displays just the customData
                        trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#0f0','#000');
                        var mediaObj=AECustomPlayer.sessionVars.media;
                        var timeObject={
                            'currentTime': mediaObj.currentTime,
                            'streamTimeWithAds': mediaObj.streamTimeWithAds,
                            'streamTimeWithoutAds': mediaObj.streamTimeWithoutAds,
                            'durationWithAds': mediaObj.duration,
                            'durationWithoutAds': mediaObj.rawFeedData.duration
                        };
                        window.customMessageBus.send(senderId,{'command':returnData.command,'data':timeObject});
                        //trace('RETURNING to sender('+senderId+'): '+JSON.stringify(AECustomPlayer.sessionVars.media.customData));
                        break;
                    case 'mediaTimetoTimeWithAds':   //displays just the customData
                        trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#0f0','#000');
                        var streamTimeWithAds = AECustomPlayer.streamData.getStreamTimeWithAds(Number(returnData.data));
                        window.customMessageBus.send(senderId,{'command':returnData.command,'data':streamTimeWithAds});
                        break;
                    case 'mediaTimetoTimeWithoutAds':   //displays just the customData
                        trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#0f0','#000');
                        var streamTimeWithAds = AECustomPlayer.streamData.getStreamTimeWithoutAds(Number(returnData.data));
                        window.customMessageBus.send(senderId,{'command':returnData.command,'data':streamTimeWithAds});
                        break;
                    case 'forceTimeline':
                        trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#0f0','#000');
                        if($('html').hasClass('forcetimeline')){
                            trace('forceTimeline toggled OFF','#000','#F2F055')
                            $('html').removeClass('forcetimeline');
                        }else{
                            trace('forceTimeline toggled ON','#000','#F2F055')
                            $('html').addClass('forcetimeline');}
                        break;
                    case 'hideConsole'://"command":"someSwitch","data":"true"
                        trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#0f0','#000');
                        if(String(returnData.data)=='true'){
                            trace('Console toggled OFF','#000','#F2F055');
                            $('.debugConsole').css('display','none');
                        }  else {
                            trace('Console toggled ON','#000','#F2F055');
                            $('.debugConsole').css('display','block');
                        }
                        break;
                    case 'enableCaptions':
                        trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#0f0','#000');
                        var cc_data=(returnData.data=='true');
                        //console.log(returnData.data+'=='+Boolean(returnData.data))
                        AECustomPlayer.sessionVars.media.customData.captions=cc_data;
                        if(AECustomPlayer.sessionVars.media.streamSource=='mDialog'){
                            trace('mDialog captions set to :'+cc_data,'#000','#F2F055');
                            playerObject.player_.enableCaptions(cc_data,cast.player.api.CaptionsType.CEA608);  //true   //,AECustomPlayer.streamData.subtitlesURL
                        }else{
                            trace('non-mDialog captions set to :'+cc_data,'#000','#F2F055');
                            playerObject.player_.enableCaptions(cc_data);  //true
                        }

                        mediaManager.setMediaInformation(AECustomPlayer.sessionVars.media); //update mediaInfo
                        window.customMessageBus.broadcast({'command':returnData.command,'data':AECustomPlayer.sessionVars.media.customData});
                        break;
                    default:
                        trace('onMessage received from '+senderId+': sendMessage("'+AETN.namespace+'",'+JSON.stringify(returnData)+');','#f00','#000');
                        window.customMessageBus.send(senderId,{'command':returnData.command,'data':'failed, no such command'});
                }

            }
        }
        catch(err) {
            trace('custom messaging failed for '+AETN.namespace);
        }


        new AECustomPlayer.CastPlayer(playerDiv).start();

        trace('AECustomReceiver Started for channel:'+AETN.brandID+' for environment:'+AETN.environment+ ' expecting ApplicationID:'+AETN.applicationID+' using namespace:'+AETN.namespace);




    };
    _thizOBJ_.prototype.kill =function(){
        var parent=this;
        parent.notify('Trace','kill');
        _notify.rem(this._id());  //kills this module's listeners
    };

    // Internal - methods  _internalmethod//external_function
    _thizOBJ_.prototype._playURL_HLS = function(getURL){
        var parent=this;
        parent.notify('Trace','_playURL_HLS('+getURL+')');
        parent._getSignedURL(getURL);
    };

    _thizOBJ_.prototype._returnProxyUrl = function(getURL){

        if(getURL.substring(0,4).toLowerCase()=='http') {
            var htLoc=getURL.indexOf('://');
            getURL=getURL.substring((htLoc+3),getURL.length)
        }//strip http,https
        var signinURLwProxy = AETN.proxyWithSigninURL+encodeURIComponent(getURL);
        return signinURLwProxy;
    };





    //myReceiver._getMDialogURL({'blank':'blank'},function(data){console.log(data.hdManifestURL)})

//sign regular URL
   // myReceiver._getSignedURL('http://link.theplatform.com/s/xc6n8B/_GG4VE3VIUix?switch=hls&assetTypes=medium_video_s3&mbr=true&metafile=false',function(ret){console.log(':)'+ret)},function(ret){':('+console.log(ret)})

//sign mdialog URL
    //myReceiver._getMDialogURL({'senderData':{'streamContext':{"assetKey":"630362691506"}}},function(data){console.log(data.hdManifestURL);myReceiver._getSignedURL(data.hdManifestURL,function(ret){console.log(':)'+ret)},function(ret){':('+console.log(ret)})})



    //get platform id
    //http://devmobileservices.aetndigital.com/jservice/wombattpservice/show_titles/episode/history?title_id=635749956000&deviceId=appletv&range=1-100000


/*

    myReceiver._getMPXURL({assetKey:"634465859847"},function(e){console.log(e),myReceiver._getSignedURL(e,function(e){console.log(":)"+e),window.thisReturn=e,mediaManager.onLoad({type:"load",$:!1,defaultPrevented:!1,Yb:!0,data:{requestId:26909096,sessionId:"700C512E-8FD6-4A79-B7B2-6FFF78BAA18F",media:{contentId:e,streamType:"BUFFERED",contentType:"video/mp4",metadata:{type:0,metadataType:0,title:"Series on mlt",subtitle:"S1 E1",images:[{url:"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg"}],subtitle2:"Some show on dev",platformID:"630362691506",userID:"630362691506630362691506630362691506630362691506630362691506",adBreaks:[{url:"http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8",duration:5,cuepoint:0,played:!1},{url:"http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8",duration:6,cuepoint:300,played:!1},{url:"http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8",duration:7,cuepoint:420,played:!1},{url:"http://dev-chromecast.ott.aetnd.com/lifetimemanifest.m3u8",duration:8,cuepoint:572,played:!1}],description:"Test Description"}},autoplay:!0,currentTime:0},senderId:"3:client-63057"})},function(e){":("+console.log(e)})});


*/
//myReceiver._omnitureTrack({});
    _thizOBJ_.prototype._omnitureTrack = function(getEvents,options){
        var s_account=AETN.adobeReportSuiteID;
        var s=s_gi(s_account);

        //Utilities
        //JavaScript AppMeasurement provides the following built-in utilities:
        //
        //Util.cookieRead
        //Util.cookieWrite
        //Util.getQueryParam
//https://marketing.adobe.com/resources/help/en_US/sc/implement/appmeasure_mjs.html


        var getFeedData=AECustomPlayer.sessionVars.media.rawFeedData;
        AECustomPlayer.sessionVars.media.customData.trackingData=AECustomPlayer.sessionVars.media.customData.trackingData||{};

        s.events=s.linkTrackEvents=getEvents;
        s.pageName = '';
        s.watch={
            //platform:"Chromecast",
            brand:AETN.brandID_omnitureName,
            originatingplatform:AECustomPlayer.sessionVars.media.customData.trackingData.origin_platform,
            adid:AECustomPlayer.sessionVars.media.customData.trackingData.origin_adid,
            kruxid:AECustomPlayer.sessionVars.media.customData.trackingData.origin_kruxid,
            sessionid:AECustomPlayer.sessionVars.lastSenderId,

            videocliptitle:getFeedData.title,
            videocategory:getFeedData.tvNtvMix,
            videolf:getFeedData.isLongForm,
            videoseason:getFeedData.tvSeasonNumber,
            videotv:getFeedData.tvNtvMix,
            //videochapter:undefined,
            videoepisode:getFeedData.tvSeasonEpisodeNumber,
            videosubcategory:getFeedData.clipType,
            videofastfollow:getFeedData.isFastFollow,
            //videoautoplay:undefined,
            videoduration:getFeedData.duration,
            videofullscreen:undefined,
            //videoadvertiser:AECustomPlayer.sessionVars.lastAdbreak.position,
            //videoadduration:AECustomPlayer.sessionVars.lastAdbreak.duration,
            //videoadtitle:AECustomPlayer.sessionVars.lastAdbreak.position,
            videorequiresauthentication:getFeedData.isBehindWall,
            videopplid:getFeedData.programId
        }

        //AECustomPlayer.sessionVars.media.customData.trackingData.origin_mvpd


        //add defaults
        s.watch = this.deepExtend(s.watch,AETN.omnitureDefaults);
        //add custom
        s.watch = this.deepExtend(s.watch,options);

        console.log('OMNITURE OBJECT:'+JSON.stringify(s.watch));

        var s_code=s.t();if(s_code)document.write(s_code);

        //var adobeToken=AECustomPlayer.sessionVars.media.customData.auth;

    }

    _thizOBJ_.prototype._kruxTrack = function(getEvents,options){
        AECustomPlayer.sessionVars.media.customData.trackingData=AECustomPlayer.sessionVars.media.customData.trackingData||{};
        var getMediaData=AECustomPlayer.sessionVars.media
        var getFeedData=getMediaData.rawFeedData;
        var getTrackingData=AECustomPlayer.sessionVars.media.customData.trackingData;
        //    "origin_platform":'missing from sender',
        //    "origin_session_id":'missing from sender',
        //    "origin_visitor_id":'missing from sender',//AECustomPlayer.sessionVars.media.customData['userID'],
        //    "origin_device_id":'missing from sender',
        //    "origin_adid":'missing from sender',//"[IDFA] if iOS or [Google Ad ID] if Android",
        //    "origin_kruxid":'missing from sender',//"sender's krux id",
        //    "origin_mvpd":'missing from sender'//"sender's cable provider name"
        var kruxVars={
            '_kcp_d':AETN.krux_brand,
            '_kcp_s':AETN.krux_brand,
            '_kuid': getMediaData.customData['userID'],
            '_kcp_sc': 'Shows',
            'tech_os': AETN.krux_tech_os,
            'tech_browser': AETN.krux_tech_browser,
            '_kpa_show_name':getFeedData.seriesName,
            '_kpa_is_tve_authenticated':getMediaData.customData['isAuth'],
            '_kpa_tve_provider_name': (getTrackingData.origin_mvpd)?getTrackingData.origin_mvpd:'missing',
            'event_id':'JYdmH5gc',
            '_kpa_video_analytics_clip_title':getFeedData.title,
            '_kpa_video_analytics_series_name':getFeedData.seriesName,
            '_kpa_video_analytics_lf_vs_sf':getFeedData.isLongForm,
            '_kpa_video_season':getFeedData.tvSeasonNumber,
            '_kpa_video_series_type':getFeedData.tvNtvMix,
            '_kpa_video_chapter':'',
            '_kpa_video_episode':getFeedData.tvSeasonEpisodeNumber,
            '_kpa_video_analytics_days_since_premiere':'',
            '_kpa_video_duration':getFeedData.duration,
            '_kpa_video_PPLID':String(getFeedData.ppl_id),
            '_kpa_originating_platform':getTrackingData.origin_platform,
            '_kua_ad_id':getTrackingData.origin_adid,
            '_kua_krux_id':getTrackingData.origin_kruxid,
            '_kua_chromecast_session_id':AECustomPlayer.sessionVars.lastSenderId,
            '_kpa_page_level_1':'Shows',
            '_kpa_page_level_2':getFeedData.title,
            '_kpa_internal_search_manual':''
        }



        switch(getEvents) {
            case 'ScreenView':

                break;
            case 'Event':

                break;
            case 'Heartbeat':

                break;
            default:

        }





    }

    _thizOBJ_.prototype._getFeedData = function(o,success_cb,fail_cb){
        trace('_getFeedData!!!','#fff','#000');
        AECustomPlayer.sessionVars.media.streamSource='MPX';
        var parent=this;
        var defaults={
            'assetKey':undefined
        };
        defaults= this.deepExtend(defaults,o);

        if(defaults.assetKey==undefined){
            //fail_cb('No assetKey defined!')

        }   else {

            //var convertBrandID=AETN.brandID;
            //if(convertBrandID=='his'){convertBrandID='history'};

            //var lookUpURL = AETN.titleFeed+'/episode/'+convertBrandID+'?title_id='+defaults.assetKey+'&deviceId=appletv&range=1-100000';
            var lookUpURL = AETN.titleFeedNew+defaults.assetKey;
            var proxyWithLookUpURL=  AETN.proxyWithSigninURL+lookUpURL;
            var proxyWithLookUpURL = proxyWithLookUpURL.replace("&url=http://", "&url=");
            console.log('proxyWithLookUpURL: '+proxyWithLookUpURL);

            oboe(proxyWithLookUpURL).done(function(data){
                var returnDATA=data.contents.results[0];
                success_cb(returnDATA);
            }).fail(function(){
                trace('_getFeedData fail for '+defaults.assetKey);
                fail_cb('please check feed: '+decodeURI(lookUpURL));
            });

        }
    };   //get that DATA!



    _thizOBJ_.prototype._getMPXURL = function(o,success_cb,fail_cb){
        trace('MPX','#fff','#000','#streamSource');
        AECustomPlayer.sessionVars.media.streamSource='MPX';
        var parent=this;
        var defaults={
            'assetKey':"630362691506"
        };
        defaults= this.deepExtend(defaults,o);

        var convertBrandID=AETN.brandID;
        if(convertBrandID=='his'){convertBrandID='history'};

        //var lookUpURL = AETN.titleFeed+'/episode/'+convertBrandID+'?title_id='+defaults.assetKey+'&deviceId=appletv&range=1-100000';
        var lookUpURL = AETN.titleFeedNew+defaults.assetKey;
        var proxyWithLookUpURL=  AETN.proxyWithSigninURL+lookUpURL;
        var proxyWithLookUpURL = proxyWithLookUpURL.replace("&url=http://", "&url=");
        console.log('proxyWithLookUpURL: '+proxyWithLookUpURL);

/*        oboe(proxyWithLookUpURL).done(function(data){
            //
            trace('ALL DATA: '+JSON.stringify(data));
            trace('data.contents.results[0].publicUrl: '+data.contents.results[0].publicUrl);
            //var returnURL=data.contents.Items[0].playURL_HLS;

            console.log('AECustomPlayer.sessionVars.media.rawFeedData.publicUrl='+AECustomPlayer.sessionVars.media.rawFeedData.publicUrl);
            console.log('data.contents.results[0].publicUrl='+data.contents.results[0].publicUrl);

            var returnURL=data.contents.results[0].publicUrl+'?'+AETN.mDialog_playoptions;
            console.log('returnURL: '+returnURL);
            success_cb(returnURL);
        }).fail(function(){
            trace('fail for '+defaults.assetKey);
            fail_cb('request to '+proxyWithLookUpURL);
        });*/



        //oboe(proxyWithLookUpURL).done(function(data){
            //
            //trace('ALL DATA: '+JSON.stringify(data));
            //trace('data.contents.results[0].publicUrl: '+data.contents.results[0].publicUrl);
            //var returnURL=data.contents.Items[0].playURL_HLS;

            //console.log('AECustomPlayer.sessionVars.media.rawFeedData.publicUrl='+AECustomPlayer.sessionVars.media.rawFeedData.publicUrl);
            //console.log('data.contents.results[0].publicUrl='+data.contents.results[0].publicUrl);

            var returnURL=AECustomPlayer.sessionVars.media.rawFeedData.publicUrl+'?'+AETN.mDialog_playoptions;

        //tack on token
          //  var authToken=AECustomPlayer.sessionVars.media.metadata.customData.auth;
        // if(authToken!=undefined&&authToken!=''){
        //     returnURL=returnURL+'&auth='+authToken;
        // }


           // console.log('authToken:'+authToken+' returnURL: '+returnURL);
            success_cb(returnURL);

        //}).fail(function(){
        //    trace('fail for '+defaults.assetKey);
        //    fail_cb('request to '+proxyWithLookUpURL);
        //});





    };

    _thizOBJ_.prototype._getMDialogURL = function(o,success_cb,fail_cb){
        trace('mDialog','#fff','#000','#streamSource');
        AECustomPlayer.sessionVars.media.streamSource='mDialog';

        var defaults={
            'senderData':{
                "debug": true,
                "sessionContext":{
                    "subDomain": "aetn-vod",
                    "apiKey": AETN.apiKey,
                    "applicationKey": AETN.applicationKey
                },
                "streamContext":{
                    "assetKey":undefined, /*"630362691506"*/
                    "adDecisioningData": {

                        "stream_activity_key": AETN.streamActivityKey, //"599b88193e3f5a6c96f7a362a75b34ad"
                        "adconfig": "staging"
                    },
                    "trackingData": {
                        "VISITOR_ID":AECustomPlayer.sessionVars.media.customData['userID'],
                        "device_platform":"chromecast",
                        "videocategory": AECustomPlayer.sessionVars.media.rawFeedData.tvNtvMix,
                        "videocliptitle":AECustomPlayer.sessionVars.media.rawFeedData.title,
                        "videolf":AECustomPlayer.sessionVars.media.rawFeedData.isLongForm,
                        "videofastfollow":AECustomPlayer.sessionVars.media.rawFeedData.isFastFollow,
                        "videotv":AECustomPlayer.sessionVars.media.rawFeedData.rating,
                        "videochapter":AECustomPlayer.sessionVars.media.rawFeedData.tvSeasonEpisodeNumber,
                        "videoseason":AECustomPlayer.sessionVars.media.rawFeedData.tvSeasonNumber,
                        "origin_platform":'missing from sender',
                        "origin_session_id":'missing from sender',
                        "origin_visitor_id":'missing from sender',//AECustomPlayer.sessionVars.media.customData['userID'],
                        "origin_device_id":'missing from sender',
                        "origin_adid":'missing from sender',//"[IDFA] if iOS or [Google Ad ID] if Android",
                        "origin_kruxid":'missing from sender',//"sender's krux id",
                        "origin_mvpd":'missing from sender'//"sender's cable provider name"
                    }
                }
            },
            'receiverData': {
                'mediaElement': playerObject.mediaElement_,
                'mediaManager': playerObject.mediaManager_ }

        }

        //add options
        defaults.senderData.streamContext= this.deepExtend(defaults.senderData.streamContext,o);

        //passthrough trackingData
        defaults.senderData.streamContext.trackingData= this.deepExtend(defaults.senderData.streamContext.trackingData,AECustomPlayer.sessionVars.media.customData.trackingData);

        //hardcoded trackingdata
        //defaults.senderData.streamContext.trackingData= this.deepExtend(defaults.senderData.streamContext.trackingData,{
        //    "device_platform":"chromecast"
        //});


        if(AECustomPlayer.sessionVars.media.customData['userID']&&(defaults.senderData.streamContext.trackingData.origin_visitor_id==undefined)){
            defaults.senderData.streamContext.trackingData.origin_visitor_id=AECustomPlayer.sessionVars.media.customData['userID'];
        }

        trace('_getMDialogURL received senderData data: '+JSON.stringify(defaults.senderData));


        //console.log()


        var streamManager = new mdialog.cast.api.StreamManager(defaults.senderData, defaults.receiverData);
        window.streamManager=streamManager;
        trace('making mdialog streamManager');

        streamManager.onAbort = function(error) {
            trace('mDialog STREAM FAILED : '+error,'#fff','#f00');
            fail_cb('mDialog stream failed');
        };
        streamManager.onStreamFailed = function(error) {
            trace('mDialog onAbort : '+error,'#fff','#f00');
            fail_cb('mDialog onAbort');
        };
        streamManager.onStreamExpired = function(error) {
            trace('mDialog onStreamExpired : '+error,'#fff','#f00');
            fail_cb('mDialog onAbort');
        };
        streamManager.onNearestPreviousAdBreak =function(data){
          //  trace('onNearestPreviousAdBreak data:'+JSON.stringify(data),'#f00','#fff');

            //if (data.isUnwatched){
            //  //  trace('Naw! you tried to skip the ad at '+data.startTime);
            //}  else {
            // //   trace('yeah, you can skip since data.isUnwatched='+data.isUnwatched)
            //}

        }
        // streamManager.adBreakStarted =function(data){
        //     trace('adBreakStarted data:'+JSON.stringify(data),'#f00','#fff')
        // }

        // streamManager.adBreakEnded=function(data){
        //     trace('adBreakEnded data:'+JSON.stringify(data),'#f00','#fff')
        // }
        //
        // streamManager.adEventStarted=function(data){
        //     trace('adEventStarted data:'+JSON.stringify(data),'#f00','#fff')
        // }

        //
        //streamManager.adBreakStarted = function(){trace('adBreakStarted','#000','#afa')}
        //streamManager.adBreakEnded = function(){trace('adBreakEnded','#000','#afa')}
        //streamManager.adEventStarted = function(){trace('adEventStarted','#000','#afa')}
        //streamManager.onNearestPreviousAdBreak = function(){trace('onNearestPreviousAdBreak','#000','#afa')}
        //streamManager.shouldProcessAdBreak = function(){trace('shouldProcessAdBreak','#000','#afa')}



        streamManager.onStreamLoaded=function(data){
            var returnURL=data.hdManifestURL;
            trace('_returnURL: '+returnURL);

            AECustomPlayer.streamData=data;
            success_cb(returnURL);

        };
        streamManager.getStream(defaults.senderData.streamContext.assetKey);

    };

    _thizOBJ_.prototype._getSignedURL = function(getURL,success_cb,fail_cb){
            var parent=this;
            trace('_getSignedURL('+getURL+')');
            parent.notify('Trace','_getSignedURL('+getURL+')');
      //  var signinURL= "https://servicesaetn-a.akamaihd.net/jservice/video/components/get-signed-signature?url="+encodeURIComponent(getURL);
        var signinURL= AETN.signinURL+encodeURIComponent(getURL);
        trace('signinURL='+signinURL);

        var proxyWithSigninURL=  AETN.proxyWithSigninURL+       signinURL;
        var proxyWithSigninURL = proxyWithSigninURL.replace("&url=http://", "&url=");

        console.log('proxyWithSigninURL='+decodeURIComponent(proxyWithSigninURL));
    oboe(proxyWithSigninURL).done(function(data){
            var token=String(data.contents);
            console.log('received token: '+token)
            var urlwithtoken=getURL+'&sig='+token;

        var adobeToken=AECustomPlayer.sessionVars.media.customData.auth;
        if(adobeToken!=undefined&&adobeToken!=''){
            urlwithtoken+='&auth='+encodeURIComponent(adobeToken);
        }


            console.log('building url with token: '+urlwithtoken);
        trace('building url with token: '+urlwithtoken,'#000','#0f0');
        //urlwithtoken = urlwithtoken.replace('aestreamingads-vh.akamaihd.net','aestreamingads-vh.akamaihd-staging.net');

            oboe({
                url: parent._returnProxyUrl(urlwithtoken)
            }).done(function(data){
                //alert('urlwithtoken:'+urlwithtoken);

                //data.status.url = data.status.url.replace('aestreamingads-vh.akamaihd.net','aestreamingads-vh.akamaihd-staging.net');

                console.log('streamURL:'+data.status.url);
                trace('if fail pls check URL 1:'+data.status.url,'#000','#f00');
                success_cb(data.status.url);
            }).fail(function(){
                trace('pls check URL 1:'+urlwithtoken,'#000','#f00');
                trace('pls check URL 2:'+parent._returnProxyUrl(urlwithtoken),'#000','#f00');
                fail_cb('failed with Signed URL: '+urlwithtoken)

            });
        }).fail(function(){
            fail_cb('failed on Getting Token from: '+proxyWithSigninURL);
        });
        };


        //http://link.theplatform.com/s/xc6n8B/hhUkMNxBStLN?switch=hls&assetTypes=medium_video_s3&mbr=true&metafile=false&sig=0056abce7eae15e37aa823a73fa56d6a8a6bc6b1dadd3e77e6733363723374


        //parent._var({childObjects:parent._var().target.children()});
        //var childObjects=parent._var().childObjects;
        //parent._var().childtrack(parent._var().currentslide);  //do tracking
        //this._multijump(index);
        //this._var().childtrack(indexCandidate);



    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});



