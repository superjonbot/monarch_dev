/**
 * swipeset Module
 *
 * Date: 12/21/13 : 1:20 PM
 *
 * NOTE: This is the daddy of all swipe objects
 *
 * USAGE:
 *
 */
define(['jquery','underscore', 'modules/definitions/standardmodule','hammer','tweenmax','scrollto'], function ($, _, parentModel,Hammer,TweenMax,Scrollto) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'swipeset', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)

            target:$('#yourdiv_X'),  //target object that contains children to control
            virtualchildren:false,  //false expects child divs as breaks, true makes virtual breaks!
            virtualchild_overlap:0,  //overlap between virtual children
            yscroll:false,  //use x or y scrolling
            Xscrolldetect:'release dragleft dragright swipeleft swiperight', //remove options to omit
            Yscrolldetect:'release dragup dragdown swipeup swipedown', //remove options to omit
            currentslide:0, //defines default slide on start
            lastdebouncedslide: 0,
            usetouchbinds:true, //adds touchbinds to the target
            enable_touchbinds:true,  //enables/disables touchbinds
            snaponrelease:true,  //on stop scroll, will snap to closest child
            callback:function(o){},  //runs when swipepanel is made
            childchange:function(index){

                var self=this.parent;
                var destination=undefined;
                var animationvars=undefined;
                    console.log("index: "+index)

                if(!self._var().yscroll){
                    destination=self._var().childVars[index].XoffsetfromScroll;
                    animationvars={scrollTo:{x:destination}, ease:Power4.easeOut}
                }else{
                    destination=self._var().childVars[index].YoffsetfromScroll;
                    animationvars={scrollTo:{y:destination}, ease:Power4.easeOut}

                }


                TweenMax.to(self._var().target,.7,animationvars );

            }, //child changing function
            //LIVE SCROLLING CALLBACKS
            onscroll_callback:function(o){
                //console.log('moving :'+ o.currentposition+' [ '+ o.currentpercent+'% ]')
            }, //on scrolling callback
            onscroll_start:function(o){
                //console.log('startmove :'+ o.currentposition+' [ '+ o.currentpercent+'% ]')
            },  //on before scrolling callback
            onscroll_debounce_callback:function(o){
                //console.log('endmove :'+ o.currentposition+' [ '+ o.currentpercent+'% ] @slide: '+this.currentslide)
            },  //on end scrolling callback
            onscroll_debounce_rate:250,  //function debouncing rate

            //INTERNAL Variables
            scrollsize:undefined,
            currentposition:undefined,
            currentpercent:undefined,

            targetXsize:undefined,
            targetYsize:undefined,

            targetwidth:undefined,
            targetheight:undefined,
            totalslides:undefined,

            childObjects:undefined,
            childVars:undefined,
            childVar_setter:function(index,value){
                var self = this;

                self.index=index;
                self.childOBJ=value;
                self.target=$(value);
                self.Xsize=self.target.outerWidth();
                self.Ysize=self.target.innerHeight();
                self.Xdefault=Math.round(self.target.position().left);
                self.Ydefault=Math.round(self.target.position().top);
                self.XoffsetfromScroll=undefined;//xoffset;
                self.YoffsetfromScroll=undefined;//yoffset;

                //live variables
                //self.OffsetfromFocus=undefined;
            },


            ObjHammer:undefined,

            isbeingdragged:false, //internal!
            startmove:true,//internal!
            busy:false

        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {
        this.notify('Trace','_startlisteners');
        var parent=this;
        var myID=  this._id();
        _notify.add(this._id(), function() {

            return {
                onQuickjump:function(o){
                    if(o.senderID==myID){
                        parent.quickjump(o.data.index)
                    }
                },
                onJump:function(o){
                    if(o.senderID==myID){
                        parent.jump(o.data.index)
                    }
                },
                onPagejump:function(o){
                    if(o.senderID==myID){
                        parent.pagejump(o.data.index)
                    }
                },
                onNext:function(o){
                    if(o.senderID==myID){
                        parent.next()
                    }
                },
                onPrev:function(o){
                    if(o.senderID==myID){
                        parent.prev()
                    }
                }




            }
        }());
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init = function () {
        /*
         if(<object>!=exist){
         <object>.create();
         }else{
         <object>.show(); //just show it and start listeners
         };
         * */
        this.notify('Trace', 'init');
        this._var({busy:true});
        this._startlisteners();//start this module's listeners
        var self=this;

        this.refresh();

    };

    _thizOBJ_.prototype.refresh = function () {
        var self= this;
        self._var({busy:true});
        self.notify('Trace', 'refresh');
        self.getpanelsizes();

        (!self._var().virtualchildren)?self._childrentochildObjects():self._virtualspacetochildObjects();





        self.quickjump(self._var().currentslide)


       //Attach touch and drag handler
        // var handlerdetect=(!self._var().yscroll)?"release dragleft dragright swipeleft swiperight":"release dragup dragdown swipeup swipedown";



        var handlerdetect=(!self._var().yscroll)?self._var().Xscrolldetect:self._var().Yscrolldetect;



        if(self._var().usetouchbinds){
        //alert('self._var().ObjHammer:'+self._var().ObjHammer);

        //if(typeof self._var().ObjHammer=='object'){self._var().target.Hammer.destroy()};


        //if(typeof self._var().ObjHammer!='object'){

        self._var().ObjHammer= Hammer (self._var().target,{ drag_lock_to_axis: true }).on(handlerdetect, self._bindHandler(self));







       // } else {alert('no hammer')}
            //self._var().ObjHammer= Hammer (self._var().target,{ drag_lock_to_axis: true });
        //self._var().ObjHammer.off(handlerdetect, self._bindHandler(self));
        //self._var().ObjHammer.on(handlerdetect, self._bindHandler(self));




        }

        //attach scroll handler
        self._var().target.unbind("scroll");
        self._var().target.scroll(self._onscroll(self))


        self._var({busy:false});
        if(self._var().callback!=undefined){self._var().callback(self) }



    };
    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };
    // UNIQUE MODULE METHODS

//internal
    _thizOBJ_.prototype._childrentochildObjects = function(){
        var self=this;
        self._var({childObjects:self._var().target.children()});
        var childObjects=self._var().childObjects;

        self._var({
            totalslides:childObjects.length,
            childVars:[]
        });

        //build childVars
        $.each(childObjects,function(index, value ){
            childVar_setter_local=self._var().childVar_setter;
            self._var().childVars.push(new childVar_setter_local(index,value))

        });
        $.each(self._var().childVars,function(index, value ){
            var zeroXdefault=self._var().childVars[0].Xdefault;
            var zeroYdefault=self._var().childVars[0].Ydefault;
            var marginOffsetX=value.target.css('marginLeft');
            var marginOffsetY=value.target.css('marginTop');

            value.XoffsetfromScroll=Math.round(value.Xdefault-zeroXdefault);
            value.YoffsetfromScroll=Math.round(value.Ydefault-zeroYdefault);

            if(marginOffsetX!=undefined){value.XoffsetfromScroll+=parseInt(marginOffsetX, 10)};
            if(marginOffsetY!=undefined){value.YoffsetfromScroll+=parseInt(marginOffsetY, 10)};
        });

        //set scrollsize

/*        var scrollsize=undefined;
        if(!self._var().yscroll){

            scrollsize=self._var().childVars[self._var().totalslides-1].XoffsetfromScroll
        }else{
            scrollsize=self._var().childVars[self._var().totalslides-1].YoffsetfromScroll

        };*/

        var totalscrollarea=(!self._var().yscroll)?self._var().targetwidth:self._var().targetheight;
        var singlechildsize=(!self._var().yscroll)?self._var().targetXsize:self._var().targetYsize;
        var scrollsize=totalscrollarea-singlechildsize;


        self._var({
            scrollsize:scrollsize
        });


      //  alert(self._var().target.width()-400)
       // alert(self._var().target.innerWidth()-400)
        //alert(self._var().target.outterWidth())
      //  alert(self._var().scrollsize+' : '+(self._var().targetwidth-(self._var().target.width()+400)))
      //  alert(self._var().scrollsize+' : '+(self._var().targetwidth-(self._var().target.innerWidth()+400)))
    };
    _thizOBJ_.prototype._virtualspacetochildObjects = function(){
        var self=this;
        self._var({childObjects:self._var().target.children()});

        var totalscrollarea=(!self._var().yscroll)?self._var().targetwidth:self._var().targetheight;
        var singlechildsize=(!self._var().yscroll)?self._var().targetXsize:self._var().targetYsize;
        singlechildsize=singlechildsize-self._var().virtualchild_overlap;

        var scrollsize=totalscrollarea-singlechildsize;

        //var latestsizes=self.getpanelsizes();
        //alert(totalscrollarea+' : '+scrollsize)

        var tempStopArray=[];
        var childcount=Math.floor(scrollsize/singlechildsize);
        for(tempval=0;tempval<childcount+1;tempval++){
            tempStopArray.push(tempval*singlechildsize)
        }
        tempStopArray.push(scrollsize);

        //alert(scrollsize)
        //alert(tempStopArray)



       // var childObjects=self._var().childObjects;

        self._var({
            totalslides:tempStopArray.length,
            childVars:[],
            scrollsize:scrollsize
        });

        //build childVars
        $.each(tempStopArray,function(index, value ){
            self._var().childVars.push({
                XoffsetfromScroll:value,
                YoffsetfromScroll:value
            })

        });



      /*  $.each(self._var().childVars,function(index, value ){
            var zeroXdefault=self._var().childVars[0].Xdefault;
            var zeroYdefault=self._var().childVars[0].Ydefault;
            var marginOffsetX=value.target.css('marginLeft');
            var marginOffsetY=value.target.css('marginTop');

            value.XoffsetfromScroll=Math.round(value.Xdefault-zeroXdefault);
            value.YoffsetfromScroll=Math.round(value.Ydefault-zeroYdefault);

            if(marginOffsetX!=undefined){value.XoffsetfromScroll+=parseInt(marginOffsetX, 10)};
            if(marginOffsetY!=undefined){value.YoffsetfromScroll+=parseInt(marginOffsetY, 10)};
        });

        //set scrollsize
        self._var({
            scrollsize:(!self._var().yscroll)?self._var().childVars[self._var().totalslides-1].XoffsetfromScroll:self._var().childVars[self._var().totalslides-1].YoffsetfromScroll
        });

*/
        //alert(self._var().scrollsize)

        //  alert(self._var().target.width()-400)
        // alert(self._var().target.innerWidth()-400)
        //alert(self._var().target.outterWidth())
        //  alert(self._var().scrollsize+' : '+(self._var().targetwidth-(self._var().target.width()+400)))
        //  alert(self._var().scrollsize+' : '+(self._var().targetwidth-(self._var().target.innerWidth()+400)))
    };
    _thizOBJ_.prototype.getpanelsizes = function () {
        var self= this;
        var objreturn={}
        objreturn.totalslides=self._var().totalslides;

        objreturn.targetwidth=self._var().targetwidth=self._var().target[0].scrollWidth;
        objreturn.targetheight=self._var().targetheight=self._var().target[0].scrollHeight;

        objreturn.targetXsize=self._var().targetXsize=self._var().target.innerWidth();
        objreturn.targetYsize=self._var().targetYsize=self._var().target.innerHeight();

        return objreturn;
    };
    _thizOBJ_.prototype.getclosestindex=function(position){
        var self=this;
        var Offsets=_.map(self._var().childVars, function(idx){
            //return Math.abs(idx.XoffsetfromScroll-index);

            return (!self._var().yscroll)?Math.abs(idx.XoffsetfromScroll-position):Math.abs(idx.YoffsetfromScroll-position)

        });
        //makes array of differences of current x stop and all stops
        var closestdiff=_.reduce(Offsets, function(memo, val) {
            return (val<memo)?val:memo;
        }, 10000);
        //get lowest number from array
        var indextoReturn = _.indexOf(Offsets, closestdiff);
        //get index of that number from array of differences
        //console.log('old:'+ self._var().currentslide+' new:'+indextoReturn)
        self._var().currentslide=indextoReturn;
        return indextoReturn;
    };
    _thizOBJ_.prototype._returncurrentposition= function(){
        var self=this;
        return (!self._var().yscroll)?self._var().target.scrollLeft():self._var().target.scrollTop()
    };
    _thizOBJ_.prototype._gotoposition= function(getdestination){
        var self=this;
        (!self._var().yscroll)?self._var().target.scrollLeft(getdestination):self._var().target.scrollTop(getdestination)
    };
    _thizOBJ_.prototype._gotopercent= function(getpercent){
        var self=this;
        var scrollsize=self._var().scrollsize
        //var currentposition=self._returncurrentposition();
        var jumptoposition=scrollsize/(100/getpercent);
        self._gotoposition(jumptoposition);
    };
//controls
    _thizOBJ_.prototype.selfie= function (o){

        console.log('SELFIEEEE!!!')
        var self=this;
        var options={
            yscroll:self._var().yscroll,
            quick:false
        };
        options = $.extend(options, o);

        if(!options.yscroll){
            //xscroll
            var thisindex=self.getclosestindex(self._returncurrentposition());


            if(!options.quick){
                self.jump(thisindex);
            }else{
                self.quickjump(thisindex);
            }

        }else{
            //yscroll
            var thisindex=self.getclosestindex(self._returncurrentposition());


            if(!options.quick){
                self.jump(thisindex);
            }else{
                self.quickjump(thisindex);
            }
        }


    };
    _thizOBJ_.prototype.next= function (o){
        var self=this;
        var options={
           quick:false
        };
        options = $.extend(options, o);
        var thisindex=(!self._var().yscroll)?self.getclosestindex(self._returncurrentposition()):self.getclosestindex(self._var().target.scrollTop());
        //console.log('thisindex:'+thisindex+' : '+self._var().currentslide)

        var nextidx=(thisindex==self._var().totalslides-1)?thisindex:thisindex+1;

        //the following compares next destination to the last debounce slide
        var diff=Math.abs(nextidx-Number(self._var().lastdebouncedslide))
        console.log("current:"+self._var().lastdebouncedslide+" index: "+nextidx+" diff:"+diff);
        if(diff>1){nextidx-=(diff-1)};
        //comparetodebounce complete

        (!options.quick)?self.jump(nextidx):self.quickjump(nextidx);

    };
    _thizOBJ_.prototype.prev= function (o){
        var self=this;
        var options={
            quick:false
        };
        options = $.extend(options, o);
        var thisindex=(!self._var().yscroll)?self.getclosestindex(self._returncurrentposition()):self.getclosestindex(self._var().target.scrollTop());
        var nextidx=(thisindex==0)?0:thisindex-1;

        //the following compares next destination to the last debounce slide
        var diff=Math.abs(nextidx-Number(self._var().lastdebouncedslide))
        console.log("current:"+self._var().lastdebouncedslide+" index: "+nextidx+" diff:"+diff);
        if(diff>1){nextidx+=(diff-1)};
        //comparetodebounce complete

        (!options.quick)?self.jump(nextidx):self.quickjump(nextidx);


    };
    _thizOBJ_.prototype.jump = function (index) {

        var self=this;
        self._var().childchange(index,self)

   /*     var self=this;
        var destination=undefined;
        var animationvars=undefined;


        if(!self._var().yscroll){
            destination=self._var().childVars[index].XoffsetfromScroll;
            animationvars={scrollTo:{x:destination}, ease:Power4.easeOut}
            }else{
            destination=self._var().childVars[index].YoffsetfromScroll;
            animationvars={scrollTo:{y:destination}, ease:Power4.easeOut}

        }


        TweenMax.to(self._var().target,.5,animationvars );*/

    };
    _thizOBJ_.prototype.pagejump = function (index) {
        this.jump(index);
    }
    _thizOBJ_.prototype.quickjump = function (index) {
        var self=this;

        if(!self._var().yscroll){
            var destination=self._var().childVars[index].XoffsetfromScroll;
           // self._var().target.scrollLeft(destination)
        }else{
            var destination=self._var().childVars[index].YoffsetfromScroll;
            //self._var().target.scrollTop(destination)
        }

        self._gotoposition(destination)

    };

//handlers
    _thizOBJ_.prototype._bindHandler = function(self){

        var lastCase=undefined;
        var touchpoint=undefined;

        var targetposition=function(){
            //console.log('touchpoint:'+touchpoint+' lastCase:'+lastCase)
            touchpoint=self._returncurrentposition();
        }

        var casechecks=undefined;
        var directions=undefined;

           if(!self._var().yscroll){
               casechecks=['dragright','dragleft','swipeleft','swiperight','release'];
               directions=[Hammer.DIRECTION_RIGHT,Hammer.DIRECTION_LEFT];
            } else {
               casechecks=['dragdown','dragup','swipeup','swipedown','release'];
               directions=[Hammer.DIRECTION_DOWN,Hammer.DIRECTION_UP];
            }


        var eventHandler = function (ev){
        // disable default action
        ev.gesture.preventDefault();

        if(ev.type!=lastCase){ targetposition(); };


         if(self._var().enable_touchbinds){

        switch(ev.type) {
            //dragright - dragleft
            //release
            case casechecks[4]:
                console.log('case4')
                self._var().isbeingdragged=false;
                targetposition();

                if(self._var().snaponrelease){
                    //console.log('selfie!')
                    self.selfie();
                } else {
                    //alert('hmmmm')


                    var currentvars={};
                    currentvars.scrollsize=self._var().scrollsize
                    currentvars.currentposition=self._returncurrentposition();
                    currentvars.currentpercent=(100/(currentvars.scrollsize/currentvars.currentposition));


                    self._debounce(self)(currentvars);
                }



                ev.gesture.stopDetect();
                break;
            case casechecks[0]:
            case casechecks[1]:
                console.log('case0/1');// ['+self._returncurrentposition()+']')
                self._var().isbeingdragged=true;


                var pane_offset=touchpoint;
                var drag_offset = (!self._var().yscroll)?ev.gesture.deltaX:ev.gesture.deltaY;

                //+ or - depending on direction
                if(ev.gesture.direction == directions[0]){
                    drag_offset=-Math.abs(drag_offset)
                } else if(ev.gesture.direction == directions[1]){
                    drag_offset=Math.abs(drag_offset)
                };


                //compute destination
                var destination=(pane_offset+drag_offset);

                //update object
                (!self._var().yscroll)?self._var().target.scrollLeft(destination):self._var().target.scrollTop(destination);
                //ev.gesture.stopDetect();
                break;
            //swipeleft
            case casechecks[2]:
                console.log('case2')
                self.next();
                self._var().isbeingdragged=false;
                ev.gesture.stopDetect();
                break;
            //swiperight
            case casechecks[3]:
                console.log('case3')
                self.prev();
                self._var().isbeingdragged=false;
                ev.gesture.stopDetect();
                break;

        }

         }



        lastCase=ev.type; //log last ev type


    };

        return  eventHandler;


    };
   _thizOBJ_.prototype._debounce= function(self){
       //var self=this;
       var debouncer=       _.debounce(function(currentvars){

           //compare current location with a acceptable release point
           var currentposition=    self._returncurrentposition();
           var closeindex=self.getclosestindex(currentposition);
           var destination=undefined;

           if(!self._var().yscroll){
               destination=self._var().childVars[closeindex].XoffsetfromScroll;
           }else{
               destination=self._var().childVars[closeindex].YoffsetfromScroll;
           }

           var difference=(Math.abs(currentposition-destination));

           if(difference>1&&!self._var().isbeingdragged&&self._var().snaponrelease){

               self.selfie();

           } else if(difference<2||(!self._var().isbeingdragged&&!self._var().snaponrelease)) {


               if(!self._var().startmove){

                   //set the debounced last slide so stuff doesn't skip
                   self._var().lastdebouncedslide=self._var().currentslide;


                   self._var().onscroll_debounce_callback(currentvars);
                   self._var({startmove:true});
                   console.log(self._var().lastdebouncedslide)
               }


           }

       },self._var().onscroll_debounce_rate)



      return  debouncer;


   };











    _thizOBJ_.prototype._onscroll= function(self){



        var Offsets=_.map(self._var().childVars, function(idx){
            return (!self._var().yscroll)?Math.abs(idx.XoffsetfromScroll):Math.abs(idx.YoffsetfromScroll)
        });

        //debounced srolling methods
   /*    var debouncefunc= _.debounce(function(currentvars){

            //compare current location with a acceptable release point
            var currentposition=    self._returncurrentposition();
            var closeindex=self.getclosestindex(currentposition);
            var destination=undefined;

            if(!self._var().yscroll){
                destination=self._var().childVars[closeindex].XoffsetfromScroll;
            }else{
                destination=self._var().childVars[closeindex].YoffsetfromScroll;
            }

            var difference=(Math.abs(currentposition-destination));

            if(difference>1&&!self._var().isbeingdragged&&self._var().snaponrelease){

                self.selfie();

            } else if(difference<2||(!self._var().isbeingdragged&&!self._var().snaponrelease)) {

//console.log('COMPARE: '+currentposition+' & '+destination+' difference='+(Math.abs(currentposition-destination)))
                self._var().onscroll_debounce_callback(currentvars);
                self._var({startmove:true});
            }

        },self._var().onscroll_debounce_rate)
*/
        var debouncefunc=self._debounce(self);
        var handler= function(){
            var currentvars={};
            currentvars.scrollsize=self._var().scrollsize
            currentvars.currentposition=self._returncurrentposition();
            currentvars.currentpercent=(100/(currentvars.scrollsize/currentvars.currentposition));

            //start onscroll functions
            if(self._var().startmove){
                self._var().onscroll_start(currentvars);
                self._var({startmove:false})
            }

            self._var().onscroll_callback(currentvars);
            debouncefunc(currentvars)
        }

        return handler
    };



    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


//knockout blurb
 /*
 define(['knockout','modules/view/viewmodels/application4vm'],
 function (ko,appViewModel) {
    ko.applyBindings(new appViewModel());
 });
 */
