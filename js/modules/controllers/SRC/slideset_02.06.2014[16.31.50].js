/**
 * slideset Module Definition
 *
 * Date: <inZertDATe>
 *
 * NOTE: This is the Daddy of all slide objects
 *
 */
define(['jquery','modules/definitions/standardmodule','tweenmax','underscore','modernizr'],function ($,parentModel,TweenMax,_,Modernizr) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_( o ){

        var defaults={
            type:'slideset', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            lasteditby:'', //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)
            target:$('#yourdiv'),  //target object that contains children to control
            childObjects:undefined,
            currentslide:0,
            totalslides:undefined,
            loopslides:false,
            toplevelZ:500,
            autoZ:true,

            child_stack:[],
            child_Zstack:[],
            child_Mstack:[],

            child_offstack:[],
            child_centerstack:[],
            child_stackpile_left:[],
            child_stackpile_right:[],
            errorfunctions:{
                childchange:function(data){console.log('error: same index')}
            },
            interval:undefined,
            busy:false,
            unfocusALL:true,
            childtrack:function(index){
                var tracking=function(index){
                    console.log('some tracking function you defined for scene# '+index);
                }

                if(index!=this.lasttrack){
                    tracking(index);
                    this.lasttrack=index;
                };

            }, //tracking on focus change
            childinit:function(index,currentslide){},//applies to all slides on load
            childchange:function(index,o){
                var self=this;

                self.parent._setstacks(index); //set stacks
                self.parent._getZorder(index); //pop Zorder and multiplier
                if(self.parent._var().autoZ){self.parent._putZorder(index)}; //auto zplace
                //console.log(this.child_stack+' ::: '+this.child_Zstack+' ::: '+this.child_Mstack)

                if(this.currentslide===index){
                    //use error callback if itz the same
                    this.errorfunctions['childchange']();
                }else{




                    var passVars={index:index,Zindex:self.child_Zstack,Multiplier:self.child_Mstack};



                    /*FOCUS INDEX*/
                    focusvars = $.extend(passVars,o)
                    this.child_focus(focusvars);

                    /*UNFOCUS Last Index or UNFOCUS Everything Else*/
                    if(this.unfocusALL){
                     //UNFOCUS EVERYTHING
                        $.each(self.parent._var().child_stackpile_left,function(offset,value){
                            var leftvars= $.extend({unfocusedindex:value,offset:offset},passVars);
                            leftvars = $.extend(leftvars,o)
                            self.parent._var().child_unfocus(leftvars);



                        })
                        $.each(self.parent._var().child_stackpile_right,function(offset,value){
                            var rightvars= $.extend({unfocusedindex:value,offset:offset},passVars);
                            rightvars = $.extend(rightvars,o)
                            self.parent._var().child_unfocus(rightvars);




                        })
                    }else{
                     //UNFOCUS JUST THE ONE
                        var unfocusVars= $.extend({unfocusedindex:this.currentslide},passVars);
                        this.child_unfocus(unfocusVars);
                    }









                this.currentslide=index;

                }
            },  //raw child changing function
            child_focus:function(o){
                var argz={
                    Zindex:undefined,  //proposed z indexes
                    Multiplier:undefined,  //proposed multipliers
                    index:undefined,
                    speed:.5,
                    tweenvars:{
                        x:0,
                        y:0,
                        rotation:0,
                        rotationY:0,
                        rotationZ:0,
                        rotationX:0,
                        scale:1,
                        opacity:1
                    }
                };
                argz= $.extend(argz,o);
                var tempObject= $(this.childObjects[argz.index]);
                argz= $.extend(argz,{
                    object:tempObject
                });


                if(Modernizr.csstransitions){
                TweenMax.to(argz.object,argz.speed,argz.tweenvars);}
                else{argz.object.show()}

            },  //do this to focused slide
            child_unfocus:function(o){
                var argz={
                    Zindex:undefined,  //proposed z indexes
                    Multiplier:undefined,  //proposed multipliers
                    index:undefined, //of current or candidate
                    unfocusedindex:undefined,
                    speed:1,  // 1 for animation
                    offset:0,
                    tweenvarsIN:{
                        x:-400,
                        y:0,
                        rotation:10,
                        rotationY:0,
                        rotationZ:0,
                        rotationX:0,
                        scale:.5,
                        opacity:0
                    },
                    tweenvarsOUT:{
                        x:400,
                        y:0,
                        rotation:-10,
                        rotationY:0,
                        rotationZ:0,
                        rotationX:0,
                        scale:.5,
                        opacity:0
                    }
                };
                argz= $.extend(argz,o);
                var tempObject= $(this.childObjects[argz.unfocusedindex]);
                argz= $.extend(argz,{
                    object:tempObject
                });



                argz.tweenvarsIN.y-=(30*argz.offset);
                argz.tweenvarsOUT.y-=(30*argz.offset);
                argz.tweenvarsIN.x+=((10*argz.offset)*(2*argz.offset));
                argz.tweenvarsOUT.x-=((10*argz.offset)*(2*argz.offset));
                argz.tweenvarsIN.rotation+=(15*argz.offset);
                argz.tweenvarsOUT.rotation+=(15*argz.offset);
                argz.tweenvarsIN.opacity+=.9- (.15*argz.offset);
                argz.tweenvarsOUT.opacity+=.9-(.15*argz.offset);



                if(Modernizr.csstransitions){
                if(argz.unfocusedindex<argz.index){
                    TweenMax.to(argz.object,argz.speed,argz.tweenvarsIN);
                }else {
                    TweenMax.to(argz.object,argz.speed,argz.tweenvarsOUT);
                };
                }
                else{
                    argz.object.hide();
                }


            },  //do this to unfocused slide
            _multijump:function(index){

                var parent=this;


                var counter = { tempvar: parent.currentslide };
                var lastindex=parent.currentslide;
                var difference= Math.abs(index-parent.currentslide);
                var animtime=difference*.2;
                //console.log(animtime);

                TweenMax.to(counter, animtime, {
                    tempvar: index,
                    onUpdate: function () {


                        var targetvalue=Math.ceil(counter.tempvar);


                        if(targetvalue!=lastindex){
                            parent.childchange(targetvalue);
                         //   console.log(lastindex+'> '+targetvalue)
                            lastindex=targetvalue;
                        }

                    }});
            }/*,  //internal pagejumping function
            _quickjump:function(index){

                var self=this;
                self.childchange(index)



            }      //internal quickjumping function*/

        };
        defaults= $.extend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };


    _thizOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners =function(){

        this.notify('Trace','_startlisteners');
        var parent=this;
        var myID=  this._id();
        _notify.add(this._id(), function() {

            return {
                onQuickjump:function(o){
                    if(o.senderID==myID){
                        parent.quickjump(o.data.index)
                    }
                },
                onJump:function(o){
                    if(o.senderID==myID){
                        parent.jump(o.data.index)
                    }
                },
                onPagejump:function(o){
                    if(o.senderID==myID){
                        parent.pagejump(o.data.index)
                    }
                },
                onNext:function(o){
                    if(o.senderID==myID){
                        parent.next()
                    }
                },
                onPrev:function(o){
                    if(o.senderID==myID){
                        parent.prev()
                    }
                }




            }
        }());


    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init =function(){
//alert(Modernizr.csstransitions)
        this.notify('Trace','init');
        this._startlisteners();//start this module's listeners
        this.refresh();
    };
    _thizOBJ_.prototype.refresh =function(){
        this.notify('Trace','refresh');
        this._childrentochildObjects();
    };
    _thizOBJ_.prototype.kill =function(){
    /*
    <object>.hide(); //just hide it and kill listeners
    * */
        this.notify('Trace','kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype._childrentochildObjects = function(){
        var parent=this;
        parent._var({childObjects:parent._var().target.children()});
        var childObjects=parent._var().childObjects;
        parent._var({totalslides:childObjects.length,child_stack:[],child_offstack:[],child_centerstack:[],child_stackpile_left:[],child_stackpile_right:[]}); //set total slide number & clear stack


//build child stack
        $.each(childObjects,function(index, value ){
            parent._var().child_stack.push(index)//add to stack

          //  if(index!=parent._var().currentslide){
                parent._var().childinit.call($(value),index,parent._var().currentslide)
           /* } else{
                parent._var().childcurrentinit.call($(value),index)
            }*/

        });
//console.log(parent._var().child_stack)
           /* */



  //PLACE OBJECTS
        parent._setstacks(parent._var().currentslide); //set stacks
        parent._getZorder(parent._var().currentslide); //pop Zorder and multiplier
        parent._var().child_focus({index:parent._var().currentslide,speed:0,Zindex:parent._var().child_Zstack,Multiplier:parent._var().child_Mstack});


        if(parent._var().autoZ){parent._putZorder(parent._var().currentslide)}; //auto zplace

        //console.log(parent._var().child_stack+' ::: '+parent._var().child_Zstack+' ::: '+parent._var().child_Mstack)

        $.each(parent._var().child_stackpile_left,function(offset,value){
            parent._var().child_unfocus({index:parent._var().currentslide,unfocusedindex:value,offset:(offset),speed:0,Zindex:parent._var().child_Zstack,Multiplier:parent._var().child_Mstack});

        })
        $.each(parent._var().child_stackpile_right,function(offset,value){
            parent._var().child_unfocus({index:parent._var().currentslide,unfocusedindex:value,offset:(offset),speed:0,Zindex:parent._var().child_Zstack,Multiplier:parent._var().child_Mstack});
        })

  //Set stacks and z-order





        parent._var().childtrack(parent._var().currentslide);  //do tracking

    };
    _thizOBJ_.prototype._putZorder = function(index){
        var parent=this;
        //put object in descending z index order based on stacks
        var tempZindex=parent._var().toplevelZ;
        var currentObject= $(parent._var().childObjects[index]);
        currentObject.css('position','absolute').css('z-index',tempZindex);
        $.each(parent._var().child_stackpile_left,function(index,value){
            var currentObject= $(parent._var().childObjects[value]);
            currentObject.css('position','absolute').css('z-index',tempZindex-(index+1));
        })
        $.each(parent._var().child_stackpile_right,function(index,value){
            var currentObject= $(parent._var().childObjects[value]);
            currentObject.css('position','absolute').css('z-index',tempZindex-(index+1));
        })

    };
    _thizOBJ_.prototype._getZorder = function(index){
        var parent=this;
        var tempZindex=parent._var().toplevelZ;

        parent._var().child_Zstack=[];
        parent._var().child_Mstack=[];

        parent._var().child_Zstack[index]=tempZindex;
        parent._var().child_Mstack[index]=0;

        var currentObject= $(parent._var().childObjects[index]);
       $.each(parent._var().child_stackpile_left,function(index,value){
           parent._var().child_Zstack[value]=tempZindex-(index+1);
            parent._var().child_Mstack[value]=index+1;
          })
        $.each(parent._var().child_stackpile_right,function(index,value){
            parent._var().child_Zstack[value]=tempZindex-(index+1);
            parent._var().child_Mstack[value]=index+1;
           })


        //return Zcandidates;

    };
    _thizOBJ_.prototype._setstacks = function(index){
        var parent=this;
        //make offset stack - start
        var splitpoint=_.indexOf(parent._var().child_stack, index);
        var splitB = _.rest(parent._var().child_stack, [splitpoint]);
        var splitA = _.first(parent._var().child_stack, [splitpoint]);
        var offsetarray = splitB.concat(splitA);
        parent._var({child_offstack:offsetarray});
        //make offset stack - end
        //make center stack - start
        var centerindex=Math.floor(parent._var().child_stack.length/2);
        var splitB = _.rest(parent._var().child_offstack, [centerindex+1]);
        var splitA = _.first(parent._var().child_offstack, [centerindex+1]);
        var offsetarray = splitB.concat(splitA);
        parent._var({child_centerstack:offsetarray});
        var child_stackpile_left = _.first(parent._var().child_stack, [splitpoint]).reverse();
        var child_stackpile_right = _.rest(parent._var().child_stack, [splitpoint+1]);
        parent._var({child_stackpile_left:child_stackpile_left,child_stackpile_right:child_stackpile_right});


        //make center stack - end

/*        console.log(' stack is:'+parent._var().child_stack+' offstack is:'+parent._var().child_offstack+' centerstack is:'+parent._var().child_centerstack+' L:'+parent._var().child_stackpile_left+' R:'+parent._var().child_stackpile_right)*/


    };

    //CONTROLS
    _thizOBJ_.prototype.pagejump = function(index){
        //alert(index+' : '+this._var().currentslide);
        //alert(Math.abs(index-this._var().currentslide));
        var difference= Math.abs(index-this._var().currentslide);
//alert(difference)
        if(difference>1){
           // alert('forward to _multijump');
            this._multijump(index);
        } else {
          //  alert('forward to jump');
            this.jump(index);
        };

    };
    _thizOBJ_.prototype.jump = function(index){
        //alert('jump!')
        this._var().childtrack(index);
        this._var().childchange(index);

    };
    _thizOBJ_.prototype.quickjump = function(index){
        //alert('jump!')
        this._var().childtrack(index);
        this._var().childchange(index,{speed:0});

    };

    _thizOBJ_.prototype._multijump = function(index){
        this._var().childtrack(index);
        this._var()._multijump(index);

    };
    _thizOBJ_.prototype.next = function(){
        var parent=this;
        var childObjects=parent._var().childObjects;
        var objectcount=childObjects.length;
        var indexCandidate=parent._var().currentslide;
        indexCandidate+=1;
        if(indexCandidate==objectcount){
            this._var().loopslides?indexCandidate=0:indexCandidate=(objectcount-1);
        }
        this._var().childtrack(indexCandidate);
        this._var().childchange(indexCandidate);
    };
    _thizOBJ_.prototype.prev = function(){
        var parent=this;
        var childObjects=parent._var().childObjects;
        var objectcount=childObjects.length;
        var indexCandidate=parent._var().currentslide;
        indexCandidate-=1;
        if(indexCandidate<0){
            this._var().loopslides?indexCandidate=(objectcount-1):indexCandidate=0;
        }
        this._var().childtrack(indexCandidate);
        this._var().childchange(indexCandidate);
    };



    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});



