/*!
 * Chromecast Sender Module
 * by Jonathan Robles
 * 12-17-15
 *
 *
 */







define(['modules/definitions/standardmodule','underscore','oboe'],function (parentModel,_,oboe) {

    // external global function(s) //comment out on init!!!
    window.globalFunction=undefined;
    //window.testbutton=undefined;

    function _thizOBJ_( o ){

        var defaults={
            scope:true,
            type:'chromecast_sender', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            lasteditby:'',
            mediaElement:document.getElementById('vid'),


            publicdata:{
                namespace:"urn:x-cast:com.aenetworks.cast.media",
                appConfig:{
                    statusText:'This is the apps status text',
                    maxInactivity:6000
                },
                somerandomDATA:'some random stuff to send to the sender'}



            //, //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)
            //target:$('#yourdiv'),  //target object that contains children to control
            //interval:undefined,
            //busy:false,
            //someinternalfunction:function(o,index){
            //    var parent=this;
            //    self.parent._someexternalfunction(index); //set stacks
            //}
        };
        defaults= this.deepExtend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };


    _thizOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners =function(){

        /*

         this.notify('Trace','_startlisteners');
         var parent=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {
         onQuickjump:function(o){
         if(o.senderID==myID){
         parent.quickjump(o.data.index)
         }
         },
         onJump:function(o){
         if(o.senderID==myID){
         parent.jump(o.data.index)
         }
         },
         onPagejump:function(o){
         if(o.senderID==myID){
         parent.pagejump(o.data.index)
         }
         },
         onNext:function(o){
         if(o.senderID==myID){
         parent.next()
         }
         },
         onPrev:function(o){
         if(o.senderID==myID){
         parent.prev()
         }
         }




         }
         }());

         */


    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init =function(){
        //alert(this._var().scope)
        this.notify('Trace','init');
        //append defaults
        this._var({
            //currentMediaURL : this._var().mediaURLs[0],
            //currentMediaTitle : this._var().mediaTitles[0],
            //currentMediaThumb : this._var().mediaThumbs[0]
        });
        this._startlisteners();//start this module's listeners
        this.refresh();
    };
    _thizOBJ_.prototype.refresh =function(){
        var parent=this;
        parent.notify('Trace','refresh');


        window.trace = function (getLog){
            console.log('[t]'+getLog);
        };


     //   window.onload = function() {
            trace('Starting Custom Receiver application Clean non-Clunky');
// Turn on debugging so that you can see what is going on.  Please turn this off
// on your production receivers.  Especially if there may be any personally
// identifiable information in the log.
            cast.receiver.logger.setLevelValue(cast.receiver.LoggerLevel.DEBUG);
            window.mediaElement = parent._var().mediaElement;//document.getElementById('vid');  //window.mediaElement  parent._var().mediaElement
            window.applicationData={};
            window.mediaManager = new cast.receiver.MediaManager(window.mediaElement);
            window.castReceiverManager = cast.receiver.CastReceiverManager.getInstance();

        parent._setMediaElement(window.mediaElement);
        parent._setMediaManager(window.mediaManager,window.mediaElement);
        parent._setCastReceiverManager(castReceiverManager);
        parent._startMessageBus(castReceiverManager);

            trace('zCustom Receiver Application is ready, starting system');
            castReceiverManager.start(parent._var().publicdata.appConfig);


        oboe('http://jonathanrobles.net/chromecast/SJB_Caster/public/shows.txt').node(function(){alert(':/')}).done(function(data){
            alert(data.length);
            var titles = _.map(data, function(entry){
                return {
                    title:entry.title,
                    id:entry.title,
                    tmsId:entry.tmsId
                }; });
            console.log(':) '+JSON.stringify(titles));
        }).fail(function(){
            console.log(':(');
        });



    };
    _thizOBJ_.prototype.kill =function(){
        var parent=this;
        parent.notify('Trace','kill');
        _notify.rem(this._id());  //kills this module's listeners
    };

    // Internal - methods  _internalmethod//external_function
    _thizOBJ_.prototype._internalmethod = function(){
        var parent=this;
        parent.notify('Trace','_internalmethod');
        //parent._var({childObjects:parent._var().target.children()});
        //var childObjects=parent._var().childObjects;
        //parent._var().childtrack(parent._var().currentslide);  //do tracking
        //this._multijump(index);
        //this._var().childtrack(indexCandidate);
    };



    _thizOBJ_.prototype._setMediaElement = function(mediaElement){
        var parent=this;
        parent.notify('Trace','_internalmethod');


        mediaElement.addEventListener('timeupdate',function(){
            trace('time!!!!   time: '+mediaElement.currentTime+' of '+mediaElement.duration)
        });
        mediaElement.addEventListener('pause',function(){
            trace('pause!!!!   time: '+mediaElement.currentTime+' of '+mediaElement.duration)
        });
        mediaElement.addEventListener('waiting',function(){
            trace('waiting!!!!   time: '+mediaElement.currentTime+' of '+mediaElement.duration)
        });


    };
    _thizOBJ_.prototype._setMediaManager = function(mediaManager,mediaElement){
        var parent=this;
        parent.notify('Trace','_internalmethod');
        //start load process
        mediaManager['origOnLoad'] = mediaManager.onLoad;
        mediaManager.onLoad = function (event) {
            trace("mediaManager.onLoad "+JSON.stringify(event));
            trace("canPlayType('"+event.data.media.contentType+"') = "+mediaElement.canPlayType(event.data.media.contentType));
            mediaManager['origOnLoad'](event);
        };  //get meta data from load
        mediaManager['origOnMetadataLoaded'] = mediaManager.onMetadataLoaded;
        mediaManager.onMetadataLoaded= function (event) {
            trace("mediaManager.onMetadataLoaded "+JSON.stringify(event));
            mediaManager['origOnMetadataLoaded'](event);
            /*          {
             "senderId": "32:client-4786",
             "message": {
             "requestId": 4316092,
             "sessionId": "F52FE9FD-D986-4E82-AEC6-E172FB38F9AB",
             "media": {
             "contentId": "http://commondatastorage.googleapis.com/gtv-videos-bucket/big_buck_bunny_1080p.mp4",
             "streamType": "BUFFERED",
             "contentType": "video/mp4",
             "metadata": {
             "type": 0,
             "metadataType": 0,
             "title": "Big Buck Bunny",
             "images": [{
             "url": "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg"
             }]
             },
             "duration": 596.501333
             },
             "autoplay": true,
             "currentTime": 0
             }
             }*/
        };
        mediaManager.addEventListener('getTest',function(){trace('getTest!!!!   time: '+mediaElement.currentTime+' of '+mediaElement.duration)});
        mediaManager.addEventListener('pause',function(data){trace('BLEH!!!!   time: '+mediaElement.currentTime+' of '+mediaElement.duration+' : '+data)});
        mediaManager['origOnPlay'] = mediaManager.onPlay;
        mediaManager.onPlay = function (event) {
            trace("mediaManager.onPlay "+JSON.stringify(event));
            mediaManager['origOnPlay'](event);
        };
    };
    _thizOBJ_.prototype._setCastReceiverManager = function(castReceiverManager){
        var parent=this;
        parent.notify('Trace','_internalmethod');
        castReceiverManager.onReady = function (event) {
            trace("sender onReady");
            trace("> a > castReceiverManager.getApplicationData(): "+JSON.stringify(castReceiverManager.getApplicationData()));
            window.applicationData=castReceiverManager.getApplicationData();
            //startMessageBus();
        };
        castReceiverManager.onSenderConnected = function (event) {
            trace("sender onSenderConnected");

            trace("> :) b > castReceiverManager.getApplicationData(): "+JSON.stringify(castReceiverManager.getApplicationData()));

        };
        castReceiverManager.onSenderDisconnected = function (event) {
            trace("sender onSenderDisconnected "+JSON.stringify(event));

        };
        castReceiverManager.onShutdown = function (event) {
            trace("sender onShutdown");
        };
        castReceiverManager.onStandbyChanged = function (event) {
            trace("sender onStandbyChanged");
        };
        castReceiverManager.onSystemVolumeChanged = function (event) {
            trace("sender onSystemVolumeChanged");
        };
        castReceiverManager.onVisibilityChanged = function (event) {
            trace("sender onVisibilityChanged");
        };
    };

    _thizOBJ_.prototype._startMessageBus = function(castReceiverManager){
        var parent=this;
        parent.notify('Trace','_internalmethod');
        var namespace=parent._var().publicdata.namespace;//"urn:x-cast:com.aenetworks.cast.media";
        trace('start messagebus on namespace: '+namespace);
        // window.customMessageBus = castReceiverManager.getCastMessageBus(namespace);
       window.customMessageBus = castReceiverManager.getCastMessageBus(namespace,cast.receiver.CastMessageBus.MessageType.JSON);






        customMessageBus.onMessage = function(event) {
            var getData=event;
            trace('senderId: '+getData.senderId+'\n receiving the data: '+JSON.stringify(getData.data)+' \n sending the data: '+JSON.stringify(parent._var().publicdata));



            window.customMessageBus.send(getData.senderId,parent._var().publicdata);


        };
    };



    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});



