/*!
 * Carouselset RequireJS Module
 * by Jonathan Robles
 *
 */
define(['jquery','underscore', 'modules/definitions/standardmodule','hammer','tweenlite','scrollto','modernizr'], function ($, _, parentModel,Hammer,TweenMax,Scrollto,Modernizr) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'carousel', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)

            target:undefined,  //target object that contains children to control
            virtualchildren:false,  //false expects child divs as breaks, true makes virtual breaks!
            virtualchild_overlap:0,  //overlap between virtual children
            yscroll:false,  //use x or y scrolling
            Xscrolldetect:'release dragleft dragright',// swipeleft swiperight', //remove options to omit
            Yscrolldetect:'release dragup dragdown',// swipeup swipedown', //remove options to omit
            currentslide:0, //defines default slide on start
            visibleslides:[],
            visibleslides_bleed:-2,  // -1 is auto mode, # is the multiplier
            visibleslides_with_bleed:[],
            lastdebouncedslide: 0,
            usetouchbinds:true, //adds touchbinds to the target
            enable_touchbinds:true,  //enables/disables touchbinds
            snaponrelease:true,  //on stop scroll, will snap to closest child
            callback:function(o){


            },  //runs when swipepanel is made
            childchange:function(index){

                var self=this.parent;
                var destination=undefined;
                var animationvars=undefined;
                // console.log("index: "+index)

                if(!self._var().yscroll){
                    destination=self._var().childVars[index].XoffsetfromScroll;
                    animationvars={scrollTo:{x:destination}, ease:Power4.easeOut}
                }else{
                    destination=self._var().childVars[index].YoffsetfromScroll;
                    animationvars={scrollTo:{y:destination}, ease:Power4.easeOut}

                }


                TweenMax.to(self._var().target,.5,animationvars );

            }, //child changing function
            //LIVE SCROLLING CALLBACKS
            onscroll_callback:function(o){
                //console.log('onscroll_callback');
                //console.log('moving :'+ o.currentposition+' [ '+ o.currentpercent+'% ]')

            }, //on scrolling callback
            onscroll_start:function(o){
                //console.log('startmove :'+ o.currentposition+' [ '+ o.currentpercent+'% ]')
            },  //on before scrolling callback
            onscroll_pause:function(o){
                //console.log('onscroll_pause');
                //console.log('moving :'+ o.currentposition+' [ '+ o.currentpercent+'% ]')

            }, //on scrolling callback

            onscroll_debounce_callback:function(o){
                console.log('onscroll_debounce_callback');
                //console.log('endmove :'+ o.currentposition+' [ '+ o.currentpercent+'% ] @slide: '+this.currentslide)

            },  //on end scrolling callback
            onscroll_debounce_rate:100,  //function debouncing rate
            maxitemsonly:true,//START omit offsets so that it removes possible indexes to scroll to when it is possible to see more that one item per page

            attachdisableclassonid:undefined, // when defined attaches disable classes prev_disabled and next_disabled appropriately
            attachhidearrowclassonid:undefined, // when defined attaches hide class appropriately
            //INTERNAL Variables


            scrollsize:undefined,
            currentposition:undefined,
            currentpercent:undefined,

            targetXsize:undefined,
            targetYsize:undefined,

            targetwidth:undefined,
            targetheight:undefined,
            totalslides:undefined,

            childObjects:undefined,
            childVars:undefined,
            childVar_setter:function(index,value){
                var self = this;

                self.index=index;
                self.childOBJ=value;
                self.target=$(value);
                self.Xsize=self.target.outerWidth();
                self.Ysize=self.target.innerHeight();
                self.Xdefault=Math.round(self.target.position().left);
                self.Ydefault=Math.round(self.target.position().top);
                self.XoffsetfromScroll=undefined;//xoffset;
                self.YoffsetfromScroll=undefined;//yoffset;

                //live variables
                //self.OffsetfromFocus=undefined;
            },


            ObjHammer:undefined,

            isbeingdragged:false, //internal!
            startmove:true,//internal!
            busy:false

        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {
        this.notify('Trace','_startlisteners');
        var parent=this;
        var myID=  this._id();
        _notify.add(this._id(), function() {

            return {
                onQuickjump:function(o){
                    if(o.senderID==myID){
                        parent.quickjump(o.data.index)
                    }
                },
                onJump:function(o){
                    if(o.senderID==myID){
                        parent.jump(o.data.index)
                    }
                },
                onPagejump:function(o){
                    if(o.senderID==myID){
                        parent.pagejump(o.data.index)
                    }
                },
                onNext:function(o){
                    if(o.senderID==myID){
                        parent.next()
                    }
                },
                onPrev:function(o){
                    if(o.senderID==myID){
                        parent.prev()
                    }
                },
                onWindowWidth:function(o){
                    parent.refresh();
                }




            }
        }());
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init = function () {
        this.notify('Trace', 'init');
        this._var({busy:true});
        this._startlisteners();//start this module's listeners
        var self=this;

        this.refresh();

    };

    _thizOBJ_.prototype.refresh = function () {

        var self= this;
        self._var({busy:true});
        self.notify('Trace', 'refresh');
        self.getpanelsizes();
       //Object {totalslides: undefined, targetwidth: 978, targetheight: 280, targetXsize: 938, targetYsize: 278}
        (!self._var().virtualchildren)?self._childrentochildObjects():self._virtualspacetochildObjects();
        window.TESTOBJ2=self._var().childObjects;




        self.quickjump(self._var().currentslide)

        self._setwhatisvisible(); //logic to figure out what is visible



        //hide arrows if necessary
        if(self._var().attachhidearrowclassonid!=undefined){




            var lastindex=self._var().childObjects.length-1;
            var windowsize;
            var firstindexcloseside;
            var lastindexfarside;


            if(!self._var().yscroll){
                windowsize=self._var().targetXsize;
                firstindexcloseside=self._var().childVars[0].XoffsetfromScroll;
                lastindexfarside=self._var().childVars[lastindex].XoffsetfromScroll+self._var().childVars[lastindex].Xsize;
            }else{
                windowsize=self._var().targetYsize;
                firstindexcloseside=self._var().childVars[0].YoffsetfromScroll;
                lastindexfarside=self._var().childVars[lastindex].YoffsetfromScroll+self._var().childVars[lastindex].Ysize;
            }



            var estimatedScrollsize=(lastindexfarside-firstindexcloseside);

            //console.log('compare '+ estimatedScrollsize+' and '+windowsize);


            if(estimatedScrollsize<=windowsize){
                self._var().attachhidearrowclassonid.addClass('hide_navigation');
            }   else  {
                self._var().attachhidearrowclassonid.removeClass('hide_navigation');
            }

        };







        //Attach touch and drag handler
        var handlerdetect=(!self._var().yscroll)?self._var().Xscrolldetect:self._var().Yscrolldetect;


        //place this once!
        if(self._var().usetouchbinds&&(self._var().ObjHammer==undefined)){
            self._var().ObjHammer= Hammer (self._var().target,{ drag_lock_to_axis: true }).on(handlerdetect, self._bindHandler(self));
        }

        //attach scroll handler
        self._var().target.unbind("scroll");
        self._var().target.scroll(self._onscroll(self))

        //self._var().onscroll_debounce_callback();

        self._var({busy:false});
        if(self._var().callback!=undefined){self._var().callback(self) }

        self._var().lastdebouncedslide=self._var().currentslide;



    };
    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };
    // UNIQUE MODULE METHODS

//internal
    _thizOBJ_.prototype._childrentochildObjects = function(){
        var self=this;
        self._var({childObjects:self._var().target.children()});
        var childObjects=self._var().childObjects;

        self._var({
            totalslides:childObjects.length,
            childVars:[]
        });

        //build childVars
        $.each(childObjects,function(index, value ){
            childVar_setter_local=self._var().childVar_setter;
            self._var().childVars.push(new childVar_setter_local(index,value))

        });
        $.each(self._var().childVars,function(index, value ){
            var zeroXdefault=self._var().childVars[0].Xdefault;
            var zeroYdefault=self._var().childVars[0].Ydefault;
            var marginOffsetX=value.target.css('marginLeft');
            var marginOffsetY=value.target.css('marginTop');

            value.XoffsetfromScroll=Math.round(value.Xdefault-zeroXdefault);
            value.YoffsetfromScroll=Math.round(value.Ydefault-zeroYdefault);

            if(marginOffsetX!=undefined){value.XoffsetfromScroll+=parseInt(marginOffsetX, 10)};
            if(marginOffsetY!=undefined){value.YoffsetfromScroll+=parseInt(marginOffsetY, 10)};
        });


        var totalscrollarea=(!self._var().yscroll)?self._var().targetwidth:self._var().targetheight;
        var singlechildsize=(!self._var().yscroll)?self._var().targetXsize:self._var().targetYsize;
        var scrollsize=totalscrollarea-singlechildsize;


        self._var({
            scrollsize:scrollsize
        });



    };
    _thizOBJ_.prototype._virtualspacetochildObjects = function(){
        var self=this;
        self._var({childObjects:self._var().target.children()});

        var totalscrollarea=(!self._var().yscroll)?self._var().targetwidth:self._var().targetheight;
        var singlechildsize=(!self._var().yscroll)?self._var().targetXsize:self._var().targetYsize;
        singlechildsize=singlechildsize-self._var().virtualchild_overlap;

        var scrollsize=totalscrollarea-singlechildsize;

        //var latestsizes=self.getpanelsizes();
        //alert(totalscrollarea+' : '+scrollsize)

        var tempStopArray=[];
        var childcount=Math.floor(scrollsize/singlechildsize);
        for(tempval=0;tempval<childcount+1;tempval++){
            tempStopArray.push(tempval*singlechildsize)
        }
        tempStopArray.push(scrollsize);

        //alert(scrollsize)
        //alert(tempStopArray)



        // var childObjects=self._var().childObjects;

        self._var({
            totalslides:tempStopArray.length,
            childVars:[],
            scrollsize:scrollsize
        });

        //build childVars
        $.each(tempStopArray,function(index, value ){
            self._var().childVars.push({
                XoffsetfromScroll:value,
                YoffsetfromScroll:value
            })

        });



        /*  $.each(self._var().childVars,function(index, value ){
         var zeroXdefault=self._var().childVars[0].Xdefault;
         var zeroYdefault=self._var().childVars[0].Ydefault;
         var marginOffsetX=value.target.css('marginLeft');
         var marginOffsetY=value.target.css('marginTop');

         value.XoffsetfromScroll=Math.round(value.Xdefault-zeroXdefault);
         value.YoffsetfromScroll=Math.round(value.Ydefault-zeroYdefault);

         if(marginOffsetX!=undefined){value.XoffsetfromScroll+=parseInt(marginOffsetX, 10)};
         if(marginOffsetY!=undefined){value.YoffsetfromScroll+=parseInt(marginOffsetY, 10)};
         });

         //set scrollsize
         self._var({
         scrollsize:(!self._var().yscroll)?self._var().childVars[self._var().totalslides-1].XoffsetfromScroll:self._var().childVars[self._var().totalslides-1].YoffsetfromScroll
         });

         */
        //alert(self._var().scrollsize)

        //  alert(self._var().target.width()-400)
        // alert(self._var().target.innerWidth()-400)
        //alert(self._var().target.outterWidth())
        //  alert(self._var().scrollsize+' : '+(self._var().targetwidth-(self._var().target.width()+400)))
        //  alert(self._var().scrollsize+' : '+(self._var().targetwidth-(self._var().target.innerWidth()+400)))
    };


    _thizOBJ_.prototype.offsetReturnForMaxItems= function () {

        //returns a list of offsets for when maxitemsonly is ON


                var self=this;
              var Offsets=_.map(self._var().childVars, function(idx){
                     return (!self._var().yscroll)?Math.abs(idx.XoffsetfromScroll):Math.abs(idx.YoffsetfromScroll)
              });

                var newOffsets = new Array();

            //START omit offsets so that it removes possible indexes to scroll to when it is possible to see more that one item per page
            var removeOffset=_.map(self._var().childVars, function(idx){
                var needed=true;
                var lastitem=self._var().childVars.length-1;
                var barrier;
                var barrierbuffer=25;  //have to fix this and some point!!!!  something off with calculation!
                var returnCandidate;

                if(!self._var().yscroll){


                    barrier=(self._var().childVars[lastitem].XoffsetfromScroll-self._var().targetXsize+self._var().childVars[lastitem].Xsize+barrierbuffer);
                    returnCandidate=(barrier+idx.Xsize)<(idx.XoffsetfromScroll);  //returns true if index should be removed!
                    //console.log('idx:'+idx+' comparing  '+(barrier+idx.Xsize)+' & '+idx.XoffsetfromScroll+'  return cand:'+returnCandidate)

                } else {
                    barrier=(self._var().childVars[lastitem].YoffsetfromScroll-self._var().targetYsize+self._var().childVars[lastitem].Ysize+barrierbuffer);
                    returnCandidate=(barrier+idx.Ysize)<(idx.YoffsetfromScroll);  //returns true if index should be removed!
                }

                return returnCandidate;
            });



            $.each(Offsets,function(index,value){
                if(index==0){
                    newOffsets.push(Offsets[index]); //always push index 0!
                } else {
                    if(!removeOffset[index]){newOffsets.push(Offsets[index])}
                }

            })

            //console.log('a:'+Offsets);console.log('b:'+newOffsets);



            //END omit offsets
            return newOffsets


    }


    _thizOBJ_.prototype.getpanelsizes = function () {
        var self= this;
        var objreturn={}
        objreturn.totalslides=self._var().totalslides;

        objreturn.targetwidth=self._var().targetwidth=self._var().target[0].scrollWidth;
        objreturn.targetheight=self._var().targetheight=self._var().target[0].scrollHeight;

        objreturn.targetXsize=self._var().targetXsize=self._var().target.innerWidth();
        objreturn.targetYsize=self._var().targetYsize=self._var().target.innerHeight();
        window.TESTOBJ=objreturn;

        //set visibleslides_bleed
        if(self._var().visibleslides_bleed<0)
        {
            var multiplier= Math.abs(self._var().visibleslides_bleed);
            self._var().visibleslides_bleed= (!self._var().yscroll)?(objreturn.targetXsize*multiplier):(objreturn.targetYsize*multiplier)

        }
        //console.log(self._var().visibleslides_bleed)


        return objreturn;
    };
    _thizOBJ_.prototype.getclosestindex=function(position)  {
        var self=this;

        var getOffsets=_.map(self._var().childVars, function(idx){
            return (!self._var().yscroll)?idx.XoffsetfromScroll:idx.YoffsetfromScroll
        });





  if(self._var().maxitemsonly){

        //USE smaller set of offsets
      getOffsets=self.offsetReturnForMaxItems();


  }


        var Offsets=_.map(getOffsets, function(off_value){
            return Math.abs(off_value-position);
        });

        //console.log(getOffsets)
        //console.log(Offsets)

        //makes array of differences of current x stop and all stops
        var closestdiff=_.reduce(Offsets, function(memo, val) {
            return (val<memo)?val:memo;
        }, 10000);
        //get lowest number from array
        var indextoReturn = _.indexOf(Offsets, closestdiff);

        self._var().currentslide=indextoReturn;
        return indextoReturn;
    };
    _thizOBJ_.prototype._returncurrentposition= function(){
        var self=this;
        return (!self._var().yscroll)?self._var().target.scrollLeft():self._var().target.scrollTop()
    };
    _thizOBJ_.prototype._gotoposition= function(getdestination){
        var self=this;
        (!self._var().yscroll)?self._var().target.scrollLeft(getdestination):self._var().target.scrollTop(getdestination)
    };
    _thizOBJ_.prototype._gotopercent= function(getpercent){
        var self=this;
        var scrollsize=self._var().scrollsize
        //var currentposition=self._returncurrentposition();
        var jumptoposition=scrollsize/(100/getpercent);
        self._gotoposition(jumptoposition);
    };
//controls
    _thizOBJ_.prototype.selfie= function (o){

        //console.log('SELFIEEEE!!!')
        var self=this;
        var options={
            yscroll:self._var().yscroll,
            quick:false
        };
        options = $.extend(options, o);

        if(!options.yscroll){
            //xscroll
            var thisindex=self.getclosestindex(self._returncurrentposition());


            if(!options.quick){
                self.jump(thisindex);
            }else{
                self.quickjump(thisindex);
            }

        }else{
            //yscroll
            var thisindex=self.getclosestindex(self._returncurrentposition());


            if(!options.quick){
                self.jump(thisindex);
            }else{
                self.quickjump(thisindex);
            }
        }


    };
    _thizOBJ_.prototype.next= function (o){
        var self=this;
        var options={
            quick:false
        };
        options = $.extend(options, o);
        var thisindex=(!self._var().yscroll)?self.getclosestindex(self._returncurrentposition()):self.getclosestindex(self._var().target.scrollTop());
        //console.log('thisindex:'+thisindex+' : '+self._var().currentslide)

        var nextidx=(thisindex==self._var().totalslides-1)?thisindex:thisindex+1;

        //the following compares next destination to the last debounce slide
        var diff=Math.abs(nextidx-Number(self._var().lastdebouncedslide))
        // console.log("current:"+self._var().lastdebouncedslide+" index: "+nextidx+" diff:"+diff);
        if(diff>1){nextidx-=(diff-1)};
        //comparetodebounce complete

        (!options.quick)?self.jump(nextidx):self.quickjump(nextidx);

    };
    _thizOBJ_.prototype.prev= function (o){
        var self=this;
        var options={
            quick:false
        };
        options = $.extend(options, o);
        var thisindex=(!self._var().yscroll)?self.getclosestindex(self._returncurrentposition()):self.getclosestindex(self._var().target.scrollTop());
        var nextidx=(thisindex==0)?0:thisindex-1;

        //the following compares next destination to the last debounce slide
        var diff=Math.abs(nextidx-Number(self._var().lastdebouncedslide))
        //console.log("current:"+self._var().lastdebouncedslide+" index: "+nextidx+" diff:"+diff);
        if(diff>1){nextidx+=(diff-1)};
        //comparetodebounce complete

        (!options.quick)?self.jump(nextidx):self.quickjump(nextidx);


    };
    _thizOBJ_.prototype.jump = function (index) {

        var self=this;
        self._var().childchange(index,self)


    };
    _thizOBJ_.prototype.pagejump = function (index) {
        this.jump(index);
    }
    _thizOBJ_.prototype.quickjump = function (index) {
        var self=this;

        if(!self._var().yscroll){
            var destination=self._var().childVars[index].XoffsetfromScroll;
            // self._var().target.scrollLeft(destination)
        }else{
            var destination=self._var().childVars[index].YoffsetfromScroll;
            //self._var().target.scrollTop(destination)
        }

        self._gotoposition(destination)

    };

//handlers
    _thizOBJ_.prototype._bindHandler = function(self){

        var lastCase=undefined;
        var touchpoint=undefined;

        var targetposition=function(){

            touchpoint=self._returncurrentposition();
        }

        var casechecks=undefined;
        var directions=undefined;

        if(!self._var().yscroll){
            casechecks=['dragright','dragleft','swipeleft','swiperight','release'];
            directions=[Hammer.DIRECTION_RIGHT,Hammer.DIRECTION_LEFT];
        } else {
            casechecks=['dragdown','dragup','swipeup','swipedown','release'];
            directions=[Hammer.DIRECTION_DOWN,Hammer.DIRECTION_UP];
        }


        var eventHandler = function (ev){
            // disable default action
            ev.gesture.preventDefault();

            if(ev.type!=lastCase){ targetposition(); };
//alert('!')

            if(self._var().enable_touchbinds){

                switch(ev.type) {


                    case casechecks[4]:
                        // console.log('case4')
                        self._var().isbeingdragged=false;
                        targetposition();

                        if(self._var().snaponrelease){
                            //console.log('selfie!')
                            self.selfie();
                        } else {
                            //alert('hmmmm')


                            var currentvars={};
                            currentvars.scrollsize=self._var().scrollsize
                            currentvars.currentposition=self._returncurrentposition();
                            currentvars.currentpercent=(100/(currentvars.scrollsize/currentvars.currentposition));


                            self._debounce(self)(currentvars);
                        }



                        ev.gesture.stopDetect();
                        break;
                    case casechecks[0]:
                    case casechecks[1]:
                        // console.log('case0/1');// ['+self._returncurrentposition()+']')
                        self._var().isbeingdragged=true;


                        var pane_offset=touchpoint;
                        var drag_offset = (!self._var().yscroll)?ev.gesture.deltaX:ev.gesture.deltaY;

                        //+ or - depending on direction
                        if(ev.gesture.direction == directions[0]){
                            drag_offset=-Math.abs(drag_offset)
                        } else if(ev.gesture.direction == directions[1]){
                            drag_offset=Math.abs(drag_offset)
                        };


                        //compute destination
                        var destination=(pane_offset+drag_offset);

                        //update object
                        (!self._var().yscroll)?self._var().target.scrollLeft(destination):self._var().target.scrollTop(destination);
                        //ev.gesture.stopDetect();
                        break;
                    //swipeleft
                    case casechecks[2]:
                        //console.log('case2')
                        self.next();
                        self._var().isbeingdragged=false;
                        ev.gesture.stopDetect();
                        break;
                    //swiperight
                    case casechecks[3]:
                        // console.log('case3')
                        self.prev();
                        self._var().isbeingdragged=false;
                        ev.gesture.stopDetect();
                        break;

                }

            }



            lastCase=ev.type; //log last ev type


        };

        return  eventHandler;


    };
    _thizOBJ_.prototype._debounce= function(self){
        //var self=this;
        //alert('!')

        var debouncer=       _.debounce(function(currentvars){

            //compare current location with a acceptable release point
            var currentposition=    self._returncurrentposition();
            var closeindex=self.getclosestindex(currentposition);
            var destination=undefined;


            if(!self._var().yscroll){
                destination=self._var().childVars[closeindex].XoffsetfromScroll;
                //alert(closeindex)


            }else{
                destination=self._var().childVars[closeindex].YoffsetfromScroll;
            }



            var difference=(Math.abs(currentposition-destination));






            self._setwhatisvisible();
            if(self._var().onscroll_pause!=undefined){self._var().onscroll_pause(currentvars) }


            if(difference>1&&!self._var().isbeingdragged&&self._var().snaponrelease){

                self.selfie();

            } else if(difference<2||(!self._var().isbeingdragged&&!self._var().snaponrelease)) {


                if(!self._var().startmove){

                    //set the debounced last slide so stuff doesn't skip
                    self._var().lastdebouncedslide=self._var().currentslide;

                    self._setwhatisvisible();
                    self._var().onscroll_debounce_callback(currentvars);  //run debounce callback
                    self._var({startmove:true});  //makes sure you don't interupt to smoothie smoothness
                    //console.log(self._var().lastdebouncedslide)
                }


            }



        },self._var().onscroll_debounce_rate)



        return  debouncer;


    };




    _thizOBJ_.prototype._setwhatisvisible=function(){
        var self=this;
        var visibleslides=[];
        var visibleslides_with_bleed=[];
        var visibleindex=self._var().currentslide;
        var visibleslides_bleed=self._var().visibleslides_bleed;

        var visiblewindow_startX=self._var().childVars[visibleindex].XoffsetfromScroll;
        var visiblewindow_startY=self._var().childVars[visibleindex].YoffsetfromScroll;
        var visiblewindow_endX=(visiblewindow_startX+self._var().targetXsize);
        var visiblewindow_endY=(visiblewindow_startY+self._var().targetYsize);
        var visiblewindow_startXb=self._var().childVars[visibleindex].XoffsetfromScroll-self._var().visibleslides_bleed;
        var visiblewindow_startYb=self._var().childVars[visibleindex].YoffsetfromScroll-self._var().visibleslides_bleed;
        var visiblewindow_endXb=(visiblewindow_startX+self._var().targetXsize+(self._var().visibleslides_bleed));
        var visiblewindow_endYb=(visiblewindow_startY+self._var().targetYsize+(self._var().visibleslides_bleed));

        if(!self._var().yscroll){
        $.each(self._var().childVars,function(index,value){

            var minVar=value.XoffsetfromScroll;
            var maxVar=minVar+value.Xsize;
            if(minVar>=visiblewindow_startX  && maxVar<=visiblewindow_endX )  {
                visibleslides.push(index)
            }
            if(minVar>=visiblewindow_startXb  && maxVar<=visiblewindow_endXb )  {
                visibleslides_with_bleed.push(index)
            }
            //console.log(visiblewindow_startXb+'< '+visiblewindow_startX+'< '+minVar+'x'+maxVar+' >'+visiblewindow_endX+' >'+visiblewindow_endXb)

        });
        } else {
            $.each(self._var().childVars,function(index,value){

                var minVar=value.YoffsetfromScroll;
                var maxVar=minVar+value.Ysize;
                if(minVar>=visiblewindow_startY  && maxVar<=visiblewindow_endY )  {
                    visibleslides.push(index)
                }
                if(minVar>=visiblewindow_startYb  && maxVar<=visiblewindow_endYb )  {
                    visibleslides_with_bleed.push(index)
                }
                // console.log(visiblewindow_startYb+'< '+visiblewindow_startY+'< '+minVar+'x'+maxVar+' >'+visiblewindow_endY+' >'+visiblewindow_endYb)


            });
        }





        self._var().visibleslides=visibleslides;
        self._var().visibleslides_with_bleed=visibleslides_with_bleed;


        //PLACE

        //disable arrows if necessary
        if(self._var().attachdisableclassonid!=undefined){
            var firstidtodisable=0;
            var lastidtodisable=(self._var().totalslides-1);//self.offsetReturnForMaxItems().length;

            //console.log('self._var().visibleslides='+self._var().visibleslides+' totalslides='+self._var().totalslides+' use last='+lastidtodisable)

            if(_.contains(self._var().visibleslides, firstidtodisable)){
                //console.log('no previous');
                self._var().attachdisableclassonid.addClass('disable_previous');
            } else {
                self._var().attachdisableclassonid.removeClass('disable_previous');
            }

            ;
            if(_.contains(self._var().visibleslides, lastidtodisable)){
                //console.log('no next');
                self._var().attachdisableclassonid.addClass('disable_next');
            } else {
                self._var().attachdisableclassonid.removeClass('disable_next');
            }

            ;





         };



        //

        //console.log('['+visiblewindow_startX+' to '+visiblewindow_endX +'] ['+visiblewindow_startXb+' to '+visiblewindow_endXb +'] '   )
        //console.log(' ['+ self._var().visibleslides+']  withbleed:['+ self._var().visibleslides_with_bleed+']')
    };






    _thizOBJ_.prototype._onscroll= function(self){



        var Offsets=_.map(self._var().childVars, function(idx){
            return (!self._var().yscroll)?Math.abs(idx.XoffsetfromScroll):Math.abs(idx.YoffsetfromScroll)
        });


        var debouncefunc=self._debounce(self);
        var handler= function(){
            var currentvars={};
            currentvars.scrollsize=self._var().scrollsize
            currentvars.currentposition=self._returncurrentposition();
            currentvars.currentpercent=(100/(currentvars.scrollsize/currentvars.currentposition));

            //start onscroll functions
            if(self._var().startmove){
                self._var().onscroll_start(currentvars);
                self._var({startmove:false})
            }

            self._var().onscroll_callback(currentvars);
            debouncefunc(currentvars)
        }

        return handler
    };



    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});

