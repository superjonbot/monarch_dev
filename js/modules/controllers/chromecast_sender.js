/*!
 * Chromecast Sender Module
 * by Jonathan Robles
 * 12-17-15
 *
 *
 */
define(['modules/definitions/standardmodule','underscore','oboe'],function (parentModel,_,oboe) {

    // external global function(s) //comment out on init!!!
    //window.globalFunction=undefined;
    //window.testbutton=undefined;

    function _thizOBJ_( o ){

        var defaults={
            scope:true,
            type:'chromecast_sender', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            lasteditby:''/*,
            applicationIDs:'2904EB5C'*/
        };
        defaults= this.deepExtend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };


    _thizOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners =function(){

        /*

         this.notify('Trace','_startlisteners');
         var parent=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {
         onQuickjump:function(o){
         if(o.senderID==myID){
         parent.quickjump(o.data.index)
         }
         },
         onJump:function(o){
         if(o.senderID==myID){
         parent.jump(o.data.index)
         }
         },
         onPagejump:function(o){
         if(o.senderID==myID){
         parent.pagejump(o.data.index)
         }
         },
         onNext:function(o){
         if(o.senderID==myID){
         parent.next()
         }
         },
         onPrev:function(o){
         if(o.senderID==myID){
         parent.prev()
         }
         }




         }
         }());

         */


    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init =function(){
        //alert(this._var().scope)
        this.notify('Trace','init');

        this._var({
            //currentMediaURL : this._var().mediaURLs[0],
            //currentMediaTitle : this._var().mediaTitles[0],
            //currentMediaThumb : this._var().mediaThumbs[0]
        })



        this._startlisteners();//start this module's listeners
        this.refresh();
    };
    _thizOBJ_.prototype.refresh =function(){
        var parent=this;
        parent.notify('Trace','refresh');


        window.CastPlayer.initializeCastPlayer();
        window.CastPlayer.initializeLocalPlayer();

        /**
         * send a message to the receiver using the custom namespace
         * receiver CastMessageBus message handler will be invoked
         * @param {string} message A message string
         */

        //chrome.cast.requestSession(function(e) {
        //                session = e;
        //                //session.sendMessage(namespace, message, onSuccess.bind(this, "Message sent: " + message), onError);
        //    session.addUpdateListener(sessionUpdateListener);
        //    session.addMessageListener(namespace, receiverMessage);
        //            }, function(){
        //    alert('no session')
        //});

       // }

        //var namespace='urn:x-cast:com.ae.chromecast';
        //function sendMessage(message) {
        //    if (session!=null) {
        //        session.sendMessage(namespace, message, onSuccess.bind(this, "Message sent: " + message), onError);
        //    }
        //    else {
        //        chrome.cast.requestSession(function(e) {
        //            session = e;
        //            session.sendMessage(namespace, message, onSuccess.bind(this, "Message sent: " + message), onError);
        //        }, onError);
        //    }
        //}
        //
        //sendMessage('cookiepuss');
window.getStatus = function(){
    CastPlayer.session.sendMessage(AETN.namespace,{'command':'sessionStatus'});
trace('CastPlayer.session.sendMessage("'+AETN.namespace+'",{"command":"sessionStatus"});')
    //CastPlayer.session.sendMessage(AETN.namespace,{'sentText':testMessage}/*,function(data){alert(':)'+JSON.stringify(data))},function(data){alert(':('+JSON.stringify(data))}*/);
}
        //testText('crunky');
//window.testTextB = function(){
//    var somejsondata={};
//            CastPlayer.session.sendMessage(AETN.namespace,'meh',function(){':)'},function(){alert(':(')});
//}


        //CastPlayer.session.sendMessage('urn:x-cast:com.ae.chromecast','meh',function(){':)'},function(){alert(':(')})

        //alert('sender init run');
    };
    _thizOBJ_.prototype.kill =function(){
        /*
         <object>.hide(); //just hide it and kill listeners
         * */
        this.notify('Trace','kill');
        _notify.rem(this._id());  //kills this module's listeners
    };

    // Internal - methods  _internalmethod//external_function
    _thizOBJ_.prototype._internalmethod = function(){
        var parent=this;
        parent.notify('Trace','_internalmethod');
        //parent._var({childObjects:parent._var().target.children()});
        //var childObjects=parent._var().childObjects;
        //parent._var().childtrack(parent._var().currentslide);  //do tracking
        //this._multijump(index);
        //this._var().childtrack(indexCandidate);
    };


    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});



