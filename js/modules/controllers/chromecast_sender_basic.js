/*!
 * Chromecast Sender Module
 * by Jonathan Robles
 * 12-17-15
 *
 *
 */
define(['modules/definitions/standardmodule','underscore','oboe'],function (parentModel,_,oboe) {

    window.loadMedia=undefined;
    window.stopMedia=undefined;
    window.playMedia=undefined;
    window.selectMedia=undefined;
    window.stopApp=undefined;
    window.launchApp=undefined;
    window.muteMedia=undefined;
    window.getMediaStatus=undefined;
    window.joinSessionBySessionId=undefined;
    window.loadCustomMedia=undefined;
    window.seekMedia=undefined;
    window.testbutton=undefined;

    function _thizOBJ_( o ){

        var defaults={
            scope:true,
            type:'chromecast_sender', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            lasteditby:'',
            applicationIDs:'0E485EB8',//'E2D955ED', //_var().applicationIDs//0E485EB8
            CAST_API_INITIALIZATION_DELAY : 1000, //Cast initialization timer delay
            PROGRESS_BAR_UPDATE_DELAY: 1000,  //Session idle time out in miliseconds
            SESSION_IDLE_TIMEOUT : 300000,
            MEDIA_SOURCE_ROOT : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/',
            CAST_ICON_THUMB_ACTIVE : 'images/cast_icon_active.png',
            CAST_ICON_THUMB_IDLE : 'images/cast_icon_idle.png',
            CAST_ICON_THUMB_WARNING : 'images/cast_icon_warning.png',
            currentMediaSession:null,
            currentVolume : 0.5,
            progressFlag : 1,
            mediaCurrentTime : 0,
            session:null,
            storedSession:null,
            mediaURLs: [
                'http://commondatastorage.googleapis.com/gtv-videos-bucket/big_buck_bunny_1080p.mp4',
                'http://commondatastorage.googleapis.com/gtv-videos-bucket/ED_1280.mp4',
                'http://commondatastorage.googleapis.com/gtv-videos-bucket/tears_of_steel_1080p.mov',
                'http://commondatastorage.googleapis.com/gtv-videos-bucket/reel_2012_1280x720.mp4',
                'http://commondatastorage.googleapis.com/gtv-videos-bucket/Google%20IO%202011%2045%20Min%20Walk%20Out.mp3'],
            mediaTitles: [
                'Big Buck Bunny',
                'Elephant Dream',
                'Tears of Steel',
                'Reel 2012',
                'Google I/O 2011 Audio'],
            mediaThumbs: [
                'images/BigBuckBunny.jpg',
                'images/ElephantsDream.jpg',
                'images/TearsOfSteel.jpg',
                'images/reel.jpg',
                'images/google-io-2011.jpg'],
            currentMediaURL : undefined,//this.mediaURLs[0],
            currentMediaTitle : undefined,//this.mediaTitles[0],
            currentMediaThumb : undefined, //this.mediaThumbs[0]
            timer : null
        };
        defaults= this.deepExtend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };


    _thizOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners =function(){

        /*

         this.notify('Trace','_startlisteners');
         var parent=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {
         onQuickjump:function(o){
         if(o.senderID==myID){
         parent.quickjump(o.data.index)
         }
         },
         onJump:function(o){
         if(o.senderID==myID){
         parent.jump(o.data.index)
         }
         },
         onPagejump:function(o){
         if(o.senderID==myID){
         parent.pagejump(o.data.index)
         }
         },
         onNext:function(o){
         if(o.senderID==myID){
         parent.next()
         }
         },
         onPrev:function(o){
         if(o.senderID==myID){
         parent.prev()
         }
         }




         }
         }());

         */


    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init =function(){
        //alert(this._var().scope)
        this.notify('Trace','init');

        this._var({
            currentMediaURL : this._var().mediaURLs[0],
            currentMediaTitle : this._var().mediaTitles[0],
            currentMediaThumb : this._var().mediaThumbs[0]
        })



        this._startlisteners();//start this module's listeners
        this.refresh();
    };
    _thizOBJ_.prototype.refresh =function(){
        var parent=this;
        //alert(CAST_API_INITIALIZATION_DELAY+' : '+this._var().CAST_API_INITIALIZATION_DELAY);

        this.notify('Trace','refresh');
        //this._childrentochildObjects();
       // if (!chrome.cast || !chrome.cast.isAvailable) {
            setTimeout(parent._initializeCastApi.call(parent,undefined), this._var().CAST_API_INITIALIZATION_DELAY);
       // }


    };
    _thizOBJ_.prototype.kill =function(){
        /*
         <object>.hide(); //just hide it and kill listeners
         * */
        this.notify('Trace','kill');
        _notify.rem(this._id());  //kills this module's listeners
    };

    // Internal - methods  _internalmethod//external_function
    _thizOBJ_.prototype._internalmethod = function(){
        var parent=this;
        //parent._var({childObjects:parent._var().target.children()});
        //var childObjects=parent._var().childObjects;
        //parent._var().childtrack(parent._var().currentslide);  //do tracking
        //this._multijump(index);
        //this._var().childtrack(indexCandidate);
    };
    _thizOBJ_.prototype._appendMessage=function(message) {
        this.notify('Trace','_appendMessage');
        var dw = document.getElementById('debugmessage');
        dw.innerHTML += '\n' + JSON.stringify(message);
    }

    /**
     * initialization
     */
    _thizOBJ_.prototype._initializeCastApi=function() {
        alert(':P');
        var parent=this;
        this.notify('Trace','initializeCastApi() scope:'+parent._var().scope);
        // default app ID to the default media receiver app
        // optional: you may change it to your own app ID/receiver
        //chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID='E2D955ED';
        //var applicationIDs = [
        //    chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID
        //];


        // auto join policy can be one of the following three
        // 1) no auto join
        // 2) same appID, same URL, same tab
        // 3) same appID and same origin URL
        var autoJoinPolicyArray = [
            chrome.cast.AutoJoinPolicy.PAGE_SCOPED,
            chrome.cast.AutoJoinPolicy.TAB_AND_ORIGIN_SCOPED,
            chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED
        ];

        // request session
        //var sessionRequest = new chrome.cast.SessionRequest(applicationIDs[0]);

        var sessionRequest = new chrome.cast.SessionRequest(parent._var().applicationIDs);

        var apiConfig = new chrome.cast.ApiConfig(sessionRequest,
            function(e){return parent._sessionListener.call(parent,e)},
            function(e){return parent._receiverListener.call(parent,e)},
            autoJoinPolicyArray[1]);
        this.notify('Trace','sessionRequest: '+sessionRequest+'= '+JSON.stringify(sessionRequest));
        this.notify('Trace','apiConfig: '+apiConfig+'= '+JSON.stringify(apiConfig));
        chrome.cast.initialize(apiConfig, function(e){return parent._onInitSuccess.call(parent,e)}, function(e){return parent._onError.call(parent,e)});



        oboe('http://jonathanrobles.net/chromecast/SJB_Caster/public/shows.txt').node(function(){alert(':/')}).done(function(data){
            alert(data.length);

            var titles = _.map(data, function(entry){



                return {
                title:entry.title,
                id:entry.title,
                tmsId:entry.tmsId
            }; });
            alert(JSON.stringify(titles));


        }).fail(function(){
            alert(':(');
        });



    }

    /**
     * initialization success callback
     */
    _thizOBJ_.prototype._onInitSuccess=function() {
        var parent=this;
        this.notify('Trace','_onInitSuccess scope:'+parent._var().scope);
        parent._var().storedSession = JSON.parse(localStorage.getItem('storedSession'));
        this.notify('Trace',"onInitSuccess : storedSession="+parent._var().storedSession+' : '+JSON.stringify(parent._var().storedSession));
        if (parent._var().storedSession) {
            var dateString = parent._var().storedSession.timestamp;
            var now = new Date().getTime();

            if (now - dateString < parent._var().SESSION_IDLE_TIMEOUT) {
                document.getElementById('joinsessionbyid').style.display = 'block';
            }
        }


        window.loadMedia=function(data)     {return parent._loadMedia(data);};
        window.stopMedia=function(data)     {return parent._stopMedia(data);};
        window.playMedia=function(data)     {return parent._playMedia(data);};
        window.selectMedia=function(data)   {return parent._selectMedia(data);};
        window.stopApp=function(data)       {return parent._stopApp(data);};
        window.launchApp=function(data)     {return parent._launchApp(data);};
        window.muteMedia=function(data)     {return parent._muteMedia(data);};
        window.getMediaStatus=function(data){return parent._getMediaStatus(data);};
        window.joinSessionBySessionId=function(data){return parent._joinSessionBySessionId(data);};
        window.loadCustomMedia=function(data){return parent._loadCustomMedia(data);};
        //window.loadCustomMedia=function(data){return parent._myCustomloadMedia();};
        window.seekMedia=function(data){return parent._seekMedia(data);};

        window.testbutton=function(data){return parent._testbutton.call(parent,data);};
        //seekMedia
   //loadCustomMedia
    }

    /**
     * generic error callback
     * @param {Object} e A chrome.cast.Error object.
     */
    _thizOBJ_.prototype._onError=function(e) {
        this.notify('Trace','Error' + JSON.stringify(e));
        //appendMessage('Error' + e);
    }

    /**
     * generic success callback
     * @param {string} message from callback
     */
    _thizOBJ_.prototype._onSuccess=function(message) {
        this.notify('Trace','_onSuccess');
        this.notify('Trace',message);
    }

    /**
     * callback on success for stopping app
     */
    _thizOBJ_.prototype._onStopAppSuccess=function() {
        var parent=this;
        this.notify('Trace','_onStopAppSuccess scope:'+parent._var().scope);
        //appendMessage('Session stopped');
        document.getElementById('casticon').src = parent._var().CAST_ICON_THUMB_IDLE;
    }

    /**
     * session listener during initialization
     * @param {Object} e session object
     * @this sessionListener
     */
    _thizOBJ_.prototype._sessionListener=function(e) {
        var parent = this;
        this.notify('Trace','_sessionListener scope:'+parent._var().scope);
        this.notify('Trace','New session ID: ' + e.sessionId);
        //appendMessage('New session ID:' + e.sessionId);
        parent._var().session = e;
        this.notify('Trace','MEDIA '+JSON.stringify(parent._var().session.media));

        document.getElementById('casticon').src = parent._var().CAST_ICON_THUMB_ACTIVE;   //change icon to show session
        if (parent._var().session.media.length != 0) {
            this.notify('Trace','Found ' + parent._var().session.media.length + ' existing media sessions.');
            parent._onMediaDiscovered.call(this,'sessionListener', parent._var().session.media[0]);
        }
        parent._var().session.addMediaListener(parent._onMediaDiscovered.bind(this, 'addMediaListener'));
        parent._var().session.addUpdateListener(parent._sessionUpdateListener.bind(this));
        // disable join by session id when auto join already
        if (parent._var().storedSession) {
            document.getElementById('joinsessionbyid').style.display = 'none';
        }


        function sessionUpdateListener(isAlive) {
            alert('!1')
        };
        function receiverMessage(namespace, message) {
            alert('Sender received from receiver: ' + message);
        };

        parent._var().session.addUpdateListener(sessionUpdateListener);
        parent._var().session.addMessageListener("urn:x-cast:com.aenetworks.cast.media", receiverMessage);

    }

    /**
     * session update listener
     * @param {boolean} isAlive status from callback
     * @this this._sessionUpdateListener
     */
    _thizOBJ_.prototype._sessionUpdateListener=function(isAlive) {
        var parent=this;
        this.notify('Trace','_sessionUpdateListener scope:'+parent._var().scope);
        if (!isAlive) {
            parent._var().session = null;
            document.getElementById('casticon').src = parent._var().CAST_ICON_THUMB_IDLE;
            var playpauseresume = document.getElementById('playpauseresume');
            playpauseresume.innerHTML = 'Play';
            if (parent._var().timer) {
                clearInterval(parent._var().timer);
            }
            else {
                parent._var().timer = setInterval(this._updateCurrentTime.bind(this),
                    parent._var().PROGRESS_BAR_UPDATE_DELAY);
                playpauseresume.innerHTML = 'Pause';
            }
        }
    }

    /**
     * receiver listener during initialization
     * @param {string} e status string from callback
     */
    _thizOBJ_.prototype._receiverListener=function(e) {
        var parent = this;
        this.notify('Trace','_receiverListener scope:'+parent._var().scope);
        this.notify('Trace','receiverListener('+e+')');
        if (e === 'available') {
            this.notify('Trace','receiver found');
            //appendMessage('receiver found');
        }
        else {
            this.notify('Trace','receiver list empty');
            //appendMessage('receiver list empty');
        }
    }

    /**
     * select a media URL
     * @param {string} m An index for media URL
     */
    _thizOBJ_.prototype._selectMedia=function(m) {
        var parent=this;
        this.notify('Trace','_selectMedia scope:'+parent._var().scope);
        this.notify('Trace','media selected' + m);
        //appendMessage('media selected' + m);
        parent._var().currentMediaURL = parent._var().mediaURLs[m];
        parent._var().currentMediaTitle = parent._var().mediaTitles[m];
        parent._var().currentMediaThumb = parent._var().mediaThumbs[m];
        var playpauseresume = document.getElementById('playpauseresume');
        document.getElementById('thumb').src = parent._var().MEDIA_SOURCE_ROOT + parent._var().mediaThumbs[m];
    }

    /**
     * launch app and request session
     */
    _thizOBJ_.prototype._launchApp=function() {
        var parent=this;
        this.notify('Trace','_launchApp scope:'+parent._var().scope);
        this.notify('Trace','1launching app...');
        //appendMessage('launching app...');
        //console.log('onRequestSessionSuccess:'+(parent._onRequestSessionSuccess)+' onLaunchError:'+(parent._onLaunchError));

        //chrome.cast.requestSession(function(e){return this._onRequestSessionSuccess.call(parent,e)}, function(e){return this._onLaunchError.call(parent,e)});

        chrome.cast.requestSession.call(parent,function(e){return parent._onRequestSessionSuccess.call(parent,e)}, function(e){return parent._onLaunchError.call(parent,e)});

        //chrome.cast.requestSession.call(parent,parent._onRequestSessionSuccess, parent._onLaunchError);
        if (parent._var().timer) {
            clearInterval(parent._var().timer);
        }
    }

    /**
     * callback on success for requestSession call
     * @param {Object} e A non-null new session.
     * @this onRequestSesionSuccess
     */
    _thizOBJ_.prototype._onRequestSessionSuccess=function(e) {
        var parent=this;
        this.notify('Trace','_onRequestSessionSuccess scope:'+parent._var().scope);
        this.notify('Trace','_onRequestSessionSuccess session success: ' + e.sessionId);
        //appendMessage('session success: ' + e.sessionId);
        parent._saveSessionID(e.sessionId);
        parent._var().session = e;
        document.getElementById('casticon').src = parent._var().CAST_ICON_THUMB_ACTIVE;
        parent._var().session.addUpdateListener(this._sessionUpdateListener.bind(this));
        if (parent._var().session.media.length != 0) {
            parent._onMediaDiscovered.call(this,'onRequestSession', parent._var().session.media[0]);
        }
        parent._var().session.addMediaListener(
            parent._onMediaDiscovered.bind(this, 'addMediaListener'));
    }

    /**
     * callback on launch error
     */
    _thizOBJ_.prototype._onLaunchError=function() {
        var parent=this;
        this.notify('Trace','_onLaunchError scope:'+parent._var().scope);
        this.notify('Trace','launch error');
        //appendMessage('launch error');
    }

    /**
     * save session ID into localStorage for sharing
     * @param {string} sessionId A string for session ID
     */
    _thizOBJ_.prototype._saveSessionID=function(sessionId) {
        var parent=this;
        this.notify('Trace','_saveSessionID scope:'+parent._var().scope);
        // Check browser support of localStorage
        if (typeof(Storage) != 'undefined') {
            // Store sessionId and timestamp into an object
            var object = {id: sessionId, timestamp: new Date().getTime()};
            localStorage.setItem('storedSession', JSON.stringify(object));
        }
    }

    /**
     * join session by a given session ID
     */
    _thizOBJ_.prototype._joinSessionBySessionId=function() {
        var parent=this;
        this.notify('Trace','_joinSessionBySessionId scope:'+parent._var().scope);
        if (parent._var().storedSession) {
            this.notify('Trace','Found stored session id: ' + parent._var().storedSession.id);
            chrome.cast.requestSessionById(parent._var().storedSession.id);
        }
    }

    /**
     * stop app/session
     */
    _thizOBJ_.prototype._stopApp=function() {
        var parent=this;
        this.notify('Trace','_stopApp scope:'+parent._var().scope);
        parent._var().session.stop(
            function(e){return parent._onStopAppSuccess.call(parent,e)},
            function(e){return parent._onError.call(parent,e)}
        );
        //session.stop(parent._onStopAppSuccess.call(parent), parent._onError.call(parent));
        if (parent._var().timer) {
            clearInterval(parent._var().timer);
        }
    }

    /**
     * load media specified by custom URL
     */
    _thizOBJ_.prototype._loadCustomMedia=function() {
        var parent=this;
        this.notify('Trace','_loadCustomMedia scope:'+parent._var().scope);
        var customMediaURL = document.getElementById('customMediaURL').value;
        if (customMediaURL.length > 0) {
            this._loadMedia(customMediaURL);
        }
    }

    /**
     * load media
     * @param {string} mediaURL media URL string
     * @this loadMedia
     */
    _thizOBJ_.prototype._loadMedia=function(mediaURL) {
        var parent=this;
        this.notify('Trace','_loadMedia scope:'+parent._var().scope);
        if (!parent._var().session) {
            this.notify('Trace','no session');
            //appendMessage('no session');
            return;
        }

        if (mediaURL) {
            var mediaInfo = new chrome.cast.media.MediaInfo(mediaURL);
            parent._var().currentMediaTitle = 'custom title needed';
            parent._var().currentMediaThumb = 'images/video_icon.png';
            document.getElementById('thumb').src = parent._var().MEDIA_SOURCE_ROOT +
                parent._var().currentMediaThumb;
        }
        else {
            this.notify('Trace','loading...' + parent._var().currentMediaURL);
            //appendMessage('loading...' + currentMediaURL);
            var mediaInfo = new chrome.cast.media.MediaInfo(parent._var().currentMediaURL);
        }
        mediaInfo.metadata = new chrome.cast.media.GenericMediaMetadata();
        mediaInfo.metadata.metadataType = chrome.cast.media.MetadataType.GENERIC;
        mediaInfo.contentType = 'video/mp4';

        mediaInfo.metadata.title = parent._var().currentMediaTitle;
        mediaInfo.metadata.images = [{'url': parent._var().MEDIA_SOURCE_ROOT + parent._var().currentMediaThumb}];

        var request = new chrome.cast.media.LoadRequest(mediaInfo);
        request.autoplay = true;
        request.currentTime = 0;

        parent._var().session.loadMedia(request,
            parent._onMediaDiscovered.bind(parent, 'loadMedia'),
            function(e){parent._onMediaError.call(parent,e)});
            //this._onMediaError.call(parent));

    }



    _thizOBJ_.prototype._myCustomloadMedia=function() {
        var parent=this;


        this.notify('Trace','_myCustomloadMedia scope:'+parent._var().scope);

        var mediaURL='http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8';
        var mediaInfo = new chrome.cast.media.MediaInfo(mediaURL);
        parent._var().currentMediaTitle = 'custom title needed';
        parent._var().currentMediaThumb = 'images/video_icon.png';
        mediaInfo.metadata = new chrome.cast.media.GenericMediaMetadata();
        mediaInfo.metadata.metadataType = chrome.cast.media.MetadataType.GENERIC;
        mediaInfo.contentType = 'application/x-mpegurl';
        mediaInfo.metadata.title = parent._var().currentMediaTitle;
        mediaInfo.metadata.images = [{'url': parent._var().MEDIA_SOURCE_ROOT + parent._var().currentMediaThumb}];

        var request = new chrome.cast.media.LoadRequest(mediaInfo);
            request.autoplay = true;
            request.currentTime = 0;

        parent._var().session.loadMedia(request,
            parent._onMediaDiscovered.bind(parent, 'loadMedia'),
            function(e){parent._onMediaError.call(parent,e)});


    }
















    /**
     * callback on success for loading media
     * @param {string} how info string from callback
     * @param {Object} mediaSession media session object
     * @this onMediaDiscovered
     */
    _thizOBJ_.prototype._onMediaDiscovered=function(how, mediaSession) {
        var parent=this;
        this.notify('Trace','_onMediaDiscovered scope:'+parent._var().scope);
        this.notify('Trace','new media session ID:' + mediaSession.mediaSessionId);
        //appendMessage('new media session ID:' + mediaSession.mediaSessionId +
        // ' (' + how + ')');
        parent._var({currentMediaSession : mediaSession});
        parent._var().currentMediaSession.addUpdateListener(function (e){return parent._onMediaStatusUpdate.call(parent,e)});
        //currentMediaSession.addUpdateListener.call(parent,this._onMediaStatusUpdate);
        parent._var().mediaCurrentTime = parent._var().currentMediaSession.currentTime;
        playpauseresume.innerHTML = 'Play';
        document.getElementById('casticon').src = parent._var().CAST_ICON_THUMB_ACTIVE;
        document.getElementById('playerstate').innerHTML =
            parent._var().currentMediaSession.playerState;
        if (!parent._var().timer) {
            parent._var().timer = setInterval(this._updateCurrentTime.bind(this),
                parent._var().PROGRESS_BAR_UPDATE_DELAY);
            playpauseresume.innerHTML = 'Pause';
        }
    }

    /**
     * callback on media loading error
     * @param {Object} e A non-null media object
     */
    _thizOBJ_.prototype._onMediaError=function(e) {
        var parent=this;
        this.notify('Trace','_onMediaError scope:'+parent._var().scope);
        //appendMessage('media error');
        document.getElementById('casticon').src = parent._var().CAST_ICON_THUMB_WARNING;
    }

    /**
     * get media status initiated by sender when necessary
     * currentMediaSession gets updated
     * @this getMediaStatus
     */
    _thizOBJ_.prototype._getMediaStatus=function() {
        var parent = this;
        this.notify('Trace','_getMediaStatus scope:'+parent._var().scope);
        if (!parent._var().session || !parent._var().currentMediaSession) {
            return;
        }

        parent._var().currentMediaSession.getStatus(null,
            this._mediaCommandSuccessCallback.bind(this, 'got media status'),
            function(e){return parent._onError.call(parent,e)});
    }

    /**
     * callback for media status event
     * @param {boolean} isAlive status from callback
     */
    _thizOBJ_.prototype._onMediaStatusUpdate=function(isAlive) {
        var parent=this;
        this.notify('Trace','_onMediaStatusUpdate scope:'+parent._var().scope);
        if (!isAlive) {
            currentMediaTime = 0;
        }
        else {
            if (parent._var().currentMediaSession.playerState == 'PLAYING') {
                if (parent._var().progressFlag) {
                    document.getElementById('progress').value = parseInt(100 *
                        parent._var().currentMediaSession.currentTime /
                        parent._var().currentMediaSession.media.duration);
                    document.getElementById('progress_tick').innerHTML =
                        parent._var().currentMediaSession.currentTime;
                    document.getElementById('duration').innerHTML =
                        parent._var().currentMediaSession.media.duration;
                    parent._var({progressFlag : 0});
                }
                document.getElementById('playpauseresume').innerHTML = 'Pause';
            }
        }
        document.getElementById('playerstate').innerHTML =
            parent._var().currentMediaSession.playerState;
    }

    /**
     * Updates the progress bar shown for each media item.
     */
    _thizOBJ_.prototype._updateCurrentTime=function() {
        var parent=this;
        this.notify('Trace','_updateCurrentTime scope:'+parent._var().scope);
        if (!parent._var().session || !parent._var().currentMediaSession) {
            return;
        }

        if (parent._var().currentMediaSession.media && parent._var().currentMediaSession.media.duration != null) {
            var cTime = parent._var().currentMediaSession.getEstimatedTime();
            document.getElementById('progress').value = parseInt(100 * cTime /
                parent._var().currentMediaSession.media.duration);
            document.getElementById('progress_tick').innerHTML = cTime;
        }
        else {
            document.getElementById('progress').value = 0;
            document.getElementById('progress_tick').innerHTML = 0;
            if (parent._var().timer) {
                clearInterval(parent._var().timer);
            }
        }
    }

    /**
     * play media
     * @this playMedia
     */
    _thizOBJ_.prototype._playMedia=function() {
        var parent=this;
        this.notify('Trace','_playMedia scope:'+parent._var().scope);
        if (!parent._var().currentMediaSession) {
            return;
        }

        if (parent._var().timer) {
            clearInterval(parent._var().timer);
        }

        var playpauseresume = document.getElementById('playpauseresume');
        if (playpauseresume.innerHTML == 'Play') {
            parent._var().currentMediaSession.play(null,
                this._mediaCommandSuccessCallback.bind(this, 'playing started for ' +
                    parent._var().currentMediaSession.sessionId),
                function(e){return parent._onError.call(parent,e)});//   this._onError);
            playpauseresume.innerHTML = 'Pause';
            this.notify('Trace','play started');
            parent._var().timer = setInterval(this._updateCurrentTime.bind(this),
                parent._var().PROGRESS_BAR_UPDATE_DELAY);
        }
        else {
            if (playpauseresume.innerHTML == 'Pause') {
                parent._var().currentMediaSession.pause(null,
                    this._mediaCommandSuccessCallback.bind(this, 'paused ' +
                        parent._var().currentMediaSession.sessionId),
                    function(e){return parent._onError.call(parent,e)});
                  //  this._onError);
                playpauseresume.innerHTML = 'Resume';
                this.notify('Trace','paused');
            }
            else {
                if (playpauseresume.innerHTML == 'Resume') {
                    parent._var().currentMediaSession.play(null,
                        this._mediaCommandSuccessCallback.bind(this, 'resumed ' +
                            parent._var().currentMediaSession.sessionId),
                        function(e){return parent._onError.call(parent,e)});
                    playpauseresume.innerHTML = 'Pause';
                    this.notify('Trace','resumed');
                    parent._var().timer = setInterval(this._updateCurrentTime.bind(this),
                        parent._var().PROGRESS_BAR_UPDATE_DELAY);
                }
            }
        }
    }

    /**
     * stop media
     * @this stopMedia
     */
    _thizOBJ_.prototype._stopMedia=function() {
        var parent=this;
        this.notify('Trace','_stopMedia scope:'+parent._var().scope);
        if (!parent._var().currentMediaSession)
            return;

        parent._var().currentMediaSession.stop(null,
            this._mediaCommandSuccessCallback.bind(this, 'stopped ' +
                parent._var().currentMediaSession.sessionId),
            function(e){return parent._onError.call(parent,e)});
        var playpauseresume = document.getElementById('playpauseresume');
        playpauseresume.innerHTML = 'Play';
        this.notify('Trace','media stopped');
        if (parent._var().timer) {
            clearInterval(parent._var().timer);
        }
    }

    /**
     * set media volume
     * @param {Number} level A number for volume level
     * @param {Boolean} mute A true/false for mute/unmute
     * @this setMediaVolume
     */
    _thizOBJ_.prototype._setMediaVolume=function(level, mute) {
        var parent=this;
        this.notify('Trace','_setMediaVolume scope:'+parent._var().scope);
        if (!parent._var().currentMediaSession)
            return;

        var volume = new chrome.cast.Volume();
        volume.level = level;
        parent._var({currentVolume : volume.level});
        volume.muted = mute;
        var request = new chrome.cast.media.VolumeRequest();
        request.volume = volume;
        parent._var().currentMediaSession.setVolume(request,
            this._mediaCommandSuccessCallback.bind(this, 'media set-volume done'),
            function(e){return parent._onError.call(parent,e)});
    }

    /**
     * set receiver volume
     * @param {Number} level A number for volume level
     * @param {Boolean} mute A true/false for mute/unmute
     * @this setReceiverVolume
     */
    _thizOBJ_.prototype._setReceiverVolume=function(level, mute) {
        var parent=this;
        this.notify('Trace','_setReceiverVolume scope:'+parent._var().scope);
        if (!parent._var().session)
            return;

        if (!mute) {
            parent._var().session.setReceiverVolumeLevel(level,
                this._mediaCommandSuccessCallback.bind(this, 'media set-volume done'),
                function(e){return parent._onError.call(parent,e)});
            parent._var({currentVolume : level});
        }
        else {
            parent._var().session.setReceiverMuted(true,
                this._mediaCommandSuccessCallback.bind(this, 'media set-volume done'),
                function(e){return parent._onError.call(parent,e)});
        }
    }

    /**
     * mute media
     */
    _thizOBJ_.prototype._muteMedia=function() {
        var parent=this;
        this.notify('Trace','_muteMedia scope:'+parent._var().scope);

        if (!parent._var().session || !parent._var().currentMediaSession) {
            return;
        }

        var muteunmute = document.getElementById('muteunmute');
        // It's recommended that setReceiverVolumeLevel be used
        // but media stream volume can be set instread as shown in the
        // setMediaVolume(currentVolume, true);
        if (muteunmute.innerHTML == 'Mute media') {
            muteunmute.innerHTML = 'Unmute media';
            parent._setReceiverVolume.call(parent,parent._var().currentVolume, true);
            this.notify('Trace','media muted');
        } else {
            muteunmute.innerHTML = 'Mute media';
            parent._setReceiverVolume.call(parent,parent._var().currentVolume, false);
            this.notify('Trace','media unmuted');
        }
    }

    /**
     * seek media position
     * @param {Number} pos A number to indicate percent
     * @this seekMedia
     */
    _thizOBJ_.prototype._seekMedia=function(pos) {
        var parent=this;
        this.notify('Trace','_seekMedia scope:'+parent._var().scope);

        this.notify('Trace','Seeking ' + parent._var().currentMediaSession.sessionId + ':' +
            parent._var().currentMediaSession.mediaSessionId + ' to ' + pos + '%');
        parent._var({progressFlag : 0});
        var request = new chrome.cast.media.SeekRequest();
        request.currentTime = pos * parent._var().currentMediaSession.media.duration / 100;
        parent._var().currentMediaSession.seek(request,
            parent._onSeekSuccess.bind(this, 'media seek done'),
            function(e){return parent._onError.call(parent,e)});
    }

    /**
     * callback on success for media commands
     * @param {string} info A message string
     */
    _thizOBJ_.prototype._onSeekSuccess=function(info) {
        var parent=this;
        this.notify('Trace','_onSeekSuccess scope:'+parent._var().scope);
        this.notify('Trace',info);
        //appendMessage(info);
        setTimeout(function() {parent._var().progressFlag = 1},parent._var().PROGRESS_BAR_UPDATE_DELAY);
    }

    /**
     * callback on success for media commands
     * @param {string} info A message string
     */
    _thizOBJ_.prototype._mediaCommandSuccessCallback=function(info) {
        var parent=this;
        this.notify('Trace','_mediaCommandSuccessCallback scope:'+parent._var().scope);
        this.notify('Trace',info);
        //appendMessage(info);
    }

    _thizOBJ_.prototype._sendMessage=function(message) {
        var parent=this;
        this.notify('Trace','_sendMessage scope:'+parent._var().scope);
        var namespace="urn:x-cast:com.aenetworks.cast.media";//parent._var().session.namespaces[0].name;
        this.notify('Trace','sendMessage('+message+') @ ['+ namespace +']');
        if (parent._var().session!=null) {
            //session.sendMessage(namespace, message, onSuccess.bind(this, "Message sent: " + message), this._onError);
            parent._var().session.sendMessage(namespace, message, function(retData){parent.notify('Trace',':)'+JSON.stringify(retData))}, function(){parent.notify('Trace',':('+JSON.stringify(retData))});
        }
        else {

            parent.notify('Trace','no session! *shrug*');
            //chrome.cast.requestSession(function(e) {
            //  session = e;
            //  session.sendMessage(session.namespaces[0].name, message, onSuccess.bind(this, "Message sent: " + message), this._onError);
            //}, this._onError);
        }
    }

    _thizOBJ_.prototype._testbutton=function(){
        var parent=this;
        parent.notify('Trace','_testbutton');

        parent._sendMessage({sometext:'crapple!'});

        // parent._sendMessage('crapple!');
        //console.log('MEH'+parent._sendMessage('sending some text crapple!'));

    }

    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});



