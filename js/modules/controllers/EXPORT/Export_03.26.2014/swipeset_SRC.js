/*!Last Updated: 03.26.2014[17.25.27] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! swipeset MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLS*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.26.2014[17.25.27] by Jonathan Robles*/
define([ "jquery", "underscore", "modules/definitions/standardmodule", "hammer", "tweenmax", "scrollto" ], function($, _, parentObject, Hammer, TweenMax, Scrollto) {
    function _thisObject_(o) {
        var settings = {
            type: "swipeset",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv_X"),
            virtualchildren: false,
            virtualchild_overlap: 0,
            yscroll: false,
            Xscrolldetect: "release dragleft dragright swipeleft swiperight dragstart",
            Yscrolldetect: "release dragup dragdown swipeup swipedown dragstart",
            currentslide: 0,
            lastdebouncedslide: 0,
            usetouchbinds: true,
            enable_touchbinds: true,
            snaponrelease: true,
            callback: function(o) {},
            childchange: function(count) {
                var self = this.parent;
                var destination = undefined;
                var animationvars = undefined;
                if (!self._var().yscroll) {
                    destination = self._var().childVars[count].XoffsetfromScroll;
                    animationvars = {
                        scrollTo: {
                            x: destination
                        },
                        ease: Power4.easeOut
                    };
                } else {
                    destination = self._var().childVars[count].YoffsetfromScroll;
                    animationvars = {
                        scrollTo: {
                            y: destination
                        },
                        ease: Power4.easeOut
                    };
                }
                TweenMax.to(self._var().target, .7, animationvars);
            },
            onscroll_callback: function(o) {},
            onscroll_start: function(o) {},
            onscroll_debounce_callback: function(o) {},
            onscroll_debounce_rate: 200,
            scrollsize: undefined,
            currentposition: undefined,
            currentpercent: undefined,
            targetXsize: undefined,
            targetYsize: undefined,
            targetwidth: undefined,
            targetheight: undefined,
            totalslides: undefined,
            childObjects: undefined,
            childVars: undefined,
            childVar_setter: function(count, value) {
                var self = this;
                self.count = count;
                self.childOBJ = value;
                self.target = $(value);
                self.Xsize = self.target.outerWidth();
                self.Ysize = self.target.innerHeight();
                self.Xdefault = Math.round(self.target.position().left);
                self.Ydefault = Math.round(self.target.position().top);
                self.XoffsetfromScroll = undefined;
                self.YoffsetfromScroll = undefined;
            },
            ObjHammer: undefined,
            isbeingdragged: false,
            startmove: true,
            busy: false
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onQuickjump: function(o) {
                    if (o.senderID == myID) {
                        parent.quickjump(o.data.count);
                    }
                },
                onJump: function(o) {
                    if (o.senderID == myID) {
                        parent.jump(o.data.count);
                    }
                },
                onPagejump: function(o) {
                    if (o.senderID == myID) {
                        parent.pagejump(o.data.count);
                    }
                },
                onNext: function(o) {
                    if (o.senderID == myID) {
                        parent.next();
                    }
                },
                onPrev: function(o) {
                    if (o.senderID == myID) {
                        parent.prev();
                    }
                }
            };
        }());
    };
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._var({
            busy: true
        });
        this._startlisteners();
        var self = this;
        this.refresh();
    };
    _thisObject_.prototype.refresh = function() {
        var self = this;
        self._var({
            busy: true
        });
        self.notify("Trace", "refresh");
        self.getpanelsizes();
        !self._var().virtualchildren ? self._childrentochildObjects() : self._virtualspacetochildObjects();
        self.quickjump(self._var().currentslide);
        var handlerdetect = !self._var().yscroll ? self._var().Xscrolldetect : self._var().Yscrolldetect;
        if (self._var().usetouchbinds) {
            self._var().ObjHammer = Hammer(self._var().target, {
                drag_lock_to_axis: true
            }).on(handlerdetect, self._bindHandler(self));
        }
        self._var().target.unbind("scroll");
        self._var().target.scroll(self._onscroll(self));
        self._var({
            busy: false
        });
        if (self._var().callback != undefined) {
            self._var().callback(self);
        }
        self._var().lastdebouncedslide = self._var().currentslide;
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype._childrentochildObjects = function() {
        var self = this;
        self._var({
            childObjects: self._var().target.children()
        });
        var childObjects = self._var().childObjects;
        self._var({
            totalslides: childObjects.length,
            childVars: []
        });
        $.each(childObjects, function(count, value) {
            childVar_setter_local = self._var().childVar_setter;
            self._var().childVars.push(new childVar_setter_local(count, value));
        });
        $.each(self._var().childVars, function(count, value) {
            var zeroXdefault = self._var().childVars[0].Xdefault;
            var zeroYdefault = self._var().childVars[0].Ydefault;
            var marginOffsetX = value.target.css("marginLeft");
            var marginOffsetY = value.target.css("marginTop");
            value.XoffsetfromScroll = Math.round(value.Xdefault - zeroXdefault);
            value.YoffsetfromScroll = Math.round(value.Ydefault - zeroYdefault);
            if (marginOffsetX != undefined) {
                value.XoffsetfromScroll += parseInt(marginOffsetX, 10);
            }
            if (marginOffsetY != undefined) {
                value.YoffsetfromScroll += parseInt(marginOffsetY, 10);
            }
        });
        var totalscrollarea = !self._var().yscroll ? self._var().targetwidth : self._var().targetheight;
        var singlechildsize = !self._var().yscroll ? self._var().targetXsize : self._var().targetYsize;
        var scrollsize = totalscrollarea - singlechildsize;
        self._var({
            scrollsize: scrollsize
        });
    };
    _thisObject_.prototype._virtualspacetochildObjects = function() {
        var self = this;
        self._var({
            childObjects: self._var().target.children()
        });
        var totalscrollarea = !self._var().yscroll ? self._var().targetwidth : self._var().targetheight;
        var singlechildsize = !self._var().yscroll ? self._var().targetXsize : self._var().targetYsize;
        singlechildsize = singlechildsize - self._var().virtualchild_overlap;
        var scrollsize = totalscrollarea - singlechildsize;
        var tempStopArray = [];
        var childcount = Math.floor(scrollsize / singlechildsize);
        for (tempval = 0; tempval < childcount + 1; tempval++) {
            tempStopArray.push(tempval * singlechildsize);
        }
        tempStopArray.push(scrollsize);
        self._var({
            totalslides: tempStopArray.length,
            childVars: [],
            scrollsize: scrollsize
        });
        $.each(tempStopArray, function(count, value) {
            self._var().childVars.push({
                XoffsetfromScroll: value,
                YoffsetfromScroll: value
            });
        });
    };
    _thisObject_.prototype.getpanelsizes = function() {
        var self = this;
        var objreturn = {};
        objreturn.totalslides = self._var().totalslides;
        objreturn.targetwidth = self._var().targetwidth = self._var().target[0].scrollWidth;
        objreturn.targetheight = self._var().targetheight = self._var().target[0].scrollHeight;
        objreturn.targetXsize = self._var().targetXsize = self._var().target.innerWidth();
        objreturn.targetYsize = self._var().targetYsize = self._var().target.innerHeight();
        return objreturn;
    };
    _thisObject_.prototype.getclosestcount = function(position) {
        var self = this;
        var Offsets = _.map(self._var().childVars, function(idx) {
            return !self._var().yscroll ? Math.abs(idx.XoffsetfromScroll - position) : Math.abs(idx.YoffsetfromScroll - position);
        });
        var closestdiff = _.reduce(Offsets, function(memo, val) {
            return val < memo ? val : memo;
        }, 1e4);
        var counttoReturn = _.countOf(Offsets, closestdiff);
        self._var().currentslide = counttoReturn;
        return counttoReturn;
    };
    _thisObject_.prototype._returncurrentposition = function() {
        var self = this;
        return !self._var().yscroll ? self._var().target.scrollLeft() : self._var().target.scrollTop();
    };
    _thisObject_.prototype._gotoposition = function(getdestination) {
        var self = this;
        !self._var().yscroll ? self._var().target.scrollLeft(getdestination) : self._var().target.scrollTop(getdestination);
    };
    _thisObject_.prototype._gotopercent = function(getpercent) {
        var self = this;
        var scrollsize = self._var().scrollsize;
        var jumptoposition = scrollsize / (100 / getpercent);
        self._gotoposition(jumptoposition);
    };
    _thisObject_.prototype.selfie = function(o) {
        var self = this;
        var options = {
            yscroll: self._var().yscroll,
            quick: false
        };
        options = $.extend(options, o);
        if (!options.yscroll) {
            var thiscount = self.getclosestcount(self._returncurrentposition());
            if (!options.quick) {
                self.jump(thiscount);
            } else {
                self.quickjump(thiscount);
            }
        } else {
            var thiscount = self.getclosestcount(self._returncurrentposition());
            if (!options.quick) {
                self.jump(thiscount);
            } else {
                self.quickjump(thiscount);
            }
        }
    };
    _thisObject_.prototype.next = function(o) {
        var self = this;
        var options = {
            quick: false
        };
        options = $.extend(options, o);
        var thiscount = !self._var().yscroll ? self.getclosestcount(self._returncurrentposition()) : self.getclosestcount(self._var().target.scrollTop());
        var nextidx = thiscount == self._var().totalslides - 1 ? thiscount : thiscount + 1;
        var diff = Math.abs(nextidx - Number(self._var().lastdebouncedslide));
        if (diff > 1) {
            nextidx -= diff - 1;
        }
        !options.quick ? self.jump(nextidx) : self.quickjump(nextidx);
    };
    _thisObject_.prototype.prev = function(o) {
        var self = this;
        var options = {
            quick: false
        };
        options = $.extend(options, o);
        var thiscount = !self._var().yscroll ? self.getclosestcount(self._returncurrentposition()) : self.getclosestcount(self._var().target.scrollTop());
        var nextidx = thiscount == 0 ? 0 : thiscount - 1;
        var diff = Math.abs(nextidx - Number(self._var().lastdebouncedslide));
        if (diff > 1) {
            nextidx += diff - 1;
        }
        !options.quick ? self.jump(nextidx) : self.quickjump(nextidx);
    };
    _thisObject_.prototype.jump = function(count) {
        var self = this;
        self._var().childchange(count, self);
    };
    _thisObject_.prototype.pagejump = function(count) {
        this.jump(count);
    };
    _thisObject_.prototype.quickjump = function(count) {
        var self = this;
        if (!self._var().yscroll) {
            var destination = self._var().childVars[count].XoffsetfromScroll;
        } else {
            var destination = self._var().childVars[count].YoffsetfromScroll;
        }
        self._gotoposition(destination);
    };
    _thisObject_.prototype._bindHandler = function(self) {
        var lastCase = undefined;
        var touchpoint = undefined;
        var targetposition = function() {
            touchpoint = self._returncurrentposition();
        };
        var casechecks = undefined;
        var directions = undefined;
        if (!self._var().yscroll) {
            casechecks = [ "dragright", "dragleft", "swipeleft", "swiperight", "release", "dragstart" ];
            directions = [ Hammer.DIRECTION_RIGHT, Hammer.DIRECTION_LEFT ];
        } else {
            casechecks = [ "dragdown", "dragup", "swipeup", "swipedown", "release", "dragstart" ];
            directions = [ Hammer.DIRECTION_DOWN, Hammer.DIRECTION_UP ];
        }
        var eventHandler = function(ev) {
            ev.gesture.preventDefault();
            if (ev.type != lastCase) {
                targetposition();
            }
            if (self._var().enable_touchbinds) {
                switch (ev.type) {
                  case casechecks[5]:
                    self._var().lastdebouncedslide = self._var().currentslide;
                    break;

                  case casechecks[4]:
                    self._var().isbeingdragged = false;
                    targetposition();
                    if (self._var().snaponrelease) {
                        self.selfie();
                    } else {
                        var currentvars = {};
                        currentvars.scrollsize = self._var().scrollsize;
                        currentvars.currentposition = self._returncurrentposition();
                        currentvars.currentpercent = 100 / (currentvars.scrollsize / currentvars.currentposition);
                        self._debounce(self)(currentvars);
                    }
                    ev.gesture.stopDetect();
                    break;

                  case casechecks[0]:
                  case casechecks[1]:
                    self._var().isbeingdragged = true;
                    var pane_offset = touchpoint;
                    var drag_offset = !self._var().yscroll ? ev.gesture.deltaX : ev.gesture.deltaY;
                    if (ev.gesture.direction == directions[0]) {
                        drag_offset = -Math.abs(drag_offset);
                    } else {
                        if (ev.gesture.direction == directions[1]) {
                            drag_offset = Math.abs(drag_offset);
                        }
                    }
                    var destination = pane_offset + drag_offset;
                    !self._var().yscroll ? self._var().target.scrollLeft(destination) : self._var().target.scrollTop(destination);
                    break;

                  case casechecks[2]:
                    self.next();
                    self._var().isbeingdragged = false;
                    ev.gesture.stopDetect();
                    break;

                  case casechecks[3]:
                    self.prev();
                    self._var().isbeingdragged = false;
                    ev.gesture.stopDetect();
                    break;
                }
            }
            lastCase = ev.type;
        };
        return eventHandler;
    };
    _thisObject_.prototype._debounce = function(self) {
        var debouncer = _.debounce(function(currentvars) {
            var currentposition = self._returncurrentposition();
            var closecount = self.getclosestcount(currentposition);
            var destination = undefined;
            if (!self._var().yscroll) {
                destination = self._var().childVars[closecount].XoffsetfromScroll;
            } else {
                destination = self._var().childVars[closecount].YoffsetfromScroll;
            }
            var difference = Math.abs(currentposition - destination);
            if (difference > 1 && !self._var().isbeingdragged && self._var().snaponrelease) {
                self.selfie();
            } else {
                if (difference < 2 || !self._var().isbeingdragged && !self._var().snaponrelease) {
                    if (!self._var().startmove) {
                        self._var().lastdebouncedslide = self._var().currentslide;
                        self._var().onscroll_debounce_callback(currentvars);
                        self._var({
                            startmove: true
                        });
                    }
                }
            }
        }, self._var().onscroll_debounce_rate);
        return debouncer;
    };
    _thisObject_.prototype._onscroll = function(self) {
        var Offsets = _.map(self._var().childVars, function(idx) {
            return !self._var().yscroll ? Math.abs(idx.XoffsetfromScroll) : Math.abs(idx.YoffsetfromScroll);
        });
        var debouncefunc = self._debounce(self);
        var handler = function() {
            var currentvars = {};
            currentvars.scrollsize = self._var().scrollsize;
            currentvars.currentposition = self._returncurrentposition();
            currentvars.currentpercent = 100 / (currentvars.scrollsize / currentvars.currentposition);
            if (self._var().startmove) {
                self._var().onscroll_start(currentvars);
                self._var({
                    startmove: false
                });
            }
            self._var().onscroll_callback(currentvars);
            debouncefunc(currentvars);
        };
        return handler;
    };
    return _thisObject_;
});
