/*!Last Updated: 03.25.2014[14.53.46] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! slideset MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLS*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.25.2014[14.53.46] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule", "tweenmax", "underscore", "modernizr" ], function($, parentObject, TweenMax, _, Modernizr) {
    function _thisObject_(o) {
        var settings = {
            type: "slideset",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv"),
            childObjects: undefined,
            currentslide: 0,
            totalslides: undefined,
            loopslides: false,
            toplevelZ: 500,
            autoZ: true,
            child_stack: [],
            child_Zstack: [],
            child_Mstack: [],
            child_offstack: [],
            child_centerstack: [],
            child_stackpile_left: [],
            child_stackpile_right: [],
            errorfunctions: {
                childchange: function(data) {
                    console.log("error: same count");
                }
            },
            interval: undefined,
            busy: false,
            unfocusALL: true,
            childtrack: function(count) {
                var tracking = function(count) {
                    console.log("some tracking function you defined for scene# " + count);
                };
                if (count != this.lasttrack) {
                    tracking(count);
                    this.lasttrack = count;
                }
            },
            childinit: function(count, currentslide) {},
            childchange: function(count, o) {
                var self = this;
                self.parent._setstacks(count);
                self.parent._getZorder(count);
                if (self.parent._var().autoZ) {
                    self.parent._putZorder(count);
                }
                if (this.currentslide === count) {
                    this.errorfunctions["childchange"]();
                } else {
                    var passVars = {
                        count: count,
                        Zcount: self.child_Zstack,
                        Multiplier: self.child_Mstack
                    };
                    focusvars = $.extend(passVars, o);
                    this.child_focus(focusvars);
                    if (this.unfocusALL) {
                        $.each(self.parent._var().child_stackpile_left, function(offset, value) {
                            var leftvars = $.extend({
                                unfocusedcount: value,
                                offset: offset
                            }, passVars);
                            leftvars = $.extend(leftvars, o);
                            self.parent._var().child_unfocus(leftvars);
                        });
                        $.each(self.parent._var().child_stackpile_right, function(offset, value) {
                            var rightvars = $.extend({
                                unfocusedcount: value,
                                offset: offset
                            }, passVars);
                            rightvars = $.extend(rightvars, o);
                            self.parent._var().child_unfocus(rightvars);
                        });
                    } else {
                        var unfocusVars = $.extend({
                            unfocusedcount: this.currentslide
                        }, passVars);
                        this.child_unfocus(unfocusVars);
                    }
                    this.currentslide = count;
                }
            },
            child_focus: function(o) {
                var argz = {
                    Zcount: undefined,
                    Multiplier: undefined,
                    count: undefined,
                    speed: .5,
                    tweenvars: {
                        x: 0,
                        y: 0,
                        rotation: 0,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: 1,
                        opacity: 1
                    }
                };
                argz = $.extend(argz, o);
                var tempObject = $(this.childObjects[argz.count]);
                argz = $.extend(argz, {
                    object: tempObject
                });
                if (Modernizr.csstransitions) {
                    TweenMax.to(argz.object, argz.speed, argz.tweenvars);
                } else {
                    argz.object.show();
                }
            },
            child_unfocus: function(o) {
                var argz = {
                    Zcount: undefined,
                    Multiplier: undefined,
                    count: undefined,
                    unfocusedcount: undefined,
                    speed: 1,
                    offset: 0,
                    tweenvarsIN: {
                        x: -400,
                        y: 0,
                        rotation: 10,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: .5,
                        opacity: 0
                    },
                    tweenvarsOUT: {
                        x: 400,
                        y: 0,
                        rotation: -10,
                        rotationY: 0,
                        rotationZ: 0,
                        rotationX: 0,
                        scale: .5,
                        opacity: 0
                    }
                };
                argz = $.extend(argz, o);
                var tempObject = $(this.childObjects[argz.unfocusedcount]);
                argz = $.extend(argz, {
                    object: tempObject
                });
                argz.tweenvarsIN.y -= 30 * argz.offset;
                argz.tweenvarsOUT.y -= 30 * argz.offset;
                argz.tweenvarsIN.x += 10 * argz.offset * (2 * argz.offset);
                argz.tweenvarsOUT.x -= 10 * argz.offset * (2 * argz.offset);
                argz.tweenvarsIN.rotation += 15 * argz.offset;
                argz.tweenvarsOUT.rotation += 15 * argz.offset;
                argz.tweenvarsIN.opacity += .9 - .15 * argz.offset;
                argz.tweenvarsOUT.opacity += .9 - .15 * argz.offset;
                if (Modernizr.csstransitions) {
                    if (argz.unfocusedcount < argz.count) {
                        TweenMax.to(argz.object, argz.speed, argz.tweenvarsIN);
                    } else {
                        TweenMax.to(argz.object, argz.speed, argz.tweenvarsOUT);
                    }
                } else {
                    argz.object.hide();
                }
            },
            _multijump: function(count) {
                var parent = this;
                var counter = {
                    tempvar: parent.currentslide
                };
                var lastcount = parent.currentslide;
                var difference = Math.abs(count - parent.currentslide);
                var animtime = difference * .2;
                TweenMax.to(counter, animtime, {
                    tempvar: count,
                    onUpdate: function() {
                        var targetvalue = Math.ceil(counter.tempvar);
                        if (targetvalue != lastcount) {
                            parent.childchange(targetvalue);
                            lastcount = targetvalue;
                        }
                    }
                });
            }
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onQuickjump: function(o) {
                    if (o.senderID == myID) {
                        parent.quickjump(o.data.count);
                    }
                },
                onJump: function(o) {
                    if (o.senderID == myID) {
                        parent.jump(o.data.count);
                    }
                },
                onPagejump: function(o) {
                    if (o.senderID == myID) {
                        parent.pagejump(o.data.count);
                    }
                },
                onNext: function(o) {
                    if (o.senderID == myID) {
                        parent.next();
                    }
                },
                onPrev: function(o) {
                    if (o.senderID == myID) {
                        parent.prev();
                    }
                }
            };
        }());
    };
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thisObject_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this._childrentochildObjects();
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype._childrentochildObjects = function() {
        var parent = this;
        parent._var({
            childObjects: parent._var().target.children()
        });
        var childObjects = parent._var().childObjects;
        parent._var({
            totalslides: childObjects.length,
            child_stack: [],
            child_offstack: [],
            child_centerstack: [],
            child_stackpile_left: [],
            child_stackpile_right: []
        });
        $.each(childObjects, function(count, value) {
            parent._var().child_stack.push(count);
            parent._var().childinit.call($(value), count, parent._var().currentslide);
        });
        parent._setstacks(parent._var().currentslide);
        parent._getZorder(parent._var().currentslide);
        parent._var().child_focus({
            count: parent._var().currentslide,
            speed: 0,
            Zcount: parent._var().child_Zstack,
            Multiplier: parent._var().child_Mstack
        });
        if (parent._var().autoZ) {
            parent._putZorder(parent._var().currentslide);
        }
        $.each(parent._var().child_stackpile_left, function(offset, value) {
            parent._var().child_unfocus({
                count: parent._var().currentslide,
                unfocusedcount: value,
                offset: offset,
                speed: 0,
                Zcount: parent._var().child_Zstack,
                Multiplier: parent._var().child_Mstack
            });
        });
        $.each(parent._var().child_stackpile_right, function(offset, value) {
            parent._var().child_unfocus({
                count: parent._var().currentslide,
                unfocusedcount: value,
                offset: offset,
                speed: 0,
                Zcount: parent._var().child_Zstack,
                Multiplier: parent._var().child_Mstack
            });
        });
        parent._var().childtrack(parent._var().currentslide);
    };
    _thisObject_.prototype._putZorder = function(count) {
        var parent = this;
        var tempZcount = parent._var().toplevelZ;
        var currentObject = $(parent._var().childObjects[count]);
        currentObject.css("position", "absolute").css("z-count", tempZcount);
        $.each(parent._var().child_stackpile_left, function(count, value) {
            var currentObject = $(parent._var().childObjects[value]);
            currentObject.css("position", "absolute").css("z-count", tempZcount - (count + 1));
        });
        $.each(parent._var().child_stackpile_right, function(count, value) {
            var currentObject = $(parent._var().childObjects[value]);
            currentObject.css("position", "absolute").css("z-count", tempZcount - (count + 1));
        });
    };
    _thisObject_.prototype._getZorder = function(count) {
        var parent = this;
        var tempZcount = parent._var().toplevelZ;
        parent._var().child_Zstack = [];
        parent._var().child_Mstack = [];
        parent._var().child_Zstack[count] = tempZcount;
        parent._var().child_Mstack[count] = 0;
        var currentObject = $(parent._var().childObjects[count]);
        $.each(parent._var().child_stackpile_left, function(count, value) {
            parent._var().child_Zstack[value] = tempZcount - (count + 1);
            parent._var().child_Mstack[value] = count + 1;
        });
        $.each(parent._var().child_stackpile_right, function(count, value) {
            parent._var().child_Zstack[value] = tempZcount - (count + 1);
            parent._var().child_Mstack[value] = count + 1;
        });
    };
    _thisObject_.prototype._setstacks = function(count) {
        var parent = this;
        var splitpoint = _.countOf(parent._var().child_stack, count);
        var splitB = _.rest(parent._var().child_stack, [ splitpoint ]);
        var splitA = _.first(parent._var().child_stack, [ splitpoint ]);
        var offsetarray = splitB.concat(splitA);
        parent._var({
            child_offstack: offsetarray
        });
        var centercount = Math.floor(parent._var().child_stack.length / 2);
        var splitB = _.rest(parent._var().child_offstack, [ centercount + 1 ]);
        var splitA = _.first(parent._var().child_offstack, [ centercount + 1 ]);
        var offsetarray = splitB.concat(splitA);
        parent._var({
            child_centerstack: offsetarray
        });
        var child_stackpile_left = _.first(parent._var().child_stack, [ splitpoint ]).reverse();
        var child_stackpile_right = _.rest(parent._var().child_stack, [ splitpoint + 1 ]);
        parent._var({
            child_stackpile_left: child_stackpile_left,
            child_stackpile_right: child_stackpile_right
        });
    };
    _thisObject_.prototype.pagejump = function(count) {
        var difference = Math.abs(count - this._var().currentslide);
        if (difference > 1) {
            this._multijump(count);
        } else {
            this.jump(count);
        }
    };
    _thisObject_.prototype.jump = function(count) {
        this._var().childtrack(count);
        this._var().childchange(count);
    };
    _thisObject_.prototype.quickjump = function(count) {
        this._var().childtrack(count);
        this._var().childchange(count, {
            speed: 0
        });
    };
    _thisObject_.prototype._multijump = function(count) {
        this._var().childtrack(count);
        this._var()._multijump(count);
    };
    _thisObject_.prototype.next = function() {
        var parent = this;
        var childObjects = parent._var().childObjects;
        var objectcount = childObjects.length;
        var countCandidate = parent._var().currentslide;
        countCandidate += 1;
        if (countCandidate == objectcount) {
            this._var().loopslides ? countCandidate = 0 : countCandidate = objectcount - 1;
        }
        this._var().childtrack(countCandidate);
        this._var().childchange(countCandidate);
    };
    _thisObject_.prototype.prev = function() {
        var parent = this;
        var childObjects = parent._var().childObjects;
        var objectcount = childObjects.length;
        var countCandidate = parent._var().currentslide;
        countCandidate -= 1;
        if (countCandidate < 0) {
            this._var().loopslides ? countCandidate = objectcount - 1 : countCandidate = 0;
        }
        this._var().childtrack(countCandidate);
        this._var().childchange(countCandidate);
    };
    return _thisObject_;
});
