/*!Last Updated: 03.09.2014[17.40.15] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! swipeset MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.09.2014[17.40.15] by Jonathan Robles*/
define([ "jquery", "underscore", "modules/definitions/standardmodule", "hammer", "tweenmax", "scrollto" ], function($, _, parentObject, Hammer, TweenMax, Scrollto) {
    function _thisObject_(o) {
        var settings = {
            type: "swipeset",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv_X"),
            virtualchildren: false,
            virtualchild_overlap: 0,
            yscroll: false,
            Xscrolldetect: "release dragleft dragright swipeleft swiperight",
            Yscrolldetect: "release dragup dragdown swipeup swipedown",
            currentslide: 0,
            usetouchbinds: true,
            enable_touchbinds: true,
            snaponrelease: true,
            callback: function(o) {},
            childchange: function(count) {
                var this = this.parent;
                var destination = undefined;
                var animationvars = undefined;
                if (!this._var().yscroll) {
                    destination = this._var().childVars[count].XoffsetfromScroll;
                    animationvars = {
                        scrollTo: {
                            x: destination
                        },
                        ease: Power4.easeOut
                    };
                } else {
                    destination = this._var().childVars[count].YoffsetfromScroll;
                    animationvars = {
                        scrollTo: {
                            y: destination
                        },
                        ease: Power4.easeOut
                    };
                }
                TweenMax.to(this._var().target, .7, animationvars);
            },
            onscroll_callback: function(o) {
                console.log("moving :" + o.currentposition + " [ " + o.currentpercent + "% ]");
            },
            onscroll_start: function(o) {
                console.log("startmove :" + o.currentposition + " [ " + o.currentpercent + "% ]");
            },
            onscroll_debounce_callback: function(o) {
                console.log("endmove :" + o.currentposition + " [ " + o.currentpercent + "% ] @slide: " + this.currentslide);
            },
            onscroll_debounce_rate: 500,
            scrollsize: undefined,
            currentposition: undefined,
            currentpercent: undefined,
            targetXsize: undefined,
            targetYsize: undefined,
            targetwidth: undefined,
            targetheight: undefined,
            totalslides: undefined,
            childObjects: undefined,
            childVars: undefined,
            childVar_setter: function(count, value) {
                var self = this;
                this.count = count;
                this.childOBJ = value;
                this.target = $(value);
                this.Xsize = this.target.outerWidth();
                this.Ysize = this.target.innerHeight();
                this.Xdefault = Math.round(this.target.position().left);
                this.Ydefault = Math.round(this.target.position().top);
                this.XoffsetfromScroll = undefined;
                this.YoffsetfromScroll = undefined;
            },
            ObjHammer: undefined,
            isbeingdragged: false,
            startmove: true,
            busy: false
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onQuickjump: function(o) {
                    if (o.senderID == myID) {
                        parent.quickjump(o.data.count);
                    }
                },
                onJump: function(o) {
                    if (o.senderID == myID) {
                        parent.jump(o.data.count);
                    }
                },
                onPagejump: function(o) {
                    if (o.senderID == myID) {
                        parent.pagejump(o.data.count);
                    }
                },
                onNext: function(o) {
                    if (o.senderID == myID) {
                        parent.next();
                    }
                },
                onPrev: function(o) {
                    if (o.senderID == myID) {
                        parent.prev();
                    }
                }
            };
        }());
    };
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._var({
            busy: true
        });
        this._startlisteners();
        var self = this;
        this.refresh();
    };
    _thisObject_.prototype.refresh = function() {
        var self = this;
        this._var({
            busy: true
        });
        this.notify("Trace", "refresh");
        this.getpanelsizes();
        !this._var().virtualchildren ? this._childrentochildObjects() : this._virtualspacetochildObjects();
        this.quickjump(this._var().currentslide);
        var handlerdetect = !this._var().yscroll ? this._var().Xscrolldetect : this._var().Yscrolldetect;
        if (this._var().usetouchbinds) {
            this._var().ObjHammer = Hammer(this._var().target, {
                drag_lock_to_axis: true
            }).on(handlerdetect, this._bindHandler(this));
        }
        this._var().target.scroll(this._onscroll(this));
        this._var({
            busy: false
        });
        if (this._var().callback != undefined) {
            this._var().callback(this);
        }
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype._childrentochildObjects = function() {
        var self = this;
        this._var({
            childObjects: this._var().target.children()
        });
        var childObjects = this._var().childObjects;
        this._var({
            totalslides: childObjects.length,
            childVars: []
        });
        $.each(childObjects, function(count, value) {
            childVar_setter_local = this._var().childVar_setter;
            this._var().childVars.push(new childVar_setter_local(count, value));
        });
        $.each(this._var().childVars, function(count, value) {
            var zeroXdefault = this._var().childVars[0].Xdefault;
            var zeroYdefault = this._var().childVars[0].Ydefault;
            var marginOffsetX = value.target.css("marginLeft");
            var marginOffsetY = value.target.css("marginTop");
            value.XoffsetfromScroll = Math.round(value.Xdefault - zeroXdefault);
            value.YoffsetfromScroll = Math.round(value.Ydefault - zeroYdefault);
            if (marginOffsetX != undefined) {
                value.XoffsetfromScroll += parseInt(marginOffsetX, 10);
            }
            if (marginOffsetY != undefined) {
                value.YoffsetfromScroll += parseInt(marginOffsetY, 10);
            }
        });
        var totalscrollarea = !this._var().yscroll ? this._var().targetwidth : this._var().targetheight;
        var singlechildsize = !this._var().yscroll ? this._var().targetXsize : this._var().targetYsize;
        var scrollsize = totalscrollarea - singlechildsize;
        this._var({
            scrollsize: scrollsize
        });
    };
    _thisObject_.prototype._virtualspacetochildObjects = function() {
        var self = this;
        this._var({
            childObjects: this._var().target.children()
        });
        var totalscrollarea = !this._var().yscroll ? this._var().targetwidth : this._var().targetheight;
        var singlechildsize = !this._var().yscroll ? this._var().targetXsize : this._var().targetYsize;
        singlechildsize = singlechildsize - this._var().virtualchild_overlap;
        var scrollsize = totalscrollarea - singlechildsize;
        var tempStopArray = [];
        var childcount = Math.floor(scrollsize / singlechildsize);
        for (tempval = 0; tempval < childcount + 1; tempval++) {
            tempStopArray.push(tempval * singlechildsize);
        }
        tempStopArray.push(scrollsize);
        this._var({
            totalslides: tempStopArray.length,
            childVars: [],
            scrollsize: scrollsize
        });
        $.each(tempStopArray, function(count, value) {
            this._var().childVars.push({
                XoffsetfromScroll: value,
                YoffsetfromScroll: value
            });
        });
    };
    _thisObject_.prototype.getpanelsizes = function() {
        var self = this;
        var objreturn = {};
        objreturn.totalslides = this._var().totalslides;
        objreturn.targetwidth = this._var().targetwidth = this._var().target[0].scrollWidth;
        objreturn.targetheight = this._var().targetheight = this._var().target[0].scrollHeight;
        objreturn.targetXsize = this._var().targetXsize = this._var().target.innerWidth();
        objreturn.targetYsize = this._var().targetYsize = this._var().target.innerHeight();
        return objreturn;
    };
    _thisObject_.prototype.getclosestcount = function(position) {
        var self = this;
        var Offsets = _.map(this._var().childVars, function(idx) {
            return !this._var().yscroll ? Math.abs(idx.XoffsetfromScroll - position) : Math.abs(idx.YoffsetfromScroll - position);
        });
        var closestdiff = _.reduce(Offsets, function(memo, val) {
            return val < memo ? val : memo;
        }, 1e4);
        var counttoReturn = _.countOf(Offsets, closestdiff);
        this._var().currentslide = counttoReturn;
        return counttoReturn;
    };
    _thisObject_.prototype._returncurrentposition = function() {
        var self = this;
        return !this._var().yscroll ? this._var().target.scrollLeft() : this._var().target.scrollTop();
    };
    _thisObject_.prototype._gotoposition = function(getdestination) {
        var self = this;
        !this._var().yscroll ? this._var().target.scrollLeft(getdestination) : this._var().target.scrollTop(getdestination);
    };
    _thisObject_.prototype._gotopercent = function(getpercent) {
        var self = this;
        var scrollsize = this._var().scrollsize;
        var jumptoposition = scrollsize / (100 / getpercent);
        this._gotoposition(jumptoposition);
    };
    _thisObject_.prototype.thisie = function(o) {
        var self = this;
        var options = {
            yscroll: this._var().yscroll,
            quick: false
        };
        options = $.extend(options, o);
        if (!options.yscroll) {
            var thiscount = this.getclosestcount(this._returncurrentposition());
            if (!options.quick) {
                this.jump(thiscount);
            } else {
                this.quickjump(thiscount);
            }
        } else {
            var thiscount = this.getclosestcount(this._returncurrentposition());
            if (!options.quick) {
                this.jump(thiscount);
            } else {
                this.quickjump(thiscount);
            }
        }
    };
    _thisObject_.prototype.next = function(o) {
        var self = this;
        var options = {
            quick: false
        };
        options = $.extend(options, o);
        var thiscount = !this._var().yscroll ? this.getclosestcount(this._returncurrentposition()) : this.getclosestcount(this._var().target.scrollTop());
        var nextidx = thiscount == this._var().totalslides - 1 ? thiscount : thiscount + 1;
        !options.quick ? this.jump(nextidx) : this.quickjump(nextidx);
    };
    _thisObject_.prototype.prev = function(o) {
        var self = this;
        var options = {
            quick: false
        };
        options = $.extend(options, o);
        var thiscount = !this._var().yscroll ? this.getclosestcount(this._returncurrentposition()) : this.getclosestcount(this._var().target.scrollTop());
        var nextidx = thiscount == 0 ? 0 : thiscount - 1;
        !options.quick ? this.jump(nextidx) : this.quickjump(nextidx);
    };
    _thisObject_.prototype.jump = function(count) {
        var self = this;
        this._var().childchange(count, this);
    };
    _thisObject_.prototype.pagejump = function(count) {
        this.jump(count);
    };
    _thisObject_.prototype.quickjump = function(count) {
        var self = this;
        if (!this._var().yscroll) {
            var destination = this._var().childVars[count].XoffsetfromScroll;
        } else {
            var destination = this._var().childVars[count].YoffsetfromScroll;
        }
        this._gotoposition(destination);
    };
    _thisObject_.prototype._bindHandler = function(this) {
        var lastCase = undefined;
        var touchpoint = undefined;
        var targetposition = function() {
            touchpoint = this._returncurrentposition();
        };
        var casechecks = undefined;
        var directions = undefined;
        if (!this._var().yscroll) {
            casechecks = [ "dragright", "dragleft", "swipeleft", "swiperight", "release" ];
            directions = [ Hammer.DIRECTION_RIGHT, Hammer.DIRECTION_LEFT ];
        } else {
            casechecks = [ "dragdown", "dragup", "swipeup", "swipedown", "release" ];
            directions = [ Hammer.DIRECTION_DOWN, Hammer.DIRECTION_UP ];
        }
        var eventHandler = function(ev) {
            ev.gesture.preventDefault();
            if (ev.type != lastCase) {
                targetposition();
            }
            if (this._var().enable_touchbinds) {
                switch (ev.type) {
                  case casechecks[0]:
                  case casechecks[1]:
                    this._var().isbeingdragged = true;
                    var pane_offset = touchpoint;
                    var drag_offset = !this._var().yscroll ? ev.gesture.deltaX : ev.gesture.deltaY;
                    if (ev.gesture.direction == directions[0]) {
                        drag_offset = -Math.abs(drag_offset);
                    } else {
                        if (ev.gesture.direction == directions[1]) {
                            drag_offset = Math.abs(drag_offset);
                        }
                    }
                    var destination = pane_offset + drag_offset;
                    !this._var().yscroll ? this._var().target.scrollLeft(destination) : this._var().target.scrollTop(destination);
                    break;

                  case casechecks[2]:
                    this.next();
                    this._var().isbeingdragged = false;
                    ev.gesture.stopDetect();
                    break;

                  case casechecks[3]:
                    this.prev();
                    this._var().isbeingdragged = false;
                    ev.gesture.stopDetect();
                    break;

                  case casechecks[4]:
                    this._var().isbeingdragged = false;
                    targetposition();
                    if (this._var().snaponrelease) {
                        this.thisie();
                    } else {
                        var currentvars = {};
                        currentvars.scrollsize = this._var().scrollsize;
                        currentvars.currentposition = this._returncurrentposition();
                        currentvars.currentpercent = 100 / (currentvars.scrollsize / currentvars.currentposition);
                        this._debounce(this)(currentvars);
                    }
                    ev.gesture.stopDetect();
                    break;
                }
            }
            lastCase = ev.type;
        };
        return eventHandler;
    };
    _thisObject_.prototype._debounce = function(this) {
        var debouncer = _.debounce(function(currentvars) {
            var currentposition = this._returncurrentposition();
            var closecount = this.getclosestcount(currentposition);
            var destination = undefined;
            if (!this._var().yscroll) {
                destination = this._var().childVars[closecount].XoffsetfromScroll;
            } else {
                destination = this._var().childVars[closecount].YoffsetfromScroll;
            }
            var difference = Math.abs(currentposition - destination);
            if (difference > 1 && !this._var().isbeingdragged && this._var().snaponrelease) {
                this.thisie();
            } else {
                if (difference < 2 || !this._var().isbeingdragged && !this._var().snaponrelease) {
                    if (!this._var().startmove) {
                        this._var().onscroll_debounce_callback(currentvars);
                        this._var({
                            startmove: true
                        });
                    }
                }
            }
        }, this._var().onscroll_debounce_rate);
        return debouncer;
    };
    _thisObject_.prototype._onscroll = function(this) {
        var Offsets = _.map(this._var().childVars, function(idx) {
            return !this._var().yscroll ? Math.abs(idx.XoffsetfromScroll) : Math.abs(idx.YoffsetfromScroll);
        });
        var debouncefunc = this._debounce(this);
        var handler = function() {
            var currentvars = {};
            currentvars.scrollsize = this._var().scrollsize;
            currentvars.currentposition = this._returncurrentposition();
            currentvars.currentpercent = 100 / (currentvars.scrollsize / currentvars.currentposition);
            if (this._var().startmove) {
                this._var().onscroll_start(currentvars);
                this._var({
                    startmove: false
                });
            }
            this._var().onscroll_callback(currentvars);
            debouncefunc(currentvars);
        };
        return handler;
    };
    return _thisObject_;
});
