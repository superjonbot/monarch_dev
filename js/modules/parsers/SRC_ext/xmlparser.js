/**
 * xmlparser Module
 *
 * Date: 10/12/13[7:46 PM]
 *
 * NOTE: Its a json parsing module.  Duh.
 *
 */
define(['x2js','jquery','modules/parsers/jsonparser'],function (X2JS,$,parentModel) {  //replace the standardmodule with the module you are extending

    function _thisOBJ_( o ){
        var defaults={
            type:'xmlparser', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            //notifyscope - parent
            //init
            lasteditby:'',  //your name if you are making the last edit (NOTE:all edits in the code must have comments with your initials!)

            //target:undefined,
            file:'data/sitedata.json',
            usenocache:true,

            dataXML:undefined,

            data:undefined,
            callback:function(data){alert('received:)? '+data.questionnaire.status)},
            jsonpReturn:"window._global$.jsonpReturn('<%id>','JSONdata','global')",
            //interval:undefined,

            format:'json', //use text for xml, json for json, jsonp for you guessed it genius!
            //uselegacyinject:false,
            busy:true




            /*, TRY TO STICK TO THESE STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             data:undefined,
             callback:function(){},
             interval:undefined
             */
        };
        defaults= $.extend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };

    _thisOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE

    _thisOBJ_.prototype._startlisteners =function(){
        this.notify('Trace','_startlisteners');
        var parent=this;
        var myID=  this._id();
            _notify.add(this._id(), function() {

         return {

                 onJSONdata:function(o){
                     //alert('listener Invoked')
                     if(o.senderID==myID){
                         parent._var({data: o.data});
                         //alert(parent._var().format+' data stored! data: '+JSON.stringify(parent._var()));
                     }

                     if(parent._var().callback!=undefined){
                         parent._var().callback(o.data)
                     }

                 }

         }
         }());
        //console.log('listeners started')
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thisOBJ_.prototype.init =function(){
        /*
         if(<object>!=exist){
         <object>.create();
         }else{
         <object>.show(); //just show it and start listeners
         };
         * */
       // var JSONtoUSE=this._nocache(this._var().file);
        this.notify('Trace','init');
        this._startlisteners();//start this module's listeners
        this.refresh();





    };

    _thisOBJ_.prototype.refresh =function(){



        this.notify('Trace','refresh');
        this.getdata();
    };

    _thisOBJ_.prototype.kill =function(){
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace','kill');
         _notify.rem(this._id());  //kills this module's listeners
    };



    // UNIQUE MODULE METHODS
    //_thisOBJ_.prototype.putdata= function(data){

        //alert('!'+parent._var(data));
   // };

    _thisOBJ_.prototype.getdata= function(){
        var parent=this;
        this.notify('Trace','getdata format:'+parent._var().format+' file'+parent._var().file);
        var JSONtoUSE=parent._nocache(parent._var().file);
        var onData=function(jsondata){
            //console.log('got the json, notify!')
            parent.notify('JSONdata',jsondata);
        };

        //alert(this._var().format)
        if(this._var().format=='text'){

            $.ajax({

                xhrFields: {
                    withCredentials: false
                },
                crossDomain: false,

                type: "GET",
                url: parent._var().file,
                dataType: parent._var().format,
                statusCode: {404: function() {alert('please check xml!');}},



                success: function(xml) {
                    var gparse = new X2JS(); //XML object parsing
                    var jsondata = gparse.parseXmlString(xml);
                    jsondata = gparse.xml2json( jsondata );

                    onData(jsondata);

                    //defaults.busy = false;


                    //alert(parent._var().format+' data stored! data: '+JSON.stringify(jsondata));




                }
            });



        }else if(this._var().format=='jsonp'){


          //  if(!this._var().uselegacyinject){

                if(this._var().jsonpReturn!=undefined){

                    JSONtoUSE += "&jsonp="+this._var().jsonpReturn;
                    JSONtoUSE = JSONtoUSE.replace('<%id>',this._id()); //tack on ID to jsonP return
                }

                JSONtoUSE += "&=?";
                //console.log('trying to get '+this._var().format+' file:'+JSONtoUSE);


                $.ajaxSetup({
                    type: "POST",
                    data: {},
                    //dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
                });
                $.getJSON(JSONtoUSE, {format: 'jsonp'}).error(function(){
                   // console.log("JSONP READ COMPLETE! (note, object data/busy are not used for jsonp files!)");
                   // console.log('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=');
                    })//,function(jsondata) {onjsondata(jsondata);}).error(function() { trace("ERROR!");trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=') });
                //alert(JSONtoUSE)
/*
            }  else {
                var ref_script = document.createElement("script");
                ref_script.setAttribute("src",JSONtoUSE);
                ref_script.setAttribute("type","text/javascript");
                document.body.appendChild(ref_script);




            }
*/



        }
        else {



            $.getJSON(JSONtoUSE, {format: 'json'},function(jsondata){
            onData(jsondata)
            }).error(function() {
                    //error messages here
                    //trace("ERROR!");
                    //trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
                });
        }
    };

/*
    _thisOBJ_.prototype.onjsondata = function(){
        alert('gotsit!')
    };

    _thisOBJ_.prototype.sayhi = function(){
        this.notify('Trace',('hi there, from object#'+this._id()+' [ '+this._var().type+' by '+this._var().author+' ] '));
    }
    //-------------------------------------------------------------------------//
  */
    return( _thisOBJ_ );

});



