/**
 * dataparser Module
 *
 * Date: 10/12/13[7:46 PM]
 *
 * NOTE: Its a json/jsonp/xml parsing module.
 *
 * USAGE:
 * var yourobject = new Module({
 *      file:'data/sitedata.json',
 *      format:'json',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * OR
 *
 * var yourobject = new Module({
 *      file:'data/sitedata.jsonp',
 *      format:'jsonp',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * OR
 *
 * var yourobject = new Module({
 *      file:'data/sitedata.xml',
 *      format:'text',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * yourobject.init();
 */
define(['x2js','jquery','modules/definitions/standardmodule'],function (X2JS,$,parentModel) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_( o ){
        var defaults={
            type:'dataparser', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            lasteditby:'',  //your name if you are making the last edit (NOTE:all edits in the code must have comments with your initials!)

            file:'data/sitedata.json',
            usenocache:true,

            dataXML:undefined,

            data:undefined,  //placeholder for final data
            callback:undefined,//function(data){alert('received? '+data.status)},
            jsonpReturn:"window._global$.jsonpReturn('<%id>','JSONdata','global')",
            //interval:undefined,

            format:'json', //use text for xml, json for json, jsonp for jsonp
            //uselegacyinject:false,
            busy:false

        };
        defaults= $.extend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners =function(){
        this.notify('Trace','_startlisteners');
        var parent=this;
        var myID=  this._id();
            _notify.add(this._id(), function() {

         return {

                 onJSONdata:function(o){
                     if(o.senderID==myID){
                         parent._var({data: o.data,busy:false});
                         if(parent._var().callback!=undefined){
                             parent._var().callback(o.data)
                         }
                     }
                 }
         }
         }());

    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init =function(){

        this.notify('Trace','init');
        this._startlisteners();//start this module's listeners
        this.refresh();

    };

    _thizOBJ_.prototype.refresh =function(){
        this.notify('Trace','refresh');
        this.getdata();
    };

    _thizOBJ_.prototype.kill =function(){
        this.notify('Trace','kill');
         _notify.rem(this._id());  //kills this module's listeners
    };



    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.getdata= function(){
        var parent=this;
        parent._var({busy:true});
        this.notify('Trace','getdata format:'+parent._var().format+' file'+parent._var().file);
        var JSONtoUSE=parent._nocache(parent._var().file);
        var onData=function(jsondata){
            parent.notify('JSONdata',jsondata);
        };


        if(this._var().format=='text'){

            $.ajax({

                xhrFields: {
                    withCredentials: false
                },
                crossDomain: false,

                type: "GET",
                url: parent._var().file,
                dataType: parent._var().format,
                statusCode: {404: function() {alert('please check xml!');}},



                success: function(xml) {

                    parent._var({dataXML:xml});
                    var gparse = new X2JS(); //XML object parsing
                    var jsondata = gparse.parseXmlString(xml);
                    jsondata = gparse.xml2json( jsondata );

                    onData(jsondata);

                }
            });


//modules/view/viewmodels
        }
        else if(this._var().format=='jsonp'){


          //  if(!this._var().uselegacyinject){

                if(this._var().jsonpReturn!=undefined){

                    JSONtoUSE += "&jsonp="+this._var().jsonpReturn;
                    JSONtoUSE = JSONtoUSE.replace('<%id>',this._id()); //tack on ID to jsonP return
                }

                JSONtoUSE += "&=?";
                //console.log('trying to get '+this._var().format+' file:'+JSONtoUSE);


                $.ajaxSetup({
                    type: "POST",
                    data: {},
                    //dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
                });
                $.getJSON(JSONtoUSE, {format: 'jsonp'}).error(function(){
                   // console.log("JSONP READ COMPLETE! (note, object data/busy are not used for jsonp files!)");
                   // console.log('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=');
                    })//,function(jsondata) {onjsondata(jsondata);}).error(function() { trace("ERROR!");trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=') });



        }
        else {



            $.getJSON(JSONtoUSE, {format: 'json'},function(jsondata){
            onData(jsondata)
            }).error(function() {
                    //error messages here
                    //trace("ERROR!");
                    //trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
                });
        }
    };



    return( _thizOBJ_ );

});



