/*!Last Updated: 03.31.2014[17.04.59] by Jonathan Robles*/
/*!*/
/*!********************************************************************************************************/
/*!                                                                                                       */
/*! jsonparser module (c) 2014 Jonathan Robles */
/*! http://jonathanrobles.net                                                                             */
/*!                                                                                                       */
/*! This software may be freely distributed under the                                                     */
/*! Creative Commons Attribution-ShareAlike (CC BY-SA) License                                            */
/*! This license lets others remix, tweak, and build upon your work even for commercial purposes,         */
/*! as long as they credit you and license their new creations under the identical terms. All new         */
/*! works based on yours will carry the same license, so any derivatives will also allow commercial use.  */
/*! link: https://creativecommons.org/examples#by-sa                                                      */
/*!                                                                                                       */
/*!********************************************************************************************************/
/*!CLX*/
/*!*/
/*!*/
/*!Last Updated: 03.31.2014[17.04.59] by Jonathan Robles*/
define(["jquery","modules/definitions/standardmodule"],function($,parentModel){function _thisOBJ_(o){var defaults={type:"jsonparser",author:"Jonathan Robles",lasteditby:"",file:"data/sitedata.json",usenocache:true,data:undefined,callback:function(data){alert("received? "+data.status);},jsonpReturn:"window._global$.jsonpReturn('<%id>','JSONdata','global')",format:"json",busy:true};defaults=$.extend(defaults,o);parentModel.call(this,defaults);return(this);}_thisOBJ_.prototype=Object.create(parentModel.prototype);_thisOBJ_.prototype._startlisteners=function(){this.notify("Trace","_startlisteners");var parent=this;var myID=this._id();_notify.add(this._id(),function(){return{onJSONdata:function(o){if(o.senderID==myID){parent._var({data:o.data});}if(parent._var().callback!=undefined){parent._var().callback(o.data);}}};}());};_thisOBJ_.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh();};_thisOBJ_.prototype.refresh=function(){this.notify("Trace","refresh");this.getdata();};_thisOBJ_.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id());};_thisOBJ_.prototype.getdata=function(){var parent=this;this.notify("Trace","getdata format:"+parent._var().format+" file"+parent._var().file);var JSONtoUSE=parent._nocache(parent._var().file);var onData=function(jsondata){parent.notify("JSONdata",jsondata);};if(this._var().format=="jsonp"){if(this._var().jsonpReturn!=undefined){JSONtoUSE+="&jsonp="+this._var().jsonpReturn;JSONtoUSE=JSONtoUSE.replace("<%id>",this._id());}JSONtoUSE+="&=?";$.ajaxSetup({type:"POST",data:{},xhrFields:{withCredentials:true},crossDomain:true});$.getJSON(JSONtoUSE,{format:"jsonp"}).error(function(){});}else{$.getJSON(JSONtoUSE,{format:"json"},function(jsondata){onData(jsondata);}).error(function(){});}};return(_thisOBJ_);});