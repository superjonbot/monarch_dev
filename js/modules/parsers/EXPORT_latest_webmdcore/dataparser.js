/*!Last Updated: 03.31.2014[17.05.03] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! dataparser MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLS*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.31.2014[17.05.03] by Jonathan Robles*/
define(["x2js","jquery","modules/definitions/standardmodule"],function(d,c,b){function a(f){var e={type:"dataparser",author:"Jonathan Robles",lasteditby:"",file:"data/sitedata.json",usenocache:true,dataXML:undefined,data:undefined,callback:undefined,jsonpReturn:"window._global$.jsonpReturn('<%id>','JSONdata','global')",format:"json",busy:false};e=c.extend(e,f);b.call(this,e);return(this)}a.prototype=Object.create(b.prototype);a.prototype._startlisteners=function(){this.notify("Trace","_startlisteners");var f=this;var e=this._id();_notify.add(this._id(),function(){return{onJSONdata:function(g){if(g.senderID==e){f._var({data:g.data,busy:false});if(f._var().callback!=undefined){f._var().callback(g.data)}}}}}())};a.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh()};a.prototype.refresh=function(){this.notify("Trace","refresh");this.getdata()};a.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id())};a.prototype.getdata=function(){var f=this;f._var({busy:true});this.notify("Trace","getdata format:"+f._var().format+" file"+f._var().file);var g=f._nocache(f._var().file);var e=function(h){f.notify("JSONdata",h)};if(this._var().format=="text"){c.ajax({xhrFields:{withCredentials:false},crossDomain:false,type:"GET",url:f._var().file,dataType:f._var().format,statusCode:{404:function(){alert("please check xml!")}},success:function(h){f._var({dataXML:h});var i=new d();var j=i.parseXmlString(h);j=i.xml2json(j);e(j)}})}else{if(this._var().format=="jsonp"){if(this._var().jsonpReturn!=undefined){g+="&jsonp="+this._var().jsonpReturn;g=g.replace("<%id>",this._id())}g+="&=?";c.ajaxSetup({type:"POST",data:{},xhrFields:{withCredentials:true},crossDomain:true});c.getJSON(g,{format:"jsonp"}).error(function(){})}else{c.getJSON(g,{format:"json"},function(h){e(h)}).error(function(){})}}};return(a)});