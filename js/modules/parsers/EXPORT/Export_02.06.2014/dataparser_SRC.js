/*!Last Updated: 02.06.2014[16.57.20] by Jonathan Robles*/



/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
//  dataparser MODULE                                  
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/

 /*
 * NOTE: Its a json/jsonp/xml parsing module.
 *
 * USAGE:
 * var yourobject = new Module({
 *      file:'data/sitedata.json',
 *      format:'json',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * OR
 *
 * var yourobject = new Module({
 *      file:'data/sitedata.jsonp',
 *      format:'jsonp',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * OR
 *
 * var yourobject = new Module({
 *      file:'data/sitedata.xml',
 *      format:'text',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * yourobject.init();
 */
/*CL1*/


/*!Last Updated: 02.06.2014[16.57.20] by Jonathan Robles*/
define([ "x2js", "jquery", "modules/definitions/standardmodule" ], function(X2JS, $, parentModel) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "dataparser",
            author: "Jonathan Robles",
            lasteditby: "",
            file: "data/sitedata.json",
            usenocache: true,
            dataXML: undefined,
            data: undefined,
            callback: undefined,
            jsonpReturn: "window._global$.jsonpReturn('<%id>','JSONdata','global')",
            format: "json",
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onJSONdata: function(o) {
                    if (o.senderID == myID) {
                        parent._var({
                            data: o.data,
                            busy: false
                        });
                        if (parent._var().callback != undefined) parent._var().callback(o.data);
                    }
                }
            };
        }());
    };
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this.getdata();
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.getdata = function() {
        var parent = this;
        parent._var({
            busy: true
        });
        this.notify("Trace", "getdata format:" + parent._var().format + " file" + parent._var().file);
        var JSONtoUSE = parent._nocache(parent._var().file);
        var onData = function(jsondata) {
            parent.notify("JSONdata", jsondata);
        };
        if (this._var().format == "text") $.ajax({
            xhrFields: {
                withCredentials: false
            },
            crossDomain: false,
            type: "GET",
            url: parent._var().file,
            dataType: parent._var().format,
            statusCode: {
                404: function() {
                    alert("please check xml!");
                }
            },
            success: function(xml) {
                parent._var({
                    dataXML: xml
                });
                var gparse = new X2JS();
                var jsondata = gparse.parseXmlString(xml);
                jsondata = gparse.xml2json(jsondata);
                onData(jsondata);
            }
        }); else if (this._var().format == "jsonp") {
            if (this._var().jsonpReturn != undefined) {
                JSONtoUSE += "&jsonp=" + this._var().jsonpReturn;
                JSONtoUSE = JSONtoUSE.replace("<%id>", this._id());
            }
            JSONtoUSE += "&=?";
            $.ajaxSetup({
                type: "POST",
                data: {},
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true
            });
            $.getJSON(JSONtoUSE, {
                format: "jsonp"
            }).error(function() {});
        } else $.getJSON(JSONtoUSE, {
            format: "json"
        }, function(jsondata) {
            onData(jsondata);
        }).error(function() {});
    };
    return _thizOBJ_;
});