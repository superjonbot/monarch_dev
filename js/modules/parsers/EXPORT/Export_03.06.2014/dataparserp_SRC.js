/*!Last Updated: 03.06.2014[20.11.58] by Jonathan Robles*/
/*!*/
/*!********************************************************************************************************/
/*!                                                                                                       */
/*! dataparser module (c) 2014 Jonathan Robles */
/*! http://jonathanrobles.net                                                                             */
/*!                                                                                                       */
/*! This software may be freely distributed under the                                                     */
/*! Creative Commons Attribution-ShareAlike (CC BY-SA) License                                            */
/*! This license lets others remix, tweak, and build upon your work even for commercial purposes,         */
/*! as long as they credit you and license their new creations under the identical terms. All new         */
/*! works based on yours will carry the same license, so any derivatives will also allow commercial use.  */
/*! link: https://creativecommons.org/examples#by-sa                                                      */
/*!                                                                                                       */
/*!********************************************************************************************************/
/*!CL1b*/
/*!*/
/*!*/
/*!Last Updated: 03.06.2014[20.11.58] by Jonathan Robles*/
define([ "x2js", "jquery", "modules/definitions/standardmodule" ], function(X2JS, $, parentModel) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "dataparser",
            author: "Jonathan Robles",
            lasteditby: "",
            file: "data/sitedata.json",
            usenocache: true,
            dataXML: undefined,
            data: undefined,
            callback: undefined,
            jsonpReturn: "window._global$.jsonpReturn('<%id>','JSONdata','global')",
            format: "json",
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onJSONdata: function(o) {
                    if (o.senderID == myID) {
                        parent._var({
                            data: o.data,
                            busy: false
                        });
                        if (parent._var().callback != undefined) {
                            parent._var().callback(o.data);
                        }
                    }
                }
            };
        }());
    };
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this.getdata();
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.getdata = function() {
        var parent = this;
        parent._var({
            busy: true
        });
        this.notify("Trace", "getdata format:" + parent._var().format + " file" + parent._var().file);
        var JSONtoUSE = parent._nocache(parent._var().file);
        var onData = function(jsondata) {
            parent.notify("JSONdata", jsondata);
        };
        if (this._var().format == "text") {
            $.ajax({
                xhrFields: {
                    withCredentials: false
                },
                crossDomain: false,
                type: "GET",
                url: parent._var().file,
                dataType: parent._var().format,
                statusCode: {
                    404: function() {
                        alert("please check xml!");
                    }
                },
                success: function(xml) {
                    parent._var({
                        dataXML: xml
                    });
                    var gparse = new X2JS();
                    var jsondata = gparse.parseXmlString(xml);
                    jsondata = gparse.xml2json(jsondata);
                    onData(jsondata);
                }
            });
        } else {
            if (this._var().format == "jsonp") {
                if (this._var().jsonpReturn != undefined) {
                    JSONtoUSE += "&jsonp=" + this._var().jsonpReturn;
                    JSONtoUSE = JSONtoUSE.replace("<%id>", this._id());
                }
                JSONtoUSE += "&=?";
                $.ajaxSetup({
                    type: "POST",
                    data: {},
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
                });
                $.getJSON(JSONtoUSE, {
                    format: "jsonp"
                }).error(function() {});
            } else {
                $.getJSON(JSONtoUSE, {
                    format: "json"
                }, function(jsondata) {
                    onData(jsondata);
                }).error(function() {});
            }
        }
    };
    return _thizOBJ_;
});