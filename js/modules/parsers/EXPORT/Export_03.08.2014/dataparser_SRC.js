/*!Last Updated: 03.08.2014[02.02.42] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! dataparser MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.08.2014[02.02.42] by Jonathan Robles*/
define([ "x2js", "jquery", "modules/definitions/standardmodule" ], function(X2JS, $, parentObject) {
    function _thisObject_(o) {
        var settings = {
            type: "dataparser",
            author: "Jonathan Robles",
            lasteditby: "",
            file: "data/sitedata.json",
            usenocache: true,
            dataXML: undefined,
            data: undefined,
            callback: undefined,
            jsonpReturn: "window._global$.jsonpReturn('<%id>','JSONdata','global')",
            format: "json",
            busy: false
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onJSONdata: function(o) {
                    if (o.senderID == myID) {
                        parent._var({
                            data: o.data,
                            busy: false
                        });
                        if (parent._var().callback != undefined) {
                            parent._var().callback(o.data);
                        }
                    }
                }
            };
        }());
    };
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thisObject_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this.getdata();
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype.getdata = function() {
        var parent = this;
        parent._var({
            busy: true
        });
        this.notify("Trace", "getdata format:" + parent._var().format + " file" + parent._var().file);
        var JSONtoUSE = parent._nocache(parent._var().file);
        var onData = function(jsondata) {
            parent.notify("JSONdata", jsondata);
        };
        if (this._var().format == "text") {
            $.ajax({
                xhrFields: {
                    withCredentials: false
                },
                crossDomain: false,
                type: "GET",
                url: parent._var().file,
                dataType: parent._var().format,
                statusCode: {
                    404: function() {
                        alert("please check xml!");
                    }
                },
                success: function(xml) {
                    parent._var({
                        dataXML: xml
                    });
                    var gparse = new X2JS();
                    var jsondata = gparse.parseXmlString(xml);
                    jsondata = gparse.xml2json(jsondata);
                    onData(jsondata);
                }
            });
        } else {
            if (this._var().format == "jsonp") {
                if (this._var().jsonpReturn != undefined) {
                    JSONtoUSE += "&jsonp=" + this._var().jsonpReturn;
                    JSONtoUSE = JSONtoUSE.replace("<%id>", this._id());
                }
                JSONtoUSE += "&=?";
                $.ajaxSetup({
                    type: "POST",
                    data: {},
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
                });
                $.getJSON(JSONtoUSE, {
                    format: "jsonp"
                }).error(function() {});
            } else {
                $.getJSON(JSONtoUSE, {
                    format: "json"
                }, function(jsondata) {
                    onData(jsondata);
                }).error(function() {});
            }
        }
    };
    return _thisObject_;
});
