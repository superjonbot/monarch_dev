/*!Last Updated: 03.08.2014[02.02.45] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! jsonparser MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.08.2014[02.02.45] by Jonathan Robles*/
define(["jquery","modules/definitions/standardmodule"],function(c,a){function b(e){var d={type:"jsonparser",author:"Jonathan Robles",lasteditby:"",file:"data/sitedata.json",usenocache:true,data:undefined,callback:function(f){alert("received? "+f.status)},jsonpReturn:"window._global$.jsonpReturn('<%id>','JSONdata','global')",format:"json",busy:true};d=c.extend(d,e);a.call(this,d);return(this)}b.prototype=Object.create(a.prototype);b.prototype._startlisteners=function(){this.notify("Trace","_startlisteners");var e=this;var d=this._id();_notify.add(this._id(),function(){return{onJSONdata:function(f){if(f.senderID==d){e._var({data:f.data})}if(e._var().callback!=undefined){e._var().callback(f.data)}}}}())};b.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh()};b.prototype.refresh=function(){this.notify("Trace","refresh");this.getdata()};b.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id())};b.prototype.getdata=function(){var e=this;this.notify("Trace","getdata format:"+e._var().format+" file"+e._var().file);var f=e._nocache(e._var().file);var d=function(g){e.notify("JSONdata",g)};if(this._var().format=="jsonp"){if(this._var().jsonpReturn!=undefined){f+="&jsonp="+this._var().jsonpReturn;f=f.replace("<%id>",this._id())}f+="&=?";c.ajaxSetup({type:"POST",data:{},xhrFields:{withCredentials:true},crossDomain:true});c.getJSON(f,{format:"jsonp"}).error(function(){})}else{c.getJSON(f,{format:"json"},function(g){d(g)}).error(function(){})}};return(b)});