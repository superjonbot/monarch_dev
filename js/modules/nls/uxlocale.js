//root UX language
define({
    "root": {
    	"close":"close",
    	"open":"open",
        "play": "play",
        "pause": "pause",
        "stop": "stop",
		"rewind": "rewind",
		"fastforward": "fastforward",
		"skip": "skip"
    },

    //available translations
    "fr": true

});