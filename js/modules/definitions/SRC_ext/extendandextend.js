/**
 * Extended Module Definition
 *
 * Date: <inZertDATe>
 *
 * NOTE: This is a developer's template, DO NOT EDIT THIS ACTUAL FILE!!!
 * you should use a copy of this to base your new Module, take this note out and put your module's documentation here!
 *
 */
define(['jquery','modules/definitions/extendedmodule'],function ($,parentModel) {  //replace the standardmodule with the module you are extending

    function _thi$OBJ_( o ){
        var defaults={
            type:'extendedmodule', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            lasteditby:''  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)
            /*, TRY TO STICK TO THESE COMMON STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             file:'somedir/somefile.ext',
             usenocache:true,
             dataIN:{},
             data:undefined,
             callback:function(){},
             interval:undefined,
             busy:false
             */
        };
        defaults= $.extend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };

    _thi$OBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thi$OBJ_.prototype._startlisteners =function(){
        /*    _notify.add(this._id(), function() {

         return {
         //   onTrace:function(){alert('Jon has got sexy code')},

         onEvent:function(o){
         //o.senderID
         //o.sendertype
         //o.notifyscope
         //o.data
         },
         onEvent:function(o){ //scoped event
         if(o.notifyscope==this._var().notifyscope){
         //o.senderID
         //o.sendertype
         //o.notifyscope
         //o.data
         }
         }

         }
         }());*/
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thi$OBJ_.prototype.init =function(){
        /*
         if(<object>!=exist){
         <object>.create();
         }else{
         <object>.show(); //just show it and start listeners
         };
         * */
        this._startlisteners();//start this module's listeners
    };

    _thi$OBJ_.prototype.refresh =function(){

    };

    _thi$OBJ_.prototype.kill =function(){
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        _notify.rem(this._id());  //kills this module's listeners
    };
    // UNIQUE MODULE METHODS
    _thi$OBJ_.prototype.sayhi = function(){
        this.notify('Trace',('hi there, from object#'+this._id()+' [ '+this._var().type+' by '+this._var().author+' ] '));
    }
    //-------------------------------------------------------------------------//
    return( _thi$OBJ_ );

});



