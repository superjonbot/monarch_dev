/*!Last Updated: 03.09.2014[17.39.31] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! extendedmodule MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.09.2014[17.39.31] by Jonathan Robles*/
define(["jquery","modules/definitions/standardmodule"],function(c,b){function a(e){var d={type:"extendedmodule",author:"Jonathan Robles",lasteditby:""};d=c.extend(d,e);b.call(this,d);return(this)}a.prototype=Object.create(b.prototype);a.prototype._startlisteners=function(){};a.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh()};a.prototype.refresh=function(){this.notify("Trace","refresh")};a.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id())};a.prototype.sayhi=function(){this.notify("Trace",("hi there, from object#"+this._id()+" [ "+this._var().type+" by "+this._var().author+" ] "))};return(a)});