/**
 * Extended Module Definition
 * Author: Jonathan Robles
 * Date: 9/30/13
 *
 * Documentation:
 *
 * Usage:
 *
 */
define(['jquery','modules/definitions/standardmodule'],function ($,parentOBJ) {

function _thi$OBJ_(){
    parentOBJ.call(this);

};

    //INITIALIZE
    _thi$OBJ_.prototype = new parentOBJ()
    _thi$OBJ_.prototype.constructor = _thi$OBJ_;

    //CONFIGURABLE VARS
    _thi$OBJ_.prototype.obj= $.extend(_thi$OBJ_.prototype.obj,{

        type:'Extended Module Definition',
        author:'your name',

        target:undefined,
        callback:undefined,
        interval:undefined

    });


    //FUNCTIONS
    _thi$OBJ_.prototype.init = function (){
        //setup all External Listeners
  /*      _notify.add(this, function() {

             return {
                onTrace: function(string) {

                    alert('bye!');
                    //_thi$OBJ_.prototype.sayhi(string);
                }
            }
        }());*/
    };

    _thi$OBJ_.prototype.sayhi=function (data){alert('hi'+String(data))};

    _thi$OBJ_.prototype.refresh = function (){};
    _thi$OBJ_.prototype.kill = function (){};
    _thi$OBJ_.prototype.somefunction = function (){
        console.log(this.obj.author+' is a cool dood')
        //this.obj.sandbox.notify={type:'isItrue',data:'truthy'};
    }


    _thi$OBJ_.prototype.init();

return _thi$OBJ_;

});


//_notify.broadcast('Trace',['your string']);
