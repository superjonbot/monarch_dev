/*!Last Updated: 03.06.2014[20.07.53] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! standardmodule MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CL1b*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.06.2014[20.07.53] by Jonathan Robles*/
define([ "jquery" ], function($) {
    var _instanceID = 0;
    var _nextInstanceID = function() {
        return ++_instanceID;
    };
    var defaults = [];
    function _thizOBJ_(o) {
        this._instanceID = _nextInstanceID();
        var _parent = this;
        defaults[this._instanceID] = {
            type: "Standard Module Definition",
            author: "Jonathan Robles",
            notifyscope: "global",
            target: undefined,
            file: undefined,
            usenocache: true,
            data: undefined,
            callback: undefined,
            interval: undefined,
            init: function() {
                _notify.broadcast("Initialize", [ {
                    senderID: _parent._instanceID,
                    sendertype: this.type,
                    notifyscope: this.notifyscope,
                    data: {
                        author: this.author
                    }
                } ]);
            },
            parent: this
        };
        defaults[this._instanceID] = $.extend(defaults[this._instanceID], o);
        defaults[this._instanceID].init();
        return this;
    }
    _thizOBJ_.prototype = {
        _init: function() {
            this._var().init();
        },
        _showdata: function() {
            return JSON.stringify(defaults[this._instanceID]);
        },
        _id: function() {
            return this._instanceID;
        },
        _var: function(o) {
            if (o != undefined) {
                defaults[this._instanceID] = $.extend(defaults[this._instanceID], o);
            }
            return defaults[this._instanceID];
        },
        _nocache: function(string) {
            if (typeof string === "string") {
                if (this._var().usenocache) {
                    var addOn = "?";
                    if (string.indexOf("?") != -1) {
                        addOn = "&";
                    }
                    return string + addOn + "nocache=" + Math.floor(Math.random() * 9999);
                } else {
                    return string;
                }
            } else {
                this.notify("Alert", "_nocache needs a string!");
                return;
            }
        },
        notify: function(type, data) {
            _notify.broadcast(type, [ {
                senderID: this._id(),
                sendertype: this._var().type,
                notifyscope: this._var().notifyscope,
                data: data
            } ]);
        },
        parent: this
    };
    return _thizOBJ_;
});