/*!Last Updated: 03.09.2014[17.42.26] by Jonathan Robles*/
/*!*/
/*!********************************************************************************************************/
/*!                                                                                                       */
/*! extendedmodule module (c) 2014 Jonathan Robles */
/*! http://jonathanrobles.net                                                                             */
/*!                                                                                                       */
/*! This software may be freely distributed under the                                                     */
/*! Creative Commons Attribution-ShareAlike (CC BY-SA) License                                            */
/*! This license lets others remix, tweak, and build upon your work even for commercial purposes,         */
/*! as long as they credit you and license their new creations under the identical terms. All new         */
/*! works based on yours will carry the same license, so any derivatives will also allow commercial use.  */
/*! link: https://creativecommons.org/examples#by-sa                                                      */
/*!                                                                                                       */
/*!********************************************************************************************************/
/*!CLS*/
/*!*/
/*!*/
/*!Last Updated: 03.09.2014[17.42.26] by Jonathan Robles*/
define(["jquery","modules/definitions/standardmodule"],function($,parentModel){function _thizOBJ_(o){var defaults={type:"extendedmodule",author:"Jonathan Robles",lasteditby:""};defaults=$.extend(defaults,o);parentModel.call(this,defaults);return(this);}_thizOBJ_.prototype=Object.create(parentModel.prototype);_thizOBJ_.prototype._startlisteners=function(){};_thizOBJ_.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh();};_thizOBJ_.prototype.refresh=function(){this.notify("Trace","refresh");};_thizOBJ_.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id());};_thizOBJ_.prototype.sayhi=function(){this.notify("Trace",("hi there, from object#"+this._id()+" [ "+this._var().type+" by "+this._var().author+" ] "));};return(_thizOBJ_);});