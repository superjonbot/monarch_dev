/*!Last Updated: 03.09.2014[17.42.34] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! standardmodule MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.09.2014[17.42.34] by Jonathan Robles*/
define(["jquery"],function(d){var b=0;var a=function(){return(++b)};var e=[];function c(g){this._instanceID=a();var f=this;e[this._instanceID]={type:"Standard Module Definition",author:"Jonathan Robles",notifyscope:"global",target:undefined,file:undefined,usenocache:true,data:undefined,callback:undefined,interval:undefined,init:function(){_notify.broadcast("Initialize",[{senderID:f._instanceID,sendertype:this.type,notifyscope:this.notifyscope,data:{author:this.author}}])},parent:this};e[this._instanceID]=d.extend(e[this._instanceID],g);e[this._instanceID].init();return(this)}c.prototype={_init:function(){this._var().init()},_showdata:function(){return JSON.stringify(e[this._instanceID])},_id:function(){return(this._instanceID)},_var:function(f){if(f!=undefined){e[this._instanceID]=d.extend(e[this._instanceID],f)}return e[this._instanceID]},_nocache:function(f){if(typeof f==="string"){if(this._var().usenocache){var g="?";if(f.indexOf("?")!=-1){g="&"}return f+g+"nocache="+(Math.floor(Math.random()*9999))}else{return f}}else{this.notify("Alert","_nocache needs a string!");return}},notify:function(f,g){_notify.broadcast(f,[{senderID:this._id(),sendertype:this._var().type,notifyscope:this._var().notifyscope,data:g}])},parent:this};return(c)});