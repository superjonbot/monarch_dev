/*!Last Updated: 02.06.2014[16.53.51] by Jonathan Robles*/



/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
//  THE EXTENDED MODULE (your module's template)                               
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/

 // NOTE: This is a developer's template, DO NOT EDIT THIS ACTUAL FILE!!!
 //
 // USAGE: you should use a copy of this to base your new Module on, take this note out and put your module's documentation here
 //
/*CL1*/


/*!Last Updated: 02.06.2014[16.53.51] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule" ], function($, parentModel) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "extendedmodule",
            author: "Jonathan Robles",
            lasteditby: ""
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {};
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.sayhi = function() {
        this.notify("Trace", "hi there, from object#" + this._id() + " [ " + this._var().type + " by " + this._var().author + " ] ");
    };
    return _thizOBJ_;
});