/**
 * Extended Module Definition
 *
 * Date: <inZertDATe>
 *
 * NOTE: This is a developer's template, DO NOT EDIT THIS ACTUAL FILE!!!
 * you should use a copy of this to base your new Module, take this note out and put your module's documentation here!
 *
 */
define(['modules/definitions/standardmodule'],function (parentModel) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_( o ){
        var defaults={
            type:'extendedmodule', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            lasteditby:''  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)
            /*, TRY TO STICK TO THESE COMMON STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             file:'somedir/somefile.ext',
             usenocache:true,
             dataIN:{},
             data:undefined,
             callback:function(){},
             interval:undefined,
             busy:false
             */
        };
        defaults= this.deepExtend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners =function(){




     /*

      this.notify('Trace','_startlisteners');
      var parent=this;
      var myID=  this._id();
      _notify.add(this._id(), function() {

      return {

      onJSONdata:function(o){
      if(o.senderID==myID){
      parent._var({data: o.data,busy:false});
      if(parent._var().callback!=undefined){
      parent._var().callback(o.data)
      }
      }
      }
      }
      }());

     */
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init =function(){
    /*
    if(<object>!=exist){
     <object>.create();
    }else{
     <object>.show(); //just show it and start listeners
    };
    * */
        this.notify('Trace','init');
        this._startlisteners();//start this module's listeners
        this.refresh();

    };

    _thizOBJ_.prototype.refresh =function(){
        this.notify('Trace','refresh');
    };

    _thizOBJ_.prototype.kill =function(){
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace','kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.sayhi = function(){
        this.notify('Trace',('hi there, from object#'+this._id()+' [ '+this._var().type+' by '+this._var().author+' ] '));
    }



    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});



