/**
 * Created with JetBrains PhpStorm.
 * User: superjonbot
 * Date: 9/30/13
 * Time: 3:25 PM
 * To change this template use File | Settings | File Templates.
 */
define(['jquery'],function ($) {
    //private - covers all
    var abc = "foo";

    //public - related to instance
    return {
        def : "blah",
        get test() { return abc + ' ::: ' + this.def; },
        setTest:function(a) {abc = a;},
        setTest2:function(a) {this.def = a;}
    };







});