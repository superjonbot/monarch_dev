/*!Last Updated: 03.31.2014[17.05.42] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! asyncloader MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.31.2014[17.05.42] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule" ], function($, parentObject) {
    function _thisObject_(o) {
        var settings = {
            type: "asyncloader",
            author: "Jonathan Robles",
            lasteditby: "",
            dataIN: [ {
                filename: "fragments/content_0.html"
            }, {
                filename: "fragments/content_1.html"
            }, {
                filename: "fragments/content_2.html"
            }, {
                filename: "fragments/content_3.html"
            }, {
                filename: "fragments/content_4.html"
            }, {
                filename: "fragments/content_5.html"
            }, {
                filename: "fragments/content_6.html"
            } ],
            totalitems: 0,
            currentlyloading: -1,
            _percentloaded: function() {
                return Math.round(100 / (this.totalitems / (this.currentlyloading + 1)));
            },
            loadercommand: function(percent) {
                console.log("percent loaded:" + percent + "%");
            },
            callback: function(data) {
                console.log("all fragments loaded! BAM!");
            },
            loadcallback: function(o) {
                var count = o.count;
                var data = o.data.replace("<body", '<body><div id="body"').replace("</body>", "</div></body>");
                var body = $(data).filter("#body").html();
                $("#yourdiv").html(body);
            }
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {};
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thisObject_.prototype.refresh = function() {
        this.initloader();
        this.notify("Trace", "refresh");
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype.initloader = function() {
        var _parent = this;
        var loadlist = this._var().dataIN;
        this._var({
            totalitems: loadlist.length
        });
        $.each(loadlist, function(count, value) {
            var jsonOBJ = this;
            this.loadlist = loadlist;
            this.count = count;
            this.loadmythis = function() {
                var self = this;
                $.get(loadlist[this.count].filename).done(function(data) {
                    jsonOBJ.data = data;
                    _parent._var({
                        currentlyloading: count
                    });
                    _parent._var().loadercommand(_parent._var()._percentloaded());
                    _parent._var().loadcallback({
                        count: count,
                        data: data
                    });
                    if (this.loadlist.length - 1 == this.count) {
                        _parent._var().callback();
                    } else {
                        this.loadlist[this.count + 1].loadmythis();
                    }
                }).fail(function() {
                    alert("error");
                });
            };
        });
        this.startloading();
    };
    _thisObject_.prototype.startloading = function() {
        var loadlist = this._var().dataIN;
        loadlist[0].loadmythis();
    };
    return _thisObject_;
});
