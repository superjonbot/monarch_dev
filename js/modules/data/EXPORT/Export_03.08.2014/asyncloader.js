/*!Last Updated: 03.08.2014[02.06.19] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! asyncloader MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.08.2014[02.06.19] by Jonathan Robles*/
define(["jquery","modules/definitions/standardmodule"],function(c,b){function a(e){var d={type:"asyncloader",author:"Jonathan Robles",lasteditby:"",dataIN:[{filename:"fragments/content_0.html"},{filename:"fragments/content_1.html"},{filename:"fragments/content_2.html"},{filename:"fragments/content_3.html"},{filename:"fragments/content_4.html"},{filename:"fragments/content_5.html"},{filename:"fragments/content_6.html"}],totalitems:0,currentlyloading:-1,_percentloaded:function(){return Math.round(100/(this.totalitems/(this.currentlyloading+1)))},loadercommand:function(f){console.log("percent loaded:"+f+"%")},callback:function(f){console.log("all fragments loaded! BAM!")},loadcallback:function(i){var g=i.index;var h=i.data.replace("<body",'<body><div id="body"').replace("</body>","</div></body>");var f=c(h).filter("#body").html();c("#yourdiv").html(f)}};d=c.extend(d,e);b.call(this,d);return(this)}a.prototype=Object.create(b.prototype);a.prototype._startlisteners=function(){};a.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh()};a.prototype.refresh=function(){this.initloader();this.notify("Trace","refresh")};a.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id())};a.prototype.initloader=function(){var e=this;var d=this._var().dataIN;this._var({totalitems:d.length});c.each(d,function(g,h){var f=this;this.loadlist=d;this.index=g;this.loadmyself=function(){var i=this;c.get(d[this.index].filename).done(function(j){f.data=j;e._var({currentlyloading:g});e._var().loadercommand(e._var()._percentloaded());e._var().loadcallback({index:g,data:j});if((i.loadlist.length-1)==i.index){e._var().callback()}else{i.loadlist[(i.index+1)].loadmyself()}}).fail(function(){alert("error")})}});this.startloading()};a.prototype.startloading=function(){var d=this._var().dataIN;d[0].loadmyself()};return(a)});