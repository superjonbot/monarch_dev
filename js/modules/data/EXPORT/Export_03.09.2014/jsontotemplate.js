/*!Last Updated: 03.09.2014[17.40.59] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! jsontotemplate MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.09.2014[17.40.59] by Jonathan Robles*/
define(["jquery","modules/definitions/standardmodule","i18n!modules/nls/uxlocale"],function(d,c,a){function b(f){var e={type:"jsontotemplate",author:"Jonathan Robles",lasteditby:"",dataIN:{window:{title:"Sample Widget",name:"main_window",width:500,height:500},image:{src:"Images/Sun.png",name:"sun1",width:250,height:250,alignment:"center"}},htmlIN:'<div id="{{window.name}}"><p>{{window.title}}</p><img src="{{image.src}}" alt="{{image.name}}" height="{{image.height}}" width="{{image.width}}"></div>',regEX:/{{(.*?)}}/g,errorFill:function(g){return'[ xALERT!!! "'+g+'" not found in json]'},data:undefined,callback:function(g){alert("data rendered :"+g)},busy:false};e=d.extend(e,f);c.call(this,e);return(this)}b.prototype=Object.create(c.prototype);b.prototype._startlisteners=function(){this.notify("Trace","_startlisteners");var f=this;var e=this._id();_notify.add(this._id(),function(){return{onJSONtoTemplateData:function(g){if(g.senderID==e){f._var({data:g.data,busy:false});if(f._var().callback!=undefined){f._var().callback(g.data)}}}}}())};b.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh()};b.prototype.refresh=function(){this.notify("Trace","refresh");this.translateData()};b.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id())};b.prototype.translateData=function(){var m=function(r,o){var q=o.split(".");var n=r[q[0]];if(q[1]){q.splice(0,1);var p=q.join(".");return m(n,p)}return n};var j=this;j._var({busy:true});this.notify("Trace","translateData dataIn:"+JSON.stringify(j._var().dataIN)+" \n to datablock:"+j._var().htmlIN);var e=function(n){j.notify("JSONtoTemplateData",n)};var i=j._var().dataIN;i.locale=a;var k=j._var().htmlIN;var l=k;var f=j._var().regEX;var g=k.match(f);var h=undefined;if(g!=null){d.each(g,function(o){var p=g[o];var n=p.replace(/[{}]/g,"");var q=m(i,n);q=d("<div />").html(q).text();if(q!=undefined){l=l.replace(p,q)}else{l=l.replace(p,j._var().errorFill(n))}});h=l}else{h=k}e(h)};return(b)});