/*!Last Updated: 03.06.2014[20.16.02] by Jonathan Robles*/
/*!*/
/*!********************************************************************************************************/
/*!                                                                                                       */
/*! jsontotemplate module (c) 2014 Jonathan Robles */
/*! http://jonathanrobles.net                                                                             */
/*!                                                                                                       */
/*! This software may be freely distributed under the                                                     */
/*! Creative Commons Attribution-ShareAlike (CC BY-SA) License                                            */
/*! This license lets others remix, tweak, and build upon your work even for commercial purposes,         */
/*! as long as they credit you and license their new creations under the identical terms. All new         */
/*! works based on yours will carry the same license, so any derivatives will also allow commercial use.  */
/*! link: https://creativecommons.org/examples#by-sa                                                      */
/*!                                                                                                       */
/*!********************************************************************************************************/
/*!CL1b*/
/*!*/
/*!*/
/*!Last Updated: 03.06.2014[20.16.02] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule", "i18n!modules/nls/uxlocale" ], function($, parentModel, locale) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "jsontotemplate",
            author: "Jonathan Robles",
            lasteditby: "",
            dataIN: {
                window: {
                    title: "Sample Widget",
                    name: "main_window",
                    width: 500,
                    height: 500
                },
                image: {
                    src: "Images/Sun.png",
                    name: "sun1",
                    width: 250,
                    height: 250,
                    alignment: "center"
                }
            },
            htmlIN: '<div id="{{window.name}}"><p>{{window.title}}</p><img src="{{image.src}}" alt="{{image.name}}" height="{{image.height}}" width="{{image.width}}"></div>',
            regEX: /{{(.*?)}}/g,
            errorFill: function(data) {
                return '[ xALERT!!! "' + data + '" not found in json]';
            },
            data: undefined,
            callback: function(data) {
                alert("data rendered :" + data);
            },
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onJSONtoTemplateData: function(o) {
                    if (o.senderID == myID) {
                        parent._var({
                            data: o.data,
                            busy: false
                        });
                        if (parent._var().callback != undefined) {
                            parent._var().callback(o.data);
                        }
                    }
                }
            };
        }());
    };
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this.translateData();
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.translateData = function() {
        var recompose = function(obj, string) {
            var parts = string.split(".");
            var newObj = obj[parts[0]];
            if (parts[1]) {
                parts.splice(0, 1);
                var newString = parts.join(".");
                return recompose(newObj, newString);
            }
            return newObj;
        };
        var parent = this;
        parent._var({
            busy: true
        });
        this.notify("Trace", "translateData dataIn:" + JSON.stringify(parent._var().dataIN) + " \n to datablock:" + parent._var().htmlIN);
        var onData = function(data) {
            parent.notify("JSONtoTemplateData", data);
        };
        var tempjsondata = parent._var().dataIN;
        tempjsondata.locale = locale;
        var originalTemplate = parent._var().htmlIN;
        var processedTemplate = originalTemplate;
        var regEX = parent._var().regEX;
        var findinstances = originalTemplate.match(regEX);
        var data = undefined;
        if (findinstances != null) {
            $.each(findinstances, function(index) {
                var searchstring = findinstances[index];
                var cleanstring = searchstring.replace(/[{}]/g, "");
                var newstring = recompose(tempjsondata, cleanstring);
                newstring = $("<div />").html(newstring).text();
                if (newstring != undefined) {
                    processedTemplate = processedTemplate.replace(searchstring, newstring);
                } else {
                    processedTemplate = processedTemplate.replace(searchstring, parent._var().errorFill(cleanstring));
                }
            });
            data = processedTemplate;
        } else {
            data = originalTemplate;
        }
        onData(data);
    };
    return _thizOBJ_;
});