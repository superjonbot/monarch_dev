/*!Last Updated: 02.06.2014[16.42.18] by Jonathan Robles*/



/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
//  asyncloader Module                                  
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/

 // Date: 12/21/13 : 1:20 PM
 //
 // NOTE: asynchronous thingamajig loader
 //
 // USAGE: 
 //
/*CL1*/


/*!Last Updated: 02.06.2014[16.42.18] by Jonathan Robles*/
define(["jquery","modules/definitions/standardmodule"],function($,parentModel){function _thizOBJ_(o){var defaults={type:"asyncloader",author:"Jonathan Robles",lasteditby:"",dataIN:[{filename:"fragments/content_0.html"},{filename:"fragments/content_1.html"},{filename:"fragments/content_2.html"},{filename:"fragments/content_3.html"},{filename:"fragments/content_4.html"},{filename:"fragments/content_5.html"},{filename:"fragments/content_6.html"}],totalitems:0,currentlyloading:-1,_percentloaded:function(){return Math.round(100/(this.totalitems/(this.currentlyloading+1)));},loadercommand:function(percent){console.log("percent loaded:"+percent+"%");},callback:function(data){console.log("all fragments loaded! BAM!");},loadcallback:function(o){var index=o.index;var data=o.data.replace("<body",'<body><div id="body"').replace("</body>","</div></body>");var body=$(data).filter("#body").html();$("#yourdiv").html(body);}};defaults=$.extend(defaults,o);parentModel.call(this,defaults);return(this);}_thizOBJ_.prototype=Object.create(parentModel.prototype);_thizOBJ_.prototype._startlisteners=function(){};_thizOBJ_.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh();};_thizOBJ_.prototype.refresh=function(){this.initloader();this.notify("Trace","refresh");};_thizOBJ_.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id());};_thizOBJ_.prototype.initloader=function(){var _parent=this;var loadlist=this._var().dataIN;this._var({totalitems:loadlist.length});$.each(loadlist,function(index,value){var jsonOBJ=this;this.loadlist=loadlist;this.index=index;this.loadmyself=function(){var self=this;$.get(loadlist[this.index].filename).done(function(data){jsonOBJ.data=data;_parent._var({currentlyloading:index});_parent._var().loadercommand(_parent._var()._percentloaded());_parent._var().loadcallback({index:index,data:data});if((self.loadlist.length-1)==self.index){_parent._var().callback();}else{self.loadlist[(self.index+1)].loadmyself();}}).fail(function(){alert("error");});};});this.startloading();};_thizOBJ_.prototype.startloading=function(){var loadlist=this._var().dataIN;loadlist[0].loadmyself();};return(_thizOBJ_);});