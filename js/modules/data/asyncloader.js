/*!
 * asyncloader Module
 *
 * Date: 12/12/13 : 3:24 PM
 *
 * NOTE: asynchronous thingamajig loader
 *
 * USAGE:
 *
 */
define(['jquery', 'modules/definitions/standardmodule'], function ($, parentModel) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'asyncloader', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)

            dataIN:[
                        {filename:'fragments/content_0.html'},
                        {filename:'fragments/content_1.html'},
                        {filename:'fragments/content_2.html'},
                        {filename:'fragments/content_3.html'},
                        {filename:'fragments/content_4.html'},
                        {filename:'fragments/content_5.html'},
                        {filename:'fragments/content_6.html'}

                    ],

            totalitems:0,
            currentlyloading:-1,
            _percentloaded:function(){
                return Math.round(100/(this.totalitems/(this.currentlyloading+1)));
            },

            loadercommand:function(percent){
                console.log('percent loaded:'+percent+'%')
            },

            callback:function(data){console.log('all fragments loaded! BAM!')},
            loadcallback:function(o){  //use o.index and o.data

                var index= o.index;
                //console.log('!hey there from '+ o.index+' : '+this._percentloaded());
                var data = o.data.replace('<body', '<body><div id="body"').replace('</body>','</div></body>');
                var body = $(data).filter('#body').html();
                $("#yourdiv").html(body);


            }

            //target:$('#yourdiv'),  //target object that contains children to control
            //childObjects:undefined,



            /*, TRY TO STICK TO THESE COMMON STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             file:'somedir/somefile.ext',
             usenocache:true,
             dataIN:{},
             data:undefined,
             callback:function(){},
             interval:undefined,
             busy:false
             */
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {
        /*

         this.notify('Trace','_startlisteners');
         var parent=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {

         onJSONdata:function(o){
         if(o.senderID==myID){
         parent._var({data: o.data,busy:false});
         if(parent._var().callback!=undefined){
         parent._var().callback(o.data)
         }
         }
         }
         }
         }());

         */
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init = function () {

        /*
         if(<object>!=exist){
         <object>.create();
         }else{
         <object>.show(); //just show it and start listeners
         };
         * */
        this.notify('Trace', 'init');
        this._startlisteners();//start this module's listeners
        this.refresh();

    };

    _thizOBJ_.prototype.refresh = function () {

        this.initloader()
        this.notify('Trace', 'refresh');
    };

    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.initloader = function () {

        var _parent=this;
        var loadlist=this._var().dataIN;
        this._var({totalitems:loadlist.length})

        $.each(loadlist,function(index,value){
            var jsonOBJ=this;
            this.loadlist=loadlist;
            this.index=index;
            this.loadmyself=function(){
                    var self=this;


                $.get(loadlist[this.index].filename)
                    .done(function(data) {
                        //store raw data
                        jsonOBJ.data=data;
                        //adjust loader
                        _parent._var({currentlyloading:index});
                        _parent._var().loadercommand(_parent._var()._percentloaded());
                        //display data
                        _parent._var().loadcallback({index:index,data:data});






                        if((self.loadlist.length-1)==self.index){
                           // alert('real callback!');
                            _parent._var().callback();
                        } else {
                            self.loadlist[(self.index+1)].loadmyself();//self.loadlist[self.index+1].loadmyself;
                        };

                    })
                    .fail(function() {
                        alert( "error" );
                    })



            };
        })


        //alert(':P'+loadlist[0].filename+' : '+loadlist[5].index+' : '+this._var().dataIN[5].index)
        this.startloading();

    }
    _thizOBJ_.prototype.startloading = function () {
        var loadlist=this._var().dataIN;
        loadlist[0].loadmyself();
    }


    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


//knockout blurb
 /*
 define(['knockout','modules/view/viewmodels/application4vm'],
 function (ko,appViewModel) {
    ko.applyBindings(new appViewModel());
 });
 */
