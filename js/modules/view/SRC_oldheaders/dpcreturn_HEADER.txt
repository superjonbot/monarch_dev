/*!Last Updated: <inZertDATe> by Jonathan Robles*/



/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
//  dpcreturn MODULE (note: this is a one-of
//  module and needs to be optimized in node.js 
//  by creating a build script)                                 
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/

/**
 *
 * NOTE: This object, takes the dpc json object and exports items for each entry
 * USAGE:
 * var dpcReturn = new Template({
 *           file: 'data/dpcdemo.json',  //file location
 *           format: 'json',
 *           target: $('#dpclistitem'),
 *           callback: function (data) {
 *               alert('COMPLETE!!:')
 *           }
 *       }).init();
 */


/*!Last Updated: <inZertDATe> by Jonathan Robles*/