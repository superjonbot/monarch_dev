/**
 * binddatatotarget Module
 *
 * Date: 12/13/13 : 11:22 AM
 *
 * NOTE: takes a Json bind data and applies to children of the defined target, or you can define children yourself.
 *
 * USAGE:
 *
 */
define(['jquery','underscore', 'modules/definitions/standardmodule','hammer'], function ($,_, parentModel,Hammer) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'binddatatotarget', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)
            target :$('#yourdiv'),
            autochild:true,  //set to false to define child objects manually
            childObjects:[$('#yourdiv>div:nth-child(2)'),$('#yourdiv>div:nth-child(2)'),$('#yourdiv>div:nth-child(3)')],  //not needed when using target and autochild


            dataIN:[
                {   label:'Button 0',
                    onBind:{
                        click:function(e){console.log('click'+this.label);this.target.hide()},
                        mouseover:function(e){console.log('hover')},
                        mouseout:function(e){console.log('out')}
                    }
                },
                {   label:'Button 1',
                    onBind:{
                        click:function(e){console.log('click'+this.enabled)},
                        mouseover:function(e){console.log('hover')},
                        mouseout:function(e){console.log('out')}
                    }

                },
                {   label:'Button 2',
                    onHammer:{
                        tap:function(e){alert('tap'+this.label)/*alert(this,event)*/},
                        hold:function(e){alert('hold')}
                    }

                },
                {   label:'Button 3',
                    onHammer:{
                        tap:function(e){alert('tap'+this.label)/*alert(this,event)*/},
                        hold:function(e){alert('hold')}
                    }

                },
                {   label:'Button 4',
                    onHammer:{
                        tap:function(e){alert('swipe:'+this.label)/*alert(this,event)*/}

                    }
                },
                {   label:'Button 5',
                    onHammer:{
                        swipe:function(e){alert('swipe:'+this.label)/*alert(this,event)*/}

                    }

                }

            ],


           /*   ALSO ADDED TO ALL ARRAY OBJECTS
        this.index=index;
        this.target=$(childObjects[index]);
        this.enabled=true;
        this.bindarray=[];
        this.hammerarray=[];
        this.normalkeysindex=undefined;
        this.hammerkeysindex=undefined;*/

     /*       htmlIN:{
                listitems:'<div><span>{{label}}</span>&nbsp;<span></span></div>'
            },

            dataparserOBJ:undefined,    //holds dataparser module
            templateOBJ:undefined,      //holds jsontotemplate module*/
            busy:false





            /*, TRY TO STICK TO THESE COMMON STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             file:'somedir/somefile.ext',
             usenocache:true,
             dataIN:{},
             data:undefined,
             callback:function(){},
             interval:undefined,
             busy:false
             */
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {
        /*

         this.notify('Trace','_startlisteners');
         var parent=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {

         onJSONdata:function(o){
         if(o.senderID==myID){
         parent._var({data: o.data,busy:false});
         if(parent._var().callback!=undefined){
         parent._var().callback(o.data)
         }
         }
         }
         }
         }());

         */
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init = function () {
        /*
         if(<object>!=exist){
         <object>.create();
         }else{
         <object>.show(); //just show it and start listeners
         };
         * */
        this.notify('Trace', 'init');
        this._startlisteners();//start this module's listeners
        this.refresh();

    };

    _thizOBJ_.prototype.refresh = function () {
        this.notify('Trace', 'refresh');
        this.setbinds()
    };

    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        this.disableALL()
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.setbinds = function () {
        //this.notify('Trace', ('hi there, from object#' + this._id() + ' [ ' + this._var().type + ' by ' + this._var().author + ' ] '));

        var self=this;

        self._var({busy:true});

        var arrayref=this._var().dataIN;

        if(self._var().autochild){
            self._var({childObjects:self._var().target.children()}); //make childobjects
        }

        var childObjects=this._var().childObjects;

        $.each(arrayref,function(index,value){


            var eachself=this;
            this.index=index;
            this.target=$(childObjects[index]);
            this.enabled=true;
            this.bindarray=[];
            this.hammerarray=[];
            this.normalkeysindex=undefined;
            this.hammerkeysindex=undefined;

            //console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-')

            var keys=_.keys(value)
            var hammerkeys=undefined;
            var normalkeys=undefined;
            //console.log(keys)

            //bind normalkeys
            self.normalkeysindex=keys.indexOf('onBind');
            if(self.normalkeysindex!=-1){
                normalkeys=_.keys(value.onBind )

                $.each(normalkeys,function(n_i,bindvalue){


                    var bindfunction=function(){
                        if(this.enabled){
                            var tempfunc=value.onBind[bindvalue];
                            tempfunc = _.bind(tempfunc, eachself);
                            tempfunc();
                        }
                    };

                    bindfunction = _.bind(bindfunction, eachself); //make parent the scope

                    //make bind and store it in the bindarray object
                    eachself.bindarray[n_i]=  eachself.target.on(bindvalue,bindfunction);

                })

                //console.log( 'normalkeys: '+normalkeys);


            }

            //bind hammerkeys
            self.hammerkeysindex=keys.indexOf('onHammer');
            if(self.hammerkeysindex!=-1){
                hammerkeys=_.keys(value.onHammer )

                $.each(hammerkeys,function(n_i,bindvalue){
                    //var bindfunction=value.onHammer[bindvalue];

                    var bindfunction=function(){
                        if(this.enabled){
                            var tempfunc=value.onHammer[bindvalue];
                            tempfunc = _.bind(tempfunc, eachself);
                            tempfunc();
                        }
                    };




                    bindfunction = _.bind(bindfunction, eachself); //make parent the scope

                    //make bind and store it in the bindarray object
                    eachself.hammerarray[n_i]= Hammer(eachself.target.on(bindvalue, bindfunction));
                    //eachself.bindarray[n_i]=  $(childObjects[index]).on(bindvalue,bindfunction);

                })




              //  console.log( 'hammerkeys: '+hammerkeys);
            }


            //console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-')
        })


        self._var({busy:false});

    };

    _thizOBJ_.prototype.enable = function (getIndex) {
        this._var().dataIN[getIndex].enabled=true;
    };
    _thizOBJ_.prototype.disable = function (getIndex) {
        this._var().dataIN[getIndex].enabled=false;
    };
    _thizOBJ_.prototype.enableALL = function () {
        $.each( this._var().dataIN,function(d_i,d_v){
            d_v.enabled=true;
        })
    };
    _thizOBJ_.prototype.disableALL = function () {
        $.each( this._var().dataIN,function(d_i,d_v){
            d_v.enabled=false;
        })
    };
    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


//knockout blurb
 /*
 define(['knockout','modules/view/viewmodels/application4vm'],
 function (ko,appViewModel) {
    ko.applyBindings(new appViewModel());
 });
 */
