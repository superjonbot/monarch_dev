/*!Last Updated: 03.06.2014[20.25.49] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! pinchzoom MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CL1b*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.06.2014[20.25.49] by Jonathan Robles*/
define([ "jquery", "underscore", "modules/definitions/standardmodule", "hammer", "tweenmax" ], function($, _, parentModel, Hammer, TweenMax) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "pinchzoom",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv"),
            child: undefined,
            childindex: 0,
            bindsonchild: false,
            touchdetect: "touch release drag pinch",
            zoombracket: [ .3, 3 ],
            zoommultiplier: .2,
            initialposition: undefined,
            currentposition: {
                matrix: undefined,
                x: 0,
                y: 0,
                z: 0
            },
            positionsteps: 30,
            enable_touchbinds: true,
            callback: function(o) {},
            childchange: function(index) {
                var self = this.parent;
                var destination = undefined;
                var animationvars = undefined;
                if (!self._var().yscroll) {
                    destination = self._var().childVars[index].XoffsetfromScroll;
                    animationvars = {
                        scrollTo: {
                            x: destination
                        },
                        ease: Power4.easeOut
                    };
                } else {
                    destination = self._var().childVars[index].YoffsetfromScroll;
                    animationvars = {
                        scrollTo: {
                            y: destination
                        },
                        ease: Power4.easeOut
                    };
                }
                TweenMax.to(self._var().target, .7, animationvars);
            },
            onscroll_callback: function(o) {},
            onscroll_start: function(o) {},
            onscroll_debounce_callback: function(o) {},
            onscroll_debounce_rate: 500,
            ObjHammer: undefined,
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {};
    _thizOBJ_.prototype.init = function() {
        var self = this;
        self.notify("Trace", "init");
        self._var({
            busy: true
        });
        self._startlisteners();
        self.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        var self = this;
        self._var({
            busy: true
        });
        self.notify("Trace", "refresh");
        self.getandsetneededvars();
        var bindtarget = self._var().bindsonchild ? self._var().child : self._var().target;
        self._var().ObjHammer = Hammer(bindtarget, {
            drag_lock_to_axis: false
        }).on(self._var().touchdetect, self._bindHandler(self));
        self._var({
            busy: false
        });
        self.refreshposition();
        if (self._var().callback != undefined) {
            self._var().callback(self);
        }
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.getandsetneededvars = function() {
        var self = this;
        var vars = {};
        var thisindex = self._var().childindex;
        vars.child = $(self._var().target.children()[thisindex]);
        self._var(vars);
        return vars;
    };
    _thizOBJ_.prototype.updatePositionvars = function() {
        var self = this;
        var positionOBJ = {};
        if (self._var().initialposition == undefined) {
            self._var().child.css("webkitTransform", "skew(0)");
        }
        positionOBJ.matrix = self._var().child.css("transform");
        positionOBJ.matrix = positionOBJ.matrix.match(/\(([^)]+)\)/)[1].split(",");
        positionOBJ.z = Number(positionOBJ.matrix[0]);
        positionOBJ.x = Number(positionOBJ.matrix[4]);
        positionOBJ.y = Number(positionOBJ.matrix[5]);
        self._var().currentposition = $.extend(self._var().currentposition, positionOBJ);
        if (self._var().initialposition === undefined) {
            var currentposition = {};
            $.extend(currentposition, positionOBJ);
            self._var().initialposition = currentposition;
        }
        return positionOBJ;
    };
    _thizOBJ_.prototype.refreshposition = function(getnewposition) {
        var self = this;
        if (getnewposition == undefined) {
            return self.updatePositionvars();
        } else {
            var destination = getnewposition;
            TweenMax.to(self._var().child, .01, {
                scaleX: destination.z,
                scaleY: destination.z,
                y: destination.y,
                x: destination.x,
                ease: Power4.easeOut,
                onComplete: function() {
                    self.updatePositionvars();
                }
            });
        }
    };
    _thizOBJ_.prototype.resetposition = function() {
        var self = this;
        self.refreshposition(self._var().initialposition);
    };
    _thizOBJ_.prototype.zoomin = function() {
        var self = this;
        var currentZ = self._var().currentposition.z;
        var targetZ = currentZ + currentZ * self._var().zoommultiplier;
        if (targetZ > self._var().zoombracket[1]) {
            targetZ = self._var().zoombracket[1];
        }
        self.refreshposition({
            z: targetZ
        });
    };
    _thizOBJ_.prototype.zoomout = function() {
        var self = this;
        var currentZ = self._var().currentposition.z;
        var targetZ = currentZ - currentZ * self._var().zoommultiplier;
        if (targetZ < self._var().zoombracket[0]) {
            targetZ = self._var().zoombracket[0];
        }
        self.refreshposition({
            z: targetZ
        });
    };
    _thizOBJ_.prototype.moveUp = function() {
        var self = this;
    };
    _thizOBJ_.prototype._bindHandler = function(self) {
        var internal_position = {
            x: 0,
            y: 0,
            z: 0
        };
        var lastX = internal_position.x;
        var lastY = internal_position.y;
        var lastZ = internal_position.z;
        var lasttype = undefined;
        console.log(":)");
        var eventHandler = function(ev) {
            ev.gesture.preventDefault();
            switch (ev.type) {
              case "touch":
                internal_position = $.extend(internal_position, self.refreshposition());
                lastX = internal_position.x;
                lastY = internal_position.y;
                lastZ = internal_position.z;
                break;

              case "drag":
                var Xdest = lastX + ev.gesture.deltaX;
                var Ydest = lastY + ev.gesture.deltaY;
                self.refreshposition({
                    x: Xdest,
                    y: Ydest
                });
                break;

              case "pinch":
                var targetscale = lastZ - (1 - ev.gesture.scale);
                if (targetscale < .1) {
                    targetscale = .1;
                }
                self.refreshposition({
                    z: targetscale
                });
                break;

              case "release":
                internal_position = $.extend(internal_position, self.refreshposition());
                lastX = internal_position.x;
                lastY = internal_position.y;
                lastZ = internal_position.z;
                var targetscale = self._var().currentposition.z;
                if (targetscale < self._var().zoombracket[0]) {
                    targetscale = self._var().zoombracket[0];
                    TweenMax.to(self._var().child, 2, {
                        scaleX: targetscale,
                        scaleY: targetscale,
                        ease: Elastic.easeOut,
                        onComplete: function() {
                            self.refreshposition();
                        }
                    });
                }
                if (targetscale > self._var().zoombracket[1]) {
                    targetscale = self._var().zoombracket[1];
                    TweenMax.to(self._var().child, 2, {
                        scaleX: targetscale,
                        scaleY: targetscale,
                        ease: Elastic.easeOut,
                        onComplete: function() {
                            self.refreshposition();
                        }
                    });
                }
                ev.gesture.stopDetect();
                break;
            }
        };
        return eventHandler;
    };
    return _thizOBJ_;
});