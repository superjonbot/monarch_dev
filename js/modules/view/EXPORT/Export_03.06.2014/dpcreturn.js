/*!Last Updated: 03.06.2014[20.25.47] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! dpcreturn MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CL1b*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.06.2014[20.25.47] by Jonathan Robles*/
define(["jquery","modules/definitions/standardmodule","modules/parsers/dataparser","modules/data/jsontotemplate"],function($,parentModel,dataOBJ,templateOBJ){function _thi$OBJ_(o){var defaults={type:"dpcparser",author:"Jonathan Robles",lasteditby:"",file:"data/dpcdemo.json",format:"json",htmlIN:{listitems:'<li><a href="http://www.medscape.fr/dpc/activity/start/{{activityId}}" class="title">{{title}}</a><div class="progressIcon {{progressStatus.name}}"></div><span class="teaser">{{teaser}}</span><div class="byline">{{specialty}}, {{releaseDate}}</div></li>'},data:undefined,dataparserOBJ:undefined,templateOBJ:undefined,busy:false};defaults=$.extend(defaults,o);parentModel.call(this,defaults);return(this);}_thi$OBJ_.prototype=Object.create(parentModel.prototype);_thi$OBJ_.prototype._startlisteners=function(){this.notify("Trace","_startlisteners");var parent=this;var myID=this._id();var dataID=parent._var().dataparserOBJ._id();_notify.add(this._id(),function(){return{onJSONdata:function(o){if(o.senderID==dataID){parent.formatdata(o.data);}},onFormatdata:function(o){if(o.senderID==myID){parent.displaydata(o.data);}}};}());};_thi$OBJ_.prototype.init=function(){this.notify("Trace","init");var parent=this;parent._var().dataparserOBJ=new dataOBJ();parent._var().templateOBJ=new templateOBJ();parent._var().templateOBJ._startlisteners();this._startlisteners();this.refresh();};_thi$OBJ_.prototype.refresh=function(){var parent=this;this.notify("Trace","refresh");parent._var({busy:true});parent._var().dataparserOBJ._var({file:parent._var().file,format:parent._var().format});parent._var().templateOBJ._var({htmlIN:parent._var().htmlIN.listitems,callback:function(data){var tempdata=parent._var().data;tempdata+=data;parent._var({data:tempdata});}});this.getdata();};_thi$OBJ_.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id());};_thi$OBJ_.prototype.getdata=function(){this._var().dataparserOBJ.init();};_thi$OBJ_.prototype.formatdata=function(data){var parent=this;parent._var().data="";$.each(data,function(index,value){parent._var().templateOBJ._var({dataIN:value});parent._var().templateOBJ.refresh();});this.notify("Formatdata",parent._var().data);};_thi$OBJ_.prototype.displaydata=function(){var parent=this;parent._var().target.empty().html(parent._var().data);parent._var({busy:false});if(parent._var().callback!=undefined){parent._var().callback(parent._var().data);}};return(_thi$OBJ_);});