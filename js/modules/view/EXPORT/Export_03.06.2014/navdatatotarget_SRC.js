/*!Last Updated: 03.06.2014[20.25.48] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! navdatatotarget MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CL1b*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.06.2014[20.25.48] by Jonathan Robles*/
define([ "jquery", "underscore", "modules/definitions/standardmodule", "hammer" ], function($, _, parentModel, Hammer) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "navdatatotarget",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv"),
            dataIN: [ {
                label: "Button 0",
                onsmartClick: function(e) {
                    alert("SMARTclick");
                },
                onBind: {
                    click: function(e) {
                        console.log("click" + this.label);
                    },
                    mouseover: function(e) {
                        console.log("hover");
                    },
                    mouseout: function(e) {
                        console.log("out");
                    }
                }
            }, {
                label: "Button 1",
                onClick: function() {
                    console.log("clicked " + this.label);
                },
                onHover: function() {
                    console.log("hovered " + this.label);
                }
            }, {
                label: "Button 2",
                onHammer: {
                    tap: function(e) {
                        alert("tap" + this.label);
                    },
                    hold: function(e) {
                        alert("hold");
                    }
                }
            }, {
                label: "Button 3",
                onHammer: {
                    tap: function(e) {
                        alert("tap" + this.label);
                    },
                    hold: function(e) {
                        alert("hold");
                    }
                }
            }, {
                label: "Button 4",
                onClick: function() {
                    console.log("clicked " + this.label);
                },
                onHover: function() {
                    console.log("hovered " + this.label);
                }
            }, {
                label: "Button 5",
                onClick: function() {
                    console.log("clicked " + this.label);
                },
                onHover: function() {
                    console.log("hovered " + this.label);
                }
            } ],
            htmlIN: {
                listitems: "<div><span>{{label}}</span>&nbsp;<span></span></div>"
            },
            dataparserOBJ: undefined,
            templateOBJ: undefined,
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {};
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this.sayhi();
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.sayhi = function() {
        this.notify("Trace", "hi there, from object#" + this._id() + " [ " + this._var().type + " by " + this._var().author + " ] ");
        var self = this;
        var arrayref = this._var().dataIN;
        self._var({
            childObjects: self._var().target.children()
        });
        var childObjects = this._var().childObjects;
        $.each(arrayref, function(index, value) {
            var eachself = this;
            this.index = index;
            this.bindarray = [];
            this.hammerarray = [];
            this.normalkeysindex = undefined;
            this.hammerkeysindex = undefined;
            console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
            var keys = _.keys(value);
            var hammerkeys = undefined;
            var normalkeys = undefined;
            console.log(keys);
            self.normalkeysindex = keys.indexOf("onBind");
            if (self.normalkeysindex != -1) {
                normalkeys = _.keys(value.onBind);
                $.each(normalkeys, function(n_i, bindvalue) {
                    var bindfunction = value.onBind[bindvalue];
                    bindfunction = _.bind(bindfunction, eachself);
                    eachself.bindarray[n_i] = $(childObjects[index]).on(bindvalue, bindfunction);
                });
                console.log("normalkeys: " + normalkeys);
            }
            self.hammerkeysindex = keys.indexOf("onHammer");
            if (self.hammerkeysindex != -1) {
                hammerkeys = _.keys(value.onHammer);
                $.each(hammerkeys, function(n_i, bindvalue) {
                    var bindfunction = value.onHammer[bindvalue];
                    bindfunction = _.bind(bindfunction, eachself);
                    eachself.hammerarray[n_i] = Hammer($(childObjects[index]).on(bindvalue, bindfunction));
                });
            }
            console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
        });
    };
    return _thizOBJ_;
});