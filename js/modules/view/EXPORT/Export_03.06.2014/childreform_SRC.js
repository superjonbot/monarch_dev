/*!Last Updated: 03.06.2014[20.25.46] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! childreform MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CL1b*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.06.2014[20.25.46] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule" ], function($, parentModel) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "childreform",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv"),
            childObjects: undefined,
            totalslides: undefined,
            childVars: undefined,
            childVar_setter: function(index, value) {
                var self = this;
                self.index = index;
                self.childOBJ = value;
                self.target = $(value);
                self.Xsize = self.target.outerWidth();
                self.Ysize = self.target.innerHeight();
                self.Xdefault = Math.round(self.target.position().left);
                self.Ydefault = Math.round(self.target.position().top);
                self.matrix = undefined;
                self.x = 0;
                self.y = 0;
                self.z = 0;
            },
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {};
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this._childrentochildObjects();
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype._childrentochildObjects = function() {
        var self = this;
        self._var({
            childObjects: self._var().target.children()
        });
        var childObjects = self._var().childObjects;
        self._var({
            totalslides: childObjects.length,
            childVars: []
        });
        $.each(childObjects, function(index, value) {
            childVar_setter_local = self._var().childVar_setter;
            self._var().childVars.push(new childVar_setter_local(index, value));
        });
        $.each(self._var().childVars, function(index, value) {
            if (value.target.css("transform") == "none") {
                value.target.css("webkitTransform", "skew(0)");
            }
            value.matrix = value.target.css("transform");
            value.matrix = value.matrix.match(/\(([^)]+)\)/)[1].split(",");
            value.z = Number(value.matrix[0]);
            value.x = Number(value.matrix[4]);
            value.y = Number(value.matrix[5]);
        });
    };
    return _thizOBJ_;
});