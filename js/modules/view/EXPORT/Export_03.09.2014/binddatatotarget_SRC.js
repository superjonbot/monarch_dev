/*!Last Updated: 03.09.2014[17.43.26] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! binddatatotarget MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.09.2014[17.43.26] by Jonathan Robles*/
define([ "jquery", "underscore", "modules/definitions/standardmodule", "hammer" ], function($, _, parentObject, Hammer) {
    function _thisObject_(o) {
        var settings = {
            type: "binddatatotarget",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv"),
            autochild: true,
            childObjects: [ $("#yourdiv>div:nth-child(2)"), $("#yourdiv>div:nth-child(2)"), $("#yourdiv>div:nth-child(3)") ],
            dataIN: [ {
                label: "Button 0",
                onBind: {
                    click: function(e) {
                        console.log("click" + this.label);
                        this.target.hide();
                    },
                    mouseover: function(e) {
                        console.log("hover");
                    },
                    mouseout: function(e) {
                        console.log("out");
                    }
                }
            }, {
                label: "Button 1",
                onBind: {
                    click: function(e) {
                        console.log("click" + this.enabled);
                    },
                    mouseover: function(e) {
                        console.log("hover");
                    },
                    mouseout: function(e) {
                        console.log("out");
                    }
                }
            }, {
                label: "Button 2",
                onHammer: {
                    tap: function(e) {
                        alert("tap" + this.label);
                    },
                    hold: function(e) {
                        alert("hold");
                    }
                }
            }, {
                label: "Button 3",
                onHammer: {
                    tap: function(e) {
                        alert("tap" + this.label);
                    },
                    hold: function(e) {
                        alert("hold");
                    }
                }
            }, {
                label: "Button 4",
                onHammer: {
                    tap: function(e) {
                        alert("swipe:" + this.label);
                    }
                }
            }, {
                label: "Button 5",
                onHammer: {
                    swipe: function(e) {
                        alert("swipe:" + this.label);
                    }
                }
            } ],
            busy: false
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {};
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thisObject_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this.setbinds();
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        this.disableALL();
        _notify.rem(this._id());
    };
    _thisObject_.prototype.setbinds = function() {
        var self = this;
        this._var({
            busy: true
        });
        var arrayref = this._var().dataIN;
        if (this._var().autochild) {
            this._var({
                childObjects: this._var().target.children()
            });
        }
        var childObjects = this._var().childObjects;
        $.each(arrayref, function(count, value) {
            var eachthis = this;
            this.count = count;
            this.target = $(childObjects[count]);
            this.enabled = true;
            this.bindarray = [];
            this.hammerarray = [];
            this.normalkeyscount = undefined;
            this.hammerkeyscount = undefined;
            var keys = _.keys(value);
            var hammerkeys = undefined;
            var normalkeys = undefined;
            this.normalkeyscount = keys.countOf("onBind");
            if (this.normalkeyscount != -1) {
                normalkeys = _.keys(value.onBind);
                $.each(normalkeys, function(n_i, bindvalue) {
                    var bindfunction = function() {
                        if (this.enabled) {
                            var tempfunc = value.onBind[bindvalue];
                            tempfunc = _.bind(tempfunc, eachthis);
                            tempfunc();
                        }
                    };
                    bindfunction = _.bind(bindfunction, eachthis);
                    eachthis.bindarray[n_i] = eachthis.target.on(bindvalue, bindfunction);
                });
            }
            this.hammerkeyscount = keys.countOf("onHammer");
            if (this.hammerkeyscount != -1) {
                hammerkeys = _.keys(value.onHammer);
                $.each(hammerkeys, function(n_i, bindvalue) {
                    var bindfunction = function() {
                        if (this.enabled) {
                            var tempfunc = value.onHammer[bindvalue];
                            tempfunc = _.bind(tempfunc, eachthis);
                            tempfunc();
                        }
                    };
                    bindfunction = _.bind(bindfunction, eachthis);
                    eachthis.hammerarray[n_i] = Hammer(eachthis.target.on(bindvalue, bindfunction));
                });
            }
        });
        this._var({
            busy: false
        });
    };
    _thisObject_.prototype.enable = function(getIndex) {
        this._var().dataIN[getIndex].enabled = true;
    };
    _thisObject_.prototype.disable = function(getIndex) {
        this._var().dataIN[getIndex].enabled = false;
    };
    _thisObject_.prototype.enableALL = function() {
        $.each(this._var().dataIN, function(d_i, d_v) {
            d_v.enabled = true;
        });
    };
    _thisObject_.prototype.disableALL = function() {
        $.each(this._var().dataIN, function(d_i, d_v) {
            d_v.enabled = false;
        });
    };
    return _thisObject_;
});
