/*!Last Updated: 03.09.2014[17.43.34] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! pinchzoom MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.09.2014[17.43.34] by Jonathan Robles*/
define([ "jquery", "underscore", "modules/definitions/standardmodule", "hammer", "tweenmax" ], function($, _, parentObject, Hammer, TweenMax) {
    function _thisObject_(o) {
        var settings = {
            type: "pinchzoom",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv"),
            child: undefined,
            childcount: 0,
            bindsonchild: false,
            touchdetect: "touch release drag pinch",
            zoombracket: [ .3, 3 ],
            zoommultiplier: .2,
            initialposition: undefined,
            currentposition: {
                matrix: undefined,
                x: 0,
                y: 0,
                z: 0
            },
            positionsteps: 30,
            enable_touchbinds: true,
            callback: function(o) {},
            childchange: function(count) {
                var this = this.parent;
                var destination = undefined;
                var animationvars = undefined;
                if (!this._var().yscroll) {
                    destination = this._var().childVars[count].XoffsetfromScroll;
                    animationvars = {
                        scrollTo: {
                            x: destination
                        },
                        ease: Power4.easeOut
                    };
                } else {
                    destination = this._var().childVars[count].YoffsetfromScroll;
                    animationvars = {
                        scrollTo: {
                            y: destination
                        },
                        ease: Power4.easeOut
                    };
                }
                TweenMax.to(this._var().target, .7, animationvars);
            },
            onscroll_callback: function(o) {},
            onscroll_start: function(o) {},
            onscroll_debounce_callback: function(o) {},
            onscroll_debounce_rate: 500,
            ObjHammer: undefined,
            busy: false
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {};
    _thisObject_.prototype.init = function() {
        var self = this;
        this.notify("Trace", "init");
        this._var({
            busy: true
        });
        this._startlisteners();
        this.refresh();
    };
    _thisObject_.prototype.refresh = function() {
        var self = this;
        this._var({
            busy: true
        });
        this.notify("Trace", "refresh");
        this.getandsetneededvars();
        var bindtarget = this._var().bindsonchild ? this._var().child : this._var().target;
        this._var().ObjHammer = Hammer(bindtarget, {
            drag_lock_to_axis: false
        }).on(this._var().touchdetect, this._bindHandler(this));
        this._var({
            busy: false
        });
        this.refreshposition();
        if (this._var().callback != undefined) {
            this._var().callback(this);
        }
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype.getandsetneededvars = function() {
        var self = this;
        var vars = {};
        var thiscount = this._var().childcount;
        vars.child = $(this._var().target.children()[thiscount]);
        this._var(vars);
        return vars;
    };
    _thisObject_.prototype.updatePositionvars = function() {
        var self = this;
        var positionOBJ = {};
        if (this._var().initialposition == undefined) {
            this._var().child.css("webkitTransform", "skew(0)");
        }
        positionOBJ.matrix = this._var().child.css("transform");
        positionOBJ.matrix = positionOBJ.matrix.match(/\(([^)]+)\)/)[1].split(",");
        positionOBJ.z = Number(positionOBJ.matrix[0]);
        positionOBJ.x = Number(positionOBJ.matrix[4]);
        positionOBJ.y = Number(positionOBJ.matrix[5]);
        this._var().currentposition = $.extend(this._var().currentposition, positionOBJ);
        if (this._var().initialposition === undefined) {
            var currentposition = {};
            $.extend(currentposition, positionOBJ);
            this._var().initialposition = currentposition;
        }
        return positionOBJ;
    };
    _thisObject_.prototype.refreshposition = function(getnewposition) {
        var self = this;
        if (getnewposition == undefined) {
            return this.updatePositionvars();
        } else {
            var destination = getnewposition;
            TweenMax.to(this._var().child, .01, {
                scaleX: destination.z,
                scaleY: destination.z,
                y: destination.y,
                x: destination.x,
                ease: Power4.easeOut,
                onComplete: function() {
                    this.updatePositionvars();
                }
            });
        }
    };
    _thisObject_.prototype.resetposition = function() {
        var self = this;
        this.refreshposition(this._var().initialposition);
    };
    _thisObject_.prototype.zoomin = function() {
        var self = this;
        var currentZ = this._var().currentposition.z;
        var targetZ = currentZ + currentZ * this._var().zoommultiplier;
        if (targetZ > this._var().zoombracket[1]) {
            targetZ = this._var().zoombracket[1];
        }
        this.refreshposition({
            z: targetZ
        });
    };
    _thisObject_.prototype.zoomout = function() {
        var self = this;
        var currentZ = this._var().currentposition.z;
        var targetZ = currentZ - currentZ * this._var().zoommultiplier;
        if (targetZ < this._var().zoombracket[0]) {
            targetZ = this._var().zoombracket[0];
        }
        this.refreshposition({
            z: targetZ
        });
    };
    _thisObject_.prototype.moveUp = function() {
        var self = this;
    };
    _thisObject_.prototype._bindHandler = function(this) {
        var internal_position = {
            x: 0,
            y: 0,
            z: 0
        };
        var lastX = internal_position.x;
        var lastY = internal_position.y;
        var lastZ = internal_position.z;
        var lasttype = undefined;
        console.log(":)");
        var eventHandler = function(ev) {
            ev.gesture.preventDefault();
            switch (ev.type) {
              case "touch":
                internal_position = $.extend(internal_position, this.refreshposition());
                lastX = internal_position.x;
                lastY = internal_position.y;
                lastZ = internal_position.z;
                break;

              case "drag":
                var Xdest = lastX + ev.gesture.deltaX;
                var Ydest = lastY + ev.gesture.deltaY;
                this.refreshposition({
                    x: Xdest,
                    y: Ydest
                });
                break;

              case "pinch":
                var targetscale = lastZ - (1 - ev.gesture.scale);
                if (targetscale < .1) {
                    targetscale = .1;
                }
                this.refreshposition({
                    z: targetscale
                });
                break;

              case "release":
                internal_position = $.extend(internal_position, this.refreshposition());
                lastX = internal_position.x;
                lastY = internal_position.y;
                lastZ = internal_position.z;
                var targetscale = this._var().currentposition.z;
                if (targetscale < this._var().zoombracket[0]) {
                    targetscale = this._var().zoombracket[0];
                    TweenMax.to(this._var().child, 2, {
                        scaleX: targetscale,
                        scaleY: targetscale,
                        ease: Elastic.easeOut,
                        onComplete: function() {
                            this.refreshposition();
                        }
                    });
                }
                if (targetscale > this._var().zoombracket[1]) {
                    targetscale = this._var().zoombracket[1];
                    TweenMax.to(this._var().child, 2, {
                        scaleX: targetscale,
                        scaleY: targetscale,
                        ease: Elastic.easeOut,
                        onComplete: function() {
                            this.refreshposition();
                        }
                    });
                }
                ev.gesture.stopDetect();
                break;
            }
        };
        return eventHandler;
    };
    return _thisObject_;
});
