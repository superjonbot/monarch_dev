/*!Last Updated: 03.09.2014[17.43.26] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! binddatatotarget MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.09.2014[17.43.26] by Jonathan Robles*/
define(["jquery","underscore","modules/definitions/standardmodule","hammer"],function(e,d,c,a){function b(g){var f={type:"binddatatotarget",author:"Jonathan Robles",lasteditby:"",target:e("#yourdiv"),autochild:true,childObjects:[e("#yourdiv>div:nth-child(2)"),e("#yourdiv>div:nth-child(2)"),e("#yourdiv>div:nth-child(3)")],dataIN:[{label:"Button 0",onBind:{click:function(h){console.log("click"+this.label);this.target.hide()},mouseover:function(h){console.log("hover")},mouseout:function(h){console.log("out")}}},{label:"Button 1",onBind:{click:function(h){console.log("click"+this.enabled)},mouseover:function(h){console.log("hover")},mouseout:function(h){console.log("out")}}},{label:"Button 2",onHammer:{tap:function(h){alert("tap"+this.label)},hold:function(h){alert("hold")}}},{label:"Button 3",onHammer:{tap:function(h){alert("tap"+this.label)},hold:function(h){alert("hold")}}},{label:"Button 4",onHammer:{tap:function(h){alert("swipe:"+this.label)}}},{label:"Button 5",onHammer:{swipe:function(h){alert("swipe:"+this.label)}}}],busy:false};f=e.extend(f,g);c.call(this,f);return(this)}b.prototype=Object.create(c.prototype);b.prototype._startlisteners=function(){};b.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh()};b.prototype.refresh=function(){this.notify("Trace","refresh");this.setbinds()};b.prototype.kill=function(){this.notify("Trace","kill");this.disableALL();_notify.rem(this._id())};b.prototype.setbinds=function(){var f=this;f._var({busy:true});var g=this._var().dataIN;if(f._var().autochild){f._var({childObjects:f._var().target.children()})}var h=this._var().childObjects;e.each(g,function(j,n){var l=this;this.index=j;this.target=e(h[j]);this.enabled=true;this.bindarray=[];this.hammerarray=[];this.normalkeysindex=undefined;this.hammerkeysindex=undefined;var k=d.keys(n);var m=undefined;var i=undefined;f.normalkeysindex=k.indexOf("onBind");if(f.normalkeysindex!=-1){i=d.keys(n.onBind);e.each(i,function(q,o){var p=function(){if(this.enabled){var r=n.onBind[o];r=d.bind(r,l);r()}};p=d.bind(p,l);l.bindarray[q]=l.target.on(o,p)})}f.hammerkeysindex=k.indexOf("onHammer");if(f.hammerkeysindex!=-1){m=d.keys(n.onHammer);e.each(m,function(q,o){var p=function(){if(this.enabled){var r=n.onHammer[o];r=d.bind(r,l);r()}};p=d.bind(p,l);l.hammerarray[q]=a(l.target.on(o,p))})}});f._var({busy:false})};b.prototype.enable=function(f){this._var().dataIN[f].enabled=true};b.prototype.disable=function(f){this._var().dataIN[f].enabled=false};b.prototype.enableALL=function(){e.each(this._var().dataIN,function(g,f){f.enabled=true})};b.prototype.disableALL=function(){e.each(this._var().dataIN,function(g,f){f.enabled=false})};return(b)});