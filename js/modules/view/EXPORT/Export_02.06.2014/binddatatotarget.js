/*!Last Updated: 02.06.2014[17.10.10] by Jonathan Robles*/



/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
//  binddatatotarget MODULE                                  
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/

 // NOTE: takes a Json bind data and applies to children of the defined target, or you can define children yourself.
 //
 // USAGE: 
 //
/*CL1*/


/*!Last Updated: 02.06.2014[17.10.10] by Jonathan Robles*/
define(["jquery","underscore","modules/definitions/standardmodule","hammer"],function($,_,parentModel,Hammer){function _thizOBJ_(o){var defaults={type:"binddatatotarget",author:"Jonathan Robles",lasteditby:"",target:$("#yourdiv"),autochild:true,childObjects:[$("#yourdiv>div:nth-child(2)"),$("#yourdiv>div:nth-child(2)"),$("#yourdiv>div:nth-child(3)")],dataIN:[{label:"Button 0",onBind:{click:function(e){console.log("click"+this.label);this.target.hide();},mouseover:function(e){console.log("hover");},mouseout:function(e){console.log("out");}}},{label:"Button 1",onBind:{click:function(e){console.log("click"+this.enabled);},mouseover:function(e){console.log("hover");},mouseout:function(e){console.log("out");}}},{label:"Button 2",onHammer:{tap:function(e){alert("tap"+this.label);},hold:function(e){alert("hold");}}},{label:"Button 3",onHammer:{tap:function(e){alert("tap"+this.label);},hold:function(e){alert("hold");}}},{label:"Button 4",onHammer:{tap:function(e){alert("swipe:"+this.label);}}},{label:"Button 5",onHammer:{swipe:function(e){alert("swipe:"+this.label);}}}],busy:false};defaults=$.extend(defaults,o);parentModel.call(this,defaults);return(this);}_thizOBJ_.prototype=Object.create(parentModel.prototype);_thizOBJ_.prototype._startlisteners=function(){};_thizOBJ_.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh();};_thizOBJ_.prototype.refresh=function(){this.notify("Trace","refresh");this.setbinds();};_thizOBJ_.prototype.kill=function(){this.notify("Trace","kill");this.disableALL();_notify.rem(this._id());};_thizOBJ_.prototype.setbinds=function(){var self=this;self._var({busy:true});var arrayref=this._var().dataIN;if(self._var().autochild){self._var({childObjects:self._var().target.children()});}var childObjects=this._var().childObjects;$.each(arrayref,function(index,value){var eachself=this;this.index=index;this.target=$(childObjects[index]);this.enabled=true;this.bindarray=[];this.hammerarray=[];this.normalkeysindex=undefined;this.hammerkeysindex=undefined;var keys=_.keys(value);var hammerkeys=undefined;var normalkeys=undefined;self.normalkeysindex=keys.indexOf("onBind");if(self.normalkeysindex!=-1){normalkeys=_.keys(value.onBind);$.each(normalkeys,function(n_i,bindvalue){var bindfunction=function(){if(this.enabled){var tempfunc=value.onBind[bindvalue];tempfunc=_.bind(tempfunc,eachself);tempfunc();}};bindfunction=_.bind(bindfunction,eachself);eachself.bindarray[n_i]=eachself.target.on(bindvalue,bindfunction);});}self.hammerkeysindex=keys.indexOf("onHammer");if(self.hammerkeysindex!=-1){hammerkeys=_.keys(value.onHammer);$.each(hammerkeys,function(n_i,bindvalue){var bindfunction=function(){if(this.enabled){var tempfunc=value.onHammer[bindvalue];tempfunc=_.bind(tempfunc,eachself);tempfunc();}};bindfunction=_.bind(bindfunction,eachself);eachself.hammerarray[n_i]=Hammer(eachself.target.on(bindvalue,bindfunction));});}});self._var({busy:false});};_thizOBJ_.prototype.enable=function(getIndex){this._var().dataIN[getIndex].enabled=true;};_thizOBJ_.prototype.disable=function(getIndex){this._var().dataIN[getIndex].enabled=false;};_thizOBJ_.prototype.enableALL=function(){$.each(this._var().dataIN,function(d_i,d_v){d_v.enabled=true;});};_thizOBJ_.prototype.disableALL=function(){$.each(this._var().dataIN,function(d_i,d_v){d_v.enabled=false;});};return(_thizOBJ_);});