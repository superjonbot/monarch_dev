/*!Last Updated: 03.31.2014[17.04.50] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! dpcreturn MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.31.2014[17.04.50] by Jonathan Robles*/
define(["jquery","modules/definitions/standardmodule","modules/parsers/dataparser","modules/data/jsontotemplate"],function(e,d,c,a){function b(g){var f={type:"dpcparser",author:"Jonathan Robles",lasteditby:"",file:"data/dpcdemo.json",format:"json",htmlIN:{listitems:'<li><a href="http://www.medscape.fr/dpc/activity/start/{{activityId}}" class="title">{{title}}</a><div class="progressIcon {{progressStatus.name}}"></div><span class="teaser">{{teaser}}</span><div class="byline">{{specialty}}, {{releaseDate}}</div></li>'},data:undefined,dataparserOBJ:undefined,templateOBJ:undefined,busy:false};f=e.extend(f,g);d.call(this,f);return(this)}b.prototype=Object.create(d.prototype);b.prototype._startlisteners=function(){this.notify("Trace","_startlisteners");var h=this;var g=this._id();var f=h._var().dataparserOBJ._id();_notify.add(this._id(),function(){return{onJSONdata:function(i){if(i.senderID==f){h.formatdata(i.data)}},onFormatdata:function(i){if(i.senderID==g){h.displaydata(i.data)}}}}())};b.prototype.init=function(){this.notify("Trace","init");var f=this;f._var().dataparserOBJ=new c();f._var().templateOBJ=new a();f._var().templateOBJ._startlisteners();this._startlisteners();this.refresh()};b.prototype.refresh=function(){var f=this;this.notify("Trace","refresh");f._var({busy:true});f._var().dataparserOBJ._var({file:f._var().file,format:f._var().format});f._var().templateOBJ._var({htmlIN:f._var().htmlIN.listitems,callback:function(h){var g=f._var().data;g+=h;f._var({data:g})}});this.getdata()};b.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id())};b.prototype.getdata=function(){this._var().dataparserOBJ.init()};b.prototype.formatdata=function(g){var f=this;f._var().data="";e.each(g,function(h,i){f._var().templateOBJ._var({dataIN:i});f._var().templateOBJ.refresh()});this.notify("Formatdata",f._var().data)};b.prototype.displaydata=function(){var f=this;f._var().target.empty().html(f._var().data);f._var({busy:false});if(f._var().callback!=undefined){f._var().callback(f._var().data)}};return(b)});