/*!Last Updated: 03.08.2014[02.02.31] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! childreform MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.08.2014[02.02.31] by Jonathan Robles*/
define([ "jquery", "modules/definitions/standardmodule" ], function($, parentObject) {
    function _thisObject_(o) {
        var settings = {
            type: "childreform",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv"),
            childObjects: undefined,
            totalslides: undefined,
            childVars: undefined,
            childVar_setter: function(count, value) {
                var self = this;
                this.count = count;
                this.childOBJ = value;
                this.target = $(value);
                this.Xsize = this.target.outerWidth();
                this.Ysize = this.target.innerHeight();
                this.Xdefault = Math.round(this.target.position().left);
                this.Ydefault = Math.round(this.target.position().top);
                this.matrix = undefined;
                this.x = 0;
                this.y = 0;
                this.z = 0;
            },
            busy: false
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {};
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thisObject_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this._childrentochildObjects();
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype._childrentochildObjects = function() {
        var self = this;
        this._var({
            childObjects: this._var().target.children()
        });
        var childObjects = this._var().childObjects;
        this._var({
            totalslides: childObjects.length,
            childVars: []
        });
        $.each(childObjects, function(count, value) {
            childVar_setter_local = this._var().childVar_setter;
            this._var().childVars.push(new childVar_setter_local(count, value));
        });
        $.each(this._var().childVars, function(count, value) {
            if (value.target.css("transform") == "none") {
                value.target.css("webkitTransform", "skew(0)");
            }
            value.matrix = value.target.css("transform");
            value.matrix = value.matrix.match(/\(([^)]+)\)/)[1].split(",");
            value.z = Number(value.matrix[0]);
            value.x = Number(value.matrix[4]);
            value.y = Number(value.matrix[5]);
        });
    };
    return _thisObject_;
});
