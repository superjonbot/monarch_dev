/*!Last Updated: 03.08.2014[02.02.39] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! navdatatotarget MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.08.2014[02.02.39] by Jonathan Robles*/
define([ "jquery", "underscore", "modules/definitions/standardmodule", "hammer" ], function($, _, parentObject, Hammer) {
    function _thisObject_(o) {
        var settings = {
            type: "navdatatotarget",
            author: "Jonathan Robles",
            lasteditby: "",
            target: $("#yourdiv"),
            dataIN: [ {
                label: "Button 0",
                onsmartClick: function(e) {
                    alert("SMARTclick");
                },
                onBind: {
                    click: function(e) {
                        console.log("click" + this.label);
                    },
                    mouseover: function(e) {
                        console.log("hover");
                    },
                    mouseout: function(e) {
                        console.log("out");
                    }
                }
            }, {
                label: "Button 1",
                onClick: function() {
                    console.log("clicked " + this.label);
                },
                onHover: function() {
                    console.log("hovered " + this.label);
                }
            }, {
                label: "Button 2",
                onHammer: {
                    tap: function(e) {
                        alert("tap" + this.label);
                    },
                    hold: function(e) {
                        alert("hold");
                    }
                }
            }, {
                label: "Button 3",
                onHammer: {
                    tap: function(e) {
                        alert("tap" + this.label);
                    },
                    hold: function(e) {
                        alert("hold");
                    }
                }
            }, {
                label: "Button 4",
                onClick: function() {
                    console.log("clicked " + this.label);
                },
                onHover: function() {
                    console.log("hovered " + this.label);
                }
            }, {
                label: "Button 5",
                onClick: function() {
                    console.log("clicked " + this.label);
                },
                onHover: function() {
                    console.log("hovered " + this.label);
                }
            } ],
            htmlIN: {
                listitems: "<div><span>{{label}}</span>&nbsp;<span></span></div>"
            },
            dataparserOBJ: undefined,
            templateOBJ: undefined,
            busy: false
        };
        settings = $.extend(settings, o);
        parentObject.call(this, settings);
        return this;
    }
    _thisObject_.prototype = Object.create(parentObject.prototype);
    _thisObject_.prototype._startlisteners = function() {};
    _thisObject_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thisObject_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this.sayhi();
    };
    _thisObject_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thisObject_.prototype.sayhi = function() {
        this.notify("Trace", "hi there, from object#" + this._id() + " [ " + this._var().type + " by " + this._var().author + " ] ");
        var self = this;
        var arrayref = this._var().dataIN;
        this._var({
            childObjects: this._var().target.children()
        });
        var childObjects = this._var().childObjects;
        $.each(arrayref, function(count, value) {
            var eachthis = this;
            this.count = count;
            this.bindarray = [];
            this.hammerarray = [];
            this.normalkeyscount = undefined;
            this.hammerkeyscount = undefined;
            console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
            var keys = _.keys(value);
            var hammerkeys = undefined;
            var normalkeys = undefined;
            console.log(keys);
            this.normalkeyscount = keys.countOf("onBind");
            if (this.normalkeyscount != -1) {
                normalkeys = _.keys(value.onBind);
                $.each(normalkeys, function(n_i, bindvalue) {
                    var bindfunction = value.onBind[bindvalue];
                    bindfunction = _.bind(bindfunction, eachthis);
                    eachthis.bindarray[n_i] = $(childObjects[count]).on(bindvalue, bindfunction);
                });
                console.log("normalkeys: " + normalkeys);
            }
            this.hammerkeyscount = keys.countOf("onHammer");
            if (this.hammerkeyscount != -1) {
                hammerkeys = _.keys(value.onHammer);
                $.each(hammerkeys, function(n_i, bindvalue) {
                    var bindfunction = value.onHammer[bindvalue];
                    bindfunction = _.bind(bindfunction, eachthis);
                    eachthis.hammerarray[n_i] = Hammer($(childObjects[count]).on(bindvalue, bindfunction));
                });
            }
            console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
        });
    };
    return _thisObject_;
});
