/*!Last Updated: 03.08.2014[02.02.39] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! navdatatotarget MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.08.2014[02.02.39] by Jonathan Robles*/
define(["jquery","underscore","modules/definitions/standardmodule","hammer"],function(e,d,c,a){function b(g){var f={type:"navdatatotarget",author:"Jonathan Robles",lasteditby:"",target:e("#yourdiv"),dataIN:[{label:"Button 0",onsmartClick:function(h){alert("SMARTclick")},onBind:{click:function(h){console.log("click"+this.label)},mouseover:function(h){console.log("hover")},mouseout:function(h){console.log("out")}}},{label:"Button 1",onClick:function(){console.log("clicked "+this.label)},onHover:function(){console.log("hovered "+this.label)}},{label:"Button 2",onHammer:{tap:function(h){alert("tap"+this.label)},hold:function(h){alert("hold")}}},{label:"Button 3",onHammer:{tap:function(h){alert("tap"+this.label)},hold:function(h){alert("hold")}}},{label:"Button 4",onClick:function(){console.log("clicked "+this.label)},onHover:function(){console.log("hovered "+this.label)}},{label:"Button 5",onClick:function(){console.log("clicked "+this.label)},onHover:function(){console.log("hovered "+this.label)}}],htmlIN:{listitems:"<div><span>{{label}}</span>&nbsp;<span></span></div>"},dataparserOBJ:undefined,templateOBJ:undefined,busy:false};f=e.extend(f,g);c.call(this,f);return(this)}b.prototype=Object.create(c.prototype);b.prototype._startlisteners=function(){};b.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh()};b.prototype.refresh=function(){this.notify("Trace","refresh");this.sayhi()};b.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id())};b.prototype.sayhi=function(){this.notify("Trace",("hi there, from object#"+this._id()+" [ "+this._var().type+" by "+this._var().author+" ] "));var f=this;var g=this._var().dataIN;f._var({childObjects:f._var().target.children()});var h=this._var().childObjects;e.each(g,function(j,n){var l=this;this.index=j;this.bindarray=[];this.hammerarray=[];this.normalkeysindex=undefined;this.hammerkeysindex=undefined;console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");var k=d.keys(n);var m=undefined;var i=undefined;console.log(k);f.normalkeysindex=k.indexOf("onBind");if(f.normalkeysindex!=-1){i=d.keys(n.onBind);e.each(i,function(q,o){var p=n.onBind[o];p=d.bind(p,l);l.bindarray[q]=e(h[j]).on(o,p)});console.log("normalkeys: "+i)}f.hammerkeysindex=k.indexOf("onHammer");if(f.hammerkeysindex!=-1){m=d.keys(n.onHammer);e.each(m,function(q,o){var p=n.onHammer[o];p=d.bind(p,l);l.hammerarray[q]=a(e(h[j]).on(o,p))})}console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")})};return(b)});