/*!Last Updated: 03.31.2014[17.04.48] by Jonathan Robles*/
/*!*/
/*!***********************************************/
/*!  __  __        _                             */
/*! |  \/  |___ __| |___ __ __ _ _ __  ___       */
/*! | |\/| / -_) _` (_-</ _/ _` | '_ \/ -_)      */
/*! |_|  |_\___\__,_/__/\__\__,_| .__/\___|      */
/*!                             |_|              */
/*! childreform MODULE */                           
/*! by Jonathan Robles for WEBMD/Medscape 2013   */
/*!                                              */
/*!***********************************************/
/*!CLX*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!*/
/*!Last Updated: 03.31.2014[17.04.48] by Jonathan Robles*/
define(["jquery","modules/definitions/standardmodule"],function(c,b){function a(e){var d={type:"childreform",author:"Jonathan Robles",lasteditby:"",target:c("#yourdiv"),childObjects:undefined,totalslides:undefined,childVars:undefined,childVar_setter:function(g,h){var f=this;f.index=g;f.childOBJ=h;f.target=c(h);f.Xsize=f.target.outerWidth();f.Ysize=f.target.innerHeight();f.Xdefault=Math.round(f.target.position().left);f.Ydefault=Math.round(f.target.position().top);f.matrix=undefined;f.x=0;f.y=0;f.z=0},busy:false};d=c.extend(d,e);b.call(this,d);return(this)}a.prototype=Object.create(b.prototype);a.prototype._startlisteners=function(){};a.prototype.init=function(){this.notify("Trace","init");this._startlisteners();this.refresh()};a.prototype.refresh=function(){this.notify("Trace","refresh");this._childrentochildObjects()};a.prototype.kill=function(){this.notify("Trace","kill");_notify.rem(this._id())};a.prototype._childrentochildObjects=function(){var d=this;d._var({childObjects:d._var().target.children()});var e=d._var().childObjects;d._var({totalslides:e.length,childVars:[]});c.each(e,function(f,g){childVar_setter_local=d._var().childVar_setter;d._var().childVars.push(new childVar_setter_local(f,g))});c.each(d._var().childVars,function(f,g){if(g.target.css("transform")=="none"){g.target.css("webkitTransform","skew(0)")}g.matrix=g.target.css("transform");g.matrix=g.matrix.match(/\(([^)]+)\)/)[1].split(",");g.z=Number(g.matrix[0]);g.x=Number(g.matrix[4]);g.y=Number(g.matrix[5])})};return(a)});