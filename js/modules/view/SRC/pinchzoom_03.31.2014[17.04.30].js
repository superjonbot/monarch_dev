/**
 * pinchzoom Module
 *
 * Date: 1/7/14 : 1:45 PM
 *
 * NOTE: makes a target object pinch and zoomable
 *
 * USAGE:
 *
 */
define(['jquery', 'underscore', 'modules/definitions/standardmodule', 'hammer', 'tweenmax'], function ($, _, parentModel, Hammer, TweenMax) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'pinchzoom', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)

            target: $('#yourdiv'),  //target object that contains children to control
            child: undefined,  //holder for child object
            childindex:0,
            bindsonchild:false,

            touchdetect: 'touch release drag pinch', //remove options to omit

            zoombracket: [.3, 3],
            zoommultiplier:.2,

            initialposition: undefined,
            currentposition: {
                matrix: undefined,
                x: 0,
                y: 0,
                z: 0
            },
            positionsteps:30,

//            usetouchbinds:true, //adds touchbinds to the target
            enable_touchbinds: true,  //enables/disables touchbinds

            callback: function (o) {
            },  //runs when zoomthing is complete is made

            childchange: function (index) {

                var self = this.parent;
                var destination = undefined;
                var animationvars = undefined;


                if (!self._var().yscroll) {
                    destination = self._var().childVars[index].XoffsetfromScroll;
                    animationvars = {scrollTo: {x: destination}, ease: Power4.easeOut}
                } else {
                    destination = self._var().childVars[index].YoffsetfromScroll;
                    animationvars = {scrollTo: {y: destination}, ease: Power4.easeOut}

                }


                TweenMax.to(self._var().target, .7, animationvars);

            }, //child changing function
            //LIVE SCROLLING CALLBACKS
            onscroll_callback: function (o) {
               // console.log('moving :' + o.currentposition + ' [ ' + o.currentpercent + '% ]')
            }, //on scrolling callback
            onscroll_start: function (o) {
               // console.log('startmove :' + o.currentposition + ' [ ' + o.currentpercent + '% ]')
            },  //on before scrolling callback
            onscroll_debounce_callback: function (o) {
               // console.log('endmove :' + o.currentposition + ' [ ' + o.currentpercent + '% ] @slide: ' + this.currentslide)
            },  //on end scrolling callback
            onscroll_debounce_rate: 500,  //function debouncing rate

            //INTERNAL Variables
            /*            scrollsize:undefined,
             currentposition:undefined,
             currentpercent:undefined,

             targetXsize:undefined,
             targetYsize:undefined,

             targetwidth:undefined,
             targetheight:undefined,
             totalslides:undefined,

             childObjects:undefined,
             childVars:undefined,
             childVar_setter:function(index,value){
             var self = this;

             self.index=index;
             self.childOBJ=value;
             self.target=$(value);
             self.Xsize=self.target.outerWidth();
             self.Ysize=self.target.innerHeight();
             self.Xdefault=Math.round(self.target.position().left);
             self.Ydefault=Math.round(self.target.position().top);
             self.XoffsetfromScroll=undefined;//xoffset;
             self.YoffsetfromScroll=undefined;//yoffset;

             //live variables
             //self.OffsetfromFocus=undefined;
             },*/

            ObjHammer: undefined,

//            isbeingdragged:false, //internal!
//            startmove:true,//internal!
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {
        /*

         this.notify('Trace','_startlisteners');
         var parent=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {

         onJSONdata:function(o){
         if(o.senderID==myID){
         parent._var({data: o.data,busy:false});
         if(parent._var().callback!=undefined){
         parent._var().callback(o.data)
         }
         }
         }
         }
         }());

         */
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init = function () {
        /*
         if(<object>!=exist){
         <object>.create();
         }else{
         <object>.show(); //just show it and start listeners
         };
         * */


        var self = this;

        self.notify('Trace', 'init');
        self._var({busy: true});
        self._startlisteners();//start this module's listeners


        self.refresh();

    };

    _thizOBJ_.prototype.refresh = function () {
        var self = this;
        self._var({busy: true});
        self.notify('Trace', 'refresh');
        self.getandsetneededvars(); //get needed vars
        //put target here it needs to be
        //start handler
        var bindtarget=(self._var().bindsonchild)?self._var().child:self._var().target
        self._var().ObjHammer = Hammer(bindtarget, { drag_lock_to_axis: false }).on(self._var().touchdetect, self._bindHandler(self));


        //   self._var().ObjHammer = Hammer(self._var().target, { drag_lock_to_axis: false }).on(self._var().touchdetect, self._bindHandler(self));


        self._var({busy: false});


        self.refreshposition()

        if (self._var().callback != undefined) {
            self._var().callback(self)
        }
    };

    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.getandsetneededvars = function () {
        var self = this;
        var vars = {}
        var thisindex=self._var().childindex;
        //alert(thisindex)
        vars.child = $(self._var().target.children()[thisindex])

        self._var(vars)
        return vars;
    };

    _thizOBJ_.prototype.updatePositionvars = function() {
        var self=this;
        var positionOBJ = {};

        if (self._var().initialposition == undefined) {
            self._var().child.css('webkitTransform', 'skew(0)'); //give child a matrix
        }
        positionOBJ.matrix = self._var().child.css("transform");
        positionOBJ.matrix = positionOBJ.matrix.match(/\(([^)]+)\)/)[1].split(',');
        positionOBJ.z = Number(positionOBJ.matrix[0]);
        positionOBJ.x = Number(positionOBJ.matrix[4]);
        positionOBJ.y = Number(positionOBJ.matrix[5]);

        //console.log('UPDATED: '+JSON.stringify(positionOBJ))
        self._var().currentposition = $.extend(self._var().currentposition, positionOBJ);
        if (self._var().initialposition === undefined) {
            var currentposition={};
            $.extend(currentposition,positionOBJ);
            self._var().initialposition=currentposition;
        }

        return positionOBJ

    }

    _thizOBJ_.prototype.refreshposition = function (getnewposition) {

        //console.log('refresh')
        var self = this;




        if (getnewposition == undefined) {
            return self.updatePositionvars();
        } else {
            var destination = getnewposition;//$.extend(self._var().currentposition, getnewposition);
             TweenMax.to(self._var().child, .01, {scaleX: destination.z, scaleY: destination.z, y: destination.y, x: destination.x, ease: Power4.easeOut
                   ,onComplete:function(){


                    self.updatePositionvars();

                 }
            });
            //updateMatrix();

        }


    };
    _thizOBJ_.prototype.resetposition = function (){
        var self=this;
        self.refreshposition(self._var().initialposition)

    }

    _thizOBJ_.prototype.zoomin=function(){
        var self=this;
        var currentZ=self._var().currentposition.z
        var targetZ=currentZ+(currentZ*self._var().zoommultiplier);
        if(targetZ>self._var().zoombracket[1]){targetZ=self._var().zoombracket[1]}
        self.refreshposition({z:targetZ});
    }
    _thizOBJ_.prototype.zoomout=function(){
        var self=this;
        var currentZ=self._var().currentposition.z
        var targetZ=currentZ-(currentZ*self._var().zoommultiplier);
        if(targetZ<self._var().zoombracket[0]){targetZ=self._var().zoombracket[0]}
        self.refreshposition({z:targetZ});

    }

    _thizOBJ_.prototype.moveUp=function(){
        var self = this;

    }


//handlers
    _thizOBJ_.prototype._bindHandler = function (self) {
        var internal_position={
            x:0,
            y:0,
            z:0
        };
        var lastX=internal_position.x;
        var lastY=internal_position.y;
        var lastZ=internal_position.z;
        var lasttype=undefined;
        console.log(':)')

        var eventHandler = function (ev) {
            // disable default action
            ev.gesture.preventDefault();



            switch (ev.type) {

                //console.log(ev.type)

                case 'touch':
                    internal_position=$.extend(internal_position, self.refreshposition());
                    lastX=internal_position.x;
                    lastY=internal_position.y;
                    lastZ=internal_position.z;

                    break;
                case 'drag':

                    var Xdest =lastX + ev.gesture.deltaX
                    var Ydest = lastY + ev.gesture.deltaY
                    self.refreshposition({x: Xdest, y: Ydest});

                    break;

                case 'pinch':
                    var targetscale = lastZ - (1 - ev.gesture.scale);
                    if (targetscale < .1) {
                        targetscale = .1
                    }
                    self.refreshposition({z: targetscale});
                    //ev.gesture.stopDetect();
                    break;

                case 'release':
                    internal_position=$.extend(internal_position, self.refreshposition());
                    lastX=internal_position.x;
                    lastY=internal_position.y;
                    lastZ=internal_position.z;

                    //bounce to minimums
                    var targetscale = self._var().currentposition.z;
                    if (targetscale < self._var().zoombracket[0]) {
                        targetscale = self._var().zoombracket[0]
                        TweenMax.to(self._var().child, 2, {scaleX: targetscale, scaleY: targetscale, ease: Power2.easeOut, onComplete: function () {
                            self.refreshposition()
                        }});

                    }
                    if (targetscale > self._var().zoombracket[1]) {
                        targetscale = self._var().zoombracket[1]
                        TweenMax.to(self._var().child, 2, {scaleX: targetscale, scaleY: targetscale, ease: Power2.easeOut, onComplete: function () {
                           self.refreshposition()
                        }});
                    }
                    //console.log('release X:'+lastX+'  Y:'+lastY+'  Z:'+lastZ);
                    ev.gesture.stopDetect();
                    break;

                /*default:
                    alert('!')
                    ev.gesture.stopDetect();*/


            }

            // }

        };

        return  eventHandler;


    };

    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


//knockout blurb
/*
 define(['knockout','modules/view/viewmodels/application4vm'],
 function (ko,appViewModel) {
 ko.applyBindings(new appViewModel());
 });
 */
