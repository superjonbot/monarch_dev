/**
 * navdatatotarget Module
 *
 * Date: 12/13/13 : 11:22 AM
 *
 * NOTE: takes a Json nav data and creates nav and the defined target
 *
 * USAGE:
 *
 */
define(['jquery','underscore', 'modules/definitions/standardmodule','hammer'], function ($,_, parentModel,Hammer) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'navdatatotarget', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)
            target :$('#yourdiv'),
            dataIN:[
                {   label:'Button 0',
                    onsmartClick:function(e){alert('SMARTclick')},
                    onBind:{
                        click:function(e){console.log('click'+this.label)},
                        mouseover:function(e){console.log('hover')},
                        mouseout:function(e){console.log('out')}
                    }
                },
                {   label:'Button 1',
                    onClick:function(){console.log('clicked '+this.label)},
                    onHover:function(){console.log('hovered '+this.label)}

                },
                {   label:'Button 2',
                    onHammer:{
                        tap:function(e){alert('tap'+this.label)/*alert(this,event)*/},
                        hold:function(e){alert('hold')}
                    }

                },
                {   label:'Button 3',
                    onHammer:{
                        tap:function(e){alert('tap'+this.label)/*alert(this,event)*/},
                        hold:function(e){alert('hold')}
                    }

                },
                {   label:'Button 4',
                    onClick:function(){console.log('clicked '+this.label)},
                    onHover:function(){console.log('hovered '+this.label)}

                },
                {   label:'Button 5',
                    onClick:function(){console.log('clicked '+this.label)},
                    onHover:function(){console.log('hovered '+this.label)}

                }

            ],
            htmlIN:{
                listitems:'<div><span>{{label}}</span>&nbsp;<span></span></div>'
            },

            dataparserOBJ:undefined,    //holds dataparser module
            templateOBJ:undefined,      //holds jsontotemplate module
            busy:false





            /*, TRY TO STICK TO THESE COMMON STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             file:'somedir/somefile.ext',
             usenocache:true,
             dataIN:{},
             data:undefined,
             callback:function(){},
             interval:undefined,
             busy:false
             */
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {
        /*

         this.notify('Trace','_startlisteners');
         var parent=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {

         onJSONdata:function(o){
         if(o.senderID==myID){
         parent._var({data: o.data,busy:false});
         if(parent._var().callback!=undefined){
         parent._var().callback(o.data)
         }
         }
         }
         }
         }());

         */
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init = function () {
        /*
         if(<object>!=exist){
         <object>.create();
         }else{
         <object>.show(); //just show it and start listeners
         };
         * */
        this.notify('Trace', 'init');
        this._startlisteners();//start this module's listeners
        this.refresh();

    };

    _thizOBJ_.prototype.refresh = function () {
        this.notify('Trace', 'refresh');
        this.sayhi()
    };

    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.sayhi = function () {
        this.notify('Trace', ('hi there, from object#' + this._id() + ' [ ' + this._var().type + ' by ' + this._var().author + ' ] '));

        var self=this;

        var arrayref=this._var().dataIN;
    //

        self._var({childObjects:self._var().target.children()});
        var childObjects=this._var().childObjects;

        $.each(arrayref,function(index,value){


            var eachself=this;
            this.index=index;
            this.bindarray=[];
            this.hammerarray=[];
            this.normalkeysindex=undefined;
            this.hammerkeysindex=undefined;

            console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-')

            var keys=_.keys(value)
            var hammerkeys=undefined;
            var normalkeys=undefined;
            console.log(keys)

            //bind normalkeys
            self.normalkeysindex=keys.indexOf('onBind');
            if(self.normalkeysindex!=-1){
                normalkeys=_.keys(value.onBind )

                $.each(normalkeys,function(n_i,bindvalue){
                    var bindfunction=value.onBind[bindvalue];

                    bindfunction = _.bind(bindfunction, eachself); //make parent the scope

                    //make bind and store it in the bindarray object
                    eachself.bindarray[n_i]=  $(childObjects[index]).on(bindvalue,bindfunction);

                })

                console.log( 'normalkeys: '+normalkeys);


            }

            //bind hammerkeys
            self.hammerkeysindex=keys.indexOf('onHammer');
            if(self.hammerkeysindex!=-1){
                hammerkeys=_.keys(value.onHammer )

                $.each(hammerkeys,function(n_i,bindvalue){
                    var bindfunction=value.onHammer[bindvalue];

                    bindfunction = _.bind(bindfunction, eachself); //make parent the scope

                    //make bind and store it in the bindarray object
                    eachself.hammerarray[n_i]= Hammer($(childObjects[index]).on(bindvalue, bindfunction));
                    //eachself.bindarray[n_i]=  $(childObjects[index]).on(bindvalue,bindfunction);

                })




              //  console.log( 'hammerkeys: '+hammerkeys);
            }


            console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-')
        })


//alert(this._var().dataIN[0].bindarray)
       // var element = );

  //      var tempobj=this._var().target.children();

// var hammertime = Hammer($(tempobj[0])).on("tap", function() { alert('hello!');  });

        //   alert(typeof Hammer)






    }


    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


//knockout blurb
 /*
 define(['knockout','modules/view/viewmodels/application4vm'],
 function (ko,appViewModel) {
    ko.applyBindings(new appViewModel());
 });
 */
