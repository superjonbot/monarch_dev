/**
 * childreform Module
 *
 * Date: 1/24/14 : 4:42 PM
 *
 * NOTE: takes a bunch of kids, slaps them around a bit, then puts them back
 *
 * USAGE:
 *
 */
define(['jquery', 'modules/definitions/standardmodule'], function ($, parentModel) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'childreform', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)
            target:$('#yourdiv'),  //target object that contains children to control
            childObjects:undefined,
            totalslides:undefined,
            childVars:undefined,

            childVar_setter:function(index,value){
                var self = this;

                self.index=index;
                self.childOBJ=value;
                self.target=$(value);
                self.Xsize=self.target.outerWidth();
                self.Ysize=self.target.innerHeight();
                self.Xdefault=Math.round(self.target.position().left);
                self.Ydefault=Math.round(self.target.position().top);

                self.matrix= undefined;
                self.x= 0;
                self.y= 0;
                self.z= 0;
            },





                //live variables
                //self.OffsetfromFocus=undefined;



            /*, TRY TO STICK TO THESE COMMON STANDARDS IF YOUR MODULE USES THEM
             target:$('#yourdiv'),
             file:'somedir/somefile.ext',
             usenocache:true,
             dataIN:{},
             data:undefined,
             callback:function(){},
             interval:undefined,

             */
            busy:false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {
        /*

         this.notify('Trace','_startlisteners');
         var parent=this;
         var myID=  this._id();
         _notify.add(this._id(), function() {

         return {

         onJSONdata:function(o){
         if(o.senderID==myID){
         parent._var({data: o.data,busy:false});
         if(parent._var().callback!=undefined){
         parent._var().callback(o.data)
         }
         }
         }
         }
         }());

         */
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init = function () {
        /*
         if(<object>!=exist){
         <object>.create();
         }else{
         <object>.show(); //just show it and start listeners
         };
         * */
        this.notify('Trace', 'init');
        this._startlisteners();//start this module's listeners
        this.refresh();

    };

    _thizOBJ_.prototype.refresh = function () {
        this.notify('Trace', 'refresh');
        this._childrentochildObjects();
    };

    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype._childrentochildObjects = function () {
        var self=this;
        self._var({childObjects:self._var().target.children()});
        var childObjects=self._var().childObjects;

        self._var({
            totalslides:childObjects.length,
            childVars:[]
        });

        $.each(childObjects,function(index, value ){
            childVar_setter_local=self._var().childVar_setter;
            self._var().childVars.push(new childVar_setter_local(index,value))

        });

        $.each(self._var().childVars,function(index, value ){

          if(value.target.css("transform")=='none'){value.target.css('webkitTransform', 'skew(0)')}

          value.matrix = value.target.css("transform");
          value.matrix = value.matrix.match(/\(([^)]+)\)/)[1].split(',')
          value.z = Number(value.matrix[0]);
          value.x = Number(value.matrix[4]);
          value.y = Number(value.matrix[5]);



        });


    }


    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


//knockout blurb
 /*
 define(['knockout','modules/view/viewmodels/application4vm'],
 function (ko,appViewModel) {
    ko.applyBindings(new appViewModel());
 });
 */
