module.exports = function(grunt) {

    var projectName = 'Lifetime SliderSet';    
    var versionnumber = '<%= pkg.version %>.' + (Math.floor(new Date().getTime() / 100000000000) / 10);
    var destinationFolder = '../exports/lifetime_v' + versionnumber + '/';
    var myTaskList = ['jshint', 'requirejs',  'uglify', 'jsbeautifier', 'usebanner','copy', 'notify:complete'];
    var projectFolder = '../../devbox_maids/webroot/mlt-d7/profiles/mylifetime_com/themes/custom/ltv/js/';  
    var thirdpartyInc = '/* stuff */';
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('../package.json'),

       returnBanner:function(src, filepath) {
            var complete_src = grunt.file.expand(src); 
            var final_name = '';
            // some string manipulations to get your the format you want 
        for (var i = 0; i < complete_src.length; i++) {
            complete_src[i] = complete_src[i].substring(complete_src[i].lastIndexOf('/') + 1, complete_src[i].length );
        }
        final_name = complete_src.join('-');

        var final_banner='';
        final_banner+='/*! '+projectName+' : '+final_name+' */';
        return final_banner;
              },    //This function places the filename at the top of the file

        banner: '/*! codebase: <%= pkg.name %> v' + versionnumber + ' by Jonathan Robles */\n' +
                '/*! built:<%= grunt.template.today("mm-dd-yyyy [h:MM:ssTT]") %> */\n'+
                '/*! -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */\n\n'+
                '/*! Prerequisites: jQuery, Underscore */\n\n'+
                '/*! Third Party Includes [start] */\n'+ thirdpartyInc+
                '\n/*! Third Party Includes [end] */\n',
        jshint: {
            // define the files to lint
            files: ['Gruntfile.js'],
            // configure JSHint (documented at http://www.jshint.com/docs/)
            options: {
                // more options here if you want to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true
                }
            }
        },

        requirejs: {
            lifetime_require: {
                options: {
                    baseUrl: ".",

                    mainConfigFile: 'libraries/etc/env/config_grunt.js',
                    optimize: 'none',
                    preserveLicenseComments: false,
                    name: "libraries/etc/required.js",
                    out: "../exports/lifetime_temp/lifetime_require.js",
                    paths: {
                        almondLib: 'libraries/almond/almond',
                        requireLib: 'libraries/require/require',
                        requireConfig: 'libraries/etc/env/config_grunt',
                        underscore: 'libraries/underscore0x/underscore',
                        jquery: "libraries/jquery0x/jquery",
                        omniture:"libraries/analytics/omniture.js"
                    },
                    include: ['almondLib', 'requireConfig']
                }
            },
            lifetime_slider: {
                options: {
                    baseUrl: ".",
                    mainConfigFile: 'libraries/etc/env/config_grunt.js',
                    optimize: 'none',
                    preserveLicenseComments: false,
                    name: "instances/slideshow_lifetimeslider_instance.js",
                    out: "../exports/lifetime_temp/lifetime_custom_carousel.js",
                    paths: {
                        almondLib: 'libraries/almond/almond',
                        requireLib: 'libraries/require/require',
                        requiredLib: 'libraries/etc/required',
                        jquery: "libraries/jquery0x/jquery",
                        modernizr: "libraries/modernizr0x/modernizr"
                    }
                }
            }
        },



        //Minify!!!
        uglify: {
            beautify: {
                options: {
                    banner: '<%= banner %>',
                    beautify: true,
                    mangle: false,
                    compress: false
                },

                src: '../exports/lifetime_temp/*.js',
                dest: destinationFolder,
                expand: true, // allow dynamic building
                flatten: true // remove all unnecessary nesting
            },
            minifier: {
                options: {
                    banner: '<%= banner %>',
                    beautify: false,
                    mangle: true,
                    compress: true
                },

                src: '../exports/lifetime_temp/*.js',
                dest: destinationFolder,
                expand: true, // allow dynamic building
                flatten: true, // remove all unnecessary nesting
                ext: '_min.js' // replace .js to .min.js
            }
        },


        jsbeautifier: {
            files: [(destinationFolder+'*.js'), ('!'+destinationFolder+'*_min.js')]
        },



usebanner: {
    taskName: {
      options: {
        position: 'top',
        linebreak: true ,
        process: '<%= returnBanner %>'


      },
      files: {
        src: [ (destinationFolder+'*') ]
      }
    }
  },


        copy: {
            main: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        flatten: true,
                        src: ['../exports/lifetime_v' + versionnumber + '/*'],
                        dest: projectFolder,
                        filter: 'isFile'
                    }
                ],
            },
        },

        watch: {
            scripts: {
                files: ['libraries/etc/required.js', 'instances/slideshow_lifetimeslider_instance.js'],
                tasks: myTaskList,
                options: {
                    spawn: true,
                    livereload: true
                }
            }


        },
        notify_hooks: {
           options: {
              enabled: true,
              max_jshint_notifications: 5, // maximum number of notifications from jshint output
              title: projectName, // defaults to the name in package.json, or will use project directory's name
              success: false, // whether successful grunt executions should be notified automatically
              duration: 3 // the duration of notification in seconds, for `notify-send only
           }
        },
        notify: {
            complete: {
                options: {
                    title: '<%= notify_hooks.options.title %>',  // optional
                    message: 'DONE!'
                }
            }
        }

    });

    // PLUGINS
    grunt.loadNpmTasks('grunt-banner'); //https://github.com/gruntjs/grunt-contrib-concat
    //grunt.loadNpmTasks('grunt-contrib-concat'); //https://github.com/gruntjs/grunt-contrib-concat
    grunt.loadNpmTasks('grunt-contrib-copy'); //https://github.com/gruntjs/grunt-contrib-copy
    //grunt.loadNpmTasks('grunt-contrib-cssmin'); //https://github.com/gruntjs/grunt-contrib-cssmin
    //grunt.loadNpmTasks('grunt-contrib-imagemin'); //https://github.com/gruntjs/grunt-contrib-imagemin
    //grunt.loadNpmTasks('grunt-contrib-jasmine');   //https://github.com/gruntjs/grunt-contrib-jasmine
    grunt.loadNpmTasks('grunt-contrib-jshint'); //https://github.com/gruntjs/grunt-contrib-jshint
    //grunt.loadNpmTasks('grunt-contrib-qunit'); //https://github.com/gruntjs/grunt-contrib-qunit
    grunt.loadNpmTasks('grunt-contrib-requirejs'); //https://github.com/gruntjs/grunt-contrib-requirejs
    grunt.loadNpmTasks('grunt-contrib-uglify');    //https://github.com/gruntjs/grunt-contrib-uglify
    grunt.loadNpmTasks('grunt-contrib-watch');    //https://github.com/gruntjs/grunt-contrib-watch
    grunt.loadNpmTasks('grunt-jsbeautifier');  //https://github.com/vkadam/grunt-jsbeautifier
    //grunt.loadNpmTasks('grunt-mocha');    //https://github.com/kmiyashiro/grunt-mocha
    //grunt.loadNpmTasks('grunt-mocha-phantomjs');  //https://github.com/jdcataldo/grunt-mocha-phantomjs
    grunt.loadNpmTasks('grunt-notify');   //https://github.com/dylang/grunt-notify
    //grunt.loadNpmTasks('grunt-shell');    //https://github.com/sindresorhus/grunt-shell
    //grunt.loadNpmTasks('grunt-ssh');    //https://github.com/chuckmo/grunt-ssh

    // run right away
    grunt.task.run('notify_hooks');  
    // Default task(s).
    grunt.registerTask('default', myTaskList);

};