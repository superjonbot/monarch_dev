/**
 * @license almond 0.3.0 Copyright (c) 2011-2014, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/jrburke/almond for details
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*jslint sloppy: true */
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex,
            foundI, foundStarMap, starI, i, j, part,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name && name.charAt(0) === ".") {
            //If have a base name, try to normalize against it,
            //otherwise, assume it is a top-level require that will
            //be relative to baseUrl in the end.
            if (baseName) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that "directory" and not name of the baseName's
                //module. For instance, baseName of "one/two/three", maps to
                //"one/two/three.js", but we want the directory, "one/two" for
                //this normalization.
                baseParts = baseParts.slice(0, baseParts.length - 1);
                name = name.split('/');
                lastIndex = name.length - 1;

                // Node .js allowance:
                if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                    name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
                }

                name = baseParts.concat(name);

                //start trimDots
                for (i = 0; i < name.length; i += 1) {
                    part = name[i];
                    if (part === ".") {
                        name.splice(i, 1);
                        i -= 1;
                    } else if (part === "..") {
                        if (i === 1 && (name[2] === '..' || name[0] === '..')) {
                            //End of the line. Keep at least one non-dot
                            //path segment at the front so it can be mapped
                            //correctly to disk. Otherwise, there is likely
                            //no path mapping for a path starting with '..'.
                            //This can still fail, but catches the most reasonable
                            //uses of ..
                            break;
                        } else if (i > 0) {
                            name.splice(i - 1, 2);
                            i -= 2;
                        }
                    }
                }
                //end trimDots

                name = name.join("/");
            } else if (name.indexOf('./') === 0) {
                // No baseName, so this is ID is resolved relative
                // to baseUrl, pull off the leading dot.
                name = name.substring(2);
            }
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            var args = aps.call(arguments, 0);

            //If first arg is not require('string'), and there is only
            //one arg, it is the array form without a callback. Insert
            //a null so that the following concat is correct.
            if (typeof args[0] !== 'string' && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relName) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relName));
            } else {
                name = normalize(name, relName);
            }
        } else {
            name = normalize(name, relName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relName);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, callback).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }

            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        return req(cfg);
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());

define("almondLib", function(){});

/* Modernizr 2.7.1 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-fontface-cssanimations-csstransforms-csstransforms3d-csstransitions-canvas-audio-video-webworkers-geolocation-inlinesvg-svg-svgclippaths-touch-shiv-mq-cssclasses-teststyles-testprop-testallprops-prefixes-domprefixes-load
 */
;window.Modernizr=function(a,b,c){function B(a){j.cssText=a}function C(a,b){return B(m.join(a+";")+(b||""))}function D(a,b){return typeof a===b}function E(a,b){return!!~(""+a).indexOf(b)}function F(a,b){for(var d in a){var e=a[d];if(!E(e,"-")&&j[e]!==c)return b=="pfx"?e:!0}return!1}function G(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:D(f,"function")?f.bind(d||b):f}return!1}function H(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+o.join(d+" ")+d).split(" ");return D(b,"string")||D(b,"undefined")?F(e,b):(e=(a+" "+p.join(d+" ")+d).split(" "),G(e,b,c))}var d="2.7.1",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k,l={}.toString,m=" -webkit- -moz- -o- -ms- ".split(" "),n="Webkit Moz O ms",o=n.split(" "),p=n.toLowerCase().split(" "),q={svg:"http://www.w3.org/2000/svg"},r={},s={},t={},u=[],v=u.slice,w,x=function(a,c,d,e){var f,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),l.appendChild(j);return f=["&#173;",'<style id="s',h,'">',a,"</style>"].join(""),l.id=h,(m?l:n).innerHTML+=f,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=g.style.overflow,g.style.overflow="hidden",g.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),g.style.overflow=k),!!i},y=function(b){var c=a.matchMedia||a.msMatchMedia;if(c)return c(b).matches;var d;return x("@media "+b+" { #"+h+" { position: absolute; } }",function(b){d=(a.getComputedStyle?getComputedStyle(b,null):b.currentStyle)["position"]=="absolute"}),d},z={}.hasOwnProperty,A;!D(z,"undefined")&&!D(z.call,"undefined")?A=function(a,b){return z.call(a,b)}:A=function(a,b){return b in a&&D(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=v.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(v.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(v.call(arguments)))};return e}),r.canvas=function(){var a=b.createElement("canvas");return!!a.getContext&&!!a.getContext("2d")},r.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:x(["@media (",m.join("touch-enabled),("),h,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c},r.geolocation=function(){return"geolocation"in navigator},r.cssanimations=function(){return H("animationName")},r.csstransforms=function(){return!!H("transform")},r.csstransforms3d=function(){var a=!!H("perspective");return a&&"webkitPerspective"in g.style&&x("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}",function(b,c){a=b.offsetLeft===9&&b.offsetHeight===3}),a},r.csstransitions=function(){return H("transition")},r.fontface=function(){var a;return x('@font-face {font-family:"font";src:url("https://")}',function(c,d){var e=b.getElementById("smodernizr"),f=e.sheet||e.styleSheet,g=f?f.cssRules&&f.cssRules[0]?f.cssRules[0].cssText:f.cssText||"":"";a=/src/i.test(g)&&g.indexOf(d.split(" ")[0])===0}),a},r.video=function(){var a=b.createElement("video"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),c.h264=a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),c.webm=a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,"")}catch(d){}return c},r.audio=function(){var a=b.createElement("audio"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,""),c.mp3=a.canPlayType("audio/mpeg;").replace(/^no$/,""),c.wav=a.canPlayType('audio/wav; codecs="1"').replace(/^no$/,""),c.m4a=(a.canPlayType("audio/x-m4a;")||a.canPlayType("audio/aac;")).replace(/^no$/,"")}catch(d){}return c},r.webworkers=function(){return!!a.Worker},r.svg=function(){return!!b.createElementNS&&!!b.createElementNS(q.svg,"svg").createSVGRect},r.inlinesvg=function(){var a=b.createElement("div");return a.innerHTML="<svg/>",(a.firstChild&&a.firstChild.namespaceURI)==q.svg},r.svgclippaths=function(){return!!b.createElementNS&&/SVGClipPath/.test(l.call(b.createElementNS(q.svg,"clipPath")))};for(var I in r)A(r,I)&&(w=I.toLowerCase(),e[w]=r[I](),u.push((e[w]?"":"no-")+w));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)A(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},B(""),i=k=null,function(a,b){function l(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function m(){var a=s.elements;return typeof a=="string"?a.split(" "):a}function n(a){var b=j[a[h]];return b||(b={},i++,a[h]=i,j[i]=b),b}function o(a,c,d){c||(c=b);if(k)return c.createElement(a);d||(d=n(c));var g;return d.cache[a]?g=d.cache[a].cloneNode():f.test(a)?g=(d.cache[a]=d.createElem(a)).cloneNode():g=d.createElem(a),g.canHaveChildren&&!e.test(a)&&!g.tagUrn?d.frag.appendChild(g):g}function p(a,c){a||(a=b);if(k)return a.createDocumentFragment();c=c||n(a);var d=c.frag.cloneNode(),e=0,f=m(),g=f.length;for(;e<g;e++)d.createElement(f[e]);return d}function q(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return s.shivMethods?o(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/[\w\-]+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(s,b.frag)}function r(a){a||(a=b);var c=n(a);return s.shivCSS&&!g&&!c.hasCSS&&(c.hasCSS=!!l(a,"article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")),k||q(a,c),a}var c="3.7.0",d=a.html5||{},e=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,f=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,g,h="_html5shiv",i=0,j={},k;(function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",g="hidden"in a,k=a.childNodes.length==1||function(){b.createElement("a");var a=b.createDocumentFragment();return typeof a.cloneNode=="undefined"||typeof a.createDocumentFragment=="undefined"||typeof a.createElement=="undefined"}()}catch(c){g=!0,k=!0}})();var s={elements:d.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:c,shivCSS:d.shivCSS!==!1,supportsUnknownElements:k,shivMethods:d.shivMethods!==!1,type:"default",shivDocument:r,createElement:o,createDocumentFragment:p};a.html5=s,r(b)}(this,b),e._version=d,e._prefixes=m,e._domPrefixes=p,e._cssomPrefixes=o,e.mq=y,e.testProp=function(a){return F([a])},e.testAllProps=H,e.testStyles=x,g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+u.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};
define("modernizr", (function (global) {
    return function () {
        var ret, fn;
        return ret || global.Modernizr;
    };
}(this)));

//Create Object.create shim
if(typeof Object.create !=='function'){
    Object.create = function (o){
        function F(){}
        F.prototype = o;
        return new F();
    };
}


/**
 * requestAnimationFrame and console polyfill
 */
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
            window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

(function() {
    if (!window.console) {
        window.console = {};
    } else {
        console.log('use http://[url]?debug=true for console.logs');
    }
    // union of Chrome, FF, IE, and Safari console methods
    var m = [
        "log", "info", "warn", "error", "debug", "trace", "dir", "group",
        "groupCollapsed", "groupEnd", "time", "timeEnd", "profile", "profileEnd",
        "dirxml", "assert", "count", "markTimeline", "timeStamp", "clear"
    ];
    // define undefined methods as noops to prevent errors
    for (var i = 0; i < m.length; i++) {
        if (!window.console[m[i]]) {
            window.console[m[i]] = function() {};
        }
    }





})();

/*global tools*/
function htmlEncode(value){
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}

function htmlDecode(value){
    return $('<div/>').html(value).text();
}

var tracecount=0;
/* example : trace('placeAllProgPoints!','#fff','#000','#counter');  */
function trace(datastring,color,backgroundcolor,targetDiv){

    if(targetDiv==undefined) {
        tracecount++;
    };
    //var datastring=o.data;
    if(typeof datastring=='object'){datastring=JSON.stringify(datastring)};
    var buildstringNS=buildstring='!['+String(tracecount)+'] > '+ datastring;



    if(targetDiv!=undefined){
        buildstring='<span style="color:'+color+'; background-color:'+backgroundcolor+';">'+htmlEncode(datastring)+'</span>';
    }
    else if(backgroundcolor!=undefined){
        buildstring='<span style="color:'+color+'; background-color:'+backgroundcolor+';">'+htmlEncode(buildstring)+'</span><br>';
    }
    else if(color!=undefined){
        buildstring='<span style="color:'+color+';">'+htmlEncode(buildstring)+'</span><br>';
    } else {
        buildstring=htmlEncode(buildstring)+'</br>';
    }

    if(targetDiv!=undefined){
        $(targetDiv).empty().prepend(buildstring);
    }else{
        console.log(buildstringNS);
        $('.debugConsole').prepend(buildstring);
    }


}

//Mediator Object
_notify = function() {
    var debug = function() {
        // console.log or air.trace as desired
    };

    var components = {};

    var broadcast = function(event, args, source) {
        if (!event) {
            return;
        }
        args = args || [];
        //debug(["Mediator received", event, args].join(' '));
        for (var c in components) {
            if (typeof components[c]["on" + event] == "function") {
                try {
                    //debug("Mediator calling " + event + " on " + c);
                    source = source || components[c];
                    components[c]["on" + event].apply(source, args);
                } catch (err) {
                    debug(["Mediator error.", event, args, source, err].join(' '));
                }
            }
        }
    };

    var addComponent = function(name, component, replaceDuplicate) {
        if (name in components) {
            if (replaceDuplicate) {
                removeComponent(name);
            } else {
                throw new Error('The object: ' + name +' has already applied listeners');
            }
        }
        components[name] = component;
    };

    var removeComponent = function(name) {
        if (name in components) {
            delete components[name];
        }
    };

    var getComponent = function(name) {
        return components[name]; // undefined if component has not been added
    };

    var contains = function(name) {
        return (name in components);
    };

    return {
        name      : "Mediator",
        broadcast : broadcast,
        add       : addComponent,
        rem       : removeComponent,
        get       : getComponent,
        has       : contains
    };
}();

//Set default Listeners
_notify.add('global', function() {
        var tracecount = 0;
        var alertcount = 0;

        return {
            onTrace: function(o) {
                tracecount++;
                var datastring=o.data;
                if(typeof datastring=='object'){datastring=JSON.stringify(datastring)}
                var buildstring='['+String(tracecount)+'] ('+ o.senderID+':'+ o.sendertype+':'+ o.notifyscope+') > '+ datastring;
                //if(_global$.islocalhost)console.log(buildstring);

                //$('.debugConsole').prepend(htmlEncode(buildstring)+'</br>');

                if(_global$.getQuery('debug'))console.log(buildstring);
            },
            onAlert: function(o) {
                alertcount++;
                var datastring=o.data;
                if(typeof datastring=='object'){datastring=JSON.stringify(datastring)}
                var buildstring='['+String(tracecount)+'] ('+ o.senderID+':'+ o.sendertype+':'+ o.notifyscope+') > '+ datastring;
                if(_global$.getQuery('debug')){alert(buildstring)};
            },
            onInitialize:function(o){
                if(_global$.getQuery('debug'))console.log('Created Instance #'+ o.senderID+' type:'+o.sendertype+' by '+ o.data.author+' [notifyscope:'+ o.notifyscope+']');

            }

        }
    }());




//Begin Global Singleton
require(['modernizr'],function(Modernizr){


    function Monarch_Base() {

        if ( arguments.callee._singletonInstance )
            return arguments.callee._singletonInstance;
        arguments.callee._singletonInstance = this;

        //_global$
        var parent=this;




        //this.isMobile=Modernizr.mq('only screen and (min-device-width : 320px) and (max-device-width : 480px)');
        this.isTouch=Modernizr.touch;   //(equivalent to .touch)
        /**/

        this.host=location.host;
        this.path=location.pathname;
        this.islocalhost=false;
        if(   (this.host.indexOf('localhost')!=-1) || (this.host.indexOf('127.0.0.1')!=-1)   ){
            this.islocalhost=true;
           // $('html').addClass('localhost');
        }






        //global jsonp return redirector
        this.jsonpReturn=function(id,type,notifyscope){
            return function(data){
                _notify.broadcast(type, [{
                    senderID:id,
                    sendertype:type,
                    notifyscope:notifyscope,
                    data:data
                }]);






            }

        }


        //Utility Functions
        this._isupdated=function(value,currentVal,notify){
           if(parent[value]!=currentVal){
               var Oldvalue=parent[value];
               parent[value]=currentVal;

               //alert(currentVal)

               if(Oldvalue!=undefined){
                   this.notify(notify,parent[value]);
                    //console.log(notify+' : '+parent[value])
               }


           }
       }; //for orientation/screen/document/window
        this.notify=function(type,data){
            _notify.broadcast(type, [{
                senderID:'global',
                sendertype:'global',
                notifyscope:'global',
                data:data
            }]);
        }; //notifier
        this.getQuery=function(variable){

            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    var returncandidate=pair[1];
                    if(returncandidate=='true'||returncandidate=='yes'||returncandidate=='y'||returncandidate=='t'){returncandidate=true;}
                    if(returncandidate=='false'||returncandidate=='no'||returncandidate=='n'||returncandidate=='f'){returncandidate=false;}
                    return  returncandidate;
                }
            }
            return false;

        };

        this.getHash=function(variable){

            return window.location.hash
        };

        this.setCookie=function(key, value) {
            var expires = new Date();
            expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
            document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        }

        this.getCookie=function(key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null;
        }







    };

    window._global$=new Monarch_Base();


})

;
define("libraries/etc/required_chromecast.js", function(){});

/* Global Config */
var AETN = AETN || {};

AETN.whatisThis='AETV custom receiver';
AETN.apiKey='599b88193e3f5a6c96f7a362a75b34ad';

//Adobe omniture
AETN.adobeTrackingServer='metrics.aetn.com';
AETN.adobeTrackingSecureServer='smetrics.aetn.com';
AETN.adobeTrackingSuiteID='aetnwatchglobal';
AETN.adobeMCORDID='08AC467352782D0D0A490D45@AdobeOrg';

//Krux
// stuff
//hls_mdialog
AETN.mDialog_playoptions='switch=hls&assetTypes=medium_video_ak&mbr=true&metafile=false';//switch=hls&assetTypes=medium_video_ak&mbr=true&metafile=false';



AETN.omnitureDefaults={
    platform:"Chromecast"/*,
    videocalltype:undefined,
videocliptitle:"XYZ:[VideoCategory]:NoTitle",
videocategory:"XYZ:NoCategory",
videolf:undefined,
videoseason:"None",
videotv:"TV",
videochapter:"None",
videoepisode:"None",
videosubcategory:"None",
videofastfollow:undefined,
videoautoplay:"NonAutoplay",
videoduration:"None",
videofullscreen:"Fullscreen",
videoadvertiser:"UnknownAdvertiser",
videoadduration:"UnknownLength",
videoadtitle:"[AdvertiserName]:UnknownAdTitle",
videorequiresauthentication:"0",
videopplid:"None"*/
};
define("globalConfig", function(){});

/* Environment Config */
AETN.environment='qa';
AETN.signinURL='http://dev-signature.video.aetndigital.com?url=';
AETN.proxyWithSigninURL='https://dev-chromecast.ott.aetnd.com/proxy.php?mode=JSON&full_status=1&full_headers=1&url=';
define("environmentConfig", function(){});

/* Brand Config */
AETN.brandID='fyi';
AETN.brandID_omnitureName='FYI'; // for omniture, do not change!
AETN.streamActivityKey='64524e36b96b6f547b3e9dce101bf8bd';//mDialog/Freewheel
AETN.applicationKey='2cd4ad341d4d86c3fb0a764506482f5d';//AETN.brandID+'_'+AETN.environment+AETN.streamActivityKey;//mDialog/Freewheel
//AETN.titleFeed='http://dev.mediaservice.aetndigital.com/SDK_v2/wa/show_titles';
AETN.titleFeedNew='http://dev-feeds.video.aetnd.com/api/fyi/videos?filter%5Bid%5D=';
//Environmental Cases for this brand










switch (AETN.environment) {
    case 'dev':
        AETN.applicationID = "9CB42BEA";
        AETN.adobeReportSuiteID='aetnchromecastfyidev';
        break;
    case 'qa':
        AETN.applicationID = "297FB284";
        AETN.adobeReportSuiteID='aetnchromecastfyidev';
        break;
    default: //production
        AETN.applicationID = "D18F7F6A";
        AETN.streamActivityKey=undefined;
        AETN.adobeReportSuiteID='aetnchromecastfyi';
}

AETN.namespace='urn:x-cast:com.'+AETN.brandID+'_'+AETN.environment+'.chromecast';
define("brandConfig", function(){});

/* This configuration is for require plus grunt*/
require.config({
    baseUrl:".",
    paths : {
        underscore:'libraries/underscore/underscore',
        jquery : "libraries/jquery1x/jquery-1.10.2",
        modernizr:"libraries/modernizr/modernizr.custom.43687",
        x2js:"libraries/parsing/x2js/xml2json",
        knockout:"libraries/knockout/knockout-2.3.0",
        i18n:"libraries/require/i18n",
        dust:"libraries/dust/dist/dust-full-2.0.0.min",

        tweenmax:"libraries/greensocks/TweenMax.min",
        tweenlite:"libraries/greensocks/TweenLite.min",
        timelinelite:"libraries/greensocks/TimelineLite.min",
        timelinemax:"libraries/greensocks/TimelineMax.min",

        tweenmax_1_11_8:"libraries/greensocks_1.11.8/TweenMax",
        tweenlite_1_11_8:"libraries/greensocks_1.11.8/TweenLite",
        timelinelite_1_11_8:"libraries/greensocks_1.11.8/TimelineLite",
        timelinemax_1_11_8:"libraries/greensocks_1.11.8/TimelineMax",
        draggable_1_11_8:"libraries/greensocks_1.11.8/utils/Draggable.js",

        scrollto:"libraries/greensocks/plugins/ScrollToPlugin.min",
        hammer:"libraries/hammer/jquery.hammer",
        angular:"libraries/angular/angular.min",
        angularresource:"libraries/angular/angular-resource.min",

        oboe:"libraries/oboe/oboe-browser"

    },
    shim:{
        'underscore': {
            exports: '_'
        },
        'modernizr': {
            exports: 'Modernizr'
        },
        'x2js': {
            exports: 'X2JS'
        },
        'dust': {
            exports: 'dust'
        },


        'tweenmax': {
            exports: 'TweenMax'
        },
        'tweenlite': {
            exports: 'TweenLite'
        },
        'timelinelite': {
            exports: 'TimelineLite'
        },
        'timelinemax': {
            exports: 'TimelineMax'
        },


        'tweenmax_1_11_8': {
            exports: 'TweenMax'
        },
        'tweenlite_1_11_8': {
            exports: 'TweenLite'
        },
        'timelinelite_1_11_8': {
            exports: 'TimelineLite'
        },
        'timelinemax_1_11_8': {
            exports: 'TimelineMax'
        },


        'hammer': {
            exports: 'Hammer'
        },
        'scrollto': {
            exports: 'Scrollto'
        },
        "angular": {
            exports: "angular"
        },
        "angular-resource": {
            deps: ["angular"]
        }
    }
    //,config:{i18n:{locale:'fr-fr'}} //uncomment to test/force internationalization
});




define("requirejsConfig", function(){});

// Copyright 2014 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


(function() {
  'use strict';

/**
 * Media source root URL
 **/
var MEDIA_SOURCE_ROOT = '';//http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/';

/**
 * Media source URL JSON
 **/
var MEDIA_SOURCE_URL = '';//http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/videos.json';

/**
 * Width of progress bar in pixel
 **/
var PROGRESS_BAR_WIDTH = 600;

/**
 * Constants of states for Chromecast device 
 **/
var DEVICE_STATE = {
  'IDLE' : 0, 
  'ACTIVE' : 1, 
  'WARNING' : 2, 
  'ERROR' : 3,
};

/**
 * Constants of states for CastPlayer 
 **/
var PLAYER_STATE = {
  'IDLE' : 'IDLE', 
  'LOADING' : 'LOADING', 
  'LOADED' : 'LOADED', 
  'PLAYING' : 'PLAYING',
  'PAUSED' : 'PAUSED',
  'STOPPED' : 'STOPPED',
  'SEEKING' : 'SEEKING',
  'ERROR' : 'ERROR'
};

/**
 * Cast player object
 * main variables:
 *  - deviceState for Cast mode: 
 *    IDLE: Default state indicating that Cast extension is installed, but showing no current activity
 *    ACTIVE: Shown when Chrome has one or more local activities running on a receiver
 *    WARNING: Shown when the device is actively being used, but when one or more issues have occurred
 *    ERROR: Should not normally occur, but shown when there is a failure 
 *  - Cast player variables for controlling Cast mode media playback 
 *  - Local player variables for controlling local mode media playbacks
 *  - Current media variables for transition between Cast and local modes
 */
var CastPlayer = function() {
  /* device variables */
  // @type {DEVICE_STATE} A state for device
  this.deviceState = DEVICE_STATE.IDLE;

  /* receivers available */
  // @type {boolean} A boolean to indicate availability of receivers
  this.receivers_available = false;

  /* Cast player variables */
  // @type {Object} a chrome.cast.media.Media object
  this.currentMediaSession = null;
  // @type {Number} volume
  this.currentVolume = 0.5;
  // @type {Boolean} A flag for autoplay after load
  this.autoplay = true;
  // @type {string} a chrome.cast.Session object
  this.session = null;
  // @type {PLAYER_STATE} A state for Cast media player
  this.castPlayerState = PLAYER_STATE.IDLE;

  /* Local player variables */
  // @type {PLAYER_STATE} A state for local media player
  this.localPlayerState = PLAYER_STATE.IDLE;
  // @type {HTMLElement} local player
  this.localPlayer = null;
  // @type {Boolean} Fullscreen mode on/off
  this.fullscreen = false;

  /* Current media variables */
  // @type {Boolean} Audio on and off
  this.audio = true;
  // @type {Number} A number for current media index
  this.currentMediaIndex = 0;
  // @type {Number} A number for current media time
  this.currentMediaTime = 0;
  // @type {Number} A number for current media duration
  this.currentMediaDuration = -1;
  // @type {Timer} A timer for tracking progress of media
  this.timer = null;
  // @type {Boolean} A boolean to stop timer update of progress when triggered by media status event 
  this.progressFlag = true;
  // @type {Number} A number in milliseconds for minimal progress update
  this.timerStep = 1000;

  /* media contents from JSON */
  this.mediaContents = null;


};

/**
 * Initialize local media player 
 */
CastPlayer.prototype.initializeLocalPlayer = function() {
  this.localPlayer = document.getElementById('video_element');
  this.localPlayer.addEventListener('loadeddata', this.onMediaLoadedLocally.bind(this));
};

/**
 * Initialize Cast media player 
 * Initializes the API. Note that either successCallback and errorCallback will be
 * invoked once the API has finished initialization. The sessionListener and 
 * receiverListener may be invoked at any time afterwards, and possibly more than once. 
 */
CastPlayer.prototype.initializeCastPlayer = function() {

  if (!chrome.cast || !chrome.cast.isAvailable) {
    setTimeout(this.initializeCastPlayer.bind(this), 1000);
    return;
  }

  //D32C496F - reference
  //60EECA51 AE_Cast(AE) [https://dev-chromecast.ott.aetnd.com/receiver_ae.html]
  //058E0830 AE_Cast(HISTORY) [https://dev-chromecast.ott.aetnd.com/receiver_his.html]
  //9BD85059 AE_Cast(LIFETIME) on [https://dev-chromecast.ott.aetnd.com/receiver_mlt.html]

  var applicationID = AETN.applicationID;//'9BD85059';

  // auto join policy can be one of the following three
  var autoJoinPolicy = chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED;
  //var autoJoinPolicy = chrome.cast.AutoJoinPolicy.PAGE_SCOPED;
  //var autoJoinPolicy = chrome.cast.AutoJoinPolicy.TAB_AND_ORIGIN_SCOPED;

  // request session
  var sessionRequest = new chrome.cast.SessionRequest(applicationID);
  var apiConfig = new chrome.cast.ApiConfig(sessionRequest,
    this.sessionListener.bind(this),
    this.receiverListener.bind(this),
    autoJoinPolicy);

  chrome.cast.initialize(apiConfig, this.onInitSuccess.bind(this), this.onError.bind(this));

  this.addVideoThumbs();
  this.initializeUI();
};

/**
 * Callback function for init success 
 */
CastPlayer.prototype.onInitSuccess = function() {
  console.log("init success");
  this.updateMediaControlUI();
};

/**
 * Generic error callback function 
 */
CastPlayer.prototype.onError = function() {
  console.log("error");
};

/**
 * @param {!Object} e A new session
 * This handles auto-join when a page is reloaded
 * When active session is detected, playback will automatically
 * join existing session and occur in Cast mode and media
 * status gets synced up with current media of the session 
 */
CastPlayer.prototype.sessionListener = function(e) {
  trace('sessionListener:/');
  var parent=this;
  this.session = e;
  if( this.session ) {
    this.deviceState = DEVICE_STATE.ACTIVE;
    if( this.session.media[0] ) {
      this.onMediaDiscovered('activeSession', this.session.media[0]);
      this.syncCurrentMedia(this.session.media[0].media.contentId);
      this.selectMediaUpdateUI(this.currentMediaIndex);
      this.updateDisplayMessage();
    }
    else {
      //console.log('TRY '+this.currentMediaIndex);
      //this.loadMedia(this.currentMediaIndex);
    }
    this.session.addUpdateListener(this.sessionUpdateListener.bind(this));

    //session.addUpdateListener(sessionUpdateListener);
    this.session.addMessageListener(AETN.namespace, parent.messageRouter);
    //this.session.addMessageListener(AETN.namespace, function(){alert('!')});
  }
}


  CastPlayer.prototype.messageRouter =function(namespace, getDataString) {
    var jData=JSON.parse(getDataString);
    alert("receiverMessage: "+namespace+", "+JSON.stringify(jData));
  };



/**
 * @param {string} currentMediaURL
 */
CastPlayer.prototype.syncCurrentMedia = function(currentMediaURL) {
  for(var i=0; i < this.mediaContents.length; i++) {
    if( currentMediaURL == this.mediaContents[i]['sources'][0] ) {
      this.currentMediaIndex = i;
    }
  }
}

/**
 * @param {string} e Receiver availability
 * This indicates availability of receivers but
 * does not provide a list of device IDs
 */
CastPlayer.prototype.receiverListener = function(e) {
  if( e === 'available' ) {
    this.receivers_available = true;
    this.updateMediaControlUI();
    console.log("receiver found");
  }
  else {
    console.log("receiver list empty");
  }
};

/**
 * session update listener
 */
CastPlayer.prototype.sessionUpdateListener = function(isAlive) {
  if (!isAlive) {
    this.session = null;
    this.deviceState = DEVICE_STATE.IDLE;
    this.castPlayerState = PLAYER_STATE.IDLE;
    this.currentMediaSession = null;
    clearInterval(this.timer);
    this.updateDisplayMessage();

    var online = navigator.onLine;
    if( online == true ) {
      // continue to play media locally
      console.log("current time: " + this.currentMediaTime);
      this.playMediaLocally();
      this.updateMediaControlUI();
    }
  }
};


/**
 * Select a media content
 * @param {Number} mediaIndex A number for media index 
 */
CastPlayer.prototype.selectMedia = function(mediaIndex) {
  console.log("media selected" + mediaIndex);

  this.currentMediaIndex = mediaIndex;
  // reset progress bar
  var pi = document.getElementById("progress_indicator"); 
  var p = document.getElementById("progress"); 

  // reset currentMediaTime
  this.currentMediaTime = 0;

  p.style.width = '0px';
  pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + 'px';

  if( !this.currentMediaSession ) {
    this.localPlayerState = PLAYER_STATE.IDLE;
    this.playMediaLocally();
  }
  else {
    this.castPlayerState = PLAYER_STATE.IDLE;
    this.playMedia();
  }
  this.selectMediaUpdateUI(mediaIndex);
};

/**
 * Requests that a receiver application session be created or joined. By default, the SessionRequest
 * passed to the API at initialization time is used; this may be overridden by passing a different
 * session request in opt_sessionRequest. 
 */
CastPlayer.prototype.launchApp = function() {
  console.log("launching app...");
  chrome.cast.requestSession(
    this.sessionListener.bind(this),
    this.onLaunchError.bind(this));
  if( this.timer ) {
    clearInterval(this.timer);
  }
};

/**
 * Callback function for request session success 
 * @param {Object} e A chrome.cast.Session object
 */
CastPlayer.prototype.onRequestSessionSuccess = function(e) {
  console.log("session success: " + e.sessionId);
  this.session = e;
  this.deviceState = DEVICE_STATE.ACTIVE;
  this.updateMediaControlUI();
  this.loadMedia(this.currentMediaIndex);
  this.session.addUpdateListener(this.sessionUpdateListener.bind(this));
};

/**
 * Callback function for launch error
 */
CastPlayer.prototype.onLaunchError = function() {
  console.log("launch error");
  this.deviceState = DEVICE_STATE.ERROR;
};

/**
 * Stops the running receiver application associated with the session.
 */
CastPlayer.prototype.stopApp = function() {
  this.session.stop(this.onStopAppSuccess.bind(this, 'Session stopped'),
      this.onError.bind(this));    

};

/**
 * Callback function for stop app success 
 */
CastPlayer.prototype.onStopAppSuccess = function(message) {
  console.log(message);
  this.deviceState = DEVICE_STATE.IDLE;
  this.castPlayerState = PLAYER_STATE.IDLE;
  this.currentMediaSession = null;
  clearInterval(this.timer);
  this.updateDisplayMessage();

  // continue to play media locally
  console.log("current time: " + this.currentMediaTime);
  this.playMediaLocally();
  this.updateMediaControlUI();
};

/**
 * Loads media into a running receiver application
 * @param {Number} mediaIndex An index number to indicate current media content
 */
CastPlayer.prototype.loadMedia = function(mediaIndex) {

/*
  if (!this.session) {
    console.log("no session");
    return;
  }
  console.log("loading..." + this.mediaContents[mediaIndex]['title']);
  var mediaInfo = new chrome.cast.media.MediaInfo(this.mediaContents[mediaIndex]['sources'][0]);

  mediaInfo.metadata = new chrome.cast.media.GenericMediaMetadata();
  mediaInfo.metadata.metadataType = chrome.cast.media.MetadataType.GENERIC;
  mediaInfo.contentType = 'video/mp4';

  mediaInfo.metadata.title = this.mediaContents[mediaIndex]['title'];
  mediaInfo.metadata.images = [{'url': MEDIA_SOURCE_ROOT + this.mediaContents[mediaIndex]['thumb']}];

  var request = new chrome.cast.media.LoadRequest(mediaInfo);
  request.autoplay = this.autoplay;
  if( this.localPlayerState == PLAYER_STATE.PLAYING ) {
    request.currentTime = this.localPlayer.currentTime;
    this.localPlayer.pause();
    this.localPlayerState = PLAYER_STATE.STOPPED;
  }
  else {
    request.currentTime = 0;
  } 

  this.castPlayerState = PLAYER_STATE.LOADING;
  this.session.loadMedia(request,
    this.onMediaDiscovered.bind(this, 'loadMedia'),
    this.onLoadMediaError.bind(this));

  document.getElementById("media_title").innerHTML = this.mediaContents[this.currentMediaIndex]['title'];
  document.getElementById("media_subtitle").innerHTML = this.mediaContents[this.currentMediaIndex]['subtitle'];
  document.getElementById("media_desc").innerHTML = this.mediaContents[this.currentMediaIndex]['description'];
*/


  this.loadMedia_manual(this.mediaContents[mediaIndex])

};




  CastPlayer.prototype.loadMedia_manual = function(mediaContents) {

    //var mediaContents=getContents.customData
    //var mediaContents = {
    //  "description": "directly showing a os-vh.akamaihd.net link",
    //  "sources": ["https://os-vh.akamaihd.net/i/AETN-Lifetime_VMS/BRANDHD2398_LFT_PRJR_169318_TVE_000_2398_60_20160113_01_,4,18,13,10,7,2,1,00.mp4.csmil/master.m3u8?__b__=400&hdnea=st=1454446598~exp=1454457428~acl=/i/AETN-Lifetime_VMS/BRANDHD2398_LFT_PRJR_169318_TVE_000_2398_60_20160113_01_*~hmac=a20ac9bcd1ef89001d01eeb4ab74c1b91a338b5e04f7491a78c6985d4315fa8c&set-cc-attribute=CC"],
    //  "subtitle": "some link s01e01",
    //  "thumb": "images/BigBuckBunny.jpg",
    //  "title": "os-vh.akamaihd.net"
    //}

    if (!this.session) {
      console.log("no session");
      return;
    }
    console.log("loading..." + mediaContents['title']);
    //var mediaInfo = new chrome.cast.media.MediaInfo(mediaContents['sources'][0]);
    var mediaInfo = new chrome.cast.media.MediaInfo(mediaContents.platformID);

    console.log('mediaInfo='+JSON.stringify(mediaInfo));


    mediaInfo.metadata = new chrome.cast.media.GenericMediaMetadata();
    //mediaInfo.metadata.metadataType = chrome.cast.media.MetadataType.GENERIC;
    mediaInfo.contentType = 'video/mp4';

    //mediaInfo.metadata.aetnTitle = mediaContents['aetnTitle'];
    //mediaInfo.metadata.aetnDescription = mediaContents['aetnDescription'];
    //mediaInfo.metadata.aetnSubtitle = mediaContents['aetnSubtitle'];
    //mediaInfo.metadata.aetnSubtitle2 = mediaContents['aetnSubtitle2'];
    //mediaInfo.metadata.aetnImages = mediaContents['aetnImages'];
    //mediaInfo.metadata.aetnThumb = mediaContents['aetnThumb'];

    mediaInfo.customData = mediaContents;

    //mediaInfo.metadata.title = mediaContents['title'];
    //mediaInfo.metadata.subtitle = mediaContents['subtitle'];
    //mediaInfo.metadata.forceMPX = mediaContents['forceMPX'];
    //mediaInfo.metadata.subtitle2 = mediaContents['subtitle2'];
    //mediaInfo.metadata.hideConsole = mediaContents['hideConsole'];
    //mediaInfo.metadata.stickyTimeline = mediaContents['stickyTimeline'];
    //
    //mediaInfo.metadata.platformID = mediaContents['platformID'];
    //mediaInfo.metadata.userID = mediaContents['userID'];
    //mediaInfo.metadata.adBreaks = mediaContents['adBreaks'];
    //mediaInfo.metadata.description = mediaContents['description'];
    //mediaInfo.metadata.trackingData = mediaContents['trackingData'];
    //mediaInfo.metadata.images = [{'url': MEDIA_SOURCE_ROOT + mediaContents['thumb']}];

    var request = new chrome.cast.media.LoadRequest(mediaInfo);
    request.autoplay = this.autoplay;
    if( this.localPlayerState == PLAYER_STATE.PLAYING ) {
      request.currentTime = this.localPlayer.currentTime;
      this.localPlayer.pause();
      this.localPlayerState = PLAYER_STATE.STOPPED;
    }
    else {
      request.currentTime = 0;
    }

    this.castPlayerState = PLAYER_STATE.LOADING;
    this.session.loadMedia(request,
        this.onMediaDiscovered.bind(this, 'loadMedia'),
        this.onLoadMediaError.bind(this));

    document.getElementById("media_title").innerHTML = this.mediaContents[this.currentMediaIndex]['title'];
    document.getElementById("media_subtitle").innerHTML = this.mediaContents[this.currentMediaIndex]['subtitle'];
    document.getElementById("media_desc").innerHTML = this.mediaContents[this.currentMediaIndex]['description'];

  };



/**
 * Callback function for loadMedia success
 * @param {Object} mediaSession A new media object.
 */
CastPlayer.prototype.onMediaDiscovered = function(how, mediaSession) {
  console.log("new media session ID:" + mediaSession.mediaSessionId + ' (' + how + ')');
  this.currentMediaSession = mediaSession;
  if( how == 'loadMedia' ) {
    if( this.autoplay ) {
      this.castPlayerState = PLAYER_STATE.PLAYING;
    }
    else {
      this.castPlayerState = PLAYER_STATE.LOADED;
    }
  }

  if( how == 'activeSession' ) {
    this.castPlayerState = this.session.media[0].playerState; 
    this.currentMediaTime = this.session.media[0].currentTime; 
  }

  if( this.castPlayerState == PLAYER_STATE.PLAYING ) {
    // start progress timer
    this.startProgressTimer(this.incrementMediaTime);
  }

  this.currentMediaSession.addUpdateListener(this.onMediaStatusUpdate.bind(this));

  this.currentMediaDuration = this.currentMediaSession.media.duration;
  var duration = this.currentMediaDuration;
  var hr = parseInt(duration/3600);
  duration -= hr * 3600;
  var min = parseInt(duration/60);
  var sec = parseInt(duration % 60);
  if ( hr > 0 ) {
    duration = hr + ":" + min + ":" + sec;
  }
  else {
    if( min > 0 ) {
      duration = min + ":" + sec;
    }
    else {
      duration = sec;
    }
  }
  document.getElementById("duration").innerHTML = duration;

  if( this.localPlayerState == PLAYER_STATE.PLAYING ) {
    this.localPlayerState == PLAYER_STATE.STOPPED;
    var vi = document.getElementById('video_image')
    vi.style.display = 'block';
    this.localPlayer.style.display = 'none';
    // start progress timer
    this.startProgressTimer(this.incrementMediaTime);
  }
  // update UIs
  this.updateMediaControlUI();
  this.updateDisplayMessage();
};

/**
 * Callback function when media load returns error 
 */
CastPlayer.prototype.onLoadMediaError = function(e) {
  console.log("media error");
  this.castPlayerState = PLAYER_STATE.IDLE;
  // update UIs
  this.updateMediaControlUI();
  this.updateDisplayMessage();
};

/**
 * Callback function for media status update from receiver
 * @param {!Boolean} e true/false
 */
CastPlayer.prototype.onMediaStatusUpdate = function(e) {
  if( e == false ) {
    this.currentMediaTime = 0;
    this.castPlayerState = PLAYER_STATE.IDLE;
  }
  console.log("updating media");
  this.updateProgressBar(e);
  this.updateDisplayMessage();
  this.updateMediaControlUI();
};

/**
 * Helper function
 * Increment media current position by 1 second 
 */
CastPlayer.prototype.incrementMediaTime = function() {
  if( this.castPlayerState == PLAYER_STATE.PLAYING || this.localPlayerState == PLAYER_STATE.PLAYING ) {
    if( this.currentMediaTime < this.currentMediaDuration ) {
      this.currentMediaTime += 1;
      this.updateProgressBarByTimer();
    }
    else {
      this.currentMediaTime = 0;
      clearInterval(this.timer);
    }
  }
};

/**
 * Play media in local player
 */
CastPlayer.prototype.playMediaLocally = function() {
  var vi = document.getElementById('video_image')
  vi.style.display = 'none';
  this.localPlayer.style.display = 'block';
  if( this.localPlayerState != PLAYER_STATE.PLAYING && this.localPlayerState != PLAYER_STATE.PAUSED ) { 
    this.localPlayer.src = this.mediaContents[this.currentMediaIndex]['sources'][0];
    this.localPlayer.load();
  }
  else {
    this.localPlayer.play();
    // start progress timer
    this.startProgressTimer(this.incrementMediaTime);
  }
  this.localPlayerState = PLAYER_STATE.PLAYING;
  this.updateMediaControlUI();
};

/**
 * Callback when media is loaded in local player 
 */
CastPlayer.prototype.onMediaLoadedLocally = function() {
  this.currentMediaDuration = this.localPlayer.duration;
  var duration = this.currentMediaDuration;
      
  var hr = parseInt(duration/3600);
  duration -= hr * 3600;
  var min = parseInt(duration/60);
  var sec = parseInt(duration % 60);
  if ( hr > 0 ) {
    duration = hr + ":" + min + ":" + sec;
  }
  else {
    if( min > 0 ) {
      duration = min + ":" + sec;
    }
    else {
      duration = sec;
    }
  }
  document.getElementById("duration").innerHTML = duration;
  this.localPlayer.currentTime = this.currentMediaTime;

  this.localPlayer.play();
  // start progress timer
  this.startProgressTimer(this.incrementMediaTime);
};

/**
 * Play media in Cast mode 
 */
CastPlayer.prototype.playMedia = function() {
  if( !this.currentMediaSession ) {
    this.playMediaLocally();
    return;
  }

  switch( this.castPlayerState ) 
  {
    case PLAYER_STATE.LOADED:
    case PLAYER_STATE.PAUSED:
      this.currentMediaSession.play(null, 
        this.mediaCommandSuccessCallback.bind(this,"playing started for " + this.currentMediaSession.sessionId),
        this.onError.bind(this));
      this.currentMediaSession.addUpdateListener(this.onMediaStatusUpdate.bind(this));
      this.castPlayerState = PLAYER_STATE.PLAYING;
      // start progress timer
      this.startProgressTimer(this.incrementMediaTime);
      break;
    case PLAYER_STATE.IDLE:
    case PLAYER_STATE.LOADING:
    case PLAYER_STATE.STOPPED:
      this.loadMedia(this.currentMediaIndex);
      this.currentMediaSession.addUpdateListener(this.onMediaStatusUpdate.bind(this));
      this.castPlayerState = PLAYER_STATE.PLAYING;
      break;
    default:
      break;
  }
  this.updateMediaControlUI();
  this.updateDisplayMessage();
};

/**
 * Pause media playback in Cast mode  
 */
CastPlayer.prototype.pauseMedia = function() {
  if( !this.currentMediaSession ) {
    this.pauseMediaLocally();
    return;
  }

  if( this.castPlayerState == PLAYER_STATE.PLAYING ) {
    this.castPlayerState = PLAYER_STATE.PAUSED;
    this.currentMediaSession.pause(null,
      this.mediaCommandSuccessCallback.bind(this,"paused " + this.currentMediaSession.sessionId),
      this.onError.bind(this));
    this.updateMediaControlUI();
    this.updateDisplayMessage();
    clearInterval(this.timer);
  }
};

/**
 * Pause media playback in local player 
 */
CastPlayer.prototype.pauseMediaLocally = function() {
  this.localPlayer.pause();
  this.localPlayerState = PLAYER_STATE.PAUSED;
  this.updateMediaControlUI();
  clearInterval(this.timer);
};

/**
 * Stop media playback in either Cast or local mode  
 */
CastPlayer.prototype.stopMedia = function() {
  if( !this.currentMediaSession ) {
    this.stopMediaLocally();
    return;
  }

  this.currentMediaSession.stop(null,
    this.mediaCommandSuccessCallback.bind(this,"stopped " + this.currentMediaSession.sessionId),
    this.onError.bind(this));
  this.castPlayerState = PLAYER_STATE.STOPPED;
  clearInterval(this.timer);

  this.updateDisplayMessage();
  this.updateMediaControlUI();
};

/**
 * Stop media playback in local player
 */
CastPlayer.prototype.stopMediaLocally = function() {
  var vi = document.getElementById('video_image')
  vi.style.display = 'block';
  this.localPlayer.style.display = 'none';
  this.localPlayer.stop();
  this.localPlayerState = PLAYER_STATE.STOPPED;
  this.updateMediaControlUI();
};

/**
 * Set media volume in Cast mode
 * @param {Boolean} mute A boolean  
 */
CastPlayer.prototype.setReceiverVolume = function(mute) {
  var p = document.getElementById("audio_bg_level"); 
  if( event.currentTarget.id == 'audio_bg_track' ) {
    var pos = 100 - parseInt(event.offsetY);
  }
  else {
    var pos = parseInt(p.clientHeight) - parseInt(event.offsetY);
  }
  if( !this.currentMediaSession ) {
      this.localPlayer.volume = pos < 100 ? pos/100 : 1;
      p.style.height = pos + 'px';
      p.style.marginTop = -pos + 'px';
      return;
  }

  if( event.currentTarget.id == 'audio_bg_track' || event.currentTarget.id == 'audio_bg_level' ) {
    // add a drag to avoid loud volume
    if( pos < 100 ) {
      var vScale = this.currentVolume * 100;
      if( pos > vScale ) {
        pos = vScale + (pos - vScale)/2;
      }
      p.style.height = pos + 'px';
      p.style.marginTop = -pos + 'px';
      this.currentVolume = pos/100;
    }
    else {
      this.currentVolume = 1;
    }
  }

  if( !mute ) {
    this.session.setReceiverVolumeLevel(this.currentVolume,
      this.mediaCommandSuccessCallback.bind(this),
      this.onError.bind(this));
  }
  else {
    this.session.setReceiverMuted(true,
      this.mediaCommandSuccessCallback.bind(this),
      this.onError.bind(this));
  }
  this.updateMediaControlUI();
};

/**
 * Mute media function in either Cast or local mode 
 */
CastPlayer.prototype.muteMedia = function() {
  if( this.audio == true ) {
    this.audio = false;
    document.getElementById('audio_on').style.display = 'none';
    document.getElementById('audio_off').style.display = 'block';
    if( this.currentMediaSession ) {
      this.setReceiverVolume(true);
    }
    else {
      this.localPlayer.muted = true;
    }
  }
  else {
    this.audio = true;
    document.getElementById('audio_on').style.display = 'block';
    document.getElementById('audio_off').style.display = 'none';
    if( this.currentMediaSession ) {
      this.setReceiverVolume(false);
    }
    else {
      this.localPlayer.muted = false;
    }
  } 
  this.updateMediaControlUI();
};


/**
 * media seek function in either Cast or local mode
 * @param {Event} e An event object from seek 
 */
CastPlayer.prototype.seekMedia = function(event) {
  var pos = parseInt(event.offsetX);
  var pi = document.getElementById("progress_indicator"); 
  var p = document.getElementById("progress"); 
  if( event.currentTarget.id == 'progress_indicator' ) {
    var curr = parseInt(this.currentMediaTime + this.currentMediaDuration * pos / PROGRESS_BAR_WIDTH);
    var pp = parseInt(pi.style.marginLeft) + pos;
    var pw = parseInt(p.style.width) + pos;
  }
  else {
    var curr = parseInt(pos * this.currentMediaDuration / PROGRESS_BAR_WIDTH);
    var pp = pos -21 - PROGRESS_BAR_WIDTH;
    var pw = pos;
  }

  if( this.localPlayerState == PLAYER_STATE.PLAYING || this.localPlayerState == PLAYER_STATE.PAUSED ) {
    this.localPlayer.currentTime= curr;
    this.currentMediaTime = curr;
    this.localPlayer.play();
  }

  if( this.localPlayerState == PLAYER_STATE.PLAYING || this.localPlayerState == PLAYER_STATE.PAUSED 
      || this.castPlayerState == PLAYER_STATE.PLAYING || this.castPlayerState == PLAYER_STATE.PAUSED ) {
    p.style.width = pw + 'px';
    pi.style.marginLeft = pp + 'px';
  }

  if( this.castPlayerState != PLAYER_STATE.PLAYING && this.castPlayerState != PLAYER_STATE.PAUSED ) {
    return;
  }

  this.currentMediaTime = curr;
  console.log('Seeking ' + this.currentMediaSession.sessionId + ':' +
    this.currentMediaSession.mediaSessionId + ' to ' + pos + "%");
  var request = new chrome.cast.media.SeekRequest();
  request.currentTime = this.currentMediaTime;
  this.currentMediaSession.seek(request,
    this.onSeekSuccess.bind(this, 'media seek done'),
    this.onError.bind(this));
  this.castPlayerState = PLAYER_STATE.SEEKING;

  this.updateDisplayMessage();
  this.updateMediaControlUI();
};

/**
 * Callback function for seek success
 * @param {String} info A string that describe seek event
 */
CastPlayer.prototype.onSeekSuccess = function(info) {
  console.log(info);
  this.castPlayerState = PLAYER_STATE.PLAYING;
  this.updateDisplayMessage();
  this.updateMediaControlUI();
};

/**
 * Callback function for media command success 
 */
CastPlayer.prototype.mediaCommandSuccessCallback = function(info, e) {
  console.log(info);
};

/**
 * Update progress bar when there is a media status update
 * @param {Object} e An media status update object 
 */
CastPlayer.prototype.updateProgressBar = function(e) {
  var p = document.getElementById("progress"); 
  var pi = document.getElementById("progress_indicator"); 
  if( e == false ) {
    p.style.width = '0px';
    pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + 'px';
    clearInterval(this.timer);
    this.castPlayerState = PLAYER_STATE.STOPPED;
    this.updateDisplayMessage();
  } else {
    p.style.width = Math.ceil(PROGRESS_BAR_WIDTH * this.currentMediaSession.currentTime / this.currentMediaSession.media.duration + 1) + 'px';
    this.progressFlag = false; 
    setTimeout(this.setProgressFlag.bind(this),1000); // don't update progress in 1 second
    var pp = Math.ceil(PROGRESS_BAR_WIDTH * this.currentMediaSession.currentTime / this.currentMediaSession.media.duration);
    pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + pp + 'px';
  }
};

/**
 * Set progressFlag with a timeout of 1 second to avoid UI update
 * until a media status update from receiver 
 */
CastPlayer.prototype.setProgressFlag = function() {
  this.progressFlag = true;
};

/**
 * Update progress bar based on timer  
 */
CastPlayer.prototype.updateProgressBarByTimer = function() {
  var p = document.getElementById("progress"); 
  if( isNaN(parseInt(p.style.width)) ) {
    p.style.width = 0;
  } 
  if( this.currentMediaDuration > 0 ) {
    var pp = Math.floor(PROGRESS_BAR_WIDTH * this.currentMediaTime/this.currentMediaDuration);
  }
    
  if( this.progressFlag ) { 
    // don't update progress if it's been updated on media status update event
    p.style.width = pp + 'px'; 
    var pi = document.getElementById("progress_indicator"); 
    pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + pp + 'px';
  }

  if( pp > PROGRESS_BAR_WIDTH ) {
    clearInterval(this.timer);
    this.deviceState = DEVICE_STATE.IDLE;
    this.castPlayerState = PLAYER_STATE.IDLE;
    this.updateDisplayMessage();
    this.updateMediaControlUI();
  }
};

/**
 * Update display message depending on cast mode by deviceState 
 */
CastPlayer.prototype.updateDisplayMessage = function() {
  if( this.deviceState != DEVICE_STATE.ACTIVE || this.castPlayerState == PLAYER_STATE.IDLE || this.castPlayerState == PLAYER_STATE.STOPPED ) {
    document.getElementById("playerstate").style.display = 'none';
    document.getElementById("playerstatebg").style.display = 'none';
    document.getElementById("play").style.display = 'block';
    document.getElementById("video_image_overlay").style.display = 'none';
  }
  else {
    document.getElementById("playerstate").style.display = 'block';
    document.getElementById("playerstatebg").style.display = 'block';
    document.getElementById("video_image_overlay").style.display = 'block';
    document.getElementById("playerstate").innerHTML = 
      this.mediaContents[this.currentMediaIndex]['title'] + " "
      + this.castPlayerState + " on " + this.session.receiver.friendlyName;
  }
}

/**
 * Update media control UI components based on localPlayerState or castPlayerState
 */
CastPlayer.prototype.updateMediaControlUI = function() {
  var playerState = this.deviceState == DEVICE_STATE.ACTIVE ? this.castPlayerState : this.localPlayerState;
  switch ( playerState )
  {
    case PLAYER_STATE.LOADED:
    case PLAYER_STATE.PLAYING:
      document.getElementById("play").style.display = 'none';
      document.getElementById("pause").style.display = 'block';
      break;
    case PLAYER_STATE.PAUSED:
    case PLAYER_STATE.IDLE:
    case PLAYER_STATE.LOADING:
    case PLAYER_STATE.STOPPED:
      document.getElementById("play").style.display = 'block';
      document.getElementById("pause").style.display = 'none';
      break;
    default:
      break;
  }

  if( !this.receivers_available ) {
    document.getElementById("casticonactive").style.display = 'none';
    document.getElementById("casticonidle").style.display = 'none';
    return;
  }

  if( this.deviceState == DEVICE_STATE.ACTIVE ) {
    document.getElementById("casticonactive").style.display = 'block';
    document.getElementById("casticonidle").style.display = 'none';
    this.hideFullscreenButton();
  }
  else {
    document.getElementById("casticonidle").style.display = 'block';
    document.getElementById("casticonactive").style.display = 'none';
    this.showFullscreenButton();
  }
}

/**
 * Update UI components after selectMedia call 
 * @param {Number} mediaIndex An number
 */
CastPlayer.prototype.selectMediaUpdateUI = function(mediaIndex) {
  document.getElementById('video_image').src = MEDIA_SOURCE_ROOT + this.mediaContents[mediaIndex]['thumb'];
  document.getElementById("progress").style.width = '0px';
  document.getElementById("media_title").innerHTML = this.mediaContents[mediaIndex]['title'];
  document.getElementById("media_subtitle").innerHTML = this.mediaContents[mediaIndex]['subtitle'];
  document.getElementById("media_desc").innerHTML = this.mediaContents[mediaIndex]['description'];
};

/**
 * Initialize UI components and add event listeners 
 */
CastPlayer.prototype.initializeUI = function() {
  // set initial values for title, subtitle, and description 
  document.getElementById("media_title").innerHTML = this.mediaContents[0]['title'];
  document.getElementById("media_subtitle").innerHTML = this.mediaContents[this.currentMediaIndex]['subtitle'];
  document.getElementById("media_desc").innerHTML = this.mediaContents[this.currentMediaIndex]['description'];

  // add event handlers to UI components
  document.getElementById("casticonidle").addEventListener('click', this.launchApp.bind(this));
  document.getElementById("casticonactive").addEventListener('click', this.stopApp.bind(this));
  document.getElementById("progress_bg").addEventListener('click', this.seekMedia.bind(this));
  document.getElementById("progress").addEventListener('click', this.seekMedia.bind(this));
  document.getElementById("progress_indicator").addEventListener('dragend', this.seekMedia.bind(this));
  document.getElementById("audio_on").addEventListener('click', this.muteMedia.bind(this));
  document.getElementById("audio_off").addEventListener('click', this.muteMedia.bind(this));
  document.getElementById("audio_bg").addEventListener('mouseover', this.showVolumeSlider.bind(this));
  document.getElementById("audio_on").addEventListener('mouseover', this.showVolumeSlider.bind(this));
  document.getElementById("audio_bg_level").addEventListener('mouseover', this.showVolumeSlider.bind(this));
  document.getElementById("audio_bg_track").addEventListener('mouseover', this.showVolumeSlider.bind(this));
  document.getElementById("audio_bg_level").addEventListener('click', this.setReceiverVolume.bind(this, false));
  document.getElementById("audio_bg_track").addEventListener('click', this.setReceiverVolume.bind(this, false));
  document.getElementById("audio_bg").addEventListener('mouseout', this.hideVolumeSlider.bind(this));
  document.getElementById("audio_on").addEventListener('mouseout', this.hideVolumeSlider.bind(this));
  document.getElementById("media_control").addEventListener('mouseover', this.showMediaControl.bind(this));
  document.getElementById("media_control").addEventListener('mouseout', this.hideMediaControl.bind(this));
  document.getElementById("fullscreen_expand").addEventListener('click', this.requestFullScreen.bind(this));
  document.getElementById("fullscreen_collapse").addEventListener('click', this.cancelFullScreen.bind(this));
  document.addEventListener("fullscreenchange", this.changeHandler.bind(this), false);      
  document.addEventListener("webkitfullscreenchange", this.changeHandler.bind(this), false);

  // enable play/pause buttons
  document.getElementById("play").addEventListener('click', this.playMedia.bind(this));
  document.getElementById("pause").addEventListener('click', this.pauseMedia.bind(this));
  document.getElementById("progress_indicator").draggable = true;

};

/**
 * Show the media control 
 */
CastPlayer.prototype.showMediaControl = function() {
  document.getElementById('media_control').style.opacity = 0.7;
};    

/**
 * Hide the media control  
 */
CastPlayer.prototype.hideMediaControl = function() {
  //document.getElementById('media_control').style.opacity = 0;
};    

/**
 * Show the volume slider
 */
CastPlayer.prototype.showVolumeSlider = function() {
  document.getElementById('audio_bg').style.opacity = 1;
  document.getElementById('audio_bg_track').style.opacity = 1;
  document.getElementById('audio_bg_level').style.opacity = 1;
  document.getElementById('audio_indicator').style.opacity = 1;
};    

/**
 * Hide the volume slider 
 */
CastPlayer.prototype.hideVolumeSlider = function() {
  document.getElementById('audio_bg').style.opacity = 0;
  document.getElementById('audio_bg_track').style.opacity = 0;
  document.getElementById('audio_bg_level').style.opacity = 0;
  document.getElementById('audio_indicator').style.opacity = 0;
};    

/**
 * Request full screen mode 
 */
CastPlayer.prototype.requestFullScreen = function() {
  // Supports most browsers and their versions.
  var element = document.getElementById("video_element");
  var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen;

  if (requestMethod) { // Native full screen.
    requestMethod.call(element);
    console.log("requested fullscreen");
  }
};

/**
 * Exit full screen mode 
 */
CastPlayer.prototype.cancelFullScreen = function() {
  // Supports most browsers and their versions.
  var requestMethod = document.cancelFullScreen || document.webkitCancelFullScreen;

  if (requestMethod) { 
    requestMethod.call(document);
  } 
};

/**
 * Exit fullscreen mode by escape 
 */
CastPlayer.prototype.changeHandler = function(){
  this.fullscreen = !this.fullscreen;
  if (this.deviceState == DEVICE_STATE.ACTIVE) {
    this.hideFullscreenButton();
  }
  else {
    this.showFullscreenButton();
  }
};

/**
 * Show expand/collapse fullscreen button
 */
CastPlayer.prototype.showFullscreenButton = function(){
  if (this.fullscreen) {
    document.getElementById('fullscreen_expand').style.display = 'none';
    document.getElementById('fullscreen_collapse').style.display = 'block';
  }
  else {
    document.getElementById('fullscreen_expand').style.display = 'block';
    document.getElementById('fullscreen_collapse').style.display = 'none';
  }
};

/**
 * Hide expand/collapse fullscreen button
 */
CastPlayer.prototype.hideFullscreenButton = function(){
  document.getElementById('fullscreen_expand').style.display = 'none';
  document.getElementById('fullscreen_collapse').style.display = 'none';
};

/**
 * @param {function} A callback function for the function to start timer 
 */
CastPlayer.prototype.startProgressTimer = function(callback) {
  if( this.timer ) {
    clearInterval(this.timer);
    this.timer = null;
  }

  // start progress timer
  this.timer = setInterval(callback.bind(this), this.timerStep);
};

/**
 * Do AJAX call to load media json
 * @param {String} src A URL for media json.
 */
CastPlayer.prototype.retrieveMediaJSON = function(src) {
  console.log('retrieveMediaJSON not needed')
  //var xhr = new XMLHttpRequest();
  //xhr.addEventListener('load', this.onMediaJsonLoad.bind(this));
  //xhr.addEventListener('error', this.onMediaJsonError.bind(this));
  //xhr.open('GET', src);
  //xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
  //xhr.responseType = "json";
  //xhr.send(null);
};

/**
 * Callback function for AJAX call on load success
 * @param {Object} evt An object returned from Ajax call
 */
CastPlayer.prototype.onMediaJsonLoad = function(evt) {
  var responseJson = evt.srcElement.response;
  this.mediaContents = responseJson['categories'][0]['videos'];
  var ni = document.getElementById('carousel');
  var newdiv = null;
  var divIdName = null;
  for( var i = 0; i < this.mediaContents.length; i++ ) {
    newdiv = document.createElement('div');
    divIdName = 'thumb'+i+'Div';
    newdiv.setAttribute('id',divIdName);
    newdiv.setAttribute('class','thumb');
    newdiv.innerHTML = '<img src="' + MEDIA_SOURCE_ROOT + this.mediaContents[i]['thumb'] + '" class="thumbnail">';
    newdiv.addEventListener('click', this.selectMedia.bind(this, i));
    ni.appendChild(newdiv);
  }
}

/**
 * Callback function for AJAX call on load error
 */
CastPlayer.prototype.onMediaJsonError = function() {
  console.log("Failed to load media JSON");
}

/**
 * Add video thumbnails div's to UI for media JSON contents 
 */
CastPlayer.prototype.addVideoThumbs = function() {
  this.mediaContents = mediaJSON['categories'][0]['videos'];
  var ni = document.getElementById('carousel');
  var newdiv = null;
  var newdivBG = null;
  var divIdName = null;
  for( var i = 0; i < this.mediaContents.length; i++ ) {
    newdiv = document.createElement('div');
    divIdName = 'thumb'+i+'Div';
    newdiv.setAttribute('id',divIdName);
    newdiv.setAttribute('class','thumb');
    //alert(this.mediaContents[i]['thumb']);
    newdiv.innerHTML = '<img src="' + MEDIA_SOURCE_ROOT + this.mediaContents[i]['thumb'] + '" class="thumbnail">';
    newdiv.addEventListener('click', this.selectMedia.bind(this, i));
    ni.appendChild(newdiv);
  }
}

/**
 * hardcoded media json objects
 */
var mediaJSON = { "categories" : [ { "name" : "Movies",
        "videos" : [
		    { "description" : "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license\nhttp://www.bigbuckbunny.org",
              "sources" : [ "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4" ],
              "subtitle" : "By Blender Foundation",
              "thumb" : "images/BigBuckBunny.jpg",
              "title" : "Big Buck Bunny"
            }
    ]}]};

 window.CastPlayer = new CastPlayer;

  //alert('sender loaded');


})();

define("chromeSender", function(){});

