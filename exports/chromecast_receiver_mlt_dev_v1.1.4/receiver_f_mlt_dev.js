/*! chromecast_receiver_mlt_dev : receiver_f_mlt_dev.js */
/*! codebase: CB2016 v1.1.4 by Jonathan Robles */
/*! built:05-03-2016 [12:55:02PM] */
/*! -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

/*! Prerequisites: //www.gstatic.com/cast/sdk/libs/receiver/2.0.0/cast_receiver.js */

/*! Third Party Includes [start] */
/* almond, modernizr, underscore, oboe */
/*! Third Party Includes [end] */
define("modules/definitions/standardmodule", [], function() {
    var _instanceID = 0;
    var _nextInstanceID = function() {
        return ++_instanceID;
    };
    var defaults = [];
    var deepExtend = function(destination, source) {
        for (var property in source) {
            if (source[property] && source[property].constructor && source[property].constructor === Object) {
                destination[property] = destination[property] || {};
                arguments.callee(destination[property], source[property]);
            } else {
                destination[property] = source[property];
            }
        }
        return destination;
    };

    function _thizOBJ_(o) {
        this._instanceID = _nextInstanceID();
        var _parent = this;
        defaults[this._instanceID] = {
            type: "Standard Module Definition",
            author: "Jonathan Robles",
            notifyscope: "global",
            target: undefined,
            file: undefined,
            usenocache: true,
            data: undefined,
            callback: undefined,
            interval: undefined,
            init: function() {
                _notify.broadcast("Initialize", [{
                    senderID: _parent._instanceID,
                    sendertype: this.type,
                    notifyscope: this.notifyscope,
                    data: {
                        author: this.author
                    }
                }]);
            },
            parent: this
        };
        defaults[this._instanceID] = deepExtend(defaults[this._instanceID], o);
        defaults[this._instanceID].init();
        return this;
    }
    _thizOBJ_.prototype = {
        _init: function() {
            this._var().init();
        },
        _showdata: function() {
            return JSON.stringify(defaults[this._instanceID]);
        },
        _id: function() {
            return this._instanceID;
        },
        _var: function(o) {
            if (o != undefined) {
                defaults[this._instanceID] = deepExtend(defaults[this._instanceID], o);
            }
            return defaults[this._instanceID];
        },
        _nocache: function(string) {
            if (typeof string === "string") {
                if (this._var().usenocache) {
                    var addOn = "?";
                    if (string.indexOf("?") != -1) {
                        addOn = "&";
                    }
                    return string + addOn + "nocache=" + Math.floor(Math.random() * 9999);
                } else {
                    return string;
                }
            } else {
                this.notify("Alert", "_nocache needs a string!");
                return;
            }
        },
        notify: function(type, data) {
            _notify.broadcast(type, [{
                senderID: this._id(),
                sendertype: this._var().type,
                notifyscope: this._var().notifyscope,
                data: data
            }]);
        },
        deepExtend: function(destination, source) {
            return deepExtend(destination, source);
        },
        parent: this
    };
    return _thizOBJ_;
});

(function() {
    function n(n) {
        function t(t, r, e, u, i, o) {
            for (; i >= 0 && o > i; i += n) {
                var a = u ? u[i] : i;
                e = r(e, t[a], a, t);
            }
            return e;
        }
        return function(r, e, u, i) {
            e = b(e, i, 4);
            var o = !k(r) && m.keys(r),
                a = (o || r).length,
                c = n > 0 ? 0 : a - 1;
            return arguments.length < 3 && (u = r[o ? o[c] : c], c += n), t(r, e, u, o, c, a);
        };
    }

    function t(n) {
        return function(t, r, e) {
            r = x(r, e);
            for (var u = O(t), i = n > 0 ? 0 : u - 1; i >= 0 && u > i; i += n)
                if (r(t[i], i, t)) return i;
            return -1;
        };
    }

    function r(n, t, r) {
        return function(e, u, i) {
            var o = 0,
                a = O(e);
            if ("number" == typeof i) n > 0 ? o = i >= 0 ? i : Math.max(i + a, o) : a = i >= 0 ? Math.min(i + 1, a) : i + a + 1;
            else if (r && i && a) return i = r(e, u),
                e[i] === u ? i : -1;
            if (u !== u) return i = t(l.call(e, o, a), m.isNaN), i >= 0 ? i + o : -1;
            for (i = n > 0 ? o : a - 1; i >= 0 && a > i; i += n)
                if (e[i] === u) return i;
            return -1;
        };
    }

    function e(n, t) {
        var r = I.length,
            e = n.constructor,
            u = m.isFunction(e) && e.prototype || a,
            i = "constructor";
        for (m.has(n, i) && !m.contains(t, i) && t.push(i); r--;) i = I[r], i in n && n[i] !== u[i] && !m.contains(t, i) && t.push(i);
    }
    var u = this,
        i = u._,
        o = Array.prototype,
        a = Object.prototype,
        c = Function.prototype,
        f = o.push,
        l = o.slice,
        s = a.toString,
        p = a.hasOwnProperty,
        h = Array.isArray,
        v = Object.keys,
        g = c.bind,
        y = Object.create,
        d = function() {},
        m = function(n) {
            return n instanceof m ? n : this instanceof m ? void(this._wrapped = n) : new m(n);
        };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = m),
        exports._ = m) : u._ = m, m.VERSION = "1.8.3";
    var b = function(n, t, r) {
            if (t === void 0) return n;
            switch (null == r ? 3 : r) {
                case 1:
                    return function(r) {
                        return n.call(t, r);
                    };

                case 2:
                    return function(r, e) {
                        return n.call(t, r, e);
                    };

                case 3:
                    return function(r, e, u) {
                        return n.call(t, r, e, u);
                    };

                case 4:
                    return function(r, e, u, i) {
                        return n.call(t, r, e, u, i);
                    };
            }
            return function() {
                return n.apply(t, arguments);
            };
        },
        x = function(n, t, r) {
            return null == n ? m.identity : m.isFunction(n) ? b(n, t, r) : m.isObject(n) ? m.matcher(n) : m.property(n);
        };
    m.iteratee = function(n, t) {
        return x(n, t, 1 / 0);
    };
    var _ = function(n, t) {
            return function(r) {
                var e = arguments.length;
                if (2 > e || null == r) return r;
                for (var u = 1; e > u; u++)
                    for (var i = arguments[u], o = n(i), a = o.length, c = 0; a > c; c++) {
                        var f = o[c];
                        t && r[f] !== void 0 || (r[f] = i[f]);
                    }
                return r;
            };
        },
        j = function(n) {
            if (!m.isObject(n)) return {};
            if (y) return y(n);
            d.prototype = n;
            var t = new d();
            return d.prototype = null, t;
        },
        w = function(n) {
            return function(t) {
                return null == t ? void 0 : t[n];
            };
        },
        A = Math.pow(2, 53) - 1,
        O = w("length"),
        k = function(n) {
            var t = O(n);
            return "number" == typeof t && t >= 0 && A >= t;
        };
    m.each = m.forEach = function(n, t, r) {
        t = b(t, r);
        var e, u;
        if (k(n))
            for (e = 0, u = n.length; u > e; e++) t(n[e], e, n);
        else {
            var i = m.keys(n);
            for (e = 0, u = i.length; u > e; e++) t(n[i[e]], i[e], n);
        }
        return n;
    }, m.map = m.collect = function(n, t, r) {
        t = x(t, r);
        for (var e = !k(n) && m.keys(n), u = (e || n).length, i = Array(u), o = 0; u > o; o++) {
            var a = e ? e[o] : o;
            i[o] = t(n[a], a, n);
        }
        return i;
    }, m.reduce = m.foldl = m.inject = n(1), m.reduceRight = m.foldr = n(-1), m.find = m.detect = function(n, t, r) {
        var e;
        return e = k(n) ? m.findIndex(n, t, r) : m.findKey(n, t, r), e !== void 0 && e !== -1 ? n[e] : void 0;
    }, m.filter = m.select = function(n, t, r) {
        var e = [];
        return t = x(t, r), m.each(n, function(n, r, u) {
            t(n, r, u) && e.push(n);
        }), e;
    }, m.reject = function(n, t, r) {
        return m.filter(n, m.negate(x(t)), r);
    }, m.every = m.all = function(n, t, r) {
        t = x(t, r);
        for (var e = !k(n) && m.keys(n), u = (e || n).length, i = 0; u > i; i++) {
            var o = e ? e[i] : i;
            if (!t(n[o], o, n)) return !1;
        }
        return !0;
    }, m.some = m.any = function(n, t, r) {
        t = x(t, r);
        for (var e = !k(n) && m.keys(n), u = (e || n).length, i = 0; u > i; i++) {
            var o = e ? e[i] : i;
            if (t(n[o], o, n)) return !0;
        }
        return !1;
    }, m.contains = m.includes = m.include = function(n, t, r, e) {
        return k(n) || (n = m.values(n)), ("number" != typeof r || e) && (r = 0), m.indexOf(n, t, r) >= 0;
    }, m.invoke = function(n, t) {
        var r = l.call(arguments, 2),
            e = m.isFunction(t);
        return m.map(n, function(n) {
            var u = e ? t : n[t];
            return null == u ? u : u.apply(n, r);
        });
    }, m.pluck = function(n, t) {
        return m.map(n, m.property(t));
    }, m.where = function(n, t) {
        return m.filter(n, m.matcher(t));
    }, m.findWhere = function(n, t) {
        return m.find(n, m.matcher(t));
    }, m.max = function(n, t, r) {
        var e, u, i = -1 / 0,
            o = -1 / 0;
        if (null == t && null != n) {
            n = k(n) ? n : m.values(n);
            for (var a = 0, c = n.length; c > a; a++) e = n[a], e > i && (i = e);
        } else t = x(t, r), m.each(n, function(n, r, e) {
            u = t(n, r, e), (u > o || u === -1 / 0 && i === -1 / 0) && (i = n, o = u);
        });
        return i;
    }, m.min = function(n, t, r) {
        var e, u, i = 1 / 0,
            o = 1 / 0;
        if (null == t && null != n) {
            n = k(n) ? n : m.values(n);
            for (var a = 0, c = n.length; c > a; a++) e = n[a], i > e && (i = e);
        } else t = x(t, r), m.each(n, function(n, r, e) {
            u = t(n, r, e), (o > u || 1 / 0 === u && 1 / 0 === i) && (i = n, o = u);
        });
        return i;
    }, m.shuffle = function(n) {
        for (var t, r = k(n) ? n : m.values(n), e = r.length, u = Array(e), i = 0; e > i; i++) t = m.random(0, i),
            t !== i && (u[i] = u[t]), u[t] = r[i];
        return u;
    }, m.sample = function(n, t, r) {
        return null == t || r ? (k(n) || (n = m.values(n)), n[m.random(n.length - 1)]) : m.shuffle(n).slice(0, Math.max(0, t));
    }, m.sortBy = function(n, t, r) {
        return t = x(t, r), m.pluck(m.map(n, function(n, r, e) {
            return {
                value: n,
                index: r,
                criteria: t(n, r, e)
            };
        }).sort(function(n, t) {
            var r = n.criteria,
                e = t.criteria;
            if (r !== e) {
                if (r > e || r === void 0) return 1;
                if (e > r || e === void 0) return -1;
            }
            return n.index - t.index;
        }), "value");
    };
    var F = function(n) {
        return function(t, r, e) {
            var u = {};
            return r = x(r, e), m.each(t, function(e, i) {
                var o = r(e, i, t);
                n(u, e, o);
            }), u;
        };
    };
    m.groupBy = F(function(n, t, r) {
        m.has(n, r) ? n[r].push(t) : n[r] = [t];
    }), m.indexBy = F(function(n, t, r) {
        n[r] = t;
    }), m.countBy = F(function(n, t, r) {
        m.has(n, r) ? n[r]++ : n[r] = 1;
    }), m.toArray = function(n) {
        return n ? m.isArray(n) ? l.call(n) : k(n) ? m.map(n, m.identity) : m.values(n) : [];
    }, m.size = function(n) {
        return null == n ? 0 : k(n) ? n.length : m.keys(n).length;
    }, m.partition = function(n, t, r) {
        t = x(t, r);
        var e = [],
            u = [];
        return m.each(n, function(n, r, i) {
            (t(n, r, i) ? e : u).push(n);
        }), [e, u];
    }, m.first = m.head = m.take = function(n, t, r) {
        return null == n ? void 0 : null == t || r ? n[0] : m.initial(n, n.length - t);
    }, m.initial = function(n, t, r) {
        return l.call(n, 0, Math.max(0, n.length - (null == t || r ? 1 : t)));
    }, m.last = function(n, t, r) {
        return null == n ? void 0 : null == t || r ? n[n.length - 1] : m.rest(n, Math.max(0, n.length - t));
    }, m.rest = m.tail = m.drop = function(n, t, r) {
        return l.call(n, null == t || r ? 1 : t);
    }, m.compact = function(n) {
        return m.filter(n, m.identity);
    };
    var S = function(n, t, r, e) {
        for (var u = [], i = 0, o = e || 0, a = O(n); a > o; o++) {
            var c = n[o];
            if (k(c) && (m.isArray(c) || m.isArguments(c))) {
                t || (c = S(c, t, r));
                var f = 0,
                    l = c.length;
                for (u.length += l; l > f;) u[i++] = c[f++];
            } else r || (u[i++] = c);
        }
        return u;
    };
    m.flatten = function(n, t) {
            return S(n, t, !1);
        }, m.without = function(n) {
            return m.difference(n, l.call(arguments, 1));
        }, m.uniq = m.unique = function(n, t, r, e) {
            m.isBoolean(t) || (e = r, r = t, t = !1), null != r && (r = x(r, e));
            for (var u = [], i = [], o = 0, a = O(n); a > o; o++) {
                var c = n[o],
                    f = r ? r(c, o, n) : c;
                t ? (o && i === f || u.push(c), i = f) : r ? m.contains(i, f) || (i.push(f), u.push(c)) : m.contains(u, c) || u.push(c);
            }
            return u;
        }, m.union = function() {
            return m.uniq(S(arguments, !0, !0));
        }, m.intersection = function(n) {
            for (var t = [], r = arguments.length, e = 0, u = O(n); u > e; e++) {
                var i = n[e];
                if (!m.contains(t, i)) {
                    for (var o = 1; r > o && m.contains(arguments[o], i); o++);
                    o === r && t.push(i);
                }
            }
            return t;
        }, m.difference = function(n) {
            var t = S(arguments, !0, !0, 1);
            return m.filter(n, function(n) {
                return !m.contains(t, n);
            });
        }, m.zip = function() {
            return m.unzip(arguments);
        }, m.unzip = function(n) {
            for (var t = n && m.max(n, O).length || 0, r = Array(t), e = 0; t > e; e++) r[e] = m.pluck(n, e);
            return r;
        }, m.object = function(n, t) {
            for (var r = {}, e = 0, u = O(n); u > e; e++) t ? r[n[e]] = t[e] : r[n[e][0]] = n[e][1];
            return r;
        }, m.findIndex = t(1), m.findLastIndex = t(-1), m.sortedIndex = function(n, t, r, e) {
            r = x(r, e, 1);
            for (var u = r(t), i = 0, o = O(n); o > i;) {
                var a = Math.floor((i + o) / 2);
                r(n[a]) < u ? i = a + 1 : o = a;
            }
            return i;
        }, m.indexOf = r(1, m.findIndex, m.sortedIndex), m.lastIndexOf = r(-1, m.findLastIndex),
        m.range = function(n, t, r) {
            null == t && (t = n || 0, n = 0), r = r || 1;
            for (var e = Math.max(Math.ceil((t - n) / r), 0), u = Array(e), i = 0; e > i; i++,
                n += r) u[i] = n;
            return u;
        };
    var E = function(n, t, r, e, u) {
        if (!(e instanceof t)) return n.apply(r, u);
        var i = j(n.prototype),
            o = n.apply(i, u);
        return m.isObject(o) ? o : i;
    };
    m.bind = function(n, t) {
        if (g && n.bind === g) return g.apply(n, l.call(arguments, 1));
        if (!m.isFunction(n)) throw new TypeError("Bind must be called on a function");
        var r = l.call(arguments, 2),
            e = function() {
                return E(n, e, t, this, r.concat(l.call(arguments)));
            };
        return e;
    }, m.partial = function(n) {
        var t = l.call(arguments, 1),
            r = function() {
                for (var e = 0, u = t.length, i = Array(u), o = 0; u > o; o++) i[o] = t[o] === m ? arguments[e++] : t[o];
                for (; e < arguments.length;) i.push(arguments[e++]);
                return E(n, r, this, this, i);
            };
        return r;
    }, m.bindAll = function(n) {
        var t, r, e = arguments.length;
        if (1 >= e) throw new Error("bindAll must be passed function names");
        for (t = 1; e > t; t++) r = arguments[t], n[r] = m.bind(n[r], n);
        return n;
    }, m.memoize = function(n, t) {
        var r = function(e) {
            var u = r.cache,
                i = "" + (t ? t.apply(this, arguments) : e);
            return m.has(u, i) || (u[i] = n.apply(this, arguments)), u[i];
        };
        return r.cache = {}, r;
    }, m.delay = function(n, t) {
        var r = l.call(arguments, 2);
        return setTimeout(function() {
            return n.apply(null, r);
        }, t);
    }, m.defer = m.partial(m.delay, m, 1), m.throttle = function(n, t, r) {
        var e, u, i, o = null,
            a = 0;
        r || (r = {});
        var c = function() {
            a = r.leading === !1 ? 0 : m.now(), o = null, i = n.apply(e, u), o || (e = u = null);
        };
        return function() {
            var f = m.now();
            a || r.leading !== !1 || (a = f);
            var l = t - (f - a);
            return e = this, u = arguments, 0 >= l || l > t ? (o && (clearTimeout(o), o = null),
                    a = f, i = n.apply(e, u), o || (e = u = null)) : o || r.trailing === !1 || (o = setTimeout(c, l)),
                i;
        };
    }, m.debounce = function(n, t, r) {
        var e, u, i, o, a, c = function() {
            var f = m.now() - o;
            t > f && f >= 0 ? e = setTimeout(c, t - f) : (e = null, r || (a = n.apply(i, u),
                e || (i = u = null)));
        };
        return function() {
            i = this, u = arguments, o = m.now();
            var f = r && !e;
            return e || (e = setTimeout(c, t)), f && (a = n.apply(i, u), i = u = null), a;
        };
    }, m.wrap = function(n, t) {
        return m.partial(t, n);
    }, m.negate = function(n) {
        return function() {
            return !n.apply(this, arguments);
        };
    }, m.compose = function() {
        var n = arguments,
            t = n.length - 1;
        return function() {
            for (var r = t, e = n[t].apply(this, arguments); r--;) e = n[r].call(this, e);
            return e;
        };
    }, m.after = function(n, t) {
        return function() {
            return --n < 1 ? t.apply(this, arguments) : void 0;
        };
    }, m.before = function(n, t) {
        var r;
        return function() {
            return --n > 0 && (r = t.apply(this, arguments)), 1 >= n && (t = null), r;
        };
    }, m.once = m.partial(m.before, 2);
    var M = !{
            toString: null
        }.propertyIsEnumerable("toString"),
        I = ["valueOf", "isPrototypeOf", "toString", "propertyIsEnumerable", "hasOwnProperty", "toLocaleString"];
    m.keys = function(n) {
        if (!m.isObject(n)) return [];
        if (v) return v(n);
        var t = [];
        for (var r in n) m.has(n, r) && t.push(r);
        return M && e(n, t), t;
    }, m.allKeys = function(n) {
        if (!m.isObject(n)) return [];
        var t = [];
        for (var r in n) t.push(r);
        return M && e(n, t), t;
    }, m.values = function(n) {
        for (var t = m.keys(n), r = t.length, e = Array(r), u = 0; r > u; u++) e[u] = n[t[u]];
        return e;
    }, m.mapObject = function(n, t, r) {
        t = x(t, r);
        for (var e, u = m.keys(n), i = u.length, o = {}, a = 0; i > a; a++) e = u[a], o[e] = t(n[e], e, n);
        return o;
    }, m.pairs = function(n) {
        for (var t = m.keys(n), r = t.length, e = Array(r), u = 0; r > u; u++) e[u] = [t[u], n[t[u]]];
        return e;
    }, m.invert = function(n) {
        for (var t = {}, r = m.keys(n), e = 0, u = r.length; u > e; e++) t[n[r[e]]] = r[e];
        return t;
    }, m.functions = m.methods = function(n) {
        var t = [];
        for (var r in n) m.isFunction(n[r]) && t.push(r);
        return t.sort();
    }, m.extend = _(m.allKeys), m.extendOwn = m.assign = _(m.keys), m.findKey = function(n, t, r) {
        t = x(t, r);
        for (var e, u = m.keys(n), i = 0, o = u.length; o > i; i++)
            if (e = u[i], t(n[e], e, n)) return e;
    }, m.pick = function(n, t, r) {
        var e, u, i = {},
            o = n;
        if (null == o) return i;
        m.isFunction(t) ? (u = m.allKeys(o), e = b(t, r)) : (u = S(arguments, !1, !1, 1),
            e = function(n, t, r) {
                return t in r;
            }, o = Object(o));
        for (var a = 0, c = u.length; c > a; a++) {
            var f = u[a],
                l = o[f];
            e(l, f, o) && (i[f] = l);
        }
        return i;
    }, m.omit = function(n, t, r) {
        if (m.isFunction(t)) t = m.negate(t);
        else {
            var e = m.map(S(arguments, !1, !1, 1), String);
            t = function(n, t) {
                return !m.contains(e, t);
            };
        }
        return m.pick(n, t, r);
    }, m.defaults = _(m.allKeys, !0), m.create = function(n, t) {
        var r = j(n);
        return t && m.extendOwn(r, t), r;
    }, m.clone = function(n) {
        return m.isObject(n) ? m.isArray(n) ? n.slice() : m.extend({}, n) : n;
    }, m.tap = function(n, t) {
        return t(n), n;
    }, m.isMatch = function(n, t) {
        var r = m.keys(t),
            e = r.length;
        if (null == n) return !e;
        for (var u = Object(n), i = 0; e > i; i++) {
            var o = r[i];
            if (t[o] !== u[o] || !(o in u)) return !1;
        }
        return !0;
    };
    var N = function(n, t, r, e) {
        if (n === t) return 0 !== n || 1 / n === 1 / t;
        if (null == n || null == t) return n === t;
        n instanceof m && (n = n._wrapped), t instanceof m && (t = t._wrapped);
        var u = s.call(n);
        if (u !== s.call(t)) return !1;
        switch (u) {
            case "[object RegExp]":
            case "[object String]":
                return "" + n == "" + t;

            case "[object Number]":
                return +n !== +n ? +t !== +t : 0 === +n ? 1 / +n === 1 / t : +n === +t;

            case "[object Date]":
            case "[object Boolean]":
                return +n === +t;
        }
        var i = "[object Array]" === u;
        if (!i) {
            if ("object" != typeof n || "object" != typeof t) return !1;
            var o = n.constructor,
                a = t.constructor;
            if (o !== a && !(m.isFunction(o) && o instanceof o && m.isFunction(a) && a instanceof a) && "constructor" in n && "constructor" in t) return !1;
        }
        r = r || [], e = e || [];
        for (var c = r.length; c--;)
            if (r[c] === n) return e[c] === t;
        if (r.push(n), e.push(t), i) {
            if (c = n.length, c !== t.length) return !1;
            for (; c--;)
                if (!N(n[c], t[c], r, e)) return !1;
        } else {
            var f, l = m.keys(n);
            if (c = l.length, m.keys(t).length !== c) return !1;
            for (; c--;)
                if (f = l[c], !m.has(t, f) || !N(n[f], t[f], r, e)) return !1;
        }
        return r.pop(), e.pop(), !0;
    };
    m.isEqual = function(n, t) {
        return N(n, t);
    }, m.isEmpty = function(n) {
        return null == n ? !0 : k(n) && (m.isArray(n) || m.isString(n) || m.isArguments(n)) ? 0 === n.length : 0 === m.keys(n).length;
    }, m.isElement = function(n) {
        return !(!n || 1 !== n.nodeType);
    }, m.isArray = h || function(n) {
        return "[object Array]" === s.call(n);
    }, m.isObject = function(n) {
        var t = typeof n;
        return "function" === t || "object" === t && !!n;
    }, m.each(["Arguments", "Function", "String", "Number", "Date", "RegExp", "Error"], function(n) {
        m["is" + n] = function(t) {
            return s.call(t) === "[object " + n + "]";
        };
    }), m.isArguments(arguments) || (m.isArguments = function(n) {
        return m.has(n, "callee");
    }), "function" != typeof /./ && "object" != typeof Int8Array && (m.isFunction = function(n) {
        return "function" == typeof n || !1;
    }), m.isFinite = function(n) {
        return isFinite(n) && !isNaN(parseFloat(n));
    }, m.isNaN = function(n) {
        return m.isNumber(n) && n !== +n;
    }, m.isBoolean = function(n) {
        return n === !0 || n === !1 || "[object Boolean]" === s.call(n);
    }, m.isNull = function(n) {
        return null === n;
    }, m.isUndefined = function(n) {
        return n === void 0;
    }, m.has = function(n, t) {
        return null != n && p.call(n, t);
    }, m.noConflict = function() {
        return u._ = i, this;
    }, m.identity = function(n) {
        return n;
    }, m.constant = function(n) {
        return function() {
            return n;
        };
    }, m.noop = function() {}, m.property = w, m.propertyOf = function(n) {
        return null == n ? function() {} : function(t) {
            return n[t];
        };
    }, m.matcher = m.matches = function(n) {
        return n = m.extendOwn({}, n),
            function(t) {
                return m.isMatch(t, n);
            };
    }, m.times = function(n, t, r) {
        var e = Array(Math.max(0, n));
        t = b(t, r, 1);
        for (var u = 0; n > u; u++) e[u] = t(u);
        return e;
    }, m.random = function(n, t) {
        return null == t && (t = n, n = 0), n + Math.floor(Math.random() * (t - n + 1));
    }, m.now = Date.now || function() {
        return new Date().getTime();
    };
    var B = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;",
            "`": "&#x60;"
        },
        T = m.invert(B),
        R = function(n) {
            var t = function(t) {
                    return n[t];
                },
                r = "(?:" + m.keys(n).join("|") + ")",
                e = RegExp(r),
                u = RegExp(r, "g");
            return function(n) {
                return n = null == n ? "" : "" + n, e.test(n) ? n.replace(u, t) : n;
            };
        };
    m.escape = R(B), m.unescape = R(T), m.result = function(n, t, r) {
        var e = null == n ? void 0 : n[t];
        return e === void 0 && (e = r), m.isFunction(e) ? e.call(n) : e;
    };
    var q = 0;
    m.uniqueId = function(n) {
        var t = ++q + "";
        return n ? n + t : t;
    }, m.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var K = /(.)^/,
        z = {
            "'": "'",
            "\\": "\\",
            "\r": "r",
            "\n": "n",
            "\u2028": "u2028",
            "\u2029": "u2029"
        },
        D = /\\|'|\r|\n|\u2028|\u2029/g,
        L = function(n) {
            return "\\" + z[n];
        };
    m.template = function(n, t, r) {
        !t && r && (t = r), t = m.defaults({}, t, m.templateSettings);
        var e = RegExp([(t.escape || K).source, (t.interpolate || K).source, (t.evaluate || K).source].join("|") + "|$", "g"),
            u = 0,
            i = "__p+='";
        n.replace(e, function(t, r, e, o, a) {
            return i += n.slice(u, a).replace(D, L), u = a + t.length, r ? i += "'+\n((__t=(" + r + "))==null?'':_.escape(__t))+\n'" : e ? i += "'+\n((__t=(" + e + "))==null?'':__t)+\n'" : o && (i += "';\n" + o + "\n__p+='"),
                t;
        }), i += "';\n", t.variable || (i = "with(obj||{}){\n" + i + "}\n"), i = "var __t,__p='',__j=Array.prototype.join," + "print=function(){__p+=__j.call(arguments,'');};\n" + i + "return __p;\n";
        try {
            var o = new Function(t.variable || "obj", "_", i);
        } catch (a) {
            throw a.source = i, a;
        }
        var c = function(n) {
                return o.call(this, n, m);
            },
            f = t.variable || "obj";
        return c.source = "function(" + f + "){\n" + i + "}", c;
    }, m.chain = function(n) {
        var t = m(n);
        return t._chain = !0, t;
    };
    var P = function(n, t) {
        return n._chain ? m(t).chain() : t;
    };
    m.mixin = function(n) {
        m.each(m.functions(n), function(t) {
            var r = m[t] = n[t];
            m.prototype[t] = function() {
                var n = [this._wrapped];
                return f.apply(n, arguments), P(this, r.apply(m, n));
            };
        });
    }, m.mixin(m), m.each(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(n) {
        var t = o[n];
        m.prototype[n] = function() {
            var r = this._wrapped;
            return t.apply(r, arguments), "shift" !== n && "splice" !== n || 0 !== r.length || delete r[0],
                P(this, r);
        };
    }), m.each(["concat", "join", "slice"], function(n) {
        var t = o[n];
        m.prototype[n] = function() {
            return P(this, t.apply(this._wrapped, arguments));
        };
    }), m.prototype.value = function() {
        return this._wrapped;
    }, m.prototype.valueOf = m.prototype.toJSON = m.prototype.value, m.prototype.toString = function() {
        return "" + this._wrapped;
    }, "function" == typeof define && define.amd && define("underscore", [], function() {
        return m;
    });
}).call(this);

(function(window, Object, Array, Error, JSON, undefined) {
    var partialComplete = varArgs(function(fn, args) {
            var numBoundArgs = args.length;
            return varArgs(function(callArgs) {
                for (var i = 0; i < callArgs.length; i++) {
                    args[numBoundArgs + i] = callArgs[i];
                }
                args.length = numBoundArgs + callArgs.length;
                return fn.apply(this, args);
            });
        }),
        compose = varArgs(function(fns) {
            var fnsList = arrayAsList(fns);

            function next(params, curFn) {
                return [apply(params, curFn)];
            }
            return varArgs(function(startParams) {
                return foldR(next, startParams, fnsList)[0];
            });
        });

    function compose2(f1, f2) {
        return function() {
            return f1.call(this, f2.apply(this, arguments));
        };
    }

    function attr(key) {
        return function(o) {
            return o[key];
        };
    }
    var lazyUnion = varArgs(function(fns) {
        return varArgs(function(params) {
            var maybeValue;
            for (var i = 0; i < len(fns); i++) {
                maybeValue = apply(params, fns[i]);
                if (maybeValue) {
                    return maybeValue;
                }
            }
        });
    });

    function apply(args, fn) {
        return fn.apply(undefined, args);
    }

    function varArgs(fn) {
        var numberOfFixedArguments = fn.length - 1,
            slice = Array.prototype.slice;
        if (numberOfFixedArguments == 0) {
            return function() {
                return fn.call(this, slice.call(arguments));
            };
        } else if (numberOfFixedArguments == 1) {
            return function() {
                return fn.call(this, arguments[0], slice.call(arguments, 1));
            };
        }
        var argsHolder = Array(fn.length);
        return function() {
            for (var i = 0; i < numberOfFixedArguments; i++) {
                argsHolder[i] = arguments[i];
            }
            argsHolder[numberOfFixedArguments] = slice.call(arguments, numberOfFixedArguments);
            return fn.apply(this, argsHolder);
        };
    }

    function flip(fn) {
        return function(a, b) {
            return fn(b, a);
        };
    }

    function lazyIntersection(fn1, fn2) {
        return function(param) {
            return fn1(param) && fn2(param);
        };
    }

    function noop() {}

    function always() {
        return true;
    }

    function functor(val) {
        return function() {
            return val;
        };
    }

    function isOfType(T, maybeSomething) {
        return maybeSomething && maybeSomething.constructor === T;
    }
    var len = attr("length"),
        isString = partialComplete(isOfType, String);

    function defined(value) {
        return value !== undefined;
    }

    function hasAllProperties(fieldList, o) {
        return o instanceof Object && all(function(field) {
            return field in o;
        }, fieldList);
    }

    function cons(x, xs) {
        return [x, xs];
    }
    var emptyList = null,
        head = attr(0),
        tail = attr(1);

    function arrayAsList(inputArray) {
        return reverseList(inputArray.reduce(flip(cons), emptyList));
    }
    var list = varArgs(arrayAsList);

    function listAsArray(list) {
        return foldR(function(arraySoFar, listItem) {
            arraySoFar.unshift(listItem);
            return arraySoFar;
        }, [], list);
    }

    function map(fn, list) {
        return list ? cons(fn(head(list)), map(fn, tail(list))) : emptyList;
    }

    function foldR(fn, startValue, list) {
        return list ? fn(foldR(fn, startValue, tail(list)), head(list)) : startValue;
    }

    function foldR1(fn, list) {
        return tail(list) ? fn(foldR1(fn, tail(list)), head(list)) : head(list);
    }

    function without(list, test, removedFn) {
        return withoutInner(list, removedFn || noop);

        function withoutInner(subList, removedFn) {
            return subList ? test(head(subList)) ? (removedFn(head(subList)), tail(subList)) : cons(head(subList), withoutInner(tail(subList), removedFn)) : emptyList;
        }
    }

    function all(fn, list) {
        return !list || fn(head(list)) && all(fn, tail(list));
    }

    function applyEach(fnList, args) {
        if (fnList) {
            head(fnList).apply(null, args);
            applyEach(tail(fnList), args);
        }
    }

    function reverseList(list) {
        function reverseInner(list, reversedAlready) {
            if (!list) {
                return reversedAlready;
            }
            return reverseInner(tail(list), cons(head(list), reversedAlready));
        }
        return reverseInner(list, emptyList);
    }

    function first(test, list) {
        return list && (test(head(list)) ? head(list) : first(test, tail(list)));
    }

    function clarinet(eventBus) {
        "use strict";
        var emitSaxKey = eventBus(SAX_KEY).emit,
            emitValueOpen = eventBus(SAX_VALUE_OPEN).emit,
            emitValueClose = eventBus(SAX_VALUE_CLOSE).emit,
            emitFail = eventBus(FAIL_EVENT).emit,
            MAX_BUFFER_LENGTH = 64 * 1024,
            stringTokenPattern = /[\\"\n]/g,
            _n = 0,
            BEGIN = _n++,
            VALUE = _n++,
            OPEN_OBJECT = _n++,
            CLOSE_OBJECT = _n++,
            OPEN_ARRAY = _n++,
            CLOSE_ARRAY = _n++,
            STRING = _n++,
            OPEN_KEY = _n++,
            CLOSE_KEY = _n++,
            TRUE = _n++,
            TRUE2 = _n++,
            TRUE3 = _n++,
            FALSE = _n++,
            FALSE2 = _n++,
            FALSE3 = _n++,
            FALSE4 = _n++,
            NULL = _n++,
            NULL2 = _n++,
            NULL3 = _n++,
            NUMBER_DECIMAL_POINT = _n++,
            NUMBER_DIGIT = _n,
            bufferCheckPosition = MAX_BUFFER_LENGTH,
            latestError, c, p, textNode = "",
            numberNode = "",
            slashed = false,
            closed = false,
            state = BEGIN,
            stack = [],
            unicodeS = null,
            unicodeI = 0,
            depth = 0,
            position = 0,
            column = 0,
            line = 1;

        function checkBufferLength() {
            var maxActual = 0;
            if (textNode.length > MAX_BUFFER_LENGTH) {
                emitError("Max buffer length exceeded: textNode");
                maxActual = Math.max(maxActual, textNode.length);
            }
            if (numberNode.length > MAX_BUFFER_LENGTH) {
                emitError("Max buffer length exceeded: numberNode");
                maxActual = Math.max(maxActual, numberNode.length);
            }
            bufferCheckPosition = MAX_BUFFER_LENGTH - maxActual + position;
        }
        eventBus(STREAM_DATA).on(handleData);
        eventBus(STREAM_END).on(handleStreamEnd);

        function emitError(errorString) {
            if (textNode) {
                emitValueOpen(textNode);
                emitValueClose();
                textNode = "";
            }
            latestError = Error(errorString + "\nLn: " + line + "\nCol: " + column + "\nChr: " + c);
            emitFail(errorReport(undefined, undefined, latestError));
        }

        function handleStreamEnd() {
            if (state == BEGIN) {
                emitValueOpen({});
                emitValueClose();
                closed = true;
                return;
            }
            if (state !== VALUE || depth !== 0) emitError("Unexpected end");
            if (textNode) {
                emitValueOpen(textNode);
                emitValueClose();
                textNode = "";
            }
            closed = true;
        }

        function whitespace(c) {
            return c == "\r" || c == "\n" || c == " " || c == "	";
        }

        function handleData(chunk) {
            if (latestError) return;
            if (closed) {
                return emitError("Cannot write after close");
            }
            var i = 0;
            c = chunk[0];
            while (c) {
                p = c;
                c = chunk[i++];
                if (!c) break;
                position++;
                if (c == "\n") {
                    line++;
                    column = 0;
                } else column++;
                switch (state) {
                    case BEGIN:
                        if (c === "{") state = OPEN_OBJECT;
                        else if (c === "[") state = OPEN_ARRAY;
                        else if (!whitespace(c)) return emitError("Non-whitespace before {[.");
                        continue;

                    case OPEN_KEY:
                    case OPEN_OBJECT:
                        if (whitespace(c)) continue;
                        if (state === OPEN_KEY) stack.push(CLOSE_KEY);
                        else {
                            if (c === "}") {
                                emitValueOpen({});
                                emitValueClose();
                                state = stack.pop() || VALUE;
                                continue;
                            } else stack.push(CLOSE_OBJECT);
                        }
                        if (c === '"') state = STRING;
                        else return emitError('Malformed object key should start with " ');
                        continue;

                    case CLOSE_KEY:
                    case CLOSE_OBJECT:
                        if (whitespace(c)) continue;
                        if (c === ":") {
                            if (state === CLOSE_OBJECT) {
                                stack.push(CLOSE_OBJECT);
                                if (textNode) {
                                    emitValueOpen({});
                                    emitSaxKey(textNode);
                                    textNode = "";
                                }
                                depth++;
                            } else {
                                if (textNode) {
                                    emitSaxKey(textNode);
                                    textNode = "";
                                }
                            }
                            state = VALUE;
                        } else if (c === "}") {
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            emitValueClose();
                            depth--;
                            state = stack.pop() || VALUE;
                        } else if (c === ",") {
                            if (state === CLOSE_OBJECT) stack.push(CLOSE_OBJECT);
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            state = OPEN_KEY;
                        } else return emitError("Bad object");
                        continue;

                    case OPEN_ARRAY:
                    case VALUE:
                        if (whitespace(c)) continue;
                        if (state === OPEN_ARRAY) {
                            emitValueOpen([]);
                            depth++;
                            state = VALUE;
                            if (c === "]") {
                                emitValueClose();
                                depth--;
                                state = stack.pop() || VALUE;
                                continue;
                            } else {
                                stack.push(CLOSE_ARRAY);
                            }
                        }
                        if (c === '"') state = STRING;
                        else if (c === "{") state = OPEN_OBJECT;
                        else if (c === "[") state = OPEN_ARRAY;
                        else if (c === "t") state = TRUE;
                        else if (c === "f") state = FALSE;
                        else if (c === "n") state = NULL;
                        else if (c === "-") {
                            numberNode += c;
                        } else if (c === "0") {
                            numberNode += c;
                            state = NUMBER_DIGIT;
                        } else if ("123456789".indexOf(c) !== -1) {
                            numberNode += c;
                            state = NUMBER_DIGIT;
                        } else return emitError("Bad value");
                        continue;

                    case CLOSE_ARRAY:
                        if (c === ",") {
                            stack.push(CLOSE_ARRAY);
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            state = VALUE;
                        } else if (c === "]") {
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            emitValueClose();
                            depth--;
                            state = stack.pop() || VALUE;
                        } else if (whitespace(c)) continue;
                        else return emitError("Bad array");
                        continue;

                    case STRING:
                        var starti = i - 1;
                        STRING_BIGLOOP: while (true) {
                            while (unicodeI > 0) {
                                unicodeS += c;
                                c = chunk.charAt(i++);
                                if (unicodeI === 4) {
                                    textNode += String.fromCharCode(parseInt(unicodeS, 16));
                                    unicodeI = 0;
                                    starti = i - 1;
                                } else {
                                    unicodeI++;
                                }
                                if (!c) break STRING_BIGLOOP;
                            }
                            if (c === '"' && !slashed) {
                                state = stack.pop() || VALUE;
                                textNode += chunk.substring(starti, i - 1);
                                if (!textNode) {
                                    emitValueOpen("");
                                    emitValueClose();
                                }
                                break;
                            }
                            if (c === "\\" && !slashed) {
                                slashed = true;
                                textNode += chunk.substring(starti, i - 1);
                                c = chunk.charAt(i++);
                                if (!c) break;
                            }
                            if (slashed) {
                                slashed = false;
                                if (c === "n") {
                                    textNode += "\n";
                                } else if (c === "r") {
                                    textNode += "\r";
                                } else if (c === "t") {
                                    textNode += "	";
                                } else if (c === "f") {
                                    textNode += "\f";
                                } else if (c === "b") {
                                    textNode += "\b";
                                } else if (c === "u") {
                                    unicodeI = 1;
                                    unicodeS = "";
                                } else {
                                    textNode += c;
                                }
                                c = chunk.charAt(i++);
                                starti = i - 1;
                                if (!c) break;
                                else continue;
                            }
                            stringTokenPattern.lastIndex = i;
                            var reResult = stringTokenPattern.exec(chunk);
                            if (!reResult) {
                                i = chunk.length + 1;
                                textNode += chunk.substring(starti, i - 1);
                                break;
                            }
                            i = reResult.index + 1;
                            c = chunk.charAt(reResult.index);
                            if (!c) {
                                textNode += chunk.substring(starti, i - 1);
                                break;
                            }
                        }
                        continue;

                    case TRUE:
                        if (!c) continue;
                        if (c === "r") state = TRUE2;
                        else return emitError("Invalid true started with t" + c);
                        continue;

                    case TRUE2:
                        if (!c) continue;
                        if (c === "u") state = TRUE3;
                        else return emitError("Invalid true started with tr" + c);
                        continue;

                    case TRUE3:
                        if (!c) continue;
                        if (c === "e") {
                            emitValueOpen(true);
                            emitValueClose();
                            state = stack.pop() || VALUE;
                        } else return emitError("Invalid true started with tru" + c);
                        continue;

                    case FALSE:
                        if (!c) continue;
                        if (c === "a") state = FALSE2;
                        else return emitError("Invalid false started with f" + c);
                        continue;

                    case FALSE2:
                        if (!c) continue;
                        if (c === "l") state = FALSE3;
                        else return emitError("Invalid false started with fa" + c);
                        continue;

                    case FALSE3:
                        if (!c) continue;
                        if (c === "s") state = FALSE4;
                        else return emitError("Invalid false started with fal" + c);
                        continue;

                    case FALSE4:
                        if (!c) continue;
                        if (c === "e") {
                            emitValueOpen(false);
                            emitValueClose();
                            state = stack.pop() || VALUE;
                        } else return emitError("Invalid false started with fals" + c);
                        continue;

                    case NULL:
                        if (!c) continue;
                        if (c === "u") state = NULL2;
                        else return emitError("Invalid null started with n" + c);
                        continue;

                    case NULL2:
                        if (!c) continue;
                        if (c === "l") state = NULL3;
                        else return emitError("Invalid null started with nu" + c);
                        continue;

                    case NULL3:
                        if (!c) continue;
                        if (c === "l") {
                            emitValueOpen(null);
                            emitValueClose();
                            state = stack.pop() || VALUE;
                        } else return emitError("Invalid null started with nul" + c);
                        continue;

                    case NUMBER_DECIMAL_POINT:
                        if (c === ".") {
                            numberNode += c;
                            state = NUMBER_DIGIT;
                        } else return emitError("Leading zero not followed by .");
                        continue;

                    case NUMBER_DIGIT:
                        if ("0123456789".indexOf(c) !== -1) numberNode += c;
                        else if (c === ".") {
                            if (numberNode.indexOf(".") !== -1) return emitError("Invalid number has two dots");
                            numberNode += c;
                        } else if (c === "e" || c === "E") {
                            if (numberNode.indexOf("e") !== -1 || numberNode.indexOf("E") !== -1) return emitError("Invalid number has two exponential");
                            numberNode += c;
                        } else if (c === "+" || c === "-") {
                            if (!(p === "e" || p === "E")) return emitError("Invalid symbol in number");
                            numberNode += c;
                        } else {
                            if (numberNode) {
                                emitValueOpen(parseFloat(numberNode));
                                emitValueClose();
                                numberNode = "";
                            }
                            i--;
                            state = stack.pop() || VALUE;
                        }
                        continue;

                    default:
                        return emitError("Unknown state: " + state);
                }
            }
            if (position >= bufferCheckPosition) checkBufferLength();
        }
    }

    function ascentManager(oboeBus, handlers) {
        "use strict";
        var listenerId = {},
            ascent;

        function stateAfter(handler) {
            return function(param) {
                ascent = handler(ascent, param);
            };
        }
        for (var eventName in handlers) {
            oboeBus(eventName).on(stateAfter(handlers[eventName]), listenerId);
        }
        oboeBus(NODE_SWAP).on(function(newNode) {
            var oldHead = head(ascent),
                key = keyOf(oldHead),
                ancestors = tail(ascent),
                parentNode;
            if (ancestors) {
                parentNode = nodeOf(head(ancestors));
                parentNode[key] = newNode;
            }
        });
        oboeBus(NODE_DROP).on(function() {
            var oldHead = head(ascent),
                key = keyOf(oldHead),
                ancestors = tail(ascent),
                parentNode;
            if (ancestors) {
                parentNode = nodeOf(head(ancestors));
                delete parentNode[key];
            }
        });
        oboeBus(ABORTING).on(function() {
            for (var eventName in handlers) {
                oboeBus(eventName).un(listenerId);
            }
        });
    }

    function parseResponseHeaders(headerStr) {
        var headers = {};
        headerStr && headerStr.split("\r\n").forEach(function(headerPair) {
            var index = headerPair.indexOf(": ");
            headers[headerPair.substring(0, index)] = headerPair.substring(index + 2);
        });
        return headers;
    }

    function isCrossOrigin(pageLocation, ajaxHost) {
        function defaultPort(protocol) {
            return {
                "http:": 80,
                "https:": 443
            }[protocol];
        }

        function portOf(location) {
            return location.port || defaultPort(location.protocol || pageLocation.protocol);
        }
        return !!(ajaxHost.protocol && ajaxHost.protocol != pageLocation.protocol || ajaxHost.host && ajaxHost.host != pageLocation.host || ajaxHost.host && portOf(ajaxHost) != portOf(pageLocation));
    }

    function parseUrlOrigin(url) {
        var URL_HOST_PATTERN = /(\w+:)?(?:\/\/)([\w.-]+)?(?::(\d+))?\/?/,
            urlHostMatch = URL_HOST_PATTERN.exec(url) || [];
        return {
            protocol: urlHostMatch[1] || "",
            host: urlHostMatch[2] || "",
            port: urlHostMatch[3] || ""
        };
    }

    function httpTransport() {
        return new XMLHttpRequest();
    }

    function streamingHttp(oboeBus, xhr, method, url, data, headers, withCredentials) {
        "use strict";
        var emitStreamData = oboeBus(STREAM_DATA).emit,
            emitFail = oboeBus(FAIL_EVENT).emit,
            numberOfCharsAlreadyGivenToCallback = 0,
            stillToSendStartEvent = true;
        oboeBus(ABORTING).on(function() {
            xhr.onreadystatechange = null;
            xhr.abort();
        });

        function handleProgress() {
            var textSoFar = xhr.responseText,
                newText = textSoFar.substr(numberOfCharsAlreadyGivenToCallback);
            if (newText) {
                emitStreamData(newText);
            }
            numberOfCharsAlreadyGivenToCallback = len(textSoFar);
        }
        if ("onprogress" in xhr) {
            xhr.onprogress = handleProgress;
        }
        xhr.onreadystatechange = function() {
            function sendStartIfNotAlready() {
                try {
                    stillToSendStartEvent && oboeBus(HTTP_START).emit(xhr.status, parseResponseHeaders(xhr.getAllResponseHeaders()));
                    stillToSendStartEvent = false;
                } catch (e) {}
            }
            switch (xhr.readyState) {
                case 2:
                case 3:
                    return sendStartIfNotAlready();

                case 4:
                    sendStartIfNotAlready();
                    var successful = String(xhr.status)[0] == 2;
                    if (successful) {
                        handleProgress();
                        oboeBus(STREAM_END).emit();
                    } else {
                        emitFail(errorReport(xhr.status, xhr.responseText));
                    }
            }
        };
        try {
            xhr.open(method, url, true);
            for (var headerName in headers) {
                xhr.setRequestHeader(headerName, headers[headerName]);
            }
            if (!isCrossOrigin(window.location, parseUrlOrigin(url))) {
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            }
            xhr.withCredentials = withCredentials;
            xhr.send(data);
        } catch (e) {
            window.setTimeout(partialComplete(emitFail, errorReport(undefined, undefined, e)), 0);
        }
    }
    var jsonPathSyntax = function() {
        var regexDescriptor = function regexDescriptor(regex) {
                return regex.exec.bind(regex);
            },
            jsonPathClause = varArgs(function(componentRegexes) {
                componentRegexes.unshift(/^/);
                return regexDescriptor(RegExp(componentRegexes.map(attr("source")).join("")));
            }),
            possiblyCapturing = /(\$?)/,
            namedNode = /([\w-_]+|\*)/,
            namePlaceholder = /()/,
            nodeInArrayNotation = /\["([^"]+)"\]/,
            numberedNodeInArrayNotation = /\[(\d+|\*)\]/,
            fieldList = /{([\w ]*?)}/,
            optionalFieldList = /(?:{([\w ]*?)})?/,
            jsonPathNamedNodeInObjectNotation = jsonPathClause(possiblyCapturing, namedNode, optionalFieldList),
            jsonPathNamedNodeInArrayNotation = jsonPathClause(possiblyCapturing, nodeInArrayNotation, optionalFieldList),
            jsonPathNumberedNodeInArrayNotation = jsonPathClause(possiblyCapturing, numberedNodeInArrayNotation, optionalFieldList),
            jsonPathPureDuckTyping = jsonPathClause(possiblyCapturing, namePlaceholder, fieldList),
            jsonPathDoubleDot = jsonPathClause(/\.\./),
            jsonPathDot = jsonPathClause(/\./),
            jsonPathBang = jsonPathClause(possiblyCapturing, /!/),
            emptyString = jsonPathClause(/$/);
        return function(fn) {
            return fn(lazyUnion(jsonPathNamedNodeInObjectNotation, jsonPathNamedNodeInArrayNotation, jsonPathNumberedNodeInArrayNotation, jsonPathPureDuckTyping), jsonPathDoubleDot, jsonPathDot, jsonPathBang, emptyString);
        };
    }();

    function namedNode(key, node) {
        return {
            key: key,
            node: node
        };
    }
    var keyOf = attr("key");
    var nodeOf = attr("node");
    var ROOT_PATH = {};

    function incrementalContentBuilder(oboeBus) {
        var emitNodeOpened = oboeBus(NODE_OPENED).emit,
            emitNodeClosed = oboeBus(NODE_CLOSED).emit,
            emitRootOpened = oboeBus(ROOT_PATH_FOUND).emit,
            emitRootClosed = oboeBus(ROOT_NODE_FOUND).emit;

        function arrayIndicesAreKeys(possiblyInconsistentAscent, newDeepestNode) {
            var parentNode = nodeOf(head(possiblyInconsistentAscent));
            return isOfType(Array, parentNode) ? keyFound(possiblyInconsistentAscent, len(parentNode), newDeepestNode) : possiblyInconsistentAscent;
        }

        function nodeOpened(ascent, newDeepestNode) {
            if (!ascent) {
                emitRootOpened(newDeepestNode);
                return keyFound(ascent, ROOT_PATH, newDeepestNode);
            }
            var arrayConsistentAscent = arrayIndicesAreKeys(ascent, newDeepestNode),
                ancestorBranches = tail(arrayConsistentAscent),
                previouslyUnmappedName = keyOf(head(arrayConsistentAscent));
            appendBuiltContent(ancestorBranches, previouslyUnmappedName, newDeepestNode);
            return cons(namedNode(previouslyUnmappedName, newDeepestNode), ancestorBranches);
        }

        function appendBuiltContent(ancestorBranches, key, node) {
            nodeOf(head(ancestorBranches))[key] = node;
        }

        function keyFound(ascent, newDeepestName, maybeNewDeepestNode) {
            if (ascent) {
                appendBuiltContent(ascent, newDeepestName, maybeNewDeepestNode);
            }
            var ascentWithNewPath = cons(namedNode(newDeepestName, maybeNewDeepestNode), ascent);
            emitNodeOpened(ascentWithNewPath);
            return ascentWithNewPath;
        }

        function nodeClosed(ascent) {
            emitNodeClosed(ascent);
            return tail(ascent) || emitRootClosed(nodeOf(head(ascent)));
        }
        var contentBuilderHandlers = {};
        contentBuilderHandlers[SAX_VALUE_OPEN] = nodeOpened;
        contentBuilderHandlers[SAX_VALUE_CLOSE] = nodeClosed;
        contentBuilderHandlers[SAX_KEY] = keyFound;
        return contentBuilderHandlers;
    }
    var jsonPathCompiler = jsonPathSyntax(function(pathNodeSyntax, doubleDotSyntax, dotSyntax, bangSyntax, emptySyntax) {
        var CAPTURING_INDEX = 1;
        var NAME_INDEX = 2;
        var FIELD_LIST_INDEX = 3;
        var headKey = compose2(keyOf, head),
            headNode = compose2(nodeOf, head);

        function nameClause(previousExpr, detection) {
            var name = detection[NAME_INDEX],
                matchesName = !name || name == "*" ? always : function(ascent) {
                    return headKey(ascent) == name;
                };
            return lazyIntersection(matchesName, previousExpr);
        }

        function duckTypeClause(previousExpr, detection) {
            var fieldListStr = detection[FIELD_LIST_INDEX];
            if (!fieldListStr) return previousExpr;
            var hasAllrequiredFields = partialComplete(hasAllProperties, arrayAsList(fieldListStr.split(/\W+/))),
                isMatch = compose2(hasAllrequiredFields, headNode);
            return lazyIntersection(isMatch, previousExpr);
        }

        function capture(previousExpr, detection) {
            var capturing = !!detection[CAPTURING_INDEX];
            if (!capturing) return previousExpr;
            return lazyIntersection(previousExpr, head);
        }

        function skip1(previousExpr) {
            if (previousExpr == always) {
                return always;
            }

            function notAtRoot(ascent) {
                return headKey(ascent) != ROOT_PATH;
            }
            return lazyIntersection(notAtRoot, compose2(previousExpr, tail));
        }

        function skipMany(previousExpr) {
            if (previousExpr == always) {
                return always;
            }
            var terminalCaseWhenArrivingAtRoot = rootExpr(),
                terminalCaseWhenPreviousExpressionIsSatisfied = previousExpr,
                recursiveCase = skip1(function(ascent) {
                    return cases(ascent);
                }),
                cases = lazyUnion(terminalCaseWhenArrivingAtRoot, terminalCaseWhenPreviousExpressionIsSatisfied, recursiveCase);
            return cases;
        }

        function rootExpr() {
            return function(ascent) {
                return headKey(ascent) == ROOT_PATH;
            };
        }

        function statementExpr(lastClause) {
            return function(ascent) {
                var exprMatch = lastClause(ascent);
                return exprMatch === true ? head(ascent) : exprMatch;
            };
        }

        function expressionsReader(exprs, parserGeneratedSoFar, detection) {
            return foldR(function(parserGeneratedSoFar, expr) {
                return expr(parserGeneratedSoFar, detection);
            }, parserGeneratedSoFar, exprs);
        }

        function generateClauseReaderIfTokenFound(tokenDetector, clauseEvaluatorGenerators, jsonPath, parserGeneratedSoFar, onSuccess) {
            var detected = tokenDetector(jsonPath);
            if (detected) {
                var compiledParser = expressionsReader(clauseEvaluatorGenerators, parserGeneratedSoFar, detected),
                    remainingUnparsedJsonPath = jsonPath.substr(len(detected[0]));
                return onSuccess(remainingUnparsedJsonPath, compiledParser);
            }
        }

        function clauseMatcher(tokenDetector, exprs) {
            return partialComplete(generateClauseReaderIfTokenFound, tokenDetector, exprs);
        }
        var clauseForJsonPath = lazyUnion(clauseMatcher(pathNodeSyntax, list(capture, duckTypeClause, nameClause, skip1)), clauseMatcher(doubleDotSyntax, list(skipMany)), clauseMatcher(dotSyntax, list()), clauseMatcher(bangSyntax, list(capture, rootExpr)), clauseMatcher(emptySyntax, list(statementExpr)), function(jsonPath) {
            throw Error('"' + jsonPath + '" could not be tokenised');
        });

        function returnFoundParser(_remainingJsonPath, compiledParser) {
            return compiledParser;
        }

        function compileJsonPathToFunction(uncompiledJsonPath, parserGeneratedSoFar) {
            var onFind = uncompiledJsonPath ? compileJsonPathToFunction : returnFoundParser;
            return clauseForJsonPath(uncompiledJsonPath, parserGeneratedSoFar, onFind);
        }
        return function(jsonPath) {
            try {
                return compileJsonPathToFunction(jsonPath, always);
            } catch (e) {
                throw Error('Could not compile "' + jsonPath + '" because ' + e.message);
            }
        };
    });

    function singleEventPubSub(eventType, newListener, removeListener) {
        var listenerTupleList, listenerList;

        function hasId(id) {
            return function(tuple) {
                return tuple.id == id;
            };
        }
        return {
            on: function(listener, listenerId) {
                var tuple = {
                    listener: listener,
                    id: listenerId || listener
                };
                if (newListener) {
                    newListener.emit(eventType, listener, tuple.id);
                }
                listenerTupleList = cons(tuple, listenerTupleList);
                listenerList = cons(listener, listenerList);
                return this;
            },
            emit: function() {
                applyEach(listenerList, arguments);
            },
            un: function(listenerId) {
                var removed;
                listenerTupleList = without(listenerTupleList, hasId(listenerId), function(tuple) {
                    removed = tuple;
                });
                if (removed) {
                    listenerList = without(listenerList, function(listener) {
                        return listener == removed.listener;
                    });
                    if (removeListener) {
                        removeListener.emit(eventType, removed.listener, removed.id);
                    }
                }
            },
            listeners: function() {
                return listenerList;
            },
            hasListener: function(listenerId) {
                var test = listenerId ? hasId(listenerId) : always;
                return defined(first(test, listenerTupleList));
            }
        };
    }

    function pubSub() {
        var singles = {},
            newListener = newSingle("newListener"),
            removeListener = newSingle("removeListener");

        function newSingle(eventName) {
            return singles[eventName] = singleEventPubSub(eventName, newListener, removeListener);
        }

        function pubSubInstance(eventName) {
            return singles[eventName] || newSingle(eventName);
        }
        ["emit", "on", "un"].forEach(function(methodName) {
            pubSubInstance[methodName] = varArgs(function(eventName, parameters) {
                apply(parameters, pubSubInstance(eventName)[methodName]);
            });
        });
        return pubSubInstance;
    }
    var _S = 1,
        NODE_OPENED = _S++,
        NODE_CLOSED = _S++,
        NODE_SWAP = _S++,
        NODE_DROP = _S++,
        FAIL_EVENT = "fail",
        ROOT_NODE_FOUND = _S++,
        ROOT_PATH_FOUND = _S++,
        HTTP_START = "start",
        STREAM_DATA = "data",
        STREAM_END = "end",
        ABORTING = _S++,
        SAX_KEY = _S++,
        SAX_VALUE_OPEN = _S++,
        SAX_VALUE_CLOSE = _S++;

    function errorReport(statusCode, body, error) {
        try {
            var jsonBody = JSON.parse(body);
        } catch (e) {}
        return {
            statusCode: statusCode,
            body: body,
            jsonBody: jsonBody,
            thrown: error
        };
    }

    function patternAdapter(oboeBus, jsonPathCompiler) {
        var predicateEventMap = {
            node: oboeBus(NODE_CLOSED),
            path: oboeBus(NODE_OPENED)
        };

        function emitMatchingNode(emitMatch, node, ascent) {
            var descent = reverseList(ascent);
            emitMatch(node, listAsArray(tail(map(keyOf, descent))), listAsArray(map(nodeOf, descent)));
        }

        function addUnderlyingListener(fullEventName, predicateEvent, compiledJsonPath) {
            var emitMatch = oboeBus(fullEventName).emit;
            predicateEvent.on(function(ascent) {
                var maybeMatchingMapping = compiledJsonPath(ascent);
                if (maybeMatchingMapping !== false) {
                    emitMatchingNode(emitMatch, nodeOf(maybeMatchingMapping), ascent);
                }
            }, fullEventName);
            oboeBus("removeListener").on(function(removedEventName) {
                if (removedEventName == fullEventName) {
                    if (!oboeBus(removedEventName).listeners()) {
                        predicateEvent.un(fullEventName);
                    }
                }
            });
        }
        oboeBus("newListener").on(function(fullEventName) {
            var match = /(node|path):(.*)/.exec(fullEventName);
            if (match) {
                var predicateEvent = predicateEventMap[match[1]];
                if (!predicateEvent.hasListener(fullEventName)) {
                    addUnderlyingListener(fullEventName, predicateEvent, jsonPathCompiler(match[2]));
                }
            }
        });
    }

    function instanceApi(oboeBus, contentSource) {
        var oboeApi, fullyQualifiedNamePattern = /^(node|path):./,
            rootNodeFinishedEvent = oboeBus(ROOT_NODE_FOUND),
            emitNodeDrop = oboeBus(NODE_DROP).emit,
            emitNodeSwap = oboeBus(NODE_SWAP).emit,
            addListener = varArgs(function(eventId, parameters) {
                if (oboeApi[eventId]) {
                    apply(parameters, oboeApi[eventId]);
                } else {
                    var event = oboeBus(eventId),
                        listener = parameters[0];
                    if (fullyQualifiedNamePattern.test(eventId)) {
                        addForgettableCallback(event, listener);
                    } else {
                        event.on(listener);
                    }
                }
                return oboeApi;
            }),
            removeListener = function(eventId, p2, p3) {
                if (eventId == "done") {
                    rootNodeFinishedEvent.un(p2);
                } else if (eventId == "node" || eventId == "path") {
                    oboeBus.un(eventId + ":" + p2, p3);
                } else {
                    var listener = p2;
                    oboeBus(eventId).un(listener);
                }
                return oboeApi;
            };

        function addProtectedCallback(eventName, callback) {
            oboeBus(eventName).on(protectedCallback(callback), callback);
            return oboeApi;
        }

        function addForgettableCallback(event, callback, listenerId) {
            listenerId = listenerId || callback;
            var safeCallback = protectedCallback(callback);
            event.on(function() {
                var discard = false;
                oboeApi.forget = function() {
                    discard = true;
                };
                apply(arguments, safeCallback);
                delete oboeApi.forget;
                if (discard) {
                    event.un(listenerId);
                }
            }, listenerId);
            return oboeApi;
        }

        function protectedCallback(callback) {
            return function() {
                try {
                    return callback.apply(oboeApi, arguments);
                } catch (e) {
                    oboeBus(FAIL_EVENT).emit(errorReport(undefined, undefined, e));
                }
            };
        }

        function fullyQualifiedPatternMatchEvent(type, pattern) {
            return oboeBus(type + ":" + pattern);
        }

        function wrapCallbackToSwapNodeIfSomethingReturned(callback) {
            return function() {
                var returnValueFromCallback = callback.apply(this, arguments);
                if (defined(returnValueFromCallback)) {
                    if (returnValueFromCallback == oboe.drop) {
                        emitNodeDrop();
                    } else {
                        emitNodeSwap(returnValueFromCallback);
                    }
                }
            };
        }

        function addSingleNodeOrPathListener(eventId, pattern, callback) {
            var effectiveCallback;
            if (eventId == "node") {
                effectiveCallback = wrapCallbackToSwapNodeIfSomethingReturned(callback);
            } else {
                effectiveCallback = callback;
            }
            addForgettableCallback(fullyQualifiedPatternMatchEvent(eventId, pattern), effectiveCallback, callback);
        }

        function addMultipleNodeOrPathListeners(eventId, listenerMap) {
            for (var pattern in listenerMap) {
                addSingleNodeOrPathListener(eventId, pattern, listenerMap[pattern]);
            }
        }

        function addNodeOrPathListenerApi(eventId, jsonPathOrListenerMap, callback) {
            if (isString(jsonPathOrListenerMap)) {
                addSingleNodeOrPathListener(eventId, jsonPathOrListenerMap, callback);
            } else {
                addMultipleNodeOrPathListeners(eventId, jsonPathOrListenerMap);
            }
            return oboeApi;
        }
        oboeBus(ROOT_PATH_FOUND).on(function(rootNode) {
            oboeApi.root = functor(rootNode);
        });
        oboeBus(HTTP_START).on(function(_statusCode, headers) {
            oboeApi.header = function(name) {
                return name ? headers[name] : headers;
            };
        });
        return oboeApi = {
            on: addListener,
            addListener: addListener,
            removeListener: removeListener,
            emit: oboeBus.emit,
            node: partialComplete(addNodeOrPathListenerApi, "node"),
            path: partialComplete(addNodeOrPathListenerApi, "path"),
            done: partialComplete(addForgettableCallback, rootNodeFinishedEvent),
            start: partialComplete(addProtectedCallback, HTTP_START),
            fail: oboeBus(FAIL_EVENT).on,
            abort: oboeBus(ABORTING).emit,
            header: noop,
            root: noop,
            source: contentSource
        };
    }

    function wire(httpMethodName, contentSource, body, headers, withCredentials) {
        var oboeBus = pubSub();
        if (contentSource) {
            streamingHttp(oboeBus, httpTransport(), httpMethodName, contentSource, body, headers, withCredentials);
        }
        clarinet(oboeBus);
        ascentManager(oboeBus, incrementalContentBuilder(oboeBus));
        patternAdapter(oboeBus, jsonPathCompiler);
        return instanceApi(oboeBus, contentSource);
    }

    function applyDefaults(passthrough, url, httpMethodName, body, headers, withCredentials, cached) {
        headers = headers ? JSON.parse(JSON.stringify(headers)) : {};
        if (body) {
            if (!isString(body)) {
                body = JSON.stringify(body);
                headers["Content-Type"] = headers["Content-Type"] || "application/json";
            }
        } else {
            body = null;
        }

        function modifiedUrl(baseUrl, cached) {
            if (cached === false) {
                if (baseUrl.indexOf("?") == -1) {
                    baseUrl += "?";
                } else {
                    baseUrl += "&";
                }
                baseUrl += "_=" + new Date().getTime();
            }
            return baseUrl;
        }
        return passthrough(httpMethodName || "GET", modifiedUrl(url, cached), body, headers, withCredentials || false);
    }

    function oboe(arg1) {
        var nodeStreamMethodNames = list("resume", "pause", "pipe"),
            isStream = partialComplete(hasAllProperties, nodeStreamMethodNames);
        if (arg1) {
            if (isStream(arg1) || isString(arg1)) {
                return applyDefaults(wire, arg1);
            } else {
                return applyDefaults(wire, arg1.url, arg1.method, arg1.body, arg1.headers, arg1.withCredentials, arg1.cached);
            }
        } else {
            return wire();
        }
    }
    oboe.drop = function() {
        return oboe.drop;
    };
    if (typeof define === "function" && define.amd) {
        define("oboe", [], function() {
            return oboe;
        });
    } else if (typeof exports === "object") {
        module.exports = oboe;
    } else {
        window.oboe = oboe;
    }
})(function() {
    try {
        return window;
    } catch (e) {
        return self;
    }
}(), Object, Array, Error, JSON);

define("modules/controllers/chromecast_receiver", ["modules/definitions/standardmodule", "underscore", "oboe"], function(parentModel, _, oboe) {
    function _thizOBJ_(o) {
        var defaults = {
            scope: true,
            type: "chromecast_receiver",
            author: "Jonathan Robles",
            lasteditby: "",
            playlist: [],
            playItemFormat: {}
        };
        defaults = this.deepExtend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {};
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._var({});
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        var parent = this;
        parent.notify("Trace", "refresh");
        oboe("whatip.php").node(function() {
            alert("ERROR");
        }).done(function(data) {
            trace("IP:" + JSON.stringify(data));
        }).fail(function() {
            console.log(":( cant get IP!");
        });
        var playerDiv = document.getElementById("player");
        try {
            window.castReceiverManager = window.cast.receiver.CastReceiverManager.getInstance();
            window.customMessageBus = castReceiverManager.getCastMessageBus(AETN.namespace, cast.receiver.CastMessageBus.MessageType.JSON);
            window.customMessageBus.onMessage = function(getData) {
                var senderId = getData.senderId;
                var returnData = getData.data;
                switch (returnData.command) {
                    case "kitchenSink":
                        trace("onMessage received from " + senderId + ': sendMessage("' + AETN.namespace + '",' + JSON.stringify(returnData) + ");", "#0f0", "#000");
                        window.customMessageBus.send(senderId, {
                            command: returnData.command,
                            data: AECustomPlayer.sessionVars
                        });
                        break;

                    case "sessionStatus":
                        trace("onMessage received from " + senderId + ': sendMessage("' + AETN.namespace + '",' + JSON.stringify(returnData) + ");", "#0f0", "#000");
                        window.customMessageBus.send(senderId, {
                            command: returnData.command,
                            data: AECustomPlayer.sessionVars.media.customData
                        });
                        break;

                    case "mediaTime":
                        trace("onMessage received from " + senderId + ': sendMessage("' + AETN.namespace + '",' + JSON.stringify(returnData) + ");", "#0f0", "#000");
                        var mediaObj = AECustomPlayer.sessionVars.media;
                        var timeObject = {
                            currentTime: mediaObj.currentTime,
                            streamTimeWithAds: mediaObj.streamTimeWithAds,
                            streamTimeWithoutAds: mediaObj.streamTimeWithoutAds,
                            durationWithAds: mediaObj.duration,
                            durationWithoutAds: mediaObj.rawFeedData.duration
                        };
                        window.customMessageBus.send(senderId, {
                            command: returnData.command,
                            data: timeObject
                        });
                        break;

                    case "mediaTimetoTimeWithAds":
                        trace("onMessage received from " + senderId + ': sendMessage("' + AETN.namespace + '",' + JSON.stringify(returnData) + ");", "#0f0", "#000");
                        var streamTimeWithAds = AECustomPlayer.streamData.getStreamTimeWithAds(Number(returnData.data));
                        window.customMessageBus.send(senderId, {
                            command: returnData.command,
                            data: streamTimeWithAds
                        });
                        break;

                    case "mediaTimetoTimeWithoutAds":
                        trace("onMessage received from " + senderId + ': sendMessage("' + AETN.namespace + '",' + JSON.stringify(returnData) + ");", "#0f0", "#000");
                        var streamTimeWithAds = AECustomPlayer.streamData.getStreamTimeWithoutAds(Number(returnData.data));
                        window.customMessageBus.send(senderId, {
                            command: returnData.command,
                            data: streamTimeWithAds
                        });
                        break;

                    case "forceTimeline":
                        trace("onMessage received from " + senderId + ': sendMessage("' + AETN.namespace + '",' + JSON.stringify(returnData) + ");", "#0f0", "#000");
                        if ($("html").hasClass("forcetimeline")) {
                            trace("forceTimeline toggled OFF", "#000", "#F2F055");
                            $("html").removeClass("forcetimeline");
                        } else {
                            trace("forceTimeline toggled ON", "#000", "#F2F055");
                            $("html").addClass("forcetimeline");
                        }
                        break;

                    case "hideConsole":
                        trace("onMessage received from " + senderId + ': sendMessage("' + AETN.namespace + '",' + JSON.stringify(returnData) + ");", "#0f0", "#000");
                        if (String(returnData.data) == "true") {
                            trace("Console toggled OFF", "#000", "#F2F055");
                            $(".debugConsole").css("display", "none");
                        } else {
                            trace("Console toggled ON", "#000", "#F2F055");
                            $(".debugConsole").css("display", "block");
                        }
                        break;

                    case "enableCaptions":
                        trace("onMessage received from " + senderId + ': sendMessage("' + AETN.namespace + '",' + JSON.stringify(returnData) + ");", "#0f0", "#000");
                        var cc_data = returnData.data == "true";
                        AECustomPlayer.sessionVars.media.customData.captions = cc_data;
                        if (AECustomPlayer.sessionVars.media.streamSource == "mDialog") {
                            trace("mDialog captions set to :" + cc_data, "#000", "#F2F055");
                            playerObject.player_.enableCaptions(cc_data, cast.player.api.CaptionsType.CEA608);
                        } else {
                            trace("non-mDialog captions set to :" + cc_data, "#000", "#F2F055");
                            playerObject.player_.enableCaptions(cc_data);
                        }
                        mediaManager.setMediaInformation(AECustomPlayer.sessionVars.media);
                        window.customMessageBus.broadcast({
                            command: returnData.command,
                            data: AECustomPlayer.sessionVars.media.customData
                        });
                        break;

                    default:
                        trace("onMessage received from " + senderId + ': sendMessage("' + AETN.namespace + '",' + JSON.stringify(returnData) + ");", "#f00", "#000");
                        window.customMessageBus.send(senderId, {
                            command: returnData.command,
                            data: "failed, no such command"
                        });
                }
            };
        } catch (err) {
            trace("custom messaging failed for " + AETN.namespace);
        }
        new AECustomPlayer.CastPlayer(playerDiv).start();
        trace("AECustomReceiver Started for channel:" + AETN.brandID + " for environment:" + AETN.environment + " expecting ApplicationID:" + AETN.applicationID + " using namespace:" + AETN.namespace);
    };
    _thizOBJ_.prototype.kill = function() {
        var parent = this;
        parent.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype._playURL_HLS = function(getURL) {
        var parent = this;
        parent.notify("Trace", "_playURL_HLS(" + getURL + ")");
        parent._getSignedURL(getURL);
    };
    _thizOBJ_.prototype._returnProxyUrl = function(getURL) {
        if (getURL.substring(0, 4).toLowerCase() == "http") {
            var htLoc = getURL.indexOf("://");
            getURL = getURL.substring(htLoc + 3, getURL.length);
        }
        var signinURLwProxy = AETN.proxyWithSigninURL + encodeURIComponent(getURL);
        return signinURLwProxy;
    };
    _thizOBJ_.prototype._omnitureTrack = function(getEvents, options) {
        var s_account = AETN.adobeReportSuiteID;
        var s = s_gi(s_account);
        var getFeedData = AECustomPlayer.sessionVars.media.rawFeedData;
        AECustomPlayer.sessionVars.media.customData.trackingData = AECustomPlayer.sessionVars.media.customData.trackingData || {};
        s.events = s.linkTrackEvents = getEvents;
        s.pageName = "";
        s.watch = {
            brand: AETN.brandID_omnitureName,
            originatingplatform: AECustomPlayer.sessionVars.media.customData.trackingData.origin_platform,
            adid: AECustomPlayer.sessionVars.media.customData.trackingData.origin_adid,
            kruxid: AECustomPlayer.sessionVars.media.customData.trackingData.origin_kruxid,
            sessionid: AECustomPlayer.sessionVars.lastSenderId,
            videocliptitle: getFeedData.title,
            videocategory: getFeedData.tvNtvMix,
            videolf: getFeedData.isLongForm,
            videoseason: getFeedData.tvSeasonNumber,
            videotv: getFeedData.tvNtvMix,
            videoepisode: getFeedData.tvSeasonEpisodeNumber,
            videosubcategory: getFeedData.clipType,
            videofastfollow: getFeedData.isFastFollow,
            videoduration: getFeedData.duration,
            videofullscreen: undefined,
            videorequiresauthentication: getFeedData.isBehindWall,
            videopplid: getFeedData.programId
        };
        s.watch = this.deepExtend(s.watch, AETN.omnitureDefaults);
        s.watch = this.deepExtend(s.watch, options);
        console.log("OMNITURE OBJECT:" + JSON.stringify(s.watch));
        var s_code = s.t();
        if (s_code) document.write(s_code);
    };
    _thizOBJ_.prototype._getFeedData = function(o, success_cb, fail_cb) {
        trace("_getFeedData!!!", "#fff", "#000");
        AECustomPlayer.sessionVars.media.streamSource = "MPX";
        var parent = this;
        var defaults = {
            assetKey: undefined
        };
        defaults = this.deepExtend(defaults, o);
        if (defaults.assetKey == undefined) {} else {
            var lookUpURL = AETN.titleFeedNew + defaults.assetKey;
            var proxyWithLookUpURL = AETN.proxyWithSigninURL + lookUpURL;
            var proxyWithLookUpURL = proxyWithLookUpURL.replace("&url=http://", "&url=");
            console.log("proxyWithLookUpURL: " + proxyWithLookUpURL);
            oboe(proxyWithLookUpURL).done(function(data) {
                var returnDATA = data.contents.results[0];
                success_cb(returnDATA);
            }).fail(function() {
                trace("_getFeedData fail for " + defaults.assetKey);
                fail_cb("please check feed: " + decodeURI(lookUpURL));
            });
        }
    };
    _thizOBJ_.prototype._getMPXURL = function(o, success_cb, fail_cb) {
        trace("MPX", "#fff", "#000", "#streamSource");
        AECustomPlayer.sessionVars.media.streamSource = "MPX";
        var parent = this;
        var defaults = {
            assetKey: "630362691506"
        };
        defaults = this.deepExtend(defaults, o);
        var convertBrandID = AETN.brandID;
        if (convertBrandID == "his") {
            convertBrandID = "history";
        }
        var lookUpURL = AETN.titleFeedNew + defaults.assetKey;
        var proxyWithLookUpURL = AETN.proxyWithSigninURL + lookUpURL;
        var proxyWithLookUpURL = proxyWithLookUpURL.replace("&url=http://", "&url=");
        console.log("proxyWithLookUpURL: " + proxyWithLookUpURL);
        var returnURL = AECustomPlayer.sessionVars.media.rawFeedData.publicUrl + "?" + AETN.mDialog_playoptions;
        success_cb(returnURL);
    };
    _thizOBJ_.prototype._getMDialogURL = function(o, success_cb, fail_cb) {
        trace("mDialog", "#fff", "#000", "#streamSource");
        AECustomPlayer.sessionVars.media.streamSource = "mDialog";
        var defaults = {
            senderData: {
                debug: true,
                sessionContext: {
                    subDomain: "aetn-vod",
                    apiKey: AETN.apiKey,
                    applicationKey: AETN.applicationKey
                },
                streamContext: {
                    assetKey: undefined,
                    adDecisioningData: {
                        stream_activity_key: AETN.streamActivityKey,
                        adconfig: "staging"
                    },
                    trackingData: {
                        VISITOR_ID: AECustomPlayer.sessionVars.media.customData["userID"],
                        device_platform: "chromecast",
                        videocategory: AECustomPlayer.sessionVars.media.rawFeedData.tvNtvMix,
                        videocliptitle: AECustomPlayer.sessionVars.media.rawFeedData.title,
                        videolf: AECustomPlayer.sessionVars.media.rawFeedData.isLongForm,
                        videofastfollow: AECustomPlayer.sessionVars.media.rawFeedData.isFastFollow,
                        videotv: AECustomPlayer.sessionVars.media.rawFeedData.rating,
                        videochapter: AECustomPlayer.sessionVars.media.rawFeedData.tvSeasonEpisodeNumber,
                        videoseason: AECustomPlayer.sessionVars.media.rawFeedData.tvSeasonNumber,
                        origin_platform: "missing from sender",
                        origin_session_id: "missing from sender",
                        origin_visitor_id: "missing from sender",
                        origin_device_id: "missing from sender"
                    }
                }
            },
            receiverData: {
                mediaElement: playerObject.mediaElement_,
                mediaManager: playerObject.mediaManager_
            }
        };
        defaults.senderData.streamContext = this.deepExtend(defaults.senderData.streamContext, o);
        defaults.senderData.streamContext.trackingData = this.deepExtend(defaults.senderData.streamContext.trackingData, AECustomPlayer.sessionVars.media.customData.trackingData);
        if (AECustomPlayer.sessionVars.media.customData["userID"] && defaults.senderData.streamContext.trackingData.origin_visitor_id == undefined) {
            defaults.senderData.streamContext.trackingData.origin_visitor_id = AECustomPlayer.sessionVars.media.customData["userID"];
        }
        trace("_getMDialogURL received senderData data: " + JSON.stringify(defaults.senderData));
        var streamManager = new mdialog.cast.api.StreamManager(defaults.senderData, defaults.receiverData);
        window.streamManager = streamManager;
        trace("making mdialog streamManager");
        streamManager.onAbort = function(error) {
            trace("mDialog STREAM FAILED : " + error, "#fff", "#f00");
            fail_cb("mDialog stream failed");
        };
        streamManager.onStreamFailed = function(error) {
            trace("mDialog onAbort : " + error, "#fff", "#f00");
            fail_cb("mDialog onAbort");
        };
        streamManager.onStreamExpired = function(error) {
            trace("mDialog onStreamExpired : " + error, "#fff", "#f00");
            fail_cb("mDialog onAbort");
        };
        streamManager.onNearestPreviousAdBreak = function(data) {};
        streamManager.onStreamLoaded = function(data) {
            var returnURL = data.hdManifestURL;
            trace("_returnURL: " + returnURL);
            AECustomPlayer.streamData = data;
            success_cb(returnURL);
        };
        streamManager.getStream(defaults.senderData.streamContext.assetKey);
    };
    _thizOBJ_.prototype._getSignedURL = function(getURL, success_cb, fail_cb) {
        var parent = this;
        trace("_getSignedURL(" + getURL + ")");
        parent.notify("Trace", "_getSignedURL(" + getURL + ")");
        var signinURL = AETN.signinURL + encodeURIComponent(getURL);
        trace("signinURL=" + signinURL);
        var proxyWithSigninURL = AETN.proxyWithSigninURL + signinURL;
        var proxyWithSigninURL = proxyWithSigninURL.replace("&url=http://", "&url=");
        console.log("proxyWithSigninURL=" + decodeURIComponent(proxyWithSigninURL));
        oboe(proxyWithSigninURL).done(function(data) {
            var token = String(data.contents);
            console.log("received token: " + token);
            var urlwithtoken = getURL + "&sig=" + token;
            var adobeToken = AECustomPlayer.sessionVars.media.customData.auth;
            if (adobeToken != undefined && adobeToken != "") {
                urlwithtoken += "&auth=" + encodeURIComponent(adobeToken);
            }
            console.log("building url with token: " + urlwithtoken);
            trace("building url with token: " + urlwithtoken, "#000", "#0f0");
            oboe({
                url: parent._returnProxyUrl(urlwithtoken)
            }).done(function(data) {
                console.log("streamURL:" + data.status.url);
                trace("if fail pls check URL 1:" + data.status.url, "#000", "#f00");
                success_cb(data.status.url);
            }).fail(function() {
                trace("pls check URL 1:" + urlwithtoken, "#000", "#f00");
                trace("pls check URL 2:" + parent._returnProxyUrl(urlwithtoken), "#000", "#f00");
                fail_cb("failed with Signed URL: " + urlwithtoken);
            });
        }).fail(function() {
            fail_cb("failed on Getting Token from: " + proxyWithSigninURL);
        });
    };
    return _thizOBJ_;
});

require(["modules/controllers/chromecast_receiver"], function(ccReceiver) {
    window.myReceiver = new ccReceiver({});
    window.myReceiver.init();
});

define("instances/chromecast_receiver_JS.js", function() {});
