/*! chromecast_sender_mlt_dev : sender_h_mlt_dev.js */
/*! codebase: CB2016 v1.1.4 by Jonathan Robles */
/*! built:05-03-2016 [12:53:53PM] */
/*! -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

/*! Prerequisites: //www.gstatic.com/cv/js/sender/v1/cast_sender.js */

/*! Third Party Includes [start] */
/* almond, modernizr, underscore, oboe */
/*! Third Party Includes [end] */
var requirejs, require, define;

(function(undef) {
    var main, req, makeMap, handlers, defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex, foundI, foundStarMap, starI, i, j, part, baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = map && map["*"] || {};
        if (name && name.charAt(0) === ".") {
            if (baseName) {
                baseParts = baseParts.slice(0, baseParts.length - 1);
                name = name.split("/");
                lastIndex = name.length - 1;
                if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                    name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, "");
                }
                name = baseParts.concat(name);
                for (i = 0; i < name.length; i += 1) {
                    part = name[i];
                    if (part === ".") {
                        name.splice(i, 1);
                        i -= 1;
                    } else if (part === "..") {
                        if (i === 1 && (name[2] === ".." || name[0] === "..")) {
                            break;
                        } else if (i > 0) {
                            name.splice(i - 1, 2);
                            i -= 2;
                        }
                    }
                }
                name = name.join("/");
            } else if (name.indexOf("./") === 0) {
                name = name.substring(2);
            }
        }
        if ((baseParts || starMap) && map) {
            nameParts = name.split("/");
            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");
                if (baseParts) {
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join("/")];
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }
                if (foundMap) {
                    break;
                }
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }
            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }
            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join("/");
            }
        }
        return name;
    }

    function makeRequire(relName, forceSync) {
        return function() {
            var args = aps.call(arguments, 0);
            if (typeof args[0] !== "string" && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function(name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function(value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }
        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error("No " + name);
        }
        return defined[name];
    }

    function splitPrefix(name) {
        var prefix, index = name ? name.indexOf("!") : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }
    makeMap = function(name, relName) {
        var plugin, parts = splitPrefix(name),
            prefix = parts[0];
        name = parts[1];
        if (prefix) {
            prefix = normalize(prefix, relName);
            plugin = callDep(prefix);
        }
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relName));
            } else {
                name = normalize(name, relName);
            }
        } else {
            name = normalize(name, relName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }
        return {
            f: prefix ? prefix + "!" + name : name,
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function() {
            return config && config.config && config.config[name] || {};
        };
    }
    handlers = {
        require: function(name) {
            return makeRequire(name);
        },
        exports: function(name) {
            var e = defined[name];
            if (typeof e !== "undefined") {
                return e;
            } else {
                return defined[name] = {};
            }
        },
        module: function(name) {
            return {
                id: name,
                uri: "",
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };
    main = function(name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i, args = [],
            callbackType = typeof callback,
            usingExports;
        relName = relName || name;
        if (callbackType === "undefined" || callbackType === "function") {
            deps = !deps.length && callback.length ? ["require", "exports", "module"] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relName);
                depName = map.f;
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) || hasProp(waiting, depName) || hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + " missing " + depName);
                }
            }
            ret = callback ? callback.apply(defined[name], args) : undefined;
            if (name) {
                if (cjsModule && cjsModule.exports !== undef && cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    defined[name] = ret;
                }
            }
        } else if (name) {
            defined[name] = callback;
        }
    };
    requirejs = require = req = function(deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                return handlers[deps](callback);
            }
            return callDep(makeMap(deps, callback).f);
        } else if (!deps.splice) {
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }
            if (callback.splice) {
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }
        callback = callback || function() {};
        if (typeof relName === "function") {
            relName = forceSync;
            forceSync = alt;
        }
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            setTimeout(function() {
                main(undef, deps, callback, relName);
            }, 4);
        }
        return req;
    };
    req.config = function(cfg) {
        return req(cfg);
    };
    requirejs._defined = defined;
    define = function(name, deps, callback) {
        if (!deps.splice) {
            callback = deps;
            deps = [];
        }
        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };
    define.amd = {
        jQuery: true
    };
})();

define("almondLib", function() {});

window.Modernizr = function(a, b, c) {
        function B(a) {
            j.cssText = a;
        }

        function C(a, b) {
            return B(m.join(a + ";") + (b || ""));
        }

        function D(a, b) {
            return typeof a === b;
        }

        function E(a, b) {
            return !!~("" + a).indexOf(b);
        }

        function F(a, b) {
            for (var d in a) {
                var e = a[d];
                if (!E(e, "-") && j[e] !== c) return b == "pfx" ? e : !0;
            }
            return !1;
        }

        function G(a, b, d) {
            for (var e in a) {
                var f = b[a[e]];
                if (f !== c) return d === !1 ? a[e] : D(f, "function") ? f.bind(d || b) : f;
            }
            return !1;
        }

        function H(a, b, c) {
            var d = a.charAt(0).toUpperCase() + a.slice(1),
                e = (a + " " + o.join(d + " ") + d).split(" ");
            return D(b, "string") || D(b, "undefined") ? F(e, b) : (e = (a + " " + p.join(d + " ") + d).split(" "),
                G(e, b, c));
        }
        var d = "2.7.1",
            e = {},
            f = !0,
            g = b.documentElement,
            h = "modernizr",
            i = b.createElement(h),
            j = i.style,
            k, l = {}.toString,
            m = " -webkit- -moz- -o- -ms- ".split(" "),
            n = "Webkit Moz O ms",
            o = n.split(" "),
            p = n.toLowerCase().split(" "),
            q = {
                svg: "http://www.w3.org/2000/svg"
            },
            r = {},
            s = {},
            t = {},
            u = [],
            v = u.slice,
            w, x = function(a, c, d, e) {
                var f, i, j, k, l = b.createElement("div"),
                    m = b.body,
                    n = m || b.createElement("body");
                if (parseInt(d, 10))
                    while (d--) j = b.createElement("div"), j.id = e ? e[d] : h + (d + 1),
                        l.appendChild(j);
                return f = ["&#173;", '<style id="s', h, '">', a, "</style>"].join(""), l.id = h, (m ? l : n).innerHTML += f, n.appendChild(l), m || (n.style.background = "", n.style.overflow = "hidden",
                        k = g.style.overflow, g.style.overflow = "hidden", g.appendChild(n)), i = c(l, a),
                    m ? l.parentNode.removeChild(l) : (n.parentNode.removeChild(n), g.style.overflow = k), !!i;
            },
            y = function(b) {
                var c = a.matchMedia || a.msMatchMedia;
                if (c) return c(b).matches;
                var d;
                return x("@media " + b + " { #" + h + " { position: absolute; } }", function(b) {
                    d = (a.getComputedStyle ? getComputedStyle(b, null) : b.currentStyle)["position"] == "absolute";
                }), d;
            },
            z = {}.hasOwnProperty,
            A;
        !D(z, "undefined") && !D(z.call, "undefined") ? A = function(a, b) {
            return z.call(a, b);
        } : A = function(a, b) {
            return b in a && D(a.constructor.prototype[b], "undefined");
        }, Function.prototype.bind || (Function.prototype.bind = function(b) {
            var c = this;
            if (typeof c != "function") throw new TypeError();
            var d = v.call(arguments, 1),
                e = function() {
                    if (this instanceof e) {
                        var a = function() {};
                        a.prototype = c.prototype;
                        var f = new a(),
                            g = c.apply(f, d.concat(v.call(arguments)));
                        return Object(g) === g ? g : f;
                    }
                    return c.apply(b, d.concat(v.call(arguments)));
                };
            return e;
        }), r.canvas = function() {
            var a = b.createElement("canvas");
            return !!a.getContext && !!a.getContext("2d");
        }, r.touch = function() {
            var c;
            return "ontouchstart" in a || a.DocumentTouch && b instanceof DocumentTouch ? c = !0 : x(["@media (", m.join("touch-enabled),("), h, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function(a) {
                c = a.offsetTop === 9;
            }), c;
        }, r.geolocation = function() {
            return "geolocation" in navigator;
        }, r.cssanimations = function() {
            return H("animationName");
        }, r.csstransforms = function() {
            return !!H("transform");
        }, r.csstransforms3d = function() {
            var a = !!H("perspective");
            return a && "webkitPerspective" in g.style && x("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(b, c) {
                a = b.offsetLeft === 9 && b.offsetHeight === 3;
            }), a;
        }, r.csstransitions = function() {
            return H("transition");
        }, r.fontface = function() {
            var a;
            return x('@font-face {font-family:"font";src:url("https://")}', function(c, d) {
                var e = b.getElementById("smodernizr"),
                    f = e.sheet || e.styleSheet,
                    g = f ? f.cssRules && f.cssRules[0] ? f.cssRules[0].cssText : f.cssText || "" : "";
                a = /src/i.test(g) && g.indexOf(d.split(" ")[0]) === 0;
            }), a;
        }, r.video = function() {
            var a = b.createElement("video"),
                c = !1;
            try {
                if (c = !!a.canPlayType) c = new Boolean(c), c.ogg = a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""),
                    c.h264 = a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), c.webm = a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, "");
            } catch (d) {}
            return c;
        }, r.audio = function() {
            var a = b.createElement("audio"),
                c = !1;
            try {
                if (c = !!a.canPlayType) c = new Boolean(c), c.ogg = a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""),
                    c.mp3 = a.canPlayType("audio/mpeg;").replace(/^no$/, ""), c.wav = a.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""),
                    c.m4a = (a.canPlayType("audio/x-m4a;") || a.canPlayType("audio/aac;")).replace(/^no$/, "");
            } catch (d) {}
            return c;
        }, r.webworkers = function() {
            return !!a.Worker;
        }, r.svg = function() {
            return !!b.createElementNS && !!b.createElementNS(q.svg, "svg").createSVGRect;
        }, r.inlinesvg = function() {
            var a = b.createElement("div");
            return a.innerHTML = "<svg/>", (a.firstChild && a.firstChild.namespaceURI) == q.svg;
        }, r.svgclippaths = function() {
            return !!b.createElementNS && /SVGClipPath/.test(l.call(b.createElementNS(q.svg, "clipPath")));
        };
        for (var I in r) A(r, I) && (w = I.toLowerCase(), e[w] = r[I](), u.push((e[w] ? "" : "no-") + w));
        return e.addTest = function(a, b) {
                if (typeof a == "object")
                    for (var d in a) A(a, d) && e.addTest(d, a[d]);
                else {
                    a = a.toLowerCase();
                    if (e[a] !== c) return e;
                    b = typeof b == "function" ? b() : b, typeof f != "undefined" && f && (g.className += " " + (b ? "" : "no-") + a),
                        e[a] = b;
                }
                return e;
            }, B(""), i = k = null,
            function(a, b) {
                function l(a, b) {
                    var c = a.createElement("p"),
                        d = a.getElementsByTagName("head")[0] || a.documentElement;
                    return c.innerHTML = "x<style>" + b + "</style>", d.insertBefore(c.lastChild, d.firstChild);
                }

                function m() {
                    var a = s.elements;
                    return typeof a == "string" ? a.split(" ") : a;
                }

                function n(a) {
                    var b = j[a[h]];
                    return b || (b = {}, i++, a[h] = i, j[i] = b), b;
                }

                function o(a, c, d) {
                    c || (c = b);
                    if (k) return c.createElement(a);
                    d || (d = n(c));
                    var g;
                    return d.cache[a] ? g = d.cache[a].cloneNode() : f.test(a) ? g = (d.cache[a] = d.createElem(a)).cloneNode() : g = d.createElem(a),
                        g.canHaveChildren && !e.test(a) && !g.tagUrn ? d.frag.appendChild(g) : g;
                }

                function p(a, c) {
                    a || (a = b);
                    if (k) return a.createDocumentFragment();
                    c = c || n(a);
                    var d = c.frag.cloneNode(),
                        e = 0,
                        f = m(),
                        g = f.length;
                    for (; e < g; e++) d.createElement(f[e]);
                    return d;
                }

                function q(a, b) {
                    b.cache || (b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment,
                        b.frag = b.createFrag()), a.createElement = function(c) {
                        return s.shivMethods ? o(c, a, b) : b.createElem(c);
                    }, a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + m().join().replace(/[\w\-]+/g, function(a) {
                        return b.createElem(a), b.frag.createElement(a), 'c("' + a + '")';
                    }) + ");return n}")(s, b.frag);
                }

                function r(a) {
                    a || (a = b);
                    var c = n(a);
                    return s.shivCSS && !g && !c.hasCSS && (c.hasCSS = !!l(a, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")),
                        k || q(a, c), a;
                }
                var c = "3.7.0",
                    d = a.html5 || {},
                    e = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
                    f = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
                    g, h = "_html5shiv",
                    i = 0,
                    j = {},
                    k;
                (function() {
                    try {
                        var a = b.createElement("a");
                        a.innerHTML = "<xyz></xyz>", g = "hidden" in a, k = a.childNodes.length == 1 || function() {
                            b.createElement("a");
                            var a = b.createDocumentFragment();
                            return typeof a.cloneNode == "undefined" || typeof a.createDocumentFragment == "undefined" || typeof a.createElement == "undefined";
                        }();
                    } catch (c) {
                        g = !0, k = !0;
                    }
                })();
                var s = {
                    elements: d.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
                    version: c,
                    shivCSS: d.shivCSS !== !1,
                    supportsUnknownElements: k,
                    shivMethods: d.shivMethods !== !1,
                    type: "default",
                    shivDocument: r,
                    createElement: o,
                    createDocumentFragment: p
                };
                a.html5 = s, r(b);
            }(this, b), e._version = d, e._prefixes = m, e._domPrefixes = p, e._cssomPrefixes = o,
            e.mq = y, e.testProp = function(a) {
                return F([a]);
            }, e.testAllProps = H, e.testStyles = x, g.className = g.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (f ? " js " + u.join(" ") : ""),
            e;
    }(this, this.document),
    function(a, b, c) {
        function d(a) {
            return "[object Function]" == o.call(a);
        }

        function e(a) {
            return "string" == typeof a;
        }

        function f() {}

        function g(a) {
            return !a || "loaded" == a || "complete" == a || "uninitialized" == a;
        }

        function h() {
            var a = p.shift();
            q = 1, a ? a.t ? m(function() {
                ("c" == a.t ? B.injectCss : B.injectJs)(a.s, 0, a.a, a.x, a.e, 1);
            }, 0) : (a(), h()) : q = 0;
        }

        function i(a, c, d, e, f, i, j) {
            function k(b) {
                if (!o && g(l.readyState) && (u.r = o = 1, !q && h(), l.onload = l.onreadystatechange = null,
                        b)) {
                    "img" != a && m(function() {
                        t.removeChild(l);
                    }, 50);
                    for (var d in y[c]) y[c].hasOwnProperty(d) && y[c][d].onload();
                }
            }
            var j = j || B.errorTimeout,
                l = b.createElement(a),
                o = 0,
                r = 0,
                u = {
                    t: d,
                    s: c,
                    e: f,
                    a: i,
                    x: j
                };
            1 === y[c] && (r = 1, y[c] = []), "object" == a ? l.data = c : (l.src = c, l.type = a),
                l.width = l.height = "0", l.onerror = l.onload = l.onreadystatechange = function() {
                    k.call(this, r);
                }, p.splice(e, 0, u), "img" != a && (r || 2 === y[c] ? (t.insertBefore(l, s ? null : n),
                    m(k, j)) : y[c].push(l));
        }

        function j(a, b, c, d, f) {
            return q = 0, b = b || "j", e(a) ? i("c" == b ? v : u, a, b, this.i++, c, d, f) : (p.splice(this.i++, 0, a),
                1 == p.length && h()), this;
        }

        function k() {
            var a = B;
            return a.loader = {
                load: j,
                i: 0
            }, a;
        }
        var l = b.documentElement,
            m = a.setTimeout,
            n = b.getElementsByTagName("script")[0],
            o = {}.toString,
            p = [],
            q = 0,
            r = "MozAppearance" in l.style,
            s = r && !!b.createRange().compareNode,
            t = s ? l : n.parentNode,
            l = a.opera && "[object Opera]" == o.call(a.opera),
            l = !!b.attachEvent && !l,
            u = r ? "object" : l ? "script" : "img",
            v = l ? "script" : u,
            w = Array.isArray || function(a) {
                return "[object Array]" == o.call(a);
            },
            x = [],
            y = {},
            z = {
                timeout: function(a, b) {
                    return b.length && (a.timeout = b[0]), a;
                }
            },
            A, B;
        B = function(a) {
            function b(a) {
                var a = a.split("!"),
                    b = x.length,
                    c = a.pop(),
                    d = a.length,
                    c = {
                        url: c,
                        origUrl: c,
                        prefixes: a
                    },
                    e, f, g;
                for (f = 0; f < d; f++) g = a[f].split("="), (e = z[g.shift()]) && (c = e(c, g));
                for (f = 0; f < b; f++) c = x[f](c);
                return c;
            }

            function g(a, e, f, g, h) {
                var i = b(a),
                    j = i.autoCallback;
                i.url.split(".").pop().split("?").shift(), i.bypass || (e && (e = d(e) ? e : e[a] || e[g] || e[a.split("/").pop().split("?")[0]]),
                    i.instead ? i.instead(a, e, f, g, h) : (y[i.url] ? i.noexec = !0 : y[i.url] = 1,
                        f.load(i.url, i.forceCSS || !i.forceJS && "css" == i.url.split(".").pop().split("?").shift() ? "c" : c, i.noexec, i.attrs, i.timeout), (d(e) || d(j)) && f.load(function() {
                            k(), e && e(i.origUrl, h, g), j && j(i.origUrl, h, g), y[i.url] = 2;
                        })));
            }

            function h(a, b) {
                function c(a, c) {
                    if (a) {
                        if (e(a)) c || (j = function() {
                            var a = [].slice.call(arguments);
                            k.apply(this, a), l();
                        }), g(a, j, b, 0, h);
                        else if (Object(a) === a)
                            for (n in m = function() {
                                    var b = 0,
                                        c;
                                    for (c in a) a.hasOwnProperty(c) && b++;
                                    return b;
                                }(), a) a.hasOwnProperty(n) && (!c && !--m && (d(j) ? j = function() {
                                var a = [].slice.call(arguments);
                                k.apply(this, a), l();
                            } : j[n] = function(a) {
                                return function() {
                                    var b = [].slice.call(arguments);
                                    a && a.apply(this, b), l();
                                };
                            }(k[n])), g(a[n], j, b, n, h));
                    } else !c && l();
                }
                var h = !!a.test,
                    i = a.load || a.both,
                    j = a.callback || f,
                    k = j,
                    l = a.complete || f,
                    m, n;
                c(h ? a.yep : a.nope, !!i), i && c(i);
            }
            var i, j, l = this.yepnope.loader;
            if (e(a)) g(a, 0, l, 0);
            else if (w(a))
                for (i = 0; i < a.length; i++) j = a[i],
                    e(j) ? g(j, 0, l, 0) : w(j) ? B(j) : Object(j) === j && h(j, l);
            else Object(a) === a && h(a, l);
        }, B.addPrefix = function(a, b) {
            z[a] = b;
        }, B.addFilter = function(a) {
            x.push(a);
        }, B.errorTimeout = 1e4, null == b.readyState && b.addEventListener && (b.readyState = "loading",
            b.addEventListener("DOMContentLoaded", A = function() {
                b.removeEventListener("DOMContentLoaded", A, 0), b.readyState = "complete";
            }, 0)), a.yepnope = k(), a.yepnope.executeStack = h, a.yepnope.injectJs = function(a, c, d, e, i, j) {
            var k = b.createElement("script"),
                l, o, e = e || B.errorTimeout;
            k.src = a;
            for (o in d) k.setAttribute(o, d[o]);
            c = j ? h : c || f, k.onreadystatechange = k.onload = function() {
                !l && g(k.readyState) && (l = 1, c(), k.onload = k.onreadystatechange = null);
            }, m(function() {
                l || (l = 1, c(1));
            }, e), i ? k.onload() : n.parentNode.insertBefore(k, n);
        }, a.yepnope.injectCss = function(a, c, d, e, g, i) {
            var e = b.createElement("link"),
                j, c = i ? h : c || f;
            e.href = a, e.rel = "stylesheet", e.type = "text/css";
            for (j in d) e.setAttribute(j, d[j]);
            g || (n.parentNode.insertBefore(e, n), m(c, 0));
        };
    }(this, document), Modernizr.load = function() {
        yepnope.apply(window, [].slice.call(arguments, 0));
    };

define("modernizr", function(global) {
    return function() {
        var ret, fn;
        return ret || global.Modernizr;
    };
}(this));

if (typeof Object.create !== "function") {
    Object.create = function(o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}

(function() {
    var lastTime = 0;
    var vendors = ["ms", "moz", "webkit", "o"];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + "RequestAnimationFrame"];
        window.cancelAnimationFrame = window[vendors[x] + "CancelAnimationFrame"] || window[vendors[x] + "CancelRequestAnimationFrame"];
    }
    if (!window.requestAnimationFrame) window.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() {
            callback(currTime + timeToCall);
        }, timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };
    if (!window.cancelAnimationFrame) window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    };
})();

(function() {
    if (!window.console) {
        window.console = {};
    } else {
        console.log("use http://[url]?debug=true for console.logs");
    }
    var m = ["log", "info", "warn", "error", "debug", "trace", "dir", "group", "groupCollapsed", "groupEnd", "time", "timeEnd", "profile", "profileEnd", "dirxml", "assert", "count", "markTimeline", "timeStamp", "clear"];
    for (var i = 0; i < m.length; i++) {
        if (!window.console[m[i]]) {
            window.console[m[i]] = function() {};
        }
    }
})();

function htmlEncode(value) {
    return $("<div/>").text(value).html();
}

function htmlDecode(value) {
    return $("<div/>").html(value).text();
}

var tracecount = 0;

function trace(datastring, color, backgroundcolor, targetDiv) {
    if (targetDiv == undefined) {
        tracecount++;
    }
    if (typeof datastring == "object") {
        datastring = JSON.stringify(datastring);
    }
    var buildstringNS = buildstring = "![" + String(tracecount) + "] > " + datastring;
    if (targetDiv != undefined) {
        buildstring = '<span style="color:' + color + "; background-color:" + backgroundcolor + ';">' + htmlEncode(datastring) + "</span>";
    } else if (backgroundcolor != undefined) {
        buildstring = '<span style="color:' + color + "; background-color:" + backgroundcolor + ';">' + htmlEncode(buildstring) + "</span><br>";
    } else if (color != undefined) {
        buildstring = '<span style="color:' + color + ';">' + htmlEncode(buildstring) + "</span><br>";
    } else {
        buildstring = htmlEncode(buildstring) + "</br>";
    }
    if (targetDiv != undefined) {
        $(targetDiv).empty().prepend(buildstring);
    } else {
        console.log(buildstringNS);
        $(".debugConsole").prepend(buildstring);
    }
}

_notify = function() {
    var debug = function() {};
    var components = {};
    var broadcast = function(event, args, source) {
        if (!event) {
            return;
        }
        args = args || [];
        for (var c in components) {
            if (typeof components[c]["on" + event] == "function") {
                try {
                    source = source || components[c];
                    components[c]["on" + event].apply(source, args);
                } catch (err) {
                    debug(["Mediator error.", event, args, source, err].join(" "));
                }
            }
        }
    };
    var addComponent = function(name, component, replaceDuplicate) {
        if (name in components) {
            if (replaceDuplicate) {
                removeComponent(name);
            } else {
                throw new Error("The object: " + name + " has already applied listeners");
            }
        }
        components[name] = component;
    };
    var removeComponent = function(name) {
        if (name in components) {
            delete components[name];
        }
    };
    var getComponent = function(name) {
        return components[name];
    };
    var contains = function(name) {
        return name in components;
    };
    return {
        name: "Mediator",
        broadcast: broadcast,
        add: addComponent,
        rem: removeComponent,
        get: getComponent,
        has: contains
    };
}();

_notify.add("global", function() {
    var tracecount = 0;
    var alertcount = 0;
    return {
        onTrace: function(o) {
            tracecount++;
            var datastring = o.data;
            if (typeof datastring == "object") {
                datastring = JSON.stringify(datastring);
            }
            var buildstring = "[" + String(tracecount) + "] (" + o.senderID + ":" + o.sendertype + ":" + o.notifyscope + ") > " + datastring;
            if (_global$.getQuery("debug")) console.log(buildstring);
        },
        onAlert: function(o) {
            alertcount++;
            var datastring = o.data;
            if (typeof datastring == "object") {
                datastring = JSON.stringify(datastring);
            }
            var buildstring = "[" + String(tracecount) + "] (" + o.senderID + ":" + o.sendertype + ":" + o.notifyscope + ") > " + datastring;
            if (_global$.getQuery("debug")) {
                alert(buildstring);
            }
        },
        onInitialize: function(o) {
            if (_global$.getQuery("debug")) console.log("Created Instance #" + o.senderID + " type:" + o.sendertype + " by " + o.data.author + " [notifyscope:" + o.notifyscope + "]");
        }
    };
}());

require(["modernizr"], function(Modernizr) {
    function Monarch_Base() {
        if (arguments.callee._singletonInstance) return arguments.callee._singletonInstance;
        arguments.callee._singletonInstance = this;
        var parent = this;
        this.isTouch = Modernizr.touch;
        this.host = location.host;
        this.path = location.pathname;
        this.islocalhost = false;
        if (this.host.indexOf("localhost") != -1 || this.host.indexOf("127.0.0.1") != -1) {
            this.islocalhost = true;
        }
        this.jsonpReturn = function(id, type, notifyscope) {
            return function(data) {
                _notify.broadcast(type, [{
                    senderID: id,
                    sendertype: type,
                    notifyscope: notifyscope,
                    data: data
                }]);
            };
        };
        this._isupdated = function(value, currentVal, notify) {
            if (parent[value] != currentVal) {
                var Oldvalue = parent[value];
                parent[value] = currentVal;
                if (Oldvalue != undefined) {
                    this.notify(notify, parent[value]);
                }
            }
        };
        this.notify = function(type, data) {
            _notify.broadcast(type, [{
                senderID: "global",
                sendertype: "global",
                notifyscope: "global",
                data: data
            }]);
        };
        this.getQuery = function(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    var returncandidate = pair[1];
                    if (returncandidate == "true" || returncandidate == "yes" || returncandidate == "y" || returncandidate == "t") {
                        returncandidate = true;
                    }
                    if (returncandidate == "false" || returncandidate == "no" || returncandidate == "n" || returncandidate == "f") {
                        returncandidate = false;
                    }
                    return returncandidate;
                }
            }
            return false;
        };
        this.getHash = function(variable) {
            return window.location.hash;
        };
        this.setCookie = function(key, value) {
            var expires = new Date();
            expires.setTime(expires.getTime() + 1 * 24 * 60 * 60 * 1e3);
            document.cookie = key + "=" + value + ";expires=" + expires.toUTCString();
        };
        this.getCookie = function(key) {
            var keyValue = document.cookie.match("(^|;) ?" + key + "=([^;]*)(;|$)");
            return keyValue ? keyValue[2] : null;
        };
    }
    window._global$ = new Monarch_Base();
});

define("libraries/etc/required_chromecast.js", function() {});

var AETN = AETN || {};

AETN.whatisThis = "AETV custom receiver";

AETN.apiKey = "599b88193e3f5a6c96f7a362a75b34ad";

AETN.adobeTrackingServer = "metrics.aetn.com";

AETN.adobeTrackingSecureServer = "smetrics.aetn.com";

AETN.adobeTrackingSuiteID = "aetnwatchglobal";

AETN.adobeMCORDID = "08AC467352782D0D0A490D45@AdobeOrg";

AETN.mDialog_playoptions = "switch=hls&assetTypes=medium_video_ak&mbr=true&metafile=false";

AETN.omnitureDefaults = {
    platform: "Chromecast"
};

define("globalConfig", function() {});

AETN.environment = "dev";

AETN.signinURL = "http://dev-signature.video.aetndigital.com?url=";

AETN.proxyWithSigninURL = "https://dev-chromecast.ott.aetnd.com/proxy.php?mode=JSON&full_status=1&full_headers=1&url=";

define("environmentConfig", function() {});

AETN.brandID = "mlt";

AETN.brandID_omnitureName = "Lifetime";

AETN.streamActivityKey = "64524e36b96b6f547b3e9dce101bf8bd";

AETN.applicationKey = "e1baf721f28301c7c405b4273fec197c";

AETN.titleFeedNew = "http://dev-feeds.video.aetnd.com/api/lifetime/videos?filter%5Bid%5D=";

switch (AETN.environment) {
    case "dev":
        AETN.applicationID = "9BD85059";
        AETN.adobeReportSuiteID = "aetnchromecastlifetimedev";
        break;

    case "qa":
        AETN.applicationID = "DF71DBC6";
        AETN.adobeReportSuiteID = "aetnchromecastlifetimedev";
        break;

    default:
        AETN.applicationID = "EC31C351";
        AETN.streamActivityKey = undefined;
        AETN.adobeReportSuiteID = "aetnchromecastlifetime";
}

AETN.namespace = "urn:x-cast:com." + AETN.brandID + "_" + AETN.environment + ".chromecast";

define("brandConfig", function() {});

require.config({
    baseUrl: ".",
    paths: {
        underscore: "libraries/underscore/underscore",
        jquery: "libraries/jquery1x/jquery-1.10.2",
        modernizr: "libraries/modernizr/modernizr.custom.43687",
        x2js: "libraries/parsing/x2js/xml2json",
        knockout: "libraries/knockout/knockout-2.3.0",
        i18n: "libraries/require/i18n",
        dust: "libraries/dust/dist/dust-full-2.0.0.min",
        tweenmax: "libraries/greensocks/TweenMax.min",
        tweenlite: "libraries/greensocks/TweenLite.min",
        timelinelite: "libraries/greensocks/TimelineLite.min",
        timelinemax: "libraries/greensocks/TimelineMax.min",
        tweenmax_1_11_8: "libraries/greensocks_1.11.8/TweenMax",
        tweenlite_1_11_8: "libraries/greensocks_1.11.8/TweenLite",
        timelinelite_1_11_8: "libraries/greensocks_1.11.8/TimelineLite",
        timelinemax_1_11_8: "libraries/greensocks_1.11.8/TimelineMax",
        draggable_1_11_8: "libraries/greensocks_1.11.8/utils/Draggable.js",
        scrollto: "libraries/greensocks/plugins/ScrollToPlugin.min",
        hammer: "libraries/hammer/jquery.hammer",
        angular: "libraries/angular/angular.min",
        angularresource: "libraries/angular/angular-resource.min",
        oboe: "libraries/oboe/oboe-browser"
    },
    shim: {
        underscore: {
            exports: "_"
        },
        modernizr: {
            exports: "Modernizr"
        },
        x2js: {
            exports: "X2JS"
        },
        dust: {
            exports: "dust"
        },
        tweenmax: {
            exports: "TweenMax"
        },
        tweenlite: {
            exports: "TweenLite"
        },
        timelinelite: {
            exports: "TimelineLite"
        },
        timelinemax: {
            exports: "TimelineMax"
        },
        tweenmax_1_11_8: {
            exports: "TweenMax"
        },
        tweenlite_1_11_8: {
            exports: "TweenLite"
        },
        timelinelite_1_11_8: {
            exports: "TimelineLite"
        },
        timelinemax_1_11_8: {
            exports: "TimelineMax"
        },
        hammer: {
            exports: "Hammer"
        },
        scrollto: {
            exports: "Scrollto"
        },
        angular: {
            exports: "angular"
        },
        "angular-resource": {
            deps: ["angular"]
        }
    }
});

define("requirejsConfig", function() {});

(function() {
    "use strict";
    var MEDIA_SOURCE_ROOT = "";
    var MEDIA_SOURCE_URL = "";
    var PROGRESS_BAR_WIDTH = 600;
    var DEVICE_STATE = {
        IDLE: 0,
        ACTIVE: 1,
        WARNING: 2,
        ERROR: 3
    };
    var PLAYER_STATE = {
        IDLE: "IDLE",
        LOADING: "LOADING",
        LOADED: "LOADED",
        PLAYING: "PLAYING",
        PAUSED: "PAUSED",
        STOPPED: "STOPPED",
        SEEKING: "SEEKING",
        ERROR: "ERROR"
    };
    var CastPlayer = function() {
        this.deviceState = DEVICE_STATE.IDLE;
        this.receivers_available = false;
        this.currentMediaSession = null;
        this.currentVolume = .5;
        this.autoplay = true;
        this.session = null;
        this.castPlayerState = PLAYER_STATE.IDLE;
        this.localPlayerState = PLAYER_STATE.IDLE;
        this.localPlayer = null;
        this.fullscreen = false;
        this.audio = true;
        this.currentMediaIndex = 0;
        this.currentMediaTime = 0;
        this.currentMediaDuration = -1;
        this.timer = null;
        this.progressFlag = true;
        this.timerStep = 1e3;
        this.mediaContents = null;
    };
    CastPlayer.prototype.initializeLocalPlayer = function() {
        this.localPlayer = document.getElementById("video_element");
        this.localPlayer.addEventListener("loadeddata", this.onMediaLoadedLocally.bind(this));
    };
    CastPlayer.prototype.initializeCastPlayer = function() {
        if (!chrome.cast || !chrome.cast.isAvailable) {
            setTimeout(this.initializeCastPlayer.bind(this), 1e3);
            return;
        }
        var applicationID = AETN.applicationID;
        var autoJoinPolicy = chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED;
        var sessionRequest = new chrome.cast.SessionRequest(applicationID);
        var apiConfig = new chrome.cast.ApiConfig(sessionRequest, this.sessionListener.bind(this), this.receiverListener.bind(this), autoJoinPolicy);
        chrome.cast.initialize(apiConfig, this.onInitSuccess.bind(this), this.onError.bind(this));
        this.addVideoThumbs();
        this.initializeUI();
    };
    CastPlayer.prototype.onInitSuccess = function() {
        console.log("init success");
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.onError = function() {
        console.log("error");
    };
    CastPlayer.prototype.sessionListener = function(e) {
        trace("sessionListener:/");
        var parent = this;
        this.session = e;
        if (this.session) {
            this.deviceState = DEVICE_STATE.ACTIVE;
            if (this.session.media[0]) {
                this.onMediaDiscovered("activeSession", this.session.media[0]);
                this.syncCurrentMedia(this.session.media[0].media.contentId);
                this.selectMediaUpdateUI(this.currentMediaIndex);
                this.updateDisplayMessage();
            } else {}
            this.session.addUpdateListener(this.sessionUpdateListener.bind(this));
            this.session.addMessageListener(AETN.namespace, parent.messageRouter);
        }
    };
    CastPlayer.prototype.messageRouter = function(namespace, getDataString) {
        var jData = JSON.parse(getDataString);
        alert("receiverMessage: " + namespace + ", " + JSON.stringify(jData));
    };
    CastPlayer.prototype.syncCurrentMedia = function(currentMediaURL) {
        for (var i = 0; i < this.mediaContents.length; i++) {
            if (currentMediaURL == this.mediaContents[i]["sources"][0]) {
                this.currentMediaIndex = i;
            }
        }
    };
    CastPlayer.prototype.receiverListener = function(e) {
        if (e === "available") {
            this.receivers_available = true;
            this.updateMediaControlUI();
            console.log("receiver found");
        } else {
            console.log("receiver list empty");
        }
    };
    CastPlayer.prototype.sessionUpdateListener = function(isAlive) {
        if (!isAlive) {
            this.session = null;
            this.deviceState = DEVICE_STATE.IDLE;
            this.castPlayerState = PLAYER_STATE.IDLE;
            this.currentMediaSession = null;
            clearInterval(this.timer);
            this.updateDisplayMessage();
            var online = navigator.onLine;
            if (online == true) {
                console.log("current time: " + this.currentMediaTime);
                this.playMediaLocally();
                this.updateMediaControlUI();
            }
        }
    };
    CastPlayer.prototype.selectMedia = function(mediaIndex) {
        console.log("media selected" + mediaIndex);
        this.currentMediaIndex = mediaIndex;
        var pi = document.getElementById("progress_indicator");
        var p = document.getElementById("progress");
        this.currentMediaTime = 0;
        p.style.width = "0px";
        pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + "px";
        if (!this.currentMediaSession) {
            this.localPlayerState = PLAYER_STATE.IDLE;
            this.playMediaLocally();
        } else {
            this.castPlayerState = PLAYER_STATE.IDLE;
            this.playMedia();
        }
        this.selectMediaUpdateUI(mediaIndex);
    };
    CastPlayer.prototype.launchApp = function() {
        console.log("launching app...");
        chrome.cast.requestSession(this.sessionListener.bind(this), this.onLaunchError.bind(this));
        if (this.timer) {
            clearInterval(this.timer);
        }
    };
    CastPlayer.prototype.onRequestSessionSuccess = function(e) {
        console.log("session success: " + e.sessionId);
        this.session = e;
        this.deviceState = DEVICE_STATE.ACTIVE;
        this.updateMediaControlUI();
        this.loadMedia(this.currentMediaIndex);
        this.session.addUpdateListener(this.sessionUpdateListener.bind(this));
    };
    CastPlayer.prototype.onLaunchError = function() {
        console.log("launch error");
        this.deviceState = DEVICE_STATE.ERROR;
    };
    CastPlayer.prototype.stopApp = function() {
        this.session.stop(this.onStopAppSuccess.bind(this, "Session stopped"), this.onError.bind(this));
    };
    CastPlayer.prototype.onStopAppSuccess = function(message) {
        console.log(message);
        this.deviceState = DEVICE_STATE.IDLE;
        this.castPlayerState = PLAYER_STATE.IDLE;
        this.currentMediaSession = null;
        clearInterval(this.timer);
        this.updateDisplayMessage();
        console.log("current time: " + this.currentMediaTime);
        this.playMediaLocally();
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.loadMedia = function(mediaIndex) {
        this.loadMedia_manual(this.mediaContents[mediaIndex]);
    };
    CastPlayer.prototype.loadMedia_manual = function(mediaContents) {
        if (!this.session) {
            console.log("no session");
            return;
        }
        console.log("loading..." + mediaContents["title"]);
        var mediaInfo = new chrome.cast.media.MediaInfo(mediaContents.platformID);
        console.log("mediaInfo=" + JSON.stringify(mediaInfo));
        mediaInfo.metadata = new chrome.cast.media.GenericMediaMetadata();
        mediaInfo.contentType = "video/mp4";
        mediaInfo.customData = mediaContents;
        var request = new chrome.cast.media.LoadRequest(mediaInfo);
        request.autoplay = this.autoplay;
        if (this.localPlayerState == PLAYER_STATE.PLAYING) {
            request.currentTime = this.localPlayer.currentTime;
            this.localPlayer.pause();
            this.localPlayerState = PLAYER_STATE.STOPPED;
        } else {
            request.currentTime = 0;
        }
        this.castPlayerState = PLAYER_STATE.LOADING;
        this.session.loadMedia(request, this.onMediaDiscovered.bind(this, "loadMedia"), this.onLoadMediaError.bind(this));
        document.getElementById("media_title").innerHTML = this.mediaContents[this.currentMediaIndex]["title"];
        document.getElementById("media_subtitle").innerHTML = this.mediaContents[this.currentMediaIndex]["subtitle"];
        document.getElementById("media_desc").innerHTML = this.mediaContents[this.currentMediaIndex]["description"];
    };
    CastPlayer.prototype.onMediaDiscovered = function(how, mediaSession) {
        console.log("new media session ID:" + mediaSession.mediaSessionId + " (" + how + ")");
        this.currentMediaSession = mediaSession;
        if (how == "loadMedia") {
            if (this.autoplay) {
                this.castPlayerState = PLAYER_STATE.PLAYING;
            } else {
                this.castPlayerState = PLAYER_STATE.LOADED;
            }
        }
        if (how == "activeSession") {
            this.castPlayerState = this.session.media[0].playerState;
            this.currentMediaTime = this.session.media[0].currentTime;
        }
        if (this.castPlayerState == PLAYER_STATE.PLAYING) {
            this.startProgressTimer(this.incrementMediaTime);
        }
        this.currentMediaSession.addUpdateListener(this.onMediaStatusUpdate.bind(this));
        this.currentMediaDuration = this.currentMediaSession.media.duration;
        var duration = this.currentMediaDuration;
        var hr = parseInt(duration / 3600);
        duration -= hr * 3600;
        var min = parseInt(duration / 60);
        var sec = parseInt(duration % 60);
        if (hr > 0) {
            duration = hr + ":" + min + ":" + sec;
        } else {
            if (min > 0) {
                duration = min + ":" + sec;
            } else {
                duration = sec;
            }
        }
        document.getElementById("duration").innerHTML = duration;
        if (this.localPlayerState == PLAYER_STATE.PLAYING) {
            this.localPlayerState == PLAYER_STATE.STOPPED;
            var vi = document.getElementById("video_image");
            vi.style.display = "block";
            this.localPlayer.style.display = "none";
            this.startProgressTimer(this.incrementMediaTime);
        }
        this.updateMediaControlUI();
        this.updateDisplayMessage();
    };
    CastPlayer.prototype.onLoadMediaError = function(e) {
        console.log("media error");
        this.castPlayerState = PLAYER_STATE.IDLE;
        this.updateMediaControlUI();
        this.updateDisplayMessage();
    };
    CastPlayer.prototype.onMediaStatusUpdate = function(e) {
        if (e == false) {
            this.currentMediaTime = 0;
            this.castPlayerState = PLAYER_STATE.IDLE;
        }
        console.log("updating media");
        this.updateProgressBar(e);
        this.updateDisplayMessage();
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.incrementMediaTime = function() {
        if (this.castPlayerState == PLAYER_STATE.PLAYING || this.localPlayerState == PLAYER_STATE.PLAYING) {
            if (this.currentMediaTime < this.currentMediaDuration) {
                this.currentMediaTime += 1;
                this.updateProgressBarByTimer();
            } else {
                this.currentMediaTime = 0;
                clearInterval(this.timer);
            }
        }
    };
    CastPlayer.prototype.playMediaLocally = function() {
        var vi = document.getElementById("video_image");
        vi.style.display = "none";
        this.localPlayer.style.display = "block";
        if (this.localPlayerState != PLAYER_STATE.PLAYING && this.localPlayerState != PLAYER_STATE.PAUSED) {
            this.localPlayer.src = this.mediaContents[this.currentMediaIndex]["sources"][0];
            this.localPlayer.load();
        } else {
            this.localPlayer.play();
            this.startProgressTimer(this.incrementMediaTime);
        }
        this.localPlayerState = PLAYER_STATE.PLAYING;
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.onMediaLoadedLocally = function() {
        this.currentMediaDuration = this.localPlayer.duration;
        var duration = this.currentMediaDuration;
        var hr = parseInt(duration / 3600);
        duration -= hr * 3600;
        var min = parseInt(duration / 60);
        var sec = parseInt(duration % 60);
        if (hr > 0) {
            duration = hr + ":" + min + ":" + sec;
        } else {
            if (min > 0) {
                duration = min + ":" + sec;
            } else {
                duration = sec;
            }
        }
        document.getElementById("duration").innerHTML = duration;
        this.localPlayer.currentTime = this.currentMediaTime;
        this.localPlayer.play();
        this.startProgressTimer(this.incrementMediaTime);
    };
    CastPlayer.prototype.playMedia = function() {
        if (!this.currentMediaSession) {
            this.playMediaLocally();
            return;
        }
        switch (this.castPlayerState) {
            case PLAYER_STATE.LOADED:
            case PLAYER_STATE.PAUSED:
                this.currentMediaSession.play(null, this.mediaCommandSuccessCallback.bind(this, "playing started for " + this.currentMediaSession.sessionId), this.onError.bind(this));
                this.currentMediaSession.addUpdateListener(this.onMediaStatusUpdate.bind(this));
                this.castPlayerState = PLAYER_STATE.PLAYING;
                this.startProgressTimer(this.incrementMediaTime);
                break;

            case PLAYER_STATE.IDLE:
            case PLAYER_STATE.LOADING:
            case PLAYER_STATE.STOPPED:
                this.loadMedia(this.currentMediaIndex);
                this.currentMediaSession.addUpdateListener(this.onMediaStatusUpdate.bind(this));
                this.castPlayerState = PLAYER_STATE.PLAYING;
                break;

            default:
                break;
        }
        this.updateMediaControlUI();
        this.updateDisplayMessage();
    };
    CastPlayer.prototype.pauseMedia = function() {
        if (!this.currentMediaSession) {
            this.pauseMediaLocally();
            return;
        }
        if (this.castPlayerState == PLAYER_STATE.PLAYING) {
            this.castPlayerState = PLAYER_STATE.PAUSED;
            this.currentMediaSession.pause(null, this.mediaCommandSuccessCallback.bind(this, "paused " + this.currentMediaSession.sessionId), this.onError.bind(this));
            this.updateMediaControlUI();
            this.updateDisplayMessage();
            clearInterval(this.timer);
        }
    };
    CastPlayer.prototype.pauseMediaLocally = function() {
        this.localPlayer.pause();
        this.localPlayerState = PLAYER_STATE.PAUSED;
        this.updateMediaControlUI();
        clearInterval(this.timer);
    };
    CastPlayer.prototype.stopMedia = function() {
        if (!this.currentMediaSession) {
            this.stopMediaLocally();
            return;
        }
        this.currentMediaSession.stop(null, this.mediaCommandSuccessCallback.bind(this, "stopped " + this.currentMediaSession.sessionId), this.onError.bind(this));
        this.castPlayerState = PLAYER_STATE.STOPPED;
        clearInterval(this.timer);
        this.updateDisplayMessage();
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.stopMediaLocally = function() {
        var vi = document.getElementById("video_image");
        vi.style.display = "block";
        this.localPlayer.style.display = "none";
        this.localPlayer.stop();
        this.localPlayerState = PLAYER_STATE.STOPPED;
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.setReceiverVolume = function(mute) {
        var p = document.getElementById("audio_bg_level");
        if (event.currentTarget.id == "audio_bg_track") {
            var pos = 100 - parseInt(event.offsetY);
        } else {
            var pos = parseInt(p.clientHeight) - parseInt(event.offsetY);
        }
        if (!this.currentMediaSession) {
            this.localPlayer.volume = pos < 100 ? pos / 100 : 1;
            p.style.height = pos + "px";
            p.style.marginTop = -pos + "px";
            return;
        }
        if (event.currentTarget.id == "audio_bg_track" || event.currentTarget.id == "audio_bg_level") {
            if (pos < 100) {
                var vScale = this.currentVolume * 100;
                if (pos > vScale) {
                    pos = vScale + (pos - vScale) / 2;
                }
                p.style.height = pos + "px";
                p.style.marginTop = -pos + "px";
                this.currentVolume = pos / 100;
            } else {
                this.currentVolume = 1;
            }
        }
        if (!mute) {
            this.session.setReceiverVolumeLevel(this.currentVolume, this.mediaCommandSuccessCallback.bind(this), this.onError.bind(this));
        } else {
            this.session.setReceiverMuted(true, this.mediaCommandSuccessCallback.bind(this), this.onError.bind(this));
        }
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.muteMedia = function() {
        if (this.audio == true) {
            this.audio = false;
            document.getElementById("audio_on").style.display = "none";
            document.getElementById("audio_off").style.display = "block";
            if (this.currentMediaSession) {
                this.setReceiverVolume(true);
            } else {
                this.localPlayer.muted = true;
            }
        } else {
            this.audio = true;
            document.getElementById("audio_on").style.display = "block";
            document.getElementById("audio_off").style.display = "none";
            if (this.currentMediaSession) {
                this.setReceiverVolume(false);
            } else {
                this.localPlayer.muted = false;
            }
        }
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.seekMedia = function(event) {
        var pos = parseInt(event.offsetX);
        var pi = document.getElementById("progress_indicator");
        var p = document.getElementById("progress");
        if (event.currentTarget.id == "progress_indicator") {
            var curr = parseInt(this.currentMediaTime + this.currentMediaDuration * pos / PROGRESS_BAR_WIDTH);
            var pp = parseInt(pi.style.marginLeft) + pos;
            var pw = parseInt(p.style.width) + pos;
        } else {
            var curr = parseInt(pos * this.currentMediaDuration / PROGRESS_BAR_WIDTH);
            var pp = pos - 21 - PROGRESS_BAR_WIDTH;
            var pw = pos;
        }
        if (this.localPlayerState == PLAYER_STATE.PLAYING || this.localPlayerState == PLAYER_STATE.PAUSED) {
            this.localPlayer.currentTime = curr;
            this.currentMediaTime = curr;
            this.localPlayer.play();
        }
        if (this.localPlayerState == PLAYER_STATE.PLAYING || this.localPlayerState == PLAYER_STATE.PAUSED || this.castPlayerState == PLAYER_STATE.PLAYING || this.castPlayerState == PLAYER_STATE.PAUSED) {
            p.style.width = pw + "px";
            pi.style.marginLeft = pp + "px";
        }
        if (this.castPlayerState != PLAYER_STATE.PLAYING && this.castPlayerState != PLAYER_STATE.PAUSED) {
            return;
        }
        this.currentMediaTime = curr;
        console.log("Seeking " + this.currentMediaSession.sessionId + ":" + this.currentMediaSession.mediaSessionId + " to " + pos + "%");
        var request = new chrome.cast.media.SeekRequest();
        request.currentTime = this.currentMediaTime;
        this.currentMediaSession.seek(request, this.onSeekSuccess.bind(this, "media seek done"), this.onError.bind(this));
        this.castPlayerState = PLAYER_STATE.SEEKING;
        this.updateDisplayMessage();
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.onSeekSuccess = function(info) {
        console.log(info);
        this.castPlayerState = PLAYER_STATE.PLAYING;
        this.updateDisplayMessage();
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.mediaCommandSuccessCallback = function(info, e) {
        console.log(info);
    };
    CastPlayer.prototype.updateProgressBar = function(e) {
        var p = document.getElementById("progress");
        var pi = document.getElementById("progress_indicator");
        if (e == false) {
            p.style.width = "0px";
            pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + "px";
            clearInterval(this.timer);
            this.castPlayerState = PLAYER_STATE.STOPPED;
            this.updateDisplayMessage();
        } else {
            p.style.width = Math.ceil(PROGRESS_BAR_WIDTH * this.currentMediaSession.currentTime / this.currentMediaSession.media.duration + 1) + "px";
            this.progressFlag = false;
            setTimeout(this.setProgressFlag.bind(this), 1e3);
            var pp = Math.ceil(PROGRESS_BAR_WIDTH * this.currentMediaSession.currentTime / this.currentMediaSession.media.duration);
            pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + pp + "px";
        }
    };
    CastPlayer.prototype.setProgressFlag = function() {
        this.progressFlag = true;
    };
    CastPlayer.prototype.updateProgressBarByTimer = function() {
        var p = document.getElementById("progress");
        if (isNaN(parseInt(p.style.width))) {
            p.style.width = 0;
        }
        if (this.currentMediaDuration > 0) {
            var pp = Math.floor(PROGRESS_BAR_WIDTH * this.currentMediaTime / this.currentMediaDuration);
        }
        if (this.progressFlag) {
            p.style.width = pp + "px";
            var pi = document.getElementById("progress_indicator");
            pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + pp + "px";
        }
        if (pp > PROGRESS_BAR_WIDTH) {
            clearInterval(this.timer);
            this.deviceState = DEVICE_STATE.IDLE;
            this.castPlayerState = PLAYER_STATE.IDLE;
            this.updateDisplayMessage();
            this.updateMediaControlUI();
        }
    };
    CastPlayer.prototype.updateDisplayMessage = function() {
        if (this.deviceState != DEVICE_STATE.ACTIVE || this.castPlayerState == PLAYER_STATE.IDLE || this.castPlayerState == PLAYER_STATE.STOPPED) {
            document.getElementById("playerstate").style.display = "none";
            document.getElementById("playerstatebg").style.display = "none";
            document.getElementById("play").style.display = "block";
            document.getElementById("video_image_overlay").style.display = "none";
        } else {
            document.getElementById("playerstate").style.display = "block";
            document.getElementById("playerstatebg").style.display = "block";
            document.getElementById("video_image_overlay").style.display = "block";
            document.getElementById("playerstate").innerHTML = this.mediaContents[this.currentMediaIndex]["title"] + " " + this.castPlayerState + " on " + this.session.receiver.friendlyName;
        }
    };
    CastPlayer.prototype.updateMediaControlUI = function() {
        var playerState = this.deviceState == DEVICE_STATE.ACTIVE ? this.castPlayerState : this.localPlayerState;
        switch (playerState) {
            case PLAYER_STATE.LOADED:
            case PLAYER_STATE.PLAYING:
                document.getElementById("play").style.display = "none";
                document.getElementById("pause").style.display = "block";
                break;

            case PLAYER_STATE.PAUSED:
            case PLAYER_STATE.IDLE:
            case PLAYER_STATE.LOADING:
            case PLAYER_STATE.STOPPED:
                document.getElementById("play").style.display = "block";
                document.getElementById("pause").style.display = "none";
                break;

            default:
                break;
        }
        if (!this.receivers_available) {
            document.getElementById("casticonactive").style.display = "none";
            document.getElementById("casticonidle").style.display = "none";
            return;
        }
        if (this.deviceState == DEVICE_STATE.ACTIVE) {
            document.getElementById("casticonactive").style.display = "block";
            document.getElementById("casticonidle").style.display = "none";
            this.hideFullscreenButton();
        } else {
            document.getElementById("casticonidle").style.display = "block";
            document.getElementById("casticonactive").style.display = "none";
            this.showFullscreenButton();
        }
    };
    CastPlayer.prototype.selectMediaUpdateUI = function(mediaIndex) {
        document.getElementById("video_image").src = MEDIA_SOURCE_ROOT + this.mediaContents[mediaIndex]["thumb"];
        document.getElementById("progress").style.width = "0px";
        document.getElementById("media_title").innerHTML = this.mediaContents[mediaIndex]["title"];
        document.getElementById("media_subtitle").innerHTML = this.mediaContents[mediaIndex]["subtitle"];
        document.getElementById("media_desc").innerHTML = this.mediaContents[mediaIndex]["description"];
    };
    CastPlayer.prototype.initializeUI = function() {
        document.getElementById("media_title").innerHTML = this.mediaContents[0]["title"];
        document.getElementById("media_subtitle").innerHTML = this.mediaContents[this.currentMediaIndex]["subtitle"];
        document.getElementById("media_desc").innerHTML = this.mediaContents[this.currentMediaIndex]["description"];
        document.getElementById("casticonidle").addEventListener("click", this.launchApp.bind(this));
        document.getElementById("casticonactive").addEventListener("click", this.stopApp.bind(this));
        document.getElementById("progress_bg").addEventListener("click", this.seekMedia.bind(this));
        document.getElementById("progress").addEventListener("click", this.seekMedia.bind(this));
        document.getElementById("progress_indicator").addEventListener("dragend", this.seekMedia.bind(this));
        document.getElementById("audio_on").addEventListener("click", this.muteMedia.bind(this));
        document.getElementById("audio_off").addEventListener("click", this.muteMedia.bind(this));
        document.getElementById("audio_bg").addEventListener("mouseover", this.showVolumeSlider.bind(this));
        document.getElementById("audio_on").addEventListener("mouseover", this.showVolumeSlider.bind(this));
        document.getElementById("audio_bg_level").addEventListener("mouseover", this.showVolumeSlider.bind(this));
        document.getElementById("audio_bg_track").addEventListener("mouseover", this.showVolumeSlider.bind(this));
        document.getElementById("audio_bg_level").addEventListener("click", this.setReceiverVolume.bind(this, false));
        document.getElementById("audio_bg_track").addEventListener("click", this.setReceiverVolume.bind(this, false));
        document.getElementById("audio_bg").addEventListener("mouseout", this.hideVolumeSlider.bind(this));
        document.getElementById("audio_on").addEventListener("mouseout", this.hideVolumeSlider.bind(this));
        document.getElementById("media_control").addEventListener("mouseover", this.showMediaControl.bind(this));
        document.getElementById("media_control").addEventListener("mouseout", this.hideMediaControl.bind(this));
        document.getElementById("fullscreen_expand").addEventListener("click", this.requestFullScreen.bind(this));
        document.getElementById("fullscreen_collapse").addEventListener("click", this.cancelFullScreen.bind(this));
        document.addEventListener("fullscreenchange", this.changeHandler.bind(this), false);
        document.addEventListener("webkitfullscreenchange", this.changeHandler.bind(this), false);
        document.getElementById("play").addEventListener("click", this.playMedia.bind(this));
        document.getElementById("pause").addEventListener("click", this.pauseMedia.bind(this));
        document.getElementById("progress_indicator").draggable = true;
    };
    CastPlayer.prototype.showMediaControl = function() {
        document.getElementById("media_control").style.opacity = .7;
    };
    CastPlayer.prototype.hideMediaControl = function() {};
    CastPlayer.prototype.showVolumeSlider = function() {
        document.getElementById("audio_bg").style.opacity = 1;
        document.getElementById("audio_bg_track").style.opacity = 1;
        document.getElementById("audio_bg_level").style.opacity = 1;
        document.getElementById("audio_indicator").style.opacity = 1;
    };
    CastPlayer.prototype.hideVolumeSlider = function() {
        document.getElementById("audio_bg").style.opacity = 0;
        document.getElementById("audio_bg_track").style.opacity = 0;
        document.getElementById("audio_bg_level").style.opacity = 0;
        document.getElementById("audio_indicator").style.opacity = 0;
    };
    CastPlayer.prototype.requestFullScreen = function() {
        var element = document.getElementById("video_element");
        var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen;
        if (requestMethod) {
            requestMethod.call(element);
            console.log("requested fullscreen");
        }
    };
    CastPlayer.prototype.cancelFullScreen = function() {
        var requestMethod = document.cancelFullScreen || document.webkitCancelFullScreen;
        if (requestMethod) {
            requestMethod.call(document);
        }
    };
    CastPlayer.prototype.changeHandler = function() {
        this.fullscreen = !this.fullscreen;
        if (this.deviceState == DEVICE_STATE.ACTIVE) {
            this.hideFullscreenButton();
        } else {
            this.showFullscreenButton();
        }
    };
    CastPlayer.prototype.showFullscreenButton = function() {
        if (this.fullscreen) {
            document.getElementById("fullscreen_expand").style.display = "none";
            document.getElementById("fullscreen_collapse").style.display = "block";
        } else {
            document.getElementById("fullscreen_expand").style.display = "block";
            document.getElementById("fullscreen_collapse").style.display = "none";
        }
    };
    CastPlayer.prototype.hideFullscreenButton = function() {
        document.getElementById("fullscreen_expand").style.display = "none";
        document.getElementById("fullscreen_collapse").style.display = "none";
    };
    CastPlayer.prototype.startProgressTimer = function(callback) {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
        this.timer = setInterval(callback.bind(this), this.timerStep);
    };
    CastPlayer.prototype.retrieveMediaJSON = function(src) {
        console.log("retrieveMediaJSON not needed");
    };
    CastPlayer.prototype.onMediaJsonLoad = function(evt) {
        var responseJson = evt.srcElement.response;
        this.mediaContents = responseJson["categories"][0]["videos"];
        var ni = document.getElementById("carousel");
        var newdiv = null;
        var divIdName = null;
        for (var i = 0; i < this.mediaContents.length; i++) {
            newdiv = document.createElement("div");
            divIdName = "thumb" + i + "Div";
            newdiv.setAttribute("id", divIdName);
            newdiv.setAttribute("class", "thumb");
            newdiv.innerHTML = '<img src="' + MEDIA_SOURCE_ROOT + this.mediaContents[i]["thumb"] + '" class="thumbnail">';
            newdiv.addEventListener("click", this.selectMedia.bind(this, i));
            ni.appendChild(newdiv);
        }
    };
    CastPlayer.prototype.onMediaJsonError = function() {
        console.log("Failed to load media JSON");
    };
    CastPlayer.prototype.addVideoThumbs = function() {
        this.mediaContents = mediaJSON["categories"][0]["videos"];
        var ni = document.getElementById("carousel");
        var newdiv = null;
        var newdivBG = null;
        var divIdName = null;
        for (var i = 0; i < this.mediaContents.length; i++) {
            newdiv = document.createElement("div");
            divIdName = "thumb" + i + "Div";
            newdiv.setAttribute("id", divIdName);
            newdiv.setAttribute("class", "thumb");
            newdiv.innerHTML = '<img src="' + MEDIA_SOURCE_ROOT + this.mediaContents[i]["thumb"] + '" class="thumbnail">';
            newdiv.addEventListener("click", this.selectMedia.bind(this, i));
            ni.appendChild(newdiv);
        }
    };
    var mediaJSON = {
        categories: [{
            name: "Movies",
            videos: [{
                description: "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license\nhttp://www.bigbuckbunny.org",
                sources: ["http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"],
                subtitle: "By Blender Foundation",
                thumb: "images/BigBuckBunny.jpg",
                title: "Big Buck Bunny"
            }]
        }]
    };
    window.CastPlayer = new CastPlayer();
})();

define("chromeSender", function() {});
