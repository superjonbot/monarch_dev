/*! chromecast_sender_fyi_prod : sender_f_fyi_prod.js */
/*! codebase: CB2016 v1.1.4 by Jonathan Robles */
/*! built:05-03-2016 [12:54:53PM] */
/*! -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

/*! Prerequisites: //www.gstatic.com/cv/js/sender/v1/cast_sender.js */

/*! Third Party Includes [start] */
/* almond, modernizr, underscore, oboe */
/*! Third Party Includes [end] */
define("modules/definitions/standardmodule", [], function() {
    var _instanceID = 0;
    var _nextInstanceID = function() {
        return ++_instanceID;
    };
    var defaults = [];
    var deepExtend = function(destination, source) {
        for (var property in source) {
            if (source[property] && source[property].constructor && source[property].constructor === Object) {
                destination[property] = destination[property] || {};
                arguments.callee(destination[property], source[property]);
            } else {
                destination[property] = source[property];
            }
        }
        return destination;
    };

    function _thizOBJ_(o) {
        this._instanceID = _nextInstanceID();
        var _parent = this;
        defaults[this._instanceID] = {
            type: "Standard Module Definition",
            author: "Jonathan Robles",
            notifyscope: "global",
            target: undefined,
            file: undefined,
            usenocache: true,
            data: undefined,
            callback: undefined,
            interval: undefined,
            init: function() {
                _notify.broadcast("Initialize", [{
                    senderID: _parent._instanceID,
                    sendertype: this.type,
                    notifyscope: this.notifyscope,
                    data: {
                        author: this.author
                    }
                }]);
            },
            parent: this
        };
        defaults[this._instanceID] = deepExtend(defaults[this._instanceID], o);
        defaults[this._instanceID].init();
        return this;
    }
    _thizOBJ_.prototype = {
        _init: function() {
            this._var().init();
        },
        _showdata: function() {
            return JSON.stringify(defaults[this._instanceID]);
        },
        _id: function() {
            return this._instanceID;
        },
        _var: function(o) {
            if (o != undefined) {
                defaults[this._instanceID] = deepExtend(defaults[this._instanceID], o);
            }
            return defaults[this._instanceID];
        },
        _nocache: function(string) {
            if (typeof string === "string") {
                if (this._var().usenocache) {
                    var addOn = "?";
                    if (string.indexOf("?") != -1) {
                        addOn = "&";
                    }
                    return string + addOn + "nocache=" + Math.floor(Math.random() * 9999);
                } else {
                    return string;
                }
            } else {
                this.notify("Alert", "_nocache needs a string!");
                return;
            }
        },
        notify: function(type, data) {
            _notify.broadcast(type, [{
                senderID: this._id(),
                sendertype: this._var().type,
                notifyscope: this._var().notifyscope,
                data: data
            }]);
        },
        deepExtend: function(destination, source) {
            return deepExtend(destination, source);
        },
        parent: this
    };
    return _thizOBJ_;
});

(function() {
    var root = this;
    var previousUnderscore = root._;
    var breaker = {};
    var ArrayProto = Array.prototype,
        ObjProto = Object.prototype,
        FuncProto = Function.prototype;
    var push = ArrayProto.push,
        slice = ArrayProto.slice,
        concat = ArrayProto.concat,
        toString = ObjProto.toString,
        hasOwnProperty = ObjProto.hasOwnProperty;
    var nativeForEach = ArrayProto.forEach,
        nativeMap = ArrayProto.map,
        nativeReduce = ArrayProto.reduce,
        nativeReduceRight = ArrayProto.reduceRight,
        nativeFilter = ArrayProto.filter,
        nativeEvery = ArrayProto.every,
        nativeSome = ArrayProto.some,
        nativeIndexOf = ArrayProto.indexOf,
        nativeLastIndexOf = ArrayProto.lastIndexOf,
        nativeIsArray = Array.isArray,
        nativeKeys = Object.keys,
        nativeBind = FuncProto.bind;
    var _ = function(obj) {
        if (obj instanceof _) return obj;
        if (!(this instanceof _)) return new _(obj);
        this._wrapped = obj;
    };
    if (typeof exports !== "undefined") {
        if (typeof module !== "undefined" && module.exports) {
            exports = module.exports = _;
        }
        exports._ = _;
    } else {
        root._ = _;
    }
    _.VERSION = "1.5.2";
    var each = _.each = _.forEach = function(obj, iterator, context) {
        if (obj == null) return;
        if (nativeForEach && obj.forEach === nativeForEach) {
            obj.forEach(iterator, context);
        } else if (obj.length === +obj.length) {
            for (var i = 0, length = obj.length; i < length; i++) {
                if (iterator.call(context, obj[i], i, obj) === breaker) return;
            }
        } else {
            var keys = _.keys(obj);
            for (var i = 0, length = keys.length; i < length; i++) {
                if (iterator.call(context, obj[keys[i]], keys[i], obj) === breaker) return;
            }
        }
    };
    _.map = _.collect = function(obj, iterator, context) {
        var results = [];
        if (obj == null) return results;
        if (nativeMap && obj.map === nativeMap) return obj.map(iterator, context);
        each(obj, function(value, index, list) {
            results.push(iterator.call(context, value, index, list));
        });
        return results;
    };
    var reduceError = "Reduce of empty array with no initial value";
    _.reduce = _.foldl = _.inject = function(obj, iterator, memo, context) {
        var initial = arguments.length > 2;
        if (obj == null) obj = [];
        if (nativeReduce && obj.reduce === nativeReduce) {
            if (context) iterator = _.bind(iterator, context);
            return initial ? obj.reduce(iterator, memo) : obj.reduce(iterator);
        }
        each(obj, function(value, index, list) {
            if (!initial) {
                memo = value;
                initial = true;
            } else {
                memo = iterator.call(context, memo, value, index, list);
            }
        });
        if (!initial) throw new TypeError(reduceError);
        return memo;
    };
    _.reduceRight = _.foldr = function(obj, iterator, memo, context) {
        var initial = arguments.length > 2;
        if (obj == null) obj = [];
        if (nativeReduceRight && obj.reduceRight === nativeReduceRight) {
            if (context) iterator = _.bind(iterator, context);
            return initial ? obj.reduceRight(iterator, memo) : obj.reduceRight(iterator);
        }
        var length = obj.length;
        if (length !== +length) {
            var keys = _.keys(obj);
            length = keys.length;
        }
        each(obj, function(value, index, list) {
            index = keys ? keys[--length] : --length;
            if (!initial) {
                memo = obj[index];
                initial = true;
            } else {
                memo = iterator.call(context, memo, obj[index], index, list);
            }
        });
        if (!initial) throw new TypeError(reduceError);
        return memo;
    };
    _.find = _.detect = function(obj, iterator, context) {
        var result;
        any(obj, function(value, index, list) {
            if (iterator.call(context, value, index, list)) {
                result = value;
                return true;
            }
        });
        return result;
    };
    _.filter = _.select = function(obj, iterator, context) {
        var results = [];
        if (obj == null) return results;
        if (nativeFilter && obj.filter === nativeFilter) return obj.filter(iterator, context);
        each(obj, function(value, index, list) {
            if (iterator.call(context, value, index, list)) results.push(value);
        });
        return results;
    };
    _.reject = function(obj, iterator, context) {
        return _.filter(obj, function(value, index, list) {
            return !iterator.call(context, value, index, list);
        }, context);
    };
    _.every = _.all = function(obj, iterator, context) {
        iterator || (iterator = _.identity);
        var result = true;
        if (obj == null) return result;
        if (nativeEvery && obj.every === nativeEvery) return obj.every(iterator, context);
        each(obj, function(value, index, list) {
            if (!(result = result && iterator.call(context, value, index, list))) return breaker;
        });
        return !!result;
    };
    var any = _.some = _.any = function(obj, iterator, context) {
        iterator || (iterator = _.identity);
        var result = false;
        if (obj == null) return result;
        if (nativeSome && obj.some === nativeSome) return obj.some(iterator, context);
        each(obj, function(value, index, list) {
            if (result || (result = iterator.call(context, value, index, list))) return breaker;
        });
        return !!result;
    };
    _.contains = _.include = function(obj, target) {
        if (obj == null) return false;
        if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1;
        return any(obj, function(value) {
            return value === target;
        });
    };
    _.invoke = function(obj, method) {
        var args = slice.call(arguments, 2);
        var isFunc = _.isFunction(method);
        return _.map(obj, function(value) {
            return (isFunc ? method : value[method]).apply(value, args);
        });
    };
    _.pluck = function(obj, key) {
        return _.map(obj, function(value) {
            return value[key];
        });
    };
    _.where = function(obj, attrs, first) {
        if (_.isEmpty(attrs)) return first ? void 0 : [];
        return _[first ? "find" : "filter"](obj, function(value) {
            for (var key in attrs) {
                if (attrs[key] !== value[key]) return false;
            }
            return true;
        });
    };
    _.findWhere = function(obj, attrs) {
        return _.where(obj, attrs, true);
    };
    _.max = function(obj, iterator, context) {
        if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) {
            return Math.max.apply(Math, obj);
        }
        if (!iterator && _.isEmpty(obj)) return -Infinity;
        var result = {
            computed: -Infinity,
            value: -Infinity
        };
        each(obj, function(value, index, list) {
            var computed = iterator ? iterator.call(context, value, index, list) : value;
            computed > result.computed && (result = {
                value: value,
                computed: computed
            });
        });
        return result.value;
    };
    _.min = function(obj, iterator, context) {
        if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) {
            return Math.min.apply(Math, obj);
        }
        if (!iterator && _.isEmpty(obj)) return Infinity;
        var result = {
            computed: Infinity,
            value: Infinity
        };
        each(obj, function(value, index, list) {
            var computed = iterator ? iterator.call(context, value, index, list) : value;
            computed < result.computed && (result = {
                value: value,
                computed: computed
            });
        });
        return result.value;
    };
    _.shuffle = function(obj) {
        var rand;
        var index = 0;
        var shuffled = [];
        each(obj, function(value) {
            rand = _.random(index++);
            shuffled[index - 1] = shuffled[rand];
            shuffled[rand] = value;
        });
        return shuffled;
    };
    _.sample = function(obj, n, guard) {
        if (arguments.length < 2 || guard) {
            return obj[_.random(obj.length - 1)];
        }
        return _.shuffle(obj).slice(0, Math.max(0, n));
    };
    var lookupIterator = function(value) {
        return _.isFunction(value) ? value : function(obj) {
            return obj[value];
        };
    };
    _.sortBy = function(obj, value, context) {
        var iterator = lookupIterator(value);
        return _.pluck(_.map(obj, function(value, index, list) {
            return {
                value: value,
                index: index,
                criteria: iterator.call(context, value, index, list)
            };
        }).sort(function(left, right) {
            var a = left.criteria;
            var b = right.criteria;
            if (a !== b) {
                if (a > b || a === void 0) return 1;
                if (a < b || b === void 0) return -1;
            }
            return left.index - right.index;
        }), "value");
    };
    var group = function(behavior) {
        return function(obj, value, context) {
            var result = {};
            var iterator = value == null ? _.identity : lookupIterator(value);
            each(obj, function(value, index) {
                var key = iterator.call(context, value, index, obj);
                behavior(result, key, value);
            });
            return result;
        };
    };
    _.groupBy = group(function(result, key, value) {
        (_.has(result, key) ? result[key] : result[key] = []).push(value);
    });
    _.indexBy = group(function(result, key, value) {
        result[key] = value;
    });
    _.countBy = group(function(result, key) {
        _.has(result, key) ? result[key]++ : result[key] = 1;
    });
    _.sortedIndex = function(array, obj, iterator, context) {
        iterator = iterator == null ? _.identity : lookupIterator(iterator);
        var value = iterator.call(context, obj);
        var low = 0,
            high = array.length;
        while (low < high) {
            var mid = low + high >>> 1;
            iterator.call(context, array[mid]) < value ? low = mid + 1 : high = mid;
        }
        return low;
    };
    _.toArray = function(obj) {
        if (!obj) return [];
        if (_.isArray(obj)) return slice.call(obj);
        if (obj.length === +obj.length) return _.map(obj, _.identity);
        return _.values(obj);
    };
    _.size = function(obj) {
        if (obj == null) return 0;
        return obj.length === +obj.length ? obj.length : _.keys(obj).length;
    };
    _.first = _.head = _.take = function(array, n, guard) {
        if (array == null) return void 0;
        return n == null || guard ? array[0] : slice.call(array, 0, n);
    };
    _.initial = function(array, n, guard) {
        return slice.call(array, 0, array.length - (n == null || guard ? 1 : n));
    };
    _.last = function(array, n, guard) {
        if (array == null) return void 0;
        if (n == null || guard) {
            return array[array.length - 1];
        } else {
            return slice.call(array, Math.max(array.length - n, 0));
        }
    };
    _.rest = _.tail = _.drop = function(array, n, guard) {
        return slice.call(array, n == null || guard ? 1 : n);
    };
    _.compact = function(array) {
        return _.filter(array, _.identity);
    };
    var flatten = function(input, shallow, output) {
        if (shallow && _.every(input, _.isArray)) {
            return concat.apply(output, input);
        }
        each(input, function(value) {
            if (_.isArray(value) || _.isArguments(value)) {
                shallow ? push.apply(output, value) : flatten(value, shallow, output);
            } else {
                output.push(value);
            }
        });
        return output;
    };
    _.flatten = function(array, shallow) {
        return flatten(array, shallow, []);
    };
    _.without = function(array) {
        return _.difference(array, slice.call(arguments, 1));
    };
    _.uniq = _.unique = function(array, isSorted, iterator, context) {
        if (_.isFunction(isSorted)) {
            context = iterator;
            iterator = isSorted;
            isSorted = false;
        }
        var initial = iterator ? _.map(array, iterator, context) : array;
        var results = [];
        var seen = [];
        each(initial, function(value, index) {
            if (isSorted ? !index || seen[seen.length - 1] !== value : !_.contains(seen, value)) {
                seen.push(value);
                results.push(array[index]);
            }
        });
        return results;
    };
    _.union = function() {
        return _.uniq(_.flatten(arguments, true));
    };
    _.intersection = function(array) {
        var rest = slice.call(arguments, 1);
        return _.filter(_.uniq(array), function(item) {
            return _.every(rest, function(other) {
                return _.indexOf(other, item) >= 0;
            });
        });
    };
    _.difference = function(array) {
        var rest = concat.apply(ArrayProto, slice.call(arguments, 1));
        return _.filter(array, function(value) {
            return !_.contains(rest, value);
        });
    };
    _.zip = function() {
        var length = _.max(_.pluck(arguments, "length").concat(0));
        var results = new Array(length);
        for (var i = 0; i < length; i++) {
            results[i] = _.pluck(arguments, "" + i);
        }
        return results;
    };
    _.object = function(list, values) {
        if (list == null) return {};
        var result = {};
        for (var i = 0, length = list.length; i < length; i++) {
            if (values) {
                result[list[i]] = values[i];
            } else {
                result[list[i][0]] = list[i][1];
            }
        }
        return result;
    };
    _.indexOf = function(array, item, isSorted) {
        if (array == null) return -1;
        var i = 0,
            length = array.length;
        if (isSorted) {
            if (typeof isSorted == "number") {
                i = isSorted < 0 ? Math.max(0, length + isSorted) : isSorted;
            } else {
                i = _.sortedIndex(array, item);
                return array[i] === item ? i : -1;
            }
        }
        if (nativeIndexOf && array.indexOf === nativeIndexOf) return array.indexOf(item, isSorted);
        for (; i < length; i++)
            if (array[i] === item) return i;
        return -1;
    };
    _.lastIndexOf = function(array, item, from) {
        if (array == null) return -1;
        var hasIndex = from != null;
        if (nativeLastIndexOf && array.lastIndexOf === nativeLastIndexOf) {
            return hasIndex ? array.lastIndexOf(item, from) : array.lastIndexOf(item);
        }
        var i = hasIndex ? from : array.length;
        while (i--)
            if (array[i] === item) return i;
        return -1;
    };
    _.range = function(start, stop, step) {
        if (arguments.length <= 1) {
            stop = start || 0;
            start = 0;
        }
        step = arguments[2] || 1;
        var length = Math.max(Math.ceil((stop - start) / step), 0);
        var idx = 0;
        var range = new Array(length);
        while (idx < length) {
            range[idx++] = start;
            start += step;
        }
        return range;
    };
    var ctor = function() {};
    _.bind = function(func, context) {
        var args, bound;
        if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
        if (!_.isFunction(func)) throw new TypeError();
        args = slice.call(arguments, 2);
        return bound = function() {
            if (!(this instanceof bound)) return func.apply(context, args.concat(slice.call(arguments)));
            ctor.prototype = func.prototype;
            var self = new ctor();
            ctor.prototype = null;
            var result = func.apply(self, args.concat(slice.call(arguments)));
            if (Object(result) === result) return result;
            return self;
        };
    };
    _.partial = function(func) {
        var args = slice.call(arguments, 1);
        return function() {
            return func.apply(this, args.concat(slice.call(arguments)));
        };
    };
    _.bindAll = function(obj) {
        var funcs = slice.call(arguments, 1);
        if (funcs.length === 0) throw new Error("bindAll must be passed function names");
        each(funcs, function(f) {
            obj[f] = _.bind(obj[f], obj);
        });
        return obj;
    };
    _.memoize = function(func, hasher) {
        var memo = {};
        hasher || (hasher = _.identity);
        return function() {
            var key = hasher.apply(this, arguments);
            return _.has(memo, key) ? memo[key] : memo[key] = func.apply(this, arguments);
        };
    };
    _.delay = function(func, wait) {
        var args = slice.call(arguments, 2);
        return setTimeout(function() {
            return func.apply(null, args);
        }, wait);
    };
    _.defer = function(func) {
        return _.delay.apply(_, [func, 1].concat(slice.call(arguments, 1)));
    };
    _.throttle = function(func, wait, options) {
        var context, args, result;
        var timeout = null;
        var previous = 0;
        options || (options = {});
        var later = function() {
            previous = options.leading === false ? 0 : new Date();
            timeout = null;
            result = func.apply(context, args);
        };
        return function() {
            var now = new Date();
            if (!previous && options.leading === false) previous = now;
            var remaining = wait - (now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0) {
                clearTimeout(timeout);
                timeout = null;
                previous = now;
                result = func.apply(context, args);
            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        };
    };
    _.debounce = function(func, wait, immediate) {
        var timeout, args, context, timestamp, result;
        return function() {
            context = this;
            args = arguments;
            timestamp = new Date();
            var later = function() {
                var last = new Date() - timestamp;
                if (last < wait) {
                    timeout = setTimeout(later, wait - last);
                } else {
                    timeout = null;
                    if (!immediate) result = func.apply(context, args);
                }
            };
            var callNow = immediate && !timeout;
            if (!timeout) {
                timeout = setTimeout(later, wait);
            }
            if (callNow) result = func.apply(context, args);
            return result;
        };
    };
    _.once = function(func) {
        var ran = false,
            memo;
        return function() {
            if (ran) return memo;
            ran = true;
            memo = func.apply(this, arguments);
            func = null;
            return memo;
        };
    };
    _.wrap = function(func, wrapper) {
        return function() {
            var args = [func];
            push.apply(args, arguments);
            return wrapper.apply(this, args);
        };
    };
    _.compose = function() {
        var funcs = arguments;
        return function() {
            var args = arguments;
            for (var i = funcs.length - 1; i >= 0; i--) {
                args = [funcs[i].apply(this, args)];
            }
            return args[0];
        };
    };
    _.after = function(times, func) {
        return function() {
            if (--times < 1) {
                return func.apply(this, arguments);
            }
        };
    };
    _.keys = nativeKeys || function(obj) {
        if (obj !== Object(obj)) throw new TypeError("Invalid object");
        var keys = [];
        for (var key in obj)
            if (_.has(obj, key)) keys.push(key);
        return keys;
    };
    _.values = function(obj) {
        var keys = _.keys(obj);
        var length = keys.length;
        var values = new Array(length);
        for (var i = 0; i < length; i++) {
            values[i] = obj[keys[i]];
        }
        return values;
    };
    _.pairs = function(obj) {
        var keys = _.keys(obj);
        var length = keys.length;
        var pairs = new Array(length);
        for (var i = 0; i < length; i++) {
            pairs[i] = [keys[i], obj[keys[i]]];
        }
        return pairs;
    };
    _.invert = function(obj) {
        var result = {};
        var keys = _.keys(obj);
        for (var i = 0, length = keys.length; i < length; i++) {
            result[obj[keys[i]]] = keys[i];
        }
        return result;
    };
    _.functions = _.methods = function(obj) {
        var names = [];
        for (var key in obj) {
            if (_.isFunction(obj[key])) names.push(key);
        }
        return names.sort();
    };
    _.extend = function(obj) {
        each(slice.call(arguments, 1), function(source) {
            if (source) {
                for (var prop in source) {
                    obj[prop] = source[prop];
                }
            }
        });
        return obj;
    };
    _.pick = function(obj) {
        var copy = {};
        var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
        each(keys, function(key) {
            if (key in obj) copy[key] = obj[key];
        });
        return copy;
    };
    _.omit = function(obj) {
        var copy = {};
        var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
        for (var key in obj) {
            if (!_.contains(keys, key)) copy[key] = obj[key];
        }
        return copy;
    };
    _.defaults = function(obj) {
        each(slice.call(arguments, 1), function(source) {
            if (source) {
                for (var prop in source) {
                    if (obj[prop] === void 0) obj[prop] = source[prop];
                }
            }
        });
        return obj;
    };
    _.clone = function(obj) {
        if (!_.isObject(obj)) return obj;
        return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
    };
    _.tap = function(obj, interceptor) {
        interceptor(obj);
        return obj;
    };
    var eq = function(a, b, aStack, bStack) {
        if (a === b) return a !== 0 || 1 / a == 1 / b;
        if (a == null || b == null) return a === b;
        if (a instanceof _) a = a._wrapped;
        if (b instanceof _) b = b._wrapped;
        var className = toString.call(a);
        if (className != toString.call(b)) return false;
        switch (className) {
            case "[object String]":
                return a == String(b);

            case "[object Number]":
                return a != +a ? b != +b : a == 0 ? 1 / a == 1 / b : a == +b;

            case "[object Date]":
            case "[object Boolean]":
                return +a == +b;

            case "[object RegExp]":
                return a.source == b.source && a.global == b.global && a.multiline == b.multiline && a.ignoreCase == b.ignoreCase;
        }
        if (typeof a != "object" || typeof b != "object") return false;
        var length = aStack.length;
        while (length--) {
            if (aStack[length] == a) return bStack[length] == b;
        }
        var aCtor = a.constructor,
            bCtor = b.constructor;
        if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor && _.isFunction(bCtor) && bCtor instanceof bCtor)) {
            return false;
        }
        aStack.push(a);
        bStack.push(b);
        var size = 0,
            result = true;
        if (className == "[object Array]") {
            size = a.length;
            result = size == b.length;
            if (result) {
                while (size--) {
                    if (!(result = eq(a[size], b[size], aStack, bStack))) break;
                }
            }
        } else {
            for (var key in a) {
                if (_.has(a, key)) {
                    size++;
                    if (!(result = _.has(b, key) && eq(a[key], b[key], aStack, bStack))) break;
                }
            }
            if (result) {
                for (key in b) {
                    if (_.has(b, key) && !size--) break;
                }
                result = !size;
            }
        }
        aStack.pop();
        bStack.pop();
        return result;
    };
    _.isEqual = function(a, b) {
        return eq(a, b, [], []);
    };
    _.isEmpty = function(obj) {
        if (obj == null) return true;
        if (_.isArray(obj) || _.isString(obj)) return obj.length === 0;
        for (var key in obj)
            if (_.has(obj, key)) return false;
        return true;
    };
    _.isElement = function(obj) {
        return !!(obj && obj.nodeType === 1);
    };
    _.isArray = nativeIsArray || function(obj) {
        return toString.call(obj) == "[object Array]";
    };
    _.isObject = function(obj) {
        return obj === Object(obj);
    };
    each(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function(name) {
        _["is" + name] = function(obj) {
            return toString.call(obj) == "[object " + name + "]";
        };
    });
    if (!_.isArguments(arguments)) {
        _.isArguments = function(obj) {
            return !!(obj && _.has(obj, "callee"));
        };
    }
    if (typeof /./ !== "function") {
        _.isFunction = function(obj) {
            return typeof obj === "function";
        };
    }
    _.isFinite = function(obj) {
        return isFinite(obj) && !isNaN(parseFloat(obj));
    };
    _.isNaN = function(obj) {
        return _.isNumber(obj) && obj != +obj;
    };
    _.isBoolean = function(obj) {
        return obj === true || obj === false || toString.call(obj) == "[object Boolean]";
    };
    _.isNull = function(obj) {
        return obj === null;
    };
    _.isUndefined = function(obj) {
        return obj === void 0;
    };
    _.has = function(obj, key) {
        return hasOwnProperty.call(obj, key);
    };
    _.noConflict = function() {
        root._ = previousUnderscore;
        return this;
    };
    _.identity = function(value) {
        return value;
    };
    _.times = function(n, iterator, context) {
        var accum = Array(Math.max(0, n));
        for (var i = 0; i < n; i++) accum[i] = iterator.call(context, i);
        return accum;
    };
    _.random = function(min, max) {
        if (max == null) {
            max = min;
            min = 0;
        }
        return min + Math.floor(Math.random() * (max - min + 1));
    };
    var entityMap = {
        escape: {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;"
        }
    };
    entityMap.unescape = _.invert(entityMap.escape);
    var entityRegexes = {
        escape: new RegExp("[" + _.keys(entityMap.escape).join("") + "]", "g"),
        unescape: new RegExp("(" + _.keys(entityMap.unescape).join("|") + ")", "g")
    };
    _.each(["escape", "unescape"], function(method) {
        _[method] = function(string) {
            if (string == null) return "";
            return ("" + string).replace(entityRegexes[method], function(match) {
                return entityMap[method][match];
            });
        };
    });
    _.result = function(object, property) {
        if (object == null) return void 0;
        var value = object[property];
        return _.isFunction(value) ? value.call(object) : value;
    };
    _.mixin = function(obj) {
        each(_.functions(obj), function(name) {
            var func = _[name] = obj[name];
            _.prototype[name] = function() {
                var args = [this._wrapped];
                push.apply(args, arguments);
                return result.call(this, func.apply(_, args));
            };
        });
    };
    var idCounter = 0;
    _.uniqueId = function(prefix) {
        var id = ++idCounter + "";
        return prefix ? prefix + id : id;
    };
    _.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var noMatch = /(.)^/;
    var escapes = {
        "'": "'",
        "\\": "\\",
        "\r": "r",
        "\n": "n",
        "	": "t",
        "\u2028": "u2028",
        "\u2029": "u2029"
    };
    var escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    _.template = function(text, data, settings) {
        var render;
        settings = _.defaults({}, settings, _.templateSettings);
        var matcher = new RegExp([(settings.escape || noMatch).source, (settings.interpolate || noMatch).source, (settings.evaluate || noMatch).source].join("|") + "|$", "g");
        var index = 0;
        var source = "__p+='";
        text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
            source += text.slice(index, offset).replace(escaper, function(match) {
                return "\\" + escapes[match];
            });
            if (escape) {
                source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
            }
            if (interpolate) {
                source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
            }
            if (evaluate) {
                source += "';\n" + evaluate + "\n__p+='";
            }
            index = offset + match.length;
            return match;
        });
        source += "';\n";
        if (!settings.variable) source = "with(obj||{}){\n" + source + "}\n";
        source = "var __t,__p='',__j=Array.prototype.join," + "print=function(){__p+=__j.call(arguments,'');};\n" + source + "return __p;\n";
        try {
            render = new Function(settings.variable || "obj", "_", source);
        } catch (e) {
            e.source = source;
            throw e;
        }
        if (data) return render(data, _);
        var template = function(data) {
            return render.call(this, data, _);
        };
        template.source = "function(" + (settings.variable || "obj") + "){\n" + source + "}";
        return template;
    };
    _.chain = function(obj) {
        return _(obj).chain();
    };
    var result = function(obj) {
        return this._chain ? _(obj).chain() : obj;
    };
    _.mixin(_);
    each(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(name) {
        var method = ArrayProto[name];
        _.prototype[name] = function() {
            var obj = this._wrapped;
            method.apply(obj, arguments);
            if ((name == "shift" || name == "splice") && obj.length === 0) delete obj[0];
            return result.call(this, obj);
        };
    });
    each(["concat", "join", "slice"], function(name) {
        var method = ArrayProto[name];
        _.prototype[name] = function() {
            return result.call(this, method.apply(this._wrapped, arguments));
        };
    });
    _.extend(_.prototype, {
        chain: function() {
            this._chain = true;
            return this;
        },
        value: function() {
            return this._wrapped;
        }
    });
}).call(this);

define("underscore", function(global) {
    return function() {
        var ret, fn;
        return ret || global._;
    };
}(this));

(function(window, Object, Array, Error, JSON, undefined) {
    var partialComplete = varArgs(function(fn, args) {
            var numBoundArgs = args.length;
            return varArgs(function(callArgs) {
                for (var i = 0; i < callArgs.length; i++) {
                    args[numBoundArgs + i] = callArgs[i];
                }
                args.length = numBoundArgs + callArgs.length;
                return fn.apply(this, args);
            });
        }),
        compose = varArgs(function(fns) {
            var fnsList = arrayAsList(fns);

            function next(params, curFn) {
                return [apply(params, curFn)];
            }
            return varArgs(function(startParams) {
                return foldR(next, startParams, fnsList)[0];
            });
        });

    function compose2(f1, f2) {
        return function() {
            return f1.call(this, f2.apply(this, arguments));
        };
    }

    function attr(key) {
        return function(o) {
            return o[key];
        };
    }
    var lazyUnion = varArgs(function(fns) {
        return varArgs(function(params) {
            var maybeValue;
            for (var i = 0; i < len(fns); i++) {
                maybeValue = apply(params, fns[i]);
                if (maybeValue) {
                    return maybeValue;
                }
            }
        });
    });

    function apply(args, fn) {
        return fn.apply(undefined, args);
    }

    function varArgs(fn) {
        var numberOfFixedArguments = fn.length - 1,
            slice = Array.prototype.slice;
        if (numberOfFixedArguments == 0) {
            return function() {
                return fn.call(this, slice.call(arguments));
            };
        } else if (numberOfFixedArguments == 1) {
            return function() {
                return fn.call(this, arguments[0], slice.call(arguments, 1));
            };
        }
        var argsHolder = Array(fn.length);
        return function() {
            for (var i = 0; i < numberOfFixedArguments; i++) {
                argsHolder[i] = arguments[i];
            }
            argsHolder[numberOfFixedArguments] = slice.call(arguments, numberOfFixedArguments);
            return fn.apply(this, argsHolder);
        };
    }

    function flip(fn) {
        return function(a, b) {
            return fn(b, a);
        };
    }

    function lazyIntersection(fn1, fn2) {
        return function(param) {
            return fn1(param) && fn2(param);
        };
    }

    function noop() {}

    function always() {
        return true;
    }

    function functor(val) {
        return function() {
            return val;
        };
    }

    function isOfType(T, maybeSomething) {
        return maybeSomething && maybeSomething.constructor === T;
    }
    var len = attr("length"),
        isString = partialComplete(isOfType, String);

    function defined(value) {
        return value !== undefined;
    }

    function hasAllProperties(fieldList, o) {
        return o instanceof Object && all(function(field) {
            return field in o;
        }, fieldList);
    }

    function cons(x, xs) {
        return [x, xs];
    }
    var emptyList = null,
        head = attr(0),
        tail = attr(1);

    function arrayAsList(inputArray) {
        return reverseList(inputArray.reduce(flip(cons), emptyList));
    }
    var list = varArgs(arrayAsList);

    function listAsArray(list) {
        return foldR(function(arraySoFar, listItem) {
            arraySoFar.unshift(listItem);
            return arraySoFar;
        }, [], list);
    }

    function map(fn, list) {
        return list ? cons(fn(head(list)), map(fn, tail(list))) : emptyList;
    }

    function foldR(fn, startValue, list) {
        return list ? fn(foldR(fn, startValue, tail(list)), head(list)) : startValue;
    }

    function foldR1(fn, list) {
        return tail(list) ? fn(foldR1(fn, tail(list)), head(list)) : head(list);
    }

    function without(list, test, removedFn) {
        return withoutInner(list, removedFn || noop);

        function withoutInner(subList, removedFn) {
            return subList ? test(head(subList)) ? (removedFn(head(subList)), tail(subList)) : cons(head(subList), withoutInner(tail(subList), removedFn)) : emptyList;
        }
    }

    function all(fn, list) {
        return !list || fn(head(list)) && all(fn, tail(list));
    }

    function applyEach(fnList, args) {
        if (fnList) {
            head(fnList).apply(null, args);
            applyEach(tail(fnList), args);
        }
    }

    function reverseList(list) {
        function reverseInner(list, reversedAlready) {
            if (!list) {
                return reversedAlready;
            }
            return reverseInner(tail(list), cons(head(list), reversedAlready));
        }
        return reverseInner(list, emptyList);
    }

    function first(test, list) {
        return list && (test(head(list)) ? head(list) : first(test, tail(list)));
    }

    function clarinet(eventBus) {
        "use strict";
        var emitSaxKey = eventBus(SAX_KEY).emit,
            emitValueOpen = eventBus(SAX_VALUE_OPEN).emit,
            emitValueClose = eventBus(SAX_VALUE_CLOSE).emit,
            emitFail = eventBus(FAIL_EVENT).emit,
            MAX_BUFFER_LENGTH = 64 * 1024,
            stringTokenPattern = /[\\"\n]/g,
            _n = 0,
            BEGIN = _n++,
            VALUE = _n++,
            OPEN_OBJECT = _n++,
            CLOSE_OBJECT = _n++,
            OPEN_ARRAY = _n++,
            CLOSE_ARRAY = _n++,
            STRING = _n++,
            OPEN_KEY = _n++,
            CLOSE_KEY = _n++,
            TRUE = _n++,
            TRUE2 = _n++,
            TRUE3 = _n++,
            FALSE = _n++,
            FALSE2 = _n++,
            FALSE3 = _n++,
            FALSE4 = _n++,
            NULL = _n++,
            NULL2 = _n++,
            NULL3 = _n++,
            NUMBER_DECIMAL_POINT = _n++,
            NUMBER_DIGIT = _n,
            bufferCheckPosition = MAX_BUFFER_LENGTH,
            latestError, c, p, textNode = "",
            numberNode = "",
            slashed = false,
            closed = false,
            state = BEGIN,
            stack = [],
            unicodeS = null,
            unicodeI = 0,
            depth = 0,
            position = 0,
            column = 0,
            line = 1;

        function checkBufferLength() {
            var maxActual = 0;
            if (textNode.length > MAX_BUFFER_LENGTH) {
                emitError("Max buffer length exceeded: textNode");
                maxActual = Math.max(maxActual, textNode.length);
            }
            if (numberNode.length > MAX_BUFFER_LENGTH) {
                emitError("Max buffer length exceeded: numberNode");
                maxActual = Math.max(maxActual, numberNode.length);
            }
            bufferCheckPosition = MAX_BUFFER_LENGTH - maxActual + position;
        }
        eventBus(STREAM_DATA).on(handleData);
        eventBus(STREAM_END).on(handleStreamEnd);

        function emitError(errorString) {
            if (textNode) {
                emitValueOpen(textNode);
                emitValueClose();
                textNode = "";
            }
            latestError = Error(errorString + "\nLn: " + line + "\nCol: " + column + "\nChr: " + c);
            emitFail(errorReport(undefined, undefined, latestError));
        }

        function handleStreamEnd() {
            if (state == BEGIN) {
                emitValueOpen({});
                emitValueClose();
                closed = true;
                return;
            }
            if (state !== VALUE || depth !== 0) emitError("Unexpected end");
            if (textNode) {
                emitValueOpen(textNode);
                emitValueClose();
                textNode = "";
            }
            closed = true;
        }

        function whitespace(c) {
            return c == "\r" || c == "\n" || c == " " || c == "	";
        }

        function handleData(chunk) {
            if (latestError) return;
            if (closed) {
                return emitError("Cannot write after close");
            }
            var i = 0;
            c = chunk[0];
            while (c) {
                p = c;
                c = chunk[i++];
                if (!c) break;
                position++;
                if (c == "\n") {
                    line++;
                    column = 0;
                } else column++;
                switch (state) {
                    case BEGIN:
                        if (c === "{") state = OPEN_OBJECT;
                        else if (c === "[") state = OPEN_ARRAY;
                        else if (!whitespace(c)) return emitError("Non-whitespace before {[.");
                        continue;

                    case OPEN_KEY:
                    case OPEN_OBJECT:
                        if (whitespace(c)) continue;
                        if (state === OPEN_KEY) stack.push(CLOSE_KEY);
                        else {
                            if (c === "}") {
                                emitValueOpen({});
                                emitValueClose();
                                state = stack.pop() || VALUE;
                                continue;
                            } else stack.push(CLOSE_OBJECT);
                        }
                        if (c === '"') state = STRING;
                        else return emitError('Malformed object key should start with " ');
                        continue;

                    case CLOSE_KEY:
                    case CLOSE_OBJECT:
                        if (whitespace(c)) continue;
                        if (c === ":") {
                            if (state === CLOSE_OBJECT) {
                                stack.push(CLOSE_OBJECT);
                                if (textNode) {
                                    emitValueOpen({});
                                    emitSaxKey(textNode);
                                    textNode = "";
                                }
                                depth++;
                            } else {
                                if (textNode) {
                                    emitSaxKey(textNode);
                                    textNode = "";
                                }
                            }
                            state = VALUE;
                        } else if (c === "}") {
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            emitValueClose();
                            depth--;
                            state = stack.pop() || VALUE;
                        } else if (c === ",") {
                            if (state === CLOSE_OBJECT) stack.push(CLOSE_OBJECT);
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            state = OPEN_KEY;
                        } else return emitError("Bad object");
                        continue;

                    case OPEN_ARRAY:
                    case VALUE:
                        if (whitespace(c)) continue;
                        if (state === OPEN_ARRAY) {
                            emitValueOpen([]);
                            depth++;
                            state = VALUE;
                            if (c === "]") {
                                emitValueClose();
                                depth--;
                                state = stack.pop() || VALUE;
                                continue;
                            } else {
                                stack.push(CLOSE_ARRAY);
                            }
                        }
                        if (c === '"') state = STRING;
                        else if (c === "{") state = OPEN_OBJECT;
                        else if (c === "[") state = OPEN_ARRAY;
                        else if (c === "t") state = TRUE;
                        else if (c === "f") state = FALSE;
                        else if (c === "n") state = NULL;
                        else if (c === "-") {
                            numberNode += c;
                        } else if (c === "0") {
                            numberNode += c;
                            state = NUMBER_DIGIT;
                        } else if ("123456789".indexOf(c) !== -1) {
                            numberNode += c;
                            state = NUMBER_DIGIT;
                        } else return emitError("Bad value");
                        continue;

                    case CLOSE_ARRAY:
                        if (c === ",") {
                            stack.push(CLOSE_ARRAY);
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            state = VALUE;
                        } else if (c === "]") {
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            emitValueClose();
                            depth--;
                            state = stack.pop() || VALUE;
                        } else if (whitespace(c)) continue;
                        else return emitError("Bad array");
                        continue;

                    case STRING:
                        var starti = i - 1;
                        STRING_BIGLOOP: while (true) {
                            while (unicodeI > 0) {
                                unicodeS += c;
                                c = chunk.charAt(i++);
                                if (unicodeI === 4) {
                                    textNode += String.fromCharCode(parseInt(unicodeS, 16));
                                    unicodeI = 0;
                                    starti = i - 1;
                                } else {
                                    unicodeI++;
                                }
                                if (!c) break STRING_BIGLOOP;
                            }
                            if (c === '"' && !slashed) {
                                state = stack.pop() || VALUE;
                                textNode += chunk.substring(starti, i - 1);
                                if (!textNode) {
                                    emitValueOpen("");
                                    emitValueClose();
                                }
                                break;
                            }
                            if (c === "\\" && !slashed) {
                                slashed = true;
                                textNode += chunk.substring(starti, i - 1);
                                c = chunk.charAt(i++);
                                if (!c) break;
                            }
                            if (slashed) {
                                slashed = false;
                                if (c === "n") {
                                    textNode += "\n";
                                } else if (c === "r") {
                                    textNode += "\r";
                                } else if (c === "t") {
                                    textNode += "	";
                                } else if (c === "f") {
                                    textNode += "\f";
                                } else if (c === "b") {
                                    textNode += "\b";
                                } else if (c === "u") {
                                    unicodeI = 1;
                                    unicodeS = "";
                                } else {
                                    textNode += c;
                                }
                                c = chunk.charAt(i++);
                                starti = i - 1;
                                if (!c) break;
                                else continue;
                            }
                            stringTokenPattern.lastIndex = i;
                            var reResult = stringTokenPattern.exec(chunk);
                            if (!reResult) {
                                i = chunk.length + 1;
                                textNode += chunk.substring(starti, i - 1);
                                break;
                            }
                            i = reResult.index + 1;
                            c = chunk.charAt(reResult.index);
                            if (!c) {
                                textNode += chunk.substring(starti, i - 1);
                                break;
                            }
                        }
                        continue;

                    case TRUE:
                        if (!c) continue;
                        if (c === "r") state = TRUE2;
                        else return emitError("Invalid true started with t" + c);
                        continue;

                    case TRUE2:
                        if (!c) continue;
                        if (c === "u") state = TRUE3;
                        else return emitError("Invalid true started with tr" + c);
                        continue;

                    case TRUE3:
                        if (!c) continue;
                        if (c === "e") {
                            emitValueOpen(true);
                            emitValueClose();
                            state = stack.pop() || VALUE;
                        } else return emitError("Invalid true started with tru" + c);
                        continue;

                    case FALSE:
                        if (!c) continue;
                        if (c === "a") state = FALSE2;
                        else return emitError("Invalid false started with f" + c);
                        continue;

                    case FALSE2:
                        if (!c) continue;
                        if (c === "l") state = FALSE3;
                        else return emitError("Invalid false started with fa" + c);
                        continue;

                    case FALSE3:
                        if (!c) continue;
                        if (c === "s") state = FALSE4;
                        else return emitError("Invalid false started with fal" + c);
                        continue;

                    case FALSE4:
                        if (!c) continue;
                        if (c === "e") {
                            emitValueOpen(false);
                            emitValueClose();
                            state = stack.pop() || VALUE;
                        } else return emitError("Invalid false started with fals" + c);
                        continue;

                    case NULL:
                        if (!c) continue;
                        if (c === "u") state = NULL2;
                        else return emitError("Invalid null started with n" + c);
                        continue;

                    case NULL2:
                        if (!c) continue;
                        if (c === "l") state = NULL3;
                        else return emitError("Invalid null started with nu" + c);
                        continue;

                    case NULL3:
                        if (!c) continue;
                        if (c === "l") {
                            emitValueOpen(null);
                            emitValueClose();
                            state = stack.pop() || VALUE;
                        } else return emitError("Invalid null started with nul" + c);
                        continue;

                    case NUMBER_DECIMAL_POINT:
                        if (c === ".") {
                            numberNode += c;
                            state = NUMBER_DIGIT;
                        } else return emitError("Leading zero not followed by .");
                        continue;

                    case NUMBER_DIGIT:
                        if ("0123456789".indexOf(c) !== -1) numberNode += c;
                        else if (c === ".") {
                            if (numberNode.indexOf(".") !== -1) return emitError("Invalid number has two dots");
                            numberNode += c;
                        } else if (c === "e" || c === "E") {
                            if (numberNode.indexOf("e") !== -1 || numberNode.indexOf("E") !== -1) return emitError("Invalid number has two exponential");
                            numberNode += c;
                        } else if (c === "+" || c === "-") {
                            if (!(p === "e" || p === "E")) return emitError("Invalid symbol in number");
                            numberNode += c;
                        } else {
                            if (numberNode) {
                                emitValueOpen(parseFloat(numberNode));
                                emitValueClose();
                                numberNode = "";
                            }
                            i--;
                            state = stack.pop() || VALUE;
                        }
                        continue;

                    default:
                        return emitError("Unknown state: " + state);
                }
            }
            if (position >= bufferCheckPosition) checkBufferLength();
        }
    }

    function ascentManager(oboeBus, handlers) {
        "use strict";
        var listenerId = {},
            ascent;

        function stateAfter(handler) {
            return function(param) {
                ascent = handler(ascent, param);
            };
        }
        for (var eventName in handlers) {
            oboeBus(eventName).on(stateAfter(handlers[eventName]), listenerId);
        }
        oboeBus(NODE_SWAP).on(function(newNode) {
            var oldHead = head(ascent),
                key = keyOf(oldHead),
                ancestors = tail(ascent),
                parentNode;
            if (ancestors) {
                parentNode = nodeOf(head(ancestors));
                parentNode[key] = newNode;
            }
        });
        oboeBus(NODE_DROP).on(function() {
            var oldHead = head(ascent),
                key = keyOf(oldHead),
                ancestors = tail(ascent),
                parentNode;
            if (ancestors) {
                parentNode = nodeOf(head(ancestors));
                delete parentNode[key];
            }
        });
        oboeBus(ABORTING).on(function() {
            for (var eventName in handlers) {
                oboeBus(eventName).un(listenerId);
            }
        });
    }

    function parseResponseHeaders(headerStr) {
        var headers = {};
        headerStr && headerStr.split("\r\n").forEach(function(headerPair) {
            var index = headerPair.indexOf(": ");
            headers[headerPair.substring(0, index)] = headerPair.substring(index + 2);
        });
        return headers;
    }

    function isCrossOrigin(pageLocation, ajaxHost) {
        function defaultPort(protocol) {
            return {
                "http:": 80,
                "https:": 443
            }[protocol];
        }

        function portOf(location) {
            return location.port || defaultPort(location.protocol || pageLocation.protocol);
        }
        return !!(ajaxHost.protocol && ajaxHost.protocol != pageLocation.protocol || ajaxHost.host && ajaxHost.host != pageLocation.host || ajaxHost.host && portOf(ajaxHost) != portOf(pageLocation));
    }

    function parseUrlOrigin(url) {
        var URL_HOST_PATTERN = /(\w+:)?(?:\/\/)([\w.-]+)?(?::(\d+))?\/?/,
            urlHostMatch = URL_HOST_PATTERN.exec(url) || [];
        return {
            protocol: urlHostMatch[1] || "",
            host: urlHostMatch[2] || "",
            port: urlHostMatch[3] || ""
        };
    }

    function httpTransport() {
        return new XMLHttpRequest();
    }

    function streamingHttp(oboeBus, xhr, method, url, data, headers, withCredentials) {
        "use strict";
        var emitStreamData = oboeBus(STREAM_DATA).emit,
            emitFail = oboeBus(FAIL_EVENT).emit,
            numberOfCharsAlreadyGivenToCallback = 0,
            stillToSendStartEvent = true;
        oboeBus(ABORTING).on(function() {
            xhr.onreadystatechange = null;
            xhr.abort();
        });

        function handleProgress() {
            var textSoFar = xhr.responseText,
                newText = textSoFar.substr(numberOfCharsAlreadyGivenToCallback);
            if (newText) {
                emitStreamData(newText);
            }
            numberOfCharsAlreadyGivenToCallback = len(textSoFar);
        }
        if ("onprogress" in xhr) {
            xhr.onprogress = handleProgress;
        }
        xhr.onreadystatechange = function() {
            function sendStartIfNotAlready() {
                try {
                    stillToSendStartEvent && oboeBus(HTTP_START).emit(xhr.status, parseResponseHeaders(xhr.getAllResponseHeaders()));
                    stillToSendStartEvent = false;
                } catch (e) {}
            }
            switch (xhr.readyState) {
                case 2:
                case 3:
                    return sendStartIfNotAlready();

                case 4:
                    sendStartIfNotAlready();
                    var successful = String(xhr.status)[0] == 2;
                    if (successful) {
                        handleProgress();
                        oboeBus(STREAM_END).emit();
                    } else {
                        emitFail(errorReport(xhr.status, xhr.responseText));
                    }
            }
        };
        try {
            xhr.open(method, url, true);
            for (var headerName in headers) {
                xhr.setRequestHeader(headerName, headers[headerName]);
            }
            if (!isCrossOrigin(window.location, parseUrlOrigin(url))) {
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            }
            xhr.withCredentials = withCredentials;
            xhr.send(data);
        } catch (e) {
            window.setTimeout(partialComplete(emitFail, errorReport(undefined, undefined, e)), 0);
        }
    }
    var jsonPathSyntax = function() {
        var regexDescriptor = function regexDescriptor(regex) {
                return regex.exec.bind(regex);
            },
            jsonPathClause = varArgs(function(componentRegexes) {
                componentRegexes.unshift(/^/);
                return regexDescriptor(RegExp(componentRegexes.map(attr("source")).join("")));
            }),
            possiblyCapturing = /(\$?)/,
            namedNode = /([\w-_]+|\*)/,
            namePlaceholder = /()/,
            nodeInArrayNotation = /\["([^"]+)"\]/,
            numberedNodeInArrayNotation = /\[(\d+|\*)\]/,
            fieldList = /{([\w ]*?)}/,
            optionalFieldList = /(?:{([\w ]*?)})?/,
            jsonPathNamedNodeInObjectNotation = jsonPathClause(possiblyCapturing, namedNode, optionalFieldList),
            jsonPathNamedNodeInArrayNotation = jsonPathClause(possiblyCapturing, nodeInArrayNotation, optionalFieldList),
            jsonPathNumberedNodeInArrayNotation = jsonPathClause(possiblyCapturing, numberedNodeInArrayNotation, optionalFieldList),
            jsonPathPureDuckTyping = jsonPathClause(possiblyCapturing, namePlaceholder, fieldList),
            jsonPathDoubleDot = jsonPathClause(/\.\./),
            jsonPathDot = jsonPathClause(/\./),
            jsonPathBang = jsonPathClause(possiblyCapturing, /!/),
            emptyString = jsonPathClause(/$/);
        return function(fn) {
            return fn(lazyUnion(jsonPathNamedNodeInObjectNotation, jsonPathNamedNodeInArrayNotation, jsonPathNumberedNodeInArrayNotation, jsonPathPureDuckTyping), jsonPathDoubleDot, jsonPathDot, jsonPathBang, emptyString);
        };
    }();

    function namedNode(key, node) {
        return {
            key: key,
            node: node
        };
    }
    var keyOf = attr("key");
    var nodeOf = attr("node");
    var ROOT_PATH = {};

    function incrementalContentBuilder(oboeBus) {
        var emitNodeOpened = oboeBus(NODE_OPENED).emit,
            emitNodeClosed = oboeBus(NODE_CLOSED).emit,
            emitRootOpened = oboeBus(ROOT_PATH_FOUND).emit,
            emitRootClosed = oboeBus(ROOT_NODE_FOUND).emit;

        function arrayIndicesAreKeys(possiblyInconsistentAscent, newDeepestNode) {
            var parentNode = nodeOf(head(possiblyInconsistentAscent));
            return isOfType(Array, parentNode) ? keyFound(possiblyInconsistentAscent, len(parentNode), newDeepestNode) : possiblyInconsistentAscent;
        }

        function nodeOpened(ascent, newDeepestNode) {
            if (!ascent) {
                emitRootOpened(newDeepestNode);
                return keyFound(ascent, ROOT_PATH, newDeepestNode);
            }
            var arrayConsistentAscent = arrayIndicesAreKeys(ascent, newDeepestNode),
                ancestorBranches = tail(arrayConsistentAscent),
                previouslyUnmappedName = keyOf(head(arrayConsistentAscent));
            appendBuiltContent(ancestorBranches, previouslyUnmappedName, newDeepestNode);
            return cons(namedNode(previouslyUnmappedName, newDeepestNode), ancestorBranches);
        }

        function appendBuiltContent(ancestorBranches, key, node) {
            nodeOf(head(ancestorBranches))[key] = node;
        }

        function keyFound(ascent, newDeepestName, maybeNewDeepestNode) {
            if (ascent) {
                appendBuiltContent(ascent, newDeepestName, maybeNewDeepestNode);
            }
            var ascentWithNewPath = cons(namedNode(newDeepestName, maybeNewDeepestNode), ascent);
            emitNodeOpened(ascentWithNewPath);
            return ascentWithNewPath;
        }

        function nodeClosed(ascent) {
            emitNodeClosed(ascent);
            return tail(ascent) || emitRootClosed(nodeOf(head(ascent)));
        }
        var contentBuilderHandlers = {};
        contentBuilderHandlers[SAX_VALUE_OPEN] = nodeOpened;
        contentBuilderHandlers[SAX_VALUE_CLOSE] = nodeClosed;
        contentBuilderHandlers[SAX_KEY] = keyFound;
        return contentBuilderHandlers;
    }
    var jsonPathCompiler = jsonPathSyntax(function(pathNodeSyntax, doubleDotSyntax, dotSyntax, bangSyntax, emptySyntax) {
        var CAPTURING_INDEX = 1;
        var NAME_INDEX = 2;
        var FIELD_LIST_INDEX = 3;
        var headKey = compose2(keyOf, head),
            headNode = compose2(nodeOf, head);

        function nameClause(previousExpr, detection) {
            var name = detection[NAME_INDEX],
                matchesName = !name || name == "*" ? always : function(ascent) {
                    return headKey(ascent) == name;
                };
            return lazyIntersection(matchesName, previousExpr);
        }

        function duckTypeClause(previousExpr, detection) {
            var fieldListStr = detection[FIELD_LIST_INDEX];
            if (!fieldListStr) return previousExpr;
            var hasAllrequiredFields = partialComplete(hasAllProperties, arrayAsList(fieldListStr.split(/\W+/))),
                isMatch = compose2(hasAllrequiredFields, headNode);
            return lazyIntersection(isMatch, previousExpr);
        }

        function capture(previousExpr, detection) {
            var capturing = !!detection[CAPTURING_INDEX];
            if (!capturing) return previousExpr;
            return lazyIntersection(previousExpr, head);
        }

        function skip1(previousExpr) {
            if (previousExpr == always) {
                return always;
            }

            function notAtRoot(ascent) {
                return headKey(ascent) != ROOT_PATH;
            }
            return lazyIntersection(notAtRoot, compose2(previousExpr, tail));
        }

        function skipMany(previousExpr) {
            if (previousExpr == always) {
                return always;
            }
            var terminalCaseWhenArrivingAtRoot = rootExpr(),
                terminalCaseWhenPreviousExpressionIsSatisfied = previousExpr,
                recursiveCase = skip1(function(ascent) {
                    return cases(ascent);
                }),
                cases = lazyUnion(terminalCaseWhenArrivingAtRoot, terminalCaseWhenPreviousExpressionIsSatisfied, recursiveCase);
            return cases;
        }

        function rootExpr() {
            return function(ascent) {
                return headKey(ascent) == ROOT_PATH;
            };
        }

        function statementExpr(lastClause) {
            return function(ascent) {
                var exprMatch = lastClause(ascent);
                return exprMatch === true ? head(ascent) : exprMatch;
            };
        }

        function expressionsReader(exprs, parserGeneratedSoFar, detection) {
            return foldR(function(parserGeneratedSoFar, expr) {
                return expr(parserGeneratedSoFar, detection);
            }, parserGeneratedSoFar, exprs);
        }

        function generateClauseReaderIfTokenFound(tokenDetector, clauseEvaluatorGenerators, jsonPath, parserGeneratedSoFar, onSuccess) {
            var detected = tokenDetector(jsonPath);
            if (detected) {
                var compiledParser = expressionsReader(clauseEvaluatorGenerators, parserGeneratedSoFar, detected),
                    remainingUnparsedJsonPath = jsonPath.substr(len(detected[0]));
                return onSuccess(remainingUnparsedJsonPath, compiledParser);
            }
        }

        function clauseMatcher(tokenDetector, exprs) {
            return partialComplete(generateClauseReaderIfTokenFound, tokenDetector, exprs);
        }
        var clauseForJsonPath = lazyUnion(clauseMatcher(pathNodeSyntax, list(capture, duckTypeClause, nameClause, skip1)), clauseMatcher(doubleDotSyntax, list(skipMany)), clauseMatcher(dotSyntax, list()), clauseMatcher(bangSyntax, list(capture, rootExpr)), clauseMatcher(emptySyntax, list(statementExpr)), function(jsonPath) {
            throw Error('"' + jsonPath + '" could not be tokenised');
        });

        function returnFoundParser(_remainingJsonPath, compiledParser) {
            return compiledParser;
        }

        function compileJsonPathToFunction(uncompiledJsonPath, parserGeneratedSoFar) {
            var onFind = uncompiledJsonPath ? compileJsonPathToFunction : returnFoundParser;
            return clauseForJsonPath(uncompiledJsonPath, parserGeneratedSoFar, onFind);
        }
        return function(jsonPath) {
            try {
                return compileJsonPathToFunction(jsonPath, always);
            } catch (e) {
                throw Error('Could not compile "' + jsonPath + '" because ' + e.message);
            }
        };
    });

    function singleEventPubSub(eventType, newListener, removeListener) {
        var listenerTupleList, listenerList;

        function hasId(id) {
            return function(tuple) {
                return tuple.id == id;
            };
        }
        return {
            on: function(listener, listenerId) {
                var tuple = {
                    listener: listener,
                    id: listenerId || listener
                };
                if (newListener) {
                    newListener.emit(eventType, listener, tuple.id);
                }
                listenerTupleList = cons(tuple, listenerTupleList);
                listenerList = cons(listener, listenerList);
                return this;
            },
            emit: function() {
                applyEach(listenerList, arguments);
            },
            un: function(listenerId) {
                var removed;
                listenerTupleList = without(listenerTupleList, hasId(listenerId), function(tuple) {
                    removed = tuple;
                });
                if (removed) {
                    listenerList = without(listenerList, function(listener) {
                        return listener == removed.listener;
                    });
                    if (removeListener) {
                        removeListener.emit(eventType, removed.listener, removed.id);
                    }
                }
            },
            listeners: function() {
                return listenerList;
            },
            hasListener: function(listenerId) {
                var test = listenerId ? hasId(listenerId) : always;
                return defined(first(test, listenerTupleList));
            }
        };
    }

    function pubSub() {
        var singles = {},
            newListener = newSingle("newListener"),
            removeListener = newSingle("removeListener");

        function newSingle(eventName) {
            return singles[eventName] = singleEventPubSub(eventName, newListener, removeListener);
        }

        function pubSubInstance(eventName) {
            return singles[eventName] || newSingle(eventName);
        }
        ["emit", "on", "un"].forEach(function(methodName) {
            pubSubInstance[methodName] = varArgs(function(eventName, parameters) {
                apply(parameters, pubSubInstance(eventName)[methodName]);
            });
        });
        return pubSubInstance;
    }
    var _S = 1,
        NODE_OPENED = _S++,
        NODE_CLOSED = _S++,
        NODE_SWAP = _S++,
        NODE_DROP = _S++,
        FAIL_EVENT = "fail",
        ROOT_NODE_FOUND = _S++,
        ROOT_PATH_FOUND = _S++,
        HTTP_START = "start",
        STREAM_DATA = "data",
        STREAM_END = "end",
        ABORTING = _S++,
        SAX_KEY = _S++,
        SAX_VALUE_OPEN = _S++,
        SAX_VALUE_CLOSE = _S++;

    function errorReport(statusCode, body, error) {
        try {
            var jsonBody = JSON.parse(body);
        } catch (e) {}
        return {
            statusCode: statusCode,
            body: body,
            jsonBody: jsonBody,
            thrown: error
        };
    }

    function patternAdapter(oboeBus, jsonPathCompiler) {
        var predicateEventMap = {
            node: oboeBus(NODE_CLOSED),
            path: oboeBus(NODE_OPENED)
        };

        function emitMatchingNode(emitMatch, node, ascent) {
            var descent = reverseList(ascent);
            emitMatch(node, listAsArray(tail(map(keyOf, descent))), listAsArray(map(nodeOf, descent)));
        }

        function addUnderlyingListener(fullEventName, predicateEvent, compiledJsonPath) {
            var emitMatch = oboeBus(fullEventName).emit;
            predicateEvent.on(function(ascent) {
                var maybeMatchingMapping = compiledJsonPath(ascent);
                if (maybeMatchingMapping !== false) {
                    emitMatchingNode(emitMatch, nodeOf(maybeMatchingMapping), ascent);
                }
            }, fullEventName);
            oboeBus("removeListener").on(function(removedEventName) {
                if (removedEventName == fullEventName) {
                    if (!oboeBus(removedEventName).listeners()) {
                        predicateEvent.un(fullEventName);
                    }
                }
            });
        }
        oboeBus("newListener").on(function(fullEventName) {
            var match = /(node|path):(.*)/.exec(fullEventName);
            if (match) {
                var predicateEvent = predicateEventMap[match[1]];
                if (!predicateEvent.hasListener(fullEventName)) {
                    addUnderlyingListener(fullEventName, predicateEvent, jsonPathCompiler(match[2]));
                }
            }
        });
    }

    function instanceApi(oboeBus, contentSource) {
        var oboeApi, fullyQualifiedNamePattern = /^(node|path):./,
            rootNodeFinishedEvent = oboeBus(ROOT_NODE_FOUND),
            emitNodeDrop = oboeBus(NODE_DROP).emit,
            emitNodeSwap = oboeBus(NODE_SWAP).emit,
            addListener = varArgs(function(eventId, parameters) {
                if (oboeApi[eventId]) {
                    apply(parameters, oboeApi[eventId]);
                } else {
                    var event = oboeBus(eventId),
                        listener = parameters[0];
                    if (fullyQualifiedNamePattern.test(eventId)) {
                        addForgettableCallback(event, listener);
                    } else {
                        event.on(listener);
                    }
                }
                return oboeApi;
            }),
            removeListener = function(eventId, p2, p3) {
                if (eventId == "done") {
                    rootNodeFinishedEvent.un(p2);
                } else if (eventId == "node" || eventId == "path") {
                    oboeBus.un(eventId + ":" + p2, p3);
                } else {
                    var listener = p2;
                    oboeBus(eventId).un(listener);
                }
                return oboeApi;
            };

        function addProtectedCallback(eventName, callback) {
            oboeBus(eventName).on(protectedCallback(callback), callback);
            return oboeApi;
        }

        function addForgettableCallback(event, callback, listenerId) {
            listenerId = listenerId || callback;
            var safeCallback = protectedCallback(callback);
            event.on(function() {
                var discard = false;
                oboeApi.forget = function() {
                    discard = true;
                };
                apply(arguments, safeCallback);
                delete oboeApi.forget;
                if (discard) {
                    event.un(listenerId);
                }
            }, listenerId);
            return oboeApi;
        }

        function protectedCallback(callback) {
            return function() {
                try {
                    return callback.apply(oboeApi, arguments);
                } catch (e) {
                    oboeBus(FAIL_EVENT).emit(errorReport(undefined, undefined, e));
                }
            };
        }

        function fullyQualifiedPatternMatchEvent(type, pattern) {
            return oboeBus(type + ":" + pattern);
        }

        function wrapCallbackToSwapNodeIfSomethingReturned(callback) {
            return function() {
                var returnValueFromCallback = callback.apply(this, arguments);
                if (defined(returnValueFromCallback)) {
                    if (returnValueFromCallback == oboe.drop) {
                        emitNodeDrop();
                    } else {
                        emitNodeSwap(returnValueFromCallback);
                    }
                }
            };
        }

        function addSingleNodeOrPathListener(eventId, pattern, callback) {
            var effectiveCallback;
            if (eventId == "node") {
                effectiveCallback = wrapCallbackToSwapNodeIfSomethingReturned(callback);
            } else {
                effectiveCallback = callback;
            }
            addForgettableCallback(fullyQualifiedPatternMatchEvent(eventId, pattern), effectiveCallback, callback);
        }

        function addMultipleNodeOrPathListeners(eventId, listenerMap) {
            for (var pattern in listenerMap) {
                addSingleNodeOrPathListener(eventId, pattern, listenerMap[pattern]);
            }
        }

        function addNodeOrPathListenerApi(eventId, jsonPathOrListenerMap, callback) {
            if (isString(jsonPathOrListenerMap)) {
                addSingleNodeOrPathListener(eventId, jsonPathOrListenerMap, callback);
            } else {
                addMultipleNodeOrPathListeners(eventId, jsonPathOrListenerMap);
            }
            return oboeApi;
        }
        oboeBus(ROOT_PATH_FOUND).on(function(rootNode) {
            oboeApi.root = functor(rootNode);
        });
        oboeBus(HTTP_START).on(function(_statusCode, headers) {
            oboeApi.header = function(name) {
                return name ? headers[name] : headers;
            };
        });
        return oboeApi = {
            on: addListener,
            addListener: addListener,
            removeListener: removeListener,
            emit: oboeBus.emit,
            node: partialComplete(addNodeOrPathListenerApi, "node"),
            path: partialComplete(addNodeOrPathListenerApi, "path"),
            done: partialComplete(addForgettableCallback, rootNodeFinishedEvent),
            start: partialComplete(addProtectedCallback, HTTP_START),
            fail: oboeBus(FAIL_EVENT).on,
            abort: oboeBus(ABORTING).emit,
            header: noop,
            root: noop,
            source: contentSource
        };
    }

    function wire(httpMethodName, contentSource, body, headers, withCredentials) {
        var oboeBus = pubSub();
        if (contentSource) {
            streamingHttp(oboeBus, httpTransport(), httpMethodName, contentSource, body, headers, withCredentials);
        }
        clarinet(oboeBus);
        ascentManager(oboeBus, incrementalContentBuilder(oboeBus));
        patternAdapter(oboeBus, jsonPathCompiler);
        return instanceApi(oboeBus, contentSource);
    }

    function applyDefaults(passthrough, url, httpMethodName, body, headers, withCredentials, cached) {
        headers = headers ? JSON.parse(JSON.stringify(headers)) : {};
        if (body) {
            if (!isString(body)) {
                body = JSON.stringify(body);
                headers["Content-Type"] = headers["Content-Type"] || "application/json";
            }
        } else {
            body = null;
        }

        function modifiedUrl(baseUrl, cached) {
            if (cached === false) {
                if (baseUrl.indexOf("?") == -1) {
                    baseUrl += "?";
                } else {
                    baseUrl += "&";
                }
                baseUrl += "_=" + new Date().getTime();
            }
            return baseUrl;
        }
        return passthrough(httpMethodName || "GET", modifiedUrl(url, cached), body, headers, withCredentials || false);
    }

    function oboe(arg1) {
        var nodeStreamMethodNames = list("resume", "pause", "pipe"),
            isStream = partialComplete(hasAllProperties, nodeStreamMethodNames);
        if (arg1) {
            if (isStream(arg1) || isString(arg1)) {
                return applyDefaults(wire, arg1);
            } else {
                return applyDefaults(wire, arg1.url, arg1.method, arg1.body, arg1.headers, arg1.withCredentials, arg1.cached);
            }
        } else {
            return wire();
        }
    }
    oboe.drop = function() {
        return oboe.drop;
    };
    if (typeof define === "function" && define.amd) {
        define("oboe", [], function() {
            return oboe;
        });
    } else if (typeof exports === "object") {
        module.exports = oboe;
    } else {
        window.oboe = oboe;
    }
})(function() {
    try {
        return window;
    } catch (e) {
        return self;
    }
}(), Object, Array, Error, JSON);

define("modules/controllers/chromecast_sender", ["modules/definitions/standardmodule", "underscore", "oboe"], function(parentModel, _, oboe) {
    function _thizOBJ_(o) {
        var defaults = {
            scope: true,
            type: "chromecast_sender",
            author: "Jonathan Robles",
            lasteditby: ""
        };
        defaults = this.deepExtend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {};
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._var({});
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        var parent = this;
        parent.notify("Trace", "refresh");
        window.CastPlayer.initializeCastPlayer();
        window.CastPlayer.initializeLocalPlayer();
        window.getStatus = function() {
            CastPlayer.session.sendMessage(AETN.namespace, {
                command: "sessionStatus"
            });
            trace('CastPlayer.session.sendMessage("' + AETN.namespace + '",{"command":"sessionStatus"});');
        };
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype._internalmethod = function() {
        var parent = this;
        parent.notify("Trace", "_internalmethod");
    };
    return _thizOBJ_;
});

require(["modules/controllers/chromecast_sender"], function(ccSender) {
    var mySender = new ccSender({});
    mySender.init();
});

define("instances/chromecast_sender_JS.js", function() {});
