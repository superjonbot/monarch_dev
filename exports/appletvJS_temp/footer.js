/*
 Copyright 2011-2013 Abdulla Abdurakhmanov
 Original sources are available at https://code.google.com/p/x2js/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

function X2JS(config) {
	'use strict';
		
	var VERSION = "1.1.3";
	
	config = config || {};
	initConfigDefaults();
	
	function initConfigDefaults() {
		if(config.escapeMode === undefined) {
			config.escapeMode = true;
		}
		config.attributePrefix = config.attributePrefix || "_";
		config.arrayAccessForm = config.arrayAccessForm || "none";
		config.emptyNodeForm = config.emptyNodeForm || "text";
		if(config.enableToStringFunc === undefined) {
			config.enableToStringFunc = true; 
		}
		config.arrayAccessFormPaths = config.arrayAccessFormPaths || []; 
		if(config.skipEmptyTextNodesForObj === undefined) {
			config.skipEmptyTextNodesForObj = true;
		}
	}

	var DOMNodeTypes = {
		ELEMENT_NODE 	   : 1,
		TEXT_NODE    	   : 3,
		CDATA_SECTION_NODE : 4,
		COMMENT_NODE	   : 8,
		DOCUMENT_NODE 	   : 9
	};
	
	function getNodeLocalName( node ) {
		var nodeLocalName = node.localName;			
		if(nodeLocalName == null) // Yeah, this is IE!! 
			nodeLocalName = node.baseName;
		if(nodeLocalName == null || nodeLocalName=="") // =="" is IE too
			nodeLocalName = node.nodeName;
		return nodeLocalName;
	}
	
	function getNodePrefix(node) {
		return node.prefix;
	}
		
	function escapeXmlChars(str) {
		if(typeof(str) == "string")
			return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#x27;').replace(/\//g, '&#x2F;');
		else
			return str;
	}

	function unescapeXmlChars(str) {
		return str.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&#x27;/g, "'").replace(/&#x2F;/g, '\/');
	}
	
	function toArrayAccessForm(obj, childName, path) {
		switch(config.arrayAccessForm) {
		case "property":
			if(!(obj[childName] instanceof Array))
				obj[childName+"_asArray"] = [obj[childName]];
			else
				obj[childName+"_asArray"] = obj[childName];
			break;		
		/*case "none":
			break;*/
		}
		
		if(!(obj[childName] instanceof Array) && config.arrayAccessFormPaths.length > 0) {
			var idx = 0;
			for(; idx < config.arrayAccessFormPaths.length; idx++) {
				var arrayPath = config.arrayAccessFormPaths[idx];
				if( typeof arrayPath === "string" ) {
					if(arrayPath == path)
						break;
				}
				else
				if( arrayPath instanceof RegExp) {
					if(arrayPath.test(path))
						break;
				}				
				else
				if( typeof arrayPath === "function") {
					if(arrayPath(obj, childName, path))
						break;
				}
			}
			if(idx!=config.arrayAccessFormPaths.length) {
				obj[childName] = [obj[childName]];
			}
		}
	}

	function parseDOMChildren( node, path ) {
		if(node.nodeType == DOMNodeTypes.DOCUMENT_NODE) {
			var result = new Object;
			var nodeChildren = node.childNodes;
			// Alternative for firstElementChild which is not supported in some environments
			for(var cidx=0; cidx <nodeChildren.length; cidx++) {
				var child = nodeChildren.item(cidx);
				if(child.nodeType == DOMNodeTypes.ELEMENT_NODE) {
					var childName = getNodeLocalName(child);
					result[childName] = parseDOMChildren(child, childName);
				}
			}
			return result;
		}
		else
		if(node.nodeType == DOMNodeTypes.ELEMENT_NODE) {
			var result = new Object;
			result.__cnt=0;
			
			var nodeChildren = node.childNodes;
			
			// Children nodes
			for(var cidx=0; cidx <nodeChildren.length; cidx++) {
				var child = nodeChildren.item(cidx); // nodeChildren[cidx];
				var childName = getNodeLocalName(child);
				
				if(child.nodeType!= DOMNodeTypes.COMMENT_NODE) {
					result.__cnt++;
					if(result[childName] == null) {
						result[childName] = parseDOMChildren(child, path+"."+childName);
						toArrayAccessForm(result, childName, path+"."+childName);					
					}
					else {
						if(result[childName] != null) {
							if( !(result[childName] instanceof Array)) {
								result[childName] = [result[childName]];
								toArrayAccessForm(result, childName, path+"."+childName);
							}
						}
						(result[childName])[result[childName].length] = parseDOMChildren(child, path+"."+childName);
					}
				}								
			}
			
			// Attributes
			for(var aidx=0; aidx <node.attributes.length; aidx++) {
				var attr = node.attributes.item(aidx); // [aidx];
				result.__cnt++;
				result[config.attributePrefix+attr.name]=attr.value;
			}
			
			// Node namespace prefix
			var nodePrefix = getNodePrefix(node);
			if(nodePrefix!=null && nodePrefix!="") {
				result.__cnt++;
				result.__prefix=nodePrefix;
			}
			
			if(result["#text"]!=null) {				
				result.__text = result["#text"];
				if(result.__text instanceof Array) {
					result.__text = result.__text.join("\n");
				}
				if(config.escapeMode)
					result.__text = unescapeXmlChars(result.__text);
				delete result["#text"];
				if(config.arrayAccessForm=="property")
					delete result["#text_asArray"];
			}
			if(result["#cdata-section"]!=null) {
				result.__cdata = result["#cdata-section"];
				delete result["#cdata-section"];
				if(config.arrayAccessForm=="property")
					delete result["#cdata-section_asArray"];
			}
			
			if( result.__cnt == 1 && result.__text!=null  ) {
				result = result.__text;
			}
			else
			if( result.__cnt == 0 && config.emptyNodeForm=="text" ) {
				result = '';
			}
			else
			if ( result.__cnt > 1 && result.__text!=null && config.skipEmptyTextNodesForObj) {
				if(result.__text.trim()=="") {
					delete result.__text;
				}
			}
			delete result.__cnt;			
			
			if( config.enableToStringFunc && result.__text!=null || result.__cdata!=null ) {
				result.toString = function() {
					return (this.__text!=null? this.__text:'')+( this.__cdata!=null ? this.__cdata:'');
				};
			}
			return result;
		}
		else
		if(node.nodeType == DOMNodeTypes.TEXT_NODE || node.nodeType == DOMNodeTypes.CDATA_SECTION_NODE) {
			return node.nodeValue;
		}	
	}
	
	function startTag(jsonObj, element, attrList, closed) {
		var resultStr = "<"+ ( (jsonObj!=null && jsonObj.__prefix!=null)? (jsonObj.__prefix+":"):"") + element;
		if(attrList!=null) {
			for(var aidx = 0; aidx < attrList.length; aidx++) {
				var attrName = attrList[aidx];
				var attrVal = jsonObj[attrName];
				resultStr+=" "+attrName.substr(config.attributePrefix.length)+"='"+attrVal+"'";
			}
		}
		if(!closed)
			resultStr+=">";
		else
			resultStr+="/>";
		return resultStr;
	}
	
	function endTag(jsonObj,elementName) {
		return "</"+ (jsonObj.__prefix!=null? (jsonObj.__prefix+":"):"")+elementName+">";
	}
	
	function endsWith(str, suffix) {
	    return str.indexOf(suffix, str.length - suffix.length) !== -1;
	}
	
	function jsonXmlSpecialElem ( jsonObj, jsonObjField ) {
		if((config.arrayAccessForm=="property" && endsWith(jsonObjField.toString(),("_asArray"))) 
				|| jsonObjField.toString().indexOf(config.attributePrefix)==0 
				|| jsonObjField.toString().indexOf("__")==0
				|| (jsonObj[jsonObjField] instanceof Function) )
			return true;
		else
			return false;
	}
	
	function jsonXmlElemCount ( jsonObj ) {
		var elementsCnt = 0;
		if(jsonObj instanceof Object ) {
			for( var it in jsonObj  ) {
				if(jsonXmlSpecialElem ( jsonObj, it) )
					continue;			
				elementsCnt++;
			}
		}
		return elementsCnt;
	}
	
	function parseJSONAttributes ( jsonObj ) {
		var attrList = [];
		if(jsonObj instanceof Object ) {
			for( var ait in jsonObj  ) {
				if(ait.toString().indexOf("__")== -1 && ait.toString().indexOf(config.attributePrefix)==0) {
					attrList.push(ait);
				}
			}
		}
		return attrList;
	}
	
	function parseJSONTextAttrs ( jsonTxtObj ) {
		var result ="";
		
		if(jsonTxtObj.__cdata!=null) {										
			result+="<![CDATA["+jsonTxtObj.__cdata+"]]>";					
		}
		
		if(jsonTxtObj.__text!=null) {			
			if(config.escapeMode)
				result+=escapeXmlChars(jsonTxtObj.__text);
			else
				result+=jsonTxtObj.__text;
		}
		return result;
	}
	
	function parseJSONTextObject ( jsonTxtObj ) {
		var result ="";

		if( jsonTxtObj instanceof Object ) {
			result+=parseJSONTextAttrs ( jsonTxtObj );
		}
		else
			if(jsonTxtObj!=null) {
				if(config.escapeMode)
					result+=escapeXmlChars(jsonTxtObj);
				else
					result+=jsonTxtObj;
			}
		
		return result;
	}
	
	function parseJSONArray ( jsonArrRoot, jsonArrObj, attrList ) {
		var result = ""; 
		if(jsonArrRoot.length == 0) {
			result+=startTag(jsonArrRoot, jsonArrObj, attrList, true);
		}
		else {
			for(var arIdx = 0; arIdx < jsonArrRoot.length; arIdx++) {
				result+=startTag(jsonArrRoot[arIdx], jsonArrObj, parseJSONAttributes(jsonArrRoot[arIdx]), false);
				result+=parseJSONObject(jsonArrRoot[arIdx]);
				result+=endTag(jsonArrRoot[arIdx],jsonArrObj);						
			}
		}
		return result;
	}
	
	function parseJSONObject ( jsonObj ) {
		var result = "";	

		var elementsCnt = jsonXmlElemCount ( jsonObj );
		
		if(elementsCnt > 0) {
			for( var it in jsonObj ) {
				
				if(jsonXmlSpecialElem ( jsonObj, it) )
					continue;			
				
				var subObj = jsonObj[it];						
				
				var attrList = parseJSONAttributes( subObj )
				
				if(subObj == null || subObj == undefined) {
					result+=startTag(subObj, it, attrList, true);
				}
				else
				if(subObj instanceof Object) {
					
					if(subObj instanceof Array) {					
						result+=parseJSONArray( subObj, it, attrList );					
					}
					else {
						var subObjElementsCnt = jsonXmlElemCount ( subObj );
						if(subObjElementsCnt > 0 || subObj.__text!=null || subObj.__cdata!=null) {
							result+=startTag(subObj, it, attrList, false);
							result+=parseJSONObject(subObj);
							result+=endTag(subObj,it);
						}
						else {
							result+=startTag(subObj, it, attrList, true);
						}
					}
				}
				else {
					result+=startTag(subObj, it, attrList, false);
					result+=parseJSONTextObject(subObj);
					result+=endTag(subObj,it);
				}
			}
		}
		result+=parseJSONTextObject(jsonObj);
		
		return result;
	}
	
	this.parseXmlString = function(xmlDocStr) {
		if (xmlDocStr === undefined) {
			return null;
		}
		var xmlDoc;
		if (window.DOMParser) {
			var parser=new window.DOMParser();			
			xmlDoc = parser.parseFromString( xmlDocStr, "text/xml" );
		}
		else {
			// IE :(
			if(xmlDocStr.indexOf("<?")==0) {
				xmlDocStr = xmlDocStr.substr( xmlDocStr.indexOf("?>") + 2 );
			}
			xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async="false";
			xmlDoc.loadXML(xmlDocStr);
		}
		return xmlDoc;
	};
	
	this.asArray = function(prop) {
		if(prop instanceof Array)
			return prop;
		else
			return [prop];
	}

	this.xml2json = function (xmlDoc) {
		return parseDOMChildren ( xmlDoc );
	};
	
	this.xml_str2json = function (xmlDocStr) {
		var xmlDoc = this.parseXmlString(xmlDocStr);	
		return this.xml2json(xmlDoc);
	};

	this.json2xml_str = function (jsonObj) {
		return parseJSONObject ( jsonObj );
	};

	this.json2xml = function (jsonObj) {
		var xmlDocStr = this.json2xml_str (jsonObj);
		return this.parseXmlString(xmlDocStr);
	};
	
	this.getVersion = function () {
		return VERSION;
	};
	
}
;
define("x2js", (function (global) {
    return function () {
        var ret, fn;
        return ret || global.X2JS;
    };
}(this)));

/**
 * Standard Module Definition
 * Author: Jonathan Robles
 *
 * Date: <inZertDATe>
 *
 *  NOTE: UNDER ANY CIRCUMSTANCES, DON'T SCREW WITH THIS MODULE!!!
 */
define('modules/definitions/standardmodule',[],function () {

    var _instanceID = 0; //instance id
    var _nextInstanceID = function(){ return( ++_instanceID );  };//instance adder
    var defaults =[]; //instance array
    var deepExtend = function(destination, source){
        for (var property in source) {
            if (source[property] && source[property].constructor &&
                source[property].constructor === Object) {
                destination[property] = destination[property] || {};
                arguments.callee(destination[property], source[property]);
            } else {
                destination[property] = source[property];
            }
        }
        return destination;
    };

    function _thizOBJ_(o){
        this._instanceID = _nextInstanceID(); //who am I?
        var _parent=this;
        //INSTANCE VARIABLES
        defaults[this._instanceID]={
            type:'Standard Module Definition',
            author:'Jonathan Robles',
            notifyscope:'global',

            target:undefined,
            file:undefined,
            usenocache:true,
            data:undefined,
            callback:undefined,
            interval:undefined,


            init:function(){
                _notify.broadcast('Initialize', [{
                    senderID:_parent._instanceID,
                    sendertype:this.type,
                    notifyscope:this.notifyscope,
                    data:{
                        author:this.author
                    }
                }]);
            },
            parent:this
        };
        defaults[this._instanceID]=deepExtend(defaults[this._instanceID],o);
        defaults[this._instanceID].init();
        return( this );

    }

    // BASE METHODS
    _thizOBJ_.prototype = {
        _init:function(){this._var().init();}, //run internal initialization
        _showdata: function(){
            return JSON.stringify(defaults[this._instanceID]);
        }, //show data as a string
        _id: function(){ return( this._instanceID );}, //get instanceID
        _var:function(o){
            if(o!=undefined){defaults[this._instanceID]= deepExtend(defaults[this._instanceID],o)};
            return defaults[this._instanceID];
        },  //get defaults variable
        _nocache:function(string){
            if(typeof string==='string'){
            if(this._var().usenocache){
                var addOn='?';
                if(string.indexOf('?')!=-1){addOn='&'}
                return string+addOn+"nocache=" + (Math.floor(Math.random() * 9999));
            } else {

                return string;
            }
        }     else {this.notify('Alert','_nocache needs a string!');return;}
        }, //tack on nocacheifneeded
        notify:function(type,data){
            _notify.broadcast(type, [{
                senderID:this._id(),
                sendertype:this._var().type,
                notifyscope:this._var().notifyscope,
                data:data
            }]);
        },
        deepExtend:function(destination, source){
            return deepExtend(destination, source)
        },
        parent:this

    };
    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});

/*

function deepExtend(destination, source) {
    for (var property in source) {
        if (source[property] && source[property].constructor &&
            source[property].constructor === Object) {
            destination[property] = destination[property] || {};
            arguments.callee(destination[property], source[property]);
        } else {
            destination[property] = source[property];
        }
    }
    return destination;
};
*/

;
/*!
 * dataparser RequireJS Module
 * by Jonathan Robles
 * Date: 10/12/13[7:46 PM]
 */
/** NOTE: Its a json/jsonp/xml parsing module.
 *
 * USAGE:
 * var yourobject = new Module({
 *      file:'data/sitedata.json',
 *      format:'json',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * OR
 *
 * var yourobject = new Module({
 *      file:'data/sitedata.jsonp',
 *      format:'jsonp',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * OR
 *
 * var yourobject = new Module({
 *      file:'data/sitedata.xml',
 *      format:'text',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * yourobject.init();
 */
define('modules/parsers/dataparser_native',['x2js','modules/definitions/standardmodule'],function (X2JS,parentModel) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_( o ){
        var defaults={
            type:'dataparser', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            lasteditby:'',  //your name if you are making the last edit (NOTE:all edits in the code must have comments with your initials!)

            file:'data/sitedata.json',
            usenocache:true,

            dataXML:undefined,

            data:undefined,  //placeholder for final data
            callback:undefined,//function(data){alert('received? '+data.status)},
            jsonpReturn:"window._global$.jsonpReturn('<%id>','JSONdata','global')",
            //interval:undefined,

            format:'json', //use text for xml, json for json, jsonp for jsonp
            //uselegacyinject:false,
            busy:false

        };
        defaults= this.deepExtend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners =function(){
        this.notify('Trace','_startlisteners');
        var parent=this;
        var myID=  this._id();
        _notify.add(this._id(), function() {

            return {

                onJSONdata:function(o){
                    if(o.senderID==myID){
                        parent._var({data: o.data,busy:false});
                        if(parent._var().callback!=undefined){
                            parent._var().callback(o.data)
                        }
                    }
                }
            }
        }());

    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init =function(){

        this.notify('Trace','init');
        this._startlisteners();//start this module's listeners
        this.refresh();

    };

    _thizOBJ_.prototype.refresh =function(){
        this.notify('Trace','refresh');
        this.getdata();
    };

    _thizOBJ_.prototype.kill =function(){
        this.notify('Trace','kill');
        _notify.rem(this._id());  //kills this module's listeners
    };



    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.getdata= function(){
        var parent=this;
        parent._var({busy:true});
        this.notify('Trace','getdata format:'+parent._var().format+' file'+parent._var().file);
        var JSONtoUSE=parent._nocache(parent._var().file);
        var onData=function(jsondata){
            parent.notify('JSONdata',jsondata);
        };


        if(this._var().format=='text'){
/*

            $.ajax({

                xhrFields: {
                    withCredentials: false
                },
                crossDomain: false,

                type: "GET",
                url: parent._var().file,
                dataType: parent._var().format,
                statusCode: {404: function() {alert('please check xml!');}},



                success: function(xml) {

                    parent._var({dataXML:xml});
                    var gparse = new X2JS(); //XML object parsing
                    var jsondata = gparse.parseXmlString(xml);
                    jsondata = gparse.xml2json( jsondata );

                    onData(jsondata);

                }
            });
*/


//modules/view/viewmodels
        }
        else if(this._var().format=='jsonp'){


            //  if(!this._var().uselegacyinject){

            if(this._var().jsonpReturn!=undefined){

                JSONtoUSE += "&jsonp="+this._var().jsonpReturn;
                JSONtoUSE = JSONtoUSE.replace('<%id>',this._id()); //tack on ID to jsonP return
            }

            JSONtoUSE += "&=?";
            //console.log('trying to get '+this._var().format+' file:'+JSONtoUSE);

/*

            $.ajaxSetup({
                type: "POST",
                data: {},
                //dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true
            });
            $.getJSON(JSONtoUSE, {format: 'jsonp'}).error(function(){
                // console.log("JSONP READ COMPLETE! (note, object data/busy are not used for jsonp files!)");
                // console.log('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=');
            })//,function(jsondata) {onjsondata(jsondata);}).error(function() { trace("ERROR!");trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=') });

*/


        }
        else {


/*
            $.getJSON(JSONtoUSE, {format: 'json'},function(jsondata){
                onData(jsondata)
            }).error(function() {
                //error messages here
                //trace("ERROR!");
                //trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
            });
            */

        }
    };



    return( _thizOBJ_ );

});




/**
 * appletvJSdemo by Jonathan Robles
 *  
 */

 require(['modules/parsers/dataparser_native'],
    function ( jsonParser) {


/*        var deepextendTEST= new jsonParser();

        var testingobj = {
            one: 'one1xt',
            two: 'two',
            three: {aye: 0, bee: 2}
        }
        var testingobjB = {
            one: 'boneabc',
            two: 'bthrr',
            three: {aye: 99, testt: 9}
        }

        var testingvar = deepExtend(testingobj,testingobjB);
        var testingvarx = deepextendTEST.deepExtend(testingobj,testingobjB);

        alert(JSON.stringify(testingvar)+' or '+JSON.stringify(testingvarx));
        alert(deepextendTEST._var().type);

        trace('wah!');*/


        //atv._debugDumpControllerStack();
        ////var proxy = new atv.ProxyDocument();
       // var xmlDoc = atv.parseXML('<?xml version="1.0" encoding="UTF-8"?><atv><body><dialog id="com.sample.error-dialog"><title>Crunk</title><description>The feed for this page has not been written.</description></dialog></body></atv>');
       // atv.loadXML(xmlDoc);
        //proxy.show();





/*
        deepExtend(atv, {
            config : {
                doesJavaScriptLoadRoot: true
            },

            onGenerateRequest:function(request){},

            onAppEntry : function () {
                alert('onAppEntry')
                //aetnUtils.initialAuthCheck();
                //aetnUtils.initialWatchlistCheck();
                //mDialog.ATVPlayer.init(aetn.globalContext.mdialogOptions);

                //var xmlDoc = atv.parseXML('<?xml version="1.0" encoding="UTF-8"?><atv><body><dialog id="com.sample.error-dialog"><title>Crunkbot</title><description>The feed for this page has not been written.</description></dialog></body></atv>');
                //atv.loadXML(xmlDoc);

            },
            onAppExit : function () {
                //atv.localStorage.removeItem('mvpd');
            },

            onPageLoad:function(id){},
            onPageUnload:function(id){},
            onPageBuried:function(id){},
            onPageExhumed:function(id){},
            onRefresh:function(){},
            onNavigate:function(event){},
            onVolatileReload:function(document){},
            onItemFocused:function(indexPath){},

            provideTopShelfItems : function(callback) {
            },
            onOpenURL : function(options) {
            }




        });
        */

       // alert('wah!');
        //trace(JSON.stringify(atv))
        atv.onAppEntry = function(){

            var xmlDoc = atv.parseXML('<?xml version="1.0" encoding="UTF-8"?><atv><body><dialog id="com.sample.error-dialog"><title>BBB</title><description>The feed for this page has not been written.</description></dialog></body></atv>');
            atv.loadXML(xmlDoc);
        }

    });


atv.onAppEntry = function(){

    var xmlDoc = atv.parseXML('<?xml version="1.0" encoding="UTF-8"?><atv><body><dialog id="com.sample.error-dialog"><title>AAA</title><description>The feed for this page has not been written.</description></dialog></body></atv>');
    atv.loadXML(xmlDoc);
}


/*


<atv>
<head>
<script src="http://dev.appletv.aetndigital.com/js/build/compiled/aetn.appletv.main.fyi.dev.js" />
    </head>
    <body>
    <viewWithNavigationBar id="com.aetn.navigation-bar" volatile="true" onNavigate="         var e = event;         require(['aetn/appletv/ui/navbar'], function(Navbar){           var navbar = new Navbar();           navbar.handleNavigate(e, 'com.aetn.navigation-bar');         });" onVolatileReload="         var e = event;         require(['aetn/appletv/ui/navbar'], function(Navbar){           var navbar = new Navbar();           navbar.handleVolatileReload(e, document, 'com.aetn.navigation-bar');         });">
    <navigation id="nav" currentIndex="0">
    <navigationItem id="fyi" accessibilityLabel="F.Y.I">
    <title>FYI</title>
    <url>http://dev.appletv.aetndigital.com/fyi/section/shows</url>
</navigationItem>
<navigationItem id="watchlist">
    <title>Watchlist</title>
    <url>http://dev.appletv.aetndigital.com/fyi/watchlist</url>
</navigationItem>
<navigationItem id="search">
    <title>Search</title>
    <url>http://dev.appletv.aetndigital.com/fyi/search</url>
</navigationItem>
<navigationItem id="settings">
    <title>Settings</title>
    <url>http://dev.appletv.aetndigital.com/fyi/settings/index</url>
</navigationItem>
</navigation>
</viewWithNavigationBar>
</body>
</atv>

    */
;
define("instances/appletvJS.js", function(){});

