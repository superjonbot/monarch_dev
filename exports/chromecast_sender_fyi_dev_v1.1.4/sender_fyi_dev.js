/*! chromecast_sender_fyi_dev : sender_fyi_dev.js */
/*! codebase: CB2016 v1.1.4 by Jonathan Robles */
/*! built:05-03-2016 [12:54:09PM] */
/*! -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

/*! Prerequisites: //www.gstatic.com/cv/js/sender/v1/cast_sender.js */

/*! Third Party Includes [start] */
/* almond, modernizr, underscore, oboe */
/*! Third Party Includes [end] */
var requirejs, require, define;

(function(undef) {
    var main, req, makeMap, handlers, defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex, foundI, foundStarMap, starI, i, j, part, baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = map && map["*"] || {};
        if (name && name.charAt(0) === ".") {
            if (baseName) {
                baseParts = baseParts.slice(0, baseParts.length - 1);
                name = name.split("/");
                lastIndex = name.length - 1;
                if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                    name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, "");
                }
                name = baseParts.concat(name);
                for (i = 0; i < name.length; i += 1) {
                    part = name[i];
                    if (part === ".") {
                        name.splice(i, 1);
                        i -= 1;
                    } else if (part === "..") {
                        if (i === 1 && (name[2] === ".." || name[0] === "..")) {
                            break;
                        } else if (i > 0) {
                            name.splice(i - 1, 2);
                            i -= 2;
                        }
                    }
                }
                name = name.join("/");
            } else if (name.indexOf("./") === 0) {
                name = name.substring(2);
            }
        }
        if ((baseParts || starMap) && map) {
            nameParts = name.split("/");
            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");
                if (baseParts) {
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join("/")];
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }
                if (foundMap) {
                    break;
                }
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }
            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }
            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join("/");
            }
        }
        return name;
    }

    function makeRequire(relName, forceSync) {
        return function() {
            var args = aps.call(arguments, 0);
            if (typeof args[0] !== "string" && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function(name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function(value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }
        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error("No " + name);
        }
        return defined[name];
    }

    function splitPrefix(name) {
        var prefix, index = name ? name.indexOf("!") : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }
    makeMap = function(name, relName) {
        var plugin, parts = splitPrefix(name),
            prefix = parts[0];
        name = parts[1];
        if (prefix) {
            prefix = normalize(prefix, relName);
            plugin = callDep(prefix);
        }
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relName));
            } else {
                name = normalize(name, relName);
            }
        } else {
            name = normalize(name, relName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }
        return {
            f: prefix ? prefix + "!" + name : name,
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function() {
            return config && config.config && config.config[name] || {};
        };
    }
    handlers = {
        require: function(name) {
            return makeRequire(name);
        },
        exports: function(name) {
            var e = defined[name];
            if (typeof e !== "undefined") {
                return e;
            } else {
                return defined[name] = {};
            }
        },
        module: function(name) {
            return {
                id: name,
                uri: "",
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };
    main = function(name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i, args = [],
            callbackType = typeof callback,
            usingExports;
        relName = relName || name;
        if (callbackType === "undefined" || callbackType === "function") {
            deps = !deps.length && callback.length ? ["require", "exports", "module"] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relName);
                depName = map.f;
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) || hasProp(waiting, depName) || hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + " missing " + depName);
                }
            }
            ret = callback ? callback.apply(defined[name], args) : undefined;
            if (name) {
                if (cjsModule && cjsModule.exports !== undef && cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    defined[name] = ret;
                }
            }
        } else if (name) {
            defined[name] = callback;
        }
    };
    requirejs = require = req = function(deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                return handlers[deps](callback);
            }
            return callDep(makeMap(deps, callback).f);
        } else if (!deps.splice) {
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }
            if (callback.splice) {
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }
        callback = callback || function() {};
        if (typeof relName === "function") {
            relName = forceSync;
            forceSync = alt;
        }
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            setTimeout(function() {
                main(undef, deps, callback, relName);
            }, 4);
        }
        return req;
    };
    req.config = function(cfg) {
        return req(cfg);
    };
    requirejs._defined = defined;
    define = function(name, deps, callback) {
        if (!deps.splice) {
            callback = deps;
            deps = [];
        }
        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };
    define.amd = {
        jQuery: true
    };
})();

define("almondLib", function() {});

window.Modernizr = function(a, b, c) {
        function B(a) {
            j.cssText = a;
        }

        function C(a, b) {
            return B(m.join(a + ";") + (b || ""));
        }

        function D(a, b) {
            return typeof a === b;
        }

        function E(a, b) {
            return !!~("" + a).indexOf(b);
        }

        function F(a, b) {
            for (var d in a) {
                var e = a[d];
                if (!E(e, "-") && j[e] !== c) return b == "pfx" ? e : !0;
            }
            return !1;
        }

        function G(a, b, d) {
            for (var e in a) {
                var f = b[a[e]];
                if (f !== c) return d === !1 ? a[e] : D(f, "function") ? f.bind(d || b) : f;
            }
            return !1;
        }

        function H(a, b, c) {
            var d = a.charAt(0).toUpperCase() + a.slice(1),
                e = (a + " " + o.join(d + " ") + d).split(" ");
            return D(b, "string") || D(b, "undefined") ? F(e, b) : (e = (a + " " + p.join(d + " ") + d).split(" "),
                G(e, b, c));
        }
        var d = "2.7.1",
            e = {},
            f = !0,
            g = b.documentElement,
            h = "modernizr",
            i = b.createElement(h),
            j = i.style,
            k, l = {}.toString,
            m = " -webkit- -moz- -o- -ms- ".split(" "),
            n = "Webkit Moz O ms",
            o = n.split(" "),
            p = n.toLowerCase().split(" "),
            q = {
                svg: "http://www.w3.org/2000/svg"
            },
            r = {},
            s = {},
            t = {},
            u = [],
            v = u.slice,
            w, x = function(a, c, d, e) {
                var f, i, j, k, l = b.createElement("div"),
                    m = b.body,
                    n = m || b.createElement("body");
                if (parseInt(d, 10))
                    while (d--) j = b.createElement("div"), j.id = e ? e[d] : h + (d + 1),
                        l.appendChild(j);
                return f = ["&#173;", '<style id="s', h, '">', a, "</style>"].join(""), l.id = h, (m ? l : n).innerHTML += f, n.appendChild(l), m || (n.style.background = "", n.style.overflow = "hidden",
                        k = g.style.overflow, g.style.overflow = "hidden", g.appendChild(n)), i = c(l, a),
                    m ? l.parentNode.removeChild(l) : (n.parentNode.removeChild(n), g.style.overflow = k), !!i;
            },
            y = function(b) {
                var c = a.matchMedia || a.msMatchMedia;
                if (c) return c(b).matches;
                var d;
                return x("@media " + b + " { #" + h + " { position: absolute; } }", function(b) {
                    d = (a.getComputedStyle ? getComputedStyle(b, null) : b.currentStyle)["position"] == "absolute";
                }), d;
            },
            z = {}.hasOwnProperty,
            A;
        !D(z, "undefined") && !D(z.call, "undefined") ? A = function(a, b) {
            return z.call(a, b);
        } : A = function(a, b) {
            return b in a && D(a.constructor.prototype[b], "undefined");
        }, Function.prototype.bind || (Function.prototype.bind = function(b) {
            var c = this;
            if (typeof c != "function") throw new TypeError();
            var d = v.call(arguments, 1),
                e = function() {
                    if (this instanceof e) {
                        var a = function() {};
                        a.prototype = c.prototype;
                        var f = new a(),
                            g = c.apply(f, d.concat(v.call(arguments)));
                        return Object(g) === g ? g : f;
                    }
                    return c.apply(b, d.concat(v.call(arguments)));
                };
            return e;
        }), r.canvas = function() {
            var a = b.createElement("canvas");
            return !!a.getContext && !!a.getContext("2d");
        }, r.touch = function() {
            var c;
            return "ontouchstart" in a || a.DocumentTouch && b instanceof DocumentTouch ? c = !0 : x(["@media (", m.join("touch-enabled),("), h, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function(a) {
                c = a.offsetTop === 9;
            }), c;
        }, r.geolocation = function() {
            return "geolocation" in navigator;
        }, r.cssanimations = function() {
            return H("animationName");
        }, r.csstransforms = function() {
            return !!H("transform");
        }, r.csstransforms3d = function() {
            var a = !!H("perspective");
            return a && "webkitPerspective" in g.style && x("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(b, c) {
                a = b.offsetLeft === 9 && b.offsetHeight === 3;
            }), a;
        }, r.csstransitions = function() {
            return H("transition");
        }, r.fontface = function() {
            var a;
            return x('@font-face {font-family:"font";src:url("https://")}', function(c, d) {
                var e = b.getElementById("smodernizr"),
                    f = e.sheet || e.styleSheet,
                    g = f ? f.cssRules && f.cssRules[0] ? f.cssRules[0].cssText : f.cssText || "" : "";
                a = /src/i.test(g) && g.indexOf(d.split(" ")[0]) === 0;
            }), a;
        }, r.video = function() {
            var a = b.createElement("video"),
                c = !1;
            try {
                if (c = !!a.canPlayType) c = new Boolean(c), c.ogg = a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""),
                    c.h264 = a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), c.webm = a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, "");
            } catch (d) {}
            return c;
        }, r.audio = function() {
            var a = b.createElement("audio"),
                c = !1;
            try {
                if (c = !!a.canPlayType) c = new Boolean(c), c.ogg = a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""),
                    c.mp3 = a.canPlayType("audio/mpeg;").replace(/^no$/, ""), c.wav = a.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""),
                    c.m4a = (a.canPlayType("audio/x-m4a;") || a.canPlayType("audio/aac;")).replace(/^no$/, "");
            } catch (d) {}
            return c;
        }, r.webworkers = function() {
            return !!a.Worker;
        }, r.svg = function() {
            return !!b.createElementNS && !!b.createElementNS(q.svg, "svg").createSVGRect;
        }, r.inlinesvg = function() {
            var a = b.createElement("div");
            return a.innerHTML = "<svg/>", (a.firstChild && a.firstChild.namespaceURI) == q.svg;
        }, r.svgclippaths = function() {
            return !!b.createElementNS && /SVGClipPath/.test(l.call(b.createElementNS(q.svg, "clipPath")));
        };
        for (var I in r) A(r, I) && (w = I.toLowerCase(), e[w] = r[I](), u.push((e[w] ? "" : "no-") + w));
        return e.addTest = function(a, b) {
                if (typeof a == "object")
                    for (var d in a) A(a, d) && e.addTest(d, a[d]);
                else {
                    a = a.toLowerCase();
                    if (e[a] !== c) return e;
                    b = typeof b == "function" ? b() : b, typeof f != "undefined" && f && (g.className += " " + (b ? "" : "no-") + a),
                        e[a] = b;
                }
                return e;
            }, B(""), i = k = null,
            function(a, b) {
                function l(a, b) {
                    var c = a.createElement("p"),
                        d = a.getElementsByTagName("head")[0] || a.documentElement;
                    return c.innerHTML = "x<style>" + b + "</style>", d.insertBefore(c.lastChild, d.firstChild);
                }

                function m() {
                    var a = s.elements;
                    return typeof a == "string" ? a.split(" ") : a;
                }

                function n(a) {
                    var b = j[a[h]];
                    return b || (b = {}, i++, a[h] = i, j[i] = b), b;
                }

                function o(a, c, d) {
                    c || (c = b);
                    if (k) return c.createElement(a);
                    d || (d = n(c));
                    var g;
                    return d.cache[a] ? g = d.cache[a].cloneNode() : f.test(a) ? g = (d.cache[a] = d.createElem(a)).cloneNode() : g = d.createElem(a),
                        g.canHaveChildren && !e.test(a) && !g.tagUrn ? d.frag.appendChild(g) : g;
                }

                function p(a, c) {
                    a || (a = b);
                    if (k) return a.createDocumentFragment();
                    c = c || n(a);
                    var d = c.frag.cloneNode(),
                        e = 0,
                        f = m(),
                        g = f.length;
                    for (; e < g; e++) d.createElement(f[e]);
                    return d;
                }

                function q(a, b) {
                    b.cache || (b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment,
                        b.frag = b.createFrag()), a.createElement = function(c) {
                        return s.shivMethods ? o(c, a, b) : b.createElem(c);
                    }, a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + m().join().replace(/[\w\-]+/g, function(a) {
                        return b.createElem(a), b.frag.createElement(a), 'c("' + a + '")';
                    }) + ");return n}")(s, b.frag);
                }

                function r(a) {
                    a || (a = b);
                    var c = n(a);
                    return s.shivCSS && !g && !c.hasCSS && (c.hasCSS = !!l(a, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")),
                        k || q(a, c), a;
                }
                var c = "3.7.0",
                    d = a.html5 || {},
                    e = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
                    f = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
                    g, h = "_html5shiv",
                    i = 0,
                    j = {},
                    k;
                (function() {
                    try {
                        var a = b.createElement("a");
                        a.innerHTML = "<xyz></xyz>", g = "hidden" in a, k = a.childNodes.length == 1 || function() {
                            b.createElement("a");
                            var a = b.createDocumentFragment();
                            return typeof a.cloneNode == "undefined" || typeof a.createDocumentFragment == "undefined" || typeof a.createElement == "undefined";
                        }();
                    } catch (c) {
                        g = !0, k = !0;
                    }
                })();
                var s = {
                    elements: d.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
                    version: c,
                    shivCSS: d.shivCSS !== !1,
                    supportsUnknownElements: k,
                    shivMethods: d.shivMethods !== !1,
                    type: "default",
                    shivDocument: r,
                    createElement: o,
                    createDocumentFragment: p
                };
                a.html5 = s, r(b);
            }(this, b), e._version = d, e._prefixes = m, e._domPrefixes = p, e._cssomPrefixes = o,
            e.mq = y, e.testProp = function(a) {
                return F([a]);
            }, e.testAllProps = H, e.testStyles = x, g.className = g.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (f ? " js " + u.join(" ") : ""),
            e;
    }(this, this.document),
    function(a, b, c) {
        function d(a) {
            return "[object Function]" == o.call(a);
        }

        function e(a) {
            return "string" == typeof a;
        }

        function f() {}

        function g(a) {
            return !a || "loaded" == a || "complete" == a || "uninitialized" == a;
        }

        function h() {
            var a = p.shift();
            q = 1, a ? a.t ? m(function() {
                ("c" == a.t ? B.injectCss : B.injectJs)(a.s, 0, a.a, a.x, a.e, 1);
            }, 0) : (a(), h()) : q = 0;
        }

        function i(a, c, d, e, f, i, j) {
            function k(b) {
                if (!o && g(l.readyState) && (u.r = o = 1, !q && h(), l.onload = l.onreadystatechange = null,
                        b)) {
                    "img" != a && m(function() {
                        t.removeChild(l);
                    }, 50);
                    for (var d in y[c]) y[c].hasOwnProperty(d) && y[c][d].onload();
                }
            }
            var j = j || B.errorTimeout,
                l = b.createElement(a),
                o = 0,
                r = 0,
                u = {
                    t: d,
                    s: c,
                    e: f,
                    a: i,
                    x: j
                };
            1 === y[c] && (r = 1, y[c] = []), "object" == a ? l.data = c : (l.src = c, l.type = a),
                l.width = l.height = "0", l.onerror = l.onload = l.onreadystatechange = function() {
                    k.call(this, r);
                }, p.splice(e, 0, u), "img" != a && (r || 2 === y[c] ? (t.insertBefore(l, s ? null : n),
                    m(k, j)) : y[c].push(l));
        }

        function j(a, b, c, d, f) {
            return q = 0, b = b || "j", e(a) ? i("c" == b ? v : u, a, b, this.i++, c, d, f) : (p.splice(this.i++, 0, a),
                1 == p.length && h()), this;
        }

        function k() {
            var a = B;
            return a.loader = {
                load: j,
                i: 0
            }, a;
        }
        var l = b.documentElement,
            m = a.setTimeout,
            n = b.getElementsByTagName("script")[0],
            o = {}.toString,
            p = [],
            q = 0,
            r = "MozAppearance" in l.style,
            s = r && !!b.createRange().compareNode,
            t = s ? l : n.parentNode,
            l = a.opera && "[object Opera]" == o.call(a.opera),
            l = !!b.attachEvent && !l,
            u = r ? "object" : l ? "script" : "img",
            v = l ? "script" : u,
            w = Array.isArray || function(a) {
                return "[object Array]" == o.call(a);
            },
            x = [],
            y = {},
            z = {
                timeout: function(a, b) {
                    return b.length && (a.timeout = b[0]), a;
                }
            },
            A, B;
        B = function(a) {
            function b(a) {
                var a = a.split("!"),
                    b = x.length,
                    c = a.pop(),
                    d = a.length,
                    c = {
                        url: c,
                        origUrl: c,
                        prefixes: a
                    },
                    e, f, g;
                for (f = 0; f < d; f++) g = a[f].split("="), (e = z[g.shift()]) && (c = e(c, g));
                for (f = 0; f < b; f++) c = x[f](c);
                return c;
            }

            function g(a, e, f, g, h) {
                var i = b(a),
                    j = i.autoCallback;
                i.url.split(".").pop().split("?").shift(), i.bypass || (e && (e = d(e) ? e : e[a] || e[g] || e[a.split("/").pop().split("?")[0]]),
                    i.instead ? i.instead(a, e, f, g, h) : (y[i.url] ? i.noexec = !0 : y[i.url] = 1,
                        f.load(i.url, i.forceCSS || !i.forceJS && "css" == i.url.split(".").pop().split("?").shift() ? "c" : c, i.noexec, i.attrs, i.timeout), (d(e) || d(j)) && f.load(function() {
                            k(), e && e(i.origUrl, h, g), j && j(i.origUrl, h, g), y[i.url] = 2;
                        })));
            }

            function h(a, b) {
                function c(a, c) {
                    if (a) {
                        if (e(a)) c || (j = function() {
                            var a = [].slice.call(arguments);
                            k.apply(this, a), l();
                        }), g(a, j, b, 0, h);
                        else if (Object(a) === a)
                            for (n in m = function() {
                                    var b = 0,
                                        c;
                                    for (c in a) a.hasOwnProperty(c) && b++;
                                    return b;
                                }(), a) a.hasOwnProperty(n) && (!c && !--m && (d(j) ? j = function() {
                                var a = [].slice.call(arguments);
                                k.apply(this, a), l();
                            } : j[n] = function(a) {
                                return function() {
                                    var b = [].slice.call(arguments);
                                    a && a.apply(this, b), l();
                                };
                            }(k[n])), g(a[n], j, b, n, h));
                    } else !c && l();
                }
                var h = !!a.test,
                    i = a.load || a.both,
                    j = a.callback || f,
                    k = j,
                    l = a.complete || f,
                    m, n;
                c(h ? a.yep : a.nope, !!i), i && c(i);
            }
            var i, j, l = this.yepnope.loader;
            if (e(a)) g(a, 0, l, 0);
            else if (w(a))
                for (i = 0; i < a.length; i++) j = a[i],
                    e(j) ? g(j, 0, l, 0) : w(j) ? B(j) : Object(j) === j && h(j, l);
            else Object(a) === a && h(a, l);
        }, B.addPrefix = function(a, b) {
            z[a] = b;
        }, B.addFilter = function(a) {
            x.push(a);
        }, B.errorTimeout = 1e4, null == b.readyState && b.addEventListener && (b.readyState = "loading",
            b.addEventListener("DOMContentLoaded", A = function() {
                b.removeEventListener("DOMContentLoaded", A, 0), b.readyState = "complete";
            }, 0)), a.yepnope = k(), a.yepnope.executeStack = h, a.yepnope.injectJs = function(a, c, d, e, i, j) {
            var k = b.createElement("script"),
                l, o, e = e || B.errorTimeout;
            k.src = a;
            for (o in d) k.setAttribute(o, d[o]);
            c = j ? h : c || f, k.onreadystatechange = k.onload = function() {
                !l && g(k.readyState) && (l = 1, c(), k.onload = k.onreadystatechange = null);
            }, m(function() {
                l || (l = 1, c(1));
            }, e), i ? k.onload() : n.parentNode.insertBefore(k, n);
        }, a.yepnope.injectCss = function(a, c, d, e, g, i) {
            var e = b.createElement("link"),
                j, c = i ? h : c || f;
            e.href = a, e.rel = "stylesheet", e.type = "text/css";
            for (j in d) e.setAttribute(j, d[j]);
            g || (n.parentNode.insertBefore(e, n), m(c, 0));
        };
    }(this, document), Modernizr.load = function() {
        yepnope.apply(window, [].slice.call(arguments, 0));
    };

define("modernizr", function(global) {
    return function() {
        var ret, fn;
        return ret || global.Modernizr;
    };
}(this));

if (typeof Object.create !== "function") {
    Object.create = function(o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}

(function() {
    var lastTime = 0;
    var vendors = ["ms", "moz", "webkit", "o"];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + "RequestAnimationFrame"];
        window.cancelAnimationFrame = window[vendors[x] + "CancelAnimationFrame"] || window[vendors[x] + "CancelRequestAnimationFrame"];
    }
    if (!window.requestAnimationFrame) window.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() {
            callback(currTime + timeToCall);
        }, timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };
    if (!window.cancelAnimationFrame) window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    };
})();

(function() {
    if (!window.console) {
        window.console = {};
    } else {
        console.log("use http://[url]?debug=true for console.logs");
    }
    var m = ["log", "info", "warn", "error", "debug", "trace", "dir", "group", "groupCollapsed", "groupEnd", "time", "timeEnd", "profile", "profileEnd", "dirxml", "assert", "count", "markTimeline", "timeStamp", "clear"];
    for (var i = 0; i < m.length; i++) {
        if (!window.console[m[i]]) {
            window.console[m[i]] = function() {};
        }
    }
})();

function htmlEncode(value) {
    return $("<div/>").text(value).html();
}

function htmlDecode(value) {
    return $("<div/>").html(value).text();
}

var tracecount = 0;

function trace(datastring, color, backgroundcolor, targetDiv) {
    if (targetDiv == undefined) {
        tracecount++;
    }
    if (typeof datastring == "object") {
        datastring = JSON.stringify(datastring);
    }
    var buildstringNS = buildstring = "![" + String(tracecount) + "] > " + datastring;
    if (targetDiv != undefined) {
        buildstring = '<span style="color:' + color + "; background-color:" + backgroundcolor + ';">' + htmlEncode(datastring) + "</span>";
    } else if (backgroundcolor != undefined) {
        buildstring = '<span style="color:' + color + "; background-color:" + backgroundcolor + ';">' + htmlEncode(buildstring) + "</span><br>";
    } else if (color != undefined) {
        buildstring = '<span style="color:' + color + ';">' + htmlEncode(buildstring) + "</span><br>";
    } else {
        buildstring = htmlEncode(buildstring) + "</br>";
    }
    if (targetDiv != undefined) {
        $(targetDiv).empty().prepend(buildstring);
    } else {
        console.log(buildstringNS);
        $(".debugConsole").prepend(buildstring);
    }
}

_notify = function() {
    var debug = function() {};
    var components = {};
    var broadcast = function(event, args, source) {
        if (!event) {
            return;
        }
        args = args || [];
        for (var c in components) {
            if (typeof components[c]["on" + event] == "function") {
                try {
                    source = source || components[c];
                    components[c]["on" + event].apply(source, args);
                } catch (err) {
                    debug(["Mediator error.", event, args, source, err].join(" "));
                }
            }
        }
    };
    var addComponent = function(name, component, replaceDuplicate) {
        if (name in components) {
            if (replaceDuplicate) {
                removeComponent(name);
            } else {
                throw new Error("The object: " + name + " has already applied listeners");
            }
        }
        components[name] = component;
    };
    var removeComponent = function(name) {
        if (name in components) {
            delete components[name];
        }
    };
    var getComponent = function(name) {
        return components[name];
    };
    var contains = function(name) {
        return name in components;
    };
    return {
        name: "Mediator",
        broadcast: broadcast,
        add: addComponent,
        rem: removeComponent,
        get: getComponent,
        has: contains
    };
}();

_notify.add("global", function() {
    var tracecount = 0;
    var alertcount = 0;
    return {
        onTrace: function(o) {
            tracecount++;
            var datastring = o.data;
            if (typeof datastring == "object") {
                datastring = JSON.stringify(datastring);
            }
            var buildstring = "[" + String(tracecount) + "] (" + o.senderID + ":" + o.sendertype + ":" + o.notifyscope + ") > " + datastring;
            if (_global$.getQuery("debug")) console.log(buildstring);
        },
        onAlert: function(o) {
            alertcount++;
            var datastring = o.data;
            if (typeof datastring == "object") {
                datastring = JSON.stringify(datastring);
            }
            var buildstring = "[" + String(tracecount) + "] (" + o.senderID + ":" + o.sendertype + ":" + o.notifyscope + ") > " + datastring;
            if (_global$.getQuery("debug")) {
                alert(buildstring);
            }
        },
        onInitialize: function(o) {
            if (_global$.getQuery("debug")) console.log("Created Instance #" + o.senderID + " type:" + o.sendertype + " by " + o.data.author + " [notifyscope:" + o.notifyscope + "]");
        }
    };
}());

require(["modernizr"], function(Modernizr) {
    function Monarch_Base() {
        if (arguments.callee._singletonInstance) return arguments.callee._singletonInstance;
        arguments.callee._singletonInstance = this;
        var parent = this;
        this.isTouch = Modernizr.touch;
        this.host = location.host;
        this.path = location.pathname;
        this.islocalhost = false;
        if (this.host.indexOf("localhost") != -1 || this.host.indexOf("127.0.0.1") != -1) {
            this.islocalhost = true;
        }
        this.jsonpReturn = function(id, type, notifyscope) {
            return function(data) {
                _notify.broadcast(type, [{
                    senderID: id,
                    sendertype: type,
                    notifyscope: notifyscope,
                    data: data
                }]);
            };
        };
        this._isupdated = function(value, currentVal, notify) {
            if (parent[value] != currentVal) {
                var Oldvalue = parent[value];
                parent[value] = currentVal;
                if (Oldvalue != undefined) {
                    this.notify(notify, parent[value]);
                }
            }
        };
        this.notify = function(type, data) {
            _notify.broadcast(type, [{
                senderID: "global",
                sendertype: "global",
                notifyscope: "global",
                data: data
            }]);
        };
        this.getQuery = function(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    var returncandidate = pair[1];
                    if (returncandidate == "true" || returncandidate == "yes" || returncandidate == "y" || returncandidate == "t") {
                        returncandidate = true;
                    }
                    if (returncandidate == "false" || returncandidate == "no" || returncandidate == "n" || returncandidate == "f") {
                        returncandidate = false;
                    }
                    return returncandidate;
                }
            }
            return false;
        };
        this.getHash = function(variable) {
            return window.location.hash;
        };
        this.setCookie = function(key, value) {
            var expires = new Date();
            expires.setTime(expires.getTime() + 1 * 24 * 60 * 60 * 1e3);
            document.cookie = key + "=" + value + ";expires=" + expires.toUTCString();
        };
        this.getCookie = function(key) {
            var keyValue = document.cookie.match("(^|;) ?" + key + "=([^;]*)(;|$)");
            return keyValue ? keyValue[2] : null;
        };
    }
    window._global$ = new Monarch_Base();
});

define("libraries/etc/required_chromecast.js", function() {});

var AETN = AETN || {};

AETN.whatisThis = "AETV custom receiver";

AETN.apiKey = "599b88193e3f5a6c96f7a362a75b34ad";

AETN.adobeTrackingServer = "metrics.aetn.com";

AETN.adobeTrackingSecureServer = "smetrics.aetn.com";

AETN.adobeTrackingSuiteID = "aetnwatchglobal";

AETN.adobeMCORDID = "08AC467352782D0D0A490D45@AdobeOrg";

AETN.mDialog_playoptions = "switch=hls&assetTypes=medium_video_ak&mbr=true&metafile=false";

AETN.omnitureDefaults = {
    platform: "Chromecast"
};

define("globalConfig", function() {});

AETN.environment = "dev";

AETN.signinURL = "http://dev-signature.video.aetndigital.com?url=";

AETN.proxyWithSigninURL = "https://dev-chromecast.ott.aetnd.com/proxy.php?mode=JSON&full_status=1&full_headers=1&url=";

define("environmentConfig", function() {});

AETN.brandID = "fyi";

AETN.brandID_omnitureName = "FYI";

AETN.streamActivityKey = "64524e36b96b6f547b3e9dce101bf8bd";

AETN.applicationKey = "2cd4ad341d4d86c3fb0a764506482f5d";

AETN.titleFeedNew = "http://dev-feeds.video.aetnd.com/api/fyi/videos?filter%5Bid%5D=";

switch (AETN.environment) {
    case "dev":
        AETN.applicationID = "9CB42BEA";
        AETN.adobeReportSuiteID = "aetnchromecastfyidev";
        break;

    case "qa":
        AETN.applicationID = "297FB284";
        AETN.adobeReportSuiteID = "aetnchromecastfyidev";
        break;

    default:
        AETN.applicationID = "D18F7F6A";
        AETN.streamActivityKey = undefined;
        AETN.adobeReportSuiteID = "aetnchromecastfyi";
}

AETN.namespace = "urn:x-cast:com." + AETN.brandID + "_" + AETN.environment + ".chromecast";

define("brandConfig", function() {});

require.config({
    baseUrl: ".",
    paths: {
        underscore: "libraries/underscore/underscore",
        jquery: "libraries/jquery1x/jquery-1.10.2",
        modernizr: "libraries/modernizr/modernizr.custom.43687",
        x2js: "libraries/parsing/x2js/xml2json",
        knockout: "libraries/knockout/knockout-2.3.0",
        i18n: "libraries/require/i18n",
        dust: "libraries/dust/dist/dust-full-2.0.0.min",
        tweenmax: "libraries/greensocks/TweenMax.min",
        tweenlite: "libraries/greensocks/TweenLite.min",
        timelinelite: "libraries/greensocks/TimelineLite.min",
        timelinemax: "libraries/greensocks/TimelineMax.min",
        tweenmax_1_11_8: "libraries/greensocks_1.11.8/TweenMax",
        tweenlite_1_11_8: "libraries/greensocks_1.11.8/TweenLite",
        timelinelite_1_11_8: "libraries/greensocks_1.11.8/TimelineLite",
        timelinemax_1_11_8: "libraries/greensocks_1.11.8/TimelineMax",
        draggable_1_11_8: "libraries/greensocks_1.11.8/utils/Draggable.js",
        scrollto: "libraries/greensocks/plugins/ScrollToPlugin.min",
        hammer: "libraries/hammer/jquery.hammer",
        angular: "libraries/angular/angular.min",
        angularresource: "libraries/angular/angular-resource.min",
        oboe: "libraries/oboe/oboe-browser"
    },
    shim: {
        underscore: {
            exports: "_"
        },
        modernizr: {
            exports: "Modernizr"
        },
        x2js: {
            exports: "X2JS"
        },
        dust: {
            exports: "dust"
        },
        tweenmax: {
            exports: "TweenMax"
        },
        tweenlite: {
            exports: "TweenLite"
        },
        timelinelite: {
            exports: "TimelineLite"
        },
        timelinemax: {
            exports: "TimelineMax"
        },
        tweenmax_1_11_8: {
            exports: "TweenMax"
        },
        tweenlite_1_11_8: {
            exports: "TweenLite"
        },
        timelinelite_1_11_8: {
            exports: "TimelineLite"
        },
        timelinemax_1_11_8: {
            exports: "TimelineMax"
        },
        hammer: {
            exports: "Hammer"
        },
        scrollto: {
            exports: "Scrollto"
        },
        angular: {
            exports: "angular"
        },
        "angular-resource": {
            deps: ["angular"]
        }
    }
});

define("requirejsConfig", function() {});

(function() {
    "use strict";
    var MEDIA_SOURCE_ROOT = "";
    var MEDIA_SOURCE_URL = "";
    var PROGRESS_BAR_WIDTH = 600;
    var DEVICE_STATE = {
        IDLE: 0,
        ACTIVE: 1,
        WARNING: 2,
        ERROR: 3
    };
    var PLAYER_STATE = {
        IDLE: "IDLE",
        LOADING: "LOADING",
        LOADED: "LOADED",
        PLAYING: "PLAYING",
        PAUSED: "PAUSED",
        STOPPED: "STOPPED",
        SEEKING: "SEEKING",
        ERROR: "ERROR"
    };
    var CastPlayer = function() {
        this.deviceState = DEVICE_STATE.IDLE;
        this.receivers_available = false;
        this.currentMediaSession = null;
        this.currentVolume = .5;
        this.autoplay = true;
        this.session = null;
        this.castPlayerState = PLAYER_STATE.IDLE;
        this.localPlayerState = PLAYER_STATE.IDLE;
        this.localPlayer = null;
        this.fullscreen = false;
        this.audio = true;
        this.currentMediaIndex = 0;
        this.currentMediaTime = 0;
        this.currentMediaDuration = -1;
        this.timer = null;
        this.progressFlag = true;
        this.timerStep = 1e3;
        this.mediaContents = null;
    };
    CastPlayer.prototype.initializeLocalPlayer = function() {
        this.localPlayer = document.getElementById("video_element");
        this.localPlayer.addEventListener("loadeddata", this.onMediaLoadedLocally.bind(this));
    };
    CastPlayer.prototype.initializeCastPlayer = function() {
        if (!chrome.cast || !chrome.cast.isAvailable) {
            setTimeout(this.initializeCastPlayer.bind(this), 1e3);
            return;
        }
        var applicationID = AETN.applicationID;
        var autoJoinPolicy = chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED;
        var sessionRequest = new chrome.cast.SessionRequest(applicationID);
        var apiConfig = new chrome.cast.ApiConfig(sessionRequest, this.sessionListener.bind(this), this.receiverListener.bind(this), autoJoinPolicy);
        chrome.cast.initialize(apiConfig, this.onInitSuccess.bind(this), this.onError.bind(this));
        this.addVideoThumbs();
        this.initializeUI();
    };
    CastPlayer.prototype.onInitSuccess = function() {
        console.log("init success");
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.onError = function() {
        console.log("error");
    };
    CastPlayer.prototype.sessionListener = function(e) {
        trace("sessionListener:/");
        var parent = this;
        this.session = e;
        if (this.session) {
            this.deviceState = DEVICE_STATE.ACTIVE;
            if (this.session.media[0]) {
                this.onMediaDiscovered("activeSession", this.session.media[0]);
                this.syncCurrentMedia(this.session.media[0].media.contentId);
                this.selectMediaUpdateUI(this.currentMediaIndex);
                this.updateDisplayMessage();
            } else {}
            this.session.addUpdateListener(this.sessionUpdateListener.bind(this));
            this.session.addMessageListener(AETN.namespace, parent.messageRouter);
        }
    };
    CastPlayer.prototype.messageRouter = function(namespace, getDataString) {
        var jData = JSON.parse(getDataString);
        alert("receiverMessage: " + namespace + ", " + JSON.stringify(jData));
    };
    CastPlayer.prototype.syncCurrentMedia = function(currentMediaURL) {
        for (var i = 0; i < this.mediaContents.length; i++) {
            if (currentMediaURL == this.mediaContents[i]["sources"][0]) {
                this.currentMediaIndex = i;
            }
        }
    };
    CastPlayer.prototype.receiverListener = function(e) {
        if (e === "available") {
            this.receivers_available = true;
            this.updateMediaControlUI();
            console.log("receiver found");
        } else {
            console.log("receiver list empty");
        }
    };
    CastPlayer.prototype.sessionUpdateListener = function(isAlive) {
        if (!isAlive) {
            this.session = null;
            this.deviceState = DEVICE_STATE.IDLE;
            this.castPlayerState = PLAYER_STATE.IDLE;
            this.currentMediaSession = null;
            clearInterval(this.timer);
            this.updateDisplayMessage();
            var online = navigator.onLine;
            if (online == true) {
                console.log("current time: " + this.currentMediaTime);
                this.playMediaLocally();
                this.updateMediaControlUI();
            }
        }
    };
    CastPlayer.prototype.selectMedia = function(mediaIndex) {
        console.log("media selected" + mediaIndex);
        this.currentMediaIndex = mediaIndex;
        var pi = document.getElementById("progress_indicator");
        var p = document.getElementById("progress");
        this.currentMediaTime = 0;
        p.style.width = "0px";
        pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + "px";
        if (!this.currentMediaSession) {
            this.localPlayerState = PLAYER_STATE.IDLE;
            this.playMediaLocally();
        } else {
            this.castPlayerState = PLAYER_STATE.IDLE;
            this.playMedia();
        }
        this.selectMediaUpdateUI(mediaIndex);
    };
    CastPlayer.prototype.launchApp = function() {
        console.log("launching app...");
        chrome.cast.requestSession(this.sessionListener.bind(this), this.onLaunchError.bind(this));
        if (this.timer) {
            clearInterval(this.timer);
        }
    };
    CastPlayer.prototype.onRequestSessionSuccess = function(e) {
        console.log("session success: " + e.sessionId);
        this.session = e;
        this.deviceState = DEVICE_STATE.ACTIVE;
        this.updateMediaControlUI();
        this.loadMedia(this.currentMediaIndex);
        this.session.addUpdateListener(this.sessionUpdateListener.bind(this));
    };
    CastPlayer.prototype.onLaunchError = function() {
        console.log("launch error");
        this.deviceState = DEVICE_STATE.ERROR;
    };
    CastPlayer.prototype.stopApp = function() {
        this.session.stop(this.onStopAppSuccess.bind(this, "Session stopped"), this.onError.bind(this));
    };
    CastPlayer.prototype.onStopAppSuccess = function(message) {
        console.log(message);
        this.deviceState = DEVICE_STATE.IDLE;
        this.castPlayerState = PLAYER_STATE.IDLE;
        this.currentMediaSession = null;
        clearInterval(this.timer);
        this.updateDisplayMessage();
        console.log("current time: " + this.currentMediaTime);
        this.playMediaLocally();
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.loadMedia = function(mediaIndex) {
        this.loadMedia_manual(this.mediaContents[mediaIndex]);
    };
    CastPlayer.prototype.loadMedia_manual = function(mediaContents) {
        if (!this.session) {
            console.log("no session");
            return;
        }
        console.log("loading..." + mediaContents["title"]);
        var mediaInfo = new chrome.cast.media.MediaInfo(mediaContents.platformID);
        console.log("mediaInfo=" + JSON.stringify(mediaInfo));
        mediaInfo.metadata = new chrome.cast.media.GenericMediaMetadata();
        mediaInfo.contentType = "video/mp4";
        mediaInfo.customData = mediaContents;
        var request = new chrome.cast.media.LoadRequest(mediaInfo);
        request.autoplay = this.autoplay;
        if (this.localPlayerState == PLAYER_STATE.PLAYING) {
            request.currentTime = this.localPlayer.currentTime;
            this.localPlayer.pause();
            this.localPlayerState = PLAYER_STATE.STOPPED;
        } else {
            request.currentTime = 0;
        }
        this.castPlayerState = PLAYER_STATE.LOADING;
        this.session.loadMedia(request, this.onMediaDiscovered.bind(this, "loadMedia"), this.onLoadMediaError.bind(this));
        document.getElementById("media_title").innerHTML = this.mediaContents[this.currentMediaIndex]["title"];
        document.getElementById("media_subtitle").innerHTML = this.mediaContents[this.currentMediaIndex]["subtitle"];
        document.getElementById("media_desc").innerHTML = this.mediaContents[this.currentMediaIndex]["description"];
    };
    CastPlayer.prototype.onMediaDiscovered = function(how, mediaSession) {
        console.log("new media session ID:" + mediaSession.mediaSessionId + " (" + how + ")");
        this.currentMediaSession = mediaSession;
        if (how == "loadMedia") {
            if (this.autoplay) {
                this.castPlayerState = PLAYER_STATE.PLAYING;
            } else {
                this.castPlayerState = PLAYER_STATE.LOADED;
            }
        }
        if (how == "activeSession") {
            this.castPlayerState = this.session.media[0].playerState;
            this.currentMediaTime = this.session.media[0].currentTime;
        }
        if (this.castPlayerState == PLAYER_STATE.PLAYING) {
            this.startProgressTimer(this.incrementMediaTime);
        }
        this.currentMediaSession.addUpdateListener(this.onMediaStatusUpdate.bind(this));
        this.currentMediaDuration = this.currentMediaSession.media.duration;
        var duration = this.currentMediaDuration;
        var hr = parseInt(duration / 3600);
        duration -= hr * 3600;
        var min = parseInt(duration / 60);
        var sec = parseInt(duration % 60);
        if (hr > 0) {
            duration = hr + ":" + min + ":" + sec;
        } else {
            if (min > 0) {
                duration = min + ":" + sec;
            } else {
                duration = sec;
            }
        }
        document.getElementById("duration").innerHTML = duration;
        if (this.localPlayerState == PLAYER_STATE.PLAYING) {
            this.localPlayerState == PLAYER_STATE.STOPPED;
            var vi = document.getElementById("video_image");
            vi.style.display = "block";
            this.localPlayer.style.display = "none";
            this.startProgressTimer(this.incrementMediaTime);
        }
        this.updateMediaControlUI();
        this.updateDisplayMessage();
    };
    CastPlayer.prototype.onLoadMediaError = function(e) {
        console.log("media error");
        this.castPlayerState = PLAYER_STATE.IDLE;
        this.updateMediaControlUI();
        this.updateDisplayMessage();
    };
    CastPlayer.prototype.onMediaStatusUpdate = function(e) {
        if (e == false) {
            this.currentMediaTime = 0;
            this.castPlayerState = PLAYER_STATE.IDLE;
        }
        console.log("updating media");
        this.updateProgressBar(e);
        this.updateDisplayMessage();
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.incrementMediaTime = function() {
        if (this.castPlayerState == PLAYER_STATE.PLAYING || this.localPlayerState == PLAYER_STATE.PLAYING) {
            if (this.currentMediaTime < this.currentMediaDuration) {
                this.currentMediaTime += 1;
                this.updateProgressBarByTimer();
            } else {
                this.currentMediaTime = 0;
                clearInterval(this.timer);
            }
        }
    };
    CastPlayer.prototype.playMediaLocally = function() {
        var vi = document.getElementById("video_image");
        vi.style.display = "none";
        this.localPlayer.style.display = "block";
        if (this.localPlayerState != PLAYER_STATE.PLAYING && this.localPlayerState != PLAYER_STATE.PAUSED) {
            this.localPlayer.src = this.mediaContents[this.currentMediaIndex]["sources"][0];
            this.localPlayer.load();
        } else {
            this.localPlayer.play();
            this.startProgressTimer(this.incrementMediaTime);
        }
        this.localPlayerState = PLAYER_STATE.PLAYING;
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.onMediaLoadedLocally = function() {
        this.currentMediaDuration = this.localPlayer.duration;
        var duration = this.currentMediaDuration;
        var hr = parseInt(duration / 3600);
        duration -= hr * 3600;
        var min = parseInt(duration / 60);
        var sec = parseInt(duration % 60);
        if (hr > 0) {
            duration = hr + ":" + min + ":" + sec;
        } else {
            if (min > 0) {
                duration = min + ":" + sec;
            } else {
                duration = sec;
            }
        }
        document.getElementById("duration").innerHTML = duration;
        this.localPlayer.currentTime = this.currentMediaTime;
        this.localPlayer.play();
        this.startProgressTimer(this.incrementMediaTime);
    };
    CastPlayer.prototype.playMedia = function() {
        if (!this.currentMediaSession) {
            this.playMediaLocally();
            return;
        }
        switch (this.castPlayerState) {
            case PLAYER_STATE.LOADED:
            case PLAYER_STATE.PAUSED:
                this.currentMediaSession.play(null, this.mediaCommandSuccessCallback.bind(this, "playing started for " + this.currentMediaSession.sessionId), this.onError.bind(this));
                this.currentMediaSession.addUpdateListener(this.onMediaStatusUpdate.bind(this));
                this.castPlayerState = PLAYER_STATE.PLAYING;
                this.startProgressTimer(this.incrementMediaTime);
                break;

            case PLAYER_STATE.IDLE:
            case PLAYER_STATE.LOADING:
            case PLAYER_STATE.STOPPED:
                this.loadMedia(this.currentMediaIndex);
                this.currentMediaSession.addUpdateListener(this.onMediaStatusUpdate.bind(this));
                this.castPlayerState = PLAYER_STATE.PLAYING;
                break;

            default:
                break;
        }
        this.updateMediaControlUI();
        this.updateDisplayMessage();
    };
    CastPlayer.prototype.pauseMedia = function() {
        if (!this.currentMediaSession) {
            this.pauseMediaLocally();
            return;
        }
        if (this.castPlayerState == PLAYER_STATE.PLAYING) {
            this.castPlayerState = PLAYER_STATE.PAUSED;
            this.currentMediaSession.pause(null, this.mediaCommandSuccessCallback.bind(this, "paused " + this.currentMediaSession.sessionId), this.onError.bind(this));
            this.updateMediaControlUI();
            this.updateDisplayMessage();
            clearInterval(this.timer);
        }
    };
    CastPlayer.prototype.pauseMediaLocally = function() {
        this.localPlayer.pause();
        this.localPlayerState = PLAYER_STATE.PAUSED;
        this.updateMediaControlUI();
        clearInterval(this.timer);
    };
    CastPlayer.prototype.stopMedia = function() {
        if (!this.currentMediaSession) {
            this.stopMediaLocally();
            return;
        }
        this.currentMediaSession.stop(null, this.mediaCommandSuccessCallback.bind(this, "stopped " + this.currentMediaSession.sessionId), this.onError.bind(this));
        this.castPlayerState = PLAYER_STATE.STOPPED;
        clearInterval(this.timer);
        this.updateDisplayMessage();
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.stopMediaLocally = function() {
        var vi = document.getElementById("video_image");
        vi.style.display = "block";
        this.localPlayer.style.display = "none";
        this.localPlayer.stop();
        this.localPlayerState = PLAYER_STATE.STOPPED;
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.setReceiverVolume = function(mute) {
        var p = document.getElementById("audio_bg_level");
        if (event.currentTarget.id == "audio_bg_track") {
            var pos = 100 - parseInt(event.offsetY);
        } else {
            var pos = parseInt(p.clientHeight) - parseInt(event.offsetY);
        }
        if (!this.currentMediaSession) {
            this.localPlayer.volume = pos < 100 ? pos / 100 : 1;
            p.style.height = pos + "px";
            p.style.marginTop = -pos + "px";
            return;
        }
        if (event.currentTarget.id == "audio_bg_track" || event.currentTarget.id == "audio_bg_level") {
            if (pos < 100) {
                var vScale = this.currentVolume * 100;
                if (pos > vScale) {
                    pos = vScale + (pos - vScale) / 2;
                }
                p.style.height = pos + "px";
                p.style.marginTop = -pos + "px";
                this.currentVolume = pos / 100;
            } else {
                this.currentVolume = 1;
            }
        }
        if (!mute) {
            this.session.setReceiverVolumeLevel(this.currentVolume, this.mediaCommandSuccessCallback.bind(this), this.onError.bind(this));
        } else {
            this.session.setReceiverMuted(true, this.mediaCommandSuccessCallback.bind(this), this.onError.bind(this));
        }
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.muteMedia = function() {
        if (this.audio == true) {
            this.audio = false;
            document.getElementById("audio_on").style.display = "none";
            document.getElementById("audio_off").style.display = "block";
            if (this.currentMediaSession) {
                this.setReceiverVolume(true);
            } else {
                this.localPlayer.muted = true;
            }
        } else {
            this.audio = true;
            document.getElementById("audio_on").style.display = "block";
            document.getElementById("audio_off").style.display = "none";
            if (this.currentMediaSession) {
                this.setReceiverVolume(false);
            } else {
                this.localPlayer.muted = false;
            }
        }
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.seekMedia = function(event) {
        var pos = parseInt(event.offsetX);
        var pi = document.getElementById("progress_indicator");
        var p = document.getElementById("progress");
        if (event.currentTarget.id == "progress_indicator") {
            var curr = parseInt(this.currentMediaTime + this.currentMediaDuration * pos / PROGRESS_BAR_WIDTH);
            var pp = parseInt(pi.style.marginLeft) + pos;
            var pw = parseInt(p.style.width) + pos;
        } else {
            var curr = parseInt(pos * this.currentMediaDuration / PROGRESS_BAR_WIDTH);
            var pp = pos - 21 - PROGRESS_BAR_WIDTH;
            var pw = pos;
        }
        if (this.localPlayerState == PLAYER_STATE.PLAYING || this.localPlayerState == PLAYER_STATE.PAUSED) {
            this.localPlayer.currentTime = curr;
            this.currentMediaTime = curr;
            this.localPlayer.play();
        }
        if (this.localPlayerState == PLAYER_STATE.PLAYING || this.localPlayerState == PLAYER_STATE.PAUSED || this.castPlayerState == PLAYER_STATE.PLAYING || this.castPlayerState == PLAYER_STATE.PAUSED) {
            p.style.width = pw + "px";
            pi.style.marginLeft = pp + "px";
        }
        if (this.castPlayerState != PLAYER_STATE.PLAYING && this.castPlayerState != PLAYER_STATE.PAUSED) {
            return;
        }
        this.currentMediaTime = curr;
        console.log("Seeking " + this.currentMediaSession.sessionId + ":" + this.currentMediaSession.mediaSessionId + " to " + pos + "%");
        var request = new chrome.cast.media.SeekRequest();
        request.currentTime = this.currentMediaTime;
        this.currentMediaSession.seek(request, this.onSeekSuccess.bind(this, "media seek done"), this.onError.bind(this));
        this.castPlayerState = PLAYER_STATE.SEEKING;
        this.updateDisplayMessage();
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.onSeekSuccess = function(info) {
        console.log(info);
        this.castPlayerState = PLAYER_STATE.PLAYING;
        this.updateDisplayMessage();
        this.updateMediaControlUI();
    };
    CastPlayer.prototype.mediaCommandSuccessCallback = function(info, e) {
        console.log(info);
    };
    CastPlayer.prototype.updateProgressBar = function(e) {
        var p = document.getElementById("progress");
        var pi = document.getElementById("progress_indicator");
        if (e == false) {
            p.style.width = "0px";
            pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + "px";
            clearInterval(this.timer);
            this.castPlayerState = PLAYER_STATE.STOPPED;
            this.updateDisplayMessage();
        } else {
            p.style.width = Math.ceil(PROGRESS_BAR_WIDTH * this.currentMediaSession.currentTime / this.currentMediaSession.media.duration + 1) + "px";
            this.progressFlag = false;
            setTimeout(this.setProgressFlag.bind(this), 1e3);
            var pp = Math.ceil(PROGRESS_BAR_WIDTH * this.currentMediaSession.currentTime / this.currentMediaSession.media.duration);
            pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + pp + "px";
        }
    };
    CastPlayer.prototype.setProgressFlag = function() {
        this.progressFlag = true;
    };
    CastPlayer.prototype.updateProgressBarByTimer = function() {
        var p = document.getElementById("progress");
        if (isNaN(parseInt(p.style.width))) {
            p.style.width = 0;
        }
        if (this.currentMediaDuration > 0) {
            var pp = Math.floor(PROGRESS_BAR_WIDTH * this.currentMediaTime / this.currentMediaDuration);
        }
        if (this.progressFlag) {
            p.style.width = pp + "px";
            var pi = document.getElementById("progress_indicator");
            pi.style.marginLeft = -21 - PROGRESS_BAR_WIDTH + pp + "px";
        }
        if (pp > PROGRESS_BAR_WIDTH) {
            clearInterval(this.timer);
            this.deviceState = DEVICE_STATE.IDLE;
            this.castPlayerState = PLAYER_STATE.IDLE;
            this.updateDisplayMessage();
            this.updateMediaControlUI();
        }
    };
    CastPlayer.prototype.updateDisplayMessage = function() {
        if (this.deviceState != DEVICE_STATE.ACTIVE || this.castPlayerState == PLAYER_STATE.IDLE || this.castPlayerState == PLAYER_STATE.STOPPED) {
            document.getElementById("playerstate").style.display = "none";
            document.getElementById("playerstatebg").style.display = "none";
            document.getElementById("play").style.display = "block";
            document.getElementById("video_image_overlay").style.display = "none";
        } else {
            document.getElementById("playerstate").style.display = "block";
            document.getElementById("playerstatebg").style.display = "block";
            document.getElementById("video_image_overlay").style.display = "block";
            document.getElementById("playerstate").innerHTML = this.mediaContents[this.currentMediaIndex]["title"] + " " + this.castPlayerState + " on " + this.session.receiver.friendlyName;
        }
    };
    CastPlayer.prototype.updateMediaControlUI = function() {
        var playerState = this.deviceState == DEVICE_STATE.ACTIVE ? this.castPlayerState : this.localPlayerState;
        switch (playerState) {
            case PLAYER_STATE.LOADED:
            case PLAYER_STATE.PLAYING:
                document.getElementById("play").style.display = "none";
                document.getElementById("pause").style.display = "block";
                break;

            case PLAYER_STATE.PAUSED:
            case PLAYER_STATE.IDLE:
            case PLAYER_STATE.LOADING:
            case PLAYER_STATE.STOPPED:
                document.getElementById("play").style.display = "block";
                document.getElementById("pause").style.display = "none";
                break;

            default:
                break;
        }
        if (!this.receivers_available) {
            document.getElementById("casticonactive").style.display = "none";
            document.getElementById("casticonidle").style.display = "none";
            return;
        }
        if (this.deviceState == DEVICE_STATE.ACTIVE) {
            document.getElementById("casticonactive").style.display = "block";
            document.getElementById("casticonidle").style.display = "none";
            this.hideFullscreenButton();
        } else {
            document.getElementById("casticonidle").style.display = "block";
            document.getElementById("casticonactive").style.display = "none";
            this.showFullscreenButton();
        }
    };
    CastPlayer.prototype.selectMediaUpdateUI = function(mediaIndex) {
        document.getElementById("video_image").src = MEDIA_SOURCE_ROOT + this.mediaContents[mediaIndex]["thumb"];
        document.getElementById("progress").style.width = "0px";
        document.getElementById("media_title").innerHTML = this.mediaContents[mediaIndex]["title"];
        document.getElementById("media_subtitle").innerHTML = this.mediaContents[mediaIndex]["subtitle"];
        document.getElementById("media_desc").innerHTML = this.mediaContents[mediaIndex]["description"];
    };
    CastPlayer.prototype.initializeUI = function() {
        document.getElementById("media_title").innerHTML = this.mediaContents[0]["title"];
        document.getElementById("media_subtitle").innerHTML = this.mediaContents[this.currentMediaIndex]["subtitle"];
        document.getElementById("media_desc").innerHTML = this.mediaContents[this.currentMediaIndex]["description"];
        document.getElementById("casticonidle").addEventListener("click", this.launchApp.bind(this));
        document.getElementById("casticonactive").addEventListener("click", this.stopApp.bind(this));
        document.getElementById("progress_bg").addEventListener("click", this.seekMedia.bind(this));
        document.getElementById("progress").addEventListener("click", this.seekMedia.bind(this));
        document.getElementById("progress_indicator").addEventListener("dragend", this.seekMedia.bind(this));
        document.getElementById("audio_on").addEventListener("click", this.muteMedia.bind(this));
        document.getElementById("audio_off").addEventListener("click", this.muteMedia.bind(this));
        document.getElementById("audio_bg").addEventListener("mouseover", this.showVolumeSlider.bind(this));
        document.getElementById("audio_on").addEventListener("mouseover", this.showVolumeSlider.bind(this));
        document.getElementById("audio_bg_level").addEventListener("mouseover", this.showVolumeSlider.bind(this));
        document.getElementById("audio_bg_track").addEventListener("mouseover", this.showVolumeSlider.bind(this));
        document.getElementById("audio_bg_level").addEventListener("click", this.setReceiverVolume.bind(this, false));
        document.getElementById("audio_bg_track").addEventListener("click", this.setReceiverVolume.bind(this, false));
        document.getElementById("audio_bg").addEventListener("mouseout", this.hideVolumeSlider.bind(this));
        document.getElementById("audio_on").addEventListener("mouseout", this.hideVolumeSlider.bind(this));
        document.getElementById("media_control").addEventListener("mouseover", this.showMediaControl.bind(this));
        document.getElementById("media_control").addEventListener("mouseout", this.hideMediaControl.bind(this));
        document.getElementById("fullscreen_expand").addEventListener("click", this.requestFullScreen.bind(this));
        document.getElementById("fullscreen_collapse").addEventListener("click", this.cancelFullScreen.bind(this));
        document.addEventListener("fullscreenchange", this.changeHandler.bind(this), false);
        document.addEventListener("webkitfullscreenchange", this.changeHandler.bind(this), false);
        document.getElementById("play").addEventListener("click", this.playMedia.bind(this));
        document.getElementById("pause").addEventListener("click", this.pauseMedia.bind(this));
        document.getElementById("progress_indicator").draggable = true;
    };
    CastPlayer.prototype.showMediaControl = function() {
        document.getElementById("media_control").style.opacity = .7;
    };
    CastPlayer.prototype.hideMediaControl = function() {};
    CastPlayer.prototype.showVolumeSlider = function() {
        document.getElementById("audio_bg").style.opacity = 1;
        document.getElementById("audio_bg_track").style.opacity = 1;
        document.getElementById("audio_bg_level").style.opacity = 1;
        document.getElementById("audio_indicator").style.opacity = 1;
    };
    CastPlayer.prototype.hideVolumeSlider = function() {
        document.getElementById("audio_bg").style.opacity = 0;
        document.getElementById("audio_bg_track").style.opacity = 0;
        document.getElementById("audio_bg_level").style.opacity = 0;
        document.getElementById("audio_indicator").style.opacity = 0;
    };
    CastPlayer.prototype.requestFullScreen = function() {
        var element = document.getElementById("video_element");
        var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen;
        if (requestMethod) {
            requestMethod.call(element);
            console.log("requested fullscreen");
        }
    };
    CastPlayer.prototype.cancelFullScreen = function() {
        var requestMethod = document.cancelFullScreen || document.webkitCancelFullScreen;
        if (requestMethod) {
            requestMethod.call(document);
        }
    };
    CastPlayer.prototype.changeHandler = function() {
        this.fullscreen = !this.fullscreen;
        if (this.deviceState == DEVICE_STATE.ACTIVE) {
            this.hideFullscreenButton();
        } else {
            this.showFullscreenButton();
        }
    };
    CastPlayer.prototype.showFullscreenButton = function() {
        if (this.fullscreen) {
            document.getElementById("fullscreen_expand").style.display = "none";
            document.getElementById("fullscreen_collapse").style.display = "block";
        } else {
            document.getElementById("fullscreen_expand").style.display = "block";
            document.getElementById("fullscreen_collapse").style.display = "none";
        }
    };
    CastPlayer.prototype.hideFullscreenButton = function() {
        document.getElementById("fullscreen_expand").style.display = "none";
        document.getElementById("fullscreen_collapse").style.display = "none";
    };
    CastPlayer.prototype.startProgressTimer = function(callback) {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
        this.timer = setInterval(callback.bind(this), this.timerStep);
    };
    CastPlayer.prototype.retrieveMediaJSON = function(src) {
        console.log("retrieveMediaJSON not needed");
    };
    CastPlayer.prototype.onMediaJsonLoad = function(evt) {
        var responseJson = evt.srcElement.response;
        this.mediaContents = responseJson["categories"][0]["videos"];
        var ni = document.getElementById("carousel");
        var newdiv = null;
        var divIdName = null;
        for (var i = 0; i < this.mediaContents.length; i++) {
            newdiv = document.createElement("div");
            divIdName = "thumb" + i + "Div";
            newdiv.setAttribute("id", divIdName);
            newdiv.setAttribute("class", "thumb");
            newdiv.innerHTML = '<img src="' + MEDIA_SOURCE_ROOT + this.mediaContents[i]["thumb"] + '" class="thumbnail">';
            newdiv.addEventListener("click", this.selectMedia.bind(this, i));
            ni.appendChild(newdiv);
        }
    };
    CastPlayer.prototype.onMediaJsonError = function() {
        console.log("Failed to load media JSON");
    };
    CastPlayer.prototype.addVideoThumbs = function() {
        this.mediaContents = mediaJSON["categories"][0]["videos"];
        var ni = document.getElementById("carousel");
        var newdiv = null;
        var newdivBG = null;
        var divIdName = null;
        for (var i = 0; i < this.mediaContents.length; i++) {
            newdiv = document.createElement("div");
            divIdName = "thumb" + i + "Div";
            newdiv.setAttribute("id", divIdName);
            newdiv.setAttribute("class", "thumb");
            newdiv.innerHTML = '<img src="' + MEDIA_SOURCE_ROOT + this.mediaContents[i]["thumb"] + '" class="thumbnail">';
            newdiv.addEventListener("click", this.selectMedia.bind(this, i));
            ni.appendChild(newdiv);
        }
    };
    var mediaJSON = {
        categories: [{
            name: "Movies",
            videos: [{
                description: "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license\nhttp://www.bigbuckbunny.org",
                sources: ["http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"],
                subtitle: "By Blender Foundation",
                thumb: "images/BigBuckBunny.jpg",
                title: "Big Buck Bunny"
            }]
        }]
    };
    window.CastPlayer = new CastPlayer();
})();

define("chromeSender", function() {});

define("modules/definitions/standardmodule", [], function() {
    var _instanceID = 0;
    var _nextInstanceID = function() {
        return ++_instanceID;
    };
    var defaults = [];
    var deepExtend = function(destination, source) {
        for (var property in source) {
            if (source[property] && source[property].constructor && source[property].constructor === Object) {
                destination[property] = destination[property] || {};
                arguments.callee(destination[property], source[property]);
            } else {
                destination[property] = source[property];
            }
        }
        return destination;
    };

    function _thizOBJ_(o) {
        this._instanceID = _nextInstanceID();
        var _parent = this;
        defaults[this._instanceID] = {
            type: "Standard Module Definition",
            author: "Jonathan Robles",
            notifyscope: "global",
            target: undefined,
            file: undefined,
            usenocache: true,
            data: undefined,
            callback: undefined,
            interval: undefined,
            init: function() {
                _notify.broadcast("Initialize", [{
                    senderID: _parent._instanceID,
                    sendertype: this.type,
                    notifyscope: this.notifyscope,
                    data: {
                        author: this.author
                    }
                }]);
            },
            parent: this
        };
        defaults[this._instanceID] = deepExtend(defaults[this._instanceID], o);
        defaults[this._instanceID].init();
        return this;
    }
    _thizOBJ_.prototype = {
        _init: function() {
            this._var().init();
        },
        _showdata: function() {
            return JSON.stringify(defaults[this._instanceID]);
        },
        _id: function() {
            return this._instanceID;
        },
        _var: function(o) {
            if (o != undefined) {
                defaults[this._instanceID] = deepExtend(defaults[this._instanceID], o);
            }
            return defaults[this._instanceID];
        },
        _nocache: function(string) {
            if (typeof string === "string") {
                if (this._var().usenocache) {
                    var addOn = "?";
                    if (string.indexOf("?") != -1) {
                        addOn = "&";
                    }
                    return string + addOn + "nocache=" + Math.floor(Math.random() * 9999);
                } else {
                    return string;
                }
            } else {
                this.notify("Alert", "_nocache needs a string!");
                return;
            }
        },
        notify: function(type, data) {
            _notify.broadcast(type, [{
                senderID: this._id(),
                sendertype: this._var().type,
                notifyscope: this._var().notifyscope,
                data: data
            }]);
        },
        deepExtend: function(destination, source) {
            return deepExtend(destination, source);
        },
        parent: this
    };
    return _thizOBJ_;
});

(function() {
    var root = this;
    var previousUnderscore = root._;
    var breaker = {};
    var ArrayProto = Array.prototype,
        ObjProto = Object.prototype,
        FuncProto = Function.prototype;
    var push = ArrayProto.push,
        slice = ArrayProto.slice,
        concat = ArrayProto.concat,
        toString = ObjProto.toString,
        hasOwnProperty = ObjProto.hasOwnProperty;
    var nativeForEach = ArrayProto.forEach,
        nativeMap = ArrayProto.map,
        nativeReduce = ArrayProto.reduce,
        nativeReduceRight = ArrayProto.reduceRight,
        nativeFilter = ArrayProto.filter,
        nativeEvery = ArrayProto.every,
        nativeSome = ArrayProto.some,
        nativeIndexOf = ArrayProto.indexOf,
        nativeLastIndexOf = ArrayProto.lastIndexOf,
        nativeIsArray = Array.isArray,
        nativeKeys = Object.keys,
        nativeBind = FuncProto.bind;
    var _ = function(obj) {
        if (obj instanceof _) return obj;
        if (!(this instanceof _)) return new _(obj);
        this._wrapped = obj;
    };
    if (typeof exports !== "undefined") {
        if (typeof module !== "undefined" && module.exports) {
            exports = module.exports = _;
        }
        exports._ = _;
    } else {
        root._ = _;
    }
    _.VERSION = "1.5.2";
    var each = _.each = _.forEach = function(obj, iterator, context) {
        if (obj == null) return;
        if (nativeForEach && obj.forEach === nativeForEach) {
            obj.forEach(iterator, context);
        } else if (obj.length === +obj.length) {
            for (var i = 0, length = obj.length; i < length; i++) {
                if (iterator.call(context, obj[i], i, obj) === breaker) return;
            }
        } else {
            var keys = _.keys(obj);
            for (var i = 0, length = keys.length; i < length; i++) {
                if (iterator.call(context, obj[keys[i]], keys[i], obj) === breaker) return;
            }
        }
    };
    _.map = _.collect = function(obj, iterator, context) {
        var results = [];
        if (obj == null) return results;
        if (nativeMap && obj.map === nativeMap) return obj.map(iterator, context);
        each(obj, function(value, index, list) {
            results.push(iterator.call(context, value, index, list));
        });
        return results;
    };
    var reduceError = "Reduce of empty array with no initial value";
    _.reduce = _.foldl = _.inject = function(obj, iterator, memo, context) {
        var initial = arguments.length > 2;
        if (obj == null) obj = [];
        if (nativeReduce && obj.reduce === nativeReduce) {
            if (context) iterator = _.bind(iterator, context);
            return initial ? obj.reduce(iterator, memo) : obj.reduce(iterator);
        }
        each(obj, function(value, index, list) {
            if (!initial) {
                memo = value;
                initial = true;
            } else {
                memo = iterator.call(context, memo, value, index, list);
            }
        });
        if (!initial) throw new TypeError(reduceError);
        return memo;
    };
    _.reduceRight = _.foldr = function(obj, iterator, memo, context) {
        var initial = arguments.length > 2;
        if (obj == null) obj = [];
        if (nativeReduceRight && obj.reduceRight === nativeReduceRight) {
            if (context) iterator = _.bind(iterator, context);
            return initial ? obj.reduceRight(iterator, memo) : obj.reduceRight(iterator);
        }
        var length = obj.length;
        if (length !== +length) {
            var keys = _.keys(obj);
            length = keys.length;
        }
        each(obj, function(value, index, list) {
            index = keys ? keys[--length] : --length;
            if (!initial) {
                memo = obj[index];
                initial = true;
            } else {
                memo = iterator.call(context, memo, obj[index], index, list);
            }
        });
        if (!initial) throw new TypeError(reduceError);
        return memo;
    };
    _.find = _.detect = function(obj, iterator, context) {
        var result;
        any(obj, function(value, index, list) {
            if (iterator.call(context, value, index, list)) {
                result = value;
                return true;
            }
        });
        return result;
    };
    _.filter = _.select = function(obj, iterator, context) {
        var results = [];
        if (obj == null) return results;
        if (nativeFilter && obj.filter === nativeFilter) return obj.filter(iterator, context);
        each(obj, function(value, index, list) {
            if (iterator.call(context, value, index, list)) results.push(value);
        });
        return results;
    };
    _.reject = function(obj, iterator, context) {
        return _.filter(obj, function(value, index, list) {
            return !iterator.call(context, value, index, list);
        }, context);
    };
    _.every = _.all = function(obj, iterator, context) {
        iterator || (iterator = _.identity);
        var result = true;
        if (obj == null) return result;
        if (nativeEvery && obj.every === nativeEvery) return obj.every(iterator, context);
        each(obj, function(value, index, list) {
            if (!(result = result && iterator.call(context, value, index, list))) return breaker;
        });
        return !!result;
    };
    var any = _.some = _.any = function(obj, iterator, context) {
        iterator || (iterator = _.identity);
        var result = false;
        if (obj == null) return result;
        if (nativeSome && obj.some === nativeSome) return obj.some(iterator, context);
        each(obj, function(value, index, list) {
            if (result || (result = iterator.call(context, value, index, list))) return breaker;
        });
        return !!result;
    };
    _.contains = _.include = function(obj, target) {
        if (obj == null) return false;
        if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1;
        return any(obj, function(value) {
            return value === target;
        });
    };
    _.invoke = function(obj, method) {
        var args = slice.call(arguments, 2);
        var isFunc = _.isFunction(method);
        return _.map(obj, function(value) {
            return (isFunc ? method : value[method]).apply(value, args);
        });
    };
    _.pluck = function(obj, key) {
        return _.map(obj, function(value) {
            return value[key];
        });
    };
    _.where = function(obj, attrs, first) {
        if (_.isEmpty(attrs)) return first ? void 0 : [];
        return _[first ? "find" : "filter"](obj, function(value) {
            for (var key in attrs) {
                if (attrs[key] !== value[key]) return false;
            }
            return true;
        });
    };
    _.findWhere = function(obj, attrs) {
        return _.where(obj, attrs, true);
    };
    _.max = function(obj, iterator, context) {
        if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) {
            return Math.max.apply(Math, obj);
        }
        if (!iterator && _.isEmpty(obj)) return -Infinity;
        var result = {
            computed: -Infinity,
            value: -Infinity
        };
        each(obj, function(value, index, list) {
            var computed = iterator ? iterator.call(context, value, index, list) : value;
            computed > result.computed && (result = {
                value: value,
                computed: computed
            });
        });
        return result.value;
    };
    _.min = function(obj, iterator, context) {
        if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) {
            return Math.min.apply(Math, obj);
        }
        if (!iterator && _.isEmpty(obj)) return Infinity;
        var result = {
            computed: Infinity,
            value: Infinity
        };
        each(obj, function(value, index, list) {
            var computed = iterator ? iterator.call(context, value, index, list) : value;
            computed < result.computed && (result = {
                value: value,
                computed: computed
            });
        });
        return result.value;
    };
    _.shuffle = function(obj) {
        var rand;
        var index = 0;
        var shuffled = [];
        each(obj, function(value) {
            rand = _.random(index++);
            shuffled[index - 1] = shuffled[rand];
            shuffled[rand] = value;
        });
        return shuffled;
    };
    _.sample = function(obj, n, guard) {
        if (arguments.length < 2 || guard) {
            return obj[_.random(obj.length - 1)];
        }
        return _.shuffle(obj).slice(0, Math.max(0, n));
    };
    var lookupIterator = function(value) {
        return _.isFunction(value) ? value : function(obj) {
            return obj[value];
        };
    };
    _.sortBy = function(obj, value, context) {
        var iterator = lookupIterator(value);
        return _.pluck(_.map(obj, function(value, index, list) {
            return {
                value: value,
                index: index,
                criteria: iterator.call(context, value, index, list)
            };
        }).sort(function(left, right) {
            var a = left.criteria;
            var b = right.criteria;
            if (a !== b) {
                if (a > b || a === void 0) return 1;
                if (a < b || b === void 0) return -1;
            }
            return left.index - right.index;
        }), "value");
    };
    var group = function(behavior) {
        return function(obj, value, context) {
            var result = {};
            var iterator = value == null ? _.identity : lookupIterator(value);
            each(obj, function(value, index) {
                var key = iterator.call(context, value, index, obj);
                behavior(result, key, value);
            });
            return result;
        };
    };
    _.groupBy = group(function(result, key, value) {
        (_.has(result, key) ? result[key] : result[key] = []).push(value);
    });
    _.indexBy = group(function(result, key, value) {
        result[key] = value;
    });
    _.countBy = group(function(result, key) {
        _.has(result, key) ? result[key]++ : result[key] = 1;
    });
    _.sortedIndex = function(array, obj, iterator, context) {
        iterator = iterator == null ? _.identity : lookupIterator(iterator);
        var value = iterator.call(context, obj);
        var low = 0,
            high = array.length;
        while (low < high) {
            var mid = low + high >>> 1;
            iterator.call(context, array[mid]) < value ? low = mid + 1 : high = mid;
        }
        return low;
    };
    _.toArray = function(obj) {
        if (!obj) return [];
        if (_.isArray(obj)) return slice.call(obj);
        if (obj.length === +obj.length) return _.map(obj, _.identity);
        return _.values(obj);
    };
    _.size = function(obj) {
        if (obj == null) return 0;
        return obj.length === +obj.length ? obj.length : _.keys(obj).length;
    };
    _.first = _.head = _.take = function(array, n, guard) {
        if (array == null) return void 0;
        return n == null || guard ? array[0] : slice.call(array, 0, n);
    };
    _.initial = function(array, n, guard) {
        return slice.call(array, 0, array.length - (n == null || guard ? 1 : n));
    };
    _.last = function(array, n, guard) {
        if (array == null) return void 0;
        if (n == null || guard) {
            return array[array.length - 1];
        } else {
            return slice.call(array, Math.max(array.length - n, 0));
        }
    };
    _.rest = _.tail = _.drop = function(array, n, guard) {
        return slice.call(array, n == null || guard ? 1 : n);
    };
    _.compact = function(array) {
        return _.filter(array, _.identity);
    };
    var flatten = function(input, shallow, output) {
        if (shallow && _.every(input, _.isArray)) {
            return concat.apply(output, input);
        }
        each(input, function(value) {
            if (_.isArray(value) || _.isArguments(value)) {
                shallow ? push.apply(output, value) : flatten(value, shallow, output);
            } else {
                output.push(value);
            }
        });
        return output;
    };
    _.flatten = function(array, shallow) {
        return flatten(array, shallow, []);
    };
    _.without = function(array) {
        return _.difference(array, slice.call(arguments, 1));
    };
    _.uniq = _.unique = function(array, isSorted, iterator, context) {
        if (_.isFunction(isSorted)) {
            context = iterator;
            iterator = isSorted;
            isSorted = false;
        }
        var initial = iterator ? _.map(array, iterator, context) : array;
        var results = [];
        var seen = [];
        each(initial, function(value, index) {
            if (isSorted ? !index || seen[seen.length - 1] !== value : !_.contains(seen, value)) {
                seen.push(value);
                results.push(array[index]);
            }
        });
        return results;
    };
    _.union = function() {
        return _.uniq(_.flatten(arguments, true));
    };
    _.intersection = function(array) {
        var rest = slice.call(arguments, 1);
        return _.filter(_.uniq(array), function(item) {
            return _.every(rest, function(other) {
                return _.indexOf(other, item) >= 0;
            });
        });
    };
    _.difference = function(array) {
        var rest = concat.apply(ArrayProto, slice.call(arguments, 1));
        return _.filter(array, function(value) {
            return !_.contains(rest, value);
        });
    };
    _.zip = function() {
        var length = _.max(_.pluck(arguments, "length").concat(0));
        var results = new Array(length);
        for (var i = 0; i < length; i++) {
            results[i] = _.pluck(arguments, "" + i);
        }
        return results;
    };
    _.object = function(list, values) {
        if (list == null) return {};
        var result = {};
        for (var i = 0, length = list.length; i < length; i++) {
            if (values) {
                result[list[i]] = values[i];
            } else {
                result[list[i][0]] = list[i][1];
            }
        }
        return result;
    };
    _.indexOf = function(array, item, isSorted) {
        if (array == null) return -1;
        var i = 0,
            length = array.length;
        if (isSorted) {
            if (typeof isSorted == "number") {
                i = isSorted < 0 ? Math.max(0, length + isSorted) : isSorted;
            } else {
                i = _.sortedIndex(array, item);
                return array[i] === item ? i : -1;
            }
        }
        if (nativeIndexOf && array.indexOf === nativeIndexOf) return array.indexOf(item, isSorted);
        for (; i < length; i++)
            if (array[i] === item) return i;
        return -1;
    };
    _.lastIndexOf = function(array, item, from) {
        if (array == null) return -1;
        var hasIndex = from != null;
        if (nativeLastIndexOf && array.lastIndexOf === nativeLastIndexOf) {
            return hasIndex ? array.lastIndexOf(item, from) : array.lastIndexOf(item);
        }
        var i = hasIndex ? from : array.length;
        while (i--)
            if (array[i] === item) return i;
        return -1;
    };
    _.range = function(start, stop, step) {
        if (arguments.length <= 1) {
            stop = start || 0;
            start = 0;
        }
        step = arguments[2] || 1;
        var length = Math.max(Math.ceil((stop - start) / step), 0);
        var idx = 0;
        var range = new Array(length);
        while (idx < length) {
            range[idx++] = start;
            start += step;
        }
        return range;
    };
    var ctor = function() {};
    _.bind = function(func, context) {
        var args, bound;
        if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
        if (!_.isFunction(func)) throw new TypeError();
        args = slice.call(arguments, 2);
        return bound = function() {
            if (!(this instanceof bound)) return func.apply(context, args.concat(slice.call(arguments)));
            ctor.prototype = func.prototype;
            var self = new ctor();
            ctor.prototype = null;
            var result = func.apply(self, args.concat(slice.call(arguments)));
            if (Object(result) === result) return result;
            return self;
        };
    };
    _.partial = function(func) {
        var args = slice.call(arguments, 1);
        return function() {
            return func.apply(this, args.concat(slice.call(arguments)));
        };
    };
    _.bindAll = function(obj) {
        var funcs = slice.call(arguments, 1);
        if (funcs.length === 0) throw new Error("bindAll must be passed function names");
        each(funcs, function(f) {
            obj[f] = _.bind(obj[f], obj);
        });
        return obj;
    };
    _.memoize = function(func, hasher) {
        var memo = {};
        hasher || (hasher = _.identity);
        return function() {
            var key = hasher.apply(this, arguments);
            return _.has(memo, key) ? memo[key] : memo[key] = func.apply(this, arguments);
        };
    };
    _.delay = function(func, wait) {
        var args = slice.call(arguments, 2);
        return setTimeout(function() {
            return func.apply(null, args);
        }, wait);
    };
    _.defer = function(func) {
        return _.delay.apply(_, [func, 1].concat(slice.call(arguments, 1)));
    };
    _.throttle = function(func, wait, options) {
        var context, args, result;
        var timeout = null;
        var previous = 0;
        options || (options = {});
        var later = function() {
            previous = options.leading === false ? 0 : new Date();
            timeout = null;
            result = func.apply(context, args);
        };
        return function() {
            var now = new Date();
            if (!previous && options.leading === false) previous = now;
            var remaining = wait - (now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0) {
                clearTimeout(timeout);
                timeout = null;
                previous = now;
                result = func.apply(context, args);
            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        };
    };
    _.debounce = function(func, wait, immediate) {
        var timeout, args, context, timestamp, result;
        return function() {
            context = this;
            args = arguments;
            timestamp = new Date();
            var later = function() {
                var last = new Date() - timestamp;
                if (last < wait) {
                    timeout = setTimeout(later, wait - last);
                } else {
                    timeout = null;
                    if (!immediate) result = func.apply(context, args);
                }
            };
            var callNow = immediate && !timeout;
            if (!timeout) {
                timeout = setTimeout(later, wait);
            }
            if (callNow) result = func.apply(context, args);
            return result;
        };
    };
    _.once = function(func) {
        var ran = false,
            memo;
        return function() {
            if (ran) return memo;
            ran = true;
            memo = func.apply(this, arguments);
            func = null;
            return memo;
        };
    };
    _.wrap = function(func, wrapper) {
        return function() {
            var args = [func];
            push.apply(args, arguments);
            return wrapper.apply(this, args);
        };
    };
    _.compose = function() {
        var funcs = arguments;
        return function() {
            var args = arguments;
            for (var i = funcs.length - 1; i >= 0; i--) {
                args = [funcs[i].apply(this, args)];
            }
            return args[0];
        };
    };
    _.after = function(times, func) {
        return function() {
            if (--times < 1) {
                return func.apply(this, arguments);
            }
        };
    };
    _.keys = nativeKeys || function(obj) {
        if (obj !== Object(obj)) throw new TypeError("Invalid object");
        var keys = [];
        for (var key in obj)
            if (_.has(obj, key)) keys.push(key);
        return keys;
    };
    _.values = function(obj) {
        var keys = _.keys(obj);
        var length = keys.length;
        var values = new Array(length);
        for (var i = 0; i < length; i++) {
            values[i] = obj[keys[i]];
        }
        return values;
    };
    _.pairs = function(obj) {
        var keys = _.keys(obj);
        var length = keys.length;
        var pairs = new Array(length);
        for (var i = 0; i < length; i++) {
            pairs[i] = [keys[i], obj[keys[i]]];
        }
        return pairs;
    };
    _.invert = function(obj) {
        var result = {};
        var keys = _.keys(obj);
        for (var i = 0, length = keys.length; i < length; i++) {
            result[obj[keys[i]]] = keys[i];
        }
        return result;
    };
    _.functions = _.methods = function(obj) {
        var names = [];
        for (var key in obj) {
            if (_.isFunction(obj[key])) names.push(key);
        }
        return names.sort();
    };
    _.extend = function(obj) {
        each(slice.call(arguments, 1), function(source) {
            if (source) {
                for (var prop in source) {
                    obj[prop] = source[prop];
                }
            }
        });
        return obj;
    };
    _.pick = function(obj) {
        var copy = {};
        var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
        each(keys, function(key) {
            if (key in obj) copy[key] = obj[key];
        });
        return copy;
    };
    _.omit = function(obj) {
        var copy = {};
        var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
        for (var key in obj) {
            if (!_.contains(keys, key)) copy[key] = obj[key];
        }
        return copy;
    };
    _.defaults = function(obj) {
        each(slice.call(arguments, 1), function(source) {
            if (source) {
                for (var prop in source) {
                    if (obj[prop] === void 0) obj[prop] = source[prop];
                }
            }
        });
        return obj;
    };
    _.clone = function(obj) {
        if (!_.isObject(obj)) return obj;
        return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
    };
    _.tap = function(obj, interceptor) {
        interceptor(obj);
        return obj;
    };
    var eq = function(a, b, aStack, bStack) {
        if (a === b) return a !== 0 || 1 / a == 1 / b;
        if (a == null || b == null) return a === b;
        if (a instanceof _) a = a._wrapped;
        if (b instanceof _) b = b._wrapped;
        var className = toString.call(a);
        if (className != toString.call(b)) return false;
        switch (className) {
            case "[object String]":
                return a == String(b);

            case "[object Number]":
                return a != +a ? b != +b : a == 0 ? 1 / a == 1 / b : a == +b;

            case "[object Date]":
            case "[object Boolean]":
                return +a == +b;

            case "[object RegExp]":
                return a.source == b.source && a.global == b.global && a.multiline == b.multiline && a.ignoreCase == b.ignoreCase;
        }
        if (typeof a != "object" || typeof b != "object") return false;
        var length = aStack.length;
        while (length--) {
            if (aStack[length] == a) return bStack[length] == b;
        }
        var aCtor = a.constructor,
            bCtor = b.constructor;
        if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor && _.isFunction(bCtor) && bCtor instanceof bCtor)) {
            return false;
        }
        aStack.push(a);
        bStack.push(b);
        var size = 0,
            result = true;
        if (className == "[object Array]") {
            size = a.length;
            result = size == b.length;
            if (result) {
                while (size--) {
                    if (!(result = eq(a[size], b[size], aStack, bStack))) break;
                }
            }
        } else {
            for (var key in a) {
                if (_.has(a, key)) {
                    size++;
                    if (!(result = _.has(b, key) && eq(a[key], b[key], aStack, bStack))) break;
                }
            }
            if (result) {
                for (key in b) {
                    if (_.has(b, key) && !size--) break;
                }
                result = !size;
            }
        }
        aStack.pop();
        bStack.pop();
        return result;
    };
    _.isEqual = function(a, b) {
        return eq(a, b, [], []);
    };
    _.isEmpty = function(obj) {
        if (obj == null) return true;
        if (_.isArray(obj) || _.isString(obj)) return obj.length === 0;
        for (var key in obj)
            if (_.has(obj, key)) return false;
        return true;
    };
    _.isElement = function(obj) {
        return !!(obj && obj.nodeType === 1);
    };
    _.isArray = nativeIsArray || function(obj) {
        return toString.call(obj) == "[object Array]";
    };
    _.isObject = function(obj) {
        return obj === Object(obj);
    };
    each(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function(name) {
        _["is" + name] = function(obj) {
            return toString.call(obj) == "[object " + name + "]";
        };
    });
    if (!_.isArguments(arguments)) {
        _.isArguments = function(obj) {
            return !!(obj && _.has(obj, "callee"));
        };
    }
    if (typeof /./ !== "function") {
        _.isFunction = function(obj) {
            return typeof obj === "function";
        };
    }
    _.isFinite = function(obj) {
        return isFinite(obj) && !isNaN(parseFloat(obj));
    };
    _.isNaN = function(obj) {
        return _.isNumber(obj) && obj != +obj;
    };
    _.isBoolean = function(obj) {
        return obj === true || obj === false || toString.call(obj) == "[object Boolean]";
    };
    _.isNull = function(obj) {
        return obj === null;
    };
    _.isUndefined = function(obj) {
        return obj === void 0;
    };
    _.has = function(obj, key) {
        return hasOwnProperty.call(obj, key);
    };
    _.noConflict = function() {
        root._ = previousUnderscore;
        return this;
    };
    _.identity = function(value) {
        return value;
    };
    _.times = function(n, iterator, context) {
        var accum = Array(Math.max(0, n));
        for (var i = 0; i < n; i++) accum[i] = iterator.call(context, i);
        return accum;
    };
    _.random = function(min, max) {
        if (max == null) {
            max = min;
            min = 0;
        }
        return min + Math.floor(Math.random() * (max - min + 1));
    };
    var entityMap = {
        escape: {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;"
        }
    };
    entityMap.unescape = _.invert(entityMap.escape);
    var entityRegexes = {
        escape: new RegExp("[" + _.keys(entityMap.escape).join("") + "]", "g"),
        unescape: new RegExp("(" + _.keys(entityMap.unescape).join("|") + ")", "g")
    };
    _.each(["escape", "unescape"], function(method) {
        _[method] = function(string) {
            if (string == null) return "";
            return ("" + string).replace(entityRegexes[method], function(match) {
                return entityMap[method][match];
            });
        };
    });
    _.result = function(object, property) {
        if (object == null) return void 0;
        var value = object[property];
        return _.isFunction(value) ? value.call(object) : value;
    };
    _.mixin = function(obj) {
        each(_.functions(obj), function(name) {
            var func = _[name] = obj[name];
            _.prototype[name] = function() {
                var args = [this._wrapped];
                push.apply(args, arguments);
                return result.call(this, func.apply(_, args));
            };
        });
    };
    var idCounter = 0;
    _.uniqueId = function(prefix) {
        var id = ++idCounter + "";
        return prefix ? prefix + id : id;
    };
    _.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var noMatch = /(.)^/;
    var escapes = {
        "'": "'",
        "\\": "\\",
        "\r": "r",
        "\n": "n",
        "	": "t",
        "\u2028": "u2028",
        "\u2029": "u2029"
    };
    var escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    _.template = function(text, data, settings) {
        var render;
        settings = _.defaults({}, settings, _.templateSettings);
        var matcher = new RegExp([(settings.escape || noMatch).source, (settings.interpolate || noMatch).source, (settings.evaluate || noMatch).source].join("|") + "|$", "g");
        var index = 0;
        var source = "__p+='";
        text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
            source += text.slice(index, offset).replace(escaper, function(match) {
                return "\\" + escapes[match];
            });
            if (escape) {
                source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
            }
            if (interpolate) {
                source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
            }
            if (evaluate) {
                source += "';\n" + evaluate + "\n__p+='";
            }
            index = offset + match.length;
            return match;
        });
        source += "';\n";
        if (!settings.variable) source = "with(obj||{}){\n" + source + "}\n";
        source = "var __t,__p='',__j=Array.prototype.join," + "print=function(){__p+=__j.call(arguments,'');};\n" + source + "return __p;\n";
        try {
            render = new Function(settings.variable || "obj", "_", source);
        } catch (e) {
            e.source = source;
            throw e;
        }
        if (data) return render(data, _);
        var template = function(data) {
            return render.call(this, data, _);
        };
        template.source = "function(" + (settings.variable || "obj") + "){\n" + source + "}";
        return template;
    };
    _.chain = function(obj) {
        return _(obj).chain();
    };
    var result = function(obj) {
        return this._chain ? _(obj).chain() : obj;
    };
    _.mixin(_);
    each(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(name) {
        var method = ArrayProto[name];
        _.prototype[name] = function() {
            var obj = this._wrapped;
            method.apply(obj, arguments);
            if ((name == "shift" || name == "splice") && obj.length === 0) delete obj[0];
            return result.call(this, obj);
        };
    });
    each(["concat", "join", "slice"], function(name) {
        var method = ArrayProto[name];
        _.prototype[name] = function() {
            return result.call(this, method.apply(this._wrapped, arguments));
        };
    });
    _.extend(_.prototype, {
        chain: function() {
            this._chain = true;
            return this;
        },
        value: function() {
            return this._wrapped;
        }
    });
}).call(this);

define("underscore", function(global) {
    return function() {
        var ret, fn;
        return ret || global._;
    };
}(this));

(function(window, Object, Array, Error, JSON, undefined) {
    var partialComplete = varArgs(function(fn, args) {
            var numBoundArgs = args.length;
            return varArgs(function(callArgs) {
                for (var i = 0; i < callArgs.length; i++) {
                    args[numBoundArgs + i] = callArgs[i];
                }
                args.length = numBoundArgs + callArgs.length;
                return fn.apply(this, args);
            });
        }),
        compose = varArgs(function(fns) {
            var fnsList = arrayAsList(fns);

            function next(params, curFn) {
                return [apply(params, curFn)];
            }
            return varArgs(function(startParams) {
                return foldR(next, startParams, fnsList)[0];
            });
        });

    function compose2(f1, f2) {
        return function() {
            return f1.call(this, f2.apply(this, arguments));
        };
    }

    function attr(key) {
        return function(o) {
            return o[key];
        };
    }
    var lazyUnion = varArgs(function(fns) {
        return varArgs(function(params) {
            var maybeValue;
            for (var i = 0; i < len(fns); i++) {
                maybeValue = apply(params, fns[i]);
                if (maybeValue) {
                    return maybeValue;
                }
            }
        });
    });

    function apply(args, fn) {
        return fn.apply(undefined, args);
    }

    function varArgs(fn) {
        var numberOfFixedArguments = fn.length - 1,
            slice = Array.prototype.slice;
        if (numberOfFixedArguments == 0) {
            return function() {
                return fn.call(this, slice.call(arguments));
            };
        } else if (numberOfFixedArguments == 1) {
            return function() {
                return fn.call(this, arguments[0], slice.call(arguments, 1));
            };
        }
        var argsHolder = Array(fn.length);
        return function() {
            for (var i = 0; i < numberOfFixedArguments; i++) {
                argsHolder[i] = arguments[i];
            }
            argsHolder[numberOfFixedArguments] = slice.call(arguments, numberOfFixedArguments);
            return fn.apply(this, argsHolder);
        };
    }

    function flip(fn) {
        return function(a, b) {
            return fn(b, a);
        };
    }

    function lazyIntersection(fn1, fn2) {
        return function(param) {
            return fn1(param) && fn2(param);
        };
    }

    function noop() {}

    function always() {
        return true;
    }

    function functor(val) {
        return function() {
            return val;
        };
    }

    function isOfType(T, maybeSomething) {
        return maybeSomething && maybeSomething.constructor === T;
    }
    var len = attr("length"),
        isString = partialComplete(isOfType, String);

    function defined(value) {
        return value !== undefined;
    }

    function hasAllProperties(fieldList, o) {
        return o instanceof Object && all(function(field) {
            return field in o;
        }, fieldList);
    }

    function cons(x, xs) {
        return [x, xs];
    }
    var emptyList = null,
        head = attr(0),
        tail = attr(1);

    function arrayAsList(inputArray) {
        return reverseList(inputArray.reduce(flip(cons), emptyList));
    }
    var list = varArgs(arrayAsList);

    function listAsArray(list) {
        return foldR(function(arraySoFar, listItem) {
            arraySoFar.unshift(listItem);
            return arraySoFar;
        }, [], list);
    }

    function map(fn, list) {
        return list ? cons(fn(head(list)), map(fn, tail(list))) : emptyList;
    }

    function foldR(fn, startValue, list) {
        return list ? fn(foldR(fn, startValue, tail(list)), head(list)) : startValue;
    }

    function foldR1(fn, list) {
        return tail(list) ? fn(foldR1(fn, tail(list)), head(list)) : head(list);
    }

    function without(list, test, removedFn) {
        return withoutInner(list, removedFn || noop);

        function withoutInner(subList, removedFn) {
            return subList ? test(head(subList)) ? (removedFn(head(subList)), tail(subList)) : cons(head(subList), withoutInner(tail(subList), removedFn)) : emptyList;
        }
    }

    function all(fn, list) {
        return !list || fn(head(list)) && all(fn, tail(list));
    }

    function applyEach(fnList, args) {
        if (fnList) {
            head(fnList).apply(null, args);
            applyEach(tail(fnList), args);
        }
    }

    function reverseList(list) {
        function reverseInner(list, reversedAlready) {
            if (!list) {
                return reversedAlready;
            }
            return reverseInner(tail(list), cons(head(list), reversedAlready));
        }
        return reverseInner(list, emptyList);
    }

    function first(test, list) {
        return list && (test(head(list)) ? head(list) : first(test, tail(list)));
    }

    function clarinet(eventBus) {
        "use strict";
        var emitSaxKey = eventBus(SAX_KEY).emit,
            emitValueOpen = eventBus(SAX_VALUE_OPEN).emit,
            emitValueClose = eventBus(SAX_VALUE_CLOSE).emit,
            emitFail = eventBus(FAIL_EVENT).emit,
            MAX_BUFFER_LENGTH = 64 * 1024,
            stringTokenPattern = /[\\"\n]/g,
            _n = 0,
            BEGIN = _n++,
            VALUE = _n++,
            OPEN_OBJECT = _n++,
            CLOSE_OBJECT = _n++,
            OPEN_ARRAY = _n++,
            CLOSE_ARRAY = _n++,
            STRING = _n++,
            OPEN_KEY = _n++,
            CLOSE_KEY = _n++,
            TRUE = _n++,
            TRUE2 = _n++,
            TRUE3 = _n++,
            FALSE = _n++,
            FALSE2 = _n++,
            FALSE3 = _n++,
            FALSE4 = _n++,
            NULL = _n++,
            NULL2 = _n++,
            NULL3 = _n++,
            NUMBER_DECIMAL_POINT = _n++,
            NUMBER_DIGIT = _n,
            bufferCheckPosition = MAX_BUFFER_LENGTH,
            latestError, c, p, textNode = "",
            numberNode = "",
            slashed = false,
            closed = false,
            state = BEGIN,
            stack = [],
            unicodeS = null,
            unicodeI = 0,
            depth = 0,
            position = 0,
            column = 0,
            line = 1;

        function checkBufferLength() {
            var maxActual = 0;
            if (textNode.length > MAX_BUFFER_LENGTH) {
                emitError("Max buffer length exceeded: textNode");
                maxActual = Math.max(maxActual, textNode.length);
            }
            if (numberNode.length > MAX_BUFFER_LENGTH) {
                emitError("Max buffer length exceeded: numberNode");
                maxActual = Math.max(maxActual, numberNode.length);
            }
            bufferCheckPosition = MAX_BUFFER_LENGTH - maxActual + position;
        }
        eventBus(STREAM_DATA).on(handleData);
        eventBus(STREAM_END).on(handleStreamEnd);

        function emitError(errorString) {
            if (textNode) {
                emitValueOpen(textNode);
                emitValueClose();
                textNode = "";
            }
            latestError = Error(errorString + "\nLn: " + line + "\nCol: " + column + "\nChr: " + c);
            emitFail(errorReport(undefined, undefined, latestError));
        }

        function handleStreamEnd() {
            if (state == BEGIN) {
                emitValueOpen({});
                emitValueClose();
                closed = true;
                return;
            }
            if (state !== VALUE || depth !== 0) emitError("Unexpected end");
            if (textNode) {
                emitValueOpen(textNode);
                emitValueClose();
                textNode = "";
            }
            closed = true;
        }

        function whitespace(c) {
            return c == "\r" || c == "\n" || c == " " || c == "	";
        }

        function handleData(chunk) {
            if (latestError) return;
            if (closed) {
                return emitError("Cannot write after close");
            }
            var i = 0;
            c = chunk[0];
            while (c) {
                p = c;
                c = chunk[i++];
                if (!c) break;
                position++;
                if (c == "\n") {
                    line++;
                    column = 0;
                } else column++;
                switch (state) {
                    case BEGIN:
                        if (c === "{") state = OPEN_OBJECT;
                        else if (c === "[") state = OPEN_ARRAY;
                        else if (!whitespace(c)) return emitError("Non-whitespace before {[.");
                        continue;

                    case OPEN_KEY:
                    case OPEN_OBJECT:
                        if (whitespace(c)) continue;
                        if (state === OPEN_KEY) stack.push(CLOSE_KEY);
                        else {
                            if (c === "}") {
                                emitValueOpen({});
                                emitValueClose();
                                state = stack.pop() || VALUE;
                                continue;
                            } else stack.push(CLOSE_OBJECT);
                        }
                        if (c === '"') state = STRING;
                        else return emitError('Malformed object key should start with " ');
                        continue;

                    case CLOSE_KEY:
                    case CLOSE_OBJECT:
                        if (whitespace(c)) continue;
                        if (c === ":") {
                            if (state === CLOSE_OBJECT) {
                                stack.push(CLOSE_OBJECT);
                                if (textNode) {
                                    emitValueOpen({});
                                    emitSaxKey(textNode);
                                    textNode = "";
                                }
                                depth++;
                            } else {
                                if (textNode) {
                                    emitSaxKey(textNode);
                                    textNode = "";
                                }
                            }
                            state = VALUE;
                        } else if (c === "}") {
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            emitValueClose();
                            depth--;
                            state = stack.pop() || VALUE;
                        } else if (c === ",") {
                            if (state === CLOSE_OBJECT) stack.push(CLOSE_OBJECT);
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            state = OPEN_KEY;
                        } else return emitError("Bad object");
                        continue;

                    case OPEN_ARRAY:
                    case VALUE:
                        if (whitespace(c)) continue;
                        if (state === OPEN_ARRAY) {
                            emitValueOpen([]);
                            depth++;
                            state = VALUE;
                            if (c === "]") {
                                emitValueClose();
                                depth--;
                                state = stack.pop() || VALUE;
                                continue;
                            } else {
                                stack.push(CLOSE_ARRAY);
                            }
                        }
                        if (c === '"') state = STRING;
                        else if (c === "{") state = OPEN_OBJECT;
                        else if (c === "[") state = OPEN_ARRAY;
                        else if (c === "t") state = TRUE;
                        else if (c === "f") state = FALSE;
                        else if (c === "n") state = NULL;
                        else if (c === "-") {
                            numberNode += c;
                        } else if (c === "0") {
                            numberNode += c;
                            state = NUMBER_DIGIT;
                        } else if ("123456789".indexOf(c) !== -1) {
                            numberNode += c;
                            state = NUMBER_DIGIT;
                        } else return emitError("Bad value");
                        continue;

                    case CLOSE_ARRAY:
                        if (c === ",") {
                            stack.push(CLOSE_ARRAY);
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            state = VALUE;
                        } else if (c === "]") {
                            if (textNode) {
                                emitValueOpen(textNode);
                                emitValueClose();
                                textNode = "";
                            }
                            emitValueClose();
                            depth--;
                            state = stack.pop() || VALUE;
                        } else if (whitespace(c)) continue;
                        else return emitError("Bad array");
                        continue;

                    case STRING:
                        var starti = i - 1;
                        STRING_BIGLOOP: while (true) {
                            while (unicodeI > 0) {
                                unicodeS += c;
                                c = chunk.charAt(i++);
                                if (unicodeI === 4) {
                                    textNode += String.fromCharCode(parseInt(unicodeS, 16));
                                    unicodeI = 0;
                                    starti = i - 1;
                                } else {
                                    unicodeI++;
                                }
                                if (!c) break STRING_BIGLOOP;
                            }
                            if (c === '"' && !slashed) {
                                state = stack.pop() || VALUE;
                                textNode += chunk.substring(starti, i - 1);
                                if (!textNode) {
                                    emitValueOpen("");
                                    emitValueClose();
                                }
                                break;
                            }
                            if (c === "\\" && !slashed) {
                                slashed = true;
                                textNode += chunk.substring(starti, i - 1);
                                c = chunk.charAt(i++);
                                if (!c) break;
                            }
                            if (slashed) {
                                slashed = false;
                                if (c === "n") {
                                    textNode += "\n";
                                } else if (c === "r") {
                                    textNode += "\r";
                                } else if (c === "t") {
                                    textNode += "	";
                                } else if (c === "f") {
                                    textNode += "\f";
                                } else if (c === "b") {
                                    textNode += "\b";
                                } else if (c === "u") {
                                    unicodeI = 1;
                                    unicodeS = "";
                                } else {
                                    textNode += c;
                                }
                                c = chunk.charAt(i++);
                                starti = i - 1;
                                if (!c) break;
                                else continue;
                            }
                            stringTokenPattern.lastIndex = i;
                            var reResult = stringTokenPattern.exec(chunk);
                            if (!reResult) {
                                i = chunk.length + 1;
                                textNode += chunk.substring(starti, i - 1);
                                break;
                            }
                            i = reResult.index + 1;
                            c = chunk.charAt(reResult.index);
                            if (!c) {
                                textNode += chunk.substring(starti, i - 1);
                                break;
                            }
                        }
                        continue;

                    case TRUE:
                        if (!c) continue;
                        if (c === "r") state = TRUE2;
                        else return emitError("Invalid true started with t" + c);
                        continue;

                    case TRUE2:
                        if (!c) continue;
                        if (c === "u") state = TRUE3;
                        else return emitError("Invalid true started with tr" + c);
                        continue;

                    case TRUE3:
                        if (!c) continue;
                        if (c === "e") {
                            emitValueOpen(true);
                            emitValueClose();
                            state = stack.pop() || VALUE;
                        } else return emitError("Invalid true started with tru" + c);
                        continue;

                    case FALSE:
                        if (!c) continue;
                        if (c === "a") state = FALSE2;
                        else return emitError("Invalid false started with f" + c);
                        continue;

                    case FALSE2:
                        if (!c) continue;
                        if (c === "l") state = FALSE3;
                        else return emitError("Invalid false started with fa" + c);
                        continue;

                    case FALSE3:
                        if (!c) continue;
                        if (c === "s") state = FALSE4;
                        else return emitError("Invalid false started with fal" + c);
                        continue;

                    case FALSE4:
                        if (!c) continue;
                        if (c === "e") {
                            emitValueOpen(false);
                            emitValueClose();
                            state = stack.pop() || VALUE;
                        } else return emitError("Invalid false started with fals" + c);
                        continue;

                    case NULL:
                        if (!c) continue;
                        if (c === "u") state = NULL2;
                        else return emitError("Invalid null started with n" + c);
                        continue;

                    case NULL2:
                        if (!c) continue;
                        if (c === "l") state = NULL3;
                        else return emitError("Invalid null started with nu" + c);
                        continue;

                    case NULL3:
                        if (!c) continue;
                        if (c === "l") {
                            emitValueOpen(null);
                            emitValueClose();
                            state = stack.pop() || VALUE;
                        } else return emitError("Invalid null started with nul" + c);
                        continue;

                    case NUMBER_DECIMAL_POINT:
                        if (c === ".") {
                            numberNode += c;
                            state = NUMBER_DIGIT;
                        } else return emitError("Leading zero not followed by .");
                        continue;

                    case NUMBER_DIGIT:
                        if ("0123456789".indexOf(c) !== -1) numberNode += c;
                        else if (c === ".") {
                            if (numberNode.indexOf(".") !== -1) return emitError("Invalid number has two dots");
                            numberNode += c;
                        } else if (c === "e" || c === "E") {
                            if (numberNode.indexOf("e") !== -1 || numberNode.indexOf("E") !== -1) return emitError("Invalid number has two exponential");
                            numberNode += c;
                        } else if (c === "+" || c === "-") {
                            if (!(p === "e" || p === "E")) return emitError("Invalid symbol in number");
                            numberNode += c;
                        } else {
                            if (numberNode) {
                                emitValueOpen(parseFloat(numberNode));
                                emitValueClose();
                                numberNode = "";
                            }
                            i--;
                            state = stack.pop() || VALUE;
                        }
                        continue;

                    default:
                        return emitError("Unknown state: " + state);
                }
            }
            if (position >= bufferCheckPosition) checkBufferLength();
        }
    }

    function ascentManager(oboeBus, handlers) {
        "use strict";
        var listenerId = {},
            ascent;

        function stateAfter(handler) {
            return function(param) {
                ascent = handler(ascent, param);
            };
        }
        for (var eventName in handlers) {
            oboeBus(eventName).on(stateAfter(handlers[eventName]), listenerId);
        }
        oboeBus(NODE_SWAP).on(function(newNode) {
            var oldHead = head(ascent),
                key = keyOf(oldHead),
                ancestors = tail(ascent),
                parentNode;
            if (ancestors) {
                parentNode = nodeOf(head(ancestors));
                parentNode[key] = newNode;
            }
        });
        oboeBus(NODE_DROP).on(function() {
            var oldHead = head(ascent),
                key = keyOf(oldHead),
                ancestors = tail(ascent),
                parentNode;
            if (ancestors) {
                parentNode = nodeOf(head(ancestors));
                delete parentNode[key];
            }
        });
        oboeBus(ABORTING).on(function() {
            for (var eventName in handlers) {
                oboeBus(eventName).un(listenerId);
            }
        });
    }

    function parseResponseHeaders(headerStr) {
        var headers = {};
        headerStr && headerStr.split("\r\n").forEach(function(headerPair) {
            var index = headerPair.indexOf(": ");
            headers[headerPair.substring(0, index)] = headerPair.substring(index + 2);
        });
        return headers;
    }

    function isCrossOrigin(pageLocation, ajaxHost) {
        function defaultPort(protocol) {
            return {
                "http:": 80,
                "https:": 443
            }[protocol];
        }

        function portOf(location) {
            return location.port || defaultPort(location.protocol || pageLocation.protocol);
        }
        return !!(ajaxHost.protocol && ajaxHost.protocol != pageLocation.protocol || ajaxHost.host && ajaxHost.host != pageLocation.host || ajaxHost.host && portOf(ajaxHost) != portOf(pageLocation));
    }

    function parseUrlOrigin(url) {
        var URL_HOST_PATTERN = /(\w+:)?(?:\/\/)([\w.-]+)?(?::(\d+))?\/?/,
            urlHostMatch = URL_HOST_PATTERN.exec(url) || [];
        return {
            protocol: urlHostMatch[1] || "",
            host: urlHostMatch[2] || "",
            port: urlHostMatch[3] || ""
        };
    }

    function httpTransport() {
        return new XMLHttpRequest();
    }

    function streamingHttp(oboeBus, xhr, method, url, data, headers, withCredentials) {
        "use strict";
        var emitStreamData = oboeBus(STREAM_DATA).emit,
            emitFail = oboeBus(FAIL_EVENT).emit,
            numberOfCharsAlreadyGivenToCallback = 0,
            stillToSendStartEvent = true;
        oboeBus(ABORTING).on(function() {
            xhr.onreadystatechange = null;
            xhr.abort();
        });

        function handleProgress() {
            var textSoFar = xhr.responseText,
                newText = textSoFar.substr(numberOfCharsAlreadyGivenToCallback);
            if (newText) {
                emitStreamData(newText);
            }
            numberOfCharsAlreadyGivenToCallback = len(textSoFar);
        }
        if ("onprogress" in xhr) {
            xhr.onprogress = handleProgress;
        }
        xhr.onreadystatechange = function() {
            function sendStartIfNotAlready() {
                try {
                    stillToSendStartEvent && oboeBus(HTTP_START).emit(xhr.status, parseResponseHeaders(xhr.getAllResponseHeaders()));
                    stillToSendStartEvent = false;
                } catch (e) {}
            }
            switch (xhr.readyState) {
                case 2:
                case 3:
                    return sendStartIfNotAlready();

                case 4:
                    sendStartIfNotAlready();
                    var successful = String(xhr.status)[0] == 2;
                    if (successful) {
                        handleProgress();
                        oboeBus(STREAM_END).emit();
                    } else {
                        emitFail(errorReport(xhr.status, xhr.responseText));
                    }
            }
        };
        try {
            xhr.open(method, url, true);
            for (var headerName in headers) {
                xhr.setRequestHeader(headerName, headers[headerName]);
            }
            if (!isCrossOrigin(window.location, parseUrlOrigin(url))) {
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            }
            xhr.withCredentials = withCredentials;
            xhr.send(data);
        } catch (e) {
            window.setTimeout(partialComplete(emitFail, errorReport(undefined, undefined, e)), 0);
        }
    }
    var jsonPathSyntax = function() {
        var regexDescriptor = function regexDescriptor(regex) {
                return regex.exec.bind(regex);
            },
            jsonPathClause = varArgs(function(componentRegexes) {
                componentRegexes.unshift(/^/);
                return regexDescriptor(RegExp(componentRegexes.map(attr("source")).join("")));
            }),
            possiblyCapturing = /(\$?)/,
            namedNode = /([\w-_]+|\*)/,
            namePlaceholder = /()/,
            nodeInArrayNotation = /\["([^"]+)"\]/,
            numberedNodeInArrayNotation = /\[(\d+|\*)\]/,
            fieldList = /{([\w ]*?)}/,
            optionalFieldList = /(?:{([\w ]*?)})?/,
            jsonPathNamedNodeInObjectNotation = jsonPathClause(possiblyCapturing, namedNode, optionalFieldList),
            jsonPathNamedNodeInArrayNotation = jsonPathClause(possiblyCapturing, nodeInArrayNotation, optionalFieldList),
            jsonPathNumberedNodeInArrayNotation = jsonPathClause(possiblyCapturing, numberedNodeInArrayNotation, optionalFieldList),
            jsonPathPureDuckTyping = jsonPathClause(possiblyCapturing, namePlaceholder, fieldList),
            jsonPathDoubleDot = jsonPathClause(/\.\./),
            jsonPathDot = jsonPathClause(/\./),
            jsonPathBang = jsonPathClause(possiblyCapturing, /!/),
            emptyString = jsonPathClause(/$/);
        return function(fn) {
            return fn(lazyUnion(jsonPathNamedNodeInObjectNotation, jsonPathNamedNodeInArrayNotation, jsonPathNumberedNodeInArrayNotation, jsonPathPureDuckTyping), jsonPathDoubleDot, jsonPathDot, jsonPathBang, emptyString);
        };
    }();

    function namedNode(key, node) {
        return {
            key: key,
            node: node
        };
    }
    var keyOf = attr("key");
    var nodeOf = attr("node");
    var ROOT_PATH = {};

    function incrementalContentBuilder(oboeBus) {
        var emitNodeOpened = oboeBus(NODE_OPENED).emit,
            emitNodeClosed = oboeBus(NODE_CLOSED).emit,
            emitRootOpened = oboeBus(ROOT_PATH_FOUND).emit,
            emitRootClosed = oboeBus(ROOT_NODE_FOUND).emit;

        function arrayIndicesAreKeys(possiblyInconsistentAscent, newDeepestNode) {
            var parentNode = nodeOf(head(possiblyInconsistentAscent));
            return isOfType(Array, parentNode) ? keyFound(possiblyInconsistentAscent, len(parentNode), newDeepestNode) : possiblyInconsistentAscent;
        }

        function nodeOpened(ascent, newDeepestNode) {
            if (!ascent) {
                emitRootOpened(newDeepestNode);
                return keyFound(ascent, ROOT_PATH, newDeepestNode);
            }
            var arrayConsistentAscent = arrayIndicesAreKeys(ascent, newDeepestNode),
                ancestorBranches = tail(arrayConsistentAscent),
                previouslyUnmappedName = keyOf(head(arrayConsistentAscent));
            appendBuiltContent(ancestorBranches, previouslyUnmappedName, newDeepestNode);
            return cons(namedNode(previouslyUnmappedName, newDeepestNode), ancestorBranches);
        }

        function appendBuiltContent(ancestorBranches, key, node) {
            nodeOf(head(ancestorBranches))[key] = node;
        }

        function keyFound(ascent, newDeepestName, maybeNewDeepestNode) {
            if (ascent) {
                appendBuiltContent(ascent, newDeepestName, maybeNewDeepestNode);
            }
            var ascentWithNewPath = cons(namedNode(newDeepestName, maybeNewDeepestNode), ascent);
            emitNodeOpened(ascentWithNewPath);
            return ascentWithNewPath;
        }

        function nodeClosed(ascent) {
            emitNodeClosed(ascent);
            return tail(ascent) || emitRootClosed(nodeOf(head(ascent)));
        }
        var contentBuilderHandlers = {};
        contentBuilderHandlers[SAX_VALUE_OPEN] = nodeOpened;
        contentBuilderHandlers[SAX_VALUE_CLOSE] = nodeClosed;
        contentBuilderHandlers[SAX_KEY] = keyFound;
        return contentBuilderHandlers;
    }
    var jsonPathCompiler = jsonPathSyntax(function(pathNodeSyntax, doubleDotSyntax, dotSyntax, bangSyntax, emptySyntax) {
        var CAPTURING_INDEX = 1;
        var NAME_INDEX = 2;
        var FIELD_LIST_INDEX = 3;
        var headKey = compose2(keyOf, head),
            headNode = compose2(nodeOf, head);

        function nameClause(previousExpr, detection) {
            var name = detection[NAME_INDEX],
                matchesName = !name || name == "*" ? always : function(ascent) {
                    return headKey(ascent) == name;
                };
            return lazyIntersection(matchesName, previousExpr);
        }

        function duckTypeClause(previousExpr, detection) {
            var fieldListStr = detection[FIELD_LIST_INDEX];
            if (!fieldListStr) return previousExpr;
            var hasAllrequiredFields = partialComplete(hasAllProperties, arrayAsList(fieldListStr.split(/\W+/))),
                isMatch = compose2(hasAllrequiredFields, headNode);
            return lazyIntersection(isMatch, previousExpr);
        }

        function capture(previousExpr, detection) {
            var capturing = !!detection[CAPTURING_INDEX];
            if (!capturing) return previousExpr;
            return lazyIntersection(previousExpr, head);
        }

        function skip1(previousExpr) {
            if (previousExpr == always) {
                return always;
            }

            function notAtRoot(ascent) {
                return headKey(ascent) != ROOT_PATH;
            }
            return lazyIntersection(notAtRoot, compose2(previousExpr, tail));
        }

        function skipMany(previousExpr) {
            if (previousExpr == always) {
                return always;
            }
            var terminalCaseWhenArrivingAtRoot = rootExpr(),
                terminalCaseWhenPreviousExpressionIsSatisfied = previousExpr,
                recursiveCase = skip1(function(ascent) {
                    return cases(ascent);
                }),
                cases = lazyUnion(terminalCaseWhenArrivingAtRoot, terminalCaseWhenPreviousExpressionIsSatisfied, recursiveCase);
            return cases;
        }

        function rootExpr() {
            return function(ascent) {
                return headKey(ascent) == ROOT_PATH;
            };
        }

        function statementExpr(lastClause) {
            return function(ascent) {
                var exprMatch = lastClause(ascent);
                return exprMatch === true ? head(ascent) : exprMatch;
            };
        }

        function expressionsReader(exprs, parserGeneratedSoFar, detection) {
            return foldR(function(parserGeneratedSoFar, expr) {
                return expr(parserGeneratedSoFar, detection);
            }, parserGeneratedSoFar, exprs);
        }

        function generateClauseReaderIfTokenFound(tokenDetector, clauseEvaluatorGenerators, jsonPath, parserGeneratedSoFar, onSuccess) {
            var detected = tokenDetector(jsonPath);
            if (detected) {
                var compiledParser = expressionsReader(clauseEvaluatorGenerators, parserGeneratedSoFar, detected),
                    remainingUnparsedJsonPath = jsonPath.substr(len(detected[0]));
                return onSuccess(remainingUnparsedJsonPath, compiledParser);
            }
        }

        function clauseMatcher(tokenDetector, exprs) {
            return partialComplete(generateClauseReaderIfTokenFound, tokenDetector, exprs);
        }
        var clauseForJsonPath = lazyUnion(clauseMatcher(pathNodeSyntax, list(capture, duckTypeClause, nameClause, skip1)), clauseMatcher(doubleDotSyntax, list(skipMany)), clauseMatcher(dotSyntax, list()), clauseMatcher(bangSyntax, list(capture, rootExpr)), clauseMatcher(emptySyntax, list(statementExpr)), function(jsonPath) {
            throw Error('"' + jsonPath + '" could not be tokenised');
        });

        function returnFoundParser(_remainingJsonPath, compiledParser) {
            return compiledParser;
        }

        function compileJsonPathToFunction(uncompiledJsonPath, parserGeneratedSoFar) {
            var onFind = uncompiledJsonPath ? compileJsonPathToFunction : returnFoundParser;
            return clauseForJsonPath(uncompiledJsonPath, parserGeneratedSoFar, onFind);
        }
        return function(jsonPath) {
            try {
                return compileJsonPathToFunction(jsonPath, always);
            } catch (e) {
                throw Error('Could not compile "' + jsonPath + '" because ' + e.message);
            }
        };
    });

    function singleEventPubSub(eventType, newListener, removeListener) {
        var listenerTupleList, listenerList;

        function hasId(id) {
            return function(tuple) {
                return tuple.id == id;
            };
        }
        return {
            on: function(listener, listenerId) {
                var tuple = {
                    listener: listener,
                    id: listenerId || listener
                };
                if (newListener) {
                    newListener.emit(eventType, listener, tuple.id);
                }
                listenerTupleList = cons(tuple, listenerTupleList);
                listenerList = cons(listener, listenerList);
                return this;
            },
            emit: function() {
                applyEach(listenerList, arguments);
            },
            un: function(listenerId) {
                var removed;
                listenerTupleList = without(listenerTupleList, hasId(listenerId), function(tuple) {
                    removed = tuple;
                });
                if (removed) {
                    listenerList = without(listenerList, function(listener) {
                        return listener == removed.listener;
                    });
                    if (removeListener) {
                        removeListener.emit(eventType, removed.listener, removed.id);
                    }
                }
            },
            listeners: function() {
                return listenerList;
            },
            hasListener: function(listenerId) {
                var test = listenerId ? hasId(listenerId) : always;
                return defined(first(test, listenerTupleList));
            }
        };
    }

    function pubSub() {
        var singles = {},
            newListener = newSingle("newListener"),
            removeListener = newSingle("removeListener");

        function newSingle(eventName) {
            return singles[eventName] = singleEventPubSub(eventName, newListener, removeListener);
        }

        function pubSubInstance(eventName) {
            return singles[eventName] || newSingle(eventName);
        }
        ["emit", "on", "un"].forEach(function(methodName) {
            pubSubInstance[methodName] = varArgs(function(eventName, parameters) {
                apply(parameters, pubSubInstance(eventName)[methodName]);
            });
        });
        return pubSubInstance;
    }
    var _S = 1,
        NODE_OPENED = _S++,
        NODE_CLOSED = _S++,
        NODE_SWAP = _S++,
        NODE_DROP = _S++,
        FAIL_EVENT = "fail",
        ROOT_NODE_FOUND = _S++,
        ROOT_PATH_FOUND = _S++,
        HTTP_START = "start",
        STREAM_DATA = "data",
        STREAM_END = "end",
        ABORTING = _S++,
        SAX_KEY = _S++,
        SAX_VALUE_OPEN = _S++,
        SAX_VALUE_CLOSE = _S++;

    function errorReport(statusCode, body, error) {
        try {
            var jsonBody = JSON.parse(body);
        } catch (e) {}
        return {
            statusCode: statusCode,
            body: body,
            jsonBody: jsonBody,
            thrown: error
        };
    }

    function patternAdapter(oboeBus, jsonPathCompiler) {
        var predicateEventMap = {
            node: oboeBus(NODE_CLOSED),
            path: oboeBus(NODE_OPENED)
        };

        function emitMatchingNode(emitMatch, node, ascent) {
            var descent = reverseList(ascent);
            emitMatch(node, listAsArray(tail(map(keyOf, descent))), listAsArray(map(nodeOf, descent)));
        }

        function addUnderlyingListener(fullEventName, predicateEvent, compiledJsonPath) {
            var emitMatch = oboeBus(fullEventName).emit;
            predicateEvent.on(function(ascent) {
                var maybeMatchingMapping = compiledJsonPath(ascent);
                if (maybeMatchingMapping !== false) {
                    emitMatchingNode(emitMatch, nodeOf(maybeMatchingMapping), ascent);
                }
            }, fullEventName);
            oboeBus("removeListener").on(function(removedEventName) {
                if (removedEventName == fullEventName) {
                    if (!oboeBus(removedEventName).listeners()) {
                        predicateEvent.un(fullEventName);
                    }
                }
            });
        }
        oboeBus("newListener").on(function(fullEventName) {
            var match = /(node|path):(.*)/.exec(fullEventName);
            if (match) {
                var predicateEvent = predicateEventMap[match[1]];
                if (!predicateEvent.hasListener(fullEventName)) {
                    addUnderlyingListener(fullEventName, predicateEvent, jsonPathCompiler(match[2]));
                }
            }
        });
    }

    function instanceApi(oboeBus, contentSource) {
        var oboeApi, fullyQualifiedNamePattern = /^(node|path):./,
            rootNodeFinishedEvent = oboeBus(ROOT_NODE_FOUND),
            emitNodeDrop = oboeBus(NODE_DROP).emit,
            emitNodeSwap = oboeBus(NODE_SWAP).emit,
            addListener = varArgs(function(eventId, parameters) {
                if (oboeApi[eventId]) {
                    apply(parameters, oboeApi[eventId]);
                } else {
                    var event = oboeBus(eventId),
                        listener = parameters[0];
                    if (fullyQualifiedNamePattern.test(eventId)) {
                        addForgettableCallback(event, listener);
                    } else {
                        event.on(listener);
                    }
                }
                return oboeApi;
            }),
            removeListener = function(eventId, p2, p3) {
                if (eventId == "done") {
                    rootNodeFinishedEvent.un(p2);
                } else if (eventId == "node" || eventId == "path") {
                    oboeBus.un(eventId + ":" + p2, p3);
                } else {
                    var listener = p2;
                    oboeBus(eventId).un(listener);
                }
                return oboeApi;
            };

        function addProtectedCallback(eventName, callback) {
            oboeBus(eventName).on(protectedCallback(callback), callback);
            return oboeApi;
        }

        function addForgettableCallback(event, callback, listenerId) {
            listenerId = listenerId || callback;
            var safeCallback = protectedCallback(callback);
            event.on(function() {
                var discard = false;
                oboeApi.forget = function() {
                    discard = true;
                };
                apply(arguments, safeCallback);
                delete oboeApi.forget;
                if (discard) {
                    event.un(listenerId);
                }
            }, listenerId);
            return oboeApi;
        }

        function protectedCallback(callback) {
            return function() {
                try {
                    return callback.apply(oboeApi, arguments);
                } catch (e) {
                    oboeBus(FAIL_EVENT).emit(errorReport(undefined, undefined, e));
                }
            };
        }

        function fullyQualifiedPatternMatchEvent(type, pattern) {
            return oboeBus(type + ":" + pattern);
        }

        function wrapCallbackToSwapNodeIfSomethingReturned(callback) {
            return function() {
                var returnValueFromCallback = callback.apply(this, arguments);
                if (defined(returnValueFromCallback)) {
                    if (returnValueFromCallback == oboe.drop) {
                        emitNodeDrop();
                    } else {
                        emitNodeSwap(returnValueFromCallback);
                    }
                }
            };
        }

        function addSingleNodeOrPathListener(eventId, pattern, callback) {
            var effectiveCallback;
            if (eventId == "node") {
                effectiveCallback = wrapCallbackToSwapNodeIfSomethingReturned(callback);
            } else {
                effectiveCallback = callback;
            }
            addForgettableCallback(fullyQualifiedPatternMatchEvent(eventId, pattern), effectiveCallback, callback);
        }

        function addMultipleNodeOrPathListeners(eventId, listenerMap) {
            for (var pattern in listenerMap) {
                addSingleNodeOrPathListener(eventId, pattern, listenerMap[pattern]);
            }
        }

        function addNodeOrPathListenerApi(eventId, jsonPathOrListenerMap, callback) {
            if (isString(jsonPathOrListenerMap)) {
                addSingleNodeOrPathListener(eventId, jsonPathOrListenerMap, callback);
            } else {
                addMultipleNodeOrPathListeners(eventId, jsonPathOrListenerMap);
            }
            return oboeApi;
        }
        oboeBus(ROOT_PATH_FOUND).on(function(rootNode) {
            oboeApi.root = functor(rootNode);
        });
        oboeBus(HTTP_START).on(function(_statusCode, headers) {
            oboeApi.header = function(name) {
                return name ? headers[name] : headers;
            };
        });
        return oboeApi = {
            on: addListener,
            addListener: addListener,
            removeListener: removeListener,
            emit: oboeBus.emit,
            node: partialComplete(addNodeOrPathListenerApi, "node"),
            path: partialComplete(addNodeOrPathListenerApi, "path"),
            done: partialComplete(addForgettableCallback, rootNodeFinishedEvent),
            start: partialComplete(addProtectedCallback, HTTP_START),
            fail: oboeBus(FAIL_EVENT).on,
            abort: oboeBus(ABORTING).emit,
            header: noop,
            root: noop,
            source: contentSource
        };
    }

    function wire(httpMethodName, contentSource, body, headers, withCredentials) {
        var oboeBus = pubSub();
        if (contentSource) {
            streamingHttp(oboeBus, httpTransport(), httpMethodName, contentSource, body, headers, withCredentials);
        }
        clarinet(oboeBus);
        ascentManager(oboeBus, incrementalContentBuilder(oboeBus));
        patternAdapter(oboeBus, jsonPathCompiler);
        return instanceApi(oboeBus, contentSource);
    }

    function applyDefaults(passthrough, url, httpMethodName, body, headers, withCredentials, cached) {
        headers = headers ? JSON.parse(JSON.stringify(headers)) : {};
        if (body) {
            if (!isString(body)) {
                body = JSON.stringify(body);
                headers["Content-Type"] = headers["Content-Type"] || "application/json";
            }
        } else {
            body = null;
        }

        function modifiedUrl(baseUrl, cached) {
            if (cached === false) {
                if (baseUrl.indexOf("?") == -1) {
                    baseUrl += "?";
                } else {
                    baseUrl += "&";
                }
                baseUrl += "_=" + new Date().getTime();
            }
            return baseUrl;
        }
        return passthrough(httpMethodName || "GET", modifiedUrl(url, cached), body, headers, withCredentials || false);
    }

    function oboe(arg1) {
        var nodeStreamMethodNames = list("resume", "pause", "pipe"),
            isStream = partialComplete(hasAllProperties, nodeStreamMethodNames);
        if (arg1) {
            if (isStream(arg1) || isString(arg1)) {
                return applyDefaults(wire, arg1);
            } else {
                return applyDefaults(wire, arg1.url, arg1.method, arg1.body, arg1.headers, arg1.withCredentials, arg1.cached);
            }
        } else {
            return wire();
        }
    }
    oboe.drop = function() {
        return oboe.drop;
    };
    if (typeof define === "function" && define.amd) {
        define("oboe", [], function() {
            return oboe;
        });
    } else if (typeof exports === "object") {
        module.exports = oboe;
    } else {
        window.oboe = oboe;
    }
})(function() {
    try {
        return window;
    } catch (e) {
        return self;
    }
}(), Object, Array, Error, JSON);

define("modules/controllers/chromecast_sender", ["modules/definitions/standardmodule", "underscore", "oboe"], function(parentModel, _, oboe) {
    function _thizOBJ_(o) {
        var defaults = {
            scope: true,
            type: "chromecast_sender",
            author: "Jonathan Robles",
            lasteditby: ""
        };
        defaults = this.deepExtend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {};
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._var({});
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        var parent = this;
        parent.notify("Trace", "refresh");
        window.CastPlayer.initializeCastPlayer();
        window.CastPlayer.initializeLocalPlayer();
        window.getStatus = function() {
            CastPlayer.session.sendMessage(AETN.namespace, {
                command: "sessionStatus"
            });
            trace('CastPlayer.session.sendMessage("' + AETN.namespace + '",{"command":"sessionStatus"});');
        };
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype._internalmethod = function() {
        var parent = this;
        parent.notify("Trace", "_internalmethod");
    };
    return _thizOBJ_;
});

require(["modules/controllers/chromecast_sender"], function(ccSender) {
    var mySender = new ccSender({});
    mySender.init();
});

define("instances/chromecast_sender_JS.js", function() {});
