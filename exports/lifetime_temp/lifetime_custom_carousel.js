define('jquery', [], function() {
    return jQuery;
});
//     Underscore.js 1.5.2
//     http://underscorejs.org
//     (c) 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.

(function() {

  // Baseline setup
  // --------------

  // Establish the root object, `window` in the browser, or `exports` on the server.
  var root = this;

  // Save the previous value of the `_` variable.
  var previousUnderscore = root._;

  // Establish the object that gets returned to break out of a loop iteration.
  var breaker = {};

  // Save bytes in the minified (but not gzipped) version:
  var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;

  // Create quick reference variables for speed access to core prototypes.
  var
    push             = ArrayProto.push,
    slice            = ArrayProto.slice,
    concat           = ArrayProto.concat,
    toString         = ObjProto.toString,
    hasOwnProperty   = ObjProto.hasOwnProperty;

  // All **ECMAScript 5** native function implementations that we hope to use
  // are declared here.
  var
    nativeForEach      = ArrayProto.forEach,
    nativeMap          = ArrayProto.map,
    nativeReduce       = ArrayProto.reduce,
    nativeReduceRight  = ArrayProto.reduceRight,
    nativeFilter       = ArrayProto.filter,
    nativeEvery        = ArrayProto.every,
    nativeSome         = ArrayProto.some,
    nativeIndexOf      = ArrayProto.indexOf,
    nativeLastIndexOf  = ArrayProto.lastIndexOf,
    nativeIsArray      = Array.isArray,
    nativeKeys         = Object.keys,
    nativeBind         = FuncProto.bind;

  // Create a safe reference to the Underscore object for use below.
  var _ = function(obj) {
    if (obj instanceof _) return obj;
    if (!(this instanceof _)) return new _(obj);
    this._wrapped = obj;
  };

  // Export the Underscore object for **Node.js**, with
  // backwards-compatibility for the old `require()` API. If we're in
  // the browser, add `_` as a global object via a string identifier,
  // for Closure Compiler "advanced" mode.
  if (typeof exports !== 'undefined') {
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = _;
    }
    exports._ = _;
  } else {
    root._ = _;
  }

  // Current version.
  _.VERSION = '1.5.2';

  // Collection Functions
  // --------------------

  // The cornerstone, an `each` implementation, aka `forEach`.
  // Handles objects with the built-in `forEach`, arrays, and raw objects.
  // Delegates to **ECMAScript 5**'s native `forEach` if available.
  var each = _.each = _.forEach = function(obj, iterator, context) {
    if (obj == null) return;
    if (nativeForEach && obj.forEach === nativeForEach) {
      obj.forEach(iterator, context);
    } else if (obj.length === +obj.length) {
      for (var i = 0, length = obj.length; i < length; i++) {
        if (iterator.call(context, obj[i], i, obj) === breaker) return;
      }
    } else {
      var keys = _.keys(obj);
      for (var i = 0, length = keys.length; i < length; i++) {
        if (iterator.call(context, obj[keys[i]], keys[i], obj) === breaker) return;
      }
    }
  };

  // Return the results of applying the iterator to each element.
  // Delegates to **ECMAScript 5**'s native `map` if available.
  _.map = _.collect = function(obj, iterator, context) {
    var results = [];
    if (obj == null) return results;
    if (nativeMap && obj.map === nativeMap) return obj.map(iterator, context);
    each(obj, function(value, index, list) {
      results.push(iterator.call(context, value, index, list));
    });
    return results;
  };

  var reduceError = 'Reduce of empty array with no initial value';

  // **Reduce** builds up a single result from a list of values, aka `inject`,
  // or `foldl`. Delegates to **ECMAScript 5**'s native `reduce` if available.
  _.reduce = _.foldl = _.inject = function(obj, iterator, memo, context) {
    var initial = arguments.length > 2;
    if (obj == null) obj = [];
    if (nativeReduce && obj.reduce === nativeReduce) {
      if (context) iterator = _.bind(iterator, context);
      return initial ? obj.reduce(iterator, memo) : obj.reduce(iterator);
    }
    each(obj, function(value, index, list) {
      if (!initial) {
        memo = value;
        initial = true;
      } else {
        memo = iterator.call(context, memo, value, index, list);
      }
    });
    if (!initial) throw new TypeError(reduceError);
    return memo;
  };

  // The right-associative version of reduce, also known as `foldr`.
  // Delegates to **ECMAScript 5**'s native `reduceRight` if available.
  _.reduceRight = _.foldr = function(obj, iterator, memo, context) {
    var initial = arguments.length > 2;
    if (obj == null) obj = [];
    if (nativeReduceRight && obj.reduceRight === nativeReduceRight) {
      if (context) iterator = _.bind(iterator, context);
      return initial ? obj.reduceRight(iterator, memo) : obj.reduceRight(iterator);
    }
    var length = obj.length;
    if (length !== +length) {
      var keys = _.keys(obj);
      length = keys.length;
    }
    each(obj, function(value, index, list) {
      index = keys ? keys[--length] : --length;
      if (!initial) {
        memo = obj[index];
        initial = true;
      } else {
        memo = iterator.call(context, memo, obj[index], index, list);
      }
    });
    if (!initial) throw new TypeError(reduceError);
    return memo;
  };

  // Return the first value which passes a truth test. Aliased as `detect`.
  _.find = _.detect = function(obj, iterator, context) {
    var result;
    any(obj, function(value, index, list) {
      if (iterator.call(context, value, index, list)) {
        result = value;
        return true;
      }
    });
    return result;
  };

  // Return all the elements that pass a truth test.
  // Delegates to **ECMAScript 5**'s native `filter` if available.
  // Aliased as `select`.
  _.filter = _.select = function(obj, iterator, context) {
    var results = [];
    if (obj == null) return results;
    if (nativeFilter && obj.filter === nativeFilter) return obj.filter(iterator, context);
    each(obj, function(value, index, list) {
      if (iterator.call(context, value, index, list)) results.push(value);
    });
    return results;
  };

  // Return all the elements for which a truth test fails.
  _.reject = function(obj, iterator, context) {
    return _.filter(obj, function(value, index, list) {
      return !iterator.call(context, value, index, list);
    }, context);
  };

  // Determine whether all of the elements match a truth test.
  // Delegates to **ECMAScript 5**'s native `every` if available.
  // Aliased as `all`.
  _.every = _.all = function(obj, iterator, context) {
    iterator || (iterator = _.identity);
    var result = true;
    if (obj == null) return result;
    if (nativeEvery && obj.every === nativeEvery) return obj.every(iterator, context);
    each(obj, function(value, index, list) {
      if (!(result = result && iterator.call(context, value, index, list))) return breaker;
    });
    return !!result;
  };

  // Determine if at least one element in the object matches a truth test.
  // Delegates to **ECMAScript 5**'s native `some` if available.
  // Aliased as `any`.
  var any = _.some = _.any = function(obj, iterator, context) {
    iterator || (iterator = _.identity);
    var result = false;
    if (obj == null) return result;
    if (nativeSome && obj.some === nativeSome) return obj.some(iterator, context);
    each(obj, function(value, index, list) {
      if (result || (result = iterator.call(context, value, index, list))) return breaker;
    });
    return !!result;
  };

  // Determine if the array or object contains a given value (using `===`).
  // Aliased as `include`.
  _.contains = _.include = function(obj, target) {
    if (obj == null) return false;
    if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1;
    return any(obj, function(value) {
      return value === target;
    });
  };

  // Invoke a method (with arguments) on every item in a collection.
  _.invoke = function(obj, method) {
    var args = slice.call(arguments, 2);
    var isFunc = _.isFunction(method);
    return _.map(obj, function(value) {
      return (isFunc ? method : value[method]).apply(value, args);
    });
  };

  // Convenience version of a common use case of `map`: fetching a property.
  _.pluck = function(obj, key) {
    return _.map(obj, function(value){ return value[key]; });
  };

  // Convenience version of a common use case of `filter`: selecting only objects
  // containing specific `key:value` pairs.
  _.where = function(obj, attrs, first) {
    if (_.isEmpty(attrs)) return first ? void 0 : [];
    return _[first ? 'find' : 'filter'](obj, function(value) {
      for (var key in attrs) {
        if (attrs[key] !== value[key]) return false;
      }
      return true;
    });
  };

  // Convenience version of a common use case of `find`: getting the first object
  // containing specific `key:value` pairs.
  _.findWhere = function(obj, attrs) {
    return _.where(obj, attrs, true);
  };

  // Return the maximum element or (element-based computation).
  // Can't optimize arrays of integers longer than 65,535 elements.
  // See [WebKit Bug 80797](https://bugs.webkit.org/show_bug.cgi?id=80797)
  _.max = function(obj, iterator, context) {
    if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) {
      return Math.max.apply(Math, obj);
    }
    if (!iterator && _.isEmpty(obj)) return -Infinity;
    var result = {computed : -Infinity, value: -Infinity};
    each(obj, function(value, index, list) {
      var computed = iterator ? iterator.call(context, value, index, list) : value;
      computed > result.computed && (result = {value : value, computed : computed});
    });
    return result.value;
  };

  // Return the minimum element (or element-based computation).
  _.min = function(obj, iterator, context) {
    if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) {
      return Math.min.apply(Math, obj);
    }
    if (!iterator && _.isEmpty(obj)) return Infinity;
    var result = {computed : Infinity, value: Infinity};
    each(obj, function(value, index, list) {
      var computed = iterator ? iterator.call(context, value, index, list) : value;
      computed < result.computed && (result = {value : value, computed : computed});
    });
    return result.value;
  };

  // Shuffle an array, using the modern version of the 
  // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
  _.shuffle = function(obj) {
    var rand;
    var index = 0;
    var shuffled = [];
    each(obj, function(value) {
      rand = _.random(index++);
      shuffled[index - 1] = shuffled[rand];
      shuffled[rand] = value;
    });
    return shuffled;
  };

  // Sample **n** random values from an array.
  // If **n** is not specified, returns a single random element from the array.
  // The internal `guard` argument allows it to work with `map`.
  _.sample = function(obj, n, guard) {
    if (arguments.length < 2 || guard) {
      return obj[_.random(obj.length - 1)];
    }
    return _.shuffle(obj).slice(0, Math.max(0, n));
  };

  // An internal function to generate lookup iterators.
  var lookupIterator = function(value) {
    return _.isFunction(value) ? value : function(obj){ return obj[value]; };
  };

  // Sort the object's values by a criterion produced by an iterator.
  _.sortBy = function(obj, value, context) {
    var iterator = lookupIterator(value);
    return _.pluck(_.map(obj, function(value, index, list) {
      return {
        value: value,
        index: index,
        criteria: iterator.call(context, value, index, list)
      };
    }).sort(function(left, right) {
      var a = left.criteria;
      var b = right.criteria;
      if (a !== b) {
        if (a > b || a === void 0) return 1;
        if (a < b || b === void 0) return -1;
      }
      return left.index - right.index;
    }), 'value');
  };

  // An internal function used for aggregate "group by" operations.
  var group = function(behavior) {
    return function(obj, value, context) {
      var result = {};
      var iterator = value == null ? _.identity : lookupIterator(value);
      each(obj, function(value, index) {
        var key = iterator.call(context, value, index, obj);
        behavior(result, key, value);
      });
      return result;
    };
  };

  // Groups the object's values by a criterion. Pass either a string attribute
  // to group by, or a function that returns the criterion.
  _.groupBy = group(function(result, key, value) {
    (_.has(result, key) ? result[key] : (result[key] = [])).push(value);
  });

  // Indexes the object's values by a criterion, similar to `groupBy`, but for
  // when you know that your index values will be unique.
  _.indexBy = group(function(result, key, value) {
    result[key] = value;
  });

  // Counts instances of an object that group by a certain criterion. Pass
  // either a string attribute to count by, or a function that returns the
  // criterion.
  _.countBy = group(function(result, key) {
    _.has(result, key) ? result[key]++ : result[key] = 1;
  });

  // Use a comparator function to figure out the smallest index at which
  // an object should be inserted so as to maintain order. Uses binary search.
  _.sortedIndex = function(array, obj, iterator, context) {
    iterator = iterator == null ? _.identity : lookupIterator(iterator);
    var value = iterator.call(context, obj);
    var low = 0, high = array.length;
    while (low < high) {
      var mid = (low + high) >>> 1;
      iterator.call(context, array[mid]) < value ? low = mid + 1 : high = mid;
    }
    return low;
  };

  // Safely create a real, live array from anything iterable.
  _.toArray = function(obj) {
    if (!obj) return [];
    if (_.isArray(obj)) return slice.call(obj);
    if (obj.length === +obj.length) return _.map(obj, _.identity);
    return _.values(obj);
  };

  // Return the number of elements in an object.
  _.size = function(obj) {
    if (obj == null) return 0;
    return (obj.length === +obj.length) ? obj.length : _.keys(obj).length;
  };

  // Array Functions
  // ---------------

  // Get the first element of an array. Passing **n** will return the first N
  // values in the array. Aliased as `head` and `take`. The **guard** check
  // allows it to work with `_.map`.
  _.first = _.head = _.take = function(array, n, guard) {
    if (array == null) return void 0;
    return (n == null) || guard ? array[0] : slice.call(array, 0, n);
  };

  // Returns everything but the last entry of the array. Especially useful on
  // the arguments object. Passing **n** will return all the values in
  // the array, excluding the last N. The **guard** check allows it to work with
  // `_.map`.
  _.initial = function(array, n, guard) {
    return slice.call(array, 0, array.length - ((n == null) || guard ? 1 : n));
  };

  // Get the last element of an array. Passing **n** will return the last N
  // values in the array. The **guard** check allows it to work with `_.map`.
  _.last = function(array, n, guard) {
    if (array == null) return void 0;
    if ((n == null) || guard) {
      return array[array.length - 1];
    } else {
      return slice.call(array, Math.max(array.length - n, 0));
    }
  };

  // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
  // Especially useful on the arguments object. Passing an **n** will return
  // the rest N values in the array. The **guard**
  // check allows it to work with `_.map`.
  _.rest = _.tail = _.drop = function(array, n, guard) {
    return slice.call(array, (n == null) || guard ? 1 : n);
  };

  // Trim out all falsy values from an array.
  _.compact = function(array) {
    return _.filter(array, _.identity);
  };

  // Internal implementation of a recursive `flatten` function.
  var flatten = function(input, shallow, output) {
    if (shallow && _.every(input, _.isArray)) {
      return concat.apply(output, input);
    }
    each(input, function(value) {
      if (_.isArray(value) || _.isArguments(value)) {
        shallow ? push.apply(output, value) : flatten(value, shallow, output);
      } else {
        output.push(value);
      }
    });
    return output;
  };

  // Flatten out an array, either recursively (by default), or just one level.
  _.flatten = function(array, shallow) {
    return flatten(array, shallow, []);
  };

  // Return a version of the array that does not contain the specified value(s).
  _.without = function(array) {
    return _.difference(array, slice.call(arguments, 1));
  };

  // Produce a duplicate-free version of the array. If the array has already
  // been sorted, you have the option of using a faster algorithm.
  // Aliased as `unique`.
  _.uniq = _.unique = function(array, isSorted, iterator, context) {
    if (_.isFunction(isSorted)) {
      context = iterator;
      iterator = isSorted;
      isSorted = false;
    }
    var initial = iterator ? _.map(array, iterator, context) : array;
    var results = [];
    var seen = [];
    each(initial, function(value, index) {
      if (isSorted ? (!index || seen[seen.length - 1] !== value) : !_.contains(seen, value)) {
        seen.push(value);
        results.push(array[index]);
      }
    });
    return results;
  };

  // Produce an array that contains the union: each distinct element from all of
  // the passed-in arrays.
  _.union = function() {
    return _.uniq(_.flatten(arguments, true));
  };

  // Produce an array that contains every item shared between all the
  // passed-in arrays.
  _.intersection = function(array) {
    var rest = slice.call(arguments, 1);
    return _.filter(_.uniq(array), function(item) {
      return _.every(rest, function(other) {
        return _.indexOf(other, item) >= 0;
      });
    });
  };

  // Take the difference between one array and a number of other arrays.
  // Only the elements present in just the first array will remain.
  _.difference = function(array) {
    var rest = concat.apply(ArrayProto, slice.call(arguments, 1));
    return _.filter(array, function(value){ return !_.contains(rest, value); });
  };

  // Zip together multiple lists into a single array -- elements that share
  // an index go together.
  _.zip = function() {
    var length = _.max(_.pluck(arguments, "length").concat(0));
    var results = new Array(length);
    for (var i = 0; i < length; i++) {
      results[i] = _.pluck(arguments, '' + i);
    }
    return results;
  };

  // Converts lists into objects. Pass either a single array of `[key, value]`
  // pairs, or two parallel arrays of the same length -- one of keys, and one of
  // the corresponding values.
  _.object = function(list, values) {
    if (list == null) return {};
    var result = {};
    for (var i = 0, length = list.length; i < length; i++) {
      if (values) {
        result[list[i]] = values[i];
      } else {
        result[list[i][0]] = list[i][1];
      }
    }
    return result;
  };

  // If the browser doesn't supply us with indexOf (I'm looking at you, **MSIE**),
  // we need this function. Return the position of the first occurrence of an
  // item in an array, or -1 if the item is not included in the array.
  // Delegates to **ECMAScript 5**'s native `indexOf` if available.
  // If the array is large and already in sort order, pass `true`
  // for **isSorted** to use binary search.
  _.indexOf = function(array, item, isSorted) {
    if (array == null) return -1;
    var i = 0, length = array.length;
    if (isSorted) {
      if (typeof isSorted == 'number') {
        i = (isSorted < 0 ? Math.max(0, length + isSorted) : isSorted);
      } else {
        i = _.sortedIndex(array, item);
        return array[i] === item ? i : -1;
      }
    }
    if (nativeIndexOf && array.indexOf === nativeIndexOf) return array.indexOf(item, isSorted);
    for (; i < length; i++) if (array[i] === item) return i;
    return -1;
  };

  // Delegates to **ECMAScript 5**'s native `lastIndexOf` if available.
  _.lastIndexOf = function(array, item, from) {
    if (array == null) return -1;
    var hasIndex = from != null;
    if (nativeLastIndexOf && array.lastIndexOf === nativeLastIndexOf) {
      return hasIndex ? array.lastIndexOf(item, from) : array.lastIndexOf(item);
    }
    var i = (hasIndex ? from : array.length);
    while (i--) if (array[i] === item) return i;
    return -1;
  };

  // Generate an integer Array containing an arithmetic progression. A port of
  // the native Python `range()` function. See
  // [the Python documentation](http://docs.python.org/library/functions.html#range).
  _.range = function(start, stop, step) {
    if (arguments.length <= 1) {
      stop = start || 0;
      start = 0;
    }
    step = arguments[2] || 1;

    var length = Math.max(Math.ceil((stop - start) / step), 0);
    var idx = 0;
    var range = new Array(length);

    while(idx < length) {
      range[idx++] = start;
      start += step;
    }

    return range;
  };

  // Function (ahem) Functions
  // ------------------

  // Reusable constructor function for prototype setting.
  var ctor = function(){};

  // Create a function bound to a given object (assigning `this`, and arguments,
  // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
  // available.
  _.bind = function(func, context) {
    var args, bound;
    if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
    if (!_.isFunction(func)) throw new TypeError;
    args = slice.call(arguments, 2);
    return bound = function() {
      if (!(this instanceof bound)) return func.apply(context, args.concat(slice.call(arguments)));
      ctor.prototype = func.prototype;
      var self = new ctor;
      ctor.prototype = null;
      var result = func.apply(self, args.concat(slice.call(arguments)));
      if (Object(result) === result) return result;
      return self;
    };
  };

  // Partially apply a function by creating a version that has had some of its
  // arguments pre-filled, without changing its dynamic `this` context.
  _.partial = function(func) {
    var args = slice.call(arguments, 1);
    return function() {
      return func.apply(this, args.concat(slice.call(arguments)));
    };
  };

  // Bind all of an object's methods to that object. Useful for ensuring that
  // all callbacks defined on an object belong to it.
  _.bindAll = function(obj) {
    var funcs = slice.call(arguments, 1);
    if (funcs.length === 0) throw new Error("bindAll must be passed function names");
    each(funcs, function(f) { obj[f] = _.bind(obj[f], obj); });
    return obj;
  };

  // Memoize an expensive function by storing its results.
  _.memoize = function(func, hasher) {
    var memo = {};
    hasher || (hasher = _.identity);
    return function() {
      var key = hasher.apply(this, arguments);
      return _.has(memo, key) ? memo[key] : (memo[key] = func.apply(this, arguments));
    };
  };

  // Delays a function for the given number of milliseconds, and then calls
  // it with the arguments supplied.
  _.delay = function(func, wait) {
    var args = slice.call(arguments, 2);
    return setTimeout(function(){ return func.apply(null, args); }, wait);
  };

  // Defers a function, scheduling it to run after the current call stack has
  // cleared.
  _.defer = function(func) {
    return _.delay.apply(_, [func, 1].concat(slice.call(arguments, 1)));
  };

  // Returns a function, that, when invoked, will only be triggered at most once
  // during a given window of time. Normally, the throttled function will run
  // as much as it can, without ever going more than once per `wait` duration;
  // but if you'd like to disable the execution on the leading edge, pass
  // `{leading: false}`. To disable execution on the trailing edge, ditto.
  _.throttle = function(func, wait, options) {
    var context, args, result;
    var timeout = null;
    var previous = 0;
    options || (options = {});
    var later = function() {
      previous = options.leading === false ? 0 : new Date;
      timeout = null;
      result = func.apply(context, args);
    };
    return function() {
      var now = new Date;
      if (!previous && options.leading === false) previous = now;
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0) {
        clearTimeout(timeout);
        timeout = null;
        previous = now;
        result = func.apply(context, args);
      } else if (!timeout && options.trailing !== false) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };
  };

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  _.debounce = function(func, wait, immediate) {
    var timeout, args, context, timestamp, result;
    return function() {
      context = this;
      args = arguments;
      timestamp = new Date();
      var later = function() {
        var last = (new Date()) - timestamp;
        if (last < wait) {
          timeout = setTimeout(later, wait - last);
        } else {
          timeout = null;
          if (!immediate) result = func.apply(context, args);
        }
      };
      var callNow = immediate && !timeout;
      if (!timeout) {
        timeout = setTimeout(later, wait);
      }
      if (callNow) result = func.apply(context, args);
      return result;
    };
  };

  // Returns a function that will be executed at most one time, no matter how
  // often you call it. Useful for lazy initialization.
  _.once = function(func) {
    var ran = false, memo;
    return function() {
      if (ran) return memo;
      ran = true;
      memo = func.apply(this, arguments);
      func = null;
      return memo;
    };
  };

  // Returns the first function passed as an argument to the second,
  // allowing you to adjust arguments, run code before and after, and
  // conditionally execute the original function.
  _.wrap = function(func, wrapper) {
    return function() {
      var args = [func];
      push.apply(args, arguments);
      return wrapper.apply(this, args);
    };
  };

  // Returns a function that is the composition of a list of functions, each
  // consuming the return value of the function that follows.
  _.compose = function() {
    var funcs = arguments;
    return function() {
      var args = arguments;
      for (var i = funcs.length - 1; i >= 0; i--) {
        args = [funcs[i].apply(this, args)];
      }
      return args[0];
    };
  };

  // Returns a function that will only be executed after being called N times.
  _.after = function(times, func) {
    return function() {
      if (--times < 1) {
        return func.apply(this, arguments);
      }
    };
  };

  // Object Functions
  // ----------------

  // Retrieve the names of an object's properties.
  // Delegates to **ECMAScript 5**'s native `Object.keys`
  _.keys = nativeKeys || function(obj) {
    if (obj !== Object(obj)) throw new TypeError('Invalid object');
    var keys = [];
    for (var key in obj) if (_.has(obj, key)) keys.push(key);
    return keys;
  };

  // Retrieve the values of an object's properties.
  _.values = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var values = new Array(length);
    for (var i = 0; i < length; i++) {
      values[i] = obj[keys[i]];
    }
    return values;
  };

  // Convert an object into a list of `[key, value]` pairs.
  _.pairs = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var pairs = new Array(length);
    for (var i = 0; i < length; i++) {
      pairs[i] = [keys[i], obj[keys[i]]];
    }
    return pairs;
  };

  // Invert the keys and values of an object. The values must be serializable.
  _.invert = function(obj) {
    var result = {};
    var keys = _.keys(obj);
    for (var i = 0, length = keys.length; i < length; i++) {
      result[obj[keys[i]]] = keys[i];
    }
    return result;
  };

  // Return a sorted list of the function names available on the object.
  // Aliased as `methods`
  _.functions = _.methods = function(obj) {
    var names = [];
    for (var key in obj) {
      if (_.isFunction(obj[key])) names.push(key);
    }
    return names.sort();
  };

  // Extend a given object with all the properties in passed-in object(s).
  _.extend = function(obj) {
    each(slice.call(arguments, 1), function(source) {
      if (source) {
        for (var prop in source) {
          obj[prop] = source[prop];
        }
      }
    });
    return obj;
  };

  // Return a copy of the object only containing the whitelisted properties.
  _.pick = function(obj) {
    var copy = {};
    var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
    each(keys, function(key) {
      if (key in obj) copy[key] = obj[key];
    });
    return copy;
  };

   // Return a copy of the object without the blacklisted properties.
  _.omit = function(obj) {
    var copy = {};
    var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
    for (var key in obj) {
      if (!_.contains(keys, key)) copy[key] = obj[key];
    }
    return copy;
  };

  // Fill in a given object with default properties.
  _.defaults = function(obj) {
    each(slice.call(arguments, 1), function(source) {
      if (source) {
        for (var prop in source) {
          if (obj[prop] === void 0) obj[prop] = source[prop];
        }
      }
    });
    return obj;
  };

  // Create a (shallow-cloned) duplicate of an object.
  _.clone = function(obj) {
    if (!_.isObject(obj)) return obj;
    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
  };

  // Invokes interceptor with the obj, and then returns obj.
  // The primary purpose of this method is to "tap into" a method chain, in
  // order to perform operations on intermediate results within the chain.
  _.tap = function(obj, interceptor) {
    interceptor(obj);
    return obj;
  };

  // Internal recursive comparison function for `isEqual`.
  var eq = function(a, b, aStack, bStack) {
    // Identical objects are equal. `0 === -0`, but they aren't identical.
    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
    if (a === b) return a !== 0 || 1 / a == 1 / b;
    // A strict comparison is necessary because `null == undefined`.
    if (a == null || b == null) return a === b;
    // Unwrap any wrapped objects.
    if (a instanceof _) a = a._wrapped;
    if (b instanceof _) b = b._wrapped;
    // Compare `[[Class]]` names.
    var className = toString.call(a);
    if (className != toString.call(b)) return false;
    switch (className) {
      // Strings, numbers, dates, and booleans are compared by value.
      case '[object String]':
        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
        // equivalent to `new String("5")`.
        return a == String(b);
      case '[object Number]':
        // `NaN`s are equivalent, but non-reflexive. An `egal` comparison is performed for
        // other numeric values.
        return a != +a ? b != +b : (a == 0 ? 1 / a == 1 / b : a == +b);
      case '[object Date]':
      case '[object Boolean]':
        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
        // millisecond representations. Note that invalid dates with millisecond representations
        // of `NaN` are not equivalent.
        return +a == +b;
      // RegExps are compared by their source patterns and flags.
      case '[object RegExp]':
        return a.source == b.source &&
               a.global == b.global &&
               a.multiline == b.multiline &&
               a.ignoreCase == b.ignoreCase;
    }
    if (typeof a != 'object' || typeof b != 'object') return false;
    // Assume equality for cyclic structures. The algorithm for detecting cyclic
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.
    var length = aStack.length;
    while (length--) {
      // Linear search. Performance is inversely proportional to the number of
      // unique nested structures.
      if (aStack[length] == a) return bStack[length] == b;
    }
    // Objects with different constructors are not equivalent, but `Object`s
    // from different frames are.
    var aCtor = a.constructor, bCtor = b.constructor;
    if (aCtor !== bCtor && !(_.isFunction(aCtor) && (aCtor instanceof aCtor) &&
                             _.isFunction(bCtor) && (bCtor instanceof bCtor))) {
      return false;
    }
    // Add the first object to the stack of traversed objects.
    aStack.push(a);
    bStack.push(b);
    var size = 0, result = true;
    // Recursively compare objects and arrays.
    if (className == '[object Array]') {
      // Compare array lengths to determine if a deep comparison is necessary.
      size = a.length;
      result = size == b.length;
      if (result) {
        // Deep compare the contents, ignoring non-numeric properties.
        while (size--) {
          if (!(result = eq(a[size], b[size], aStack, bStack))) break;
        }
      }
    } else {
      // Deep compare objects.
      for (var key in a) {
        if (_.has(a, key)) {
          // Count the expected number of properties.
          size++;
          // Deep compare each member.
          if (!(result = _.has(b, key) && eq(a[key], b[key], aStack, bStack))) break;
        }
      }
      // Ensure that both objects contain the same number of properties.
      if (result) {
        for (key in b) {
          if (_.has(b, key) && !(size--)) break;
        }
        result = !size;
      }
    }
    // Remove the first object from the stack of traversed objects.
    aStack.pop();
    bStack.pop();
    return result;
  };

  // Perform a deep comparison to check if two objects are equal.
  _.isEqual = function(a, b) {
    return eq(a, b, [], []);
  };

  // Is a given array, string, or object empty?
  // An "empty" object has no enumerable own-properties.
  _.isEmpty = function(obj) {
    if (obj == null) return true;
    if (_.isArray(obj) || _.isString(obj)) return obj.length === 0;
    for (var key in obj) if (_.has(obj, key)) return false;
    return true;
  };

  // Is a given value a DOM element?
  _.isElement = function(obj) {
    return !!(obj && obj.nodeType === 1);
  };

  // Is a given value an array?
  // Delegates to ECMA5's native Array.isArray
  _.isArray = nativeIsArray || function(obj) {
    return toString.call(obj) == '[object Array]';
  };

  // Is a given variable an object?
  _.isObject = function(obj) {
    return obj === Object(obj);
  };

  // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp.
  each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp'], function(name) {
    _['is' + name] = function(obj) {
      return toString.call(obj) == '[object ' + name + ']';
    };
  });

  // Define a fallback version of the method in browsers (ahem, IE), where
  // there isn't any inspectable "Arguments" type.
  if (!_.isArguments(arguments)) {
    _.isArguments = function(obj) {
      return !!(obj && _.has(obj, 'callee'));
    };
  }

  // Optimize `isFunction` if appropriate.
  if (typeof (/./) !== 'function') {
    _.isFunction = function(obj) {
      return typeof obj === 'function';
    };
  }

  // Is a given object a finite number?
  _.isFinite = function(obj) {
    return isFinite(obj) && !isNaN(parseFloat(obj));
  };

  // Is the given value `NaN`? (NaN is the only number which does not equal itself).
  _.isNaN = function(obj) {
    return _.isNumber(obj) && obj != +obj;
  };

  // Is a given value a boolean?
  _.isBoolean = function(obj) {
    return obj === true || obj === false || toString.call(obj) == '[object Boolean]';
  };

  // Is a given value equal to null?
  _.isNull = function(obj) {
    return obj === null;
  };

  // Is a given variable undefined?
  _.isUndefined = function(obj) {
    return obj === void 0;
  };

  // Shortcut function for checking if an object has a given property directly
  // on itself (in other words, not on a prototype).
  _.has = function(obj, key) {
    return hasOwnProperty.call(obj, key);
  };

  // Utility Functions
  // -----------------

  // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
  // previous owner. Returns a reference to the Underscore object.
  _.noConflict = function() {
    root._ = previousUnderscore;
    return this;
  };

  // Keep the identity function around for default iterators.
  _.identity = function(value) {
    return value;
  };

  // Run a function **n** times.
  _.times = function(n, iterator, context) {
    var accum = Array(Math.max(0, n));
    for (var i = 0; i < n; i++) accum[i] = iterator.call(context, i);
    return accum;
  };

  // Return a random integer between min and max (inclusive).
  _.random = function(min, max) {
    if (max == null) {
      max = min;
      min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
  };

  // List of HTML entities for escaping.
  var entityMap = {
    escape: {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#x27;'
    }
  };
  entityMap.unescape = _.invert(entityMap.escape);

  // Regexes containing the keys and values listed immediately above.
  var entityRegexes = {
    escape:   new RegExp('[' + _.keys(entityMap.escape).join('') + ']', 'g'),
    unescape: new RegExp('(' + _.keys(entityMap.unescape).join('|') + ')', 'g')
  };

  // Functions for escaping and unescaping strings to/from HTML interpolation.
  _.each(['escape', 'unescape'], function(method) {
    _[method] = function(string) {
      if (string == null) return '';
      return ('' + string).replace(entityRegexes[method], function(match) {
        return entityMap[method][match];
      });
    };
  });

  // If the value of the named `property` is a function then invoke it with the
  // `object` as context; otherwise, return it.
  _.result = function(object, property) {
    if (object == null) return void 0;
    var value = object[property];
    return _.isFunction(value) ? value.call(object) : value;
  };

  // Add your own custom functions to the Underscore object.
  _.mixin = function(obj) {
    each(_.functions(obj), function(name) {
      var func = _[name] = obj[name];
      _.prototype[name] = function() {
        var args = [this._wrapped];
        push.apply(args, arguments);
        return result.call(this, func.apply(_, args));
      };
    });
  };

  // Generate a unique integer id (unique within the entire client session).
  // Useful for temporary DOM ids.
  var idCounter = 0;
  _.uniqueId = function(prefix) {
    var id = ++idCounter + '';
    return prefix ? prefix + id : id;
  };

  // By default, Underscore uses ERB-style template delimiters, change the
  // following template settings to use alternative delimiters.
  _.templateSettings = {
    evaluate    : /<%([\s\S]+?)%>/g,
    interpolate : /<%=([\s\S]+?)%>/g,
    escape      : /<%-([\s\S]+?)%>/g
  };

  // When customizing `templateSettings`, if you don't want to define an
  // interpolation, evaluation or escaping regex, we need one that is
  // guaranteed not to match.
  var noMatch = /(.)^/;

  // Certain characters need to be escaped so that they can be put into a
  // string literal.
  var escapes = {
    "'":      "'",
    '\\':     '\\',
    '\r':     'r',
    '\n':     'n',
    '\t':     't',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  var escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g;

  // JavaScript micro-templating, similar to John Resig's implementation.
  // Underscore templating handles arbitrary delimiters, preserves whitespace,
  // and correctly escapes quotes within interpolated code.
  _.template = function(text, data, settings) {
    var render;
    settings = _.defaults({}, settings, _.templateSettings);

    // Combine delimiters into one regular expression via alternation.
    var matcher = new RegExp([
      (settings.escape || noMatch).source,
      (settings.interpolate || noMatch).source,
      (settings.evaluate || noMatch).source
    ].join('|') + '|$', 'g');

    // Compile the template source, escaping string literals appropriately.
    var index = 0;
    var source = "__p+='";
    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
      source += text.slice(index, offset)
        .replace(escaper, function(match) { return '\\' + escapes[match]; });

      if (escape) {
        source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
      }
      if (interpolate) {
        source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
      }
      if (evaluate) {
        source += "';\n" + evaluate + "\n__p+='";
      }
      index = offset + match.length;
      return match;
    });
    source += "';\n";

    // If a variable is not specified, place data values in local scope.
    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

    source = "var __t,__p='',__j=Array.prototype.join," +
      "print=function(){__p+=__j.call(arguments,'');};\n" +
      source + "return __p;\n";

    try {
      render = new Function(settings.variable || 'obj', '_', source);
    } catch (e) {
      e.source = source;
      throw e;
    }

    if (data) return render(data, _);
    var template = function(data) {
      return render.call(this, data, _);
    };

    // Provide the compiled function source as a convenience for precompilation.
    template.source = 'function(' + (settings.variable || 'obj') + '){\n' + source + '}';

    return template;
  };

  // Add a "chain" function, which will delegate to the wrapper.
  _.chain = function(obj) {
    return _(obj).chain();
  };

  // OOP
  // ---------------
  // If Underscore is called as a function, it returns a wrapped object that
  // can be used OO-style. This wrapper holds altered versions of all the
  // underscore functions. Wrapped objects may be chained.

  // Helper function to continue chaining intermediate results.
  var result = function(obj) {
    return this._chain ? _(obj).chain() : obj;
  };

  // Add all of the Underscore functions to the wrapper object.
  _.mixin(_);

  // Add all mutator Array functions to the wrapper.
  each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      var obj = this._wrapped;
      method.apply(obj, arguments);
      if ((name == 'shift' || name == 'splice') && obj.length === 0) delete obj[0];
      return result.call(this, obj);
    };
  });

  // Add all accessor Array functions to the wrapper.
  each(['concat', 'join', 'slice'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      return result.call(this, method.apply(this._wrapped, arguments));
    };
  });

  _.extend(_.prototype, {

    // Start chaining a wrapped Underscore object.
    chain: function() {
      this._chain = true;
      return this;
    },

    // Extracts the result from a wrapped and chained object.
    value: function() {
      return this._wrapped;
    }

  });

}).call(this);

define("underscore", (function (global) {
    return function () {
        var ret, fn;
        return ret || global._;
    };
}(this)));

/**
 * Standard Module Definition
 * Author: Jonathan Robles
 *
 * Date: <inZertDATe>
 *
 *  NOTE: UNDER ANY CIRCUMSTANCES, DON'T SCREW WITH THIS MODULE!!!
 */
define('modules/definitions/standardmodule',[],function () {

    var _instanceID = 0; //instance id
    var _nextInstanceID = function(){ return( ++_instanceID );  };//instance adder
    var defaults =[]; //instance array
    var deepExtend = function(destination, source){
        for (var property in source) {
            if (source[property] && source[property].constructor &&
                source[property].constructor === Object) {
                destination[property] = destination[property] || {};
                arguments.callee(destination[property], source[property]);
            } else {
                destination[property] = source[property];
            }
        }
        return destination;
    };

    function _thizOBJ_(o){
        this._instanceID = _nextInstanceID(); //who am I?
        var _parent=this;
        //INSTANCE VARIABLES
        defaults[this._instanceID]={
            type:'Standard Module Definition',
            author:'Jonathan Robles',
            notifyscope:'global',

            target:undefined,
            file:undefined,
            usenocache:true,
            data:undefined,
            callback:undefined,
            interval:undefined,


            init:function(){
                _notify.broadcast('Initialize', [{
                    senderID:_parent._instanceID,
                    sendertype:this.type,
                    notifyscope:this.notifyscope,
                    data:{
                        author:this.author
                    }
                }]);
            },
            parent:this
        };
        defaults[this._instanceID]=deepExtend(defaults[this._instanceID],o);
        defaults[this._instanceID].init();
        return( this );

    }

    // BASE METHODS
    _thizOBJ_.prototype = {
        _init:function(){this._var().init();}, //run internal initialization
        _showdata: function(){
            return JSON.stringify(defaults[this._instanceID]);
        }, //show data as a string
        _id: function(){ return( this._instanceID );}, //get instanceID
        _var:function(o){
            if(o!=undefined){defaults[this._instanceID]= deepExtend(defaults[this._instanceID],o)};
            return defaults[this._instanceID];
        },  //get defaults variable
        _nocache:function(string){
            if(typeof string==='string'){
            if(this._var().usenocache){
                var addOn='?';
                if(string.indexOf('?')!=-1){addOn='&'}
                return string+addOn+"nocache=" + (Math.floor(Math.random() * 9999));
            } else {

                return string;
            }
        }     else {this.notify('Alert','_nocache needs a string!');return;}
        }, //tack on nocacheifneeded
        notify:function(type,data){
            _notify.broadcast(type, [{
                senderID:this._id(),
                sendertype:this._var().type,
                notifyscope:this._var().notifyscope,
                data:data
            }]);
        },
        deepExtend:function(destination, source){
            return deepExtend(destination, source)
        },
        parent:this

    };
    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});

/*

function deepExtend(destination, source) {
    for (var property in source) {
        if (source[property] && source[property].constructor &&
            source[property].constructor === Object) {
            destination[property] = destination[property] || {};
            arguments.callee(destination[property], source[property]);
        } else {
            destination[property] = source[property];
        }
    }
    return destination;
};
*/

;
/*! Hammer.JS - v1.0.5 - 2013-04-07
 * http://eightmedia.github.com/hammer.js
 *
 * Copyright (c) 2013 Jorik Tangelder <j.tangelder@gmail.com>;
 * Licensed under the MIT license */

(function(window, undefined) {
    'use strict';

/**
 * Hammer
 * use this to create instances
 * @param   {HTMLElement}   element
 * @param   {Object}        options
 * @returns {Hammer.Instance}
 * @constructor
 */
var Hammer = function(element, options) {
    return new Hammer.Instance(element, options || {});
};

// default settings
Hammer.defaults = {
    // add styles and attributes to the element to prevent the browser from doing
    // its native behavior. this doesnt prevent the scrolling, but cancels
    // the contextmenu, tap highlighting etc
    // set to false to disable this
    stop_browser_behavior: {
		// this also triggers onselectstart=false for IE
        userSelect: 'none',
		// this makes the element blocking in IE10 >, you could experiment with the value
		// see for more options this issue; https://github.com/EightMedia/hammer.js/issues/241
        touchAction: 'none',
		touchCallout: 'none',
        contentZooming: 'none',
        userDrag: 'none',
        tapHighlightColor: 'rgba(0,0,0,0)'
    }

    // more settings are defined per gesture at gestures.js
};

// detect touchevents
Hammer.HAS_POINTEREVENTS = navigator.pointerEnabled || navigator.msPointerEnabled;
Hammer.HAS_TOUCHEVENTS = ('ontouchstart' in window);

// dont use mouseevents on mobile devices
Hammer.MOBILE_REGEX = /mobile|tablet|ip(ad|hone|od)|android/i;
Hammer.NO_MOUSEEVENTS = Hammer.HAS_TOUCHEVENTS && navigator.userAgent.match(Hammer.MOBILE_REGEX);

// eventtypes per touchevent (start, move, end)
// are filled by Hammer.event.determineEventTypes on setup
Hammer.EVENT_TYPES = {};

// direction defines
Hammer.DIRECTION_DOWN = 'down';
Hammer.DIRECTION_LEFT = 'left';
Hammer.DIRECTION_UP = 'up';
Hammer.DIRECTION_RIGHT = 'right';

// pointer type
Hammer.POINTER_MOUSE = 'mouse';
Hammer.POINTER_TOUCH = 'touch';
Hammer.POINTER_PEN = 'pen';

// touch event defines
Hammer.EVENT_START = 'start';
Hammer.EVENT_MOVE = 'move';
Hammer.EVENT_END = 'end';

// hammer document where the base events are added at
Hammer.DOCUMENT = document;

// plugins namespace
Hammer.plugins = {};

// if the window events are set...
Hammer.READY = false;

/**
 * setup events to detect gestures on the document
 */
function setup() {
    if(Hammer.READY) {
        return;
    }

    // find what eventtypes we add listeners to
    Hammer.event.determineEventTypes();

    // Register all gestures inside Hammer.gestures
    for(var name in Hammer.gestures) {
        if(Hammer.gestures.hasOwnProperty(name)) {
            Hammer.detection.register(Hammer.gestures[name]);
        }
    }

    // Add touch events on the document
    Hammer.event.onTouch(Hammer.DOCUMENT, Hammer.EVENT_MOVE, Hammer.detection.detect);
    Hammer.event.onTouch(Hammer.DOCUMENT, Hammer.EVENT_END, Hammer.detection.detect);

    // Hammer is ready...!
    Hammer.READY = true;
}

/**
 * create new hammer instance
 * all methods should return the instance itself, so it is chainable.
 * @param   {HTMLElement}       element
 * @param   {Object}            [options={}]
 * @returns {Hammer.Instance}
 * @constructor
 */
Hammer.Instance = function(element, options) {
    var self = this;

    // setup HammerJS window events and register all gestures
    // this also sets up the default options
    setup();

    this.element = element;

    // start/stop detection option
    this.enabled = true;

    // merge options
    this.options = Hammer.utils.extend(
        Hammer.utils.extend({}, Hammer.defaults),
        options || {});

    // add some css to the element to prevent the browser from doing its native behavoir
    if(this.options.stop_browser_behavior) {
        Hammer.utils.stopDefaultBrowserBehavior(this.element, this.options.stop_browser_behavior);
    }

    // start detection on touchstart
    Hammer.event.onTouch(element, Hammer.EVENT_START, function(ev) {
        if(self.enabled) {
            Hammer.detection.startDetect(self, ev);
        }
    });

    // return instance
    return this;
};


Hammer.Instance.prototype = {
    /**
     * bind events to the instance
     * @param   {String}      gesture
     * @param   {Function}    handler
     * @returns {Hammer.Instance}
     */
    on: function onEvent(gesture, handler){
        var gestures = gesture.split(' ');
        for(var t=0; t<gestures.length; t++) {
            this.element.addEventListener(gestures[t], handler, false);
        }
        return this;
    },


    /**
     * unbind events to the instance
     * @param   {String}      gesture
     * @param   {Function}    handler
     * @returns {Hammer.Instance}
     */
    off: function offEvent(gesture, handler){
        var gestures = gesture.split(' ');
        for(var t=0; t<gestures.length; t++) {
            this.element.removeEventListener(gestures[t], handler, false);
        }
        return this;
    },


    /**
     * trigger gesture event
     * @param   {String}      gesture
     * @param   {Object}      eventData
     * @returns {Hammer.Instance}
     */
    trigger: function triggerEvent(gesture, eventData){
        // create DOM event
        var event = Hammer.DOCUMENT.createEvent('Event');
		event.initEvent(gesture, true, true);
		event.gesture = eventData;

        // trigger on the target if it is in the instance element,
        // this is for event delegation tricks
        var element = this.element;
        if(Hammer.utils.hasParent(eventData.target, element)) {
            element = eventData.target;
        }

        element.dispatchEvent(event);
        return this;
    },


    /**
     * enable of disable hammer.js detection
     * @param   {Boolean}   state
     * @returns {Hammer.Instance}
     */
    enable: function enable(state) {
        this.enabled = state;
        return this;
    }
};

/**
 * this holds the last move event,
 * used to fix empty touchend issue
 * see the onTouch event for an explanation
 * @type {Object}
 */
var last_move_event = null;


/**
 * when the mouse is hold down, this is true
 * @type {Boolean}
 */
var enable_detect = false;


/**
 * when touch events have been fired, this is true
 * @type {Boolean}
 */
var touch_triggered = false;


Hammer.event = {
    /**
     * simple addEventListener
     * @param   {HTMLElement}   element
     * @param   {String}        type
     * @param   {Function}      handler
     */
    bindDom: function(element, type, handler) {
        var types = type.split(' ');
        for(var t=0; t<types.length; t++) {
            element.addEventListener(types[t], handler, false);
        }
    },


    /**
     * touch events with mouse fallback
     * @param   {HTMLElement}   element
     * @param   {String}        eventType        like Hammer.EVENT_MOVE
     * @param   {Function}      handler
     */
    onTouch: function onTouch(element, eventType, handler) {
		var self = this;

        this.bindDom(element, Hammer.EVENT_TYPES[eventType], function bindDomOnTouch(ev) {
            var sourceEventType = ev.type.toLowerCase();

            // onmouseup, but when touchend has been fired we do nothing.
            // this is for touchdevices which also fire a mouseup on touchend
            if(sourceEventType.match(/mouse/) && touch_triggered) {
                return;
            }

            // mousebutton must be down or a touch event
            else if( sourceEventType.match(/touch/) ||   // touch events are always on screen
                sourceEventType.match(/pointerdown/) || // pointerevents touch
                (sourceEventType.match(/mouse/) && ev.which === 1)   // mouse is pressed
            ){
                enable_detect = true;
            }

            // we are in a touch event, set the touch triggered bool to true,
            // this for the conflicts that may occur on ios and android
            if(sourceEventType.match(/touch|pointer/)) {
                touch_triggered = true;
            }

            // count the total touches on the screen
            var count_touches = 0;

            // when touch has been triggered in this detection session
            // and we are now handling a mouse event, we stop that to prevent conflicts
            if(enable_detect) {
                // update pointerevent
                if(Hammer.HAS_POINTEREVENTS && eventType != Hammer.EVENT_END) {
                    count_touches = Hammer.PointerEvent.updatePointer(eventType, ev);
                }
                // touch
                else if(sourceEventType.match(/touch/)) {
                    count_touches = ev.touches.length;
                }
                // mouse
                else if(!touch_triggered) {
                    count_touches = sourceEventType.match(/up/) ? 0 : 1;
                }

                // if we are in a end event, but when we remove one touch and
                // we still have enough, set eventType to move
                if(count_touches > 0 && eventType == Hammer.EVENT_END) {
                    eventType = Hammer.EVENT_MOVE;
                }
                // no touches, force the end event
                else if(!count_touches) {
                    eventType = Hammer.EVENT_END;
                }

                // because touchend has no touches, and we often want to use these in our gestures,
                // we send the last move event as our eventData in touchend
                if(!count_touches && last_move_event !== null) {
                    ev = last_move_event;
                }
                // store the last move event
                else {
                    last_move_event = ev;
                }

                // trigger the handler
                handler.call(Hammer.detection, self.collectEventData(element, eventType, ev));

                // remove pointerevent from list
                if(Hammer.HAS_POINTEREVENTS && eventType == Hammer.EVENT_END) {
                    count_touches = Hammer.PointerEvent.updatePointer(eventType, ev);
                }
            }

            //debug(sourceEventType +" "+ eventType);

            // on the end we reset everything
            if(!count_touches) {
                last_move_event = null;
                enable_detect = false;
                touch_triggered = false;
                Hammer.PointerEvent.reset();
            }
        });
    },


    /**
     * we have different events for each device/browser
     * determine what we need and set them in the Hammer.EVENT_TYPES constant
     */
    determineEventTypes: function determineEventTypes() {
        // determine the eventtype we want to set
        var types;

        // pointerEvents magic
        if(Hammer.HAS_POINTEREVENTS) {
            types = Hammer.PointerEvent.getEvents();
        }
        // on Android, iOS, blackberry, windows mobile we dont want any mouseevents
        else if(Hammer.NO_MOUSEEVENTS) {
            types = [
                'touchstart',
                'touchmove',
                'touchend touchcancel'];
        }
        // for non pointer events browsers and mixed browsers,
        // like chrome on windows8 touch laptop
        else {
            types = [
                'touchstart mousedown',
                'touchmove mousemove',
                'touchend touchcancel mouseup'];
        }

        Hammer.EVENT_TYPES[Hammer.EVENT_START]  = types[0];
        Hammer.EVENT_TYPES[Hammer.EVENT_MOVE]   = types[1];
        Hammer.EVENT_TYPES[Hammer.EVENT_END]    = types[2];
    },


    /**
     * create touchlist depending on the event
     * @param   {Object}    ev
     * @param   {String}    eventType   used by the fakemultitouch plugin
     */
    getTouchList: function getTouchList(ev/*, eventType*/) {
        // get the fake pointerEvent touchlist
        if(Hammer.HAS_POINTEREVENTS) {
            return Hammer.PointerEvent.getTouchList();
        }
        // get the touchlist
        else if(ev.touches) {
            return ev.touches;
        }
        // make fake touchlist from mouse position
        else {
            return [{
                identifier: 1,
                pageX: ev.pageX,
                pageY: ev.pageY,
                target: ev.target
            }];
        }
    },


    /**
     * collect event data for Hammer js
     * @param   {HTMLElement}   element
     * @param   {String}        eventType        like Hammer.EVENT_MOVE
     * @param   {Object}        eventData
     */
    collectEventData: function collectEventData(element, eventType, ev) {
        var touches = this.getTouchList(ev, eventType);

        // find out pointerType
        var pointerType = Hammer.POINTER_TOUCH;
        if(ev.type.match(/mouse/) || Hammer.PointerEvent.matchType(Hammer.POINTER_MOUSE, ev)) {
            pointerType = Hammer.POINTER_MOUSE;
        }

        return {
            center      : Hammer.utils.getCenter(touches),
            timeStamp   : new Date().getTime(),
            target      : ev.target,
            touches     : touches,
            eventType   : eventType,
            pointerType : pointerType,
            srcEvent    : ev,

            /**
             * prevent the browser default actions
             * mostly used to disable scrolling of the browser
             */
            preventDefault: function() {
                if(this.srcEvent.preventManipulation) {
                    this.srcEvent.preventManipulation();
                }

                if(this.srcEvent.preventDefault) {
                    this.srcEvent.preventDefault();
                }
            },

            /**
             * stop bubbling the event up to its parents
             */
            stopPropagation: function() {
                this.srcEvent.stopPropagation();
            },

            /**
             * immediately stop gesture detection
             * might be useful after a swipe was detected
             * @return {*}
             */
            stopDetect: function() {
                return Hammer.detection.stopDetect();
            }
        };
    }
};

Hammer.PointerEvent = {
    /**
     * holds all pointers
     * @type {Object}
     */
    pointers: {},

    /**
     * get a list of pointers
     * @returns {Array}     touchlist
     */
    getTouchList: function() {
        var self = this;
        var touchlist = [];

        // we can use forEach since pointerEvents only is in IE10
        Object.keys(self.pointers).sort().forEach(function(id) {
            touchlist.push(self.pointers[id]);
        });
        return touchlist;
    },

    /**
     * update the position of a pointer
     * @param   {String}   type             Hammer.EVENT_END
     * @param   {Object}   pointerEvent
     */
    updatePointer: function(type, pointerEvent) {
        if(type == Hammer.EVENT_END) {
            this.pointers = {};
        }
        else {
            pointerEvent.identifier = pointerEvent.pointerId;
            this.pointers[pointerEvent.pointerId] = pointerEvent;
        }

        return Object.keys(this.pointers).length;
    },

    /**
     * check if ev matches pointertype
     * @param   {String}        pointerType     Hammer.POINTER_MOUSE
     * @param   {PointerEvent}  ev
     */
    matchType: function(pointerType, ev) {
        if(!ev.pointerType) {
            return false;
        }

        var types = {};
        types[Hammer.POINTER_MOUSE] = (ev.pointerType == ev.MSPOINTER_TYPE_MOUSE || ev.pointerType == Hammer.POINTER_MOUSE);
        types[Hammer.POINTER_TOUCH] = (ev.pointerType == ev.MSPOINTER_TYPE_TOUCH || ev.pointerType == Hammer.POINTER_TOUCH);
        types[Hammer.POINTER_PEN] = (ev.pointerType == ev.MSPOINTER_TYPE_PEN || ev.pointerType == Hammer.POINTER_PEN);
        return types[pointerType];
    },


    /**
     * get events
     */
    getEvents: function() {
        return [
            'pointerdown MSPointerDown',
            'pointermove MSPointerMove',
            'pointerup pointercancel MSPointerUp MSPointerCancel'
        ];
    },

    /**
     * reset the list
     */
    reset: function() {
        this.pointers = {};
    }
};


Hammer.utils = {
    /**
     * extend method,
     * also used for cloning when dest is an empty object
     * @param   {Object}    dest
     * @param   {Object}    src
	 * @parm	{Boolean}	merge		do a merge
     * @returns {Object}    dest
     */
    extend: function extend(dest, src, merge) {
        for (var key in src) {
			if(dest[key] !== undefined && merge) {
				continue;
			}
            dest[key] = src[key];
        }
        return dest;
    },


    /**
     * find if a node is in the given parent
     * used for event delegation tricks
     * @param   {HTMLElement}   node
     * @param   {HTMLElement}   parent
     * @returns {boolean}       has_parent
     */
    hasParent: function(node, parent) {
        while(node){
            if(node == parent) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    },


    /**
     * get the center of all the touches
     * @param   {Array}     touches
     * @returns {Object}    center
     */
    getCenter: function getCenter(touches) {
        var valuesX = [], valuesY = [];

        for(var t= 0,len=touches.length; t<len; t++) {
            valuesX.push(touches[t].pageX);
            valuesY.push(touches[t].pageY);
        }

        return {
            pageX: ((Math.min.apply(Math, valuesX) + Math.max.apply(Math, valuesX)) / 2),
            pageY: ((Math.min.apply(Math, valuesY) + Math.max.apply(Math, valuesY)) / 2)
        };
    },


    /**
     * calculate the velocity between two points
     * @param   {Number}    delta_time
     * @param   {Number}    delta_x
     * @param   {Number}    delta_y
     * @returns {Object}    velocity
     */
    getVelocity: function getVelocity(delta_time, delta_x, delta_y) {
        return {
            x: Math.abs(delta_x / delta_time) || 0,
            y: Math.abs(delta_y / delta_time) || 0
        };
    },


    /**
     * calculate the angle between two coordinates
     * @param   {Touch}     touch1
     * @param   {Touch}     touch2
     * @returns {Number}    angle
     */
    getAngle: function getAngle(touch1, touch2) {
        var y = touch2.pageY - touch1.pageY,
            x = touch2.pageX - touch1.pageX;
        return Math.atan2(y, x) * 180 / Math.PI;
    },


    /**
     * angle to direction define
     * @param   {Touch}     touch1
     * @param   {Touch}     touch2
     * @returns {String}    direction constant, like Hammer.DIRECTION_LEFT
     */
    getDirection: function getDirection(touch1, touch2) {
        var x = Math.abs(touch1.pageX - touch2.pageX),
            y = Math.abs(touch1.pageY - touch2.pageY);

        if(x >= y) {
            return touch1.pageX - touch2.pageX > 0 ? Hammer.DIRECTION_LEFT : Hammer.DIRECTION_RIGHT;
        }
        else {
            return touch1.pageY - touch2.pageY > 0 ? Hammer.DIRECTION_UP : Hammer.DIRECTION_DOWN;
        }
    },


    /**
     * calculate the distance between two touches
     * @param   {Touch}     touch1
     * @param   {Touch}     touch2
     * @returns {Number}    distance
     */
    getDistance: function getDistance(touch1, touch2) {
        var x = touch2.pageX - touch1.pageX,
            y = touch2.pageY - touch1.pageY;
        return Math.sqrt((x*x) + (y*y));
    },


    /**
     * calculate the scale factor between two touchLists (fingers)
     * no scale is 1, and goes down to 0 when pinched together, and bigger when pinched out
     * @param   {Array}     start
     * @param   {Array}     end
     * @returns {Number}    scale
     */
    getScale: function getScale(start, end) {
        // need two fingers...
        if(start.length >= 2 && end.length >= 2) {
            return this.getDistance(end[0], end[1]) /
                this.getDistance(start[0], start[1]);
        }
        return 1;
    },


    /**
     * calculate the rotation degrees between two touchLists (fingers)
     * @param   {Array}     start
     * @param   {Array}     end
     * @returns {Number}    rotation
     */
    getRotation: function getRotation(start, end) {
        // need two fingers
        if(start.length >= 2 && end.length >= 2) {
            return this.getAngle(end[1], end[0]) -
                this.getAngle(start[1], start[0]);
        }
        return 0;
    },


    /**
     * boolean if the direction is vertical
     * @param    {String}    direction
     * @returns  {Boolean}   is_vertical
     */
    isVertical: function isVertical(direction) {
        return (direction == Hammer.DIRECTION_UP || direction == Hammer.DIRECTION_DOWN);
    },


    /**
     * stop browser default behavior with css props
     * @param   {HtmlElement}   element
     * @param   {Object}        css_props
     */
    stopDefaultBrowserBehavior: function stopDefaultBrowserBehavior(element, css_props) {
        var prop,
            vendors = ['webkit','khtml','moz','ms','o',''];

        if(!css_props || !element.style) {
            return;
        }

        // with css properties for modern browsers
        for(var i = 0; i < vendors.length; i++) {
            for(var p in css_props) {
                if(css_props.hasOwnProperty(p)) {
                    prop = p;

                    // vender prefix at the property
                    if(vendors[i]) {
                        prop = vendors[i] + prop.substring(0, 1).toUpperCase() + prop.substring(1);
                    }

                    // set the style
                    element.style[prop] = css_props[p];
                }
            }
        }

        // also the disable onselectstart
        if(css_props.userSelect == 'none') {
            element.onselectstart = function() {
                return false;
            };
        }
    }
};

Hammer.detection = {
    // contains all registred Hammer.gestures in the correct order
    gestures: [],

    // data of the current Hammer.gesture detection session
    current: null,

    // the previous Hammer.gesture session data
    // is a full clone of the previous gesture.current object
    previous: null,

    // when this becomes true, no gestures are fired
    stopped: false,


    /**
     * start Hammer.gesture detection
     * @param   {Hammer.Instance}   inst
     * @param   {Object}            eventData
     */
    startDetect: function startDetect(inst, eventData) {
        // already busy with a Hammer.gesture detection on an element
        if(this.current) {
            return;
        }

        this.stopped = false;

        this.current = {
            inst        : inst, // reference to HammerInstance we're working for
            startEvent  : Hammer.utils.extend({}, eventData), // start eventData for distances, timing etc
            lastEvent   : false, // last eventData
            name        : '' // current gesture we're in/detected, can be 'tap', 'hold' etc
        };

        this.detect(eventData);
    },


    /**
     * Hammer.gesture detection
     * @param   {Object}    eventData
     * @param   {Object}    eventData
     */
    detect: function detect(eventData) {
        if(!this.current || this.stopped) {
            return;
        }

        // extend event data with calculations about scale, distance etc
        eventData = this.extendEventData(eventData);

        // instance options
        var inst_options = this.current.inst.options;

        // call Hammer.gesture handlers
        for(var g=0,len=this.gestures.length; g<len; g++) {
            var gesture = this.gestures[g];

            // only when the instance options have enabled this gesture
            if(!this.stopped && inst_options[gesture.name] !== false) {
                // if a handler returns false, we stop with the detection
                if(gesture.handler.call(gesture, eventData, this.current.inst) === false) {
                    this.stopDetect();
                    break;
                }
            }
        }

        // store as previous event event
        if(this.current) {
            this.current.lastEvent = eventData;
        }

        // endevent, but not the last touch, so dont stop
        if(eventData.eventType == Hammer.EVENT_END && !eventData.touches.length-1) {
            this.stopDetect();
        }

        return eventData;
    },


    /**
     * clear the Hammer.gesture vars
     * this is called on endDetect, but can also be used when a final Hammer.gesture has been detected
     * to stop other Hammer.gestures from being fired
     */
    stopDetect: function stopDetect() {
        // clone current data to the store as the previous gesture
        // used for the double tap gesture, since this is an other gesture detect session
        this.previous = Hammer.utils.extend({}, this.current);

        // reset the current
        this.current = null;

        // stopped!
        this.stopped = true;
    },


    /**
     * extend eventData for Hammer.gestures
     * @param   {Object}   ev
     * @returns {Object}   ev
     */
    extendEventData: function extendEventData(ev) {
        var startEv = this.current.startEvent;

        // if the touches change, set the new touches over the startEvent touches
        // this because touchevents don't have all the touches on touchstart, or the
        // user must place his fingers at the EXACT same time on the screen, which is not realistic
        // but, sometimes it happens that both fingers are touching at the EXACT same time
        if(startEv && (ev.touches.length != startEv.touches.length || ev.touches === startEv.touches)) {
            // extend 1 level deep to get the touchlist with the touch objects
            startEv.touches = [];
            for(var i=0,len=ev.touches.length; i<len; i++) {
                startEv.touches.push(Hammer.utils.extend({}, ev.touches[i]));
            }
        }

        var delta_time = ev.timeStamp - startEv.timeStamp,
            delta_x = ev.center.pageX - startEv.center.pageX,
            delta_y = ev.center.pageY - startEv.center.pageY,
            velocity = Hammer.utils.getVelocity(delta_time, delta_x, delta_y);

        Hammer.utils.extend(ev, {
            deltaTime   : delta_time,

            deltaX      : delta_x,
            deltaY      : delta_y,

            velocityX   : velocity.x,
            velocityY   : velocity.y,

            distance    : Hammer.utils.getDistance(startEv.center, ev.center),
            angle       : Hammer.utils.getAngle(startEv.center, ev.center),
            direction   : Hammer.utils.getDirection(startEv.center, ev.center),

            scale       : Hammer.utils.getScale(startEv.touches, ev.touches),
            rotation    : Hammer.utils.getRotation(startEv.touches, ev.touches),

            startEvent  : startEv
        });

        return ev;
    },


    /**
     * register new gesture
     * @param   {Object}    gesture object, see gestures.js for documentation
     * @returns {Array}     gestures
     */
    register: function register(gesture) {
        // add an enable gesture options if there is no given
        var options = gesture.defaults || {};
        if(options[gesture.name] === undefined) {
            options[gesture.name] = true;
        }

        // extend Hammer default options with the Hammer.gesture options
        Hammer.utils.extend(Hammer.defaults, options, true);

        // set its index
        gesture.index = gesture.index || 1000;

        // add Hammer.gesture to the list
        this.gestures.push(gesture);

        // sort the list by index
        this.gestures.sort(function(a, b) {
            if (a.index < b.index) {
                return -1;
            }
            if (a.index > b.index) {
                return 1;
            }
            return 0;
        });

        return this.gestures;
    }
};


Hammer.gestures = Hammer.gestures || {};

/**
 * Custom gestures
 * ==============================
 *
 * Gesture object
 * --------------------
 * The object structure of a gesture:
 *
 * { name: 'mygesture',
 *   index: 1337,
 *   defaults: {
 *     mygesture_option: true
 *   }
 *   handler: function(type, ev, inst) {
 *     // trigger gesture event
 *     inst.trigger(this.name, ev);
 *   }
 * }

 * @param   {String}    name
 * this should be the name of the gesture, lowercase
 * it is also being used to disable/enable the gesture per instance config.
 *
 * @param   {Number}    [index=1000]
 * the index of the gesture, where it is going to be in the stack of gestures detection
 * like when you build an gesture that depends on the drag gesture, it is a good
 * idea to place it after the index of the drag gesture.
 *
 * @param   {Object}    [defaults={}]
 * the default settings of the gesture. these are added to the instance settings,
 * and can be overruled per instance. you can also add the name of the gesture,
 * but this is also added by default (and set to true).
 *
 * @param   {Function}  handler
 * this handles the gesture detection of your custom gesture and receives the
 * following arguments:
 *
 *      @param  {Object}    eventData
 *      event data containing the following properties:
 *          timeStamp   {Number}        time the event occurred
 *          target      {HTMLElement}   target element
 *          touches     {Array}         touches (fingers, pointers, mouse) on the screen
 *          pointerType {String}        kind of pointer that was used. matches Hammer.POINTER_MOUSE|TOUCH
 *          center      {Object}        center position of the touches. contains pageX and pageY
 *          deltaTime   {Number}        the total time of the touches in the screen
 *          deltaX      {Number}        the delta on x axis we haved moved
 *          deltaY      {Number}        the delta on y axis we haved moved
 *          velocityX   {Number}        the velocity on the x
 *          velocityY   {Number}        the velocity on y
 *          angle       {Number}        the angle we are moving
 *          direction   {String}        the direction we are moving. matches Hammer.DIRECTION_UP|DOWN|LEFT|RIGHT
 *          distance    {Number}        the distance we haved moved
 *          scale       {Number}        scaling of the touches, needs 2 touches
 *          rotation    {Number}        rotation of the touches, needs 2 touches *
 *          eventType   {String}        matches Hammer.EVENT_START|MOVE|END
 *          srcEvent    {Object}        the source event, like TouchStart or MouseDown *
 *          startEvent  {Object}        contains the same properties as above,
 *                                      but from the first touch. this is used to calculate
 *                                      distances, deltaTime, scaling etc
 *
 *      @param  {Hammer.Instance}    inst
 *      the instance we are doing the detection for. you can get the options from
 *      the inst.options object and trigger the gesture event by calling inst.trigger
 *
 *
 * Handle gestures
 * --------------------
 * inside the handler you can get/set Hammer.detection.current. This is the current
 * detection session. It has the following properties
 *      @param  {String}    name
 *      contains the name of the gesture we have detected. it has not a real function,
 *      only to check in other gestures if something is detected.
 *      like in the drag gesture we set it to 'drag' and in the swipe gesture we can
 *      check if the current gesture is 'drag' by accessing Hammer.detection.current.name
 *
 *      @readonly
 *      @param  {Hammer.Instance}    inst
 *      the instance we do the detection for
 *
 *      @readonly
 *      @param  {Object}    startEvent
 *      contains the properties of the first gesture detection in this session.
 *      Used for calculations about timing, distance, etc.
 *
 *      @readonly
 *      @param  {Object}    lastEvent
 *      contains all the properties of the last gesture detect in this session.
 *
 * after the gesture detection session has been completed (user has released the screen)
 * the Hammer.detection.current object is copied into Hammer.detection.previous,
 * this is usefull for gestures like doubletap, where you need to know if the
 * previous gesture was a tap
 *
 * options that have been set by the instance can be received by calling inst.options
 *
 * You can trigger a gesture event by calling inst.trigger("mygesture", event).
 * The first param is the name of your gesture, the second the event argument
 *
 *
 * Register gestures
 * --------------------
 * When an gesture is added to the Hammer.gestures object, it is auto registered
 * at the setup of the first Hammer instance. You can also call Hammer.detection.register
 * manually and pass your gesture object as a param
 *
 */

/**
 * Hold
 * Touch stays at the same place for x time
 * @events  hold
 */
Hammer.gestures.Hold = {
    name: 'hold',
    index: 10,
    defaults: {
        hold_timeout	: 500,
        hold_threshold	: 1
    },
    timer: null,
    handler: function holdGesture(ev, inst) {
        switch(ev.eventType) {
            case Hammer.EVENT_START:
                // clear any running timers
                clearTimeout(this.timer);

                // set the gesture so we can check in the timeout if it still is
                Hammer.detection.current.name = this.name;

                // set timer and if after the timeout it still is hold,
                // we trigger the hold event
                this.timer = setTimeout(function() {
                    if(Hammer.detection.current.name == 'hold') {
                        inst.trigger('hold', ev);
                    }
                }, inst.options.hold_timeout);
                break;

            // when you move or end we clear the timer
            case Hammer.EVENT_MOVE:
                if(ev.distance > inst.options.hold_threshold) {
                    clearTimeout(this.timer);
                }
                break;

            case Hammer.EVENT_END:
                clearTimeout(this.timer);
                break;
        }
    }
};


/**
 * Tap/DoubleTap
 * Quick touch at a place or double at the same place
 * @events  tap, doubletap
 */
Hammer.gestures.Tap = {
    name: 'tap',
    index: 100,
    defaults: {
        tap_max_touchtime	: 250,
        tap_max_distance	: 10,
		tap_always			: true,
        doubletap_distance	: 20,
        doubletap_interval	: 300
    },
    handler: function tapGesture(ev, inst) {
        if(ev.eventType == Hammer.EVENT_END) {
            // previous gesture, for the double tap since these are two different gesture detections
            var prev = Hammer.detection.previous,
				did_doubletap = false;

            // when the touchtime is higher then the max touch time
            // or when the moving distance is too much
            if(ev.deltaTime > inst.options.tap_max_touchtime ||
                ev.distance > inst.options.tap_max_distance) {
                return;
            }

            // check if double tap
            if(prev && prev.name == 'tap' &&
                (ev.timeStamp - prev.lastEvent.timeStamp) < inst.options.doubletap_interval &&
                ev.distance < inst.options.doubletap_distance) {
				inst.trigger('doubletap', ev);
				did_doubletap = true;
            }

			// do a single tap
			if(!did_doubletap || inst.options.tap_always) {
				Hammer.detection.current.name = 'tap';
				inst.trigger(Hammer.detection.current.name, ev);
			}
        }
    }
};


/**
 * Swipe
 * triggers swipe events when the end velocity is above the threshold
 * @events  swipe, swipeleft, swiperight, swipeup, swipedown
 */
Hammer.gestures.Swipe = {
    name: 'swipe',
    index: 40,
    defaults: {
        // set 0 for unlimited, but this can conflict with transform
        swipe_max_touches  : 1,
        swipe_velocity     : 0.7
    },
    handler: function swipeGesture(ev, inst) {
        if(ev.eventType == Hammer.EVENT_END) {
            // max touches
            if(inst.options.swipe_max_touches > 0 &&
                ev.touches.length > inst.options.swipe_max_touches) {
                return;
            }

            // when the distance we moved is too small we skip this gesture
            // or we can be already in dragging
            if(ev.velocityX > inst.options.swipe_velocity ||
                ev.velocityY > inst.options.swipe_velocity) {
                // trigger swipe events
                inst.trigger(this.name, ev);
                inst.trigger(this.name + ev.direction, ev);
            }
        }
    }
};


/**
 * Drag
 * Move with x fingers (default 1) around on the page. Blocking the scrolling when
 * moving left and right is a good practice. When all the drag events are blocking
 * you disable scrolling on that area.
 * @events  drag, drapleft, dragright, dragup, dragdown
 */
Hammer.gestures.Drag = {
    name: 'drag',
    index: 50,
    defaults: {
        drag_min_distance : 10,
        // set 0 for unlimited, but this can conflict with transform
        drag_max_touches  : 1,
        // prevent default browser behavior when dragging occurs
        // be careful with it, it makes the element a blocking element
        // when you are using the drag gesture, it is a good practice to set this true
        drag_block_horizontal   : false,
        drag_block_vertical     : false,
        // drag_lock_to_axis keeps the drag gesture on the axis that it started on,
        // It disallows vertical directions if the initial direction was horizontal, and vice versa.
        drag_lock_to_axis       : false,
        // drag lock only kicks in when distance > drag_lock_min_distance
        // This way, locking occurs only when the distance has become large enough to reliably determine the direction
        drag_lock_min_distance : 25
    },
    triggered: false,
    handler: function dragGesture(ev, inst) {
        // current gesture isnt drag, but dragged is true
        // this means an other gesture is busy. now call dragend
        if(Hammer.detection.current.name != this.name && this.triggered) {
            inst.trigger(this.name +'end', ev);
            this.triggered = false;
            return;
        }

        // max touches
        if(inst.options.drag_max_touches > 0 &&
            ev.touches.length > inst.options.drag_max_touches) {
            return;
        }

        switch(ev.eventType) {
            case Hammer.EVENT_START:
                this.triggered = false;
                break;

            case Hammer.EVENT_MOVE:
                // when the distance we moved is too small we skip this gesture
                // or we can be already in dragging
                if(ev.distance < inst.options.drag_min_distance &&
                    Hammer.detection.current.name != this.name) {
                    return;
                }

                // we are dragging!
                Hammer.detection.current.name = this.name;

                // lock drag to axis?
                if(Hammer.detection.current.lastEvent.drag_locked_to_axis || (inst.options.drag_lock_to_axis && inst.options.drag_lock_min_distance<=ev.distance)) {
                    ev.drag_locked_to_axis = true;
                }
                var last_direction = Hammer.detection.current.lastEvent.direction;
                if(ev.drag_locked_to_axis && last_direction !== ev.direction) {
                    // keep direction on the axis that the drag gesture started on
                    if(Hammer.utils.isVertical(last_direction)) {
                        ev.direction = (ev.deltaY < 0) ? Hammer.DIRECTION_UP : Hammer.DIRECTION_DOWN;
                    }
                    else {
                        ev.direction = (ev.deltaX < 0) ? Hammer.DIRECTION_LEFT : Hammer.DIRECTION_RIGHT;
                    }
                }

                // first time, trigger dragstart event
                if(!this.triggered) {
                    inst.trigger(this.name +'start', ev);
                    this.triggered = true;
                }

                // trigger normal event
                inst.trigger(this.name, ev);

                // direction event, like dragdown
                inst.trigger(this.name + ev.direction, ev);

                // block the browser events
                if( (inst.options.drag_block_vertical && Hammer.utils.isVertical(ev.direction)) ||
                    (inst.options.drag_block_horizontal && !Hammer.utils.isVertical(ev.direction))) {
                    ev.preventDefault();
                }
                break;

            case Hammer.EVENT_END:
                // trigger dragend
                if(this.triggered) {
                    inst.trigger(this.name +'end', ev);
                }

                this.triggered = false;
                break;
        }
    }
};


/**
 * Transform
 * User want to scale or rotate with 2 fingers
 * @events  transform, pinch, pinchin, pinchout, rotate
 */
Hammer.gestures.Transform = {
    name: 'transform',
    index: 45,
    defaults: {
        // factor, no scale is 1, zoomin is to 0 and zoomout until higher then 1
        transform_min_scale     : 0.01,
        // rotation in degrees
        transform_min_rotation  : 1,
        // prevent default browser behavior when two touches are on the screen
        // but it makes the element a blocking element
        // when you are using the transform gesture, it is a good practice to set this true
        transform_always_block  : false
    },
    triggered: false,
    handler: function transformGesture(ev, inst) {
        // current gesture isnt drag, but dragged is true
        // this means an other gesture is busy. now call dragend
        if(Hammer.detection.current.name != this.name && this.triggered) {
            inst.trigger(this.name +'end', ev);
            this.triggered = false;
            return;
        }

        // atleast multitouch
        if(ev.touches.length < 2) {
            return;
        }

        // prevent default when two fingers are on the screen
        if(inst.options.transform_always_block) {
            ev.preventDefault();
        }

        switch(ev.eventType) {
            case Hammer.EVENT_START:
                this.triggered = false;
                break;

            case Hammer.EVENT_MOVE:
                var scale_threshold = Math.abs(1-ev.scale);
                var rotation_threshold = Math.abs(ev.rotation);

                // when the distance we moved is too small we skip this gesture
                // or we can be already in dragging
                if(scale_threshold < inst.options.transform_min_scale &&
                    rotation_threshold < inst.options.transform_min_rotation) {
                    return;
                }

                // we are transforming!
                Hammer.detection.current.name = this.name;

                // first time, trigger dragstart event
                if(!this.triggered) {
                    inst.trigger(this.name +'start', ev);
                    this.triggered = true;
                }

                inst.trigger(this.name, ev); // basic transform event

                // trigger rotate event
                if(rotation_threshold > inst.options.transform_min_rotation) {
                    inst.trigger('rotate', ev);
                }

                // trigger pinch event
                if(scale_threshold > inst.options.transform_min_scale) {
                    inst.trigger('pinch', ev);
                    inst.trigger('pinch'+ ((ev.scale < 1) ? 'in' : 'out'), ev);
                }
                break;

            case Hammer.EVENT_END:
                // trigger dragend
                if(this.triggered) {
                    inst.trigger(this.name +'end', ev);
                }

                this.triggered = false;
                break;
        }
    }
};


/**
 * Touch
 * Called as first, tells the user has touched the screen
 * @events  touch
 */
Hammer.gestures.Touch = {
    name: 'touch',
    index: -Infinity,
    defaults: {
        // call preventDefault at touchstart, and makes the element blocking by
        // disabling the scrolling of the page, but it improves gestures like
        // transforming and dragging.
        // be careful with using this, it can be very annoying for users to be stuck
        // on the page
        prevent_default: false,

        // disable mouse events, so only touch (or pen!) input triggers events
        prevent_mouseevents: false
    },
    handler: function touchGesture(ev, inst) {
        if(inst.options.prevent_mouseevents && ev.pointerType == Hammer.POINTER_MOUSE) {
            ev.stopDetect();
            return;
        }

        if(inst.options.prevent_default) {
            ev.preventDefault();
        }

        if(ev.eventType ==  Hammer.EVENT_START) {
            inst.trigger(this.name, ev);
        }
    }
};


/**
 * Release
 * Called as last, tells the user has released the screen
 * @events  release
 */
Hammer.gestures.Release = {
    name: 'release',
    index: Infinity,
    handler: function releaseGesture(ev, inst) {
        if(ev.eventType ==  Hammer.EVENT_END) {
            inst.trigger(this.name, ev);
        }
    }
};

// node export
if(typeof module === 'object' && typeof module.exports === 'object'){
    module.exports = Hammer;
}
// just window export
else {
    window.Hammer = Hammer;

    // requireJS module definition
    if(typeof window.define === 'function' && window.define.amd) {
        window.define('hammer', [], function() {
            return Hammer;
        });
    }
}
})(this);

(function($, undefined) {
    'use strict';

    // no jQuery or Zepto!
    if($ === undefined) {
        return;
    }

    /**
     * bind dom events
     * this overwrites addEventListener
     * @param   {HTMLElement}   element
     * @param   {String}        eventTypes
     * @param   {Function}      handler
     */
    Hammer.event.bindDom = function(element, eventTypes, handler) {
        $(element).on(eventTypes, function(ev) {
            var data = ev.originalEvent || ev;

            // IE pageX fix
            if(data.pageX === undefined) {
                data.pageX = ev.pageX;
                data.pageY = ev.pageY;
            }

            // IE target fix
            if(!data.target) {
                data.target = ev.target;
            }

            // IE button fix
            if(data.which === undefined) {
                data.which = data.button;
            }

            // IE preventDefault
            if(!data.preventDefault) {
                data.preventDefault = ev.preventDefault;
            }

            // IE stopPropagation
            if(!data.stopPropagation) {
                data.stopPropagation = ev.stopPropagation;
            }

            handler.call(this, data);
        });
    };

    /**
     * the methods are called by the instance, but with the jquery plugin
     * we use the jquery event methods instead.
     * @this    {Hammer.Instance}
     * @return  {jQuery}
     */
    Hammer.Instance.prototype.on = function(types, handler) {
        return $(this.element).on(types, handler);
    };
    Hammer.Instance.prototype.off = function(types, handler) {
        return $(this.element).off(types, handler);
    };


    /**
     * trigger events
     * this is called by the gestures to trigger an event like 'tap'
     * @this    {Hammer.Instance}
     * @param   {String}    gesture
     * @param   {Object}    eventData
     * @return  {jQuery}
     */
    Hammer.Instance.prototype.trigger = function(gesture, eventData){
        var el = $(this.element);
        if(el.has(eventData.target).length) {
            el = $(eventData.target);
        }

        return el.trigger({
            type: gesture,
            gesture: eventData
        });
    };


    /**
     * jQuery plugin
     * create instance of Hammer and watch for gestures,
     * and when called again you can change the options
     * @param   {Object}    [options={}]
     * @return  {jQuery}
     */
    $.fn.hammer = function(options) {
        return this.each(function() {
            var el = $(this);
            var inst = el.data('hammer');
            // start new hammer instance
            if(!inst) {
                el.data('hammer', new Hammer(this, options || {}));
            }
            // change the options
            else if(inst && options) {
                Hammer.utils.extend(inst.options, options);
            }
        });
    };

})(window.jQuery || window.Zepto);

define("hammer", (function (global) {
    return function () {
        var ret, fn;
        return ret || global.Hammer;
    };
}(this)));

/*!
 * VERSION: beta 1.10.3
 * DATE: 2013-09-02
 * UPDATES AND DOCS AT: http://www.greensock.com
 *
 * @license Copyright (c) 2008-2013, GreenSock. All rights reserved.
 * This work is subject to the terms at http://www.greensock.com/terms_of_use.html or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
(function(t){"use strict";var e,i,s,n,r,a=t.GreenSockGlobals||t,o=function(t){var e,i=t.split("."),s=a;for(e=0;i.length>e;e++)s[i[e]]=s=s[i[e]]||{};return s},l=o("com.greensock"),_=[].slice,h=function(){},u={},m=function(e,i,s,n){this.sc=u[e]?u[e].sc:[],u[e]=this,this.gsClass=null,this.func=s;var r=[];this.check=function(l){for(var _,h,f,p,c=i.length,d=c;--c>-1;)(_=u[i[c]]||new m(i[c],[])).gsClass?(r[c]=_.gsClass,d--):l&&_.sc.push(this);if(0===d&&s)for(h=("com.greensock."+e).split("."),f=h.pop(),p=o(h.join("."))[f]=this.gsClass=s.apply(s,r),n&&(a[f]=p,"function"==typeof define&&define.amd?define((t.GreenSockAMDPath?t.GreenSockAMDPath+"/":"")+e.split(".").join("/"),[],function(){return p}):"undefined"!=typeof module&&module.exports&&(module.exports=p)),c=0;this.sc.length>c;c++)this.sc[c].check()},this.check(!0)},f=t._gsDefine=function(t,e,i,s){return new m(t,e,i,s)},p=l._class=function(t,e,i){return e=e||function(){},f(t,[],function(){return e},i),e};f.globals=a;var c=[0,0,1,1],d=[],v=p("easing.Ease",function(t,e,i,s){this._func=t,this._type=i||0,this._power=s||0,this._params=e?c.concat(e):c},!0),g=v.map={},T=v.register=function(t,e,i,s){for(var n,r,a,o,_=e.split(","),h=_.length,u=(i||"easeIn,easeOut,easeInOut").split(",");--h>-1;)for(r=_[h],n=s?p("easing."+r,null,!0):l.easing[r]||{},a=u.length;--a>-1;)o=u[a],g[r+"."+o]=g[o+r]=n[o]=t.getRatio?t:t[o]||new t};for(s=v.prototype,s._calcEnd=!1,s.getRatio=function(t){if(this._func)return this._params[0]=t,this._func.apply(null,this._params);var e=this._type,i=this._power,s=1===e?1-t:2===e?t:.5>t?2*t:2*(1-t);return 1===i?s*=s:2===i?s*=s*s:3===i?s*=s*s*s:4===i&&(s*=s*s*s*s),1===e?1-s:2===e?s:.5>t?s/2:1-s/2},e=["Linear","Quad","Cubic","Quart","Quint,Strong"],i=e.length;--i>-1;)s=e[i]+",Power"+i,T(new v(null,null,1,i),s,"easeOut",!0),T(new v(null,null,2,i),s,"easeIn"+(0===i?",easeNone":"")),T(new v(null,null,3,i),s,"easeInOut");g.linear=l.easing.Linear.easeIn,g.swing=l.easing.Quad.easeInOut;var w=p("events.EventDispatcher",function(t){this._listeners={},this._eventTarget=t||this});s=w.prototype,s.addEventListener=function(t,e,i,s,a){a=a||0;var o,l,_=this._listeners[t],h=0;for(null==_&&(this._listeners[t]=_=[]),l=_.length;--l>-1;)o=_[l],o.c===e&&o.s===i?_.splice(l,1):0===h&&a>o.pr&&(h=l+1);_.splice(h,0,{c:e,s:i,up:s,pr:a}),this!==n||r||n.wake()},s.removeEventListener=function(t,e){var i,s=this._listeners[t];if(s)for(i=s.length;--i>-1;)if(s[i].c===e)return s.splice(i,1),void 0},s.dispatchEvent=function(t){var e,i,s,n=this._listeners[t];if(n)for(e=n.length,i=this._eventTarget;--e>-1;)s=n[e],s.up?s.c.call(s.s||i,{type:t,target:i}):s.c.call(s.s||i)};var P=t.requestAnimationFrame,y=t.cancelAnimationFrame,k=Date.now||function(){return(new Date).getTime()},b=k();for(e=["ms","moz","webkit","o"],i=e.length;--i>-1&&!P;)P=t[e[i]+"RequestAnimationFrame"],y=t[e[i]+"CancelAnimationFrame"]||t[e[i]+"CancelRequestAnimationFrame"];p("Ticker",function(t,e){var i,s,a,o,l,_=this,u=k(),m=e!==!1&&P,f=function(t){b=k(),_.time=(b-u)/1e3;var e,n=_.time-l;(!i||n>0||t===!0)&&(_.frame++,l+=n+(n>=o?.004:o-n),e=!0),t!==!0&&(a=s(f)),e&&_.dispatchEvent("tick")};w.call(_),_.time=_.frame=0,_.tick=function(){f(!0)},_.sleep=function(){null!=a&&(m&&y?y(a):clearTimeout(a),s=h,a=null,_===n&&(r=!1))},_.wake=function(){null!==a&&_.sleep(),s=0===i?h:m&&P?P:function(t){return setTimeout(t,0|1e3*(l-_.time)+1)},_===n&&(r=!0),f(2)},_.fps=function(t){return arguments.length?(i=t,o=1/(i||60),l=this.time+o,_.wake(),void 0):i},_.useRAF=function(t){return arguments.length?(_.sleep(),m=t,_.fps(i),void 0):m},_.fps(t),setTimeout(function(){m&&(!a||5>_.frame)&&_.useRAF(!1)},1500)}),s=l.Ticker.prototype=new l.events.EventDispatcher,s.constructor=l.Ticker;var A=p("core.Animation",function(t,e){if(this.vars=e=e||{},this._duration=this._totalDuration=t||0,this._delay=Number(e.delay)||0,this._timeScale=1,this._active=e.immediateRender===!0,this.data=e.data,this._reversed=e.reversed===!0,j){r||n.wake();var i=this.vars.useFrames?F:j;i.add(this,i._time),this.vars.paused&&this.paused(!0)}});n=A.ticker=new l.Ticker,s=A.prototype,s._dirty=s._gc=s._initted=s._paused=!1,s._totalTime=s._time=0,s._rawPrevTime=-1,s._next=s._last=s._onUpdate=s._timeline=s.timeline=null,s._paused=!1;var S=function(){k()-b>2e3&&n.wake(),setTimeout(S,2e3)};S(),s.play=function(t,e){return arguments.length&&this.seek(t,e),this.reversed(!1).paused(!1)},s.pause=function(t,e){return arguments.length&&this.seek(t,e),this.paused(!0)},s.resume=function(t,e){return arguments.length&&this.seek(t,e),this.paused(!1)},s.seek=function(t,e){return this.totalTime(Number(t),e!==!1)},s.restart=function(t,e){return this.reversed(!1).paused(!1).totalTime(t?-this._delay:0,e!==!1,!0)},s.reverse=function(t,e){return arguments.length&&this.seek(t||this.totalDuration(),e),this.reversed(!0).paused(!1)},s.render=function(){},s.invalidate=function(){return this},s._enabled=function(t,e){return r||n.wake(),this._gc=!t,this._active=t&&!this._paused&&this._totalTime>0&&this._totalTime<this._totalDuration,e!==!0&&(t&&!this.timeline?this._timeline.add(this,this._startTime-this._delay):!t&&this.timeline&&this._timeline._remove(this,!0)),!1},s._kill=function(){return this._enabled(!1,!1)},s.kill=function(t,e){return this._kill(t,e),this},s._uncache=function(t){for(var e=t?this:this.timeline;e;)e._dirty=!0,e=e.timeline;return this},s._swapSelfInParams=function(t){for(var e=t.length,i=t.concat();--e>-1;)"{self}"===t[e]&&(i[e]=this);return i},s.eventCallback=function(t,e,i,s){if("on"===(t||"").substr(0,2)){var n=this.vars;if(1===arguments.length)return n[t];null==e?delete n[t]:(n[t]=e,n[t+"Params"]=i instanceof Array&&-1!==i.join("").indexOf("{self}")?this._swapSelfInParams(i):i,n[t+"Scope"]=s),"onUpdate"===t&&(this._onUpdate=e)}return this},s.delay=function(t){return arguments.length?(this._timeline.smoothChildTiming&&this.startTime(this._startTime+t-this._delay),this._delay=t,this):this._delay},s.duration=function(t){return arguments.length?(this._duration=this._totalDuration=t,this._uncache(!0),this._timeline.smoothChildTiming&&this._time>0&&this._time<this._duration&&0!==t&&this.totalTime(this._totalTime*(t/this._duration),!0),this):(this._dirty=!1,this._duration)},s.totalDuration=function(t){return this._dirty=!1,arguments.length?this.duration(t):this._totalDuration},s.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),this.totalTime(t>this._duration?this._duration:t,e)):this._time},s.totalTime=function(t,e,i){if(r||n.wake(),!arguments.length)return this._totalTime;if(this._timeline){if(0>t&&!i&&(t+=this.totalDuration()),this._timeline.smoothChildTiming){this._dirty&&this.totalDuration();var s=this._totalDuration,a=this._timeline;if(t>s&&!i&&(t=s),this._startTime=(this._paused?this._pauseTime:a._time)-(this._reversed?s-t:t)/this._timeScale,a._dirty||this._uncache(!1),a._timeline)for(;a._timeline;)a._timeline._time!==(a._startTime+a._totalTime)/a._timeScale&&a.totalTime(a._totalTime,!0),a=a._timeline}this._gc&&this._enabled(!0,!1),this._totalTime!==t&&this.render(t,e,!1)}return this},s.startTime=function(t){return arguments.length?(t!==this._startTime&&(this._startTime=t,this.timeline&&this.timeline._sortChildren&&this.timeline.add(this,t-this._delay)),this):this._startTime},s.timeScale=function(t){if(!arguments.length)return this._timeScale;if(t=t||1e-6,this._timeline&&this._timeline.smoothChildTiming){var e=this._pauseTime,i=e||0===e?e:this._timeline.totalTime();this._startTime=i-(i-this._startTime)*this._timeScale/t}return this._timeScale=t,this._uncache(!1)},s.reversed=function(t){return arguments.length?(t!=this._reversed&&(this._reversed=t,this.totalTime(this._totalTime,!0)),this):this._reversed},s.paused=function(t){if(!arguments.length)return this._paused;if(t!=this._paused&&this._timeline){r||t||n.wake();var e=this._timeline,i=e.rawTime(),s=i-this._pauseTime;!t&&e.smoothChildTiming&&(this._startTime+=s,this._uncache(!1)),this._pauseTime=t?i:null,this._paused=t,this._active=!t&&this._totalTime>0&&this._totalTime<this._totalDuration,t||0===s||0===this._duration||this.render(e.smoothChildTiming?this._totalTime:(i-this._startTime)/this._timeScale,!0,!0)}return this._gc&&!t&&this._enabled(!0,!1),this};var x=p("core.SimpleTimeline",function(t){A.call(this,0,t),this.autoRemoveChildren=this.smoothChildTiming=!0});s=x.prototype=new A,s.constructor=x,s.kill()._gc=!1,s._first=s._last=null,s._sortChildren=!1,s.add=s.insert=function(t,e){var i,s;if(t._startTime=Number(e||0)+t._delay,t._paused&&this!==t._timeline&&(t._pauseTime=t._startTime+(this.rawTime()-t._startTime)/t._timeScale),t.timeline&&t.timeline._remove(t,!0),t.timeline=t._timeline=this,t._gc&&t._enabled(!0,!0),i=this._last,this._sortChildren)for(s=t._startTime;i&&i._startTime>s;)i=i._prev;return i?(t._next=i._next,i._next=t):(t._next=this._first,this._first=t),t._next?t._next._prev=t:this._last=t,t._prev=i,this._timeline&&this._uncache(!0),this},s._remove=function(t,e){return t.timeline===this&&(e||t._enabled(!1,!0),t.timeline=null,t._prev?t._prev._next=t._next:this._first===t&&(this._first=t._next),t._next?t._next._prev=t._prev:this._last===t&&(this._last=t._prev),this._timeline&&this._uncache(!0)),this},s.render=function(t,e,i){var s,n=this._first;for(this._totalTime=this._time=this._rawPrevTime=t;n;)s=n._next,(n._active||t>=n._startTime&&!n._paused)&&(n._reversed?n.render((n._dirty?n.totalDuration():n._totalDuration)-(t-n._startTime)*n._timeScale,e,i):n.render((t-n._startTime)*n._timeScale,e,i)),n=s},s.rawTime=function(){return r||n.wake(),this._totalTime};var C=p("TweenLite",function(e,i,s){if(A.call(this,i,s),this.render=C.prototype.render,null==e)throw"Cannot tween a null target.";this.target=e="string"!=typeof e?e:C.selector(e)||e;var n,r,a,o=e.jquery||e.length&&e!==t&&e[0]&&(e[0]===t||e[0].nodeType&&e[0].style&&!e.nodeType),l=this.vars.overwrite;if(this._overwrite=l=null==l?L[C.defaultOverwrite]:"number"==typeof l?l>>0:L[l],(o||e instanceof Array)&&"number"!=typeof e[0])for(this._targets=a=_.call(e,0),this._propLookup=[],this._siblings=[],n=0;a.length>n;n++)r=a[n],r?"string"!=typeof r?r.length&&r!==t&&r[0]&&(r[0]===t||r[0].nodeType&&r[0].style&&!r.nodeType)?(a.splice(n--,1),this._targets=a=a.concat(_.call(r,0))):(this._siblings[n]=G(r,this,!1),1===l&&this._siblings[n].length>1&&Q(r,this,null,1,this._siblings[n])):(r=a[n--]=C.selector(r),"string"==typeof r&&a.splice(n+1,1)):a.splice(n--,1);else this._propLookup={},this._siblings=G(e,this,!1),1===l&&this._siblings.length>1&&Q(e,this,null,1,this._siblings);(this.vars.immediateRender||0===i&&0===this._delay&&this.vars.immediateRender!==!1)&&this.render(-this._delay,!1,!0)},!0),R=function(e){return e.length&&e!==t&&e[0]&&(e[0]===t||e[0].nodeType&&e[0].style&&!e.nodeType)},D=function(t,e){var i,s={};for(i in t)U[i]||i in e&&"x"!==i&&"y"!==i&&"width"!==i&&"height"!==i&&"className"!==i&&"border"!==i||!(!I[i]||I[i]&&I[i]._autoCSS)||(s[i]=t[i],delete t[i]);t.css=s};s=C.prototype=new A,s.constructor=C,s.kill()._gc=!1,s.ratio=0,s._firstPT=s._targets=s._overwrittenProps=s._startAt=null,s._notifyPluginsOfEnabled=!1,C.version="1.10.3",C.defaultEase=s._ease=new v(null,null,1,1),C.defaultOverwrite="auto",C.ticker=n,C.autoSleep=!0,C.selector=t.$||t.jQuery||function(e){return t.$?(C.selector=t.$,t.$(e)):t.document?t.document.getElementById("#"===e.charAt(0)?e.substr(1):e):e};var E=C._internals={},I=C._plugins={},O=C._tweenLookup={},N=0,U=E.reservedProps={ease:1,delay:1,overwrite:1,onComplete:1,onCompleteParams:1,onCompleteScope:1,useFrames:1,runBackwards:1,startAt:1,onUpdate:1,onUpdateParams:1,onUpdateScope:1,onStart:1,onStartParams:1,onStartScope:1,onReverseComplete:1,onReverseCompleteParams:1,onReverseCompleteScope:1,onRepeat:1,onRepeatParams:1,onRepeatScope:1,easeParams:1,yoyo:1,immediateRender:1,repeat:1,repeatDelay:1,data:1,paused:1,reversed:1,autoCSS:1},L={none:0,all:1,auto:2,concurrent:3,allOnStart:4,preexisting:5,"true":1,"false":0},F=A._rootFramesTimeline=new x,j=A._rootTimeline=new x;j._startTime=n.time,F._startTime=n.frame,j._active=F._active=!0,A._updateRoot=function(){if(j.render((n.time-j._startTime)*j._timeScale,!1,!1),F.render((n.frame-F._startTime)*F._timeScale,!1,!1),!(n.frame%120)){var t,e,i;for(i in O){for(e=O[i].tweens,t=e.length;--t>-1;)e[t]._gc&&e.splice(t,1);0===e.length&&delete O[i]}if(i=j._first,(!i||i._paused)&&C.autoSleep&&!F._first&&1===n._listeners.tick.length){for(;i&&i._paused;)i=i._next;i||n.sleep()}}},n.addEventListener("tick",A._updateRoot);var G=function(t,e,i){var s,n,r=t._gsTweenID;if(O[r||(t._gsTweenID=r="t"+N++)]||(O[r]={target:t,tweens:[]}),e&&(s=O[r].tweens,s[n=s.length]=e,i))for(;--n>-1;)s[n]===e&&s.splice(n,1);return O[r].tweens},Q=function(t,e,i,s,n){var r,a,o,l;if(1===s||s>=4){for(l=n.length,r=0;l>r;r++)if((o=n[r])!==e)o._gc||o._enabled(!1,!1)&&(a=!0);else if(5===s)break;return a}var _,h=e._startTime+1e-10,u=[],m=0,f=0===e._duration;for(r=n.length;--r>-1;)(o=n[r])===e||o._gc||o._paused||(o._timeline!==e._timeline?(_=_||B(e,0,f),0===B(o,_,f)&&(u[m++]=o)):h>=o._startTime&&o._startTime+o.totalDuration()/o._timeScale+1e-10>h&&((f||!o._initted)&&2e-10>=h-o._startTime||(u[m++]=o)));for(r=m;--r>-1;)o=u[r],2===s&&o._kill(i,t)&&(a=!0),(2!==s||!o._firstPT&&o._initted)&&o._enabled(!1,!1)&&(a=!0);return a},B=function(t,e,i){for(var s=t._timeline,n=s._timeScale,r=t._startTime,a=1e-10;s._timeline;){if(r+=s._startTime,n*=s._timeScale,s._paused)return-100;s=s._timeline}return r/=n,r>e?r-e:i&&r===e||!t._initted&&2*a>r-e?a:(r+=t.totalDuration()/t._timeScale/n)>e+a?0:r-e-a};s._init=function(){var t,e,i,s,n=this.vars,r=this._overwrittenProps,a=this._duration,o=n.immediateRender,l=n.ease;if(n.startAt){if(this._startAt&&this._startAt.render(-1,!0),n.startAt.overwrite=0,n.startAt.immediateRender=!0,this._startAt=C.to(this.target,0,n.startAt),o)if(this._time>0)this._startAt=null;else if(0!==a)return}else if(n.runBackwards&&n.immediateRender&&0!==a)if(this._startAt)this._startAt.render(-1,!0),this._startAt=null;else if(0===this._time){i={};for(s in n)U[s]&&"autoCSS"!==s||(i[s]=n[s]);return i.overwrite=0,this._startAt=C.to(this.target,0,i),void 0}if(this._ease=l?l instanceof v?n.easeParams instanceof Array?l.config.apply(l,n.easeParams):l:"function"==typeof l?new v(l,n.easeParams):g[l]||C.defaultEase:C.defaultEase,this._easeType=this._ease._type,this._easePower=this._ease._power,this._firstPT=null,this._targets)for(t=this._targets.length;--t>-1;)this._initProps(this._targets[t],this._propLookup[t]={},this._siblings[t],r?r[t]:null)&&(e=!0);else e=this._initProps(this.target,this._propLookup,this._siblings,r);if(e&&C._onPluginEvent("_onInitAllProps",this),r&&(this._firstPT||"function"!=typeof this.target&&this._enabled(!1,!1)),n.runBackwards)for(i=this._firstPT;i;)i.s+=i.c,i.c=-i.c,i=i._next;this._onUpdate=n.onUpdate,this._initted=!0},s._initProps=function(e,i,s,n){var r,a,o,l,_,h;if(null==e)return!1;this.vars.css||e.style&&e!==t&&e.nodeType&&I.css&&this.vars.autoCSS!==!1&&D(this.vars,e);for(r in this.vars){if(h=this.vars[r],U[r])h instanceof Array&&-1!==h.join("").indexOf("{self}")&&(this.vars[r]=h=this._swapSelfInParams(h,this));else if(I[r]&&(l=new I[r])._onInitTween(e,this.vars[r],this)){for(this._firstPT=_={_next:this._firstPT,t:l,p:"setRatio",s:0,c:1,f:!0,n:r,pg:!0,pr:l._priority},a=l._overwriteProps.length;--a>-1;)i[l._overwriteProps[a]]=this._firstPT;(l._priority||l._onInitAllProps)&&(o=!0),(l._onDisable||l._onEnable)&&(this._notifyPluginsOfEnabled=!0)}else this._firstPT=i[r]=_={_next:this._firstPT,t:e,p:r,f:"function"==typeof e[r],n:r,pg:!1,pr:0},_.s=_.f?e[r.indexOf("set")||"function"!=typeof e["get"+r.substr(3)]?r:"get"+r.substr(3)]():parseFloat(e[r]),_.c="string"==typeof h&&"="===h.charAt(1)?parseInt(h.charAt(0)+"1",10)*Number(h.substr(2)):Number(h)-_.s||0;_&&_._next&&(_._next._prev=_)}return n&&this._kill(n,e)?this._initProps(e,i,s,n):this._overwrite>1&&this._firstPT&&s.length>1&&Q(e,this,i,this._overwrite,s)?(this._kill(i,e),this._initProps(e,i,s,n)):o},s.render=function(t,e,i){var s,n,r,a=this._time;if(t>=this._duration)this._totalTime=this._time=this._duration,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1,this._reversed||(s=!0,n="onComplete"),0===this._duration&&((0===t||0>this._rawPrevTime)&&this._rawPrevTime!==t&&(i=!0,this._rawPrevTime>0&&(n="onReverseComplete",e&&(t=-1))),this._rawPrevTime=t);else if(1e-7>t)this._totalTime=this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==a||0===this._duration&&this._rawPrevTime>0)&&(n="onReverseComplete",s=this._reversed),0>t?(this._active=!1,0===this._duration&&(this._rawPrevTime>=0&&(i=!0),this._rawPrevTime=t)):this._initted||(i=!0);else if(this._totalTime=this._time=t,this._easeType){var o=t/this._duration,l=this._easeType,_=this._easePower;(1===l||3===l&&o>=.5)&&(o=1-o),3===l&&(o*=2),1===_?o*=o:2===_?o*=o*o:3===_?o*=o*o*o:4===_&&(o*=o*o*o*o),this.ratio=1===l?1-o:2===l?o:.5>t/this._duration?o/2:1-o/2}else this.ratio=this._ease.getRatio(t/this._duration);if(this._time!==a||i){if(!this._initted){if(this._init(),!this._initted)return;this._time&&!s?this.ratio=this._ease.getRatio(this._time/this._duration):s&&this._ease._calcEnd&&(this.ratio=this._ease.getRatio(0===this._time?0:1))}for(this._active||!this._paused&&this._time!==a&&t>=0&&(this._active=!0),0===a&&(this._startAt&&(t>=0?this._startAt.render(t,e,i):n||(n="_dummyGS")),this.vars.onStart&&(0!==this._time||0===this._duration)&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||d))),r=this._firstPT;r;)r.f?r.t[r.p](r.c*this.ratio+r.s):r.t[r.p]=r.c*this.ratio+r.s,r=r._next;this._onUpdate&&(0>t&&this._startAt&&this._startAt.render(t,e,i),e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||d)),n&&(this._gc||(0>t&&this._startAt&&!this._onUpdate&&this._startAt.render(t,e,i),s&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[n]&&this.vars[n].apply(this.vars[n+"Scope"]||this,this.vars[n+"Params"]||d)))}},s._kill=function(t,e){if("all"===t&&(t=null),null==t&&(null==e||e===this.target))return this._enabled(!1,!1);e="string"!=typeof e?e||this._targets||this.target:C.selector(e)||e;var i,s,n,r,a,o,l,_;if((e instanceof Array||R(e))&&"number"!=typeof e[0])for(i=e.length;--i>-1;)this._kill(t,e[i])&&(o=!0);else{if(this._targets){for(i=this._targets.length;--i>-1;)if(e===this._targets[i]){a=this._propLookup[i]||{},this._overwrittenProps=this._overwrittenProps||[],s=this._overwrittenProps[i]=t?this._overwrittenProps[i]||{}:"all";break}}else{if(e!==this.target)return!1;a=this._propLookup,s=this._overwrittenProps=t?this._overwrittenProps||{}:"all"}if(a){l=t||a,_=t!==s&&"all"!==s&&t!==a&&(null==t||t._tempKill!==!0);for(n in l)(r=a[n])&&(r.pg&&r.t._kill(l)&&(o=!0),r.pg&&0!==r.t._overwriteProps.length||(r._prev?r._prev._next=r._next:r===this._firstPT&&(this._firstPT=r._next),r._next&&(r._next._prev=r._prev),r._next=r._prev=null),delete a[n]),_&&(s[n]=1);!this._firstPT&&this._initted&&this._enabled(!1,!1)}}return o},s.invalidate=function(){return this._notifyPluginsOfEnabled&&C._onPluginEvent("_onDisable",this),this._firstPT=null,this._overwrittenProps=null,this._onUpdate=null,this._startAt=null,this._initted=this._active=this._notifyPluginsOfEnabled=!1,this._propLookup=this._targets?{}:[],this},s._enabled=function(t,e){if(r||n.wake(),t&&this._gc){var i,s=this._targets;if(s)for(i=s.length;--i>-1;)this._siblings[i]=G(s[i],this,!0);else this._siblings=G(this.target,this,!0)}return A.prototype._enabled.call(this,t,e),this._notifyPluginsOfEnabled&&this._firstPT?C._onPluginEvent(t?"_onEnable":"_onDisable",this):!1},C.to=function(t,e,i){return new C(t,e,i)},C.from=function(t,e,i){return i.runBackwards=!0,i.immediateRender=0!=i.immediateRender,new C(t,e,i)},C.fromTo=function(t,e,i,s){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,new C(t,e,s)},C.delayedCall=function(t,e,i,s,n){return new C(e,0,{delay:t,onComplete:e,onCompleteParams:i,onCompleteScope:s,onReverseComplete:e,onReverseCompleteParams:i,onReverseCompleteScope:s,immediateRender:!1,useFrames:n,overwrite:0})},C.set=function(t,e){return new C(t,0,e)},C.killTweensOf=C.killDelayedCallsTo=function(t,e){for(var i=C.getTweensOf(t),s=i.length;--s>-1;)i[s]._kill(e,t)},C.getTweensOf=function(t){if(null==t)return[];t="string"!=typeof t?t:C.selector(t)||t;var e,i,s,n;if((t instanceof Array||R(t))&&"number"!=typeof t[0]){for(e=t.length,i=[];--e>-1;)i=i.concat(C.getTweensOf(t[e]));for(e=i.length;--e>-1;)for(n=i[e],s=e;--s>-1;)n===i[s]&&i.splice(e,1)}else for(i=G(t).concat(),e=i.length;--e>-1;)i[e]._gc&&i.splice(e,1);return i};var q=p("plugins.TweenPlugin",function(t,e){this._overwriteProps=(t||"").split(","),this._propName=this._overwriteProps[0],this._priority=e||0,this._super=q.prototype},!0);if(s=q.prototype,q.version="1.10.1",q.API=2,s._firstPT=null,s._addTween=function(t,e,i,s,n,r){var a,o;return null!=s&&(a="number"==typeof s||"="!==s.charAt(1)?Number(s)-i:parseInt(s.charAt(0)+"1",10)*Number(s.substr(2)))?(this._firstPT=o={_next:this._firstPT,t:t,p:e,s:i,c:a,f:"function"==typeof t[e],n:n||e,r:r},o._next&&(o._next._prev=o),o):void 0},s.setRatio=function(t){for(var e,i=this._firstPT,s=1e-6;i;)e=i.c*t+i.s,i.r?e=0|e+(e>0?.5:-.5):s>e&&e>-s&&(e=0),i.f?i.t[i.p](e):i.t[i.p]=e,i=i._next},s._kill=function(t){var e,i=this._overwriteProps,s=this._firstPT;if(null!=t[this._propName])this._overwriteProps=[];else for(e=i.length;--e>-1;)null!=t[i[e]]&&i.splice(e,1);for(;s;)null!=t[s.n]&&(s._next&&(s._next._prev=s._prev),s._prev?(s._prev._next=s._next,s._prev=null):this._firstPT===s&&(this._firstPT=s._next)),s=s._next;return!1},s._roundProps=function(t,e){for(var i=this._firstPT;i;)(t[this._propName]||null!=i.n&&t[i.n.split(this._propName+"_").join("")])&&(i.r=e),i=i._next},C._onPluginEvent=function(t,e){var i,s,n,r,a,o=e._firstPT;if("_onInitAllProps"===t){for(;o;){for(a=o._next,s=n;s&&s.pr>o.pr;)s=s._next;(o._prev=s?s._prev:r)?o._prev._next=o:n=o,(o._next=s)?s._prev=o:r=o,o=a}o=e._firstPT=n}for(;o;)o.pg&&"function"==typeof o.t[t]&&o.t[t]()&&(i=!0),o=o._next;return i},q.activate=function(t){for(var e=t.length;--e>-1;)t[e].API===q.API&&(I[(new t[e])._propName]=t[e]);return!0},f.plugin=function(t){if(!(t&&t.propName&&t.init&&t.API))throw"illegal plugin definition.";var e,i=t.propName,s=t.priority||0,n=t.overwriteProps,r={init:"_onInitTween",set:"setRatio",kill:"_kill",round:"_roundProps",initAll:"_onInitAllProps"},a=p("plugins."+i.charAt(0).toUpperCase()+i.substr(1)+"Plugin",function(){q.call(this,i,s),this._overwriteProps=n||[]},t.global===!0),o=a.prototype=new q(i);o.constructor=a,a.API=t.API;for(e in r)"function"==typeof t[e]&&(o[r[e]]=t[e]);return a.version=t.version,q.activate([a]),a},e=t._gsQueue){for(i=0;e.length>i;i++)e[i]();for(s in u)u[s].func||t.console.log("GSAP encountered missing dependency: com.greensock."+s)}r=!1})(window);
define("tweenlite", (function (global) {
    return function () {
        var ret, fn;
        return ret || global.TweenLite;
    };
}(this)));

/*!
 * VERSION: beta 1.7.0
 * DATE: 2013-02-27
 * UPDATES AND DOCS AT: http://www.greensock.com
 *
 * @license Copyright (c) 2008-2013, GreenSock. All rights reserved.
 * This work is subject to the terms at http://www.greensock.com/terms_of_use.html or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 **/
(window._gsQueue||(window._gsQueue=[])).push(function(){"use strict";var t=document.documentElement,e=window,i=function(i,s){var r="x"===s?"Width":"Height",n="scroll"+r,a="client"+r,o=document.body;return i===e||i===t||i===o?Math.max(t[n],o[n])-Math.max(t[a],o[a]):i[n]-i["offset"+r]},s=window._gsDefine.plugin({propName:"scrollTo",API:2,init:function(t,s,r){return this._wdw=t===e,this._target=t,this._tween=r,"object"!=typeof s&&(s={y:s}),this._autoKill=s.autoKill!==!1,this.x=this.xPrev=this.getX(),this.y=this.yPrev=this.getY(),null!=s.x?this._addTween(this,"x",this.x,"max"===s.x?i(t,"x"):s.x,"scrollTo_x",!0):this.skipX=!0,null!=s.y?this._addTween(this,"y",this.y,"max"===s.y?i(t,"y"):s.y,"scrollTo_y",!0):this.skipY=!0,!0},set:function(t){this._super.setRatio.call(this,t);var i=this._wdw||!this.skipX?this.getX():this.xPrev,s=this._wdw||!this.skipY?this.getY():this.yPrev,r=s-this.yPrev,n=i-this.xPrev;this._autoKill&&(!this.skipX&&(n>7||-7>n)&&(this.skipX=!0),!this.skipY&&(r>7||-7>r)&&(this.skipY=!0),this.skipX&&this.skipY&&this._tween.kill()),this._wdw?e.scrollTo(this.skipX?i:this.x,this.skipY?s:this.y):(this.skipY||(this._target.scrollTop=this.y),this.skipX||(this._target.scrollLeft=this.x)),this.xPrev=this.x,this.yPrev=this.y}}),r=s.prototype;s.max=i,r.getX=function(){return this._wdw?null!=e.pageXOffset?e.pageXOffset:null!=t.scrollLeft?t.scrollLeft:document.body.scrollLeft:this._target.scrollLeft},r.getY=function(){return this._wdw?null!=e.pageYOffset?e.pageYOffset:null!=t.scrollTop?t.scrollTop:document.body.scrollTop:this._target.scrollTop},r._kill=function(t){return t.scrollTo_x&&(this.skipX=!0),t.scrollTo_y&&(this.skipY=!0),this._super._kill.call(this,t)}}),window._gsDefine&&window._gsQueue.pop()();
define("scrollto", (function (global) {
    return function () {
        var ret, fn;
        return ret || global.Scrollto;
    };
}(this)));

define('jquery', [], function() {
    return Modernizr;
});
define("modernizr", (function (global) {
    return function () {
        var ret, fn;
        return ret || global.Modernizr;
    };
}(this)));

/*!
 * Carouselset RequireJS Module
 * by Jonathan Robles
 *
 */
define('modules/controllers/carouselset',['jquery','underscore', 'modules/definitions/standardmodule','hammer','tweenlite','scrollto','modernizr'], function ($, _, parentModel,Hammer,TweenMax,Scrollto,Modernizr) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_(o) {
        var defaults = {
            type: 'carousel', //name your module, use this file's exact filename w/o the .js.
            author: 'Jonathan Robles', //your name if you are the original writer
            lasteditby: '',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)

            target:undefined,  //target object that contains children to control
            virtualchildren:false,  //false expects child divs as breaks, true makes virtual breaks!
            virtualchild_overlap:0,  //overlap between virtual children
            yscroll:false,  //use x or y scrolling
            Xscrolldetect:'release dragleft dragright',// swipeleft swiperight', //remove options to omit
            Yscrolldetect:'release dragup dragdown',// swipeup swipedown', //remove options to omit
            currentslide:0, //defines default slide on start
            visibleslides:[],
            visibleslides_bleed:-2,  // -1 is auto mode, # is the multiplier
            visibleslides_with_bleed:[],
            lastdebouncedslide: 0,
            usetouchbinds:true, //adds touchbinds to the target
            enable_touchbinds:true,  //enables/disables touchbinds
            snaponrelease:true,  //on stop scroll, will snap to closest child
            callback:function(o){


            },  //runs when swipepanel is made
            childchange:function(index){

                var self=this.parent;
                var destination=undefined;
                var animationvars=undefined;
                // console.log("index: "+index)

                if(!self._var().yscroll){
                    destination=self._var().childVars[index].XoffsetfromScroll;
                    animationvars={scrollTo:{x:destination}, ease:Power4.easeOut}
                }else{
                    destination=self._var().childVars[index].YoffsetfromScroll;
                    animationvars={scrollTo:{y:destination}, ease:Power4.easeOut}

                }


                TweenMax.to(self._var().target,.5,animationvars );

            }, //child changing function
            //LIVE SCROLLING CALLBACKS
            onscroll_callback:function(o){
                //console.log('onscroll_callback');
                //console.log('moving :'+ o.currentposition+' [ '+ o.currentpercent+'% ]')

            }, //on scrolling callback
            onscroll_start:function(o){
                //console.log('startmove :'+ o.currentposition+' [ '+ o.currentpercent+'% ]')
            },  //on before scrolling callback
            onscroll_pause:function(o){
                //console.log('onscroll_pause');
                //console.log('moving :'+ o.currentposition+' [ '+ o.currentpercent+'% ]')

            }, //on scrolling callback

            onscroll_debounce_callback:function(o){
                console.log('onscroll_debounce_callback');
                //console.log('endmove :'+ o.currentposition+' [ '+ o.currentpercent+'% ] @slide: '+this.currentslide)

            },  //on end scrolling callback
            onscroll_debounce_rate:100,  //function debouncing rate
            maxitemsonly:true,//START omit offsets so that it removes possible indexes to scroll to when it is possible to see more that one item per page

            attachdisableclassonid:undefined, // when defined attaches disable classes prev_disabled and next_disabled appropriately
            attachhidearrowclassonid:undefined, // when defined attaches hide class appropriately
            //INTERNAL Variables


            scrollsize:undefined,
            currentposition:undefined,
            currentpercent:undefined,

            targetXsize:undefined,
            targetYsize:undefined,

            targetwidth:undefined,
            targetheight:undefined,
            totalslides:undefined,

            childObjects:undefined,
            childVars:undefined,
            childVar_setter:function(index,value){
                var self = this;

                self.index=index;
                self.childOBJ=value;
                self.target=$(value);
                self.Xsize=self.target.outerWidth();
                self.Ysize=self.target.innerHeight();
                self.Xdefault=Math.round(self.target.position().left);
                self.Ydefault=Math.round(self.target.position().top);
                self.XoffsetfromScroll=undefined;//xoffset;
                self.YoffsetfromScroll=undefined;//yoffset;

                //live variables
                //self.OffsetfromFocus=undefined;
            },


            ObjHammer:undefined,

            isbeingdragged:false, //internal!
            startmove:true,//internal!
            busy:false

        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create(parentModel.prototype);

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners = function () {
        this.notify('Trace','_startlisteners');
        var parent=this;
        var myID=  this._id();
        _notify.add(this._id(), function() {

            return {
                onQuickjump:function(o){
                    if(o.senderID==myID){
                        parent.quickjump(o.data.index)
                    }
                },
                onJump:function(o){
                    if(o.senderID==myID){
                        parent.jump(o.data.index)
                    }
                },
                onPagejump:function(o){
                    if(o.senderID==myID){
                        parent.pagejump(o.data.index)
                    }
                },
                onNext:function(o){
                    if(o.senderID==myID){
                        parent.next()
                    }
                },
                onPrev:function(o){
                    if(o.senderID==myID){
                        parent.prev()
                    }
                },
                onWindowWidth:function(o){
                    parent.refresh();
                }




            }
        }());
    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init = function () {
        this.notify('Trace', 'init');
        this._var({busy:true});
        this._startlisteners();//start this module's listeners
        var self=this;

        this.refresh();

    };

    _thizOBJ_.prototype.refresh = function () {

        var self= this;
        self._var({busy:true});
        self.notify('Trace', 'refresh');
        self.getpanelsizes();
       //Object {totalslides: undefined, targetwidth: 978, targetheight: 280, targetXsize: 938, targetYsize: 278}
        (!self._var().virtualchildren)?self._childrentochildObjects():self._virtualspacetochildObjects();
        window.TESTOBJ2=self._var().childObjects;




        self.quickjump(self._var().currentslide)

        self._setwhatisvisible(); //logic to figure out what is visible



        //hide arrows if necessary
        if(self._var().attachhidearrowclassonid!=undefined){




            var lastindex=self._var().childObjects.length-1;
            var windowsize;
            var firstindexcloseside;
            var lastindexfarside;


            if(!self._var().yscroll){
                windowsize=self._var().targetXsize;
                firstindexcloseside=self._var().childVars[0].XoffsetfromScroll;
                lastindexfarside=self._var().childVars[lastindex].XoffsetfromScroll+self._var().childVars[lastindex].Xsize;
            }else{
                windowsize=self._var().targetYsize;
                firstindexcloseside=self._var().childVars[0].YoffsetfromScroll;
                lastindexfarside=self._var().childVars[lastindex].YoffsetfromScroll+self._var().childVars[lastindex].Ysize;
            }



            var estimatedScrollsize=(lastindexfarside-firstindexcloseside);

            //console.log('compare '+ estimatedScrollsize+' and '+windowsize);


            if(estimatedScrollsize<=windowsize){
                self._var().attachhidearrowclassonid.addClass('hide_navigation');
            }   else  {
                self._var().attachhidearrowclassonid.removeClass('hide_navigation');
            }

        };







        //Attach touch and drag handler
        var handlerdetect=(!self._var().yscroll)?self._var().Xscrolldetect:self._var().Yscrolldetect;


        //place this once!
        if(self._var().usetouchbinds&&(self._var().ObjHammer==undefined)){
            self._var().ObjHammer= Hammer (self._var().target,{ drag_lock_to_axis: true }).on(handlerdetect, self._bindHandler(self));
        }

        //attach scroll handler
        self._var().target.unbind("scroll");
        self._var().target.scroll(self._onscroll(self))

        //self._var().onscroll_debounce_callback();

        self._var({busy:false});
        if(self._var().callback!=undefined){self._var().callback(self) }

        self._var().lastdebouncedslide=self._var().currentslide;



    };
    _thizOBJ_.prototype.kill = function () {
        /*
         <object>.hide(); //just hide it and start listeners
         * */
        this.notify('Trace', 'kill');
        _notify.rem(this._id());  //kills this module's listeners
    };
    // UNIQUE MODULE METHODS

//internal
    _thizOBJ_.prototype._childrentochildObjects = function(){
        var self=this;
        self._var({childObjects:self._var().target.children()});
        var childObjects=self._var().childObjects;

        self._var({
            totalslides:childObjects.length,
            childVars:[]
        });

        //build childVars
        $.each(childObjects,function(index, value ){
            childVar_setter_local=self._var().childVar_setter;
            self._var().childVars.push(new childVar_setter_local(index,value))

        });
        $.each(self._var().childVars,function(index, value ){
            var zeroXdefault=self._var().childVars[0].Xdefault;
            var zeroYdefault=self._var().childVars[0].Ydefault;
            var marginOffsetX=value.target.css('marginLeft');
            var marginOffsetY=value.target.css('marginTop');

            value.XoffsetfromScroll=Math.round(value.Xdefault-zeroXdefault);
            value.YoffsetfromScroll=Math.round(value.Ydefault-zeroYdefault);

            if(marginOffsetX!=undefined){value.XoffsetfromScroll+=parseInt(marginOffsetX, 10)};
            if(marginOffsetY!=undefined){value.YoffsetfromScroll+=parseInt(marginOffsetY, 10)};
        });


        var totalscrollarea=(!self._var().yscroll)?self._var().targetwidth:self._var().targetheight;
        var singlechildsize=(!self._var().yscroll)?self._var().targetXsize:self._var().targetYsize;
        var scrollsize=totalscrollarea-singlechildsize;


        self._var({
            scrollsize:scrollsize
        });



    };
    _thizOBJ_.prototype._virtualspacetochildObjects = function(){
        var self=this;
        self._var({childObjects:self._var().target.children()});

        var totalscrollarea=(!self._var().yscroll)?self._var().targetwidth:self._var().targetheight;
        var singlechildsize=(!self._var().yscroll)?self._var().targetXsize:self._var().targetYsize;
        singlechildsize=singlechildsize-self._var().virtualchild_overlap;

        var scrollsize=totalscrollarea-singlechildsize;

        //var latestsizes=self.getpanelsizes();
        //alert(totalscrollarea+' : '+scrollsize)

        var tempStopArray=[];
        var childcount=Math.floor(scrollsize/singlechildsize);
        for(tempval=0;tempval<childcount+1;tempval++){
            tempStopArray.push(tempval*singlechildsize)
        }
        tempStopArray.push(scrollsize);

        //alert(scrollsize)
        //alert(tempStopArray)



        // var childObjects=self._var().childObjects;

        self._var({
            totalslides:tempStopArray.length,
            childVars:[],
            scrollsize:scrollsize
        });

        //build childVars
        $.each(tempStopArray,function(index, value ){
            self._var().childVars.push({
                XoffsetfromScroll:value,
                YoffsetfromScroll:value
            })

        });



        /*  $.each(self._var().childVars,function(index, value ){
         var zeroXdefault=self._var().childVars[0].Xdefault;
         var zeroYdefault=self._var().childVars[0].Ydefault;
         var marginOffsetX=value.target.css('marginLeft');
         var marginOffsetY=value.target.css('marginTop');

         value.XoffsetfromScroll=Math.round(value.Xdefault-zeroXdefault);
         value.YoffsetfromScroll=Math.round(value.Ydefault-zeroYdefault);

         if(marginOffsetX!=undefined){value.XoffsetfromScroll+=parseInt(marginOffsetX, 10)};
         if(marginOffsetY!=undefined){value.YoffsetfromScroll+=parseInt(marginOffsetY, 10)};
         });

         //set scrollsize
         self._var({
         scrollsize:(!self._var().yscroll)?self._var().childVars[self._var().totalslides-1].XoffsetfromScroll:self._var().childVars[self._var().totalslides-1].YoffsetfromScroll
         });

         */
        //alert(self._var().scrollsize)

        //  alert(self._var().target.width()-400)
        // alert(self._var().target.innerWidth()-400)
        //alert(self._var().target.outterWidth())
        //  alert(self._var().scrollsize+' : '+(self._var().targetwidth-(self._var().target.width()+400)))
        //  alert(self._var().scrollsize+' : '+(self._var().targetwidth-(self._var().target.innerWidth()+400)))
    };


    _thizOBJ_.prototype.offsetReturnForMaxItems= function () {

        //returns a list of offsets for when maxitemsonly is ON


                var self=this;
              var Offsets=_.map(self._var().childVars, function(idx){
                     return (!self._var().yscroll)?Math.abs(idx.XoffsetfromScroll):Math.abs(idx.YoffsetfromScroll)
              });

                var newOffsets = new Array();

            //START omit offsets so that it removes possible indexes to scroll to when it is possible to see more that one item per page
            var removeOffset=_.map(self._var().childVars, function(idx){
                var needed=true;
                var lastitem=self._var().childVars.length-1;
                var barrier;
                var barrierbuffer=25;  //have to fix this and some point!!!!  something off with calculation!
                var returnCandidate;

                if(!self._var().yscroll){


                    barrier=(self._var().childVars[lastitem].XoffsetfromScroll-self._var().targetXsize+self._var().childVars[lastitem].Xsize+barrierbuffer);
                    returnCandidate=(barrier+idx.Xsize)<(idx.XoffsetfromScroll);  //returns true if index should be removed!
                    //console.log('idx:'+idx+' comparing  '+(barrier+idx.Xsize)+' & '+idx.XoffsetfromScroll+'  return cand:'+returnCandidate)

                } else {
                    barrier=(self._var().childVars[lastitem].YoffsetfromScroll-self._var().targetYsize+self._var().childVars[lastitem].Ysize+barrierbuffer);
                    returnCandidate=(barrier+idx.Ysize)<(idx.YoffsetfromScroll);  //returns true if index should be removed!
                }

                return returnCandidate;
            });



            $.each(Offsets,function(index,value){
                if(index==0){
                    newOffsets.push(Offsets[index]); //always push index 0!
                } else {
                    if(!removeOffset[index]){newOffsets.push(Offsets[index])}
                }

            })

            //console.log('a:'+Offsets);console.log('b:'+newOffsets);



            //END omit offsets
            return newOffsets


    }


    _thizOBJ_.prototype.getpanelsizes = function () {
        var self= this;
        var objreturn={}
        objreturn.totalslides=self._var().totalslides;

        objreturn.targetwidth=self._var().targetwidth=self._var().target[0].scrollWidth;
        objreturn.targetheight=self._var().targetheight=self._var().target[0].scrollHeight;

        objreturn.targetXsize=self._var().targetXsize=self._var().target.innerWidth();
        objreturn.targetYsize=self._var().targetYsize=self._var().target.innerHeight();
        window.TESTOBJ=objreturn;

        //set visibleslides_bleed
        if(self._var().visibleslides_bleed<0)
        {
            var multiplier= Math.abs(self._var().visibleslides_bleed);
            self._var().visibleslides_bleed= (!self._var().yscroll)?(objreturn.targetXsize*multiplier):(objreturn.targetYsize*multiplier)

        }
        //console.log(self._var().visibleslides_bleed)


        return objreturn;
    };
    _thizOBJ_.prototype.getclosestindex=function(position)  {
        var self=this;

        var getOffsets=_.map(self._var().childVars, function(idx){
            return (!self._var().yscroll)?idx.XoffsetfromScroll:idx.YoffsetfromScroll
        });





  if(self._var().maxitemsonly){

        //USE smaller set of offsets
      getOffsets=self.offsetReturnForMaxItems();


  }


        var Offsets=_.map(getOffsets, function(off_value){
            return Math.abs(off_value-position);
        });

        //console.log(getOffsets)
        //console.log(Offsets)

        //makes array of differences of current x stop and all stops
        var closestdiff=_.reduce(Offsets, function(memo, val) {
            return (val<memo)?val:memo;
        }, 10000);
        //get lowest number from array
        var indextoReturn = _.indexOf(Offsets, closestdiff);

        self._var().currentslide=indextoReturn;
        return indextoReturn;
    };
    _thizOBJ_.prototype._returncurrentposition= function(){
        var self=this;
        return (!self._var().yscroll)?self._var().target.scrollLeft():self._var().target.scrollTop()
    };
    _thizOBJ_.prototype._gotoposition= function(getdestination){
        var self=this;
        (!self._var().yscroll)?self._var().target.scrollLeft(getdestination):self._var().target.scrollTop(getdestination)
    };
    _thizOBJ_.prototype._gotopercent= function(getpercent){
        var self=this;
        var scrollsize=self._var().scrollsize
        //var currentposition=self._returncurrentposition();
        var jumptoposition=scrollsize/(100/getpercent);
        self._gotoposition(jumptoposition);
    };
//controls
    _thizOBJ_.prototype.selfie= function (o){

        //console.log('SELFIEEEE!!!')
        var self=this;
        var options={
            yscroll:self._var().yscroll,
            quick:false
        };
        options = $.extend(options, o);

        if(!options.yscroll){
            //xscroll
            var thisindex=self.getclosestindex(self._returncurrentposition());


            if(!options.quick){
                self.jump(thisindex);
            }else{
                self.quickjump(thisindex);
            }

        }else{
            //yscroll
            var thisindex=self.getclosestindex(self._returncurrentposition());


            if(!options.quick){
                self.jump(thisindex);
            }else{
                self.quickjump(thisindex);
            }
        }


    };
    _thizOBJ_.prototype.next= function (o){
        var self=this;
        var options={
            quick:false
        };
        options = $.extend(options, o);
        var thisindex=(!self._var().yscroll)?self.getclosestindex(self._returncurrentposition()):self.getclosestindex(self._var().target.scrollTop());
        //console.log('thisindex:'+thisindex+' : '+self._var().currentslide)

        var nextidx=(thisindex==self._var().totalslides-1)?thisindex:thisindex+1;

        //the following compares next destination to the last debounce slide
        var diff=Math.abs(nextidx-Number(self._var().lastdebouncedslide))
        // console.log("current:"+self._var().lastdebouncedslide+" index: "+nextidx+" diff:"+diff);
        if(diff>1){nextidx-=(diff-1)};
        //comparetodebounce complete

        (!options.quick)?self.jump(nextidx):self.quickjump(nextidx);

    };
    _thizOBJ_.prototype.prev= function (o){
        var self=this;
        var options={
            quick:false
        };
        options = $.extend(options, o);
        var thisindex=(!self._var().yscroll)?self.getclosestindex(self._returncurrentposition()):self.getclosestindex(self._var().target.scrollTop());
        var nextidx=(thisindex==0)?0:thisindex-1;

        //the following compares next destination to the last debounce slide
        var diff=Math.abs(nextidx-Number(self._var().lastdebouncedslide))
        //console.log("current:"+self._var().lastdebouncedslide+" index: "+nextidx+" diff:"+diff);
        if(diff>1){nextidx+=(diff-1)};
        //comparetodebounce complete

        (!options.quick)?self.jump(nextidx):self.quickjump(nextidx);


    };
    _thizOBJ_.prototype.jump = function (index) {

        var self=this;
        self._var().childchange(index,self)


    };
    _thizOBJ_.prototype.pagejump = function (index) {
        this.jump(index);
    }
    _thizOBJ_.prototype.quickjump = function (index) {
        var self=this;

        if(!self._var().yscroll){
            var destination=self._var().childVars[index].XoffsetfromScroll;
            // self._var().target.scrollLeft(destination)
        }else{
            var destination=self._var().childVars[index].YoffsetfromScroll;
            //self._var().target.scrollTop(destination)
        }

        self._gotoposition(destination)

    };

//handlers
    _thizOBJ_.prototype._bindHandler = function(self){

        var lastCase=undefined;
        var touchpoint=undefined;

        var targetposition=function(){

            touchpoint=self._returncurrentposition();
        }

        var casechecks=undefined;
        var directions=undefined;

        if(!self._var().yscroll){
            casechecks=['dragright','dragleft','swipeleft','swiperight','release'];
            directions=[Hammer.DIRECTION_RIGHT,Hammer.DIRECTION_LEFT];
        } else {
            casechecks=['dragdown','dragup','swipeup','swipedown','release'];
            directions=[Hammer.DIRECTION_DOWN,Hammer.DIRECTION_UP];
        }


        var eventHandler = function (ev){
            // disable default action
            ev.gesture.preventDefault();

            if(ev.type!=lastCase){ targetposition(); };
//alert('!')

            if(self._var().enable_touchbinds){

                switch(ev.type) {


                    case casechecks[4]:
                        // console.log('case4')
                        self._var().isbeingdragged=false;
                        targetposition();

                        if(self._var().snaponrelease){
                            //console.log('selfie!')
                            self.selfie();
                        } else {
                            //alert('hmmmm')


                            var currentvars={};
                            currentvars.scrollsize=self._var().scrollsize
                            currentvars.currentposition=self._returncurrentposition();
                            currentvars.currentpercent=(100/(currentvars.scrollsize/currentvars.currentposition));


                            self._debounce(self)(currentvars);
                        }



                        ev.gesture.stopDetect();
                        break;
                    case casechecks[0]:
                    case casechecks[1]:
                        // console.log('case0/1');// ['+self._returncurrentposition()+']')
                        self._var().isbeingdragged=true;


                        var pane_offset=touchpoint;
                        var drag_offset = (!self._var().yscroll)?ev.gesture.deltaX:ev.gesture.deltaY;

                        //+ or - depending on direction
                        if(ev.gesture.direction == directions[0]){
                            drag_offset=-Math.abs(drag_offset)
                        } else if(ev.gesture.direction == directions[1]){
                            drag_offset=Math.abs(drag_offset)
                        };


                        //compute destination
                        var destination=(pane_offset+drag_offset);

                        //update object
                        (!self._var().yscroll)?self._var().target.scrollLeft(destination):self._var().target.scrollTop(destination);
                        //ev.gesture.stopDetect();
                        break;
                    //swipeleft
                    case casechecks[2]:
                        //console.log('case2')
                        self.next();
                        self._var().isbeingdragged=false;
                        ev.gesture.stopDetect();
                        break;
                    //swiperight
                    case casechecks[3]:
                        // console.log('case3')
                        self.prev();
                        self._var().isbeingdragged=false;
                        ev.gesture.stopDetect();
                        break;

                }

            }



            lastCase=ev.type; //log last ev type


        };

        return  eventHandler;


    };
    _thizOBJ_.prototype._debounce= function(self){
        //var self=this;
        //alert('!')

        var debouncer=       _.debounce(function(currentvars){

            //compare current location with a acceptable release point
            var currentposition=    self._returncurrentposition();
            var closeindex=self.getclosestindex(currentposition);
            var destination=undefined;


            if(!self._var().yscroll){
                destination=self._var().childVars[closeindex].XoffsetfromScroll;
                //alert(closeindex)


            }else{
                destination=self._var().childVars[closeindex].YoffsetfromScroll;
            }



            var difference=(Math.abs(currentposition-destination));






            self._setwhatisvisible();
            if(self._var().onscroll_pause!=undefined){self._var().onscroll_pause(currentvars) }


            if(difference>1&&!self._var().isbeingdragged&&self._var().snaponrelease){

                self.selfie();

            } else if(difference<2||(!self._var().isbeingdragged&&!self._var().snaponrelease)) {


                if(!self._var().startmove){

                    //set the debounced last slide so stuff doesn't skip
                    self._var().lastdebouncedslide=self._var().currentslide;

                    self._setwhatisvisible();
                    self._var().onscroll_debounce_callback(currentvars);  //run debounce callback
                    self._var({startmove:true});  //makes sure you don't interupt to smoothie smoothness
                    //console.log(self._var().lastdebouncedslide)
                }


            }



        },self._var().onscroll_debounce_rate)



        return  debouncer;


    };




    _thizOBJ_.prototype._setwhatisvisible=function(){
        var self=this;
        var visibleslides=[];
        var visibleslides_with_bleed=[];
        var visibleindex=self._var().currentslide;
        var visibleslides_bleed=self._var().visibleslides_bleed;

        var visiblewindow_startX=self._var().childVars[visibleindex].XoffsetfromScroll;
        var visiblewindow_startY=self._var().childVars[visibleindex].YoffsetfromScroll;
        var visiblewindow_endX=(visiblewindow_startX+self._var().targetXsize);
        var visiblewindow_endY=(visiblewindow_startY+self._var().targetYsize);
        var visiblewindow_startXb=self._var().childVars[visibleindex].XoffsetfromScroll-self._var().visibleslides_bleed;
        var visiblewindow_startYb=self._var().childVars[visibleindex].YoffsetfromScroll-self._var().visibleslides_bleed;
        var visiblewindow_endXb=(visiblewindow_startX+self._var().targetXsize+(self._var().visibleslides_bleed));
        var visiblewindow_endYb=(visiblewindow_startY+self._var().targetYsize+(self._var().visibleslides_bleed));

        if(!self._var().yscroll){
        $.each(self._var().childVars,function(index,value){

            var minVar=value.XoffsetfromScroll;
            var maxVar=minVar+value.Xsize;
            if(minVar>=visiblewindow_startX  && maxVar<=visiblewindow_endX )  {
                visibleslides.push(index)
            }
            if(minVar>=visiblewindow_startXb  && maxVar<=visiblewindow_endXb )  {
                visibleslides_with_bleed.push(index)
            }
            //console.log(visiblewindow_startXb+'< '+visiblewindow_startX+'< '+minVar+'x'+maxVar+' >'+visiblewindow_endX+' >'+visiblewindow_endXb)

        });
        } else {
            $.each(self._var().childVars,function(index,value){

                var minVar=value.YoffsetfromScroll;
                var maxVar=minVar+value.Ysize;
                if(minVar>=visiblewindow_startY  && maxVar<=visiblewindow_endY )  {
                    visibleslides.push(index)
                }
                if(minVar>=visiblewindow_startYb  && maxVar<=visiblewindow_endYb )  {
                    visibleslides_with_bleed.push(index)
                }
                // console.log(visiblewindow_startYb+'< '+visiblewindow_startY+'< '+minVar+'x'+maxVar+' >'+visiblewindow_endY+' >'+visiblewindow_endYb)


            });
        }





        self._var().visibleslides=visibleslides;
        self._var().visibleslides_with_bleed=visibleslides_with_bleed;


        //PLACE

        //disable arrows if necessary
        if(self._var().attachdisableclassonid!=undefined){
            var firstidtodisable=0;
            var lastidtodisable=(self._var().totalslides-1);//self.offsetReturnForMaxItems().length;

            //console.log('self._var().visibleslides='+self._var().visibleslides+' totalslides='+self._var().totalslides+' use last='+lastidtodisable)

            if(_.contains(self._var().visibleslides, firstidtodisable)){
                //console.log('no previous');
                self._var().attachdisableclassonid.addClass('disable_previous');
            } else {
                self._var().attachdisableclassonid.removeClass('disable_previous');
            }

            ;
            if(_.contains(self._var().visibleslides, lastidtodisable)){
                //console.log('no next');
                self._var().attachdisableclassonid.addClass('disable_next');
            } else {
                self._var().attachdisableclassonid.removeClass('disable_next');
            }

            ;





         };



        //

        //console.log('['+visiblewindow_startX+' to '+visiblewindow_endX +'] ['+visiblewindow_startXb+' to '+visiblewindow_endXb +'] '   )
        //console.log(' ['+ self._var().visibleslides+']  withbleed:['+ self._var().visibleslides_with_bleed+']')
    };






    _thizOBJ_.prototype._onscroll= function(self){



        var Offsets=_.map(self._var().childVars, function(idx){
            return (!self._var().yscroll)?Math.abs(idx.XoffsetfromScroll):Math.abs(idx.YoffsetfromScroll)
        });


        var debouncefunc=self._debounce(self);
        var handler= function(){
            var currentvars={};
            currentvars.scrollsize=self._var().scrollsize
            currentvars.currentposition=self._returncurrentposition();
            currentvars.currentpercent=(100/(currentvars.scrollsize/currentvars.currentposition));

            //start onscroll functions
            if(self._var().startmove){
                self._var().onscroll_start(currentvars);
                self._var({startmove:false})
            }

            self._var().onscroll_callback(currentvars);
            debouncefunc(currentvars)
        }

        return handler
    };



    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});


/*
 Copyright 2011-2013 Abdulla Abdurakhmanov
 Original sources are available at https://code.google.com/p/x2js/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

function X2JS(config) {
	'use strict';
		
	var VERSION = "1.1.3";
	
	config = config || {};
	initConfigDefaults();
	
	function initConfigDefaults() {
		if(config.escapeMode === undefined) {
			config.escapeMode = true;
		}
		config.attributePrefix = config.attributePrefix || "_";
		config.arrayAccessForm = config.arrayAccessForm || "none";
		config.emptyNodeForm = config.emptyNodeForm || "text";
		if(config.enableToStringFunc === undefined) {
			config.enableToStringFunc = true; 
		}
		config.arrayAccessFormPaths = config.arrayAccessFormPaths || []; 
		if(config.skipEmptyTextNodesForObj === undefined) {
			config.skipEmptyTextNodesForObj = true;
		}
	}

	var DOMNodeTypes = {
		ELEMENT_NODE 	   : 1,
		TEXT_NODE    	   : 3,
		CDATA_SECTION_NODE : 4,
		COMMENT_NODE	   : 8,
		DOCUMENT_NODE 	   : 9
	};
	
	function getNodeLocalName( node ) {
		var nodeLocalName = node.localName;			
		if(nodeLocalName == null) // Yeah, this is IE!! 
			nodeLocalName = node.baseName;
		if(nodeLocalName == null || nodeLocalName=="") // =="" is IE too
			nodeLocalName = node.nodeName;
		return nodeLocalName;
	}
	
	function getNodePrefix(node) {
		return node.prefix;
	}
		
	function escapeXmlChars(str) {
		if(typeof(str) == "string")
			return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#x27;').replace(/\//g, '&#x2F;');
		else
			return str;
	}

	function unescapeXmlChars(str) {
		return str.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&#x27;/g, "'").replace(/&#x2F;/g, '\/');
	}
	
	function toArrayAccessForm(obj, childName, path) {
		switch(config.arrayAccessForm) {
		case "property":
			if(!(obj[childName] instanceof Array))
				obj[childName+"_asArray"] = [obj[childName]];
			else
				obj[childName+"_asArray"] = obj[childName];
			break;		
		/*case "none":
			break;*/
		}
		
		if(!(obj[childName] instanceof Array) && config.arrayAccessFormPaths.length > 0) {
			var idx = 0;
			for(; idx < config.arrayAccessFormPaths.length; idx++) {
				var arrayPath = config.arrayAccessFormPaths[idx];
				if( typeof arrayPath === "string" ) {
					if(arrayPath == path)
						break;
				}
				else
				if( arrayPath instanceof RegExp) {
					if(arrayPath.test(path))
						break;
				}				
				else
				if( typeof arrayPath === "function") {
					if(arrayPath(obj, childName, path))
						break;
				}
			}
			if(idx!=config.arrayAccessFormPaths.length) {
				obj[childName] = [obj[childName]];
			}
		}
	}

	function parseDOMChildren( node, path ) {
		if(node.nodeType == DOMNodeTypes.DOCUMENT_NODE) {
			var result = new Object;
			var nodeChildren = node.childNodes;
			// Alternative for firstElementChild which is not supported in some environments
			for(var cidx=0; cidx <nodeChildren.length; cidx++) {
				var child = nodeChildren.item(cidx);
				if(child.nodeType == DOMNodeTypes.ELEMENT_NODE) {
					var childName = getNodeLocalName(child);
					result[childName] = parseDOMChildren(child, childName);
				}
			}
			return result;
		}
		else
		if(node.nodeType == DOMNodeTypes.ELEMENT_NODE) {
			var result = new Object;
			result.__cnt=0;
			
			var nodeChildren = node.childNodes;
			
			// Children nodes
			for(var cidx=0; cidx <nodeChildren.length; cidx++) {
				var child = nodeChildren.item(cidx); // nodeChildren[cidx];
				var childName = getNodeLocalName(child);
				
				if(child.nodeType!= DOMNodeTypes.COMMENT_NODE) {
					result.__cnt++;
					if(result[childName] == null) {
						result[childName] = parseDOMChildren(child, path+"."+childName);
						toArrayAccessForm(result, childName, path+"."+childName);					
					}
					else {
						if(result[childName] != null) {
							if( !(result[childName] instanceof Array)) {
								result[childName] = [result[childName]];
								toArrayAccessForm(result, childName, path+"."+childName);
							}
						}
						(result[childName])[result[childName].length] = parseDOMChildren(child, path+"."+childName);
					}
				}								
			}
			
			// Attributes
			for(var aidx=0; aidx <node.attributes.length; aidx++) {
				var attr = node.attributes.item(aidx); // [aidx];
				result.__cnt++;
				result[config.attributePrefix+attr.name]=attr.value;
			}
			
			// Node namespace prefix
			var nodePrefix = getNodePrefix(node);
			if(nodePrefix!=null && nodePrefix!="") {
				result.__cnt++;
				result.__prefix=nodePrefix;
			}
			
			if(result["#text"]!=null) {				
				result.__text = result["#text"];
				if(result.__text instanceof Array) {
					result.__text = result.__text.join("\n");
				}
				if(config.escapeMode)
					result.__text = unescapeXmlChars(result.__text);
				delete result["#text"];
				if(config.arrayAccessForm=="property")
					delete result["#text_asArray"];
			}
			if(result["#cdata-section"]!=null) {
				result.__cdata = result["#cdata-section"];
				delete result["#cdata-section"];
				if(config.arrayAccessForm=="property")
					delete result["#cdata-section_asArray"];
			}
			
			if( result.__cnt == 1 && result.__text!=null  ) {
				result = result.__text;
			}
			else
			if( result.__cnt == 0 && config.emptyNodeForm=="text" ) {
				result = '';
			}
			else
			if ( result.__cnt > 1 && result.__text!=null && config.skipEmptyTextNodesForObj) {
				if(result.__text.trim()=="") {
					delete result.__text;
				}
			}
			delete result.__cnt;			
			
			if( config.enableToStringFunc && result.__text!=null || result.__cdata!=null ) {
				result.toString = function() {
					return (this.__text!=null? this.__text:'')+( this.__cdata!=null ? this.__cdata:'');
				};
			}
			return result;
		}
		else
		if(node.nodeType == DOMNodeTypes.TEXT_NODE || node.nodeType == DOMNodeTypes.CDATA_SECTION_NODE) {
			return node.nodeValue;
		}	
	}
	
	function startTag(jsonObj, element, attrList, closed) {
		var resultStr = "<"+ ( (jsonObj!=null && jsonObj.__prefix!=null)? (jsonObj.__prefix+":"):"") + element;
		if(attrList!=null) {
			for(var aidx = 0; aidx < attrList.length; aidx++) {
				var attrName = attrList[aidx];
				var attrVal = jsonObj[attrName];
				resultStr+=" "+attrName.substr(config.attributePrefix.length)+"='"+attrVal+"'";
			}
		}
		if(!closed)
			resultStr+=">";
		else
			resultStr+="/>";
		return resultStr;
	}
	
	function endTag(jsonObj,elementName) {
		return "</"+ (jsonObj.__prefix!=null? (jsonObj.__prefix+":"):"")+elementName+">";
	}
	
	function endsWith(str, suffix) {
	    return str.indexOf(suffix, str.length - suffix.length) !== -1;
	}
	
	function jsonXmlSpecialElem ( jsonObj, jsonObjField ) {
		if((config.arrayAccessForm=="property" && endsWith(jsonObjField.toString(),("_asArray"))) 
				|| jsonObjField.toString().indexOf(config.attributePrefix)==0 
				|| jsonObjField.toString().indexOf("__")==0
				|| (jsonObj[jsonObjField] instanceof Function) )
			return true;
		else
			return false;
	}
	
	function jsonXmlElemCount ( jsonObj ) {
		var elementsCnt = 0;
		if(jsonObj instanceof Object ) {
			for( var it in jsonObj  ) {
				if(jsonXmlSpecialElem ( jsonObj, it) )
					continue;			
				elementsCnt++;
			}
		}
		return elementsCnt;
	}
	
	function parseJSONAttributes ( jsonObj ) {
		var attrList = [];
		if(jsonObj instanceof Object ) {
			for( var ait in jsonObj  ) {
				if(ait.toString().indexOf("__")== -1 && ait.toString().indexOf(config.attributePrefix)==0) {
					attrList.push(ait);
				}
			}
		}
		return attrList;
	}
	
	function parseJSONTextAttrs ( jsonTxtObj ) {
		var result ="";
		
		if(jsonTxtObj.__cdata!=null) {										
			result+="<![CDATA["+jsonTxtObj.__cdata+"]]>";					
		}
		
		if(jsonTxtObj.__text!=null) {			
			if(config.escapeMode)
				result+=escapeXmlChars(jsonTxtObj.__text);
			else
				result+=jsonTxtObj.__text;
		}
		return result;
	}
	
	function parseJSONTextObject ( jsonTxtObj ) {
		var result ="";

		if( jsonTxtObj instanceof Object ) {
			result+=parseJSONTextAttrs ( jsonTxtObj );
		}
		else
			if(jsonTxtObj!=null) {
				if(config.escapeMode)
					result+=escapeXmlChars(jsonTxtObj);
				else
					result+=jsonTxtObj;
			}
		
		return result;
	}
	
	function parseJSONArray ( jsonArrRoot, jsonArrObj, attrList ) {
		var result = ""; 
		if(jsonArrRoot.length == 0) {
			result+=startTag(jsonArrRoot, jsonArrObj, attrList, true);
		}
		else {
			for(var arIdx = 0; arIdx < jsonArrRoot.length; arIdx++) {
				result+=startTag(jsonArrRoot[arIdx], jsonArrObj, parseJSONAttributes(jsonArrRoot[arIdx]), false);
				result+=parseJSONObject(jsonArrRoot[arIdx]);
				result+=endTag(jsonArrRoot[arIdx],jsonArrObj);						
			}
		}
		return result;
	}
	
	function parseJSONObject ( jsonObj ) {
		var result = "";	

		var elementsCnt = jsonXmlElemCount ( jsonObj );
		
		if(elementsCnt > 0) {
			for( var it in jsonObj ) {
				
				if(jsonXmlSpecialElem ( jsonObj, it) )
					continue;			
				
				var subObj = jsonObj[it];						
				
				var attrList = parseJSONAttributes( subObj )
				
				if(subObj == null || subObj == undefined) {
					result+=startTag(subObj, it, attrList, true);
				}
				else
				if(subObj instanceof Object) {
					
					if(subObj instanceof Array) {					
						result+=parseJSONArray( subObj, it, attrList );					
					}
					else {
						var subObjElementsCnt = jsonXmlElemCount ( subObj );
						if(subObjElementsCnt > 0 || subObj.__text!=null || subObj.__cdata!=null) {
							result+=startTag(subObj, it, attrList, false);
							result+=parseJSONObject(subObj);
							result+=endTag(subObj,it);
						}
						else {
							result+=startTag(subObj, it, attrList, true);
						}
					}
				}
				else {
					result+=startTag(subObj, it, attrList, false);
					result+=parseJSONTextObject(subObj);
					result+=endTag(subObj,it);
				}
			}
		}
		result+=parseJSONTextObject(jsonObj);
		
		return result;
	}
	
	this.parseXmlString = function(xmlDocStr) {
		if (xmlDocStr === undefined) {
			return null;
		}
		var xmlDoc;
		if (window.DOMParser) {
			var parser=new window.DOMParser();			
			xmlDoc = parser.parseFromString( xmlDocStr, "text/xml" );
		}
		else {
			// IE :(
			if(xmlDocStr.indexOf("<?")==0) {
				xmlDocStr = xmlDocStr.substr( xmlDocStr.indexOf("?>") + 2 );
			}
			xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async="false";
			xmlDoc.loadXML(xmlDocStr);
		}
		return xmlDoc;
	};
	
	this.asArray = function(prop) {
		if(prop instanceof Array)
			return prop;
		else
			return [prop];
	}

	this.xml2json = function (xmlDoc) {
		return parseDOMChildren ( xmlDoc );
	};
	
	this.xml_str2json = function (xmlDocStr) {
		var xmlDoc = this.parseXmlString(xmlDocStr);	
		return this.xml2json(xmlDoc);
	};

	this.json2xml_str = function (jsonObj) {
		return parseJSONObject ( jsonObj );
	};

	this.json2xml = function (jsonObj) {
		var xmlDocStr = this.json2xml_str (jsonObj);
		return this.parseXmlString(xmlDocStr);
	};
	
	this.getVersion = function () {
		return VERSION;
	};
	
}
;
define("x2js", (function (global) {
    return function () {
        var ret, fn;
        return ret || global.X2JS;
    };
}(this)));

/*!
 * dataparser RequireJS Module
 * by Jonathan Robles
 * Date: 10/12/13[7:46 PM]
 */
/** NOTE: Its a json/jsonp/xml parsing module.
 *
 * USAGE:
 * var yourobject = new Module({
 *      file:'data/sitedata.json',
 *      format:'json',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * OR
 *
 * var yourobject = new Module({
 *      file:'data/sitedata.jsonp',
 *      format:'jsonp',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * OR
 *
 * var yourobject = new Module({
 *      file:'data/sitedata.xml',
 *      format:'text',
 *      callback:function(data){alert('data received? '+data.status)}
 * });
 *
 * yourobject.init();
 */
define('modules/parsers/dataparser',['x2js','jquery','modules/definitions/standardmodule'],function (X2JS,$,parentModel) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_( o ){
        var defaults={
            type:'dataparser', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            lasteditby:'',  //your name if you are making the last edit (NOTE:all edits in the code must have comments with your initials!)

            file:'data/sitedata.json',
            usenocache:true,

            dataXML:undefined,

            data:undefined,  //placeholder for final data
            callback:undefined,//function(data){alert('received? '+data.status)},
            jsonpReturn:"window._global$.jsonpReturn('<%id>','JSONdata','global')",
            //interval:undefined,

            format:'json', //use text for xml, json for json, jsonp for jsonp
            //uselegacyinject:false,
            busy:false

        };
        defaults= $.extend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners =function(){
        this.notify('Trace','_startlisteners');
        var parent=this;
        var myID=  this._id();
            _notify.add(this._id(), function() {

         return {

                 onJSONdata:function(o){
                     if(o.senderID==myID){
                         parent._var({data: o.data,busy:false});
                         if(parent._var().callback!=undefined){
                             parent._var().callback(o.data)
                         }
                     }
                 }
         }
         }());

    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init =function(){

        this.notify('Trace','init');
        this._startlisteners();//start this module's listeners
        this.refresh();

    };

    _thizOBJ_.prototype.refresh =function(){
        this.notify('Trace','refresh');
        this.getdata();
    };

    _thizOBJ_.prototype.kill =function(){
        this.notify('Trace','kill');
         _notify.rem(this._id());  //kills this module's listeners
    };



    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.getdata= function(){
        var parent=this;
        parent._var({busy:true});
        this.notify('Trace','getdata format:'+parent._var().format+' file'+parent._var().file);
        var JSONtoUSE=parent._nocache(parent._var().file);
        var onData=function(jsondata){
            parent.notify('JSONdata',jsondata);
        };


        if(this._var().format=='text'){

            $.ajax({

                xhrFields: {
                    withCredentials: false
                },
                crossDomain: false,

                type: "GET",
                url: parent._var().file,
                dataType: parent._var().format,
                statusCode: {404: function() {alert('please check xml!');}},



                success: function(xml) {

                    parent._var({dataXML:xml});
                    var gparse = new X2JS(); //XML object parsing
                    var jsondata = gparse.parseXmlString(xml);
                    jsondata = gparse.xml2json( jsondata );

                    onData(jsondata);

                }
            });


//modules/view/viewmodels
        }
        else if(this._var().format=='jsonp'){


          //  if(!this._var().uselegacyinject){

                if(this._var().jsonpReturn!=undefined){

                    JSONtoUSE += "&jsonp="+this._var().jsonpReturn;
                    JSONtoUSE = JSONtoUSE.replace('<%id>',this._id()); //tack on ID to jsonP return
                }

                JSONtoUSE += "&=?";
                //console.log('trying to get '+this._var().format+' file:'+JSONtoUSE);


                $.ajaxSetup({
                    type: "POST",
                    data: {},
                    //dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
                });
                $.getJSON(JSONtoUSE, {format: 'jsonp'}).error(function(){
                   // console.log("JSONP READ COMPLETE! (note, object data/busy are not used for jsonp files!)");
                   // console.log('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=');
                    })//,function(jsondata) {onjsondata(jsondata);}).error(function() { trace("ERROR!");trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=') });



        }
        else {



            $.getJSON(JSONtoUSE, {format: 'json'},function(jsondata){
            onData(jsondata)
            }).error(function() {
                    //error messages here
                    //trace("ERROR!");
                    //trace('JsonOBJ_END-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
                });
        }
    };



    return( _thizOBJ_ );

});




/**
 * @license RequireJS i18n 2.0.4 Copyright (c) 2010-2012, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/requirejs/i18n for details
 */
/*jslint regexp: true */
/*global require: false, navigator: false, define: false */

/**
 * This plugin handles i18n! prefixed modules. It does the following:
 *
 * 1) A regular module can have a dependency on an i18n bundle, but the regular
 * module does not want to specify what locale to load. So it just specifies
 * the top-level bundle, like "i18n!nls/colors".
 *
 * This plugin will load the i18n bundle at nls/colors, see that it is a root/master
 * bundle since it does not have a locale in its name. It will then try to find
 * the best match locale available in that master bundle, then request all the
 * locale pieces for that best match locale. For instance, if the locale is "en-us",
 * then the plugin will ask for the "en-us", "en" and "root" bundles to be loaded
 * (but only if they are specified on the master bundle).
 *
 * Once all the bundles for the locale pieces load, then it mixes in all those
 * locale pieces into each other, then finally sets the context.defined value
 * for the nls/colors bundle to be that mixed in locale.
 *
 * 2) A regular module specifies a specific locale to load. For instance,
 * i18n!nls/fr-fr/colors. In this case, the plugin needs to load the master bundle
 * first, at nls/colors, then figure out what the best match locale is for fr-fr,
 * since maybe only fr or just root is defined for that locale. Once that best
 * fit is found, all of its locale pieces need to have their bundles loaded.
 *
 * Once all the bundles for the locale pieces load, then it mixes in all those
 * locale pieces into each other, then finally sets the context.defined value
 * for the nls/fr-fr/colors bundle to be that mixed in locale.
 */
(function () {
    'use strict';

    //regexp for reconstructing the master bundle name from parts of the regexp match
    //nlsRegExp.exec("foo/bar/baz/nls/en-ca/foo") gives:
    //["foo/bar/baz/nls/en-ca/foo", "foo/bar/baz/nls/", "/", "/", "en-ca", "foo"]
    //nlsRegExp.exec("foo/bar/baz/nls/foo") gives:
    //["foo/bar/baz/nls/foo", "foo/bar/baz/nls/", "/", "/", "foo", ""]
    //so, if match[5] is blank, it means this is the top bundle definition.
    var nlsRegExp = /(^.*(^|\/)nls(\/|$))([^\/]*)\/?([^\/]*)/;

    //Helper function to avoid repeating code. Lots of arguments in the
    //desire to stay functional and support RequireJS contexts without having
    //to know about the RequireJS contexts.
    function addPart(locale, master, needed, toLoad, prefix, suffix) {
        if (master[locale]) {
            needed.push(locale);
            if (master[locale] === true || master[locale] === 1) {
                toLoad.push(prefix + locale + '/' + suffix);
            }
        }
    }

    function addIfExists(req, locale, toLoad, prefix, suffix) {
        var fullName = prefix + locale + '/' + suffix;
        if (require._fileExists(req.toUrl(fullName + '.js'))) {
            toLoad.push(fullName);
        }
    }

    /**
     * Simple function to mix in properties from source into target,
     * but only if target does not already have a property of the same name.
     * This is not robust in IE for transferring methods that match
     * Object.prototype names, but the uses of mixin here seem unlikely to
     * trigger a problem related to that.
     */
    function mixin(target, source, force) {
        var prop;
        for (prop in source) {
            if (source.hasOwnProperty(prop) && (!target.hasOwnProperty(prop) || force)) {
                target[prop] = source[prop];
            } else if (typeof source[prop] === 'object') {
                if (!target[prop] && source[prop]) {
                    target[prop] = {};
                }
                mixin(target[prop], source[prop], force);
            }
        }
    }

    define('i18n',['module'], function (module) {
        var masterConfig = module.config ? module.config() : {};

        return {
            version: '2.0.4',
            /**
             * Called when a dependency needs to be loaded.
             */
            load: function (name, req, onLoad, config) {
                config = config || {};

                if (config.locale) {
                    masterConfig.locale = config.locale;
                }

                var masterName,
                    match = nlsRegExp.exec(name),
                    prefix = match[1],
                    locale = match[4],
                    suffix = match[5],
                    parts = locale.split("-"),
                    toLoad = [],
                    value = {},
                    i, part, current = "";

                //If match[5] is blank, it means this is the top bundle definition,
                //so it does not have to be handled. Locale-specific requests
                //will have a match[4] value but no match[5]
                if (match[5]) {
                    //locale-specific bundle
                    prefix = match[1];
                    masterName = prefix + suffix;
                } else {
                    //Top-level bundle.
                    masterName = name;
                    suffix = match[4];
                    locale = masterConfig.locale;
                    if (!locale) {
                        locale = masterConfig.locale =
                            typeof navigator === "undefined" ? "root" :
                            (navigator.language ||
                             navigator.userLanguage || "root").toLowerCase();
                    }
                    parts = locale.split("-");
                }

                if (config.isBuild) {
                    //Check for existence of all locale possible files and
                    //require them if exist.
                    toLoad.push(masterName);
                    addIfExists(req, "root", toLoad, prefix, suffix);
                    for (i = 0; i < parts.length; i++) {
                        part = parts[i];
                        current += (current ? "-" : "") + part;
                        addIfExists(req, current, toLoad, prefix, suffix);
                    }

                    req(toLoad, function () {
                        onLoad();
                    });
                } else {
                    //First, fetch the master bundle, it knows what locales are available.
                    req([masterName], function (master) {
                        //Figure out the best fit
                        var needed = [],
                            part;

                        //Always allow for root, then do the rest of the locale parts.
                        addPart("root", master, needed, toLoad, prefix, suffix);
                        for (i = 0; i < parts.length; i++) {
                            part = parts[i];
                            current += (current ? "-" : "") + part;
                            addPart(current, master, needed, toLoad, prefix, suffix);
                        }

                        //Load all the parts missing.
                        req(toLoad, function () {
                            var i, partBundle, part;
                            for (i = needed.length - 1; i > -1 && needed[i]; i--) {
                                part = needed[i];
                                partBundle = master[part];
                                if (partBundle === true || partBundle === 1) {
                                    partBundle = req(prefix + part + '/' + suffix);
                                }
                                mixin(value, partBundle);
                            }

                            //All done, notify the loader.
                            onLoad(value);
                        });
                    });
                }
            }
        };
    });
}());

//root UX language
define('modules/nls/uxlocale',{
    "root": {
    	"close":"close",
    	"open":"open",
        "play": "play",
        "pause": "pause",
        "stop": "stop",
		"rewind": "rewind",
		"fastforward": "fastforward",
		"skip": "skip"
    },

    //available translations
    "fr": true

});

/*!
 * jsontotemplate RequireJS Module
 * by Jonathan Robles
 * Date: 11/6/13[3:02 PM]
 */
/**NOTE: This object, takes a json object and applies it to an html block, also applies locale if necessary
 * USAGE:
 * var yourobject = new Module({
 *      dataIN:{some json data},
 *      htmlIN:'html with {{variable references}} or {{locale.text}} to pull from the json file',
 *      callback:function(data){alert('my new html: '+data)}
 * });
 *
 * yourobject.init();
 */
define('modules/data/jsontotemplate',['jquery','modules/definitions/standardmodule','i18n!modules/nls/uxlocale'],function ($,parentModel,locale) {  //replace the standardmodule with the module you are extending

    function _thizOBJ_( o ){
        var defaults={
            type:'jsontotemplate', //name your module, use this file's exact filename w/o the .js.
            author:'Jonathan Robles', //your name if you are the original writer
            lasteditby:'',  //your name if you are making the last edit (NOTE:all edits must have comments with your initials!)

            dataIN:{'window': {
                'title': 'Sample Widget',
                'name': 'main_window',
                'width': 500,
                'height': 500
            },
                'image': {
                    'src': 'Images/Sun.png',
                    'name': 'sun1',
                    'width': 250,
                    'height': 250,
                    'alignment': 'center'
                }},
            htmlIN:'<div id="{{window.name}}"><p>{{window.title}}</p><img src="{{image.src}}" alt="{{image.name}}" height="{{image.height}}" width="{{image.width}}"></div>',
            regEX:/{{(.*?)}}/g,
            errorFill:function(data){  return '[ xALERT!!! "'+data+'" not found in json]'  },
            data:undefined, //placeholder for final data
            callback:function(data){alert('data rendered :'+data)},
            busy:false

        };
        defaults= $.extend(defaults,o);
        parentModel.call( this,defaults);
        return( this );
    };

    _thizOBJ_.prototype = Object.create( parentModel.prototype );

    //DEFINE LISTENERS HERE
    _thizOBJ_.prototype._startlisteners =function(){
        this.notify('Trace','_startlisteners');
        var parent=this;
        var myID=  this._id();
        _notify.add(this._id(), function() {

            return {

                onJSONtoTemplateData:function(o){
                    if(o.senderID==myID){
                        parent._var({data: o.data,busy:false});
                        if(parent._var().callback!=undefined){
                            parent._var().callback(o.data)
                        }
                    }
                }
            }
        }());

    };

    // MODULE METHODS - you should always have init/refresh/kill defined!
    _thizOBJ_.prototype.init =function(){

        this.notify('Trace','init');
        this._startlisteners();//start this module's listeners
        this.refresh();
    };

    _thizOBJ_.prototype.refresh =function(){
        this.notify('Trace','refresh');
        this.translateData();
    };

    _thizOBJ_.prototype.kill =function(){
        this.notify('Trace','kill');
        _notify.rem(this._id());  //kills this module's listeners
    };


    // UNIQUE MODULE METHODS
    _thizOBJ_.prototype.translateData= function(){


        var recompose=function(obj,string){
            var parts = string.split('.');
            var newObj = obj[parts[0]];
            if(parts[1]){
                parts.splice(0,1);
                var newString = parts.join('.');
                return recompose(newObj,newString);
            }
            return newObj;
        } //F'ing awesome function for variable goodness

        var parent=this;
        parent._var({busy:true});
        this.notify('Trace','translateData dataIn');//:'+JSON.stringify(parent._var().dataIN)+' \n to datablock:'+parent._var().htmlIN);
        var onData=function(data){
            parent.notify('JSONtoTemplateData',data);
        };



        var tempjsondata=parent._var().dataIN;
        tempjsondata.locale=locale;
        var originalTemplate = parent._var().htmlIN;
        var processedTemplate = originalTemplate;
        
        var regEX = parent._var().regEX;
        var findinstances = originalTemplate.match(regEX);

        var data=undefined;

        if(findinstances!=null){
        $.each(findinstances,function(index){
            //alert()
            var searchstring=findinstances[index];
            var cleanstring=searchstring.replace(/[{}]/g, '');
            var newstring=recompose(tempjsondata,cleanstring);
            newstring=$('<div />').html(newstring).text();
            //If json value doesn't exist, use errorfill
            if(newstring!=undefined){processedTemplate=processedTemplate.replace(searchstring, newstring)} else {
                processedTemplate=processedTemplate.replace(searchstring, parent._var().errorFill(cleanstring))
            }


        });
            data=processedTemplate;
        } else {
            data=originalTemplate;
        };



        onData(data);




    };




    //-------------------------------------------------------------------------//
    return( _thizOBJ_ );

});




/**
 * myLifetime Slider Instance
 * sample by Jonathan Robles
 *
 * Date: 10/14
 *
 */

 require(['jquery', 'modules/controllers/carouselset','modules/parsers/dataparser','modules/data/jsontotemplate','tweenlite','scrollto'],
    function ($, Carousel, jsonParser,jsontoTemplate,TweenMax,Scrollto) {
        var mainOBJ=this;
        var self=this;
        var fullShows_list=[{"title":"Abby\u0027s Ultimate Dance Competition",
            "url":"shows\/abbys-ultimate-dance-competition",
            "nid":"22790","total_videos":"11"},
            {"title":"Dance Moms: Abby\u0027s Studio Rescue",
                "url":"shows\/abbys-studio-rescue",
                "nid":"22790","total_videos":"4"},
            {"title":"Raising Asia","url":"shows\/raising-asia",
                "nid":"22790","total_videos":"13"},
            {"title":"Bristol Palin: Life\u0027s a Tripp",
                "url":"shows\/bristol-palin-lifes-a-tripp",
                "nid":"22790","total_videos":"1"}];

        //This modifies the JSON data before it gets to the template
        var modifyData = function (getData){


            if(getData.duration!=undefined){


                var tempnum=Math.round(Number(getData.duration));
                var tempremainder=(tempnum%60);
                if (tempremainder<=9){tempremainder='0'+tempremainder}
                getData.duration=Math.floor(tempnum/60)+':'+tempremainder;



            }

            if(getData.season==''||getData.season==undefined){getData.season='0X'};
            if(getData.episode==''||getData.episode==undefined){getData.episode='0X'};
            if(getData.duration==''||getData.duration==undefined){getData.duration='X0X'};

            var mpxdataattrib='';
            if(getData.behind_wall=='1'){mpxdataattrib='data-auth-required="true" data-mpx-id="'+getData.mpxid+'" '}
            getData.mpxdataattrib=mpxdataattrib;

            return getData;
        }

        //This modifies the output before it gets displayed
        var modifyHTML = function (getHTML) {


            var getHTML = getHTML.replace("S0X | E0X |", "");
            var getHTML = getHTML.replace("S0X | E0X", "");
            var getHTML = getHTML.replace("E0X | X0X", "");
            var getHTML = getHTML.replace("S0X |", "");
            var getHTML = getHTML.replace("| X0X", "");
            var getHTML = getHTML.replace("E0X", "");
            var getHTML = getHTML.replace(/X0X/gi, "");

            var getHTML = getHTML.replace("Season 0X | Episode 0X |", "");
            var getHTML = getHTML.replace("Season 0X | Episode 0X", "");
            var getHTML = getHTML.replace("Episode 0X | X0X", "");
            var getHTML = getHTML.replace("Season 0X |", "");
            var getHTML = getHTML.replace("Episode 0X", "");

            return getHTML;
        }

        var initializeScroller = function(scrollerOBJ,getDATA){
            //initialize the scroller!

            if(getDATA!=undefined){
                scrollerOBJ.instance._var({
                    custom_contentData:getDATA.items
                })
            }; //plug in partial data to objects itemlist


            //initialize stuff
            scrollerOBJ.instance.init();
            scrollerOBJ.instance._var().custom_jsontotemplate._startlisteners();
            scrollerOBJ.instance._var().custom_Dataparser._startlisteners();




            //make navigation
            $(scrollerOBJ.obj).find(scrollerOBJ.data.next).on((_global$.isTouch)?'touchstart':'click',function(){
                scrollerOBJ.instance.next();
            });
            $(scrollerOBJ.obj).find(scrollerOBJ.data.previous).on((_global$.isTouch)?'touchstart':'click',function(){
                scrollerOBJ.instance.prev();
            });


        }


        var makeAd =function() {


            var adhtml='<div class="row-fluid standalone_ad_container container_outer">'
            adhtml+='<div class="container_inner">'
            adhtml+='<div class="standalone_ad_container_ad_area">'
            adhtml+='</div>'
            adhtml+='</div>'
            adhtml+='</div>'

            $('.fullepisodes_container').find('.container_inner').find('div.episode_set:eq(2)').after(adhtml);



            //start onWidth Listener




        };

        var placeAd =function() {
            var placeItnow= function(){var jumptoY=$('.standalone_ad_container_ad_area').offset().top;
                var diffY=$('.dart-name-sidebar_300x250').offset().top;
                var addYtarget=$('.dart-name-sidebar_300x250').position().top+(jumptoY-diffY);
                $('.dart-name-sidebar_300x250').css('top',(addYtarget+'px'));};

            placeItnow();
            if(waitforAnimation!=undefined){clearInterval(waitforAnimation)};
            var waitforAnimation=setTimeout(
                function(){
                    placeItnow();
                    $('.dart-name-sidebar_300x250').addClass('visible');
                }


                ,1000); //compensate for any height animations
        }


        var applyCarousels = function(){
            //build Carousels list
            var pageCarousels=new Array();
            $.each($('[data-uiType="carousel"]'), function (index, value) {


               $(value).attr('data-uiType','carousel_rendered') ;


            var uiData = $(value).attr('data-uiData')
            uiData = eval("(" + uiData + ")");//get JSON

            var targetListContainer = $(value).find(uiData.container)
            pageCarousels.push({
                obj: value,
                instance: new Carousel({
                    custom_updateClassesAndImagePreload: function (o) {
                        //alert('debounce callback!');
                        var visibleslides = this.visibleslides;
                        var visibleslides_with_bleed = this.visibleslides_with_bleed;
                        var childObjects = this.childObjects;

                        //console.log(visibleslides + ' : ' + visibleslides_with_bleed + ' : ' + childObjects)

                        $.each(childObjects, function (index, value) {
                            var isthere = _.indexOf(visibleslides, index);
                            if (isthere != -1) {
                                $(value).addClass('inview');
                                $(value).addClass('viewed');
                            } else {
                                $(value).removeClass('inview');
                            }
                        })

                        $.each(childObjects, function (index, value) {
                            var isthere = _.indexOf(visibleslides_with_bleed, index);
                            if (isthere != -1) {
                                $(value).addClass('inbleed');
                            } else {
                                $(value).removeClass('inbleed');
                            }
                        })


                        $.each(visibleslides_with_bleed, function (index, value) {

                            $(childObjects[value]).find('[data-imageUrl]').each(function () {
                                var tempimageCAN = $(this).attr('data-imageUrl');
                                var tempimageSRC = $(this).attr('src');
                                //console.log(tempimageCAN+' & '+tempimageSRC);
                                if (tempimageCAN != tempimageSRC) {
                                    $(this).attr('src', tempimageCAN)
                                }

                            });

                        })
                    },
                    custom_MissingbleedIndex:function(){
                        var self=this;
                        var visibleslides_with_bleed = this.visibleslides_with_bleed.reverse();
                        var returnCandidate=undefined; //true
                        $.each(visibleslides_with_bleed, function (index, value) {

                            var isMissing=(self.custom_contentData[value]==undefined);

                           // _global$.notify('Trace','xox checking item:'+value+' missing?'+isMissing);


                            if (isMissing){
                                returnCandidate=value;


                            }
                        });
                        //RETURN THE INDEX OF MISSING STUFF
                        return returnCandidate;
                    },
                    custom_jsontotemplate:new jsontoTemplate({
                        dataIN:undefined,
                        htmlIN:undefined,
                        callback:function(data){
                            alert(data);
                        }
                    }),
                    custom_Dataparser: new jsonParser({
                                usenocache:(_global$.getQuery('debug')),
                                file: undefined,
                                format: 'json',
                                callback: function (data) {
                                    alert(JSON.stringify(data.items));
                                }
                            }),
                    custom_uiData:uiData,
                    custom_contentData:[],   //Array that WILL HOLD ITEM DATA
                    custom_datatoIndex:function(getData){
                        var self=this;
                        _global$.notify('Trace','custom_datatoIndex('+getData.index+') >>> '+getData.data);


                        var getHTML=$(self.childObjects[getData.index]).html()


                        self.custom_jsontotemplate._var({
                            dataIN:modifyData(getData.data),
                            htmlIN:getHTML,
                            callback:function(data){

                                $(self.childObjects[getData.index]).empty().append(modifyHTML(data));
                               // $(self.childObjects[getData.index]).removeClass('loading');
                            }
                        })
                        self.custom_jsontotemplate.refresh();
                        self.custom_updateClassesAndImagePreload();



                    },


                    target: targetListContainer,
                    attachdisableclassonid:$(value),
                    attachhidearrowclassonid:$(value),

                    usetouchbinds:_global$.isTouch, //adds touchbinds to the target

                    callback: function (o) {

                        this.custom_updateClassesAndImagePreload(o);
                    },  //runs when swipepanel is made





                    onscroll_pause: function (o) {
                        //console.log('onscroll_pause');
                        var self=this;

                        if(this.custom_uiData.dataservice!=undefined){

                        var missingIndexData=this.custom_MissingbleedIndex();


                        if(missingIndexData!=undefined){


                        var numberofItemstoget=Number(this.custom_uiData.datacount);
                        var myDataURL= this.custom_uiData.dataservice;

                        var convertIndextoPage = function (getIndex) {
                            var pagecandidate = Math.floor(getIndex/numberofItemstoget);
                            return pagecandidate;
                        };
                        var getPage=convertIndextoPage(missingIndexData);

                        var convertIndextoHardIndex =function (getIndex) {
                            var pagecandidate = convertIndextoPage(getIndex)*numberofItemstoget
                            return pagecandidate;
                        };



                            _global$.notify('Trace',' received index:'+missingIndexData+' calculate page:'+convertIndextoPage(missingIndexData)+' start replacement index:'+convertIndextoHardIndex(missingIndexData));

                            // $(self.childObjects[getData.index]).removeClass('loading');

                        var myDataURLwithCount= myDataURL + '/'+getPage+((numberofItemstoget!=undefined)? ('/'+numberofItemstoget):'');
                        if (!_global$.islocalhost){myDataURL=myDataURLwithCount}; //patch for local development
                        _global$.notify('Trace','attempting to retrieve:'+ myDataURLwithCount+'  to index:'+missingIndexData+'  page:'+convertIndextoPage(missingIndexData));

                            //SHOWLOADER!
                            for(tempvar=0;tempvar<numberofItemstoget;tempvar++){

                                $(self.childObjects[convertIndextoHardIndex(missingIndexData)+tempvar]).addClass('loading');
                                $(self.childObjects[convertIndextoHardIndex(missingIndexData)+tempvar]).removeClass('notloaded');
                            }


                            //LOADEM!
                            self.custom_Dataparser._var({
                                file: myDataURL,
                                callback: function (data) {
                                    var listData=data.items;

                                $.each(listData,function(index,value){





                                    var useIndex = (          (convertIndextoPage(missingIndexData)*numberofItemstoget)          +index);





                                    var useData =     value;

                                    self.custom_contentData[useIndex]=useData;  //put data in Array
                                    self.custom_datatoIndex({index:useIndex,data:useData});  //put data in Object
                                    $(self.childObjects[useIndex]).removeClass('loading');
                                })
                            }
                            });
                            self.custom_Dataparser.refresh();


                        }




                        }else{

                            this.custom_updateClassesAndImagePreload(o);
                        }


                        //console.log('moving :'+ o.currentposition+' [ '+ o.currentpercent+'% ]')

                    }, //on scrolling callback

                    onscroll_debounce_callback: function (o) {
                        //alert(this.custom_contentData)
                        //console.log('onscroll_debounce_callback');
                        this.custom_updateClassesAndImagePreload(o);

                    }
                }),
                data: uiData
            });

        })
            //start Carousels in list
            $.each(pageCarousels,function(index,value){


            var _InitialBuild= function(getDATA){
//                alert('Successfully read data!: ' + JSON.stringify(getDATA) + ' !' +JSON.stringify(value.data))

                var listContainer=$(value.obj).find(value.data.container);
                var htmltemplateHolder=$(value.obj).find(value.data.template);
                var htmlTemplate=htmltemplateHolder.html();
                var htmlTemplateStrip = htmlTemplate.replace(/\s/g, '');
                var itemCount = Number(getDATA.total)

                if(htmltemplateHolder.length!=0&&htmlTemplateStrip.length!=0){

                    //clear the listcontainer
                    listContainer.empty();


for(temp_count=0;temp_count<itemCount;temp_count++){


    var injectData=getDATA.items[temp_count];


    //initially build scroller with as much data as possible
    if(injectData!=undefined){
        var pluggedInHTML = new jsontoTemplate({
            dataIN:modifyData(injectData),
            htmlIN:htmlTemplate,
            callback:function(data){
                //alert(data);
                listContainer.append(modifyHTML(data))

            }


        }).init();


    } else {

        listContainer.append(htmlTemplate);
        listContainer.children().last().addClass('notloaded')
    }

    //$(listContainer.children()[listContainer.children().length]).addClass('notloaded');


}

                    initializeScroller(value,getDATA); //BUILD THE SCROLLER!!!

//                    alert(
//
//                        itemCount+' <<< '+
//
//                        htmltemplateHolder.length+' '+
//                            htmlTemplateStrip
//
//                    );



                }else{



                    _global$.notify('Trace',('There is no template called "'+value.data.template+'" or the content is blank!') );
                }



            };

            //if there is json data lets do some special stuff
           if(value.data.dataservice!=undefined){

               var myDataURL = value.data.dataservice;
               var myDataURLwithCount= myDataURL + '/0'+((value.data.datacount!=undefined)? ('/'+value.data.datacount):'');

               if (!_global$.islocalhost){myDataURL=myDataURLwithCount};

               _global$.notify('Trace','attempting to retrieve:'+ myDataURLwithCount);

               var myjsontest = new jsonParser({
                   usenocache:(_global$.getQuery('debug')),
                   file: myDataURL,
                   format: 'json',
                   callback: function (data) {
                       _InitialBuild(data);
                   }
               });
               myjsontest.init();



           }  else {
               initializeScroller(value); //BUILD THE SCROLLER!!!
           }


        });

           // alert('xoxi')
if(_global$.path.indexOf('watch-full-episodes-online')!=-1){
    makeAd();
    showhideOnAdobePass();
    placeAd();

$('body').prepend('<div id="back-top" class="hidethis"><img src="profiles/mylifetime_com/themes/custom/ltv/images/show-landing-button-up.png"></div>');


            _notify.add('slider_instance', function() {
                return {
                    onWindowWidth:function(o){
                        placeAd();
                    },
                    onWindowScrollStart:function(o){
                        //console.log('scrollstart');
                    },
                    onWindowScrollEnd:function(o){
                        //console.log('scrollend');
                        if (_global$.lastscrollpositionY>_global$.windowHeight ){
                            $('#back-top').removeClass('hidethis');
                        } else {
                            $('#back-top').addClass('hidethis');

                        };
                        placeAd();
                    }
                }
            }());



} else {
    showhideOnAdobePass();

    $('body').prepend('<div id="back-top" class="hidethis"><img src="profiles/mylifetime_com/themes/custom/ltv/images/show-landing-button-up.png"></div>');

    _notify.add('slider_instance', function() {
        return {
            onWindowScrollEnd:function(o){


                if (_global$.lastscrollpositionY>_global$.windowHeight ){
                    $('#back-top').removeClass('hidethis');
                } else {
                    $('#back-top').addClass('hidethis');

                };

            }
        }
    }());



}



        };
        var makeButtons = function() {
               $(document).on('click','[data-uiType^="button_jumpto"]',function(value){
                    window.location.href = $(this).attr('data-uiType').substring(14,999);
                });

                $(document).on('click','[data-uiType="adobePassLogin"]',function(value){



                    try {
                        aetn.video.adobePass.login()
                    }
                    catch (e) {
                        console.log('ALERT!: aetn.video.adobePass.login() failed, prerequisite js loaded? '+e)
                    }


                });

            $(document).on((_global$.isTouch)?'touchstart':'click','#back-top',function(value){
               TweenMax.to(window, 2, {scrollTo:{y:0}, ease:Power2.easeOut});
            });
            //Second POPULATE fullepisodes template
            applyFEtemplate();
        };





        var applyFEtemplate = function(){




            if($('[data-uiType="full_episode_template"]').length==0){applyCarousels();}  else {



                var templateHolder = new Array(); //build Array to hold html templates

                var FEjsonplug = new jsontoTemplate({
                    dataIN:undefined,
                    htmlIN:undefined,
                    regEX:/{{{(.*?)}}}/g

                });
                var FEjsonloader = new jsonParser({
                    usenocache:(_global$.getQuery('debug'))
                });

                FEjsonplug._startlisteners();
                FEjsonloader._startlisteners();


                //find full episode template
                $.each($('[data-uiType="full_episode_template"]'),function(index,value){
                    //HIDE THIS OBJECT TILL ITS DONE BEING CREATED (you really should hardcode the class so its invisible right away)
                    $(value).addClass('hidden'); //
                    var uiData = $(value).attr('data-uiData');




                    uiData = eval("(" + uiData + ")");//get JSON

                    //hide button
                    $('[data-uiType="'+uiData.loadmore_button_uiType+'"]').hide();

                    if(uiData.loadlist!=undefined){

                        //Build the load list URL
                        var buildloadurl=uiData.loadlist;

                        if (!_global$.islocalhost){
                            buildloadurl+='/0/'+uiData.loadcount;
                        }
                        //PLACE the data in an abject incase we need it later
                        templateHolder.push({
                            target_obj:value,
                            html_template:$(value).html(),
                            data:uiData,
                            loadPageCount:uiData.loadcount,
                            currentPage:0
                        });
                        //LOAD JSON and plug it in
                        FEjsonloader._var({
                            file: buildloadurl,
                            callback: function (newloaddata) {
                                //BUILD
                                var originalHTML=$(value).html();
                                $(value).empty();
                                //BUILD each ROW
                                $.each(newloaddata.items,function(e_index,e_value){
                                    e_value= $.extend(e_value,uiData);
                                    FEjsonplug._var({
                                        dataIN:modifyData(e_value),
                                        htmlIN:originalHTML,
                                        callback:function(renderedHTML){
                                            $(value).append(modifyHTML(renderedHTML));
                                        }
                                    })
                                    FEjsonplug.refresh();
                                });
                                //SHOW ROWS
                                $(value).removeClass('hidden');
                                //POPULATE ROWS

                                if(Number(newloaddata.total)>$(value).children().length){
                                        $('[data-uiType="'+uiData.loadmore_button_uiType+'"]').show();
                                    }

                                //alert(Number(newloaddata.total)+' '+$(value).children().length)
//                                if(newloaddata.length==Number(uiData.loadcount)){
//                                    $('[data-uiType="'+uiData.loadmore_button_uiType+'"]').show();
//                                }


                                applyCarousels();
                            }
                        });
                        FEjsonloader.refresh();

                    } else {
                        //NO LOADLIST
                        alert('please define loadlist (json data for the scrollers)!');
                    }
                });



                $.each(templateHolder,function(index,value){
                    var self=templateHolder[index];
                   // alert(value.html_template)
                   // alert(JSON.stringify(value.data));
                   // alert();

//                    $.each($('[data-uiType="'+value.data.loadmore_button_uiType+'"]'),function(button_index,button_value){
//
//                    });


                    $(document).on('click','[data-uiType="'+value.data.loadmore_button_uiType+'"]',function(){
                      // alert('hi'+value.loadPageCount);
                     //   value.loadPageCount++
                       // window.location.href = $(this).attr('data-uiType').substring(14,999);

                        var thisButton=this;
                        var uiData=value.data;

                        //Build the load list URL
                        var buildloadurl=uiData.loadlist;

                        value.currentPage++
                       // alert(value.currentPage)
                        if (!_global$.islocalhost){
                            buildloadurl+='/'+value.currentPage+'/'+value.loadPageCount;
                        }


                        //LOAD JSON and plug it in
                        FEjsonloader._var({
                            file: buildloadurl,
                            callback: function (newloaddata) {
                                //BUILD
                                var originalHTML=value.html_template
                               // $(value).empty();
                                //BUILD each ROW
                                $.each(newloaddata.items,function(e_index,e_value){
                                    e_value= $.extend(e_value,uiData);
                                    FEjsonplug._var({
                                        dataIN:modifyData(e_value),
                                        htmlIN:originalHTML,
                                        callback:function(renderedHTML){
                                            //alert(renderedHTML)
                                            $(value.target_obj).append(modifyHTML(renderedHTML));
                                        }
                                    })
                                    FEjsonplug.refresh();
                                });
                                //SHOW ROWS
                                //$(value).removeClass('hidden');
                                //POPULATE ROWS

                                // if(Number(newloaddata.total)>$(value).children().length){
                                //alert( ($(value.target_obj).children().length>=Number(newloaddata.total))  );
                                var Showingall= ($(value.target_obj).children().length>=Number(newloaddata.total));

                                if(newloaddata.length<Number(value.loadPageCount)  || Showingall){

                                    $(thisButton).hide();

                                }

                                applyCarousels();
                            }
                        });
                        FEjsonloader.refresh();


















                    });




                })


            }
            //Third MAKE THEM CAROUSELS!

        }
        var startAdobePassListeners = function (){
            try {
                aetn.video.adobePass.addEventListener( 'displayUserAsUnAuthenticated', function(){
                    console.log('removeClass(hideOnVLoginStart)');
                    $('[data-uiType="hideOnVLoginStart"]').removeClass('hideOnVLoginStart'); //show login
                });

                aetn.video.adobePass.addEventListener( 'displayUserAsAuthenticated', function(){
                    console.log('addClass(hideOnVLoginStart)');
                    $('[data-uiType="hideOnVLoginStart"]').addClass('hideOnVLoginStart'); //hide login

                });

            }
            catch (e) {
                console.log('keep hidden if On Adobe.pass FAIL');
            }

        };
        var showhideOnAdobePass = function(){
           // alert('p3');

            var isAndroid=$('body').hasClass('android');
            var isIOS=$('body').hasClass('iphone')||$('body').hasClass('ipad');
//            if(!(isAndroid||isIOS)){
                //startAdobePassListeners();
                $('[data-uiType="hideOnVLoginStart"]').removeClass('hideOnVLoginStart');


           // };

            //placeAd();
            //var waitforAnimation=setTimeout(placeAd,1000);
        };



//        var checkAdobePass = function(OBJ){
//
//            if(!aetn.video.adobePass.isAuthenticated()){
//                console.log('unhiding login block');
//                $(OBJ).removeClass('hideOnVLoginStart'); //show login
//            } else {
//                console.log('hiding login block');
//                $(OBJ).addClass('hideOnVLoginStart'); //hide login
//
//            }
//        }


        makeButtons();


        //unhide stuff on *not* mobile
        $('.hideOnMobile').each(function(idx,value){
            var isAndroid=$('body').hasClass('android');
            var isIOS=$('body').hasClass('iphone')||$('body').hasClass('ipad');
            if(!(isAndroid||isIOS)){$(value).removeClass('hideOnMobile')};
        })



//        _notify.add('slider_instance', function() {
//            return {
//                onWindowScroll:function(o){
//                    alert('!'+ o.data.y +' : '+_global$.windowHeight)
//                }
//            }
//        }());




//        <iframe id="aetn_dart_2" iframe="" frameborder="0" scrolling="no" width="300" height="250" src="http://local.movies.mylifetime.com/profiles/mylifetime_com/modules/aenetworks/aetn_dart/iframe/multisize-iframe.html?dfp=N5936&amp;path=ltv.myltv.movies%2Flanding%3Bs1%3Dlanding%3Bs2%3D%3Bpid%3Dwatch-full-episodes-online%3Bmovie%3D%3Bptype%3D%3Baetn%3Dad%3Bmovies%3Dltv%3Btest%3D%3Bkuid%3Dokob4snnl%3Bpos%3Dtop%3Bsz%3D300x250%3Bshow%3D%3Btile%3D2%3Bord%3D1730669341%3F"></iframe>


        //alert($('div.standalone_ad_container').html())//$('div.standalone_ad_container').appendTo('#testOBJ');
        //alert('?')

        //$('#testOBJ').append($('div.standalone_ad_container').html())













    });


// End main slideshow_promo instance




;
define("instances/slideshow_lifetimeslider_instance.js", function(){});

