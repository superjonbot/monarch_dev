/*! Drupal Site JS : header_global.js */
/*! codebase: Monarch v1.1.5 by Jonathan Robles */
/*! built:12-27-2014 [5:24:02PM] */
/*! -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

/*! Prerequisites: jQuery, Underscore */

/*! Third Party Includes [start] */
/* xxx */
/*! Third Party Includes [end] */
var requirejs, require, define;

(function(undef) {
    var main, req, makeMap, handlers, defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex, foundI, foundStarMap, starI, i, j, part, baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = map && map["*"] || {};
        if (name && name.charAt(0) === ".") {
            if (baseName) {
                baseParts = baseParts.slice(0, baseParts.length - 1);
                name = name.split("/");
                lastIndex = name.length - 1;
                if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                    name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, "");
                }
                name = baseParts.concat(name);
                for (i = 0; i < name.length; i += 1) {
                    part = name[i];
                    if (part === ".") {
                        name.splice(i, 1);
                        i -= 1;
                    } else if (part === "..") {
                        if (i === 1 && (name[2] === ".." || name[0] === "..")) {
                            break;
                        } else if (i > 0) {
                            name.splice(i - 1, 2);
                            i -= 2;
                        }
                    }
                }
                name = name.join("/");
            } else if (name.indexOf("./") === 0) {
                name = name.substring(2);
            }
        }
        if ((baseParts || starMap) && map) {
            nameParts = name.split("/");
            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");
                if (baseParts) {
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join("/")];
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }
                if (foundMap) {
                    break;
                }
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }
            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }
            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join("/");
            }
        }
        return name;
    }

    function makeRequire(relName, forceSync) {
        return function() {
            var args = aps.call(arguments, 0);
            if (typeof args[0] !== "string" && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function(name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function(value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }
        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error("No " + name);
        }
        return defined[name];
    }

    function splitPrefix(name) {
        var prefix, index = name ? name.indexOf("!") : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }
    makeMap = function(name, relName) {
        var plugin, parts = splitPrefix(name),
            prefix = parts[0];
        name = parts[1];
        if (prefix) {
            prefix = normalize(prefix, relName);
            plugin = callDep(prefix);
        }
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relName));
            } else {
                name = normalize(name, relName);
            }
        } else {
            name = normalize(name, relName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }
        return {
            f: prefix ? prefix + "!" + name : name,
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function() {
            return config && config.config && config.config[name] || {};
        };
    }
    handlers = {
        require: function(name) {
            return makeRequire(name);
        },
        exports: function(name) {
            var e = defined[name];
            if (typeof e !== "undefined") {
                return e;
            } else {
                return defined[name] = {};
            }
        },
        module: function(name) {
            return {
                id: name,
                uri: "",
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };
    main = function(name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i, args = [],
            callbackType = typeof callback,
            usingExports;
        relName = relName || name;
        if (callbackType === "undefined" || callbackType === "function") {
            deps = !deps.length && callback.length ? ["require", "exports", "module"] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relName);
                depName = map.f;
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) || hasProp(waiting, depName) || hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + " missing " + depName);
                }
            }
            ret = callback ? callback.apply(defined[name], args) : undefined;
            if (name) {
                if (cjsModule && cjsModule.exports !== undef && cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    defined[name] = ret;
                }
            }
        } else if (name) {
            defined[name] = callback;
        }
    };
    requirejs = require = req = function(deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                return handlers[deps](callback);
            }
            return callDep(makeMap(deps, callback).f);
        } else if (!deps.splice) {
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }
            if (callback.splice) {
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }
        callback = callback || function() {};
        if (typeof relName === "function") {
            relName = forceSync;
            forceSync = alt;
        }
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            setTimeout(function() {
                main(undef, deps, callback, relName);
            }, 4);
        }
        return req;
    };
    req.config = function(cfg) {
        return req(cfg);
    };
    requirejs._defined = defined;
    define = function(name, deps, callback) {
        if (!deps.splice) {
            callback = deps;
            deps = [];
        }
        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };
    define.amd = {
        jQuery: true
    };
})();

define("almondLib", function() {});

window.Modernizr = function(a, b, c) {
        function B(a) {
            j.cssText = a;
        }

        function C(a, b) {
            return B(m.join(a + ";") + (b || ""));
        }

        function D(a, b) {
            return typeof a === b;
        }

        function E(a, b) {
            return !!~("" + a).indexOf(b);
        }

        function F(a, b) {
            for (var d in a) {
                var e = a[d];
                if (!E(e, "-") && j[e] !== c) return b == "pfx" ? e : !0;
            }
            return !1;
        }

        function G(a, b, d) {
            for (var e in a) {
                var f = b[a[e]];
                if (f !== c) return d === !1 ? a[e] : D(f, "function") ? f.bind(d || b) : f;
            }
            return !1;
        }

        function H(a, b, c) {
            var d = a.charAt(0).toUpperCase() + a.slice(1),
                e = (a + " " + o.join(d + " ") + d).split(" ");
            return D(b, "string") || D(b, "undefined") ? F(e, b) : (e = (a + " " + p.join(d + " ") + d).split(" "),
                G(e, b, c));
        }
        var d = "2.7.1",
            e = {},
            f = !0,
            g = b.documentElement,
            h = "modernizr",
            i = b.createElement(h),
            j = i.style,
            k, l = {}.toString,
            m = " -webkit- -moz- -o- -ms- ".split(" "),
            n = "Webkit Moz O ms",
            o = n.split(" "),
            p = n.toLowerCase().split(" "),
            q = {
                svg: "http://www.w3.org/2000/svg"
            },
            r = {},
            s = {},
            t = {},
            u = [],
            v = u.slice,
            w, x = function(a, c, d, e) {
                var f, i, j, k, l = b.createElement("div"),
                    m = b.body,
                    n = m || b.createElement("body");
                if (parseInt(d, 10))
                    while (d--) j = b.createElement("div"), j.id = e ? e[d] : h + (d + 1),
                        l.appendChild(j);
                return f = ["&#173;", '<style id="s', h, '">', a, "</style>"].join(""), l.id = h, (m ? l : n).innerHTML += f, n.appendChild(l), m || (n.style.background = "", n.style.overflow = "hidden",
                        k = g.style.overflow, g.style.overflow = "hidden", g.appendChild(n)), i = c(l, a),
                    m ? l.parentNode.removeChild(l) : (n.parentNode.removeChild(n), g.style.overflow = k), !!i;
            },
            y = function(b) {
                var c = a.matchMedia || a.msMatchMedia;
                if (c) return c(b).matches;
                var d;
                return x("@media " + b + " { #" + h + " { position: absolute; } }", function(b) {
                    d = (a.getComputedStyle ? getComputedStyle(b, null) : b.currentStyle)["position"] == "absolute";
                }), d;
            },
            z = {}.hasOwnProperty,
            A;
        !D(z, "undefined") && !D(z.call, "undefined") ? A = function(a, b) {
            return z.call(a, b);
        } : A = function(a, b) {
            return b in a && D(a.constructor.prototype[b], "undefined");
        }, Function.prototype.bind || (Function.prototype.bind = function(b) {
            var c = this;
            if (typeof c != "function") throw new TypeError();
            var d = v.call(arguments, 1),
                e = function() {
                    if (this instanceof e) {
                        var a = function() {};
                        a.prototype = c.prototype;
                        var f = new a(),
                            g = c.apply(f, d.concat(v.call(arguments)));
                        return Object(g) === g ? g : f;
                    }
                    return c.apply(b, d.concat(v.call(arguments)));
                };
            return e;
        }), r.canvas = function() {
            var a = b.createElement("canvas");
            return !!a.getContext && !!a.getContext("2d");
        }, r.touch = function() {
            var c;
            return "ontouchstart" in a || a.DocumentTouch && b instanceof DocumentTouch ? c = !0 : x(["@media (", m.join("touch-enabled),("), h, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function(a) {
                c = a.offsetTop === 9;
            }), c;
        }, r.geolocation = function() {
            return "geolocation" in navigator;
        }, r.cssanimations = function() {
            return H("animationName");
        }, r.csstransforms = function() {
            return !!H("transform");
        }, r.csstransforms3d = function() {
            var a = !!H("perspective");
            return a && "webkitPerspective" in g.style && x("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(b, c) {
                a = b.offsetLeft === 9 && b.offsetHeight === 3;
            }), a;
        }, r.csstransitions = function() {
            return H("transition");
        }, r.fontface = function() {
            var a;
            return x('@font-face {font-family:"font";src:url("https://")}', function(c, d) {
                var e = b.getElementById("smodernizr"),
                    f = e.sheet || e.styleSheet,
                    g = f ? f.cssRules && f.cssRules[0] ? f.cssRules[0].cssText : f.cssText || "" : "";
                a = /src/i.test(g) && g.indexOf(d.split(" ")[0]) === 0;
            }), a;
        }, r.video = function() {
            var a = b.createElement("video"),
                c = !1;
            try {
                if (c = !!a.canPlayType) c = new Boolean(c), c.ogg = a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""),
                    c.h264 = a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), c.webm = a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, "");
            } catch (d) {}
            return c;
        }, r.audio = function() {
            var a = b.createElement("audio"),
                c = !1;
            try {
                if (c = !!a.canPlayType) c = new Boolean(c), c.ogg = a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""),
                    c.mp3 = a.canPlayType("audio/mpeg;").replace(/^no$/, ""), c.wav = a.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""),
                    c.m4a = (a.canPlayType("audio/x-m4a;") || a.canPlayType("audio/aac;")).replace(/^no$/, "");
            } catch (d) {}
            return c;
        }, r.webworkers = function() {
            return !!a.Worker;
        }, r.svg = function() {
            return !!b.createElementNS && !!b.createElementNS(q.svg, "svg").createSVGRect;
        }, r.inlinesvg = function() {
            var a = b.createElement("div");
            return a.innerHTML = "<svg/>", (a.firstChild && a.firstChild.namespaceURI) == q.svg;
        }, r.svgclippaths = function() {
            return !!b.createElementNS && /SVGClipPath/.test(l.call(b.createElementNS(q.svg, "clipPath")));
        };
        for (var I in r) A(r, I) && (w = I.toLowerCase(), e[w] = r[I](), u.push((e[w] ? "" : "no-") + w));
        return e.addTest = function(a, b) {
                if (typeof a == "object")
                    for (var d in a) A(a, d) && e.addTest(d, a[d]);
                else {
                    a = a.toLowerCase();
                    if (e[a] !== c) return e;
                    b = typeof b == "function" ? b() : b, typeof f != "undefined" && f && (g.className += " " + (b ? "" : "no-") + a),
                        e[a] = b;
                }
                return e;
            }, B(""), i = k = null,
            function(a, b) {
                function l(a, b) {
                    var c = a.createElement("p"),
                        d = a.getElementsByTagName("head")[0] || a.documentElement;
                    return c.innerHTML = "x<style>" + b + "</style>", d.insertBefore(c.lastChild, d.firstChild);
                }

                function m() {
                    var a = s.elements;
                    return typeof a == "string" ? a.split(" ") : a;
                }

                function n(a) {
                    var b = j[a[h]];
                    return b || (b = {}, i++, a[h] = i, j[i] = b), b;
                }

                function o(a, c, d) {
                    c || (c = b);
                    if (k) return c.createElement(a);
                    d || (d = n(c));
                    var g;
                    return d.cache[a] ? g = d.cache[a].cloneNode() : f.test(a) ? g = (d.cache[a] = d.createElem(a)).cloneNode() : g = d.createElem(a),
                        g.canHaveChildren && !e.test(a) && !g.tagUrn ? d.frag.appendChild(g) : g;
                }

                function p(a, c) {
                    a || (a = b);
                    if (k) return a.createDocumentFragment();
                    c = c || n(a);
                    var d = c.frag.cloneNode(),
                        e = 0,
                        f = m(),
                        g = f.length;
                    for (; e < g; e++) d.createElement(f[e]);
                    return d;
                }

                function q(a, b) {
                    b.cache || (b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment,
                        b.frag = b.createFrag()), a.createElement = function(c) {
                        return s.shivMethods ? o(c, a, b) : b.createElem(c);
                    }, a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + m().join().replace(/[\w\-]+/g, function(a) {
                        return b.createElem(a), b.frag.createElement(a), 'c("' + a + '")';
                    }) + ");return n}")(s, b.frag);
                }

                function r(a) {
                    a || (a = b);
                    var c = n(a);
                    return s.shivCSS && !g && !c.hasCSS && (c.hasCSS = !!l(a, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")),
                        k || q(a, c), a;
                }
                var c = "3.7.0",
                    d = a.html5 || {},
                    e = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
                    f = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
                    g, h = "_html5shiv",
                    i = 0,
                    j = {},
                    k;
                (function() {
                    try {
                        var a = b.createElement("a");
                        a.innerHTML = "<xyz></xyz>", g = "hidden" in a, k = a.childNodes.length == 1 || function() {
                            b.createElement("a");
                            var a = b.createDocumentFragment();
                            return typeof a.cloneNode == "undefined" || typeof a.createDocumentFragment == "undefined" || typeof a.createElement == "undefined";
                        }();
                    } catch (c) {
                        g = !0, k = !0;
                    }
                })();
                var s = {
                    elements: d.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
                    version: c,
                    shivCSS: d.shivCSS !== !1,
                    supportsUnknownElements: k,
                    shivMethods: d.shivMethods !== !1,
                    type: "default",
                    shivDocument: r,
                    createElement: o,
                    createDocumentFragment: p
                };
                a.html5 = s, r(b);
            }(this, b), e._version = d, e._prefixes = m, e._domPrefixes = p, e._cssomPrefixes = o,
            e.mq = y, e.testProp = function(a) {
                return F([a]);
            }, e.testAllProps = H, e.testStyles = x, g.className = g.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (f ? " js " + u.join(" ") : ""),
            e;
    }(this, this.document),
    function(a, b, c) {
        function d(a) {
            return "[object Function]" == o.call(a);
        }

        function e(a) {
            return "string" == typeof a;
        }

        function f() {}

        function g(a) {
            return !a || "loaded" == a || "complete" == a || "uninitialized" == a;
        }

        function h() {
            var a = p.shift();
            q = 1, a ? a.t ? m(function() {
                ("c" == a.t ? B.injectCss : B.injectJs)(a.s, 0, a.a, a.x, a.e, 1);
            }, 0) : (a(), h()) : q = 0;
        }

        function i(a, c, d, e, f, i, j) {
            function k(b) {
                if (!o && g(l.readyState) && (u.r = o = 1, !q && h(), l.onload = l.onreadystatechange = null,
                        b)) {
                    "img" != a && m(function() {
                        t.removeChild(l);
                    }, 50);
                    for (var d in y[c]) y[c].hasOwnProperty(d) && y[c][d].onload();
                }
            }
            var j = j || B.errorTimeout,
                l = b.createElement(a),
                o = 0,
                r = 0,
                u = {
                    t: d,
                    s: c,
                    e: f,
                    a: i,
                    x: j
                };
            1 === y[c] && (r = 1, y[c] = []), "object" == a ? l.data = c : (l.src = c, l.type = a),
                l.width = l.height = "0", l.onerror = l.onload = l.onreadystatechange = function() {
                    k.call(this, r);
                }, p.splice(e, 0, u), "img" != a && (r || 2 === y[c] ? (t.insertBefore(l, s ? null : n),
                    m(k, j)) : y[c].push(l));
        }

        function j(a, b, c, d, f) {
            return q = 0, b = b || "j", e(a) ? i("c" == b ? v : u, a, b, this.i++, c, d, f) : (p.splice(this.i++, 0, a),
                1 == p.length && h()), this;
        }

        function k() {
            var a = B;
            return a.loader = {
                load: j,
                i: 0
            }, a;
        }
        var l = b.documentElement,
            m = a.setTimeout,
            n = b.getElementsByTagName("script")[0],
            o = {}.toString,
            p = [],
            q = 0,
            r = "MozAppearance" in l.style,
            s = r && !!b.createRange().compareNode,
            t = s ? l : n.parentNode,
            l = a.opera && "[object Opera]" == o.call(a.opera),
            l = !!b.attachEvent && !l,
            u = r ? "object" : l ? "script" : "img",
            v = l ? "script" : u,
            w = Array.isArray || function(a) {
                return "[object Array]" == o.call(a);
            },
            x = [],
            y = {},
            z = {
                timeout: function(a, b) {
                    return b.length && (a.timeout = b[0]), a;
                }
            },
            A, B;
        B = function(a) {
            function b(a) {
                var a = a.split("!"),
                    b = x.length,
                    c = a.pop(),
                    d = a.length,
                    c = {
                        url: c,
                        origUrl: c,
                        prefixes: a
                    },
                    e, f, g;
                for (f = 0; f < d; f++) g = a[f].split("="), (e = z[g.shift()]) && (c = e(c, g));
                for (f = 0; f < b; f++) c = x[f](c);
                return c;
            }

            function g(a, e, f, g, h) {
                var i = b(a),
                    j = i.autoCallback;
                i.url.split(".").pop().split("?").shift(), i.bypass || (e && (e = d(e) ? e : e[a] || e[g] || e[a.split("/").pop().split("?")[0]]),
                    i.instead ? i.instead(a, e, f, g, h) : (y[i.url] ? i.noexec = !0 : y[i.url] = 1,
                        f.load(i.url, i.forceCSS || !i.forceJS && "css" == i.url.split(".").pop().split("?").shift() ? "c" : c, i.noexec, i.attrs, i.timeout), (d(e) || d(j)) && f.load(function() {
                            k(), e && e(i.origUrl, h, g), j && j(i.origUrl, h, g), y[i.url] = 2;
                        })));
            }

            function h(a, b) {
                function c(a, c) {
                    if (a) {
                        if (e(a)) c || (j = function() {
                            var a = [].slice.call(arguments);
                            k.apply(this, a), l();
                        }), g(a, j, b, 0, h);
                        else if (Object(a) === a)
                            for (n in m = function() {
                                    var b = 0,
                                        c;
                                    for (c in a) a.hasOwnProperty(c) && b++;
                                    return b;
                                }(), a) a.hasOwnProperty(n) && (!c && !--m && (d(j) ? j = function() {
                                var a = [].slice.call(arguments);
                                k.apply(this, a), l();
                            } : j[n] = function(a) {
                                return function() {
                                    var b = [].slice.call(arguments);
                                    a && a.apply(this, b), l();
                                };
                            }(k[n])), g(a[n], j, b, n, h));
                    } else !c && l();
                }
                var h = !!a.test,
                    i = a.load || a.both,
                    j = a.callback || f,
                    k = j,
                    l = a.complete || f,
                    m, n;
                c(h ? a.yep : a.nope, !!i), i && c(i);
            }
            var i, j, l = this.yepnope.loader;
            if (e(a)) g(a, 0, l, 0);
            else if (w(a))
                for (i = 0; i < a.length; i++) j = a[i],
                    e(j) ? g(j, 0, l, 0) : w(j) ? B(j) : Object(j) === j && h(j, l);
            else Object(a) === a && h(a, l);
        }, B.addPrefix = function(a, b) {
            z[a] = b;
        }, B.addFilter = function(a) {
            x.push(a);
        }, B.errorTimeout = 1e4, null == b.readyState && b.addEventListener && (b.readyState = "loading",
            b.addEventListener("DOMContentLoaded", A = function() {
                b.removeEventListener("DOMContentLoaded", A, 0), b.readyState = "complete";
            }, 0)), a.yepnope = k(), a.yepnope.executeStack = h, a.yepnope.injectJs = function(a, c, d, e, i, j) {
            var k = b.createElement("script"),
                l, o, e = e || B.errorTimeout;
            k.src = a;
            for (o in d) k.setAttribute(o, d[o]);
            c = j ? h : c || f, k.onreadystatechange = k.onload = function() {
                !l && g(k.readyState) && (l = 1, c(), k.onload = k.onreadystatechange = null);
            }, m(function() {
                l || (l = 1, c(1));
            }, e), i ? k.onload() : n.parentNode.insertBefore(k, n);
        }, a.yepnope.injectCss = function(a, c, d, e, g, i) {
            var e = b.createElement("link"),
                j, c = i ? h : c || f;
            e.href = a, e.rel = "stylesheet", e.type = "text/css";
            for (j in d) e.setAttribute(j, d[j]);
            g || (n.parentNode.insertBefore(e, n), m(c, 0));
        };
    }(this, document), Modernizr.load = function() {
        yepnope.apply(window, [].slice.call(arguments, 0));
    };

define("modernizr", function(global) {
    return function() {
        var ret, fn;
        return ret || global.Modernizr;
    };
}(this));

define("jquery", [], function() {
    return jQuery;
});

define("underscore", [], function() {
    return _;
});

if (typeof Object.create !== "function") {
    Object.create = function(o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}

(function() {
    var lastTime = 0;
    var vendors = ["ms", "moz", "webkit", "o"];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + "RequestAnimationFrame"];
        window.cancelAnimationFrame = window[vendors[x] + "CancelAnimationFrame"] || window[vendors[x] + "CancelRequestAnimationFrame"];
    }
    if (!window.requestAnimationFrame) window.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() {
            callback(currTime + timeToCall);
        }, timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };
    if (!window.cancelAnimationFrame) window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    };
})();

(function() {
    if (!window.console) {
        window.console = {};
    }
    var m = ["log", "info", "warn", "error", "debug", "trace", "dir", "group", "groupCollapsed", "groupEnd", "time", "timeEnd", "profile", "profileEnd", "dirxml", "assert", "count", "markTimeline", "timeStamp", "clear"];
    for (var i = 0; i < m.length; i++) {
        if (!window.console[m[i]]) {
            window.console[m[i]] = function() {};
        }
    }
})();

_notify = function() {
    var debug = function() {};
    var components = {};
    var broadcast = function(event, args, source) {
        if (!event) {
            return;
        }
        args = args || [];
        for (var c in components) {
            if (typeof components[c]["on" + event] == "function") {
                try {
                    source = source || components[c];
                    components[c]["on" + event].apply(source, args);
                } catch (err) {
                    debug(["Mediator error.", event, args, source, err].join(" "));
                }
            }
        }
    };
    var addComponent = function(name, component, replaceDuplicate) {
        if (name in components) {
            if (replaceDuplicate) {
                removeComponent(name);
            } else {
                throw new Error("The object: " + name + " has already applied listeners");
            }
        }
        components[name] = component;
    };
    var removeComponent = function(name) {
        if (name in components) {
            delete components[name];
        }
    };
    var getComponent = function(name) {
        return components[name];
    };
    var contains = function(name) {
        return name in components;
    };
    return {
        name: "Mediator",
        broadcast: broadcast,
        add: addComponent,
        rem: removeComponent,
        get: getComponent,
        has: contains
    };
}();

_notify.add("global", function() {
    var tracecount = 0;
    var alertcount = 0;
    return {
        onTrace: function(o) {
            tracecount++;
            var datastring = o.data;
            if (typeof datastring == "object") {
                datastring = JSON.stringify(datastring);
            }
            var buildstring = "[" + String(tracecount) + "] (" + o.senderID + ":" + o.sendertype + ":" + o.notifyscope + ") > " + datastring;
            if (_global$.getQuery("debug")) console.log(buildstring);
        },
        onAlert: function(o) {
            alertcount++;
            var datastring = o.data;
            if (typeof datastring == "object") {
                datastring = JSON.stringify(datastring);
            }
            var buildstring = "[" + String(tracecount) + "] (" + o.senderID + ":" + o.sendertype + ":" + o.notifyscope + ") > " + datastring;
            if (_global$.getQuery("debug")) {
                alert(buildstring);
            }
        },
        onInitialize: function(o) {
            if (_global$.getQuery("debug")) console.log("Created Instance #" + o.senderID + " type:" + o.sendertype + " by " + o.data.author + " [notifyscope:" + o.notifyscope + "]");
        },
        onOrientation: function(o) {
            window._global$.addorientationtohtml(o);
        }
    };
}());

require(["modernizr", "jquery", "underscore"], function(Modernizr, $, _) {
    function Monarch_Base() {
        if (arguments.callee._singletonInstance) return arguments.callee._singletonInstance;
        arguments.callee._singletonInstance = this;
        var parent = this;
        var debouncetimeout = 250;
        var scrolltimeout = undefined;
        this.lastscrollpositionX = 0;
        this.lastscrollpositionY = 0;
        this.orientation = "notset";
        this.windowHeight = undefined;
        this.windowWidth = undefined;
        this.documentHeight = undefined;
        this.documentWidth = undefined;
        this.screenHeight = undefined;
        this.screenWidth = undefined;
        this.isTouch = Modernizr.touch;
        this.host = location.host;
        this.path = location.pathname;
        this.islocalhost = false;
        if (this.host.indexOf("localhost") != -1 || this.host.indexOf("127.0.0.1") != -1) {
            this.islocalhost = true;
            $("html").addClass("localhost");
        }
        this.addorientationtohtml = function(o) {
            if (o.data == "portrait") {
                $("html").addClass("portrait");
                $("html").removeClass("landscape");
            } else {
                $("html").addClass("landscape");
                $("html").removeClass("portrait");
            }
        };
        this.setOrientation = function() {
            var candidate = "portrait";
            this.setWindowHeight();
            this.setWindowWidth();
            if (this.windowWidth > this.windowHeight) {
                candidate = "landscape";
            }
            this._isupdated("orientation", candidate, "Orientation");
        };
        this.setWindowHeight = function() {
            this._isupdated("windowHeight", $(window).height(), "WindowHeight");
        };
        this.setWindowWidth = function() {
            this._isupdated("windowWidth", $(window).width(), "WindowWidth");
        };
        this.setDocumentHeight = function() {
            this._isupdated("documentHeight", $(document).height(), "DocumentHeight");
        };
        this.setDocumentWidth = function() {
            this._isupdated("documentWidth", $(document).width(), "DocumentWidth");
        };
        this.setScreenHeight = function() {
            this._isupdated("screenHeight", screen.height, "ScreenHeight");
        };
        this.setScreenWidth = function() {
            this._isupdated("screenWidth", screen.width, "ScreenWidth");
        };
        this.setLastScrollposition = function() {
            var newXval = $(window).scrollLeft();
            var newYval = $(window).scrollTop();
            if (newYval > parent.lastscrollpositionY) {
                $("html").addClass("scrolldown");
                $("html").removeClass("scrollup");
            }
            if (newYval < parent.lastscrollpositionY) {
                $("html").addClass("scrollup");
                $("html").removeClass("scrolldown");
            }
            if (newXval > parent.lastscrollpositionX) {
                $("html").addClass("scrollright");
                $("html").removeClass("scrollleft");
            }
            if (newXval < parent.lastscrollpositionX) {
                $("html").addClass("scrollleft");
                $("html").removeClass("scrollright");
            }
            this._isupdated("lastscrollpositionX", newXval, "LastXscroll");
            this._isupdated("lastscrollpositionY", newYval, "LastYscroll");
        };
        this.jsonpReturn = function(id, type, notifyscope) {
            return function(data) {
                _notify.broadcast(type, [{
                    senderID: id,
                    sendertype: type,
                    notifyscope: notifyscope,
                    data: data
                }]);
            };
        };
        this._isupdated = function(value, currentVal, notify) {
            if (parent[value] != currentVal) {
                var Oldvalue = parent[value];
                parent[value] = currentVal;
                if (Oldvalue != undefined) {
                    this.notify(notify, parent[value]);
                }
            }
        };
        this.notify = function(type, data) {
            _notify.broadcast(type, [{
                senderID: "global",
                sendertype: "global",
                notifyscope: "global",
                data: data
            }]);
        };
        this.getQuery = function(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    var returncandidate = pair[1];
                    if (returncandidate == "true" || returncandidate == "yes" || returncandidate == "y" || returncandidate == "t") {
                        returncandidate = true;
                    }
                    if (returncandidate == "false" || returncandidate == "no" || returncandidate == "n" || returncandidate == "f") {
                        returncandidate = false;
                    }
                    return returncandidate;
                }
            }
            return false;
        };
        this.getHash = function(variable) {
            return window.location.hash;
        };
        this.setCookie = function(key, value) {
            var expires = new Date();
            expires.setTime(expires.getTime() + 1 * 24 * 60 * 60 * 1e3);
            document.cookie = key + "=" + value + ";expires=" + expires.toUTCString();
        };
        this.getCookie = function(key) {
            var keyValue = document.cookie.match("(^|;) ?" + key + "=([^;]*)(;|$)");
            return keyValue ? keyValue[2] : null;
        };
        $(window).scroll(_.debounce(function(event) {
            parent.setLastScrollposition();
            parent.notify("WindowScrollStart", {
                x: parent.lastscrollpositionX,
                y: parent.lastscrollpositionY
            });
        }, debouncetimeout, true));
        $(window).scroll(_.debounce(function(event) {
            parent.setLastScrollposition();
            parent.notify("WindowScrollEnd", {
                x: parent.lastscrollpositionX,
                y: parent.lastscrollpositionY
            });
        }, debouncetimeout));
        $(window).on("resize", _.debounce(function(event) {
            parent.setOrientation();
        }, debouncetimeout));
    }
    window._global$ = new Monarch_Base();
    window._global$.setOrientation();
    window._global$.setDocumentHeight();
    window._global$.setDocumentWidth();
    window._global$.setScreenHeight();
    window._global$.setScreenWidth();
});

define("libraries/etc/required.js", function() {});

require.config({
    baseUrl: ".",
    paths: {
        underscore: "libraries/underscore0x/underscore",
        jquery: "libraries/jquery0x/jquery",
        modernizr: "libraries/modernizr/modernizr.custom.43687",
        x2js: "libraries/parsing/x2js/xml2json",
        knockout: "libraries/knockout/knockout-2.3.0",
        i18n: "libraries/require/i18n",
        dust: "libraries/dust/dist/dust-full-2.0.0.min",
        tweenmax: "libraries/greensocks/TweenMax.min",
        tweenlite: "libraries/greensocks/TweenLite.min",
        timelinelite: "libraries/greensocks/TimelineLite.min",
        timelinemax: "libraries/greensocks/TimelineMax.min",
        tweenmax_1_11_8: "libraries/greensocks_1.11.8/TweenMax",
        tweenlite_1_11_8: "libraries/greensocks_1.11.8/TweenLite",
        timelinelite_1_11_8: "libraries/greensocks_1.11.8/TimelineLite",
        timelinemax_1_11_8: "libraries/greensocks_1.11.8/TimelineMax",
        draggable_1_11_8: "libraries/greensocks_1.11.8/utils/Draggable.js",
        scrollto: "libraries/greensocks/plugins/ScrollToPlugin.min",
        hammer: "libraries/hammer/jquery.hammer",
        angular: "libraries/angular/angular.min",
        angularresource: "libraries/angular/angular-resource.min"
    },
    shim: {
        underscore: {
            exports: "_"
        },
        modernizr: {
            exports: "Modernizr"
        },
        x2js: {
            exports: "X2JS"
        },
        dust: {
            exports: "dust"
        },
        tweenmax: {
            exports: "TweenMax"
        },
        tweenlite: {
            exports: "TweenLite"
        },
        timelinelite: {
            exports: "TimelineLite"
        },
        timelinemax: {
            exports: "TimelineMax"
        },
        tweenmax_1_11_8: {
            exports: "TweenMax"
        },
        tweenlite_1_11_8: {
            exports: "TweenLite"
        },
        timelinelite_1_11_8: {
            exports: "TimelineLite"
        },
        timelinemax_1_11_8: {
            exports: "TimelineMax"
        },
        hammer: {
            exports: "Hammer"
        },
        scrollto: {
            exports: "Scrollto"
        },
        angular: {
            exports: "angular"
        },
        "angular-resource": {
            deps: ["angular"]
        }
    }
});

define("requireConfig", function() {});
