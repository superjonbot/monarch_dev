/*! Drupal Site JS : footer_global.js */
/*! codebase: Monarch v1.1.5 by Jonathan Robles */
/*! built:12-27-2014 [5:24:02PM] */
/*! -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

/*! Prerequisites: jQuery, Underscore */

/*! Third Party Includes [start] */
/* xxx */
/*! Third Party Includes [end] */
define("jquery", [], function() {
    return jQuery;
});

define("underscore", [], function() {
    return _;
});

define("modules/definitions/standardmodule", ["jquery"], function($) {
    var _instanceID = 0;
    var _nextInstanceID = function() {
        return ++_instanceID;
    };
    var defaults = [];

    function _thizOBJ_(o) {
        this._instanceID = _nextInstanceID();
        var _parent = this;
        defaults[this._instanceID] = {
            type: "Standard Module Definition",
            author: "Jonathan Robles",
            notifyscope: "global",
            target: undefined,
            file: undefined,
            usenocache: true,
            data: undefined,
            callback: undefined,
            interval: undefined,
            init: function() {
                _notify.broadcast("Initialize", [{
                    senderID: _parent._instanceID,
                    sendertype: this.type,
                    notifyscope: this.notifyscope,
                    data: {
                        author: this.author
                    }
                }]);
            },
            parent: this
        };
        defaults[this._instanceID] = $.extend(defaults[this._instanceID], o);
        defaults[this._instanceID].init();
        return this;
    }
    _thizOBJ_.prototype = {
        _init: function() {
            this._var().init();
        },
        _showdata: function() {
            return JSON.stringify(defaults[this._instanceID]);
        },
        _id: function() {
            return this._instanceID;
        },
        _var: function(o) {
            if (o != undefined) {
                defaults[this._instanceID] = $.extend(defaults[this._instanceID], o);
            }
            return defaults[this._instanceID];
        },
        _nocache: function(string) {
            if (typeof string === "string") {
                if (this._var().usenocache) {
                    var addOn = "?";
                    if (string.indexOf("?") != -1) {
                        addOn = "&";
                    }
                    return string + addOn + "nocache=" + Math.floor(Math.random() * 9999);
                } else {
                    return string;
                }
            } else {
                this.notify("Alert", "_nocache needs a string!");
                return;
            }
        },
        notify: function(type, data) {
            _notify.broadcast(type, [{
                senderID: this._id(),
                sendertype: this._var().type,
                notifyscope: this._var().notifyscope,
                data: data
            }]);
        },
        parent: this
    };
    return _thizOBJ_;
});

(function(window, undefined) {
    var Hammer = function(element, options) {
        return new Hammer.Instance(element, options || {});
    };
    Hammer.defaults = {
        stop_browser_behavior: {
            userSelect: "none",
            touchAction: "none",
            touchCallout: "none",
            contentZooming: "none",
            userDrag: "none",
            tapHighlightColor: "rgba(0,0,0,0)"
        }
    };
    Hammer.HAS_POINTEREVENTS = navigator.pointerEnabled || navigator.msPointerEnabled;
    Hammer.HAS_TOUCHEVENTS = "ontouchstart" in window;
    Hammer.MOBILE_REGEX = /mobile|tablet|ip(ad|hone|od)|android/i;
    Hammer.NO_MOUSEEVENTS = Hammer.HAS_TOUCHEVENTS && navigator.userAgent.match(Hammer.MOBILE_REGEX);
    Hammer.EVENT_TYPES = {};
    Hammer.DIRECTION_DOWN = "down";
    Hammer.DIRECTION_LEFT = "left";
    Hammer.DIRECTION_UP = "up";
    Hammer.DIRECTION_RIGHT = "right";
    Hammer.POINTER_MOUSE = "mouse";
    Hammer.POINTER_TOUCH = "touch";
    Hammer.POINTER_PEN = "pen";
    Hammer.EVENT_START = "start";
    Hammer.EVENT_MOVE = "move";
    Hammer.EVENT_END = "end";
    Hammer.DOCUMENT = document;
    Hammer.plugins = {};
    Hammer.READY = false;

    function setup() {
        if (Hammer.READY) {
            return;
        }
        Hammer.event.determineEventTypes();
        for (var name in Hammer.gestures) {
            if (Hammer.gestures.hasOwnProperty(name)) {
                Hammer.detection.register(Hammer.gestures[name]);
            }
        }
        Hammer.event.onTouch(Hammer.DOCUMENT, Hammer.EVENT_MOVE, Hammer.detection.detect);
        Hammer.event.onTouch(Hammer.DOCUMENT, Hammer.EVENT_END, Hammer.detection.detect);
        Hammer.READY = true;
    }
    Hammer.Instance = function(element, options) {
        var self = this;
        setup();
        this.element = element;
        this.enabled = true;
        this.options = Hammer.utils.extend(Hammer.utils.extend({}, Hammer.defaults), options || {});
        if (this.options.stop_browser_behavior) {
            Hammer.utils.stopDefaultBrowserBehavior(this.element, this.options.stop_browser_behavior);
        }
        Hammer.event.onTouch(element, Hammer.EVENT_START, function(ev) {
            if (self.enabled) {
                Hammer.detection.startDetect(self, ev);
            }
        });
        return this;
    };
    Hammer.Instance.prototype = {
        on: function onEvent(gesture, handler) {
            var gestures = gesture.split(" ");
            for (var t = 0; t < gestures.length; t++) {
                this.element.addEventListener(gestures[t], handler, false);
            }
            return this;
        },
        off: function offEvent(gesture, handler) {
            var gestures = gesture.split(" ");
            for (var t = 0; t < gestures.length; t++) {
                this.element.removeEventListener(gestures[t], handler, false);
            }
            return this;
        },
        trigger: function triggerEvent(gesture, eventData) {
            var event = Hammer.DOCUMENT.createEvent("Event");
            event.initEvent(gesture, true, true);
            event.gesture = eventData;
            var element = this.element;
            if (Hammer.utils.hasParent(eventData.target, element)) {
                element = eventData.target;
            }
            element.dispatchEvent(event);
            return this;
        },
        enable: function enable(state) {
            this.enabled = state;
            return this;
        }
    };
    var last_move_event = null;
    var enable_detect = false;
    var touch_triggered = false;
    Hammer.event = {
        bindDom: function(element, type, handler) {
            var types = type.split(" ");
            for (var t = 0; t < types.length; t++) {
                element.addEventListener(types[t], handler, false);
            }
        },
        onTouch: function onTouch(element, eventType, handler) {
            var self = this;
            this.bindDom(element, Hammer.EVENT_TYPES[eventType], function bindDomOnTouch(ev) {
                var sourceEventType = ev.type.toLowerCase();
                if (sourceEventType.match(/mouse/) && touch_triggered) {
                    return;
                } else if (sourceEventType.match(/touch/) || sourceEventType.match(/pointerdown/) || sourceEventType.match(/mouse/) && ev.which === 1) {
                    enable_detect = true;
                }
                if (sourceEventType.match(/touch|pointer/)) {
                    touch_triggered = true;
                }
                var count_touches = 0;
                if (enable_detect) {
                    if (Hammer.HAS_POINTEREVENTS && eventType != Hammer.EVENT_END) {
                        count_touches = Hammer.PointerEvent.updatePointer(eventType, ev);
                    } else if (sourceEventType.match(/touch/)) {
                        count_touches = ev.touches.length;
                    } else if (!touch_triggered) {
                        count_touches = sourceEventType.match(/up/) ? 0 : 1;
                    }
                    if (count_touches > 0 && eventType == Hammer.EVENT_END) {
                        eventType = Hammer.EVENT_MOVE;
                    } else if (!count_touches) {
                        eventType = Hammer.EVENT_END;
                    }
                    if (!count_touches && last_move_event !== null) {
                        ev = last_move_event;
                    } else {
                        last_move_event = ev;
                    }
                    handler.call(Hammer.detection, self.collectEventData(element, eventType, ev));
                    if (Hammer.HAS_POINTEREVENTS && eventType == Hammer.EVENT_END) {
                        count_touches = Hammer.PointerEvent.updatePointer(eventType, ev);
                    }
                }
                if (!count_touches) {
                    last_move_event = null;
                    enable_detect = false;
                    touch_triggered = false;
                    Hammer.PointerEvent.reset();
                }
            });
        },
        determineEventTypes: function determineEventTypes() {
            var types;
            if (Hammer.HAS_POINTEREVENTS) {
                types = Hammer.PointerEvent.getEvents();
            } else if (Hammer.NO_MOUSEEVENTS) {
                types = ["touchstart", "touchmove", "touchend touchcancel"];
            } else {
                types = ["touchstart mousedown", "touchmove mousemove", "touchend touchcancel mouseup"];
            }
            Hammer.EVENT_TYPES[Hammer.EVENT_START] = types[0];
            Hammer.EVENT_TYPES[Hammer.EVENT_MOVE] = types[1];
            Hammer.EVENT_TYPES[Hammer.EVENT_END] = types[2];
        },
        getTouchList: function getTouchList(ev) {
            if (Hammer.HAS_POINTEREVENTS) {
                return Hammer.PointerEvent.getTouchList();
            } else if (ev.touches) {
                return ev.touches;
            } else {
                return [{
                    identifier: 1,
                    pageX: ev.pageX,
                    pageY: ev.pageY,
                    target: ev.target
                }];
            }
        },
        collectEventData: function collectEventData(element, eventType, ev) {
            var touches = this.getTouchList(ev, eventType);
            var pointerType = Hammer.POINTER_TOUCH;
            if (ev.type.match(/mouse/) || Hammer.PointerEvent.matchType(Hammer.POINTER_MOUSE, ev)) {
                pointerType = Hammer.POINTER_MOUSE;
            }
            return {
                center: Hammer.utils.getCenter(touches),
                timeStamp: new Date().getTime(),
                target: ev.target,
                touches: touches,
                eventType: eventType,
                pointerType: pointerType,
                srcEvent: ev,
                preventDefault: function() {
                    if (this.srcEvent.preventManipulation) {
                        this.srcEvent.preventManipulation();
                    }
                    if (this.srcEvent.preventDefault) {
                        this.srcEvent.preventDefault();
                    }
                },
                stopPropagation: function() {
                    this.srcEvent.stopPropagation();
                },
                stopDetect: function() {
                    return Hammer.detection.stopDetect();
                }
            };
        }
    };
    Hammer.PointerEvent = {
        pointers: {},
        getTouchList: function() {
            var self = this;
            var touchlist = [];
            Object.keys(self.pointers).sort().forEach(function(id) {
                touchlist.push(self.pointers[id]);
            });
            return touchlist;
        },
        updatePointer: function(type, pointerEvent) {
            if (type == Hammer.EVENT_END) {
                this.pointers = {};
            } else {
                pointerEvent.identifier = pointerEvent.pointerId;
                this.pointers[pointerEvent.pointerId] = pointerEvent;
            }
            return Object.keys(this.pointers).length;
        },
        matchType: function(pointerType, ev) {
            if (!ev.pointerType) {
                return false;
            }
            var types = {};
            types[Hammer.POINTER_MOUSE] = ev.pointerType == ev.MSPOINTER_TYPE_MOUSE || ev.pointerType == Hammer.POINTER_MOUSE;
            types[Hammer.POINTER_TOUCH] = ev.pointerType == ev.MSPOINTER_TYPE_TOUCH || ev.pointerType == Hammer.POINTER_TOUCH;
            types[Hammer.POINTER_PEN] = ev.pointerType == ev.MSPOINTER_TYPE_PEN || ev.pointerType == Hammer.POINTER_PEN;
            return types[pointerType];
        },
        getEvents: function() {
            return ["pointerdown MSPointerDown", "pointermove MSPointerMove", "pointerup pointercancel MSPointerUp MSPointerCancel"];
        },
        reset: function() {
            this.pointers = {};
        }
    };
    Hammer.utils = {
        extend: function extend(dest, src, merge) {
            for (var key in src) {
                if (dest[key] !== undefined && merge) {
                    continue;
                }
                dest[key] = src[key];
            }
            return dest;
        },
        hasParent: function(node, parent) {
            while (node) {
                if (node == parent) {
                    return true;
                }
                node = node.parentNode;
            }
            return false;
        },
        getCenter: function getCenter(touches) {
            var valuesX = [],
                valuesY = [];
            for (var t = 0, len = touches.length; t < len; t++) {
                valuesX.push(touches[t].pageX);
                valuesY.push(touches[t].pageY);
            }
            return {
                pageX: (Math.min.apply(Math, valuesX) + Math.max.apply(Math, valuesX)) / 2,
                pageY: (Math.min.apply(Math, valuesY) + Math.max.apply(Math, valuesY)) / 2
            };
        },
        getVelocity: function getVelocity(delta_time, delta_x, delta_y) {
            return {
                x: Math.abs(delta_x / delta_time) || 0,
                y: Math.abs(delta_y / delta_time) || 0
            };
        },
        getAngle: function getAngle(touch1, touch2) {
            var y = touch2.pageY - touch1.pageY,
                x = touch2.pageX - touch1.pageX;
            return Math.atan2(y, x) * 180 / Math.PI;
        },
        getDirection: function getDirection(touch1, touch2) {
            var x = Math.abs(touch1.pageX - touch2.pageX),
                y = Math.abs(touch1.pageY - touch2.pageY);
            if (x >= y) {
                return touch1.pageX - touch2.pageX > 0 ? Hammer.DIRECTION_LEFT : Hammer.DIRECTION_RIGHT;
            } else {
                return touch1.pageY - touch2.pageY > 0 ? Hammer.DIRECTION_UP : Hammer.DIRECTION_DOWN;
            }
        },
        getDistance: function getDistance(touch1, touch2) {
            var x = touch2.pageX - touch1.pageX,
                y = touch2.pageY - touch1.pageY;
            return Math.sqrt(x * x + y * y);
        },
        getScale: function getScale(start, end) {
            if (start.length >= 2 && end.length >= 2) {
                return this.getDistance(end[0], end[1]) / this.getDistance(start[0], start[1]);
            }
            return 1;
        },
        getRotation: function getRotation(start, end) {
            if (start.length >= 2 && end.length >= 2) {
                return this.getAngle(end[1], end[0]) - this.getAngle(start[1], start[0]);
            }
            return 0;
        },
        isVertical: function isVertical(direction) {
            return direction == Hammer.DIRECTION_UP || direction == Hammer.DIRECTION_DOWN;
        },
        stopDefaultBrowserBehavior: function stopDefaultBrowserBehavior(element, css_props) {
            var prop, vendors = ["webkit", "khtml", "moz", "ms", "o", ""];
            if (!css_props || !element.style) {
                return;
            }
            for (var i = 0; i < vendors.length; i++) {
                for (var p in css_props) {
                    if (css_props.hasOwnProperty(p)) {
                        prop = p;
                        if (vendors[i]) {
                            prop = vendors[i] + prop.substring(0, 1).toUpperCase() + prop.substring(1);
                        }
                        element.style[prop] = css_props[p];
                    }
                }
            }
            if (css_props.userSelect == "none") {
                element.onselectstart = function() {
                    return false;
                };
            }
        }
    };
    Hammer.detection = {
        gestures: [],
        current: null,
        previous: null,
        stopped: false,
        startDetect: function startDetect(inst, eventData) {
            if (this.current) {
                return;
            }
            this.stopped = false;
            this.current = {
                inst: inst,
                startEvent: Hammer.utils.extend({}, eventData),
                lastEvent: false,
                name: ""
            };
            this.detect(eventData);
        },
        detect: function detect(eventData) {
            if (!this.current || this.stopped) {
                return;
            }
            eventData = this.extendEventData(eventData);
            var inst_options = this.current.inst.options;
            for (var g = 0, len = this.gestures.length; g < len; g++) {
                var gesture = this.gestures[g];
                if (!this.stopped && inst_options[gesture.name] !== false) {
                    if (gesture.handler.call(gesture, eventData, this.current.inst) === false) {
                        this.stopDetect();
                        break;
                    }
                }
            }
            if (this.current) {
                this.current.lastEvent = eventData;
            }
            if (eventData.eventType == Hammer.EVENT_END && !eventData.touches.length - 1) {
                this.stopDetect();
            }
            return eventData;
        },
        stopDetect: function stopDetect() {
            this.previous = Hammer.utils.extend({}, this.current);
            this.current = null;
            this.stopped = true;
        },
        extendEventData: function extendEventData(ev) {
            var startEv = this.current.startEvent;
            if (startEv && (ev.touches.length != startEv.touches.length || ev.touches === startEv.touches)) {
                startEv.touches = [];
                for (var i = 0, len = ev.touches.length; i < len; i++) {
                    startEv.touches.push(Hammer.utils.extend({}, ev.touches[i]));
                }
            }
            var delta_time = ev.timeStamp - startEv.timeStamp,
                delta_x = ev.center.pageX - startEv.center.pageX,
                delta_y = ev.center.pageY - startEv.center.pageY,
                velocity = Hammer.utils.getVelocity(delta_time, delta_x, delta_y);
            Hammer.utils.extend(ev, {
                deltaTime: delta_time,
                deltaX: delta_x,
                deltaY: delta_y,
                velocityX: velocity.x,
                velocityY: velocity.y,
                distance: Hammer.utils.getDistance(startEv.center, ev.center),
                angle: Hammer.utils.getAngle(startEv.center, ev.center),
                direction: Hammer.utils.getDirection(startEv.center, ev.center),
                scale: Hammer.utils.getScale(startEv.touches, ev.touches),
                rotation: Hammer.utils.getRotation(startEv.touches, ev.touches),
                startEvent: startEv
            });
            return ev;
        },
        register: function register(gesture) {
            var options = gesture.defaults || {};
            if (options[gesture.name] === undefined) {
                options[gesture.name] = true;
            }
            Hammer.utils.extend(Hammer.defaults, options, true);
            gesture.index = gesture.index || 1e3;
            this.gestures.push(gesture);
            this.gestures.sort(function(a, b) {
                if (a.index < b.index) {
                    return -1;
                }
                if (a.index > b.index) {
                    return 1;
                }
                return 0;
            });
            return this.gestures;
        }
    };
    Hammer.gestures = Hammer.gestures || {};
    Hammer.gestures.Hold = {
        name: "hold",
        index: 10,
        defaults: {
            hold_timeout: 500,
            hold_threshold: 1
        },
        timer: null,
        handler: function holdGesture(ev, inst) {
            switch (ev.eventType) {
                case Hammer.EVENT_START:
                    clearTimeout(this.timer);
                    Hammer.detection.current.name = this.name;
                    this.timer = setTimeout(function() {
                        if (Hammer.detection.current.name == "hold") {
                            inst.trigger("hold", ev);
                        }
                    }, inst.options.hold_timeout);
                    break;

                case Hammer.EVENT_MOVE:
                    if (ev.distance > inst.options.hold_threshold) {
                        clearTimeout(this.timer);
                    }
                    break;

                case Hammer.EVENT_END:
                    clearTimeout(this.timer);
                    break;
            }
        }
    };
    Hammer.gestures.Tap = {
        name: "tap",
        index: 100,
        defaults: {
            tap_max_touchtime: 250,
            tap_max_distance: 10,
            tap_always: true,
            doubletap_distance: 20,
            doubletap_interval: 300
        },
        handler: function tapGesture(ev, inst) {
            if (ev.eventType == Hammer.EVENT_END) {
                var prev = Hammer.detection.previous,
                    did_doubletap = false;
                if (ev.deltaTime > inst.options.tap_max_touchtime || ev.distance > inst.options.tap_max_distance) {
                    return;
                }
                if (prev && prev.name == "tap" && ev.timeStamp - prev.lastEvent.timeStamp < inst.options.doubletap_interval && ev.distance < inst.options.doubletap_distance) {
                    inst.trigger("doubletap", ev);
                    did_doubletap = true;
                }
                if (!did_doubletap || inst.options.tap_always) {
                    Hammer.detection.current.name = "tap";
                    inst.trigger(Hammer.detection.current.name, ev);
                }
            }
        }
    };
    Hammer.gestures.Swipe = {
        name: "swipe",
        index: 40,
        defaults: {
            swipe_max_touches: 1,
            swipe_velocity: .7
        },
        handler: function swipeGesture(ev, inst) {
            if (ev.eventType == Hammer.EVENT_END) {
                if (inst.options.swipe_max_touches > 0 && ev.touches.length > inst.options.swipe_max_touches) {
                    return;
                }
                if (ev.velocityX > inst.options.swipe_velocity || ev.velocityY > inst.options.swipe_velocity) {
                    inst.trigger(this.name, ev);
                    inst.trigger(this.name + ev.direction, ev);
                }
            }
        }
    };
    Hammer.gestures.Drag = {
        name: "drag",
        index: 50,
        defaults: {
            drag_min_distance: 10,
            drag_max_touches: 1,
            drag_block_horizontal: false,
            drag_block_vertical: false,
            drag_lock_to_axis: false,
            drag_lock_min_distance: 25
        },
        triggered: false,
        handler: function dragGesture(ev, inst) {
            if (Hammer.detection.current.name != this.name && this.triggered) {
                inst.trigger(this.name + "end", ev);
                this.triggered = false;
                return;
            }
            if (inst.options.drag_max_touches > 0 && ev.touches.length > inst.options.drag_max_touches) {
                return;
            }
            switch (ev.eventType) {
                case Hammer.EVENT_START:
                    this.triggered = false;
                    break;

                case Hammer.EVENT_MOVE:
                    if (ev.distance < inst.options.drag_min_distance && Hammer.detection.current.name != this.name) {
                        return;
                    }
                    Hammer.detection.current.name = this.name;
                    if (Hammer.detection.current.lastEvent.drag_locked_to_axis || inst.options.drag_lock_to_axis && inst.options.drag_lock_min_distance <= ev.distance) {
                        ev.drag_locked_to_axis = true;
                    }
                    var last_direction = Hammer.detection.current.lastEvent.direction;
                    if (ev.drag_locked_to_axis && last_direction !== ev.direction) {
                        if (Hammer.utils.isVertical(last_direction)) {
                            ev.direction = ev.deltaY < 0 ? Hammer.DIRECTION_UP : Hammer.DIRECTION_DOWN;
                        } else {
                            ev.direction = ev.deltaX < 0 ? Hammer.DIRECTION_LEFT : Hammer.DIRECTION_RIGHT;
                        }
                    }
                    if (!this.triggered) {
                        inst.trigger(this.name + "start", ev);
                        this.triggered = true;
                    }
                    inst.trigger(this.name, ev);
                    inst.trigger(this.name + ev.direction, ev);
                    if (inst.options.drag_block_vertical && Hammer.utils.isVertical(ev.direction) || inst.options.drag_block_horizontal && !Hammer.utils.isVertical(ev.direction)) {
                        ev.preventDefault();
                    }
                    break;

                case Hammer.EVENT_END:
                    if (this.triggered) {
                        inst.trigger(this.name + "end", ev);
                    }
                    this.triggered = false;
                    break;
            }
        }
    };
    Hammer.gestures.Transform = {
        name: "transform",
        index: 45,
        defaults: {
            transform_min_scale: .01,
            transform_min_rotation: 1,
            transform_always_block: false
        },
        triggered: false,
        handler: function transformGesture(ev, inst) {
            if (Hammer.detection.current.name != this.name && this.triggered) {
                inst.trigger(this.name + "end", ev);
                this.triggered = false;
                return;
            }
            if (ev.touches.length < 2) {
                return;
            }
            if (inst.options.transform_always_block) {
                ev.preventDefault();
            }
            switch (ev.eventType) {
                case Hammer.EVENT_START:
                    this.triggered = false;
                    break;

                case Hammer.EVENT_MOVE:
                    var scale_threshold = Math.abs(1 - ev.scale);
                    var rotation_threshold = Math.abs(ev.rotation);
                    if (scale_threshold < inst.options.transform_min_scale && rotation_threshold < inst.options.transform_min_rotation) {
                        return;
                    }
                    Hammer.detection.current.name = this.name;
                    if (!this.triggered) {
                        inst.trigger(this.name + "start", ev);
                        this.triggered = true;
                    }
                    inst.trigger(this.name, ev);
                    if (rotation_threshold > inst.options.transform_min_rotation) {
                        inst.trigger("rotate", ev);
                    }
                    if (scale_threshold > inst.options.transform_min_scale) {
                        inst.trigger("pinch", ev);
                        inst.trigger("pinch" + (ev.scale < 1 ? "in" : "out"), ev);
                    }
                    break;

                case Hammer.EVENT_END:
                    if (this.triggered) {
                        inst.trigger(this.name + "end", ev);
                    }
                    this.triggered = false;
                    break;
            }
        }
    };
    Hammer.gestures.Touch = {
        name: "touch",
        index: -Infinity,
        defaults: {
            prevent_default: false,
            prevent_mouseevents: false
        },
        handler: function touchGesture(ev, inst) {
            if (inst.options.prevent_mouseevents && ev.pointerType == Hammer.POINTER_MOUSE) {
                ev.stopDetect();
                return;
            }
            if (inst.options.prevent_default) {
                ev.preventDefault();
            }
            if (ev.eventType == Hammer.EVENT_START) {
                inst.trigger(this.name, ev);
            }
        }
    };
    Hammer.gestures.Release = {
        name: "release",
        index: Infinity,
        handler: function releaseGesture(ev, inst) {
            if (ev.eventType == Hammer.EVENT_END) {
                inst.trigger(this.name, ev);
            }
        }
    };
    if (typeof module === "object" && typeof module.exports === "object") {
        module.exports = Hammer;
    } else {
        window.Hammer = Hammer;
        if (typeof window.define === "function" && window.define.amd) {
            window.define("hammer", [], function() {
                return Hammer;
            });
        }
    }
})(this);

(function($, undefined) {
    if ($ === undefined) {
        return;
    }
    Hammer.event.bindDom = function(element, eventTypes, handler) {
        $(element).on(eventTypes, function(ev) {
            var data = ev.originalEvent || ev;
            if (data.pageX === undefined) {
                data.pageX = ev.pageX;
                data.pageY = ev.pageY;
            }
            if (!data.target) {
                data.target = ev.target;
            }
            if (data.which === undefined) {
                data.which = data.button;
            }
            if (!data.preventDefault) {
                data.preventDefault = ev.preventDefault;
            }
            if (!data.stopPropagation) {
                data.stopPropagation = ev.stopPropagation;
            }
            handler.call(this, data);
        });
    };
    Hammer.Instance.prototype.on = function(types, handler) {
        return $(this.element).on(types, handler);
    };
    Hammer.Instance.prototype.off = function(types, handler) {
        return $(this.element).off(types, handler);
    };
    Hammer.Instance.prototype.trigger = function(gesture, eventData) {
        var el = $(this.element);
        if (el.has(eventData.target).length) {
            el = $(eventData.target);
        }
        return el.trigger({
            type: gesture,
            gesture: eventData
        });
    };
    $.fn.hammer = function(options) {
        return this.each(function() {
            var el = $(this);
            var inst = el.data("hammer");
            if (!inst) {
                el.data("hammer", new Hammer(this, options || {}));
            } else if (inst && options) {
                Hammer.utils.extend(inst.options, options);
            }
        });
    };
})(window.jQuery || window.Zepto);

define("hammer", function(global) {
    return function() {
        var ret, fn;
        return ret || global.Hammer;
    };
}(this));

(function(t) {
    var e, i, s, n, r, a = t.GreenSockGlobals || t,
        o = function(t) {
            var e, i = t.split("."),
                s = a;
            for (e = 0; i.length > e; e++) s[i[e]] = s = s[i[e]] || {};
            return s;
        },
        l = o("com.greensock"),
        _ = [].slice,
        h = function() {},
        u = {},
        m = function(e, i, s, n) {
            this.sc = u[e] ? u[e].sc : [], u[e] = this, this.gsClass = null, this.func = s;
            var r = [];
            this.check = function(l) {
                for (var _, h, f, p, c = i.length, d = c; --c > -1;)(_ = u[i[c]] || new m(i[c], [])).gsClass ? (r[c] = _.gsClass,
                    d--) : l && _.sc.push(this);
                if (0 === d && s)
                    for (h = ("com.greensock." + e).split("."), f = h.pop(), p = o(h.join("."))[f] = this.gsClass = s.apply(s, r),
                        n && (a[f] = p, "function" == typeof define && define.amd ? define((t.GreenSockAMDPath ? t.GreenSockAMDPath + "/" : "") + e.split(".").join("/"), [], function() {
                            return p;
                        }) : "undefined" != typeof module && module.exports && (module.exports = p)), c = 0; this.sc.length > c; c++) this.sc[c].check();
            }, this.check(!0);
        },
        f = t._gsDefine = function(t, e, i, s) {
            return new m(t, e, i, s);
        },
        p = l._class = function(t, e, i) {
            return e = e || function() {}, f(t, [], function() {
                return e;
            }, i), e;
        };
    f.globals = a;
    var c = [0, 0, 1, 1],
        d = [],
        v = p("easing.Ease", function(t, e, i, s) {
            this._func = t, this._type = i || 0, this._power = s || 0, this._params = e ? c.concat(e) : c;
        }, !0),
        g = v.map = {},
        T = v.register = function(t, e, i, s) {
            for (var n, r, a, o, _ = e.split(","), h = _.length, u = (i || "easeIn,easeOut,easeInOut").split(","); --h > -1;)
                for (r = _[h],
                    n = s ? p("easing." + r, null, !0) : l.easing[r] || {}, a = u.length; --a > -1;) o = u[a],
                    g[r + "." + o] = g[o + r] = n[o] = t.getRatio ? t : t[o] || new t();
        };
    for (s = v.prototype, s._calcEnd = !1, s.getRatio = function(t) {
            if (this._func) return this._params[0] = t, this._func.apply(null, this._params);
            var e = this._type,
                i = this._power,
                s = 1 === e ? 1 - t : 2 === e ? t : .5 > t ? 2 * t : 2 * (1 - t);
            return 1 === i ? s *= s : 2 === i ? s *= s * s : 3 === i ? s *= s * s * s : 4 === i && (s *= s * s * s * s),
                1 === e ? 1 - s : 2 === e ? s : .5 > t ? s / 2 : 1 - s / 2;
        }, e = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"], i = e.length; --i > -1;) s = e[i] + ",Power" + i,
        T(new v(null, null, 1, i), s, "easeOut", !0), T(new v(null, null, 2, i), s, "easeIn" + (0 === i ? ",easeNone" : "")),
        T(new v(null, null, 3, i), s, "easeInOut");
    g.linear = l.easing.Linear.easeIn, g.swing = l.easing.Quad.easeInOut;
    var w = p("events.EventDispatcher", function(t) {
        this._listeners = {}, this._eventTarget = t || this;
    });
    s = w.prototype, s.addEventListener = function(t, e, i, s, a) {
        a = a || 0;
        var o, l, _ = this._listeners[t],
            h = 0;
        for (null == _ && (this._listeners[t] = _ = []), l = _.length; --l > -1;) o = _[l],
            o.c === e && o.s === i ? _.splice(l, 1) : 0 === h && a > o.pr && (h = l + 1);
        _.splice(h, 0, {
            c: e,
            s: i,
            up: s,
            pr: a
        }), this !== n || r || n.wake();
    }, s.removeEventListener = function(t, e) {
        var i, s = this._listeners[t];
        if (s)
            for (i = s.length; --i > -1;)
                if (s[i].c === e) return s.splice(i, 1), void 0;
    }, s.dispatchEvent = function(t) {
        var e, i, s, n = this._listeners[t];
        if (n)
            for (e = n.length, i = this._eventTarget; --e > -1;) s = n[e], s.up ? s.c.call(s.s || i, {
                type: t,
                target: i
            }) : s.c.call(s.s || i);
    };
    var P = t.requestAnimationFrame,
        y = t.cancelAnimationFrame,
        k = Date.now || function() {
            return new Date().getTime();
        },
        b = k();
    for (e = ["ms", "moz", "webkit", "o"], i = e.length; --i > -1 && !P;) P = t[e[i] + "RequestAnimationFrame"],
        y = t[e[i] + "CancelAnimationFrame"] || t[e[i] + "CancelRequestAnimationFrame"];
    p("Ticker", function(t, e) {
        var i, s, a, o, l, _ = this,
            u = k(),
            m = e !== !1 && P,
            f = function(t) {
                b = k(), _.time = (b - u) / 1e3;
                var e, n = _.time - l;
                (!i || n > 0 || t === !0) && (_.frame++, l += n + (n >= o ? .004 : o - n), e = !0),
                t !== !0 && (a = s(f)), e && _.dispatchEvent("tick");
            };
        w.call(_), _.time = _.frame = 0, _.tick = function() {
            f(!0);
        }, _.sleep = function() {
            null != a && (m && y ? y(a) : clearTimeout(a), s = h, a = null, _ === n && (r = !1));
        }, _.wake = function() {
            null !== a && _.sleep(), s = 0 === i ? h : m && P ? P : function(t) {
                return setTimeout(t, 0 | 1e3 * (l - _.time) + 1);
            }, _ === n && (r = !0), f(2);
        }, _.fps = function(t) {
            return arguments.length ? (i = t, o = 1 / (i || 60), l = this.time + o, _.wake(),
                void 0) : i;
        }, _.useRAF = function(t) {
            return arguments.length ? (_.sleep(), m = t, _.fps(i), void 0) : m;
        }, _.fps(t), setTimeout(function() {
            m && (!a || 5 > _.frame) && _.useRAF(!1);
        }, 1500);
    }), s = l.Ticker.prototype = new l.events.EventDispatcher(), s.constructor = l.Ticker;
    var A = p("core.Animation", function(t, e) {
        if (this.vars = e = e || {}, this._duration = this._totalDuration = t || 0, this._delay = Number(e.delay) || 0,
            this._timeScale = 1, this._active = e.immediateRender === !0, this.data = e.data,
            this._reversed = e.reversed === !0, j) {
            r || n.wake();
            var i = this.vars.useFrames ? F : j;
            i.add(this, i._time), this.vars.paused && this.paused(!0);
        }
    });
    n = A.ticker = new l.Ticker(), s = A.prototype, s._dirty = s._gc = s._initted = s._paused = !1,
        s._totalTime = s._time = 0, s._rawPrevTime = -1, s._next = s._last = s._onUpdate = s._timeline = s.timeline = null,
        s._paused = !1;
    var S = function() {
        k() - b > 2e3 && n.wake(), setTimeout(S, 2e3);
    };
    S(), s.play = function(t, e) {
        return arguments.length && this.seek(t, e), this.reversed(!1).paused(!1);
    }, s.pause = function(t, e) {
        return arguments.length && this.seek(t, e), this.paused(!0);
    }, s.resume = function(t, e) {
        return arguments.length && this.seek(t, e), this.paused(!1);
    }, s.seek = function(t, e) {
        return this.totalTime(Number(t), e !== !1);
    }, s.restart = function(t, e) {
        return this.reversed(!1).paused(!1).totalTime(t ? -this._delay : 0, e !== !1, !0);
    }, s.reverse = function(t, e) {
        return arguments.length && this.seek(t || this.totalDuration(), e), this.reversed(!0).paused(!1);
    }, s.render = function() {}, s.invalidate = function() {
        return this;
    }, s._enabled = function(t, e) {
        return r || n.wake(), this._gc = !t, this._active = t && !this._paused && this._totalTime > 0 && this._totalTime < this._totalDuration,
            e !== !0 && (t && !this.timeline ? this._timeline.add(this, this._startTime - this._delay) : !t && this.timeline && this._timeline._remove(this, !0)), !1;
    }, s._kill = function() {
        return this._enabled(!1, !1);
    }, s.kill = function(t, e) {
        return this._kill(t, e), this;
    }, s._uncache = function(t) {
        for (var e = t ? this : this.timeline; e;) e._dirty = !0, e = e.timeline;
        return this;
    }, s._swapSelfInParams = function(t) {
        for (var e = t.length, i = t.concat(); --e > -1;) "{self}" === t[e] && (i[e] = this);
        return i;
    }, s.eventCallback = function(t, e, i, s) {
        if ("on" === (t || "").substr(0, 2)) {
            var n = this.vars;
            if (1 === arguments.length) return n[t];
            null == e ? delete n[t] : (n[t] = e, n[t + "Params"] = i instanceof Array && -1 !== i.join("").indexOf("{self}") ? this._swapSelfInParams(i) : i,
                n[t + "Scope"] = s), "onUpdate" === t && (this._onUpdate = e);
        }
        return this;
    }, s.delay = function(t) {
        return arguments.length ? (this._timeline.smoothChildTiming && this.startTime(this._startTime + t - this._delay),
            this._delay = t, this) : this._delay;
    }, s.duration = function(t) {
        return arguments.length ? (this._duration = this._totalDuration = t, this._uncache(!0),
            this._timeline.smoothChildTiming && this._time > 0 && this._time < this._duration && 0 !== t && this.totalTime(this._totalTime * (t / this._duration), !0),
            this) : (this._dirty = !1, this._duration);
    }, s.totalDuration = function(t) {
        return this._dirty = !1, arguments.length ? this.duration(t) : this._totalDuration;
    }, s.time = function(t, e) {
        return arguments.length ? (this._dirty && this.totalDuration(), this.totalTime(t > this._duration ? this._duration : t, e)) : this._time;
    }, s.totalTime = function(t, e, i) {
        if (r || n.wake(), !arguments.length) return this._totalTime;
        if (this._timeline) {
            if (0 > t && !i && (t += this.totalDuration()), this._timeline.smoothChildTiming) {
                this._dirty && this.totalDuration();
                var s = this._totalDuration,
                    a = this._timeline;
                if (t > s && !i && (t = s), this._startTime = (this._paused ? this._pauseTime : a._time) - (this._reversed ? s - t : t) / this._timeScale,
                    a._dirty || this._uncache(!1), a._timeline)
                    for (; a._timeline;) a._timeline._time !== (a._startTime + a._totalTime) / a._timeScale && a.totalTime(a._totalTime, !0),
                        a = a._timeline;
            }
            this._gc && this._enabled(!0, !1), this._totalTime !== t && this.render(t, e, !1);
        }
        return this;
    }, s.startTime = function(t) {
        return arguments.length ? (t !== this._startTime && (this._startTime = t, this.timeline && this.timeline._sortChildren && this.timeline.add(this, t - this._delay)),
            this) : this._startTime;
    }, s.timeScale = function(t) {
        if (!arguments.length) return this._timeScale;
        if (t = t || 1e-6, this._timeline && this._timeline.smoothChildTiming) {
            var e = this._pauseTime,
                i = e || 0 === e ? e : this._timeline.totalTime();
            this._startTime = i - (i - this._startTime) * this._timeScale / t;
        }
        return this._timeScale = t, this._uncache(!1);
    }, s.reversed = function(t) {
        return arguments.length ? (t != this._reversed && (this._reversed = t, this.totalTime(this._totalTime, !0)),
            this) : this._reversed;
    }, s.paused = function(t) {
        if (!arguments.length) return this._paused;
        if (t != this._paused && this._timeline) {
            r || t || n.wake();
            var e = this._timeline,
                i = e.rawTime(),
                s = i - this._pauseTime;
            !t && e.smoothChildTiming && (this._startTime += s, this._uncache(!1)), this._pauseTime = t ? i : null,
                this._paused = t, this._active = !t && this._totalTime > 0 && this._totalTime < this._totalDuration,
                t || 0 === s || 0 === this._duration || this.render(e.smoothChildTiming ? this._totalTime : (i - this._startTime) / this._timeScale, !0, !0);
        }
        return this._gc && !t && this._enabled(!0, !1), this;
    };
    var x = p("core.SimpleTimeline", function(t) {
        A.call(this, 0, t), this.autoRemoveChildren = this.smoothChildTiming = !0;
    });
    s = x.prototype = new A(), s.constructor = x, s.kill()._gc = !1, s._first = s._last = null,
        s._sortChildren = !1, s.add = s.insert = function(t, e) {
            var i, s;
            if (t._startTime = Number(e || 0) + t._delay, t._paused && this !== t._timeline && (t._pauseTime = t._startTime + (this.rawTime() - t._startTime) / t._timeScale),
                t.timeline && t.timeline._remove(t, !0), t.timeline = t._timeline = this, t._gc && t._enabled(!0, !0),
                i = this._last, this._sortChildren)
                for (s = t._startTime; i && i._startTime > s;) i = i._prev;
            return i ? (t._next = i._next, i._next = t) : (t._next = this._first, this._first = t),
                t._next ? t._next._prev = t : this._last = t, t._prev = i, this._timeline && this._uncache(!0),
                this;
        }, s._remove = function(t, e) {
            return t.timeline === this && (e || t._enabled(!1, !0), t.timeline = null, t._prev ? t._prev._next = t._next : this._first === t && (this._first = t._next),
                t._next ? t._next._prev = t._prev : this._last === t && (this._last = t._prev),
                this._timeline && this._uncache(!0)), this;
        }, s.render = function(t, e, i) {
            var s, n = this._first;
            for (this._totalTime = this._time = this._rawPrevTime = t; n;) s = n._next, (n._active || t >= n._startTime && !n._paused) && (n._reversed ? n.render((n._dirty ? n.totalDuration() : n._totalDuration) - (t - n._startTime) * n._timeScale, e, i) : n.render((t - n._startTime) * n._timeScale, e, i)),
                n = s;
        }, s.rawTime = function() {
            return r || n.wake(), this._totalTime;
        };
    var C = p("TweenLite", function(e, i, s) {
            if (A.call(this, i, s), this.render = C.prototype.render, null == e) throw "Cannot tween a null target.";
            this.target = e = "string" != typeof e ? e : C.selector(e) || e;
            var n, r, a, o = e.jquery || e.length && e !== t && e[0] && (e[0] === t || e[0].nodeType && e[0].style && !e.nodeType),
                l = this.vars.overwrite;
            if (this._overwrite = l = null == l ? L[C.defaultOverwrite] : "number" == typeof l ? l >> 0 : L[l], (o || e instanceof Array) && "number" != typeof e[0])
                for (this._targets = a = _.call(e, 0),
                    this._propLookup = [], this._siblings = [], n = 0; a.length > n; n++) r = a[n],
                    r ? "string" != typeof r ? r.length && r !== t && r[0] && (r[0] === t || r[0].nodeType && r[0].style && !r.nodeType) ? (a.splice(n--, 1),
                        this._targets = a = a.concat(_.call(r, 0))) : (this._siblings[n] = G(r, this, !1),
                        1 === l && this._siblings[n].length > 1 && Q(r, this, null, 1, this._siblings[n])) : (r = a[n--] = C.selector(r),
                        "string" == typeof r && a.splice(n + 1, 1)) : a.splice(n--, 1);
            else this._propLookup = {},
                this._siblings = G(e, this, !1), 1 === l && this._siblings.length > 1 && Q(e, this, null, 1, this._siblings);
            (this.vars.immediateRender || 0 === i && 0 === this._delay && this.vars.immediateRender !== !1) && this.render(-this._delay, !1, !0);
        }, !0),
        R = function(e) {
            return e.length && e !== t && e[0] && (e[0] === t || e[0].nodeType && e[0].style && !e.nodeType);
        },
        D = function(t, e) {
            var i, s = {};
            for (i in t) U[i] || i in e && "x" !== i && "y" !== i && "width" !== i && "height" !== i && "className" !== i && "border" !== i || !(!I[i] || I[i] && I[i]._autoCSS) || (s[i] = t[i],
                delete t[i]);
            t.css = s;
        };
    s = C.prototype = new A(), s.constructor = C, s.kill()._gc = !1, s.ratio = 0, s._firstPT = s._targets = s._overwrittenProps = s._startAt = null,
        s._notifyPluginsOfEnabled = !1, C.version = "1.10.3", C.defaultEase = s._ease = new v(null, null, 1, 1),
        C.defaultOverwrite = "auto", C.ticker = n, C.autoSleep = !0, C.selector = t.$ || t.jQuery || function(e) {
            return t.$ ? (C.selector = t.$, t.$(e)) : t.document ? t.document.getElementById("#" === e.charAt(0) ? e.substr(1) : e) : e;
        };
    var E = C._internals = {},
        I = C._plugins = {},
        O = C._tweenLookup = {},
        N = 0,
        U = E.reservedProps = {
            ease: 1,
            delay: 1,
            overwrite: 1,
            onComplete: 1,
            onCompleteParams: 1,
            onCompleteScope: 1,
            useFrames: 1,
            runBackwards: 1,
            startAt: 1,
            onUpdate: 1,
            onUpdateParams: 1,
            onUpdateScope: 1,
            onStart: 1,
            onStartParams: 1,
            onStartScope: 1,
            onReverseComplete: 1,
            onReverseCompleteParams: 1,
            onReverseCompleteScope: 1,
            onRepeat: 1,
            onRepeatParams: 1,
            onRepeatScope: 1,
            easeParams: 1,
            yoyo: 1,
            immediateRender: 1,
            repeat: 1,
            repeatDelay: 1,
            data: 1,
            paused: 1,
            reversed: 1,
            autoCSS: 1
        },
        L = {
            none: 0,
            all: 1,
            auto: 2,
            concurrent: 3,
            allOnStart: 4,
            preexisting: 5,
            "true": 1,
            "false": 0
        },
        F = A._rootFramesTimeline = new x(),
        j = A._rootTimeline = new x();
    j._startTime = n.time, F._startTime = n.frame, j._active = F._active = !0, A._updateRoot = function() {
        if (j.render((n.time - j._startTime) * j._timeScale, !1, !1), F.render((n.frame - F._startTime) * F._timeScale, !1, !1), !(n.frame % 120)) {
            var t, e, i;
            for (i in O) {
                for (e = O[i].tweens, t = e.length; --t > -1;) e[t]._gc && e.splice(t, 1);
                0 === e.length && delete O[i];
            }
            if (i = j._first, (!i || i._paused) && C.autoSleep && !F._first && 1 === n._listeners.tick.length) {
                for (; i && i._paused;) i = i._next;
                i || n.sleep();
            }
        }
    }, n.addEventListener("tick", A._updateRoot);
    var G = function(t, e, i) {
            var s, n, r = t._gsTweenID;
            if (O[r || (t._gsTweenID = r = "t" + N++)] || (O[r] = {
                    target: t,
                    tweens: []
                }), e && (s = O[r].tweens, s[n = s.length] = e, i))
                for (; --n > -1;) s[n] === e && s.splice(n, 1);
            return O[r].tweens;
        },
        Q = function(t, e, i, s, n) {
            var r, a, o, l;
            if (1 === s || s >= 4) {
                for (l = n.length, r = 0; l > r; r++)
                    if ((o = n[r]) !== e) o._gc || o._enabled(!1, !1) && (a = !0);
                    else if (5 === s) break;
                return a;
            }
            var _, h = e._startTime + 1e-10,
                u = [],
                m = 0,
                f = 0 === e._duration;
            for (r = n.length; --r > -1;)(o = n[r]) === e || o._gc || o._paused || (o._timeline !== e._timeline ? (_ = _ || B(e, 0, f),
                0 === B(o, _, f) && (u[m++] = o)) : h >= o._startTime && o._startTime + o.totalDuration() / o._timeScale + 1e-10 > h && ((f || !o._initted) && 2e-10 >= h - o._startTime || (u[m++] = o)));
            for (r = m; --r > -1;) o = u[r], 2 === s && o._kill(i, t) && (a = !0), (2 !== s || !o._firstPT && o._initted) && o._enabled(!1, !1) && (a = !0);
            return a;
        },
        B = function(t, e, i) {
            for (var s = t._timeline, n = s._timeScale, r = t._startTime, a = 1e-10; s._timeline;) {
                if (r += s._startTime, n *= s._timeScale, s._paused) return -100;
                s = s._timeline;
            }
            return r /= n, r > e ? r - e : i && r === e || !t._initted && 2 * a > r - e ? a : (r += t.totalDuration() / t._timeScale / n) > e + a ? 0 : r - e - a;
        };
    s._init = function() {
        var t, e, i, s, n = this.vars,
            r = this._overwrittenProps,
            a = this._duration,
            o = n.immediateRender,
            l = n.ease;
        if (n.startAt) {
            if (this._startAt && this._startAt.render(-1, !0), n.startAt.overwrite = 0, n.startAt.immediateRender = !0,
                this._startAt = C.to(this.target, 0, n.startAt), o)
                if (this._time > 0) this._startAt = null;
                else if (0 !== a) return;
        } else if (n.runBackwards && n.immediateRender && 0 !== a)
            if (this._startAt) this._startAt.render(-1, !0),
                this._startAt = null;
            else if (0 === this._time) {
            i = {};
            for (s in n) U[s] && "autoCSS" !== s || (i[s] = n[s]);
            return i.overwrite = 0, this._startAt = C.to(this.target, 0, i), void 0;
        }
        if (this._ease = l ? l instanceof v ? n.easeParams instanceof Array ? l.config.apply(l, n.easeParams) : l : "function" == typeof l ? new v(l, n.easeParams) : g[l] || C.defaultEase : C.defaultEase,
            this._easeType = this._ease._type, this._easePower = this._ease._power, this._firstPT = null,
            this._targets)
            for (t = this._targets.length; --t > -1;) this._initProps(this._targets[t], this._propLookup[t] = {}, this._siblings[t], r ? r[t] : null) && (e = !0);
        else e = this._initProps(this.target, this._propLookup, this._siblings, r);
        if (e && C._onPluginEvent("_onInitAllProps", this), r && (this._firstPT || "function" != typeof this.target && this._enabled(!1, !1)),
            n.runBackwards)
            for (i = this._firstPT; i;) i.s += i.c, i.c = -i.c, i = i._next;
        this._onUpdate = n.onUpdate, this._initted = !0;
    }, s._initProps = function(e, i, s, n) {
        var r, a, o, l, _, h;
        if (null == e) return !1;
        this.vars.css || e.style && e !== t && e.nodeType && I.css && this.vars.autoCSS !== !1 && D(this.vars, e);
        for (r in this.vars) {
            if (h = this.vars[r], U[r]) h instanceof Array && -1 !== h.join("").indexOf("{self}") && (this.vars[r] = h = this._swapSelfInParams(h, this));
            else if (I[r] && (l = new I[r]())._onInitTween(e, this.vars[r], this)) {
                for (this._firstPT = _ = {
                        _next: this._firstPT,
                        t: l,
                        p: "setRatio",
                        s: 0,
                        c: 1,
                        f: !0,
                        n: r,
                        pg: !0,
                        pr: l._priority
                    }, a = l._overwriteProps.length; --a > -1;) i[l._overwriteProps[a]] = this._firstPT;
                (l._priority || l._onInitAllProps) && (o = !0), (l._onDisable || l._onEnable) && (this._notifyPluginsOfEnabled = !0);
            } else this._firstPT = i[r] = _ = {
                    _next: this._firstPT,
                    t: e,
                    p: r,
                    f: "function" == typeof e[r],
                    n: r,
                    pg: !1,
                    pr: 0
                }, _.s = _.f ? e[r.indexOf("set") || "function" != typeof e["get" + r.substr(3)] ? r : "get" + r.substr(3)]() : parseFloat(e[r]),
                _.c = "string" == typeof h && "=" === h.charAt(1) ? parseInt(h.charAt(0) + "1", 10) * Number(h.substr(2)) : Number(h) - _.s || 0;
            _ && _._next && (_._next._prev = _);
        }
        return n && this._kill(n, e) ? this._initProps(e, i, s, n) : this._overwrite > 1 && this._firstPT && s.length > 1 && Q(e, this, i, this._overwrite, s) ? (this._kill(i, e),
            this._initProps(e, i, s, n)) : o;
    }, s.render = function(t, e, i) {
        var s, n, r, a = this._time;
        if (t >= this._duration) this._totalTime = this._time = this._duration, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1,
            this._reversed || (s = !0, n = "onComplete"), 0 === this._duration && ((0 === t || 0 > this._rawPrevTime) && this._rawPrevTime !== t && (i = !0,
                this._rawPrevTime > 0 && (n = "onReverseComplete", e && (t = -1))), this._rawPrevTime = t);
        else if (1e-7 > t) this._totalTime = this._time = 0,
            this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== a || 0 === this._duration && this._rawPrevTime > 0) && (n = "onReverseComplete",
                s = this._reversed), 0 > t ? (this._active = !1, 0 === this._duration && (this._rawPrevTime >= 0 && (i = !0),
                this._rawPrevTime = t)) : this._initted || (i = !0);
        else if (this._totalTime = this._time = t,
            this._easeType) {
            var o = t / this._duration,
                l = this._easeType,
                _ = this._easePower;
            (1 === l || 3 === l && o >= .5) && (o = 1 - o), 3 === l && (o *= 2), 1 === _ ? o *= o : 2 === _ ? o *= o * o : 3 === _ ? o *= o * o * o : 4 === _ && (o *= o * o * o * o),
                this.ratio = 1 === l ? 1 - o : 2 === l ? o : .5 > t / this._duration ? o / 2 : 1 - o / 2;
        } else this.ratio = this._ease.getRatio(t / this._duration);
        if (this._time !== a || i) {
            if (!this._initted) {
                if (this._init(), !this._initted) return;
                this._time && !s ? this.ratio = this._ease.getRatio(this._time / this._duration) : s && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1));
            }
            for (this._active || !this._paused && this._time !== a && t >= 0 && (this._active = !0),
                0 === a && (this._startAt && (t >= 0 ? this._startAt.render(t, e, i) : n || (n = "_dummyGS")),
                    this.vars.onStart && (0 !== this._time || 0 === this._duration) && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || d))),
                r = this._firstPT; r;) r.f ? r.t[r.p](r.c * this.ratio + r.s) : r.t[r.p] = r.c * this.ratio + r.s,
                r = r._next;
            this._onUpdate && (0 > t && this._startAt && this._startAt.render(t, e, i), e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || d)),
                n && (this._gc || (0 > t && this._startAt && !this._onUpdate && this._startAt.render(t, e, i),
                    s && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[n] && this.vars[n].apply(this.vars[n + "Scope"] || this, this.vars[n + "Params"] || d)));
        }
    }, s._kill = function(t, e) {
        if ("all" === t && (t = null), null == t && (null == e || e === this.target)) return this._enabled(!1, !1);
        e = "string" != typeof e ? e || this._targets || this.target : C.selector(e) || e;
        var i, s, n, r, a, o, l, _;
        if ((e instanceof Array || R(e)) && "number" != typeof e[0])
            for (i = e.length; --i > -1;) this._kill(t, e[i]) && (o = !0);
        else {
            if (this._targets) {
                for (i = this._targets.length; --i > -1;)
                    if (e === this._targets[i]) {
                        a = this._propLookup[i] || {}, this._overwrittenProps = this._overwrittenProps || [],
                            s = this._overwrittenProps[i] = t ? this._overwrittenProps[i] || {} : "all";
                        break;
                    }
            } else {
                if (e !== this.target) return !1;
                a = this._propLookup, s = this._overwrittenProps = t ? this._overwrittenProps || {} : "all";
            }
            if (a) {
                l = t || a, _ = t !== s && "all" !== s && t !== a && (null == t || t._tempKill !== !0);
                for (n in l)(r = a[n]) && (r.pg && r.t._kill(l) && (o = !0), r.pg && 0 !== r.t._overwriteProps.length || (r._prev ? r._prev._next = r._next : r === this._firstPT && (this._firstPT = r._next),
                    r._next && (r._next._prev = r._prev), r._next = r._prev = null), delete a[n]), _ && (s[n] = 1);
                !this._firstPT && this._initted && this._enabled(!1, !1);
            }
        }
        return o;
    }, s.invalidate = function() {
        return this._notifyPluginsOfEnabled && C._onPluginEvent("_onDisable", this), this._firstPT = null,
            this._overwrittenProps = null, this._onUpdate = null, this._startAt = null, this._initted = this._active = this._notifyPluginsOfEnabled = !1,
            this._propLookup = this._targets ? {} : [], this;
    }, s._enabled = function(t, e) {
        if (r || n.wake(), t && this._gc) {
            var i, s = this._targets;
            if (s)
                for (i = s.length; --i > -1;) this._siblings[i] = G(s[i], this, !0);
            else this._siblings = G(this.target, this, !0);
        }
        return A.prototype._enabled.call(this, t, e), this._notifyPluginsOfEnabled && this._firstPT ? C._onPluginEvent(t ? "_onEnable" : "_onDisable", this) : !1;
    }, C.to = function(t, e, i) {
        return new C(t, e, i);
    }, C.from = function(t, e, i) {
        return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, new C(t, e, i);
    }, C.fromTo = function(t, e, i, s) {
        return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender,
            new C(t, e, s);
    }, C.delayedCall = function(t, e, i, s, n) {
        return new C(e, 0, {
            delay: t,
            onComplete: e,
            onCompleteParams: i,
            onCompleteScope: s,
            onReverseComplete: e,
            onReverseCompleteParams: i,
            onReverseCompleteScope: s,
            immediateRender: !1,
            useFrames: n,
            overwrite: 0
        });
    }, C.set = function(t, e) {
        return new C(t, 0, e);
    }, C.killTweensOf = C.killDelayedCallsTo = function(t, e) {
        for (var i = C.getTweensOf(t), s = i.length; --s > -1;) i[s]._kill(e, t);
    }, C.getTweensOf = function(t) {
        if (null == t) return [];
        t = "string" != typeof t ? t : C.selector(t) || t;
        var e, i, s, n;
        if ((t instanceof Array || R(t)) && "number" != typeof t[0]) {
            for (e = t.length, i = []; --e > -1;) i = i.concat(C.getTweensOf(t[e]));
            for (e = i.length; --e > -1;)
                for (n = i[e], s = e; --s > -1;) n === i[s] && i.splice(e, 1);
        } else
            for (i = G(t).concat(), e = i.length; --e > -1;) i[e]._gc && i.splice(e, 1);
        return i;
    };
    var q = p("plugins.TweenPlugin", function(t, e) {
        this._overwriteProps = (t || "").split(","), this._propName = this._overwriteProps[0],
            this._priority = e || 0, this._super = q.prototype;
    }, !0);
    if (s = q.prototype, q.version = "1.10.1", q.API = 2, s._firstPT = null, s._addTween = function(t, e, i, s, n, r) {
            var a, o;
            return null != s && (a = "number" == typeof s || "=" !== s.charAt(1) ? Number(s) - i : parseInt(s.charAt(0) + "1", 10) * Number(s.substr(2))) ? (this._firstPT = o = {
                _next: this._firstPT,
                t: t,
                p: e,
                s: i,
                c: a,
                f: "function" == typeof t[e],
                n: n || e,
                r: r
            }, o._next && (o._next._prev = o), o) : void 0;
        }, s.setRatio = function(t) {
            for (var e, i = this._firstPT, s = 1e-6; i;) e = i.c * t + i.s, i.r ? e = 0 | e + (e > 0 ? .5 : -.5) : s > e && e > -s && (e = 0),
                i.f ? i.t[i.p](e) : i.t[i.p] = e, i = i._next;
        }, s._kill = function(t) {
            var e, i = this._overwriteProps,
                s = this._firstPT;
            if (null != t[this._propName]) this._overwriteProps = [];
            else
                for (e = i.length; --e > -1;) null != t[i[e]] && i.splice(e, 1);
            for (; s;) null != t[s.n] && (s._next && (s._next._prev = s._prev), s._prev ? (s._prev._next = s._next,
                s._prev = null) : this._firstPT === s && (this._firstPT = s._next)), s = s._next;
            return !1;
        }, s._roundProps = function(t, e) {
            for (var i = this._firstPT; i;)(t[this._propName] || null != i.n && t[i.n.split(this._propName + "_").join("")]) && (i.r = e),
                i = i._next;
        }, C._onPluginEvent = function(t, e) {
            var i, s, n, r, a, o = e._firstPT;
            if ("_onInitAllProps" === t) {
                for (; o;) {
                    for (a = o._next, s = n; s && s.pr > o.pr;) s = s._next;
                    (o._prev = s ? s._prev : r) ? o._prev._next = o: n = o, (o._next = s) ? s._prev = o : r = o,
                        o = a;
                }
                o = e._firstPT = n;
            }
            for (; o;) o.pg && "function" == typeof o.t[t] && o.t[t]() && (i = !0), o = o._next;
            return i;
        }, q.activate = function(t) {
            for (var e = t.length; --e > -1;) t[e].API === q.API && (I[new t[e]()._propName] = t[e]);
            return !0;
        }, f.plugin = function(t) {
            if (!(t && t.propName && t.init && t.API)) throw "illegal plugin definition.";
            var e, i = t.propName,
                s = t.priority || 0,
                n = t.overwriteProps,
                r = {
                    init: "_onInitTween",
                    set: "setRatio",
                    kill: "_kill",
                    round: "_roundProps",
                    initAll: "_onInitAllProps"
                },
                a = p("plugins." + i.charAt(0).toUpperCase() + i.substr(1) + "Plugin", function() {
                    q.call(this, i, s), this._overwriteProps = n || [];
                }, t.global === !0),
                o = a.prototype = new q(i);
            o.constructor = a, a.API = t.API;
            for (e in r) "function" == typeof t[e] && (o[r[e]] = t[e]);
            return a.version = t.version, q.activate([a]), a;
        }, e = t._gsQueue) {
        for (i = 0; e.length > i; i++) e[i]();
        for (s in u) u[s].func || t.console.log("GSAP encountered missing dependency: com.greensock." + s);
    }
    r = !1;
})(window);

define("tweenlite", function(global) {
    return function() {
        var ret, fn;
        return ret || global.TweenLite;
    };
}(this));

(window._gsQueue || (window._gsQueue = [])).push(function() {
    var t = document.documentElement,
        e = window,
        i = function(i, s) {
            var r = "x" === s ? "Width" : "Height",
                n = "scroll" + r,
                a = "client" + r,
                o = document.body;
            return i === e || i === t || i === o ? Math.max(t[n], o[n]) - Math.max(t[a], o[a]) : i[n] - i["offset" + r];
        },
        s = window._gsDefine.plugin({
            propName: "scrollTo",
            API: 2,
            init: function(t, s, r) {
                return this._wdw = t === e, this._target = t, this._tween = r, "object" != typeof s && (s = {
                        y: s
                    }), this._autoKill = s.autoKill !== !1, this.x = this.xPrev = this.getX(), this.y = this.yPrev = this.getY(),
                    null != s.x ? this._addTween(this, "x", this.x, "max" === s.x ? i(t, "x") : s.x, "scrollTo_x", !0) : this.skipX = !0,
                    null != s.y ? this._addTween(this, "y", this.y, "max" === s.y ? i(t, "y") : s.y, "scrollTo_y", !0) : this.skipY = !0, !0;
            },
            set: function(t) {
                this._super.setRatio.call(this, t);
                var i = this._wdw || !this.skipX ? this.getX() : this.xPrev,
                    s = this._wdw || !this.skipY ? this.getY() : this.yPrev,
                    r = s - this.yPrev,
                    n = i - this.xPrev;
                this._autoKill && (!this.skipX && (n > 7 || -7 > n) && (this.skipX = !0), !this.skipY && (r > 7 || -7 > r) && (this.skipY = !0),
                    this.skipX && this.skipY && this._tween.kill()), this._wdw ? e.scrollTo(this.skipX ? i : this.x, this.skipY ? s : this.y) : (this.skipY || (this._target.scrollTop = this.y),
                    this.skipX || (this._target.scrollLeft = this.x)), this.xPrev = this.x, this.yPrev = this.y;
            }
        }),
        r = s.prototype;
    s.max = i, r.getX = function() {
        return this._wdw ? null != e.pageXOffset ? e.pageXOffset : null != t.scrollLeft ? t.scrollLeft : document.body.scrollLeft : this._target.scrollLeft;
    }, r.getY = function() {
        return this._wdw ? null != e.pageYOffset ? e.pageYOffset : null != t.scrollTop ? t.scrollTop : document.body.scrollTop : this._target.scrollTop;
    }, r._kill = function(t) {
        return t.scrollTo_x && (this.skipX = !0), t.scrollTo_y && (this.skipY = !0), this._super._kill.call(this, t);
    };
}), window._gsDefine && window._gsQueue.pop()();

define("scrollto", function(global) {
    return function() {
        var ret, fn;
        return ret || global.Scrollto;
    };
}(this));

define("jquery", [], function() {
    return Modernizr;
});

define("modernizr", function(global) {
    return function() {
        var ret, fn;
        return ret || global.Modernizr;
    };
}(this));

define("modules/controllers/carouselset", ["jquery", "underscore", "modules/definitions/standardmodule", "hammer", "tweenlite", "scrollto", "modernizr"], function($, _, parentModel, Hammer, TweenMax, Scrollto, Modernizr) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "carousel",
            author: "Jonathan Robles",
            lasteditby: "",
            target: undefined,
            virtualchildren: false,
            virtualchild_overlap: 0,
            yscroll: false,
            Xscrolldetect: "release dragleft dragright",
            Yscrolldetect: "release dragup dragdown",
            currentslide: 0,
            visibleslides: [],
            visibleslides_bleed: -2,
            visibleslides_with_bleed: [],
            lastdebouncedslide: 0,
            usetouchbinds: true,
            enable_touchbinds: true,
            snaponrelease: true,
            callback: function(o) {},
            childchange: function(index) {
                var self = this.parent;
                var destination = undefined;
                var animationvars = undefined;
                if (!self._var().yscroll) {
                    destination = self._var().childVars[index].XoffsetfromScroll;
                    animationvars = {
                        scrollTo: {
                            x: destination
                        },
                        ease: Power4.easeOut
                    };
                } else {
                    destination = self._var().childVars[index].YoffsetfromScroll;
                    animationvars = {
                        scrollTo: {
                            y: destination
                        },
                        ease: Power4.easeOut
                    };
                }
                TweenMax.to(self._var().target, .5, animationvars);
            },
            onscroll_callback: function(o) {},
            onscroll_start: function(o) {},
            onscroll_pause: function(o) {},
            onscroll_debounce_callback: function(o) {
                console.log("onscroll_debounce_callback");
            },
            onscroll_debounce_rate: 100,
            maxitemsonly: true,
            attachdisableclassonid: undefined,
            attachhidearrowclassonid: undefined,
            scrollsize: undefined,
            currentposition: undefined,
            currentpercent: undefined,
            targetXsize: undefined,
            targetYsize: undefined,
            targetwidth: undefined,
            targetheight: undefined,
            totalslides: undefined,
            childObjects: undefined,
            childVars: undefined,
            childVar_setter: function(index, value) {
                var self = this;
                self.index = index;
                self.childOBJ = value;
                self.target = $(value);
                self.Xsize = self.target.outerWidth();
                self.Ysize = self.target.innerHeight();
                self.Xdefault = Math.round(self.target.position().left);
                self.Ydefault = Math.round(self.target.position().top);
                self.XoffsetfromScroll = undefined;
                self.YoffsetfromScroll = undefined;
            },
            ObjHammer: undefined,
            isbeingdragged: false,
            startmove: true,
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onQuickjump: function(o) {
                    if (o.senderID == myID) {
                        parent.quickjump(o.data.index);
                    }
                },
                onJump: function(o) {
                    if (o.senderID == myID) {
                        parent.jump(o.data.index);
                    }
                },
                onPagejump: function(o) {
                    if (o.senderID == myID) {
                        parent.pagejump(o.data.index);
                    }
                },
                onNext: function(o) {
                    if (o.senderID == myID) {
                        parent.next();
                    }
                },
                onPrev: function(o) {
                    if (o.senderID == myID) {
                        parent.prev();
                    }
                },
                onWindowWidth: function(o) {
                    parent.refresh();
                }
            };
        }());
    };
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._var({
            busy: true
        });
        this._startlisteners();
        var self = this;
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        var self = this;
        self._var({
            busy: true
        });
        self.notify("Trace", "refresh");
        self.getpanelsizes();
        !self._var().virtualchildren ? self._childrentochildObjects() : self._virtualspacetochildObjects();
        window.TESTOBJ2 = self._var().childObjects;
        self.quickjump(self._var().currentslide);
        self._setwhatisvisible();
        if (self._var().attachhidearrowclassonid != undefined) {
            var lastindex = self._var().childObjects.length - 1;
            var windowsize;
            var firstindexcloseside;
            var lastindexfarside;
            if (!self._var().yscroll) {
                windowsize = self._var().targetXsize;
                firstindexcloseside = self._var().childVars[0].XoffsetfromScroll;
                lastindexfarside = self._var().childVars[lastindex].XoffsetfromScroll + self._var().childVars[lastindex].Xsize;
            } else {
                windowsize = self._var().targetYsize;
                firstindexcloseside = self._var().childVars[0].YoffsetfromScroll;
                lastindexfarside = self._var().childVars[lastindex].YoffsetfromScroll + self._var().childVars[lastindex].Ysize;
            }
            var estimatedScrollsize = lastindexfarside - firstindexcloseside;
            if (estimatedScrollsize <= windowsize) {
                self._var().attachhidearrowclassonid.addClass("hide_navigation");
            } else {
                self._var().attachhidearrowclassonid.removeClass("hide_navigation");
            }
        }
        var handlerdetect = !self._var().yscroll ? self._var().Xscrolldetect : self._var().Yscrolldetect;
        if (self._var().usetouchbinds && self._var().ObjHammer == undefined) {
            self._var().ObjHammer = Hammer(self._var().target, {
                drag_lock_to_axis: true
            }).on(handlerdetect, self._bindHandler(self));
        }
        self._var().target.unbind("scroll");
        self._var().target.scroll(self._onscroll(self));
        self._var({
            busy: false
        });
        if (self._var().callback != undefined) {
            self._var().callback(self);
        }
        self._var().lastdebouncedslide = self._var().currentslide;
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype._childrentochildObjects = function() {
        var self = this;
        self._var({
            childObjects: self._var().target.children()
        });
        var childObjects = self._var().childObjects;
        self._var({
            totalslides: childObjects.length,
            childVars: []
        });
        $.each(childObjects, function(index, value) {
            childVar_setter_local = self._var().childVar_setter;
            self._var().childVars.push(new childVar_setter_local(index, value));
        });
        $.each(self._var().childVars, function(index, value) {
            var zeroXdefault = self._var().childVars[0].Xdefault;
            var zeroYdefault = self._var().childVars[0].Ydefault;
            var marginOffsetX = value.target.css("marginLeft");
            var marginOffsetY = value.target.css("marginTop");
            value.XoffsetfromScroll = Math.round(value.Xdefault - zeroXdefault);
            value.YoffsetfromScroll = Math.round(value.Ydefault - zeroYdefault);
            if (marginOffsetX != undefined) {
                value.XoffsetfromScroll += parseInt(marginOffsetX, 10);
            }
            if (marginOffsetY != undefined) {
                value.YoffsetfromScroll += parseInt(marginOffsetY, 10);
            }
        });
        var totalscrollarea = !self._var().yscroll ? self._var().targetwidth : self._var().targetheight;
        var singlechildsize = !self._var().yscroll ? self._var().targetXsize : self._var().targetYsize;
        var scrollsize = totalscrollarea - singlechildsize;
        self._var({
            scrollsize: scrollsize
        });
    };
    _thizOBJ_.prototype._virtualspacetochildObjects = function() {
        var self = this;
        self._var({
            childObjects: self._var().target.children()
        });
        var totalscrollarea = !self._var().yscroll ? self._var().targetwidth : self._var().targetheight;
        var singlechildsize = !self._var().yscroll ? self._var().targetXsize : self._var().targetYsize;
        singlechildsize = singlechildsize - self._var().virtualchild_overlap;
        var scrollsize = totalscrollarea - singlechildsize;
        var tempStopArray = [];
        var childcount = Math.floor(scrollsize / singlechildsize);
        for (tempval = 0; tempval < childcount + 1; tempval++) {
            tempStopArray.push(tempval * singlechildsize);
        }
        tempStopArray.push(scrollsize);
        self._var({
            totalslides: tempStopArray.length,
            childVars: [],
            scrollsize: scrollsize
        });
        $.each(tempStopArray, function(index, value) {
            self._var().childVars.push({
                XoffsetfromScroll: value,
                YoffsetfromScroll: value
            });
        });
    };
    _thizOBJ_.prototype.offsetReturnForMaxItems = function() {
        var self = this;
        var Offsets = _.map(self._var().childVars, function(idx) {
            return !self._var().yscroll ? Math.abs(idx.XoffsetfromScroll) : Math.abs(idx.YoffsetfromScroll);
        });
        var newOffsets = new Array();
        var removeOffset = _.map(self._var().childVars, function(idx) {
            var needed = true;
            var lastitem = self._var().childVars.length - 1;
            var barrier;
            var barrierbuffer = 25;
            var returnCandidate;
            if (!self._var().yscroll) {
                barrier = self._var().childVars[lastitem].XoffsetfromScroll - self._var().targetXsize + self._var().childVars[lastitem].Xsize + barrierbuffer;
                returnCandidate = barrier + idx.Xsize < idx.XoffsetfromScroll;
            } else {
                barrier = self._var().childVars[lastitem].YoffsetfromScroll - self._var().targetYsize + self._var().childVars[lastitem].Ysize + barrierbuffer;
                returnCandidate = barrier + idx.Ysize < idx.YoffsetfromScroll;
            }
            return returnCandidate;
        });
        $.each(Offsets, function(index, value) {
            if (index == 0) {
                newOffsets.push(Offsets[index]);
            } else {
                if (!removeOffset[index]) {
                    newOffsets.push(Offsets[index]);
                }
            }
        });
        return newOffsets;
    };
    _thizOBJ_.prototype.getpanelsizes = function() {
        var self = this;
        var objreturn = {};
        objreturn.totalslides = self._var().totalslides;
        objreturn.targetwidth = self._var().targetwidth = self._var().target[0].scrollWidth;
        objreturn.targetheight = self._var().targetheight = self._var().target[0].scrollHeight;
        objreturn.targetXsize = self._var().targetXsize = self._var().target.innerWidth();
        objreturn.targetYsize = self._var().targetYsize = self._var().target.innerHeight();
        window.TESTOBJ = objreturn;
        if (self._var().visibleslides_bleed < 0) {
            var multiplier = Math.abs(self._var().visibleslides_bleed);
            self._var().visibleslides_bleed = !self._var().yscroll ? objreturn.targetXsize * multiplier : objreturn.targetYsize * multiplier;
        }
        return objreturn;
    };
    _thizOBJ_.prototype.getclosestindex = function(position) {
        var self = this;
        var getOffsets = _.map(self._var().childVars, function(idx) {
            return !self._var().yscroll ? idx.XoffsetfromScroll : idx.YoffsetfromScroll;
        });
        if (self._var().maxitemsonly) {
            getOffsets = self.offsetReturnForMaxItems();
        }
        var Offsets = _.map(getOffsets, function(off_value) {
            return Math.abs(off_value - position);
        });
        var closestdiff = _.reduce(Offsets, function(memo, val) {
            return val < memo ? val : memo;
        }, 1e4);
        var indextoReturn = _.indexOf(Offsets, closestdiff);
        self._var().currentslide = indextoReturn;
        return indextoReturn;
    };
    _thizOBJ_.prototype._returncurrentposition = function() {
        var self = this;
        return !self._var().yscroll ? self._var().target.scrollLeft() : self._var().target.scrollTop();
    };
    _thizOBJ_.prototype._gotoposition = function(getdestination) {
        var self = this;
        !self._var().yscroll ? self._var().target.scrollLeft(getdestination) : self._var().target.scrollTop(getdestination);
    };
    _thizOBJ_.prototype._gotopercent = function(getpercent) {
        var self = this;
        var scrollsize = self._var().scrollsize;
        var jumptoposition = scrollsize / (100 / getpercent);
        self._gotoposition(jumptoposition);
    };
    _thizOBJ_.prototype.selfie = function(o) {
        var self = this;
        var options = {
            yscroll: self._var().yscroll,
            quick: false
        };
        options = $.extend(options, o);
        if (!options.yscroll) {
            var thisindex = self.getclosestindex(self._returncurrentposition());
            if (!options.quick) {
                self.jump(thisindex);
            } else {
                self.quickjump(thisindex);
            }
        } else {
            var thisindex = self.getclosestindex(self._returncurrentposition());
            if (!options.quick) {
                self.jump(thisindex);
            } else {
                self.quickjump(thisindex);
            }
        }
    };
    _thizOBJ_.prototype.next = function(o) {
        var self = this;
        var options = {
            quick: false
        };
        options = $.extend(options, o);
        var thisindex = !self._var().yscroll ? self.getclosestindex(self._returncurrentposition()) : self.getclosestindex(self._var().target.scrollTop());
        var nextidx = thisindex == self._var().totalslides - 1 ? thisindex : thisindex + 1;
        var diff = Math.abs(nextidx - Number(self._var().lastdebouncedslide));
        if (diff > 1) {
            nextidx -= diff - 1;
        }!options.quick ? self.jump(nextidx) : self.quickjump(nextidx);
    };
    _thizOBJ_.prototype.prev = function(o) {
        var self = this;
        var options = {
            quick: false
        };
        options = $.extend(options, o);
        var thisindex = !self._var().yscroll ? self.getclosestindex(self._returncurrentposition()) : self.getclosestindex(self._var().target.scrollTop());
        var nextidx = thisindex == 0 ? 0 : thisindex - 1;
        var diff = Math.abs(nextidx - Number(self._var().lastdebouncedslide));
        if (diff > 1) {
            nextidx += diff - 1;
        }!options.quick ? self.jump(nextidx) : self.quickjump(nextidx);
    };
    _thizOBJ_.prototype.jump = function(index) {
        var self = this;
        self._var().childchange(index, self);
    };
    _thizOBJ_.prototype.pagejump = function(index) {
        this.jump(index);
    };
    _thizOBJ_.prototype.quickjump = function(index) {
        var self = this;
        if (!self._var().yscroll) {
            var destination = self._var().childVars[index].XoffsetfromScroll;
        } else {
            var destination = self._var().childVars[index].YoffsetfromScroll;
        }
        self._gotoposition(destination);
    };
    _thizOBJ_.prototype._bindHandler = function(self) {
        var lastCase = undefined;
        var touchpoint = undefined;
        var targetposition = function() {
            touchpoint = self._returncurrentposition();
        };
        var casechecks = undefined;
        var directions = undefined;
        if (!self._var().yscroll) {
            casechecks = ["dragright", "dragleft", "swipeleft", "swiperight", "release"];
            directions = [Hammer.DIRECTION_RIGHT, Hammer.DIRECTION_LEFT];
        } else {
            casechecks = ["dragdown", "dragup", "swipeup", "swipedown", "release"];
            directions = [Hammer.DIRECTION_DOWN, Hammer.DIRECTION_UP];
        }
        var eventHandler = function(ev) {
            ev.gesture.preventDefault();
            if (ev.type != lastCase) {
                targetposition();
            }
            if (self._var().enable_touchbinds) {
                switch (ev.type) {
                    case casechecks[4]:
                        self._var().isbeingdragged = false;
                        targetposition();
                        if (self._var().snaponrelease) {
                            self.selfie();
                        } else {
                            var currentvars = {};
                            currentvars.scrollsize = self._var().scrollsize;
                            currentvars.currentposition = self._returncurrentposition();
                            currentvars.currentpercent = 100 / (currentvars.scrollsize / currentvars.currentposition);
                            self._debounce(self)(currentvars);
                        }
                        ev.gesture.stopDetect();
                        break;

                    case casechecks[0]:
                    case casechecks[1]:
                        self._var().isbeingdragged = true;
                        var pane_offset = touchpoint;
                        var drag_offset = !self._var().yscroll ? ev.gesture.deltaX : ev.gesture.deltaY;
                        if (ev.gesture.direction == directions[0]) {
                            drag_offset = -Math.abs(drag_offset);
                        } else if (ev.gesture.direction == directions[1]) {
                            drag_offset = Math.abs(drag_offset);
                        };
                        var destination = pane_offset + drag_offset;
                        !self._var().yscroll ? self._var().target.scrollLeft(destination) : self._var().target.scrollTop(destination);
                        break;

                    case casechecks[2]:
                        self.next();
                        self._var().isbeingdragged = false;
                        ev.gesture.stopDetect();
                        break;

                    case casechecks[3]:
                        self.prev();
                        self._var().isbeingdragged = false;
                        ev.gesture.stopDetect();
                        break;
                }
            }
            lastCase = ev.type;
        };
        return eventHandler;
    };
    _thizOBJ_.prototype._debounce = function(self) {
        var debouncer = _.debounce(function(currentvars) {
            var currentposition = self._returncurrentposition();
            var closeindex = self.getclosestindex(currentposition);
            var destination = undefined;
            if (!self._var().yscroll) {
                destination = self._var().childVars[closeindex].XoffsetfromScroll;
            } else {
                destination = self._var().childVars[closeindex].YoffsetfromScroll;
            }
            var difference = Math.abs(currentposition - destination);
            self._setwhatisvisible();
            if (self._var().onscroll_pause != undefined) {
                self._var().onscroll_pause(currentvars);
            }
            if (difference > 1 && !self._var().isbeingdragged && self._var().snaponrelease) {
                self.selfie();
            } else if (difference < 2 || !self._var().isbeingdragged && !self._var().snaponrelease) {
                if (!self._var().startmove) {
                    self._var().lastdebouncedslide = self._var().currentslide;
                    self._setwhatisvisible();
                    self._var().onscroll_debounce_callback(currentvars);
                    self._var({
                        startmove: true
                    });
                }
            }
        }, self._var().onscroll_debounce_rate);
        return debouncer;
    };
    _thizOBJ_.prototype._setwhatisvisible = function() {
        var self = this;
        var visibleslides = [];
        var visibleslides_with_bleed = [];
        var visibleindex = self._var().currentslide;
        var visibleslides_bleed = self._var().visibleslides_bleed;
        var visiblewindow_startX = self._var().childVars[visibleindex].XoffsetfromScroll;
        var visiblewindow_startY = self._var().childVars[visibleindex].YoffsetfromScroll;
        var visiblewindow_endX = visiblewindow_startX + self._var().targetXsize;
        var visiblewindow_endY = visiblewindow_startY + self._var().targetYsize;
        var visiblewindow_startXb = self._var().childVars[visibleindex].XoffsetfromScroll - self._var().visibleslides_bleed;
        var visiblewindow_startYb = self._var().childVars[visibleindex].YoffsetfromScroll - self._var().visibleslides_bleed;
        var visiblewindow_endXb = visiblewindow_startX + self._var().targetXsize + self._var().visibleslides_bleed;
        var visiblewindow_endYb = visiblewindow_startY + self._var().targetYsize + self._var().visibleslides_bleed;
        if (!self._var().yscroll) {
            $.each(self._var().childVars, function(index, value) {
                var minVar = value.XoffsetfromScroll;
                var maxVar = minVar + value.Xsize;
                if (minVar >= visiblewindow_startX && maxVar <= visiblewindow_endX) {
                    visibleslides.push(index);
                }
                if (minVar >= visiblewindow_startXb && maxVar <= visiblewindow_endXb) {
                    visibleslides_with_bleed.push(index);
                }
            });
        } else {
            $.each(self._var().childVars, function(index, value) {
                var minVar = value.YoffsetfromScroll;
                var maxVar = minVar + value.Ysize;
                if (minVar >= visiblewindow_startY && maxVar <= visiblewindow_endY) {
                    visibleslides.push(index);
                }
                if (minVar >= visiblewindow_startYb && maxVar <= visiblewindow_endYb) {
                    visibleslides_with_bleed.push(index);
                }
            });
        }
        self._var().visibleslides = visibleslides;
        self._var().visibleslides_with_bleed = visibleslides_with_bleed;
        if (self._var().attachdisableclassonid != undefined) {
            var firstidtodisable = 0;
            var lastidtodisable = self._var().totalslides - 1;
            if (_.contains(self._var().visibleslides, firstidtodisable)) {
                self._var().attachdisableclassonid.addClass("disable_previous");
            } else {
                self._var().attachdisableclassonid.removeClass("disable_previous");
            }
            if (_.contains(self._var().visibleslides, lastidtodisable)) {
                self._var().attachdisableclassonid.addClass("disable_next");
            } else {
                self._var().attachdisableclassonid.removeClass("disable_next");
            }
        }
    };
    _thizOBJ_.prototype._onscroll = function(self) {
        var Offsets = _.map(self._var().childVars, function(idx) {
            return !self._var().yscroll ? Math.abs(idx.XoffsetfromScroll) : Math.abs(idx.YoffsetfromScroll);
        });
        var debouncefunc = self._debounce(self);
        var handler = function() {
            var currentvars = {};
            currentvars.scrollsize = self._var().scrollsize;
            currentvars.currentposition = self._returncurrentposition();
            currentvars.currentpercent = 100 / (currentvars.scrollsize / currentvars.currentposition);
            if (self._var().startmove) {
                self._var().onscroll_start(currentvars);
                self._var({
                    startmove: false
                });
            }
            self._var().onscroll_callback(currentvars);
            debouncefunc(currentvars);
        };
        return handler;
    };
    return _thizOBJ_;
});

function X2JS(config) {
    var VERSION = "1.1.3";
    config = config || {};
    initConfigDefaults();

    function initConfigDefaults() {
        if (config.escapeMode === undefined) {
            config.escapeMode = true;
        }
        config.attributePrefix = config.attributePrefix || "_";
        config.arrayAccessForm = config.arrayAccessForm || "none";
        config.emptyNodeForm = config.emptyNodeForm || "text";
        if (config.enableToStringFunc === undefined) {
            config.enableToStringFunc = true;
        }
        config.arrayAccessFormPaths = config.arrayAccessFormPaths || [];
        if (config.skipEmptyTextNodesForObj === undefined) {
            config.skipEmptyTextNodesForObj = true;
        }
    }
    var DOMNodeTypes = {
        ELEMENT_NODE: 1,
        TEXT_NODE: 3,
        CDATA_SECTION_NODE: 4,
        COMMENT_NODE: 8,
        DOCUMENT_NODE: 9
    };

    function getNodeLocalName(node) {
        var nodeLocalName = node.localName;
        if (nodeLocalName == null) nodeLocalName = node.baseName;
        if (nodeLocalName == null || nodeLocalName == "") nodeLocalName = node.nodeName;
        return nodeLocalName;
    }

    function getNodePrefix(node) {
        return node.prefix;
    }

    function escapeXmlChars(str) {
        if (typeof str == "string") return str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#x27;").replace(/\//g, "&#x2F;");
        else return str;
    }

    function unescapeXmlChars(str) {
        return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"').replace(/&#x27;/g, "'").replace(/&#x2F;/g, "/");
    }

    function toArrayAccessForm(obj, childName, path) {
        switch (config.arrayAccessForm) {
            case "property":
                if (!(obj[childName] instanceof Array)) obj[childName + "_asArray"] = [obj[childName]];
                else obj[childName + "_asArray"] = obj[childName];
                break;
        }
        if (!(obj[childName] instanceof Array) && config.arrayAccessFormPaths.length > 0) {
            var idx = 0;
            for (; idx < config.arrayAccessFormPaths.length; idx++) {
                var arrayPath = config.arrayAccessFormPaths[idx];
                if (typeof arrayPath === "string") {
                    if (arrayPath == path) break;
                } else if (arrayPath instanceof RegExp) {
                    if (arrayPath.test(path)) break;
                } else if (typeof arrayPath === "function") {
                    if (arrayPath(obj, childName, path)) break;
                }
            }
            if (idx != config.arrayAccessFormPaths.length) {
                obj[childName] = [obj[childName]];
            }
        }
    }

    function parseDOMChildren(node, path) {
        if (node.nodeType == DOMNodeTypes.DOCUMENT_NODE) {
            var result = new Object();
            var nodeChildren = node.childNodes;
            for (var cidx = 0; cidx < nodeChildren.length; cidx++) {
                var child = nodeChildren.item(cidx);
                if (child.nodeType == DOMNodeTypes.ELEMENT_NODE) {
                    var childName = getNodeLocalName(child);
                    result[childName] = parseDOMChildren(child, childName);
                }
            }
            return result;
        } else if (node.nodeType == DOMNodeTypes.ELEMENT_NODE) {
            var result = new Object();
            result.__cnt = 0;
            var nodeChildren = node.childNodes;
            for (var cidx = 0; cidx < nodeChildren.length; cidx++) {
                var child = nodeChildren.item(cidx);
                var childName = getNodeLocalName(child);
                if (child.nodeType != DOMNodeTypes.COMMENT_NODE) {
                    result.__cnt++;
                    if (result[childName] == null) {
                        result[childName] = parseDOMChildren(child, path + "." + childName);
                        toArrayAccessForm(result, childName, path + "." + childName);
                    } else {
                        if (result[childName] != null) {
                            if (!(result[childName] instanceof Array)) {
                                result[childName] = [result[childName]];
                                toArrayAccessForm(result, childName, path + "." + childName);
                            }
                        }
                        result[childName][result[childName].length] = parseDOMChildren(child, path + "." + childName);
                    }
                }
            }
            for (var aidx = 0; aidx < node.attributes.length; aidx++) {
                var attr = node.attributes.item(aidx);
                result.__cnt++;
                result[config.attributePrefix + attr.name] = attr.value;
            }
            var nodePrefix = getNodePrefix(node);
            if (nodePrefix != null && nodePrefix != "") {
                result.__cnt++;
                result.__prefix = nodePrefix;
            }
            if (result["#text"] != null) {
                result.__text = result["#text"];
                if (result.__text instanceof Array) {
                    result.__text = result.__text.join("\n");
                }
                if (config.escapeMode) result.__text = unescapeXmlChars(result.__text);
                delete result["#text"];
                if (config.arrayAccessForm == "property") delete result["#text_asArray"];
            }
            if (result["#cdata-section"] != null) {
                result.__cdata = result["#cdata-section"];
                delete result["#cdata-section"];
                if (config.arrayAccessForm == "property") delete result["#cdata-section_asArray"];
            }
            if (result.__cnt == 1 && result.__text != null) {
                result = result.__text;
            } else if (result.__cnt == 0 && config.emptyNodeForm == "text") {
                result = "";
            } else if (result.__cnt > 1 && result.__text != null && config.skipEmptyTextNodesForObj) {
                if (result.__text.trim() == "") {
                    delete result.__text;
                }
            }
            delete result.__cnt;
            if (config.enableToStringFunc && result.__text != null || result.__cdata != null) {
                result.toString = function() {
                    return (this.__text != null ? this.__text : "") + (this.__cdata != null ? this.__cdata : "");
                };
            }
            return result;
        } else if (node.nodeType == DOMNodeTypes.TEXT_NODE || node.nodeType == DOMNodeTypes.CDATA_SECTION_NODE) {
            return node.nodeValue;
        }
    }

    function startTag(jsonObj, element, attrList, closed) {
        var resultStr = "<" + (jsonObj != null && jsonObj.__prefix != null ? jsonObj.__prefix + ":" : "") + element;
        if (attrList != null) {
            for (var aidx = 0; aidx < attrList.length; aidx++) {
                var attrName = attrList[aidx];
                var attrVal = jsonObj[attrName];
                resultStr += " " + attrName.substr(config.attributePrefix.length) + "='" + attrVal + "'";
            }
        }
        if (!closed) resultStr += ">";
        else resultStr += "/>";
        return resultStr;
    }

    function endTag(jsonObj, elementName) {
        return "</" + (jsonObj.__prefix != null ? jsonObj.__prefix + ":" : "") + elementName + ">";
    }

    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    function jsonXmlSpecialElem(jsonObj, jsonObjField) {
        if (config.arrayAccessForm == "property" && endsWith(jsonObjField.toString(), "_asArray") || jsonObjField.toString().indexOf(config.attributePrefix) == 0 || jsonObjField.toString().indexOf("__") == 0 || jsonObj[jsonObjField] instanceof Function) return true;
        else return false;
    }

    function jsonXmlElemCount(jsonObj) {
        var elementsCnt = 0;
        if (jsonObj instanceof Object) {
            for (var it in jsonObj) {
                if (jsonXmlSpecialElem(jsonObj, it)) continue;
                elementsCnt++;
            }
        }
        return elementsCnt;
    }

    function parseJSONAttributes(jsonObj) {
        var attrList = [];
        if (jsonObj instanceof Object) {
            for (var ait in jsonObj) {
                if (ait.toString().indexOf("__") == -1 && ait.toString().indexOf(config.attributePrefix) == 0) {
                    attrList.push(ait);
                }
            }
        }
        return attrList;
    }

    function parseJSONTextAttrs(jsonTxtObj) {
        var result = "";
        if (jsonTxtObj.__cdata != null) {
            result += "<![CDATA[" + jsonTxtObj.__cdata + "]]>";
        }
        if (jsonTxtObj.__text != null) {
            if (config.escapeMode) result += escapeXmlChars(jsonTxtObj.__text);
            else result += jsonTxtObj.__text;
        }
        return result;
    }

    function parseJSONTextObject(jsonTxtObj) {
        var result = "";
        if (jsonTxtObj instanceof Object) {
            result += parseJSONTextAttrs(jsonTxtObj);
        } else if (jsonTxtObj != null) {
            if (config.escapeMode) result += escapeXmlChars(jsonTxtObj);
            else result += jsonTxtObj;
        }
        return result;
    }

    function parseJSONArray(jsonArrRoot, jsonArrObj, attrList) {
        var result = "";
        if (jsonArrRoot.length == 0) {
            result += startTag(jsonArrRoot, jsonArrObj, attrList, true);
        } else {
            for (var arIdx = 0; arIdx < jsonArrRoot.length; arIdx++) {
                result += startTag(jsonArrRoot[arIdx], jsonArrObj, parseJSONAttributes(jsonArrRoot[arIdx]), false);
                result += parseJSONObject(jsonArrRoot[arIdx]);
                result += endTag(jsonArrRoot[arIdx], jsonArrObj);
            }
        }
        return result;
    }

    function parseJSONObject(jsonObj) {
        var result = "";
        var elementsCnt = jsonXmlElemCount(jsonObj);
        if (elementsCnt > 0) {
            for (var it in jsonObj) {
                if (jsonXmlSpecialElem(jsonObj, it)) continue;
                var subObj = jsonObj[it];
                var attrList = parseJSONAttributes(subObj);
                if (subObj == null || subObj == undefined) {
                    result += startTag(subObj, it, attrList, true);
                } else if (subObj instanceof Object) {
                    if (subObj instanceof Array) {
                        result += parseJSONArray(subObj, it, attrList);
                    } else {
                        var subObjElementsCnt = jsonXmlElemCount(subObj);
                        if (subObjElementsCnt > 0 || subObj.__text != null || subObj.__cdata != null) {
                            result += startTag(subObj, it, attrList, false);
                            result += parseJSONObject(subObj);
                            result += endTag(subObj, it);
                        } else {
                            result += startTag(subObj, it, attrList, true);
                        }
                    }
                } else {
                    result += startTag(subObj, it, attrList, false);
                    result += parseJSONTextObject(subObj);
                    result += endTag(subObj, it);
                }
            }
        }
        result += parseJSONTextObject(jsonObj);
        return result;
    }
    this.parseXmlString = function(xmlDocStr) {
        if (xmlDocStr === undefined) {
            return null;
        }
        var xmlDoc;
        if (window.DOMParser) {
            var parser = new window.DOMParser();
            xmlDoc = parser.parseFromString(xmlDocStr, "text/xml");
        } else {
            if (xmlDocStr.indexOf("<?") == 0) {
                xmlDocStr = xmlDocStr.substr(xmlDocStr.indexOf("?>") + 2);
            }
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = "false";
            xmlDoc.loadXML(xmlDocStr);
        }
        return xmlDoc;
    };
    this.asArray = function(prop) {
        if (prop instanceof Array) return prop;
        else return [prop];
    };
    this.xml2json = function(xmlDoc) {
        return parseDOMChildren(xmlDoc);
    };
    this.xml_str2json = function(xmlDocStr) {
        var xmlDoc = this.parseXmlString(xmlDocStr);
        return this.xml2json(xmlDoc);
    };
    this.json2xml_str = function(jsonObj) {
        return parseJSONObject(jsonObj);
    };
    this.json2xml = function(jsonObj) {
        var xmlDocStr = this.json2xml_str(jsonObj);
        return this.parseXmlString(xmlDocStr);
    };
    this.getVersion = function() {
        return VERSION;
    };
}

define("x2js", function(global) {
    return function() {
        var ret, fn;
        return ret || global.X2JS;
    };
}(this));

define("modules/parsers/dataparser", ["x2js", "jquery", "modules/definitions/standardmodule"], function(X2JS, $, parentModel) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "dataparser",
            author: "Jonathan Robles",
            lasteditby: "",
            file: "data/sitedata.json",
            usenocache: true,
            dataXML: undefined,
            data: undefined,
            callback: undefined,
            jsonpReturn: "window._global$.jsonpReturn('<%id>','JSONdata','global')",
            format: "json",
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onJSONdata: function(o) {
                    if (o.senderID == myID) {
                        parent._var({
                            data: o.data,
                            busy: false
                        });
                        if (parent._var().callback != undefined) {
                            parent._var().callback(o.data);
                        }
                    }
                }
            };
        }());
    };
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this.getdata();
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.getdata = function() {
        var parent = this;
        parent._var({
            busy: true
        });
        this.notify("Trace", "getdata format:" + parent._var().format + " file" + parent._var().file);
        var JSONtoUSE = parent._nocache(parent._var().file);
        var onData = function(jsondata) {
            parent.notify("JSONdata", jsondata);
        };
        if (this._var().format == "text") {
            $.ajax({
                xhrFields: {
                    withCredentials: false
                },
                crossDomain: false,
                type: "GET",
                url: parent._var().file,
                dataType: parent._var().format,
                statusCode: {
                    404: function() {
                        alert("please check xml!");
                    }
                },
                success: function(xml) {
                    parent._var({
                        dataXML: xml
                    });
                    var gparse = new X2JS();
                    var jsondata = gparse.parseXmlString(xml);
                    jsondata = gparse.xml2json(jsondata);
                    onData(jsondata);
                }
            });
        } else if (this._var().format == "jsonp") {
            if (this._var().jsonpReturn != undefined) {
                JSONtoUSE += "&jsonp=" + this._var().jsonpReturn;
                JSONtoUSE = JSONtoUSE.replace("<%id>", this._id());
            }
            JSONtoUSE += "&=?";
            $.ajaxSetup({
                type: "POST",
                data: {},
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true
            });
            $.getJSON(JSONtoUSE, {
                format: "jsonp"
            }).error(function() {});
        } else {
            $.getJSON(JSONtoUSE, {
                format: "json"
            }, function(jsondata) {
                onData(jsondata);
            }).error(function() {});
        }
    };
    return _thizOBJ_;
});

(function() {
    var nlsRegExp = /(^.*(^|\/)nls(\/|$))([^\/]*)\/?([^\/]*)/;

    function addPart(locale, master, needed, toLoad, prefix, suffix) {
        if (master[locale]) {
            needed.push(locale);
            if (master[locale] === true || master[locale] === 1) {
                toLoad.push(prefix + locale + "/" + suffix);
            }
        }
    }

    function addIfExists(req, locale, toLoad, prefix, suffix) {
        var fullName = prefix + locale + "/" + suffix;
        if (require._fileExists(req.toUrl(fullName + ".js"))) {
            toLoad.push(fullName);
        }
    }

    function mixin(target, source, force) {
        var prop;
        for (prop in source) {
            if (source.hasOwnProperty(prop) && (!target.hasOwnProperty(prop) || force)) {
                target[prop] = source[prop];
            } else if (typeof source[prop] === "object") {
                if (!target[prop] && source[prop]) {
                    target[prop] = {};
                }
                mixin(target[prop], source[prop], force);
            }
        }
    }
    define("i18n", ["module"], function(module) {
        var masterConfig = module.config ? module.config() : {};
        return {
            version: "2.0.4",
            load: function(name, req, onLoad, config) {
                config = config || {};
                if (config.locale) {
                    masterConfig.locale = config.locale;
                }
                var masterName, match = nlsRegExp.exec(name),
                    prefix = match[1],
                    locale = match[4],
                    suffix = match[5],
                    parts = locale.split("-"),
                    toLoad = [],
                    value = {},
                    i, part, current = "";
                if (match[5]) {
                    prefix = match[1];
                    masterName = prefix + suffix;
                } else {
                    masterName = name;
                    suffix = match[4];
                    locale = masterConfig.locale;
                    if (!locale) {
                        locale = masterConfig.locale = typeof navigator === "undefined" ? "root" : (navigator.language || navigator.userLanguage || "root").toLowerCase();
                    }
                    parts = locale.split("-");
                }
                if (config.isBuild) {
                    toLoad.push(masterName);
                    addIfExists(req, "root", toLoad, prefix, suffix);
                    for (i = 0; i < parts.length; i++) {
                        part = parts[i];
                        current += (current ? "-" : "") + part;
                        addIfExists(req, current, toLoad, prefix, suffix);
                    }
                    req(toLoad, function() {
                        onLoad();
                    });
                } else {
                    req([masterName], function(master) {
                        var needed = [],
                            part;
                        addPart("root", master, needed, toLoad, prefix, suffix);
                        for (i = 0; i < parts.length; i++) {
                            part = parts[i];
                            current += (current ? "-" : "") + part;
                            addPart(current, master, needed, toLoad, prefix, suffix);
                        }
                        req(toLoad, function() {
                            var i, partBundle, part;
                            for (i = needed.length - 1; i > -1 && needed[i]; i--) {
                                part = needed[i];
                                partBundle = master[part];
                                if (partBundle === true || partBundle === 1) {
                                    partBundle = req(prefix + part + "/" + suffix);
                                }
                                mixin(value, partBundle);
                            }
                            onLoad(value);
                        });
                    });
                }
            }
        };
    });
})();

define("modules/nls/uxlocale", {
    root: {
        close: "close",
        open: "open",
        play: "play",
        pause: "pause",
        stop: "stop",
        rewind: "rewind",
        fastforward: "fastforward",
        skip: "skip"
    },
    fr: true
});

define("modules/data/jsontotemplate", ["jquery", "modules/definitions/standardmodule", "i18n!modules/nls/uxlocale"], function($, parentModel, locale) {
    function _thizOBJ_(o) {
        var defaults = {
            type: "jsontotemplate",
            author: "Jonathan Robles",
            lasteditby: "",
            dataIN: {
                window: {
                    title: "Sample Widget",
                    name: "main_window",
                    width: 500,
                    height: 500
                },
                image: {
                    src: "Images/Sun.png",
                    name: "sun1",
                    width: 250,
                    height: 250,
                    alignment: "center"
                }
            },
            htmlIN: '<div id="{{window.name}}"><p>{{window.title}}</p><img src="{{image.src}}" alt="{{image.name}}" height="{{image.height}}" width="{{image.width}}"></div>',
            regEX: /{{(.*?)}}/g,
            errorFill: function(data) {
                return '[ xALERT!!! "' + data + '" not found in json]';
            },
            data: undefined,
            callback: function(data) {
                alert("data rendered :" + data);
            },
            busy: false
        };
        defaults = $.extend(defaults, o);
        parentModel.call(this, defaults);
        return this;
    }
    _thizOBJ_.prototype = Object.create(parentModel.prototype);
    _thizOBJ_.prototype._startlisteners = function() {
        this.notify("Trace", "_startlisteners");
        var parent = this;
        var myID = this._id();
        _notify.add(this._id(), function() {
            return {
                onJSONtoTemplateData: function(o) {
                    if (o.senderID == myID) {
                        parent._var({
                            data: o.data,
                            busy: false
                        });
                        if (parent._var().callback != undefined) {
                            parent._var().callback(o.data);
                        }
                    }
                }
            };
        }());
    };
    _thizOBJ_.prototype.init = function() {
        this.notify("Trace", "init");
        this._startlisteners();
        this.refresh();
    };
    _thizOBJ_.prototype.refresh = function() {
        this.notify("Trace", "refresh");
        this.translateData();
    };
    _thizOBJ_.prototype.kill = function() {
        this.notify("Trace", "kill");
        _notify.rem(this._id());
    };
    _thizOBJ_.prototype.translateData = function() {
        var recompose = function(obj, string) {
            var parts = string.split(".");
            var newObj = obj[parts[0]];
            if (parts[1]) {
                parts.splice(0, 1);
                var newString = parts.join(".");
                return recompose(newObj, newString);
            }
            return newObj;
        };
        var parent = this;
        parent._var({
            busy: true
        });
        this.notify("Trace", "translateData dataIn");
        var onData = function(data) {
            parent.notify("JSONtoTemplateData", data);
        };
        var tempjsondata = parent._var().dataIN;
        tempjsondata.locale = locale;
        var originalTemplate = parent._var().htmlIN;
        var processedTemplate = originalTemplate;
        var regEX = parent._var().regEX;
        var findinstances = originalTemplate.match(regEX);
        var data = undefined;
        if (findinstances != null) {
            $.each(findinstances, function(index) {
                var searchstring = findinstances[index];
                var cleanstring = searchstring.replace(/[{}]/g, "");
                var newstring = recompose(tempjsondata, cleanstring);
                newstring = $("<div />").html(newstring).text();
                if (newstring != undefined) {
                    processedTemplate = processedTemplate.replace(searchstring, newstring);
                } else {
                    processedTemplate = processedTemplate.replace(searchstring, parent._var().errorFill(cleanstring));
                }
            });
            data = processedTemplate;
        } else {
            data = originalTemplate;
        }
        onData(data);
    };
    return _thizOBJ_;
});

require(["jquery", "modules/controllers/carouselset", "modules/parsers/dataparser", "modules/data/jsontotemplate", "tweenlite", "scrollto"], function($, Carousel, jsonParser, jsontoTemplate, TweenMax, Scrollto) {
    var mainOBJ = this;
    var self = this;
    var fullShows_list = [{
        title: "Abby's Ultimate Dance Competition",
        url: "shows/abbys-ultimate-dance-competition",
        nid: "22790",
        total_videos: "11"
    }, {
        title: "Dance Moms: Abby's Studio Rescue",
        url: "shows/abbys-studio-rescue",
        nid: "22790",
        total_videos: "4"
    }, {
        title: "Raising Asia",
        url: "shows/raising-asia",
        nid: "22790",
        total_videos: "13"
    }, {
        title: "Bristol Palin: Life's a Tripp",
        url: "shows/bristol-palin-lifes-a-tripp",
        nid: "22790",
        total_videos: "1"
    }];
    var modifyData = function(getData) {
        if (getData.duration != undefined) {
            var tempnum = Math.round(Number(getData.duration));
            var tempremainder = tempnum % 60;
            if (tempremainder <= 9) {
                tempremainder = "0" + tempremainder;
            }
            getData.duration = Math.floor(tempnum / 60) + ":" + tempremainder;
        }
        if (getData.season == "" || getData.season == undefined) {
            getData.season = "0X";
        }
        if (getData.episode == "" || getData.episode == undefined) {
            getData.episode = "0X";
        }
        if (getData.duration == "" || getData.duration == undefined) {
            getData.duration = "X0X";
        }
        var mpxdataattrib = "";
        if (getData.behind_wall == "1") {
            mpxdataattrib = 'data-auth-required="true" data-mpx-id="' + getData.mpxid + '" ';
        }
        getData.mpxdataattrib = mpxdataattrib;
        return getData;
    };
    var modifyHTML = function(getHTML) {
        var getHTML = getHTML.replace("S0X | E0X |", "");
        var getHTML = getHTML.replace("S0X | E0X", "");
        var getHTML = getHTML.replace("E0X | X0X", "");
        var getHTML = getHTML.replace("S0X |", "");
        var getHTML = getHTML.replace("| X0X", "");
        var getHTML = getHTML.replace("E0X", "");
        var getHTML = getHTML.replace(/X0X/gi, "");
        var getHTML = getHTML.replace("Season 0X | Episode 0X |", "");
        var getHTML = getHTML.replace("Season 0X | Episode 0X", "");
        var getHTML = getHTML.replace("Episode 0X | X0X", "");
        var getHTML = getHTML.replace("Season 0X |", "");
        var getHTML = getHTML.replace("Episode 0X", "");
        return getHTML;
    };
    var initializeScroller = function(scrollerOBJ, getDATA) {
        if (getDATA != undefined) {
            scrollerOBJ.instance._var({
                custom_contentData: getDATA.items
            });
        }
        scrollerOBJ.instance.init();
        scrollerOBJ.instance._var().custom_jsontotemplate._startlisteners();
        scrollerOBJ.instance._var().custom_Dataparser._startlisteners();
        $(scrollerOBJ.obj).find(scrollerOBJ.data.next).on(_global$.isTouch ? "touchstart" : "click", function() {
            scrollerOBJ.instance.next();
        });
        $(scrollerOBJ.obj).find(scrollerOBJ.data.previous).on(_global$.isTouch ? "touchstart" : "click", function() {
            scrollerOBJ.instance.prev();
        });
    };
    var makeAd = function() {
        var adhtml = '<div class="row-fluid standalone_ad_container container_outer">';
        adhtml += '<div class="container_inner">';
        adhtml += '<div class="standalone_ad_container_ad_area">';
        adhtml += "</div>";
        adhtml += "</div>";
        adhtml += "</div>";
        $(".fullepisodes_container").find(".container_inner").find("div.episode_set:eq(2)").after(adhtml);
    };
    var placeAd = function() {
        var placeItnow = function() {
            var jumptoY = $(".standalone_ad_container_ad_area").offset().top;
            var diffY = $(".dart-name-sidebar_300x250").offset().top;
            var addYtarget = $(".dart-name-sidebar_300x250").position().top + (jumptoY - diffY);
            $(".dart-name-sidebar_300x250").css("top", addYtarget + "px");
        };
        placeItnow();
        if (waitforAnimation != undefined) {
            clearInterval(waitforAnimation);
        }
        var waitforAnimation = setTimeout(function() {
            placeItnow();
            $(".dart-name-sidebar_300x250").addClass("visible");
        }, 1e3);
    };
    var applyCarousels = function() {
        var pageCarousels = new Array();
        $.each($('[data-uiType="carousel"]'), function(index, value) {
            $(value).attr("data-uiType", "carousel_rendered");
            var uiData = $(value).attr("data-uiData");
            uiData = eval("(" + uiData + ")");
            var targetListContainer = $(value).find(uiData.container);
            pageCarousels.push({
                obj: value,
                instance: new Carousel({
                    custom_updateClassesAndImagePreload: function(o) {
                        var visibleslides = this.visibleslides;
                        var visibleslides_with_bleed = this.visibleslides_with_bleed;
                        var childObjects = this.childObjects;
                        $.each(childObjects, function(index, value) {
                            var isthere = _.indexOf(visibleslides, index);
                            if (isthere != -1) {
                                $(value).addClass("inview");
                                $(value).addClass("viewed");
                            } else {
                                $(value).removeClass("inview");
                            }
                        });
                        $.each(childObjects, function(index, value) {
                            var isthere = _.indexOf(visibleslides_with_bleed, index);
                            if (isthere != -1) {
                                $(value).addClass("inbleed");
                            } else {
                                $(value).removeClass("inbleed");
                            }
                        });
                        $.each(visibleslides_with_bleed, function(index, value) {
                            $(childObjects[value]).find("[data-imageUrl]").each(function() {
                                var tempimageCAN = $(this).attr("data-imageUrl");
                                var tempimageSRC = $(this).attr("src");
                                if (tempimageCAN != tempimageSRC) {
                                    $(this).attr("src", tempimageCAN);
                                }
                            });
                        });
                    },
                    custom_MissingbleedIndex: function() {
                        var self = this;
                        var visibleslides_with_bleed = this.visibleslides_with_bleed.reverse();
                        var returnCandidate = undefined;
                        $.each(visibleslides_with_bleed, function(index, value) {
                            var isMissing = self.custom_contentData[value] == undefined;
                            if (isMissing) {
                                returnCandidate = value;
                            }
                        });
                        return returnCandidate;
                    },
                    custom_jsontotemplate: new jsontoTemplate({
                        dataIN: undefined,
                        htmlIN: undefined,
                        callback: function(data) {
                            alert(data);
                        }
                    }),
                    custom_Dataparser: new jsonParser({
                        usenocache: _global$.getQuery("debug"),
                        file: undefined,
                        format: "json",
                        callback: function(data) {
                            alert(JSON.stringify(data.items));
                        }
                    }),
                    custom_uiData: uiData,
                    custom_contentData: [],
                    custom_datatoIndex: function(getData) {
                        var self = this;
                        _global$.notify("Trace", "custom_datatoIndex(" + getData.index + ") >>> " + getData.data);
                        var getHTML = $(self.childObjects[getData.index]).html();
                        self.custom_jsontotemplate._var({
                            dataIN: modifyData(getData.data),
                            htmlIN: getHTML,
                            callback: function(data) {
                                $(self.childObjects[getData.index]).empty().append(modifyHTML(data));
                            }
                        });
                        self.custom_jsontotemplate.refresh();
                        self.custom_updateClassesAndImagePreload();
                    },
                    target: targetListContainer,
                    attachdisableclassonid: $(value),
                    attachhidearrowclassonid: $(value),
                    usetouchbinds: _global$.isTouch,
                    callback: function(o) {
                        this.custom_updateClassesAndImagePreload(o);
                    },
                    onscroll_pause: function(o) {
                        var self = this;
                        if (this.custom_uiData.dataservice != undefined) {
                            var missingIndexData = this.custom_MissingbleedIndex();
                            if (missingIndexData != undefined) {
                                var numberofItemstoget = Number(this.custom_uiData.datacount);
                                var myDataURL = this.custom_uiData.dataservice;
                                var convertIndextoPage = function(getIndex) {
                                    var pagecandidate = Math.floor(getIndex / numberofItemstoget);
                                    return pagecandidate;
                                };
                                var getPage = convertIndextoPage(missingIndexData);
                                var convertIndextoHardIndex = function(getIndex) {
                                    var pagecandidate = convertIndextoPage(getIndex) * numberofItemstoget;
                                    return pagecandidate;
                                };
                                _global$.notify("Trace", " received index:" + missingIndexData + " calculate page:" + convertIndextoPage(missingIndexData) + " start replacement index:" + convertIndextoHardIndex(missingIndexData));
                                var myDataURLwithCount = myDataURL + "/" + getPage + (numberofItemstoget != undefined ? "/" + numberofItemstoget : "");
                                if (!_global$.islocalhost) {
                                    myDataURL = myDataURLwithCount;
                                }
                                _global$.notify("Trace", "attempting to retrieve:" + myDataURLwithCount + "  to index:" + missingIndexData + "  page:" + convertIndextoPage(missingIndexData));
                                for (tempvar = 0; tempvar < numberofItemstoget; tempvar++) {
                                    $(self.childObjects[convertIndextoHardIndex(missingIndexData) + tempvar]).addClass("loading");
                                    $(self.childObjects[convertIndextoHardIndex(missingIndexData) + tempvar]).removeClass("notloaded");
                                }
                                self.custom_Dataparser._var({
                                    file: myDataURL,
                                    callback: function(data) {
                                        var listData = data.items;
                                        $.each(listData, function(index, value) {
                                            var useIndex = convertIndextoPage(missingIndexData) * numberofItemstoget + index;
                                            var useData = value;
                                            self.custom_contentData[useIndex] = useData;
                                            self.custom_datatoIndex({
                                                index: useIndex,
                                                data: useData
                                            });
                                            $(self.childObjects[useIndex]).removeClass("loading");
                                        });
                                    }
                                });
                                self.custom_Dataparser.refresh();
                            }
                        } else {
                            this.custom_updateClassesAndImagePreload(o);
                        }
                    },
                    onscroll_debounce_callback: function(o) {
                        this.custom_updateClassesAndImagePreload(o);
                    }
                }),
                data: uiData
            });
        });
        $.each(pageCarousels, function(index, value) {
            var _InitialBuild = function(getDATA) {
                var listContainer = $(value.obj).find(value.data.container);
                var htmltemplateHolder = $(value.obj).find(value.data.template);
                var htmlTemplate = htmltemplateHolder.html();
                var htmlTemplateStrip = htmlTemplate.replace(/\s/g, "");
                var itemCount = Number(getDATA.total);
                if (htmltemplateHolder.length != 0 && htmlTemplateStrip.length != 0) {
                    listContainer.empty();
                    for (temp_count = 0; temp_count < itemCount; temp_count++) {
                        var injectData = getDATA.items[temp_count];
                        if (injectData != undefined) {
                            var pluggedInHTML = new jsontoTemplate({
                                dataIN: modifyData(injectData),
                                htmlIN: htmlTemplate,
                                callback: function(data) {
                                    listContainer.append(modifyHTML(data));
                                }
                            }).init();
                        } else {
                            listContainer.append(htmlTemplate);
                            listContainer.children().last().addClass("notloaded");
                        }
                    }
                    initializeScroller(value, getDATA);
                } else {
                    _global$.notify("Trace", 'There is no template called "' + value.data.template + '" or the content is blank!');
                }
            };
            if (value.data.dataservice != undefined) {
                var myDataURL = value.data.dataservice;
                var myDataURLwithCount = myDataURL + "/0" + (value.data.datacount != undefined ? "/" + value.data.datacount : "");
                if (!_global$.islocalhost) {
                    myDataURL = myDataURLwithCount;
                }
                _global$.notify("Trace", "attempting to retrieve:" + myDataURLwithCount);
                var myjsontest = new jsonParser({
                    usenocache: _global$.getQuery("debug"),
                    file: myDataURL,
                    format: "json",
                    callback: function(data) {
                        _InitialBuild(data);
                    }
                });
                myjsontest.init();
            } else {
                initializeScroller(value);
            }
        });
        if (_global$.path.indexOf("watch-full-episodes-online") != -1) {
            makeAd();
            showhideOnAdobePass();
            placeAd();
            $("body").prepend('<div id="back-top" class="hidethis"><img src="profiles/mylifetime_com/themes/custom/ltv/images/show-landing-button-up.png"></div>');
            _notify.add("slider_instance", function() {
                return {
                    onWindowWidth: function(o) {
                        placeAd();
                    },
                    onWindowScrollStart: function(o) {},
                    onWindowScrollEnd: function(o) {
                        if (_global$.lastscrollpositionY > _global$.windowHeight) {
                            $("#back-top").removeClass("hidethis");
                        } else {
                            $("#back-top").addClass("hidethis");
                        }
                        placeAd();
                    }
                };
            }());
        } else {
            showhideOnAdobePass();
            $("body").prepend('<div id="back-top" class="hidethis"><img src="profiles/mylifetime_com/themes/custom/ltv/images/show-landing-button-up.png"></div>');
            _notify.add("slider_instance", function() {
                return {
                    onWindowScrollEnd: function(o) {
                        if (_global$.lastscrollpositionY > _global$.windowHeight) {
                            $("#back-top").removeClass("hidethis");
                        } else {
                            $("#back-top").addClass("hidethis");
                        }
                    }
                };
            }());
        }
    };
    var makeButtons = function() {
        $(document).on("click", '[data-uiType^="button_jumpto"]', function(value) {
            window.location.href = $(this).attr("data-uiType").substring(14, 999);
        });
        $(document).on("click", '[data-uiType="adobePassLogin"]', function(value) {
            try {
                aetn.video.adobePass.login();
            } catch (e) {
                console.log("ALERT!: aetn.video.adobePass.login() failed, prerequisite js loaded? " + e);
            }
        });
        $(document).on(_global$.isTouch ? "touchstart" : "click", "#back-top", function(value) {
            TweenMax.to(window, 2, {
                scrollTo: {
                    y: 0
                },
                ease: Power2.easeOut
            });
        });
        applyFEtemplate();
    };
    var applyFEtemplate = function() {
        if ($('[data-uiType="full_episode_template"]').length == 0) {
            applyCarousels();
        } else {
            var templateHolder = new Array();
            var FEjsonplug = new jsontoTemplate({
                dataIN: undefined,
                htmlIN: undefined,
                regEX: /{{{(.*?)}}}/g
            });
            var FEjsonloader = new jsonParser({
                usenocache: _global$.getQuery("debug")
            });
            FEjsonplug._startlisteners();
            FEjsonloader._startlisteners();
            $.each($('[data-uiType="full_episode_template"]'), function(index, value) {
                $(value).addClass("hidden");
                var uiData = $(value).attr("data-uiData");
                uiData = eval("(" + uiData + ")");
                $('[data-uiType="' + uiData.loadmore_button_uiType + '"]').hide();
                if (uiData.loadlist != undefined) {
                    var buildloadurl = uiData.loadlist;
                    if (!_global$.islocalhost) {
                        buildloadurl += "/0/" + uiData.loadcount;
                    }
                    templateHolder.push({
                        target_obj: value,
                        html_template: $(value).html(),
                        data: uiData,
                        loadPageCount: uiData.loadcount,
                        currentPage: 0
                    });
                    FEjsonloader._var({
                        file: buildloadurl,
                        callback: function(newloaddata) {
                            var originalHTML = $(value).html();
                            $(value).empty();
                            $.each(newloaddata.items, function(e_index, e_value) {
                                e_value = $.extend(e_value, uiData);
                                FEjsonplug._var({
                                    dataIN: modifyData(e_value),
                                    htmlIN: originalHTML,
                                    callback: function(renderedHTML) {
                                        $(value).append(modifyHTML(renderedHTML));
                                    }
                                });
                                FEjsonplug.refresh();
                            });
                            $(value).removeClass("hidden");
                            if (Number(newloaddata.total) > $(value).children().length) {
                                $('[data-uiType="' + uiData.loadmore_button_uiType + '"]').show();
                            }
                            applyCarousels();
                        }
                    });
                    FEjsonloader.refresh();
                } else {
                    alert("please define loadlist (json data for the scrollers)!");
                }
            });
            $.each(templateHolder, function(index, value) {
                var self = templateHolder[index];
                $(document).on("click", '[data-uiType="' + value.data.loadmore_button_uiType + '"]', function() {
                    var thisButton = this;
                    var uiData = value.data;
                    var buildloadurl = uiData.loadlist;
                    value.currentPage++;
                    if (!_global$.islocalhost) {
                        buildloadurl += "/" + value.currentPage + "/" + value.loadPageCount;
                    }
                    FEjsonloader._var({
                        file: buildloadurl,
                        callback: function(newloaddata) {
                            var originalHTML = value.html_template;
                            $.each(newloaddata.items, function(e_index, e_value) {
                                e_value = $.extend(e_value, uiData);
                                FEjsonplug._var({
                                    dataIN: modifyData(e_value),
                                    htmlIN: originalHTML,
                                    callback: function(renderedHTML) {
                                        $(value.target_obj).append(modifyHTML(renderedHTML));
                                    }
                                });
                                FEjsonplug.refresh();
                            });
                            var Showingall = $(value.target_obj).children().length >= Number(newloaddata.total);
                            if (newloaddata.length < Number(value.loadPageCount) || Showingall) {
                                $(thisButton).hide();
                            }
                            applyCarousels();
                        }
                    });
                    FEjsonloader.refresh();
                });
            });
        }
    };
    var startAdobePassListeners = function() {
        try {
            aetn.video.adobePass.addEventListener("displayUserAsUnAuthenticated", function() {
                console.log("removeClass(hideOnVLoginStart)");
                $('[data-uiType="hideOnVLoginStart"]').removeClass("hideOnVLoginStart");
            });
            aetn.video.adobePass.addEventListener("displayUserAsAuthenticated", function() {
                console.log("addClass(hideOnVLoginStart)");
                $('[data-uiType="hideOnVLoginStart"]').addClass("hideOnVLoginStart");
            });
        } catch (e) {
            console.log("keep hidden if On Adobe.pass FAIL");
        }
    };
    var showhideOnAdobePass = function() {
        var isAndroid = $("body").hasClass("android");
        var isIOS = $("body").hasClass("iphone") || $("body").hasClass("ipad");
        $('[data-uiType="hideOnVLoginStart"]').removeClass("hideOnVLoginStart");
    };
    makeButtons();
    $(".hideOnMobile").each(function(idx, value) {
        var isAndroid = $("body").hasClass("android");
        var isIOS = $("body").hasClass("iphone") || $("body").hasClass("ipad");
        if (!(isAndroid || isIOS)) {
            $(value).removeClass("hideOnMobile");
        }
    });
});

define("instances/drupal_footer_global.js", function() {});
