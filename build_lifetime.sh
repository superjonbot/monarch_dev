cd ~/Sites/monarch_dev/js
r.js -o buildtemplate_lifetime_required.js
r.js -o buildtemplate_lifetime_slider.js

java -jar compressors/yuicompressor-2.4.8.jar exports/lifetime/lifetime_require.js -o exports/lifetime/lifetime_require_min.js
java -jar compressors/yuicompressor-2.4.8.jar exports/lifetime/lifetime_custom_carousel.js -o exports/lifetime/lifetime_custom_carousel_min.js


cp -f ~/Sites/monarch_dev/js/exports/lifetime/* ~/Sites/devbox_maids/webroot/mlt-d7/profiles/mylifetime_com/themes/custom/ltv/js/