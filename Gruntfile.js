module.exports = function(grunt) {

    var versionnumber = '<%= pkg.version %>.' + (Math.floor(new Date().getTime() / 100000000000) / 10);
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('../package.json'),
        banner: '/*! Package: <%= pkg.name %> v.' + versionnumber + ' by Jonathan Robles */\n' +
            '/*! released:<%= grunt.template.today("mm-dd-yyyy [h:MM:ssTT]") %> */\n',

        jshint: {
            // define the files to lint
            files: ['Gruntfile.js'],
            // configure JSHint (documented at http://www.jshint.com/docs/)
            options: {
                // more options here if you want to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true
                }
            }
        },

        requirejs: {
            lifetime_require: {
                options: {
                	baseUrl: "js",

                    mainConfigFile: 'libraries/etc/env/config_grunt.js',
                    optimize: 'none',
                    preserveLicenseComments: false,
                    name: "libraries/etc/required.js",
                    out: "exports/lifetime/lifetime_require.js",
                    paths: {
                        almondLib: 'libraries/almond/almond',
                        requireLib: 'libraries/require/require',
                        requireConfig: 'libraries/etc/env/config_grunt',
                        underscore: 'libraries/underscore0x/underscore',
                        jquery: "libraries/jquery0x/jquery"
                    },
                    include: ['almondLib', 'requireConfig']
                }
            }
            /*,
            lifetime_slider: {
                options: {
                    baseUrl: "js",
                    mainConfigFile: 'js/libraries/etc/env/config_grunt.js',
                    optimize: 'none',
                    preserveLicenseComments: false,
                    name: "js/instances/slideshow_lifetimeslider_instance.js",
                    out: "exports/lifetime/lifetime_custom_carousel.js",
                    paths: {
                    	almondLib: 'js/libraries/almond/almond',
                        requireLib: 'js/libraries/require/require',
                        requiredLib: 'js/libraries/etc/required',
                        jquery: "js/libraries/jquery0x/jquery",
                        modernizr: "js/libraries/modernizr0x/modernizr"
                    }
                }
            }*/
        },

        //Minify!!!
        uglify: {
            options: {
                banner: '<%= banner %>'
            },
            build: {
                src: 'js/exports/lifetime/lifetime_require.js',
                dest: 'js/exports/lifetime/lifetime_require_min.js'
            }
        },

        copy: {
            main: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        flatten: true,
                        src: ['js/exports/lifetime/*'],
                        dest: 'js/exports/lifetime/lifetime_require_v' + versionnumber + '/',
                        filter: 'isFile'
                    }
                ],
            },
        }

    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    // Default task(s).
    grunt.registerTask('default', ['jshint', 'requirejs:lifetime_require', 'uglify', 'copy']);

};