/**
 * ${FILE_NAME} Instance
 * sample by Jonathan Robles
 *
 * Date: ${DATE} : ${TIME}
 *
 */

// Start main ${FILE_NAME} instance

require(['jquery', 'modules/${module_directory}/${FILE_NAME}'],
    function ($,${module_reference}) {

        var myinstancetest = new ${module_reference}({
            //variable:value
        });

        myinstancetest.init();


    });


// End main ${FILE_NAME} instance




